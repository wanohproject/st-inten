function changesoal(no) {
  var sounds = document.getElementsByTagName('audio');
  for(i=0; i<sounds.length; i++) sounds[i].pause();

  var video = document.getElementsByTagName('video');
  for(i=0; i<video.length; i++) video[i].pause();

  akhir = $("#jumlah_soal").val();
  if (no == 0) {
  } else if (no == akhir) {
    $("#nomor span").html(no);
    $(".no").removeClass("active");
    $(".no.no-" + no).addClass("active");
    $("#next-soal").hide();
    $("#last-soal").show();
  } else {
    $("#nomor span").html(no);
    $(".no").removeClass("active");
    $(".no.no-" + no).addClass("active");
    $("#next-soal").show();
    $("#last-soal").hide();
  }

  $(".ragu-check").removeClass("glyphicon-check");
  $(".ragu-check").removeClass("glyphicon-unchecked");
  if ($(".no.active").hasClass("ragu-ragu")) {
    $(".ragu-check").addClass("glyphicon-check");
  } else {
    $(".ragu-check").addClass("glyphicon-unchecked");
  }
}

$(document).ready(function() {

  $(".ragu-check").removeClass("glyphicon-check");
  $(".ragu-check").removeClass("glyphicon-unchecked");
  if ($(".no.active").hasClass("ragu-ragu")) {
    $(".ragu-check").addClass("glyphicon-check");
  } else {
    $(".ragu-check").addClass("glyphicon-unchecked");
  }

  $(".option").click(function() {
    $("#ajax").show();
    $(this).closest(".options-group")
      .find(".option")
      .each(function() {
        $(this).removeClass("checked");
      });

    act_no = $(this);
    act_no.addClass("checked");
    nomor = $(this).attr("data-nomor");
    ragu = $(".no.no-" + nomor).hasClass("ragu-ragu");
    nomor_asli = $(this).attr("data-nomor-asli");
    opt = $(this).attr("data-option");
    opt_asli = $(this).attr("data-option-asli");
    userid = $("#userid").val();
    if (ragu) {
      ragu = "Y";
    } else {
      ragu = "N";
    }

    act_no.removeClass("checked");
    $.post(
      site_url + "/ujian2/jawab",
      {
        userid: userid,
        nomor: nomor,
        nomor_asli: nomor_asli,
        option: opt,
        option_asli: opt_asli,
        ragu: ragu
      },
      function(s) {
        if (s.message == "Logout") {
          alert("UserId anda telah di reset oleh Proktor, anda akan Logout");
          window.location = site_url + '/ujian2/logout';
        }
        if (s.response == true){
          $("#ajax").hide();
          act_no.addClass("checked");
          $("#serial-no-" + nomor).val(opt);
          nomor = act_no.attr("data-nomor");
          $(".no.no-" + nomor + " span").html(act_no.html());
          $(".no.no-" + nomor).addClass("done");
          $(".no.no-" + nomor).removeClass("not-done");
          console.log(s);
        }
      }
    );
  });

  $(".essay").focusout(function(event) {
    $("#ajax").show();
    act_no = $(this);
    jawaban = $(this).val();
    nomor = $(this).attr("data-nomor");
    ragu = $(".no.no-" + nomor).hasClass("ragu-ragu");
    nomor_asli = $(this).attr("data-nomor-asli");
    opt = jawaban;
    userid = $("#userid").val();
    if (ragu) {
      ragu = "Y";
    } else {
      ragu = "N";
    }

    act_no.removeClass("checked");
    $.post(
      site_url + "/ujian2/esai",
      {
        userid: userid,
        nomor: nomor,
        nomor_asli: nomor_asli,
        option: opt,
        ragu: ragu
      },
      function(s) {
        if (s.message == "Logout") {
          alert("UserId anda telah di reset oleh Proktor, anda akan Logout");
          window.location = site_url + '/ujian2/logout';
        }
        if (s.response == true){
          if (s.message == "Success"){
            act_no.addClass("checked");
            $("#serial-no-" + nomor).val(opt);
            nomor = act_no.attr("data-nomor");
            $(".no.no-" + nomor).addClass("done");
            $(".no.no-" + nomor).removeClass("not-done");
          } else {
            act_no.removeClass("checked");
            $("#serial-no-" + nomor).val(opt);
            nomor = act_no.attr("data-nomor");
            $(".no.no-" + nomor).addClass("not-done");
            $(".no.no-" + nomor).removeClass("done");
          }
        } else {
          act_no.removeClass("checked");
          $("#serial-no-" + nomor).val(opt);
          nomor = act_no.attr("data-nomor");
          $(".no.no-" + nomor).addClass("not-done");
          $(".no.no-" + nomor).removeClass("done");
        }
        $("#ajax").hide();
      }
    );
  });

  $(".no.done").removeClass("not-done");

  $(".ragu").click(function() {
    a = $(this).find(".ragu-check");
    if (a.hasClass("glyphicon-unchecked")) {
      a.removeClass("glyphicon-unchecked");
      a.addClass("glyphicon-check");
      nomor = $(".soal.active")
        .find(".nomor")
        .text();
      nomor_asli = $(".soal.active")
        .find(".nomor_asli")
        .text();
      $(".no.no-" + nomor).addClass("ragu-ragu");
      userid = $("#userid").val();
      $.post(
        site_url + "/ujian2/ragu",
        {
          userid: userid,
          nomor: nomor,
          nomor_asli: nomor_asli,
          ragu: "Y"
        },
        function(s) {
          if (s.message == "Logout") {
            alert("UserId anda telah di reset oleh Proktor, anda akan Logout");
            window.location = site_url + '/ujian2/logout';
          }
        }
      );
    } else {
      a.removeClass("glyphicon-check");
      a.addClass("glyphicon-unchecked");
      nomor = $(".soal.active")
        .find(".nomor")
        .text();
      $(".no.no-" + nomor).removeClass("ragu-ragu");
    }
    $(".soal.active .option.checked").click();
  });

  $(".no").click(function() {
    nomor = $(this)
      .find("p")
      .html();
    $(".soal").removeClass("active");
    $(".soal.nomor-" + nomor).addClass("active");
    changesoal(nomor);
  });

  $("#next-soal").click(function() {
    $(".soal.active")
      .next()
      .addClass("active");
    $(".soal.active")
      .eq(0)
      .removeClass("active");
    nomor = $(".soal.active")
      .find(".nomor")
      .text();
    changesoal(nomor);
  });
  $("#prev-soal").click(function() {
    $(".soal.active")
      .prev()
      .addClass("active");
    $(".soal.active")
      .eq(1)
      .removeClass("active");
    nomor = $(".soal.active")
      .find(".nomor")
      .text();
    changesoal(nomor);
  });

  $(".close").click(function() {
    $(".modal").hide();
  });
  $(".close-modal").click(function() {
    $(".modal").hide();
  });

  $("#yakin").change(function() {
    if ($(this).is(":checked")) {
      $("#selesai").removeClass("btn-default");
      $("#selesai").addClass("btn-success");
    } else {
      $("#selesai").removeClass("btn-success");
      $("#selesai").addClass("btn-default");
    }
  });

  $('#last-soal').click(function(){
		if ($('.not-done').length>0) {
			$('#ragu-modal').show();
		} else {
			if ($('.ragu-ragu').length>0) {
				$('#ragu-modal').show();
			} else {
				$('#yakin-modal').show();
			}
		}
		
	})
 
	$('#selesai').click(function(){
		if ($(this).hasClass('btn-success')){
			$.post(site_url + '/ujian2/finish',{
				userid : $("#userid").val()
			},function(s){
        if (s.response == true){
          window.location = s.url;
        } 
			});
		} else {
		}
	})

  $(".a3").click(function() {
    fontsize = parseInt($(".soal").css("font-size"));
    font = fontsize + 5 + "px";
    font2 = fontsize + 3 + "px";
    optslineheight = fontsize + 1 + "px";
    optsleft = fontsize + 20 + "px";
    width = $(".option").css("width");
    width = parseInt(width.replace("px", "")) + 4 + "px";
    height = $(".option").css("height");
    height = parseInt(height.replace("px", "")) + 4 + "px";
    $(".soal").css({ "font-size": font });
    $(".soal span").css({ "font-size": font });
    $(".option").css({ "font-size": font2 });
    $(".option").css({ "line-height": font2 });
    $(".option").css({ height: height });
    $(".option").css({ width: width });
    $(".options p").css({ "line-height": optslineheight });
    $(".options p").css({ "left": optsleft });
  });

  $(".a1").click(function() {
    fontsize = parseInt($(".soal").css("font-size"));
    font = fontsize - 5 + "px";
    font2 = fontsize - 3 + "px";
    optslineheight = fontsize + 1 + "px";
    optsleft = fontsize + 20 + "px";
    width = $(".option").css("width");
    width = parseInt(width.replace("px", "")) - 4 + "px";
    height = $(".option").css("height");
    height = parseInt(height.replace("px", "")) - 4 + "px";
    $(".soal").css({ "font-size": font });
    $(".soal span").css({ "font-size": font });
    $(".option").css({ "font-size": font2 });
    $(".option").css({ "line-height": font2 });
    $(".option").css({ height: height });
    $(".option").css({ width: width });
    $(".options p").css({ "line-height": optslineheight });
    $(".options p").css({ "left": optsleft });
  });

  $(".a2").click(function() {
    fontsize = "16px";
    $(".soal").css({ "font-size": fontsize });
    $(".soal span").css({ "font-size": fontsize });
    $(".option").css({ height: "26px" });
    $(".option").css({ width: "26px" });
    $(".option").css({ "font-size": "16px" });
    $(".option").css({ "line-height": "16px" });
    $(".options p").css({ "line-height": "20px" });
    $(".options p").css({ "left": "35px" });
  });
});