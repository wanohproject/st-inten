function changesoal(no) {
  akhir = $("#jumlah_soal").val();
  if (no == 0) {
  } else if (no == akhir) {
    $("#nomor span").html(no);
    $(".no").removeClass("active");
    $(".no.no-" + no).addClass("active");
    $("#next-soal").hide();
    $("#last-soal").show();

    $("#ajax2").show();
    $("#soal-body").html("<div style=\"margin:50px auto;width:50px;\"><img src=\""+ base_url+ "/asset/image/loader.gif\" /></div>");
    $.post( site_url + "/ujian2/get_soal", { nomor: no}, function( data ) {
      if (data == "NOTFOUND"){
        window.location = site_url + '/jadwal-ujian';
      } else {
        $("#soal-body").html(data);
        $("#ajax2").hide();
      }
    });
    
  } else {
    $("#nomor span").html(no);
    $(".no").removeClass("active");
    $(".no.no-" + no).addClass("active");
    $("#next-soal").show();
    $("#last-soal").hide();
    
    $("#ajax2").show();
    $.post( site_url + "/ujian2/get_soal", { nomor: no}, function( data ) {
      if (data == "NOTFOUND"){
        window.location = site_url + '/jadwal-ujian';
      } else {
        $("#soal-body").html(data);
        $("#ajax2").hide();
      }
    });
  }

  $(".ragu-check").removeClass("glyphicon-check");
  $(".ragu-check").removeClass("glyphicon-unchecked");
  if ($(".no.active").hasClass("ragu-ragu")) {
    $(".ragu-check").addClass("glyphicon-check");
  } else {
    $(".ragu-check").addClass("glyphicon-unchecked");
  }
}

$(document).ready(function() {

  $.post( site_url + "/ujian2/get_soal", { nomor: 1}, function( data ) {
	  $("#soal-body").html(data);
	});

  $(".ragu-check").removeClass("glyphicon-check");
  $(".ragu-check").removeClass("glyphicon-unchecked");
  if ($(".no.active").hasClass("ragu-ragu")) {
    $(".ragu-check").addClass("glyphicon-check");
  } else {
    $(".ragu-check").addClass("glyphicon-unchecked");
  }
  
  $(".no.done").removeClass("not-done");

  $(".ragu").click(function() {
    $("#ajax2").show();
    a = $(this).find(".ragu-check");
    if (a.hasClass("glyphicon-unchecked")) {
      a.removeClass("glyphicon-unchecked");
      a.addClass("glyphicon-check");
      nomor = $(".soal.active").find(".nomor").text();
      nomor_asli = $(".soal.active").find(".nomor_asli").text();
      $(".no.no-" + nomor).addClass("ragu-ragu");
      userid = $("#userid").val();
      $.post(
        site_url + "/ujian2/ragu",
        {
          userid: userid,
          nomor: nomor,
          nomor_asli: nomor_asli,
          ragu: "Y"
        },
        function(s) {
          $("#ajax2").hide();
          if (s.message == "Logout") {
            alert("UserId anda telah di reset oleh Proktor, anda akan Logout");
            window.location = site_url + '/ujian2/logout';
          }
        }
      );
    } else {
      a.removeClass("glyphicon-check");
      a.addClass("glyphicon-unchecked");
      nomor = $(".soal.active").find(".nomor").text();
      nomor_asli = $(".soal.active").find(".nomor_asli").text();
      $(".no.no-" + nomor).removeClass("ragu-ragu");
      userid = $("#userid").val();
      $.post(
        site_url + "/ujian2/ragu",
        {
          userid: userid,
          nomor: nomor,
          nomor_asli: nomor_asli,
          ragu: "N"
        },
        function(s) {
          if (s.message == "Logout") {
            alert("UserId anda telah di reset oleh Proktor, anda akan Logout");
            window.location = site_url + '/ujian2/logout';
          }
        }
      );
    }
    $(".soal.active .option.checked").click();
  });

  $(".no").click(function() {
    nomor = $(this).find("p").html();
    $("#current_soal").val(nomor);
    changesoal(nomor);
  });

  $("#next-soal").click(function() {
    nomor = parseInt($("#current_soal").val());
    jumlah_soal = parseInt($("#jumlah_soal").val());
    if (nomor < jumlah_soal){
      nomor++;
    }
    $("#current_soal").val(nomor);
    changesoal(nomor);
  });
  $("#prev-soal").click(function() {
    nomor = parseInt($("#current_soal").val());
    jumlah_soal = parseInt($("#jumlah_soal").val());
    if (nomor > 1){
      nomor--;
    }
    $("#current_soal").val(nomor);
    changesoal(nomor);
  });

  $(".close").click(function() {
    $(".modal").hide();
  });
  $(".close-modal").click(function() {
    $(".modal").hide();
  });

  $("#yakin").change(function() {
    if ($(this).is(":checked")) {
      $("#selesai").removeClass("btn-default");
      $("#selesai").addClass("btn-success");
    } else {
      $("#selesai").removeClass("btn-success");
      $("#selesai").addClass("btn-default");
    }
  });

  $('#last-soal').click(function(){
		// if ($('.not-done').length>0) {
		// 	$('#ragu-modal').show();
		// } else {
		// 	if ($('.ragu-ragu').length>0) {
		// 		$('#ragu-modal').show();
		// 	} else {
				$('#yakin-modal').show();
		// 	}
		// }
		
	})
 
	$('#selesai').click(function(){
		if ($(this).hasClass('btn-success')){
			$.post(site_url + '/ujian2/finish',{
				userid : $("#userid").val()
			},function(s){
        if (s.response == true){
          window.location = s.url;
        } 
			});
		} else {
		}
	})

  $(".a3").click(function() {
    fontsize = parseInt($(".soal").css("font-size"));
    font = fontsize + 5 + "px";
    font2 = fontsize + 3 + "px";
    optslineheight = fontsize + 1 + "px";
    optsleft = fontsize + 20 + "px";
    width = $(".option").css("width");
    width = parseInt(width.replace("px", "")) + 4 + "px";
    height = $(".option").css("height");
    height = parseInt(height.replace("px", "")) + 4 + "px";
    $(".soal").css({ "font-size": font });
    $(".soal span").css({ "font-size": font });
    $(".option").css({ "font-size": font2 });
    $(".option").css({ "line-height": font2 });
    $(".option").css({ height: height });
    $(".option").css({ width: width });
    $(".options p").css({ "line-height": optslineheight });
    $(".options p").css({ "left": optsleft });
  });

  $(".a1").click(function() {
    fontsize = parseInt($(".soal").css("font-size"));
    font = fontsize - 5 + "px";
    font2 = fontsize - 3 + "px";
    optslineheight = fontsize + 1 + "px";
    optsleft = fontsize + 20 + "px";
    width = $(".option").css("width");
    width = parseInt(width.replace("px", "")) - 4 + "px";
    height = $(".option").css("height");
    height = parseInt(height.replace("px", "")) - 4 + "px";
    $(".soal").css({ "font-size": font });
    $(".soal span").css({ "font-size": font });
    $(".option").css({ "font-size": font2 });
    $(".option").css({ "line-height": font2 });
    $(".option").css({ height: height });
    $(".option").css({ width: width });
    $(".options p").css({ "line-height": optslineheight });
    $(".options p").css({ "left": optsleft });
  });

  $(".a2").click(function() {
    fontsize = "16px";
    $(".soal").css({ "font-size": fontsize });
    $(".soal span").css({ "font-size": fontsize });
    $(".option").css({ height: "26px" });
    $(".option").css({ width: "26px" });
    $(".option").css({ "font-size": "16px" });
    $(".option").css({ "line-height": "16px" });
    $(".options p").css({ "line-height": "20px" });
    $(".options p").css({ "left": "35px" });
  });
});

function getUserIP(onNewIP) {
  var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
  var pc = new myPeerConnection({
      iceServers: []
  }),
  noop = function() {},
  localIPs = {},
  ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
  key;

  function iterateIP(ip) {
      if (!localIPs[ip]) onNewIP(ip);
      localIPs[ip] = true;
  }

  pc.createDataChannel('');

  pc.createOffer().then(function(sdp) {
      sdp.sdp.split('\n').forEach(function(line) {
          if (line.indexOf('candidate') < 0) return;
          line.match(ipRegex).forEach(iterateIP);
      });
      
      pc.setLocalDescription(sdp, noop, noop);
  }).catch(function(reason) {
  });

  pc.onicecandidate = function(ice) {
      if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
      ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
  };
}

getUserIP(function(ip){
  document.getElementById("current_ip").value = ip;
});