<?php
class Loginnew_Model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	function cek_login($where){
		$data = "";
		if ($where['pengguna_nama'] && $where['pengguna_kunci']){
			$this->db->select("*");
			$this->db->from("pengguna");
			$this->db->where(array('pengguna_nama'=>$where['pengguna_nama']));
			$Q = $this->db->get();
			if ($Q->num_rows() > 0){
				$data = $Q->result();
				foreach ($data as $row) {
					if ($this->bcrypt->check_password($where['pengguna_kunci'], $row->pengguna_kunci)){
						$this->set_session($row);
						return true;
					}
				}
			}
		}
		return false;
	}

	function cek_user($where){
		$data = "";
		if ($where['pengguna_nama'] && $where['pengguna_kunci']){
			$this->db->select("*");
			$this->db->from("pengguna");
			$this->db->where(array('pengguna_nama'=>$where['pengguna_nama']));
			$Q = $this->db->get();
			if ($Q->num_rows() > 0){
				$data = $Q->result();
				foreach ($data as $row) {
					if ($this->bcrypt->check_password($where['pengguna_kunci'], $row->pengguna_kunci)){
						return true;
					}
				}
			}
		}
		return false;
	}

	function cek_byid($where){
		$data = "";
		if ($where['pengguna_id'] && $where['pengguna_kunci']){
			$this->db->select("*");
			$this->db->from("pengguna");
			$this->db->where(array('pengguna_id'=>$where['pengguna_id']));
			$Q = $this->db->get();
			if ($Q->num_rows() > 0){
				$data = $Q->result();
				foreach ($data as $row) {
					if ($this->bcrypt->check_password($where['pengguna_kunci'], $row->pengguna_kunci)){
						return true;
					}
				}
			}
		}
		return false;
	}

	function set_session(&$data){
		$full_sess = array(
			'id'				=> $data->pengguna_id,
			'user'				=> $data->pengguna_nama,
			'level'				=> $data->pengguna_level_id,
			'logged_in'			=> TRUE
		);
		
		$session = array(
			'id'				=> $data->pengguna_id,
			'user'				=> $data->pengguna_nama,
			'level'				=> $data->pengguna_level_id,
			'logged_in'			=> TRUE,
			'sess_datafinder'	=> 'eb3555b67d206fc7a9fe174c511e9c13',
			'sess_full'			=> $full_sess
		);
		$this->session->set_userdata($session);	
	}
	
	function update_log(&$data){
		$where['pengguna_nama'] = $data->pengguna_nama;
		$data['pengguna_ip']	= $_SERVER['REMOTE_ADDR'];
		$data['pengguna_online']= time();
		$this->db->update('pengguna', $data, $where);
	}
	
	function remov_session(){
		$session = array(
			'id'				=> '',
			'user' 				=> '',
			'level' 			=> '',
			'logged_in' 		=> FALSE,	
			'sess_datafinder'	=> '',
			'sess_full'			=> ''
		);
		$this->session->unset_userdata($session);	
	}
}

/* End of file login_model.php */
/* Location: ./application/models/login_model.php */