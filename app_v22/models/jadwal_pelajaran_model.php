<?php
class Jadwal_pelajaran_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel akd_jadwal_pelajaran
	public function insert_jadwal_pelajaran($data){
        return $this->db->insert("akd_jadwal_pelajaran",$data);
    }
    
    public function update_jadwal_pelajaran($where,$data){
        return $this->db->update("akd_jadwal_pelajaran",$data,$where);
    }

    public function delete_jadwal_pelajaran($where){
        return $this->db->delete("akd_jadwal_pelajaran", $where);
    }

	public function get_jadwal_pelajaran($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_jadwal_pelajaran jadwal_pelajaran");
		$this->db->join("akd_matakuliah matakuliah", "jadwal_pelajaran.matakuliah_id=matakuliah.matakuliah_id", "left");
		$this->db->join("akd_dosen dosen", "jadwal_pelajaran.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('jadwal_pelajaran.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_jadwal_pelajaran($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_jadwal_pelajaran jadwal_pelajaran");
		$this->db->join("akd_matakuliah matakuliah", "jadwal_pelajaran.matakuliah_id=matakuliah.matakuliah_id", "left");
		$this->db->join("akd_dosen dosen", "jadwal_pelajaran.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_jadwal_pelajaran($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_jadwal_pelajaran jadwal_pelajaran");
		$this->db->join("akd_matakuliah matakuliah", "jadwal_pelajaran.matakuliah_id=matakuliah.matakuliah_id", "left");
		$this->db->join("akd_dosen dosen", "jadwal_pelajaran.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file akd_jadwal_pelajaran.php */
/* Location: ./application/models/akd_jadwal_pelajaran.php */