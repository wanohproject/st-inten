<?php
class Semester_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel semester
	public function insert_semester($data){
        $this->db->insert("akd_semester",$data);
    }
    
    public function update_semester($where,$data){
        $this->db->update("akd_semester",$data,$where);
    }

    public function delete_semester($where){
        $this->db->delete("akd_semester", $where);
    }

	public function get_semester($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_semester semester");
		$this->db->join("akd_tahun tahun", "semester.tahun_kode=tahun.tahun_kode", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('semester_kode','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
    }
	
	public function get_semester_aktif(){
        $data = "";
		$this->db->select("*");
        $this->db->from("akd_semester semester");
		$this->db->join("akd_tahun tahun", "semester.tahun_kode=tahun.tahun_kode", "left");
		$this->db->where(array("semester_aktif"=>"Y"));
		$this->db->order_by('semester_kode','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_semester($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_semester semester");
		$this->db->join("akd_tahun tahun", "semester.tahun_kode=tahun.tahun_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_semester($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_semester semester");
		$this->db->join("akd_tahun tahun", "semester.tahun_kode=tahun.tahun_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file semester.php */
/* Location: ./application/models/semester.php */