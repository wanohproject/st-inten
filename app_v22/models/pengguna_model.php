<?php
class Pengguna_Model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	public function insert_pengguna($data){
        $this->db->insert("pengguna",$data);
    }
    
    public function update_pengguna($where,$data){
        $this->db->update("pengguna",$data,$where);
    }

    public function delete_pengguna($where){
        $this->db->delete("pengguna", $where);
    }

	public function get_pengguna($select="*", $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("pengguna");
		$this->db->join('pengguna_level', 'pengguna.pengguna_level_id=pengguna_level.pengguna_level_id', 'left');
		$this->db->join('akd_perguruan_tinggi perguruan_tinggi', 'pengguna.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id', 'left');
		$this->db->join('akd_dosen dosen', 'pengguna.dosen_id=dosen.dosen_id', 'left');
		$this->db->join('akd_mahasiswa mahasiswa', 'pengguna.mahasiswa_id=mahasiswa.mahasiswa_id', 'left');
		if ($where){$this->db->where($where);}
		$this->db->order_by('pengguna.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_pengguna($select="*", $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("pengguna");
		$this->db->join('pengguna_level', 'pengguna.pengguna_level_id=pengguna_level.pengguna_level_id', 'left');
		$this->db->join('akd_perguruan_tinggi perguruan_tinggi', 'pengguna.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id', 'left');
		$this->db->join('akd_dosen dosen', 'pengguna.dosen_id=dosen.dosen_id', 'left');
		$this->db->join('akd_mahasiswa mahasiswa', 'pengguna.mahasiswa_id=mahasiswa.mahasiswa_id', 'left');
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_pengguna($where="", $like=""){
        $this->db->select("*");
        $this->db->from("pengguna");
		$this->db->join('pengguna_level', 'pengguna.pengguna_level_id=pengguna_level.pengguna_level_id', 'left');
		$this->db->join('akd_perguruan_tinggi perguruan_tinggi', 'pengguna.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id', 'left');
		$this->db->join('akd_dosen dosen', 'pengguna.dosen_id=dosen.dosen_id', 'left');
		$this->db->join('akd_mahasiswa mahasiswa', 'pengguna.mahasiswa_id=mahasiswa.mahasiswa_id', 'left');
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
	
	// konfigurasi tabel pengguna level
	public function insert_pengguna_level($data){
        $this->db->insert("pengguna_level",$data);
    }
    
    public function update_pengguna_level($where,$data){
        $this->db->update("pengguna_level",$data,$where);
    }

    public function delete_pengguna_level($where){
        $this->db->delete("pengguna_level", $where);
    }

	public function get_pengguna_level($select="*", $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("pengguna_level");
		if ($where){$this->db->where($where);}
		$this->db->order_by('pengguna_level_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_pengguna_level($select="*", $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("pengguna_level");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_pengguna_level($where="", $like=""){
        $this->db->select("*");
        $this->db->from("pengguna_level");		
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file pengguna.php */
/* Location: ./application/models/pengguna.php */