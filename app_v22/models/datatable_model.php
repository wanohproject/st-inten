<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Datatable_model extends CI_Model {
 
    var $table = '';
    var $column_order = array(); //set column field database for datatable orderable
    var $column_search = array(); //set column field database for datatable searchable 
    var $order = array(); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function set_table($table){
        $this->table = $table;
    }
 
    public function set_column_order($column_order){
        $this->column_order = $column_order;
    }
 
    public function set_column_search($column_search){
        $this->column_search = $column_search;
    }
 
    public function set_order($order){
        $this->order = $order;
    }
 
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
        $post_search = $this->input->post('search');
        $post_order = $this->input->post('order');
     
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($post_search['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->like($item, $post_search['value']);
                }
                else
                {
                    $this->db->or_like($item, $post_search['value']);
                }
            }
            $i++;
        }
         
        if($post_order) // here order processing
        {
            $this->db->order_by($this->column_order[$post_order['0']['column']], $post_order['0']['dir']);
        } 
        else if($this->order)
        {
            $order = $this->order;
            $this->db->order_by($order[0], $order[1]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($this->input->post('length') != -1)
        $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}