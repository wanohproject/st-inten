<?php
class Profil_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// tabel profil
	public function insert_profil($data){
        $this->db->insert("profil",$data);
    }
    
    public function update_profil($where,$data){
        $this->db->update("profil",$data,$where);
    }

    public function delete_profil($where){
        $this->db->delete("profil", $where);
    }

	public function get_profil($select="*", $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("profil");
		if ($where){$this->db->where($where);}
		$this->db->order_by('profil_id', 'DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_profil($select="*", $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("profil");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_profil($where="", $like=""){
        $this->db->select("*");
        $this->db->from("profil");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }		
}

/* End of file profil_model.php */
/* Location: ./application/models/profil_model.php */