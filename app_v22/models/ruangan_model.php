<?php
class Ruangan_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel ruangan
	public function insert_ruangan($data){
        $this->db->insert("akd_ruangan",$data);
    }
    
    public function update_ruangan($where,$data){
        $this->db->update("akd_ruangan",$data,$where);
    }

    public function delete_ruangan($where){
        $this->db->delete("akd_ruangan", $where);
    }

	public function get_ruangan($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_ruangan ruangan");
		if ($where){$this->db->where($where);}
		$this->db->order_by('ruangan_kode','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_ruangan($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_ruangan ruangan");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_ruangan($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_ruangan ruangan");		
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file ruangan.php */
/* Location: ./application/models/ruangan.php */