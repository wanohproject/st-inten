<?php
class referensi_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel ref_jenis
	public function insert_jenis($data){
        $this->db->insert("ref_jenis",$data);
    }
    
    public function update_jenis($where,$data){
        $this->db->update("ref_jenis",$data,$where);
    }

    public function delete_jenis($where){
        $this->db->delete("ref_jenis", $where);
    }

	public function get_jenis($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("ref_jenis jenis");
		if ($where){$this->db->where($where);}
		$this->db->order_by('jenis_value','ASC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_jenis($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("ref_jenis jenis");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_jenis($where="", $like=""){
        $this->db->select("*");
        $this->db->from("ref_jenis jenis");		
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
	}
	
	// konfigurasi tabel ref_kode
	public function insert_kode($data){
        $this->db->insert("ref_kode",$data);
    }
    
    public function update_kode($where,$data){
        $this->db->update("ref_kode",$data,$where);
    }

    public function delete_kode($where){
        $this->db->delete("ref_kode", $where);
    }

	public function get_kode($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("ref_kode kode");
        $this->db->join("ref_jenis jenis", "kode.jenis_id=jenis.jenis_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('kode_value','ASC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_kode($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("ref_kode kode");
        $this->db->join("ref_jenis jenis", "kode.jenis_id=jenis.jenis_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_kode($where="", $like=""){
        $this->db->select("*");
        $this->db->from("ref_kode kode");
        $this->db->join("ref_jenis jenis", "kode.jenis_id=jenis.jenis_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }

    public function combobox($jenis_value, $name, $value, $name_value, $pilihan, $js='', $label='', $script=''){
        $hasil = "<select name='$name' id='".str_replace(array("[", "]"), "_", $name)."' onchange='$js' $script>";
        if ($label != "none"){
            $hasil .= "<option value=''>".$label."</option>";
        }
		
        $kode = $this->grid_all_kode('', 'kode_value', 'ASC', 0, 0, "jenis.jenis_value = '$jenis_value'");
        foreach($kode as $row){
            if ($pilihan == $row->$value){
                $hasil .= "<option value='".$row->$value."' selected>".$row->$name_value."</option>";
            } else {
                $hasil .= "<option value='".$row->$value."'>".$row->$name_value."</option>";
            }
        }
		
		$hasil .= "</select>";
		return $hasil;
	}
    
    public function radiobutton($jenis_value, $name, $value, $name_value, $pilihan, $js='', $label='', $script=''){
        $hasil = "<div class=\"radio\">";
        $kode = $this->grid_all_kode('', 'kode_value', 'ASC', 0, 0, "jenis.jenis_value = '$jenis_value'");
        foreach($kode as $row){
            if ($pilihan == $row->$value){
                $hasil .= "<label><input type=\"radio\" name=\"$name\" value=\"".$row->$value."\" checked> ".$row->$name_value."</label>";
                $hasil .= "&nbsp;&nbsp;&nbsp;";
            } else {
                $hasil .= "<label><input type=\"radio\" name=\"$name\" value=\"".$row->$value."\"> ".$row->$name_value."</label>";
                $hasil .= "&nbsp;&nbsp;&nbsp;";
            }
        }
		
		$hasil .= "</div>";
		return $hasil;
	}
    
    public function get_name($jenis_value, $kode_value){
        $kode = $this->get_kode('', "jenis.jenis_value = '$jenis_value' AND kode.kode_value = '$kode_value'");
        if ($kode){
            return $kode->kode_nama;
        }
        return '';
    }
    
    // konfigurasi tabel ref_perguruan_tinggi
	public function insert_perguruan_tinggi($data){
        $this->db->insert("ref_perguruan_tinggi",$data);
    }
    
    public function update_perguruan_tinggi($where,$data){
        $this->db->update("ref_perguruan_tinggi",$data,$where);
    }

    public function delete_perguruan_tinggi($where){
        $this->db->delete("ref_perguruan_tinggi", $where);
    }

	public function get_perguruan_tinggi($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("ref_perguruan_tinggi perguruan_tinggi");
		if ($where){$this->db->where($where);}
		$this->db->order_by('perguruan_tinggi_id','ASC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_perguruan_tinggi($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("ref_perguruan_tinggi perguruan_tinggi");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_perguruan_tinggi($where="", $like=""){
        $this->db->select("*");
        $this->db->from("ref_perguruan_tinggi perguruan_tinggi");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
    
    // konfigurasi tabel ref_program_studi
	public function insert_program_studi($data){
        $this->db->insert("ref_program_studi",$data);
    }
    
    public function update_program_studi($where,$data){
        $this->db->update("ref_program_studi",$data,$where);
    }

    public function delete_program_studi($where){
        $this->db->delete("ref_program_studi", $where);
    }

	public function get_program_studi($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("ref_program_studi program_studi");
        $this->db->join("ref_perguruan_tinggi perguruan_tinggi", "program_studi.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('program_studi_id','ASC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_program_studi($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("ref_program_studi program_studi");
        $this->db->join("ref_perguruan_tinggi perguruan_tinggi", "program_studi.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_program_studi($where="", $like=""){
        $this->db->select("*");
        $this->db->from("ref_program_studi program_studi");
        $this->db->join("ref_perguruan_tinggi perguruan_tinggi", "program_studi.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file referensi_model.php */
/* Location: ./application/models/referensi_model.php */