<?php
class Modul_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// tabel modul 
	public function insert_modul($data){
        $this->db->insert("modul",$data);
    }
    
    public function update_modul($where,$data){
        $this->db->update("modul",$data,$where);
    }

    public function delete_modul($where){
        $this->db->delete("modul", $where);
    }

	public function get_modul($select="*", $where=""){
        $data = array();
		$this->db->select($select);
        $this->db->from("modul");
		if ($where){$this->db->where($where);}
		$this->db->order_by('modul_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_modul($select="*", $sidx="", $sord="", $limit="", $start="", $where="", $like=""){
        $data = array();
        $this->db->select($select);
        $this->db->from("modul");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        if ($sidx && $sord){$this->db->order_by($sidx,$sord);}
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_modul($where="", $like=""){
        $this->db->select("*");
        $this->db->from("modul");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file modul.php */
/* Location: ./application/models/modul.php */