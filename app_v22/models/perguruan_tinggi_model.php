<?php
class Perguruan_tinggi_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel akd_perguruan_tinggi
	public function insert_perguruan_tinggi($data){
        return $this->db->insert("akd_perguruan_tinggi",$data);
    }
    
    public function update_perguruan_tinggi($where,$data){
        return $this->db->update("akd_perguruan_tinggi",$data,$where);
    }

    public function delete_perguruan_tinggi($where){
        return $this->db->delete("akd_perguruan_tinggi", $where);
    }

	public function get_perguruan_tinggi($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_perguruan_tinggi perguruan_tinggi");
		$this->db->join("akd_yayasan yayasan", "perguruan_tinggi.yayasan_id=yayasan.yayasan_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('perguruan_tinggi_kode','ASC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
    }
    
    public function get_perguruan_tinggi_aktif(){
        $data = "";
		$this->db->select("*");
        $this->db->from("akd_perguruan_tinggi perguruan_tinggi");
		$this->db->where(array("perguruan_tinggi_aktif"=>"Y"));
		$this->db->order_by('perguruan_tinggi_kode','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_perguruan_tinggi($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_perguruan_tinggi perguruan_tinggi");
		$this->db->join("akd_yayasan yayasan", "perguruan_tinggi.yayasan_id=yayasan.yayasan_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_perguruan_tinggi($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_perguruan_tinggi perguruan_tinggi");
		$this->db->join("akd_yayasan yayasan", "perguruan_tinggi.yayasan_id=yayasan.yayasan_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file perguruan_tinggi_model.php */
/* Location: ./application/models/perguruan_tinggi_model.php */