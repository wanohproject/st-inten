<?php
class Mahasiswa_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel mahasiswa
	public function insert_mahasiswa($data){
        return $this->db->insert("akd_mahasiswa",$data);
    }
    
    public function update_mahasiswa($where,$data){
        return $this->db->update("akd_mahasiswa",$data,$where);
    }

    public function delete_mahasiswa($where){
        return $this->db->delete("akd_mahasiswa", $where);
    }

	public function get_mahasiswa($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_mahasiswa mahasiswa");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "mahasiswa.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "mahasiswa.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "mahasiswa.jenjang_kode=jenjang.kode_value", "left");
		$this->db->join("akd_tahun tahun", "mahasiswa.mahasiswa_tahun_masuk=tahun.tahun_kode", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('mahasiswa_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_mahasiswa($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_mahasiswa mahasiswa");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "mahasiswa.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "mahasiswa.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "mahasiswa.jenjang_kode=jenjang.kode_value", "left");
		$this->db->join("akd_tahun tahun", "mahasiswa.mahasiswa_tahun_masuk=tahun.tahun_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_mahasiswa($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_mahasiswa mahasiswa");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "mahasiswa.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "mahasiswa.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "mahasiswa.jenjang_kode=jenjang.kode_value", "left");
		$this->db->join("akd_tahun tahun", "mahasiswa.mahasiswa_tahun_masuk=tahun.tahun_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }

    // konfigurasi tabel mahasiswa_nilai
	public function insert_mahasiswa_nilai($data){
        return $this->db->insert("akd_mahasiswa_nilai",$data);
    }
    
    public function update_mahasiswa_nilai($where,$data){
        return $this->db->update("akd_mahasiswa_nilai",$data,$where);
    }

    public function delete_mahasiswa_nilai($where){
        return $this->db->delete("akd_mahasiswa_nilai", $where);
    }

	public function get_mahasiswa_nilai($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_mahasiswa_nilai mahasiswa_nilai");
        $this->db->join("akd_mahasiswa mahasiswa", "mahasiswa_nilai.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
        $this->db->join("akd_matakuliah matakuliah", "mahasiswa_nilai.matakuliah_id=matakuliah.matakuliah_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('mahasiswa_nilai_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_mahasiswa_nilai($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_mahasiswa_nilai mahasiswa_nilai");
        $this->db->join("akd_mahasiswa mahasiswa", "mahasiswa_nilai.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
        $this->db->join("akd_matakuliah matakuliah", "mahasiswa_nilai.matakuliah_id=matakuliah.matakuliah_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_mahasiswa_nilai($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_mahasiswa_nilai mahasiswa_nilai");
        $this->db->join("akd_mahasiswa mahasiswa", "mahasiswa_nilai.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
        $this->db->join("akd_matakuliah matakuliah", "mahasiswa_nilai.matakuliah_id=matakuliah.matakuliah_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
    
    // konfigurasi tabel mahasiswa_kuliah
	public function insert_mahasiswa_kuliah($data){
        return $this->db->insert("akd_mahasiswa_kuliah",$data);
    }
    
    public function update_mahasiswa_kuliah($where,$data){
        return $this->db->update("akd_mahasiswa_kuliah",$data,$where);
    }

    public function delete_mahasiswa_kuliah($where){
        return $this->db->delete("akd_mahasiswa_kuliah", $where);
    }

	public function get_mahasiswa_kuliah($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_mahasiswa_kuliah mahasiswa_kuliah");
        $this->db->join("akd_mahasiswa mahasiswa", "mahasiswa_kuliah.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
        $this->db->join("akd_program_studi program_studi", "mahasiswa_kuliah.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("akd_semester semester", "mahasiswa_kuliah.semester_kode=semester.semester_kode", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('mahasiswa_kuliah_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_mahasiswa_kuliah($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_mahasiswa_kuliah mahasiswa_kuliah");
        $this->db->join("akd_mahasiswa mahasiswa", "mahasiswa_kuliah.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
        $this->db->join("akd_program_studi program_studi", "mahasiswa_kuliah.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("akd_semester semester", "mahasiswa_kuliah.semester_kode=semester.semester_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_mahasiswa_kuliah($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_mahasiswa_kuliah mahasiswa_kuliah");
        $this->db->join("akd_mahasiswa mahasiswa", "mahasiswa_kuliah.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
        $this->db->join("akd_program_studi program_studi", "mahasiswa_kuliah.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("akd_semester semester", "mahasiswa_kuliah.semester_kode=semester.semester_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file mahasiswa_model.php */
/* Location: ./application/models/mahasiswa_model.php */