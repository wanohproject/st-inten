<?php
class Dosen_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel dosen
	public function insert_dosen($data){
        return $this->db->insert("akd_dosen",$data);
    }
    
    public function update_dosen($where,$data){
        return $this->db->update("akd_dosen",$data,$where);
    }

    public function delete_dosen($where){
        return $this->db->delete("akd_dosen", $where);
    }

	public function get_dosen($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_dosen dosen");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "dosen.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "dosen.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "dosen.jenjang_kode=jenjang.kode_value", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('dosen_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_dosen($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_dosen dosen");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "dosen.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "dosen.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "dosen.jenjang_kode=jenjang.kode_value", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_dosen($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_dosen dosen");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "dosen.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "dosen.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "dosen.jenjang_kode=jenjang.kode_value", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }

    // konfigurasi tabel dosen_mengajar
	public function insert_dosen_mengajar($data){
        return $this->db->insert("akd_dosen_mengajar",$data);
    }
    
    public function update_dosen_mengajar($where,$data){
        return $this->db->update("akd_dosen_mengajar",$data,$where);
    }

    public function delete_dosen_mengajar($where){
        return $this->db->delete("akd_dosen_mengajar", $where);
    }

	public function get_dosen_mengajar($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_dosen_mengajar dosen_mengajar");
        $this->db->join("akd_dosen dosen", "dosen_mengajar.dosen_id=dosen.dosen_id", "left");
        $this->db->join("akd_program_studi program_studi", "dosen_mengajar.program_studi_id=program_studi.program_studi_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('dosen_mengajar_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_dosen_mengajar($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_dosen_mengajar dosen_mengajar");
        $this->db->join("akd_dosen dosen", "dosen_mengajar.dosen_id=dosen.dosen_id", "left");
        $this->db->join("akd_program_studi program_studi", "dosen_mengajar.program_studi_id=program_studi.program_studi_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_dosen_mengajar($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_dosen_mengajar dosen_mengajar");
        $this->db->join("akd_dosen dosen", "dosen_mengajar.dosen_id=dosen.dosen_id", "left");
        $this->db->join("akd_program_studi program_studi", "dosen_mengajar.program_studi_id=program_studi.program_studi_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }

    public function combobox_dosen_mengajar($name, $pilihan, $js='', $label='', $script='', $semester_kode=""){
        $hasil = "";
        $hasil .= "<select name='$name' id='$name' onchange='$js' $script>";
        if ($label != "none"){
            $hasil .= "<option value=''>".$label."</option>";
        }
		
        $grid_program_studi = $this->db->query("SELECT akd_dosen_mengajar.program_studi_id, program_studi_nama FROM akd_dosen_mengajar LEFT JOIN akd_program_studi ON akd_dosen_mengajar.program_studi_id=akd_program_studi.program_studi_id WHERE akd_dosen_mengajar.semester_kode = '$semester_kode' GROUP BY akd_dosen_mengajar.program_studi_id ORDER BY program_studi_nama ASC")->result();
        if ($grid_program_studi){
            foreach ($grid_program_studi as $row_program_studi) {
                $hasil .= "<optgroup label=\"$row_program_studi->program_studi_nama\">";
                $grid_matakuliah = $this->db->query("SELECT akd_dosen_mengajar.dosen_mengajar_id, matakuliah_kode, matakuliah_nama, dosen_nama FROM akd_dosen_mengajar LEFT JOIN akd_matakuliah ON akd_dosen_mengajar.matakuliah_id=akd_matakuliah.matakuliah_id LEFT JOIN akd_dosen ON akd_dosen_mengajar.dosen_id=akd_dosen.dosen_id WHERE akd_dosen_mengajar.program_studi_id = '$row_program_studi->program_studi_id' AND akd_dosen_mengajar.semester_kode = '$semester_kode' ORDER BY matakuliah_kode, dosen_nama ASC")->result();
                if ($grid_matakuliah){
                    foreach ($grid_matakuliah as $row_matakuliah) {
                        if ($pilihan == $row_matakuliah->dosen_mengajar_id){
                            $hasil .= "<option value='".$row_matakuliah->dosen_mengajar_id."' selected>".$row_matakuliah->matakuliah_kode." - ".$row_matakuliah->matakuliah_nama." (".$row_matakuliah->dosen_nama.")</option>";
                        } else {
                            $hasil .= "<option value='".$row_matakuliah->dosen_mengajar_id."'>".$row_matakuliah->matakuliah_kode." - ".$row_matakuliah->matakuliah_nama." (".$row_matakuliah->dosen_nama.")</option>";
                        }
                    }
                }
                $hasil .= "</optgroup>";
            }
        }
		
		$hasil .= "</select>";
		return $hasil;
	}
}

/* End of file dosen_model.php */
/* Location: ./application/models/dosen_model.php */