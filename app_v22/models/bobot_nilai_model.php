<?php
class Bobot_nilai_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel bobot_nilai
	public function insert_bobot_nilai($data){
        return $this->db->insert("akd_bobot_nilai",$data);
    }
    
    public function update_bobot_nilai($where,$data){
        return $this->db->update("akd_bobot_nilai",$data,$where);
    }

    public function delete_bobot_nilai($where){
        return $this->db->delete("akd_bobot_nilai", $where);
    }

	public function get_bobot_nilai($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_bobot_nilai bobot_nilai");
        $this->db->join("akd_program_studi program_studi", "bobot_nilai.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("akd_semester semester", "bobot_nilai.semester_kode=semester.semester_kode", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('bobot_nilai.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}
    
    public function grid_all_bobot_nilai($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_bobot_nilai bobot_nilai");
        $this->db->join("akd_program_studi program_studi", "bobot_nilai.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("akd_semester semester", "bobot_nilai.semester_kode=semester.semester_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_bobot_nilai($where="", $like=""){
        $this->db->select("bobot_nilai_id");
        $this->db->from("akd_bobot_nilai bobot_nilai");
        $this->db->join("akd_program_studi program_studi", "bobot_nilai.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("akd_semester semester", "bobot_nilai.semester_kode=semester.semester_kode", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file bobot_nilai_model.php */
/* Location: ./application/models/bobot_nilai_model.php */