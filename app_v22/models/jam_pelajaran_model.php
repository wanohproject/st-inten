<?php
class Jam_pelajaran_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel jam_pelajaran
	public function insert_jam_pelajaran($data){
        return $this->db->insert("akd_jam_pelajaran",$data);
    }
    
    public function update_jam_pelajaran($where,$data){
        return $this->db->update("akd_jam_pelajaran",$data,$where);
    }

    public function delete_jam_pelajaran($where){
        return $this->db->delete("akd_jam_pelajaran", $where);
    }

	public function get_jam_pelajaran($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_jam_pelajaran jam_pelajaran");
		if ($where){$this->db->where($where);}
		$this->db->order_by('jam_pelajaran_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_jam_pelajaran($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_jam_pelajaran jam_pelajaran");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_jam_pelajaran($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_jam_pelajaran jam_pelajaran");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file jam_pelajaran.php */
/* Location: ./application/models/jam_pelajaran.php */