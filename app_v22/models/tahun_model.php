<?php
class Tahun_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel tahun
	public function insert_tahun($data){
        $this->db->insert("akd_tahun",$data);
    }
    
    public function update_tahun($where,$data){
        $this->db->update("akd_tahun",$data,$where);
    }

    public function delete_tahun($where){
        $this->db->delete("akd_tahun", $where);
    }

	public function get_tahun($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_tahun tahun");
		if ($where){$this->db->where($where);}
		$this->db->order_by('tahun_kode','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}
	
	public function get_tahun_aktif(){
        $data = "";
		$this->db->select("*");
        $this->db->from("akd_tahun tahun");
		$this->db->where(array("tahun_aktif"=>"Y"));
		$this->db->order_by('tahun_kode','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_tahun($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_tahun tahun");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_tahun($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_tahun tahun");		
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file tahun.php */
/* Location: ./application/models/tahun.php */