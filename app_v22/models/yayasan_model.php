<?php
class Yayasan_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel akd_yayasan
	public function insert_yayasan($data){
        return $this->db->insert("akd_yayasan",$data);
    }
    
    public function update_yayasan($where,$data){
        return $this->db->update("akd_yayasan",$data,$where);
    }

    public function delete_yayasan($where){
        return $this->db->delete("akd_yayasan", $where);
    }

	public function get_yayasan($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_yayasan yayasan");
		if ($where){$this->db->where($where);}
		$this->db->order_by('yayasan_kode','ASC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_yayasan($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_yayasan yayasan");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_yayasan($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_yayasan yayasan");		
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file yayasan_model.php */
/* Location: ./application/models/yayasan_model.php */