<?php
class Perwalian_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel perwalian
	public function insert_perwalian($data){
        return $this->db->insert("akd_perwalian",$data);
    }
    
    public function update_perwalian($where,$data){
        return $this->db->update("akd_perwalian",$data,$where);
    }

    public function delete_perwalian($where){
        return $this->db->delete("akd_perwalian", $where);
    }

	public function get_perwalian($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_perwalian perwalian");
        $this->db->join("akd_program_studi program_studi", "perwalian.program_studi_id=program_studi.program_studi_id", "left");
		$this->db->join("akd_semester semester", "perwalian.semester_kode=semester.semester_kode", "left");
		$this->db->join("akd_mahasiswa mahasiswa", "perwalian.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
		$this->db->join("akd_dosen dosen", "perwalian.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('perwalian.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_perwalian($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_perwalian perwalian");
        $this->db->join("akd_program_studi program_studi", "perwalian.program_studi_id=program_studi.program_studi_id", "left");
		$this->db->join("akd_semester semester", "perwalian.semester_kode=semester.semester_kode", "left");
		$this->db->join("akd_mahasiswa mahasiswa", "perwalian.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
		$this->db->join("akd_dosen dosen", "perwalian.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_perwalian($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_perwalian perwalian");
        $this->db->join("akd_program_studi program_studi", "perwalian.program_studi_id=program_studi.program_studi_id", "left");
		$this->db->join("akd_semester semester", "perwalian.semester_kode=semester.semester_kode", "left");
		$this->db->join("akd_mahasiswa mahasiswa", "perwalian.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
		$this->db->join("akd_dosen dosen", "perwalian.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }

    // konfigurasi tabel perwalian_matakuliah
	public function insert_perwalian_matakuliah($data){
        return $this->db->insert("akd_perwalian_matakuliah",$data);
    }
    
    public function update_perwalian_matakuliah($where,$data){
        return $this->db->update("akd_perwalian_matakuliah",$data,$where);
    }

    public function delete_perwalian_matakuliah($where){
        return $this->db->delete("akd_perwalian_matakuliah", $where);
    }

	public function get_perwalian_matakuliah($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_perwalian_matakuliah perwalian_matakuliah");
        $this->db->join("akd_perwalian perwalian", "perwalian_matakuliah.perwalian_id=perwalian.perwalian_id", "left");
        $this->db->join("akd_matakuliah matakuliah", "perwalian_matakuliah.matakuliah_id=matakuliah.matakuliah_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('perwalian_matakuliah.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_perwalian_matakuliah($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_perwalian_matakuliah perwalian_matakuliah");
        $this->db->join("akd_perwalian perwalian", "perwalian_matakuliah.perwalian_id=perwalian.perwalian_id", "left");
        $this->db->join("akd_matakuliah matakuliah", "perwalian_matakuliah.matakuliah_id=matakuliah.matakuliah_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_perwalian_matakuliah($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_perwalian_matakuliah perwalian_matakuliah");
        $this->db->join("akd_perwalian perwalian", "perwalian_matakuliah.perwalian_id=perwalian.perwalian_id", "left");
        $this->db->join("akd_matakuliah matakuliah", "perwalian_matakuliah.matakuliah_id=matakuliah.matakuliah_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
    
    // konfigurasi tabel perwalian_catatan
	public function insert_perwalian_catatan($data){
        return $this->db->insert("akd_perwalian_catatan",$data);
    }
    
    public function update_perwalian_catatan($where,$data){
        return $this->db->update("akd_perwalian_catatan",$data,$where);
    }

    public function delete_perwalian_catatan($where){
        return $this->db->delete("akd_perwalian_catatan", $where);
    }

	public function get_perwalian_catatan($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_perwalian_catatan perwalian_catatan");
		$this->db->join("akd_mahasiswa mahasiswa", "perwalian_catatan.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
		$this->db->join("akd_dosen dosen", "perwalian_catatan.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('perwalian_catatan.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_perwalian_catatan($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_perwalian_catatan perwalian_catatan");
		$this->db->join("akd_mahasiswa mahasiswa", "perwalian_catatan.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
		$this->db->join("akd_dosen dosen", "perwalian_catatan.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_perwalian_catatan($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_perwalian_catatan perwalian_catatan");
		$this->db->join("akd_mahasiswa mahasiswa", "perwalian_catatan.mahasiswa_id=mahasiswa.mahasiswa_id", "left");
		$this->db->join("akd_dosen dosen", "perwalian_catatan.dosen_id=dosen.dosen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file perwalian_model.php */
/* Location: ./application/models/perwalian_model.php */