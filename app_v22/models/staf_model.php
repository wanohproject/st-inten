<?php
class Staf_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel staf
	public function insert_staf($data){
        $this->db->insert("staf",$data);
    }
    
    public function update_staf($where,$data){
        $this->db->update("staf",$data,$where);
    }

    public function delete_staf($where){
        $this->db->delete("staf", $where);
    }

	public function get_staf($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("staf");
        $this->db->join("departemen", "staf.departemen_id=departemen.departemen_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('staf_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_staf($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("staf");
        $this->db->join("departemen", "staf.departemen_id=departemen.departemen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
	}
	
    public function grid_active_staf($date="", $select="", $sidx="", $sord="", $limit="", $start="", $where="", $like=""){
		if (!$date) {
			$date = date("Y-m-d");
		}
        $data = "";
        $this->db->select($select);
		$this->db->from("staf");
		$this->db->where("(staf_tmt = '0000-00-00' OR staf_tmt <= '$date' OR staf_tmt IS NULL) AND (staf_tst = '0000-00-00' OR staf_tst >= '$date' OR staf_tst IS NULL)");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
		if ($sidx && $sord){
			$this->db->order_by($sidx,$sord);
		} else {
			$this->db->order_by("staf_nama","ASC");
		}
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
	}
	
	public function grid_staf_departemen($departemen_id="", $date="", $select="", $sidx="", $sord="", $limit="", $start="", $where="", $like=""){
		$where_departemen = " AND staf_status = 'Guru'";
		if ($departemen_id){
			$departemen_list = $this->recursive_departemen_child($departemen_id);
			$departemen_list_ = array();
			foreach ($departemen_list as $key => $value) {
				$departemen_list_[] = "	staf.departemen_id = '$value' ";
			}
			$departemen_list_ = implode(" OR ", $departemen_list_);
			$where_departemen .= " AND ($departemen_list_)";
		}

		if (!$date) {
			$date = date("Y-m-d");
		}
        $data = "";
        $this->db->select($select);
		$this->db->from("staf");
		$this->db->where("(staf_tmt = '0000-00-00' OR staf_tmt <= '$date' OR staf_tmt IS NULL) AND (staf_tst = '0000-00-00' OR staf_tst >= '$date' OR staf_tst IS NULL) $where_departemen");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
		if ($sidx && $sord){
			$this->db->order_by($sidx,$sord);
		} else {
			$this->db->order_by("staf_nama","ASC");
		}
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_staf($where="", $like=""){
        $this->db->select("*");
        $this->db->from("staf");
        $this->db->join("departemen", "staf.departemen_id=departemen.departemen_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
	
	public function combobox_guru($name, $pilihan, $js='', $label='', $script='', $additional=true, $tahun_kode='', $staf_id=''){
		echo "<select name='$name' id='$name' onchange='$js' $script>";
		if ($label != "none"){
			echo "<option value=''>".$label."</option>";
		}
		
		if ($staf_id){
			$grid_staf = $this->db->query("SELECT staf.staf_id, staf_nama 
											FROM guru_pelajaran 
												LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
											WHERE guru_pelajaran.tahun_kode = '{$tahun_kode}'
												AND staf.staf_id = '{$staf_id}'
											GROUP BY staf.staf_id 
											ORDER BY staf_nama ASC")->result();
		} else {
			$grid_staf = $this->db->query("SELECT staf.staf_id, staf_nama 
											FROM guru_pelajaran 
												LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
											WHERE guru_pelajaran.tahun_kode = '{$tahun_kode}'
											GROUP BY staf.staf_id 
											ORDER BY staf_nama ASC")->result();
		}
		foreach($grid_staf as $row_staf){
			if ($pilihan == $row_staf->staf_id){
				echo "<option value='".$row_staf->staf_id."' selected>".$row_staf->staf_nama."</option>";
			} else {
				echo "<option value='".$row_staf->staf_id."'>".$row_staf->staf_nama."</option>";
			}
		}
		echo "</select>";
	}

	public function combobox_guru_pelajaran($name, $pilihan, $js='', $label='', $script='', $additional=true, $tahun_kode='', $staf_id='', $pelajaran_id=''){
		echo "<select name='$name' id='$name' onchange='$js' $script>";
		if ($label != "none"){
			echo "<option value=''>".$label."</option>";
		}
		
		if ($pelajaran_id){
			if ($staf_id){
				$grid_staf = $this->db->query("SELECT staf.staf_id, staf_nama 
												FROM guru_pelajaran 
													LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
												WHERE guru_pelajaran.tahun_kode = '{$tahun_kode}'
													AND staf.staf_id = '{$staf_id}'
													AND guru_pelajaran.pelajaran_id = '{$pelajaran_id}'
												GROUP BY staf.staf_id 
												ORDER BY staf_nama ASC")->result();
			} else {
				$grid_staf = $this->db->query("SELECT staf.staf_id, staf_nama 
												FROM guru_pelajaran 
													LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
												WHERE guru_pelajaran.tahun_kode = '{$tahun_kode}'
													AND guru_pelajaran.pelajaran_id = '{$pelajaran_id}'
												GROUP BY staf.staf_id 
												ORDER BY staf_nama ASC")->result();
			}
		} else {
			if ($staf_id){
				$grid_staf = $this->db->query("SELECT staf.staf_id, staf_nama 
												FROM guru_pelajaran 
													LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
												WHERE guru_pelajaran.tahun_kode = '{$tahun_kode}'
													AND staf.staf_id = '{$staf_id}'
												GROUP BY staf.staf_id 
												ORDER BY staf_nama ASC")->result();
			} else {
				$grid_staf = $this->db->query("SELECT staf.staf_id, staf_nama 
												FROM guru_pelajaran 
													LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
												WHERE guru_pelajaran.tahun_kode = '{$tahun_kode}'
												GROUP BY staf.staf_id 
												ORDER BY staf_nama ASC")->result();
			}
		}
		foreach($grid_staf as $row_staf){
			if ($pilihan == $row_staf->staf_id){
				echo "<option value='".$row_staf->staf_id."' selected>".$row_staf->staf_nama."</option>";
			} else {
				echo "<option value='".$row_staf->staf_id."'>".$row_staf->staf_nama."</option>";
			}
		}
		echo "</select>";
	}

	public function recursive_departemen_child($departemen_id="", $hasil = array(), $loop = 0){
		
		if ($loop == 0){
			$hasil = explode(',', $departemen_id);
		}
		
		$jumlah_departemen = $this->count_all_departemen("departemen_utama IN ($departemen_id)");
		$departemen = $this->grid_all_departemen('', 'departemen_id', 'ASC', '', '', "departemen_utama IN ($departemen_id)");
		if ($jumlah_departemen > 0){
			foreach($departemen as $row){
				$hasil[] = $row->departemen_id;
				$loop++;
				$hasil = $this->recursive_departemen_child($row->departemen_id, $hasil, $loop);
			}
		}
		return $hasil;
	}

	public function recursive_departemen_parent($departemen_id="", $hasil = array(), $loop = 0){
		
		$departemen = $this->get_departemen('', array('departemen_id'=>$departemen_id));
		if ($departemen){
			$hasil[] = $departemen->departemen_id;
			$loop++;
			$hasil = $this->recursive_departemen_parent($departemen->departemen_utama, $hasil, $loop);
		}
		return $hasil;
	}
	
	public function get_departemen_parent($departemen_id="", $hasil = "", $loop = 0, $found = false){
		
		$departemen = $this->get_departemen('', array('departemen_id'=>$departemen_id));
		if ($departemen){
			if ($departemen->departemen_utama == 0){
				$hasil = $departemen->departemen_id;
				$found = true;
			}
			if ($found == false){
				$loop++;
				$this->get_departemen_parent($departemen->departemen_utama, $hasil, $loop, $found);
			}
		}
		return $hasil;
	}

	public function grid_all_departemen($select="*", $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("departemen");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_departemen($where="", $like=""){
        $this->db->select("*");
        $this->db->from("departemen");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file staf.php */
/* Location: ./application/models/staf.php */