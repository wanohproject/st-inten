<?php
class Menu_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// tabel menu 
	public function insert_menu($data){
        return $this->db->insert("menu",$data);
    }
    
    public function update_menu($where,$data){
        return $this->db->update("menu",$data,$where);
    }

    public function delete_menu($where){
        return $this->db->delete("menu", $where);
    }

	public function get_menu($select="*", $where=""){
        $data = array();
		$this->db->select($select);
        $this->db->from("menu");
		$this->db->join("modul", "menu.modul_id=modul.modul_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('menu_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_menu($select="*", $sidx="", $sord="", $limit="", $start="", $where="", $like=""){
        $data = array();
        $this->db->select($select);
        $this->db->from("menu");
		$this->db->join("modul", "menu.modul_id=modul.modul_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        if ($sidx && $sord){$this->db->order_by($sidx,$sord);}
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_menu($where="", $like=""){
        $this->db->select("*");
        $this->db->from("menu");
		$this->db->join("modul", "menu.modul_id=modul.modul_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
	
	// konfigurasi tabel menu pengguna
	public function insert_menu_pengguna($data){
        $this->db->insert("menu_pengguna",$data);
    }
    
    public function update_menu_pengguna($where,$data){
        $this->db->update("menu_pengguna",$data,$where);
    }

    public function delete_menu_pengguna($where){
        $this->db->delete("menu_pengguna", $where);
    }

	public function get_menu_pengguna($select="*", $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("menu_pengguna");
		$this->db->join('menu', 'menu_pengguna.menu_id= menu.menu_id', 'left');
		$this->db->join('pengguna_level', 'menu_pengguna.pengguna_level_id= pengguna_level.pengguna_level_id', 'left');
		if ($where){$this->db->where($where);}
		$this->db->order_by('menu_pengguna_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_menu_pengguna($select="*", $sidx="", $sord="", $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("menu_pengguna");
		$this->db->join('menu', 'menu_pengguna.menu_id= menu.menu_id', 'left');
		$this->db->join('pengguna_level', 'menu_pengguna.pengguna_level_id= pengguna_level.pengguna_level_id', 'left');
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        if ($sidx && $sord){$this->db->order_by($sidx,$sord);}
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_menu_pengguna($where="", $like=""){
        $this->db->select("menu_pengguna.*");
        $this->db->from("menu_pengguna");
		$this->db->join('menu', 'menu_pengguna.menu_id= menu.menu_id', 'left');
		$this->db->join('pengguna_level', 'menu_pengguna.pengguna_level_id= pengguna_level.pengguna_level_id', 'left');
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
	
	public function recursive_menu_child($menu_id="", $hasil = array(), $loop = 0){
		
		if ($loop == 0){
			$hasil = explode(',', $menu_id);
		}
		
		$jumlah_menu = $this->count_all_menu("menu_utama IN ($menu_id)");
		$menu = $this->grid_all_menu('', '', '', '', '', "menu_utama IN ($menu_id)");
		if ($jumlah_menu > 0){
			foreach($menu as $row){
				$hasil[] = $row->menu_id;
				$loop++;
				$hasil = $this->recursive_menu_child($row->menu_id, $hasil, $loop);
			}
		}
		return $hasil;
	}
	
	public function recursive_menu_parent($menu_id="", $hasil = array(), $loop = 0){
		
		$menu = $this->get_menu('', array('menu_id'=>$menu_id));
		if ($menu){
			$hasil[] = $menu->menu_id;
			$loop++;
			$hasil = $this->recursive_menu_parent($menu->menu_utama, $hasil, $loop);
		}
		return $hasil;
	}
	
	public function get_parent($menu_id="", $hasil = "", $loop = 0){
		
		$menu = $this->get_menu('', array('menu_id'=>$menu_id));
		if ($menu){
			$hasil = $menu->menu_id;
			$loop++;
			$hasil = $this->get_parent($menu->menu_utama, $hasil, $loop);
		}
		return $hasil;
	}
	
	public function combobox_menu($modul_id="", $menu_id="", $hasil = "", $loop = 0, $name, $value, $name_value, $pilihan, $js='', $label='', $script=''){
		if ($loop == 0){
			$hasil .= "<select name='$name' id='$name' onchange='$js' $script>";
			if ($label != "none"){
				$hasil .= "<option value=''>".$label."</option>";
			}
		}
		
		if ($loop < 0){
			$jumlah_menu = $this->count_all_menu("menu_id IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			$menu = $this->grid_all_menu('', 'menu_urutan', 'ASC', '', '', "menu_id IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			if ($jumlah_menu > 0){
				$loop++;
				foreach($menu as $row){			
					if ($pilihan == $row->menu_id){
						$hasil .= "<option value='".$row->menu_id."' selected>".$row->menu_nama."</option>";
					} else {
						$hasil .= "<option value='".$row->menu_id."'>".$row->menu_nama."</option>";
					}
					
					$hasil = $this->combobox_menu($modul_id, $row->menu_id, $hasil, $loop, $name, $value, $name_value, $pilihan, $js='', $label='', $script='');
				}
				$loop--;
			} 
		} else {
			$jumlah_menu = $this->count_all_menu("menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			$menu = $this->grid_all_menu('', 'menu_urutan', 'ASC', '', '', "menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			if ($jumlah_menu > 0){
				$loop++;
				foreach($menu as $row){
					if ($loop == 1){
						$space = "";
					} else if ($loop == 2){
						$space = "&nbsp;&nbsp;&nbsp;";
					} else if ($loop == 3){
						$space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					} else if ($loop == 4){
						$space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}				
					if ($pilihan == $row->menu_id){
						$hasil .= "<option value='".$row->menu_id."' selected>".$space.$row->menu_nama."</option>";
					} else {
						$hasil .= "<option value='".$row->menu_id."'>".$space.$row->menu_nama."</option>";
					}
					
					$hasil = $this->combobox_menu($modul_id, $row->menu_id, $hasil, $loop, $name, $value, $name_value, $pilihan, $js='', $label='', $script='');
				}
				$loop--;
			}
		}
		
		if ($loop == 0){
			$hasil .= "</select>";
		}
		return $hasil;
	}
	
	public function combobox_menu2($modul_id="", $menu_id="", $hasil = "", $loop = 0, $name, $value, $name_value, $pilihan, $js='', $label='', $script=''){
		
		if ($loop == 0){
			$hasil .= "<select name='$name' id='$name' onchange='$js' $script>";
			if ($label != "none"){
				$hasil .= "<option value=''>".$label."</option>";
			}
		}
		
		if ($loop == 0){
			$jumlah_menu = $this->count_all_menu("menu_id IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			$menu = $this->grid_all_menu('', 'menu_urutan', 'ASC', '', '', "menu_id IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			if ($jumlah_menu > 0){
				$loop++;
				foreach($menu as $row){			
					if ($pilihan == $row->menu_id){
						$hasil .= "<option value='".$row->menu_id."' selected>".$row->menu_nama."</option>";
					} else {
						$hasil .= "<option value='".$row->menu_id."'>".$row->menu_nama."</option>";
					}
					
					$hasil = $this->combobox_menu($modul_id, $row->menu_id, $hasil, $loop, $name, $value, $name_value, $pilihan, $js='', $label='', $script='');
				}
				$loop--;
			} 
		} else {
			$jumlah_menu = $this->count_all_menu("menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			$menu = $this->grid_all_menu('', 'menu_urutan', 'ASC', '', '', "menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			if ($jumlah_menu > 0){
				$loop++;
				foreach($menu as $row){
					if ($loop == 0){
						$space = "";
					} else if ($loop == 1){
						$space = "&nbsp;&nbsp;&nbsp;";
					} else if ($loop == 2){
						$space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					} else if ($loop == 3){
						$space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					} else if ($loop == 4){
						$space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}
					
					if ($pilihan == $row->menu_id){
						$hasil .= "<option value='".$row->menu_id."' selected>".$space.$row->menu_nama."</option>";
					} else {
						$hasil .= "<option value='".$row->menu_id."'>".$space.$row->menu_nama."</option>";
					}
					
					$hasil = $this->combobox_menu($modul_id, $row->menu_id, $hasil, $loop, $name, $value, $name_value, $pilihan, $js='', $label='', $script='');
				}
				$loop--;
			}
		}
		
		if ($loop == 0){
			$hasil .= "</select>";
		}
		return $hasil;
	}

	public function table_menu($modul_id="", $menu_id="", $hasil = "", $loop = 0, $penggun_level_id=""){
		$where = ($penggun_level_id)?" WHERE pengguna_level_id = '$penggun_level_id' ":"";
		$jumlah_level = $this->db->query("SELECT pengguna_level_id FROM pengguna_level $where")->num_rows();
		$level = $this->db->query("SELECT * FROM pengguna_level $where ORDER BY pengguna_level_kode")->result();
		
		if ($loop == 0){
			$hasil .= "<table class=\"table table-bordered\">";
			$hasil .= "<tr>
							<th rowspan=\"2\">Menu</th>
							<th colspan=\"".($jumlah_level)."\">Kelompok Pengguna</th>
						</tr>";
			$hasil .= "<tr>";
				foreach ($level as $row) {
					$hasil .= "<th width=\"60\">$row->pengguna_level_kode</th>";
				}
			$hasil .= "</tr>";
		}

		
		if ($loop == 0){
			$jumlah_modul = $this->db->query("SELECT modul_id FROM modul WHERE modul_status = 'A'")->num_rows();
			$modul = $this->db->query("SELECT * FROM modul WHERE modul_status = 'A' ORDER BY modul_nama ASC")->result();
			if ($jumlah_modul > 0){
				foreach ($modul as $row_modul) {
					$hasil .= "<tr><th colspan=\"".($jumlah_level + 1)."\">$row_modul->modul_nama</th></tr>";
					$jumlah_menu = $this->count_all_menu("menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$row_modul->modul_id'");
					$menu = $this->grid_all_menu('', 'menu_urutan', 'ASC', '', '', "menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$row_modul->modul_id'");
					if ($jumlah_menu > 0){
						$loop++;
						foreach($menu as $row){
							$hasil .= "<tr><td style=\"padding-left:".(($loop + 1) * 20)."px\">$row->menu_nama</td>";
							foreach ($level as $row_level) {
								$ceklist = ($this->db->query("SELECT * FROM menu_pengguna WHERE menu_id = '$row->menu_id' AND pengguna_level_id = '$row_level->pengguna_level_id'")->num_rows() < 1) ? '' : 'checked';
								$hasil .= "<td><input type=\"checkbox\" name=\"menu[$row->menu_id][$row_level->pengguna_level_id]\" value=\"\" title=\"$row_level->pengguna_level_nama\" $ceklist></td>";
							}
							$hasil .= "</tr>";
							
							$hasil = $this->table_menu($row_modul->modul_id, $row->menu_id, $hasil, $loop, $penggun_level_id);
						}
						$loop--;
					} 
				}
			}
			
		} else {
			$jumlah_menu = $this->count_all_menu("menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			$menu = $this->grid_all_menu('', 'menu_urutan', 'ASC', '', '', "menu_utama IN ($menu_id) AND menu_level < 4 AND menu.modul_id = '$modul_id'");
			if ($jumlah_menu > 0){
				$loop++;
				foreach($menu as $row){
					$hasil .= "<tr><td style=\"padding-left:".(($loop + 1) * 20)."px\">$row->menu_nama</td>";
					foreach ($level as $row_level) {
						$ceklist = ($this->db->query("SELECT * FROM menu_pengguna WHERE menu_id = '$row->menu_id' AND pengguna_level_id = '$row_level->pengguna_level_id'")->num_rows() < 1) ? '' : 'checked';
						$hasil .= "<td><input type=\"checkbox\" name=\"menu[$row->menu_id][$row_level->pengguna_level_id]\" value=\"\" title=\"$row_level->pengguna_level_nama\" $ceklist></td>";
					}
					$hasil .= "</tr>";
					
					$hasil = $this->table_menu($modul_id, $row->menu_id, $hasil, $loop, $penggun_level_id);
				}
				$loop--;
			} 
		}
		
		if ($loop == 0){
			$hasil .= "</table>";
		}
		return $hasil;
	}
}

/* End of file menu.php */
/* Location: ./application/models/menu.php */