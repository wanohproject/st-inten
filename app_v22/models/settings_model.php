<?php
class Settings_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// tabel settings
    public function set_value($key, $value){
        $this->db->update("settings", array('setting_value'=>$value), array('setting_key'=>$key));
    }

	public function get_value($key){
        $data = "";
		$this->db->select("*");
        $this->db->from("settings");
		$this->db->where(array('setting_key'=>$key));
		$this->db->order_by('setting_id', 'DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}	
}

/* End of file settings_model.php */
/* Location: ./application/models/settings_model.php */