<?php
class Matakuliah_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel matakuliah
	public function insert_matakuliah($data){
        return $this->db->insert("akd_matakuliah",$data);
    }
    
    public function update_matakuliah($where,$data){
        return $this->db->update("akd_matakuliah",$data,$where);
    }

    public function delete_matakuliah($where){
        return $this->db->delete("akd_matakuliah", $where);
    }

	public function get_matakuliah($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_matakuliah matakuliah");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "matakuliah.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "matakuliah.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "matakuliah.jenjang_kode=jenjang.kode_value", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '28') matakuliah_jenis_kode", "matakuliah.matakuliah_jenis_kode=matakuliah_jenis_kode.kode_value", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('matakuliah_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}
    
	public function get_matakuliah2($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("akd_matakuliah matakuliah");
        $this->db->join("akd_program_studi program_studi", "matakuliah.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("akd_semester semester", "matakuliah.semester_kode=semester.semester_kode", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('matakuliah_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}
    
    public function grid_all_matakuliah($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("akd_matakuliah matakuliah");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "matakuliah.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "matakuliah.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "matakuliah.jenjang_kode=jenjang.kode_value", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '28') matakuliah_jenis_kode", "matakuliah.matakuliah_jenis_kode=matakuliah_jenis_kode.kode_value", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_matakuliah($where="", $like=""){
        $this->db->select("*");
        $this->db->from("akd_matakuliah matakuliah");
        $this->db->join("akd_perguruan_tinggi perguruan_tinggi", "matakuliah.perguruan_tinggi_id=perguruan_tinggi.perguruan_tinggi_id", "left");
        $this->db->join("akd_program_studi program_studi", "matakuliah.program_studi_id=program_studi.program_studi_id", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '4') jenjang", "matakuliah.jenjang_kode=jenjang.kode_value", "left");
        $this->db->join("(SELECT ref_kode.* FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '28') matakuliah_jenis_kode", "matakuliah.matakuliah_jenis_kode=matakuliah_jenis_kode.kode_value", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }

    public function combobox($name, $value, $name_value, $pilihan, $js='', $label='', $script='', $semester_kode="", $program_studi_id=""){
        $hasil = "";
        $hasil .= "<select name='$name' id='$name' onchange='$js' $script>";
        if ($label != "none"){
            $hasil .= "<option value=''>".$label."</option>";
        }
		
        $grid_semester = $this->db->query("SELECT * FROM akd_matakuliah WHERE program_studi_id = '$program_studi_id' AND semester_kode = '$semester_kode' GROUP BY matakuliah_semester_no ORDER BY matakuliah_semester_no ASC")->result();
        if ($grid_semester){
            foreach ($grid_semester as $row_semester) {
                $hasil .= "<optgroup label=\"Semester $row_semester->matakuliah_semester_no\">";
                $grid_matakuliah = $this->db->query("SELECT * FROM akd_matakuliah WHERE matakuliah_semester_no = '$row_semester->matakuliah_semester_no' AND program_studi_id = '$program_studi_id' AND semester_kode = '$semester_kode' ORDER BY matakuliah_nama ASC")->result();
                if ($grid_matakuliah){
                    foreach ($grid_matakuliah as $row_matakuliah) {
                        if ($pilihan == $row_matakuliah->matakuliah_id){
                            $hasil .= "<option value='".$row->matakuliah_id."' selected>".$row->matakuliah_nama."</option>";
                        } else {
                            $hasil .= "<option value='".$row->matakuliah_id."'>".$row->matakuliah_nama."</option>";
                        }
                    }
                }
                $hasil .= "</optgroup>";
            }
        }
		
		$hasil .= "</select>";
		return $hasil;
	}
}

/* End of file matakuliah_model.php */
/* Location: ./application/models/matakuliah_model.php */