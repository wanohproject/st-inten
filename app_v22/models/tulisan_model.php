<?php
class Tulisan_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// tabel tulisan
	public function insert_tulisan($data){
        return $this->db->insert("tulisan",$data);
    }
    
    public function update_tulisan($where,$data){
        return $this->db->update("tulisan",$data,$where);
    }

    public function delete_tulisan($where){
        return $this->db->delete("tulisan", $where);
    }

	public function get_tulisan($select="*", $where=""){
        $data = array();
		$this->db->select($select);
        $this->db->from("tulisan");
		$this->db->join('pengguna', 'tulisan.pengguna_id= pengguna.pengguna_id', 'left');
		if ($where){$this->db->where($where);}
		$this->db->order_by('tulisan_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_tulisan($select="*", $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = array();
        $this->db->select($select);
        $this->db->from("tulisan");
		$this->db->join('pengguna', 'tulisan.pengguna_id= pengguna.pengguna_id', 'left');
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }
	
	public function grid_all_tulisan_group($select="*", $from, $sidx, $sord, $limit="", $start="", $where="", $like="", $group_by=""){
        $data = array();
        $this->db->select($select);
        $this->db->from($from);
		$this->db->join('pengguna', 'tulisan.pengguna_id= pengguna.pengguna_id', 'left');
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        if ($group_by) {$this->db->group_by($group_by);}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_tulisan($where="", $like="", $group_by=""){
        $this->db->select("*");
        $this->db->from("tulisan");
		$this->db->join('pengguna', 'tulisan.pengguna_id= pengguna.pengguna_id', 'left');
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
		if ($group_by) {$this->db->group_by($group_by);}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
	
	public function post_hit($tulisan_id=""){
		if ($tulisan_id){
			return $this->db->query("UPDATE tulisan SET tulisan_hit = tulisan_hit + 1 WHERE tulisan_id = '$tulisan_id'");
		} else {
			return "";
		}
	}
}

/* End of file tulisan_model.php */
/* Location: ./application/models/tulisan_model.php */