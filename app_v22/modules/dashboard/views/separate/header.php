<!-- skin-green layout-boxed  -->
<body class="hold-transition <?php echo (profile('profil_warna'))?profile('profil_warna'):'skin-blue'; ?> <?php echo (profile('profil_boxed') == 'Y')?'layout-boxed':''; ?> layout-top-nav">
    <div class="wrapper">
	  <header class="main-header">        
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
		  <div class="navbar-header">
			  <a href="<?php echo site_url(); ?>" class="navbar-brand logo"><b><?php echo profile('profil_singkatan'); ?></b></a>
		  </div>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url('asset/image/default/user-default.png');?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo userdata('pengguna_nama_depan').' '.userdata('pengguna_nama_belakang'); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url('asset/image/default/user-default.png');?>" class="img-circle" alt="User Image">
                    <p>
						<?php echo userdata('pengguna_nama_depan').' '.userdata('pengguna_nama_belakang'); ?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo module_url('account/profile'); ?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo site_url('auth/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>