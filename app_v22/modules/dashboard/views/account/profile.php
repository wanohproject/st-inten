<?php if ($action == 'profile') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show');
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pengguna
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="active">Profil Pengguna</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Profil Pengguna</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
									<input type="hidden" class="form-control" name="pengguna_id" id="pengguna_id" value="<?php echo $pengguna_id; ?>" placeholder="">
									<div class="form-group row">
										<label for="pengguna_nama" class="col-sm-2 control-label">Username</label>
										<div class="col-md-4 col-sm-6">
											<?php echo $pengguna_nama; ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="pengguna_nama_depan" class="col-sm-2 control-label">Nama Lengkap</label>
										<div class="col-sm-8">
											<?php echo $pengguna_nama_depan.' '.$pengguna_nama_belakang; ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="pengguna_alamat" class="col-sm-2 control-label">Alamat</label>
										<div class="col-sm-6">
											<?php echo $pengguna_alamat; ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="pengguna_telepon" class="col-sm-2 control-label">Telepon</label>
										<div class="col-md-4 col-sm-6">
											<?php echo $pengguna_telepon; ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="pengguna_surel" class="col-sm-2 control-label">Email</label>
										<div class="col-md-4 col-sm-6">
											<?php echo $pengguna_surel; ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="pengguna_level_id" class="col-sm-2 control-label">Kelompok</label>
										<div class="col-md-4 col-sm-6">
											<?php echo $pengguna_level_nama; ?>
										</div>
									</div>
									<?php if (userdata('pengguna_level_id') == '11'){?>
									<div class="form-group row">
										<label for="mahasiswa_nama" class="col-sm-2 control-label">Mahasiswa</label>
										<div class="col-md-4 col-sm-6">
											<?php echo $mahasiswa_nama; ?>
										</div>
									</div>
									<?php } else if (userdata('pengguna_level_id') == '10'){ ?>
									<div class="form-group row">
										<label for="dosen_nama" class="col-sm-2 control-label">Dosen</label>
										<div class="col-md-4 col-sm-6">
											<?php echo $dosen_nama; ?>
										</div>
									</div>
									<?php } ?>
                </div><!-- /.box-body -->
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url(); ?>'" class="btn btn-primary">Ke Dashboard</button>
								</div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Ganti Password</h3>
                </div><!-- /.box-header -->
								<form action="<?php echo module_url('account/change-password'); ?>" method="post">
                <div class="box-body form-horizontal">
									<input type="hidden" class="form-control" name="pengguna_id" id="pengguna_id" value="<?php echo $pengguna_id; ?>" placeholder="">
									<div class="form-group">
										<label for="pengguna_nama" class="col-md-3 control-label">Password Sekarang</label>
										<div class="col-md-6">
											<input type="password" name="password_current" id="password_current" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label for="pengguna_nama" class="col-md-3 control-label">Password Baru</label>
										<div class="col-md-6">
											<input type="password" name="password_new" id="password_new" class="form-control" required>
										</div>
									</div>
									<div class="form-group">
										<label for="pengguna_nama" class="col-md-3 control-label">Ulangi Password Baru</label>
										<div class="col-md-6">
											<input type="password" name="password_renew" id="password_renew" class="form-control" required>
										</div>
									</div>
                </div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" name="changePassword" value="save" class="btn btn-primary">Simpan</button>
									<input type="reset" class="btn btn-default" value="Batalkan">
								</div><!-- /.box-footer -->
								</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>