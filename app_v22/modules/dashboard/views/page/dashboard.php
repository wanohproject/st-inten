<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/iCheck/flat/blue.css');?>">
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/morris/morris.css');?>">
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datepicker/datepicker3.css');?>">
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/daterangepicker/daterangepicker-bs3.css');?>">
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">

<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo theme_dir('admin_v2/plugins/raphael/raphael-min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/morris/morris.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/knob/jquery.knob.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/daterangepicker/daterangepicker.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
$(function () {
  console.log("LOAD DASHBOARD");
});
</script>
<style>
	.small-box {cursor:pointer;height:117px}
	.small-box h3 {font-size:28px;white-space:normal;}
	.small-box .icon {font-size:80px}
	.small-box .icon:hover {font-size:90px}
	.small-box>.small-box-footer {bottom:0px;width:100%;position:absolute}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo profile('profil_website'); ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-dashboard"></i>Beranda</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <h3 style="margin-top:0px;">DAFTAR APLIKASI</h3>
          <!-- Small boxes (Stat box) -->
          <div class="row"><?php 
          if($modul){
            foreach($modul as $row){
            ?><div class="col-sm-6 col-md-3" onclick="location.href='<?php echo site_url($row->modul_url); ?>'">
              <!-- small box -->
              <div class="small-box <?php echo ($row->modul_warna)?$row->modul_warna:"bg-blue"; ?>">
              <div class="inner">
                <h3><?php echo strtoupper($row->modul_nama); ?></h3>
              </div>
              <div class="icon">
                <i class="<?php echo $row->modul_icon; ?>"></i>
              </div>
              <a href="<?php echo site_url($row->modul_url); ?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col --><?php 
            }
          } ?>
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->