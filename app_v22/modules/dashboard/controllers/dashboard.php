<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();		
		$this->title			= 'Dashboard | ' . profile('profil_website');;
		$this->active_menu		= 0;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Modul_model');
    }

	public function index()
	{
		$head['title']		= $this->title;
		$data				= '';

		if ($this->session->userdata('level') == 11){
			redirect(site_url('mahasiswa'));
			exit();
		}
		
		if ($this->session->userdata('level') == 10){
			redirect(site_url('dosen'));
			exit();
		}
		
		// $data['modul']			= $this->Modul_model->grid_all_modul("", "modul_urutan", "ASC", 0, 0, array('modul_status'=>'A'));
		$data['modul']			= $this->db->query("SELECT * FROM modul WHERE modul_id IN (SELECT menu.modul_id FROM menu_pengguna LEFT JOIN menu ON menu_pengguna.menu_id=menu.menu_id WHERE menu_pengguna.pengguna_level_id='".userdata('pengguna_level_id')."' GROUP BY menu.modul_id)ORDER BY modul_urutan ASC")->result();
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/page/dashboard', $data);
		$this->load->view(module_dir().'/separate/footer');
		$this->load->view(module_dir().'/separate/foot');
	}
}
