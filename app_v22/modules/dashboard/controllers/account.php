<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Akun | ' . profile('profil_website');
		$this->active_menu		= 0;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Pengguna_model');
		$this->load->model('Loginnew_model');
	}
	
	public function index()
	{
		redirect(module_url('account/profile'), 'refresh');
	}
	
	public function profile()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'profile';
		
		$where['pengguna_id']	= userdata('pengguna_id');
		$pengguna		 		= $this->Pengguna_model->get_pengguna('*', $where);
		
		$data['pengguna_id']			= $pengguna->pengguna_id;
		$data['pengguna_nama']			= $pengguna->pengguna_nama;
		$data['pengguna_nama_depan']	= $pengguna->pengguna_nama_depan;
		$data['pengguna_nama_belakang']	= $pengguna->pengguna_nama_belakang;
		$data['pengguna_alamat']		= $pengguna->pengguna_alamat;
		$data['pengguna_surel']			= $pengguna->pengguna_surel;
		$data['pengguna_telepon']		= $pengguna->pengguna_telepon;
		$data['pengguna_level_nama']	= $pengguna->pengguna_level_nama;
		$data['dosen_nama']				= $pengguna->dosen_nama;
		$data['mahasiswa_nama']				= $pengguna->mahasiswa_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/account/profile', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function change_password()
	{
		$changePassword = $this->input->post('changePassword');
		if ($changePassword){
			$where['pengguna_id']		= validasi_sql($this->input->post('pengguna_id'));
			$where['pengguna_kunci']	= md5(validasi_sql($this->input->post('password_current')));
			$pengguna = $this->Loginnew_model->cek_byid($where);
			if ($pengguna){
				if ($this->input->post('password_new') == $this->input->post('password_renew')){

					$where_edit['pengguna_id']		= validasi_sql($this->input->post('pengguna_id'));
					$edit['pengguna_kunci']			= hash_password($this->input->post('password_new'));
					$this->Pengguna_model->update_pengguna($where_edit, $edit);

					if (userdata('pengguna_level_id') == '11'){
						$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_id'=>userdata('mahasiswa_id')), array('mahasiswa_pin'=>'CHANGED'));
					} else if (userdata('pengguna_level_id') == '10'){
						$this->Dosen_model->update_dosen(array('dosen_id'=>userdata('dosen_id')), array('dosen_pin'=>'CHANGED'));
					}

					$this->session->set_flashdata('success','Password telah berhasil diubah.');
					redirect(module_url($this->uri->segment(2).'/profile'));
				} else {
					$this->session->set_flashdata('error','Password Baru tidak sama.');
					redirect(module_url($this->uri->segment(2).'/profile'));
				}
			} else {
				$this->session->set_flashdata('error','Password yang Anda masukkan salah.');
				redirect(module_url($this->uri->segment(2).'/profile'));
			}
		} else {
			redirect(module_url($this->uri->segment(2)));
		}
	}
}