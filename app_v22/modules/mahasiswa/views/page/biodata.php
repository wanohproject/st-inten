<?php
if ($action == '' || $action == 'grid'){
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Biodata Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="active">Biodata Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning form-horizontal">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Biodata Mahasiswa</h3>
				  <div class="pull-right">
				  	<a href="<?php echo module_url($this->uri->segment(2).'/edit'); ?>" class="btn btn-success"><span class="fa fa-pencil	"></span> Edit</a>
				  </div>
                </div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="mahasiswa_user" class="col-md-2 control-label" style="text-align:left">User</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_user" id="mahasiswa_user" value="<?php echo $mahasiswa_user; ?>" placeholder="" readonly="readonly">
						</div>
					</div>
				</div>
				<div class="box-header">
					<h3 class="box-title">Informasi Identitas</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" disabled');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_nim" class="col-md-2 control-label" style="text-align:left">NIM</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_nim" id="mahasiswa_nim" value="<?php echo $mahasiswa_nim; ?>" placeholder="" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="mahasiswa_nama" id="mahasiswa_nama" value="<?php echo $mahasiswa_nama; ?>" placeholder="" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(8, 'mahasiswa_jenis_kelamin', 'kode_value', 'kode_nama', $mahasiswa_jenis_kelamin, '', '', 'class="form-control" disabled');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_tempat_lahir" class="col-md-2 control-label" style="text-align:left">Tempat, Tanggal Lahir</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="mahasiswa_tempat_lahir" id="mahasiswa_tempat_lahir" value="<?php echo $mahasiswa_tempat_lahir; ?>" placeholder="Tempat Lahir" readonly="readonly">
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_tanggal_lahir" id="mahasiswa_tanggal_lahir" value="<?php echo $mahasiswa_tanggal_lahir; ?>" placeholder="Tanggal Lahir" autocomplete="off" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_tahun_masuk" class="col-md-2 control-label" style="text-align:left">Tahun Masuk</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_nama', 'ASC'), 'mahasiswa_tahun_masuk', 'tahun_kode', 'tahun_nama', $mahasiswa_tahun_masuk, 'getSemester();', '', 'class="form-control" disabled');?>
						</div>
						<label for="mahasiswa_semester_awal" class="col-md-2 control-label" style="text-align:left">Semester Awal Masuk</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_semester_awal', 'semester_kode', 'semester_nama', $mahasiswa_semester_awal, '', '', 'class="form-control" disabled');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_batas_studi" class="col-md-2 control-label" style="text-align:left">Batas Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_batas_studi', 'semester_kode', 'semester_nama', $mahasiswa_batas_studi, '', '', 'class="form-control" disabled');?>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
					<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit'); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
				</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } else if ($action == 'edit') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#mahasiswa_tanggal_lahir').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Biodata Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="active">Update Biodata Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Update Biodata Mahasiswa</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$mahasiswa_id);?>" method="post">
				<input type="hidden" class="form-control" name="mahasiswa_id" id="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>" placeholder="">
				<div class="box-body">
					<div class="form-group">
						<label for="mahasiswa_user" class="col-md-2 control-label" style="text-align:left">User</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_user" id="mahasiswa_user" value="<?php echo $mahasiswa_user; ?>" placeholder="" readonly>
						</div>
				</div>
				<div class="box-header">
					<h3 class="box-title">Informasi Identitas</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" disabled');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_nim" class="col-md-2 control-label" style="text-align:left">NIM</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_nim" id="mahasiswa_nim" value="<?php echo $mahasiswa_nim; ?>" placeholder="" readonly>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="mahasiswa_nama" id="mahasiswa_nama" value="<?php echo $mahasiswa_nama; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(8, 'mahasiswa_jenis_kelamin', 'kode_value', 'kode_nama', $mahasiswa_jenis_kelamin, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_tempat_lahir" class="col-md-2 control-label" style="text-align:left">Tempat, Tanggal Lahir</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="mahasiswa_tempat_lahir" id="mahasiswa_tempat_lahir" value="<?php echo $mahasiswa_tempat_lahir; ?>" placeholder="Tempat Lahir">
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_tanggal_lahir" id="mahasiswa_tanggal_lahir" value="<?php echo $mahasiswa_tanggal_lahir; ?>" placeholder="Tanggal Lahir" autocomplete="off">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>