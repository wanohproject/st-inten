<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datepicker/datepicker3.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script>
$(function () {
  $('#presensi_tanggal_awal').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true,
      endDate: '0d'
  });
  $('#presensi_tanggal_akhir').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true,
      startDate: $('#presensi_tanggal_awal').val(),
      endDate: '0d'
  });
});
</script>
<style>
#datagrid,
#datagrid td,
#datagrid th {
  border-color: #000;
  padding: 3px 5px;
}
#datagrid th {
  background: #676767;
  color: #FFF;
}
.table-info-kehadiran td {
  font-size: 11px;
}
.table-grid-kehadiran th,
.table-grid-kehadiran td {
  padding: 0 !important;
  font-size: 12px;
}
</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Rekap Presensi Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('presensi_siswa'); ?>">Rekap Presensi Mahasiswa</a></li>
            <li class="active">Daftar Rekap Presensi Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Rekap Presensi Mahasiswa</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$siswa_id);?>" method="post">
									<div class="row">
                    <?php if (in_array(userdata('pengguna_level_id'), array(11, 15)) == false){ ?>
                    <div class="col-md-3">
											<div class="form-group">
												<label for="departemen_id" class="control-label">Departemen</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Alumni') ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="semester_id" class="control-label">Semester</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
												</div>		
											</div>
										</div>
										<?php if ($this->session->userdata('level') == 10){?>
										<div class="col-md-2">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tingkat</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tingkat.departemen_id = '$departemen_id' AND kelas.tahun_kode = '$tahun_kode' AND kelas.siswa_id = '$siswa_id' GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2"');?>
												</div>		
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="kelas_id" class="control-label">Kelas</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$tingkat_id' AND kelas.siswa_id = '$siswa_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2"');?>
												</div>		
											</div>
										</div>
										<?php } else { ?>
										<div class="col-md-2">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tingkat</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM tingkat WHERE tingkat.departemen_id = '$departemen_id' ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '-- SEMUA TINGKAT --', 'class="form-control select2"');?>
												</div>		
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="kelas_id" class="control-label">Kelas</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '$tahun_kode' AND tingkat_id = '$tingkat_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '-- SEMUA KELAS --', 'class="form-control select2"');?>
												</div>		
											</div>
										</div>
										<?php } ?>

                    <div class="col-md-4">
											<div class="form-group">
												<label for="siswa_id" class="control-label">Mahasiswa</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE kelas_id = '$kelas_id' ORDER BY siswa.siswa_nama ASC")->result(), 'siswa_id', 'siswa_id', 'siswa_nama', $siswa_id, 'submit();', 'Pilih Mahasiswa', 'class="form-control select2"'); ?>
												</div>
											</div>
                    </div>
                    <?php } ?>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="presensi_tanggal_awal" class="control-label">Awal</label>
                        <div class="">
                          <input type="text" class="form-control" name="presensi_tanggal_awal" id="presensi_tanggal_awal" value="<?php echo $presensi_tanggal_awal; ?>" placeholder="" onchange="submit();" autocomplete="off">
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="presensi_tanggal_akhir" class="control-label">Akhir</label>
                        <div class="">
                          <input type="text" class="form-control" name="presensi_tanggal_akhir" id="presensi_tanggal_akhir" value="<?php echo $presensi_tanggal_akhir; ?>" placeholder="" onchange="submit();" autocomplete="off">
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="" class="control-label">&nbsp;</label>
                        <div class="">
                          <button type="submit" name="show" value="show" class="btn btn-primary">Tampilkan</button>
                        </div>		
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-body -->
								</form>
                <?php if ($days && $departemen_id != '-' && ($show || $siswa_id != '-')){ ?>
                <div class="box-body">
                  <div style="overflow:auto;max-height:500px;border:1px solid;">
                  <table id="datagrid" class="table no-border">
                    <?php 
                    $where_tingkat = "";
                    if ($tingkat_id != '-') {
                      $where_tingkat .= " AND tingkat.tingkat_id = '$tingkat_id'";
                    }
                    if (userdata('pengguna_level_id') == 10){
                      $list_tingkat = $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tingkat.departemen_id = '$departemen_id' AND kelas.tahun_kode = '$tahun_kode' AND kelas.staf_id = '".userdata('staf_id')."' $where_tingkat GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama")->result();
                    } else {
                      $list_tingkat = $this->db->query("SELECT * FROM tingkat WHERE tingkat.departemen_id = '$departemen_id' $where_tingkat ORDER BY tingkat_nama ASC")->result();
                    }
                     if ($list_tingkat){
                       foreach ($list_tingkat as $row_tingkat) {
                        $where_kelas = "";
                        if ($kelas_id != '-') {
                          $where_kelas .= " AND kelas.kelas_id = '$kelas_id'";
                        }
                        if (userdata('pengguna_level_id') == 10){
                          $list_kelas = $this->db->query("SELECT * FROM kelas WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$row_tingkat->tingkat_id' AND kelas.staf_id = '".userdata('staf_id')."' $where_kelas ORDER BY kelas_nama ASC")->result();
                        } else {
                          $list_kelas = $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '$tahun_kode' AND tingkat_id = '$row_tingkat->tingkat_id' $where_kelas ORDER BY kelas_nama ASC")->result();
                        }

                        if ($list_kelas){
                          foreach ($list_kelas as $row_kelas) {
                            ?>
                            <tr>
                              <th class="text-left" colspan="2">Kelas <?php echo $row_kelas->kelas_nama;?></th>
                            </tr>
                            <?php
                            $where_siswa = "";
                            if ($siswa_id != '-') {
                              $where_siswa .= " AND siswa.siswa_id = '$siswa_id'";
                            }
                            $list_siswa = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE kelas_id = '$row_kelas->kelas_id' $where_siswa ORDER BY siswa.siswa_nama ASC")->result();
                            if ($list_siswa){
                              foreach ($list_siswa as $row_siswa) {
                                $grid_kehadiran = array();
                                $hadir = 0;
                                $tidakhadir = 0;
                                $sakit = 0;
                                $izin = 0;
                                $cuti = 0;
                                $lainnya = 0;

                                $j = 0;
                                for ($i=0; $i < $days; $i++){
                                  if ($i == 0){
                                    $date = $presensi_tanggal_awal;
                                  } else {
                                    $date = date('Y-m-d', strtotime($presensi_tanggal_awal . " + $i days"));
                                  }
            
                                  $N = date('N', strtotime($date));
                                  $jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$departemen_id));
                                  
                                  if ($jadwal){
                                    $info_presensi = "&nbsp;";
                                    if ($this->Libur_model->count_all_libur('', array('libur_tanggal'=>$date)) > 0){
                                      $hadir++;
                                    } else {
                                      $list_presensi	= $this->Presensi_model->grid_all_presensi('*', "presensi_tanggal_masuk", "ASC", 0, 0, "presensi.presensi_user = '$row_siswa->siswa_user' AND DATE(presensi.presensi_tanggal_masuk) = '$date'");
                                      if ($list_presensi){
                                        $hadir++;
                                        $info_presensi = "";
                                        foreach ($list_presensi as $row_presensi) {
                                          $presensi_tanggal_masuk = ($row_presensi)?($row_presensi->presensi_tanggal_masuk)?$row_presensi->presensi_tanggal_masuk:'':'';
                                          $presensi_tanggal_keluar = ($row_presensi)?($row_presensi->presensi_tanggal_keluar)?$row_presensi->presensi_tanggal_keluar:'':'';
                                          $jam_masuk = ($presensi_tanggal_masuk)?date('H:i', strtotime($row_presensi->presensi_tanggal_masuk)):'';
                                          $jam_pulang = ($presensi_tanggal_keluar)?date('H:i', strtotime($row_presensi->presensi_tanggal_keluar)):'';
                                          $info_presensi .= $jam_masuk."-".$jam_pulang."<br />";
                                        }
                                      } else {
                                        $tidakhadir++;
                                      }

                                      $list_absensi	= $this->Absensi_model->get_absensi('*', "absensi.tahun_kode = '$tahun_kode' AND absensi.semester_id = '$semester_id' AND absensi.absensi_user = '$row_siswa->siswa_user' AND DATE(absensi.absensi_tanggal) = '$date'");
                                      if ($list_absensi){
                                        if ($list_absensi->absensi_info == "Sakit"){
                                          $sakit++;
                                        } else if ($list_absensi->absensi_info == "Izin"){
                                          $izin++;
                                        } else if ($list_absensi->absensi_info == "Cuti"){
                                          $cuti++;
                                        }
                                      }
                                    }
                                    $grid_kehadiran[$j] = "<td style=\"vertical-align:top;\" nowrap>
                                                            <div style=\"text-align:center;background:#DDD;font-weight:bold;padding:3px 5px;\">".dateShort($date)." ".getDayShort($date)."</div>
                                                            <div style=\"text-align:center;padding:3px 5px;\">$info_presensi</div>
                                                          </td>";
                                    $j++;
                                  }
                                }
                                ?>
                                <tr>
                                  <td width="300">
                                    <table class="table-info-siswa" width="100%">
                                      <tr>
                                        <td width="50">NIS</td>
                                        <td style="border-bottom:1px solid #ddd"><?php echo $row_siswa->siswa_nis; ?></td>
                                      </tr>
                                      <tr>
                                        <td>Nama</td>
                                        <td style="border-bottom:1px solid #ddd"><?php echo $row_siswa->siswa_nama; ?></td>
                                      </tr>
                                    </table>
                                    <table class="no-border table-info-kehadiran" style="margin-top:5px;" width="100%">
                                      <tr>
                                        <td>Jumlah Kehadiran</td>
                                        <td style="text-align:right;"><?php echo $hadir; ?></td>
                                        <td>hari</td>
                                        <td style="text-align:right;">Tidak Hadir</td>
                                        <td style="text-align:right;"><?php echo $tidakhadir; ?></td>
                                        <td>hari</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align:right;">Sakit</td>
                                        <td style="text-align:right;"><?php echo $sakit; ?></td>
                                        <td>hari</td>
                                        <td style="text-align:right;">Izin</td>
                                        <td style="text-align:right;"><?php echo $izin; ?></td>
                                        <td>hari</td>
                                      </tr>
                                      <tr>
                                        <td style="text-align:right;">Lainnya</td>
                                        <td style="text-align:right;"><?php echo $lainnya; ?></td>
                                        <td>hari</td>
                                        <td style="text-align:right;">&nbsp;</td>
                                        <td style="text-align:right;">&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td>
                                    <table class="no-border table-grid-kehadiran" cellpadding="0" cellspacing="0" width="100%">
                                      <?php
                                      $jml_kehadiran = count($grid_kehadiran) - 1;
                                      foreach ($grid_kehadiran as $key_kehadiran => $value_kehadiran) {
                                        if ($key_kehadiran % 10 == 0){
                                          echo "<tr>";
                                        }
                                        echo $value_kehadiran;
                                        if ($key_kehadiran % 10 == 9 || $key_kehadiran == $jml_kehadiran){
                                          echo "</tr>";
                                        }
                                      }
                                      ?>
                                    </table>
                                  </td>
                                </tr>
                                <?php
                              }
                            }
                          }
                        }
                      }
                    } 
                    ?>
                  </table>
                  </div>
                </div><!-- /.box-body -->
                <?php }  ?>
                <div class="box-footer">
                  <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
                  <?php
                    if ($tingkat_id && $kelas_id && $semester_id && $siswa_id && $tingkat_id != '-' && $kelas_id != '-' && $semester_id != '-'){
                      ?>
                      <a href="<?php echo module_url($this->uri->segment(2).'/cetak/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$siswa_id.'/'.$presensi_tanggal_awal.'/'.$presensi_tanggal_akhir); ?>" target="_blank" class="btn btn-info">Cetak</a>                      
                      <?php
                    }
                  ?>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>