<?php 
$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
$this->output->set_header('Pragma: no-cache');

statistik();
log_url();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3.0, minimum-scale=1, user-scalable=yes"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3.0, minimum-scale=1, user-scalable=yes"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('asset/profil/' . profile('profil_favicon')); ?>" sizes="16x16"/>
	<!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/ionicons/css/ionicons.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/select2/select2.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/AdminLTE.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/skins/_all-skins.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/animate.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/custom.css');?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
    <script src="<?php echo theme_dir('admin_v2/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <script src="<?php echo theme_dir('admin_v2/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo theme_dir('admin_v2/plugins/select2/select2.full.min.js');?>"></script>
    <script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-notify/bootstrap-notify.min.js');?>"></script>
    <script src="<?php echo theme_dir('admin_v2/dist/js/app.js');?>"></script>
    <script src="<?php echo theme_dir('admin_v2/dist/js/demo.js');?>"></script>
	<script>
	  $(function () {
		//Initialize Select2 Elements
		$(".select2").select2();
	  });
	</script>
  </head>