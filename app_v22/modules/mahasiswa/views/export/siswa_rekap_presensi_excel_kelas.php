<?php
if ($excel == TRUE){
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msexcel'); // Other browsers
header("Content-Disposition: attachment;Filename=PRESENSI_".date("YmdHis").".xls");
}
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=ProgId content=Excel.Sheet>
	<title>LAPORAN PRESENSI</title>
	<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
</head>
<?php
$day = array();
for ($i=0; $i < $days; $i++) {
	if ($i == 0){
	$date = $presensi_tanggal_awal;
	} else {
	$date = date('Y-m-d', strtotime($presensi_tanggal_awal . " + $i days"));
	}
	$N = date('N', strtotime($date));
	$jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari_level));
	
	if ($jadwal){
	$day[] = $date;
	}
}
?>
<body>		
	<table border="1" style="border-collapse:collapse">
		<tr>
			<th style="text-align:center;" colspan="<?php echo (3 + count($day)); ?>">DATA REKAP PRESENSI</th>
		</tr>
		<tr>
			<th style="text-align:center;" colspan="<?php echo (3 + count($day)); ?>"><?php echo dateIndo($presensi_tanggal_awal) . ' s/d ' . dateIndo($presensi_tanggal_akhir); ?></th>
		</tr>
		<tr>
			<th style="text-align:center;" colspan="<?php echo (3 + count($day)); ?>">&nbsp;</th>
		</tr>
		<tr>
			<th style="text-align:center;" rowspan="2" width="35">No</th>
			<th style="text-align:center;" rowspan="2" width="250">Nama</th>
			<th style="text-align:center;" colspan="<?php echo count($day); ?>"><?php echo getBulan(date('m', strtotime($presensi_tanggal_awal))); ?></th>
			<th style="text-align:center;" rowspan="2" width="120">Lama Kerja</th>
		</tr>
		<tr>
		<?php
		foreach ($day as $value) {
		echo "<th style=\"text-align:center;\" width=\"40\">".date('d', strtotime($value))."</th>";
		}
		?>
		</tr>
		<?php
		$hadir = 0;
		$terlambat = 0;
		$tidakhadir = 0;
		$where_siswa_kelas['siswa_kelas.tahun_kode']	= $tahun_kode;
		$where_siswa_kelas['siswa_kelas.kelas_id']	= $kelas_id;
		$grid_siswa = $this->Siswa_kelas_model->grid_all_siswa_kelas("", "siswa_nama", "ASC", 0, 0, $where_siswa_kelas);
		$i = 1;
		foreach ($grid_siswa as $row) {
			$siswa_user = $row->siswa_user;
			echo "<tr>";
			echo "<td style=\"text-align:center;\">$i</td>";
			echo "<td>$row->siswa_nama</td>";
			$lama_kerja = 0;
			foreach ($day as $value) {
				$date = $value;
				$color = "FBCBCB";
				$color_masuk = "FBCBCB";
				$color_pulang = "FBCBCB";
				$status = "TK";

				if ($this->Libur_model->count_all_libur('', array('libur_tanggal'=>$date)) < 1){
					$presensi	= $this->Presensi_model->get_presensi('*', "presensi.tahun_kode = '$tahun_kode' AND presensi.semester_id = '$semester_id' AND presensi.presensi_user = '$siswa_user' AND DATE(presensi.presensi_tanggal_masuk) = '$date'");                        
					if ($presensi){
						$status = "H";
					}
					
					$presensi_tanggal_masuk = ($presensi)?($presensi->presensi_tanggal_masuk)?$presensi->presensi_tanggal_masuk:'':'';
					$presensi_tanggal_keluar = ($presensi)?($presensi->presensi_tanggal_keluar)?$presensi->presensi_tanggal_keluar:'':'';
					$jam_masuk = ($presensi_tanggal_masuk)?date('H:i:s', strtotime($presensi->presensi_tanggal_masuk)):'';
					$jam_pulang = ($presensi_tanggal_keluar)?date('H:i:s', strtotime($presensi->presensi_tanggal_keluar)):'';
					
					$jam_masuk_ = $jam_masuk;
					$jam_pulang_ = $jam_pulang;
					if ($presensi_tanggal_masuk){
						if (strtotime($presensi_tanggal_masuk) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
						$color = "#B3DE81";
						$color_masuk = "#B3DE81";
						$hadir++;
						} else if (strtotime($presensi_tanggal_masuk) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
						$color = "#FBCBCB";
						$color_masuk = "#FBCBCB";
						$terlambat++;
						}
					}
					
					if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($presensi_tanggal_keluar) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
						$color_pulang = "#B3DE81";
					} else if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($presensi_tanggal_keluar) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
						$color_pulang = "#FBCBCB";
					}

					$absensi	= $this->Absensi_model->get_absensi('*', "absensi.tahun_kode = '$tahun_kode' AND absensi.semester_id = '$semester_id' AND absensi.absensi_user = '$siswa_user' AND DATE(absensi.absensi_tanggal) = '$date'");
					if ($absensi){
						$status = substr($absensi->absensi_info, 0, 1);
						$color = "FBCBCB";
					}

					if ($status != "H"){
						$tidakhadir++;
					}

					if ($jam_masuk_ && !$jam_pulang_){
						$status = "-";
						$color = "FBCBCB";
					}

					$lama_kerja_ = ($jam_masuk_ && $jam_pulang_)?selisihDate2($jam_masuk_, $jam_pulang_):0;
					$lama_kerja = $lama_kerja + $lama_kerja_;

					echo "<td style=\"text-align:center;\" bgcolor=\"$color\">$status</td>";
				} else {
					$status = "L";
					echo "<td style=\"text-align:center;\" bgcolor=\"$color\">$status</td>";
				}
			}
			echo "<td style=\"text-align:center;\">".stoh($lama_kerja)."</td>";
			echo "</tr>";
			$i++;
		}
		?>
	</table> 
</body>
</html>	