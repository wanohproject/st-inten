<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=ProgId content=Excel.Sheet>
	<title>LAPORAN PRESENSI SISWA</title>
	<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/AdminLTE.min.css');?>">
	<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/skins/_all-skins.min.css');?>">
	<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/custom.css');?>">
	<style>
		table { page-break-inside:auto }
		tr    { page-break-inside:avoid; page-break-after:auto }
		thead { display:table-header-group }
		tfoot { display:table-footer-group }
		table {
			font-size: 13px !important;
		}
		#dataheader,
		#dataheader td,
		#dataheader th {
			border-color: #000;
			padding: 3px 5px;
			margin-bottom: 0px;
			font-size: 10px;
			line-height:1.1em;
		}
		#dataheader div {
			line-height:1.1em;
		} 
		#datagrid,
		#datagrid td,
		#datagrid th {
			border-color: #000;
			padding: 3px 3px;
			margin-bottom: 0px;
		}
		#datagrid th {
			background: #676767 !important;
			color: #FFF !important;
			font-size: 14px;
		}
		.table-info-siswa td {
			font-size: 11px;
		}
		.table-info-kehadiran td {
			font-size: 9px;
		}
		.table-grid-kehadiran th,
		.table-grid-kehadiran td {
			padding: 0 !important;
			font-size: 9px;
		}
	</style>
</head>
<body style="padding:20px;" onload="window.print();">
<table id="dataheader" class="table no-border" width="90%">
	<tr>
		<td width="58"><img src="<?php echo base_url('asset/profil/'.profile('profil_logo')); ?>" alt="" style="width:100%"></td>
		<td>
			<div style="font-size:22px;"><?php echo profile('profil_institusi'); ?></div>
			<?php echo profile('profil_kontak'); ?><br />
			Telp. <?php echo profile('profil_telp'); ?> Fax. <?php echo profile('profil_fax'); ?><br />
			Website: <?php echo profile('profil_situs'); ?> Email: <?php echo profile('profil_email'); ?>
		</td>
		<td style="text-align:right;vertical-align:bottom;">
			<div style="font-size:15px;font-weight:bold;">Laporan Absensi Kehadiran</div>
			Periode: <?php echo dateIndo3($presensi_tanggal_awal); ?> s/d <?php echo dateIndo3($presensi_tanggal_akhir); ?><br />
			Tanggal Cetak : <?php echo dateIndo(date('Y-m-d')); ?>
		</td>
	</tr>
</table>
<table id="datagrid" class="table no-border" width="90%">
<?php 
$where_tingkat = "";
if ($tingkat_id != '-') {
	$where_tingkat .= " AND tingkat.tingkat_id = '$tingkat_id'";
}
if (userdata('pengguna_level_id') == 10){
	$list_tingkat = $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tingkat.departemen_id = '$departemen_id' AND kelas.tahun_kode = '$tahun_kode' AND kelas.staf_id = '".userdata('staf_id')."' $where_tingkat GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama")->result();
} else {
	$list_tingkat = $this->db->query("SELECT * FROM tingkat WHERE tingkat.departemen_id = '$departemen_id' $where_tingkat ORDER BY tingkat_nama ASC")->result();
}
	if ($list_tingkat){
	foreach ($list_tingkat as $row_tingkat) {
	$where_kelas = "";
	if ($kelas_id != '-') {
		$where_kelas .= " AND kelas.kelas_id = '$kelas_id'";
	}
	if (userdata('pengguna_level_id') == 10){
		$list_kelas = $this->db->query("SELECT * FROM kelas WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$row_tingkat->tingkat_id' AND kelas.staf_id = '".userdata('staf_id')."' $where_kelas ORDER BY kelas_nama ASC")->result();
	} else {
		$list_kelas = $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '$tahun_kode' AND tingkat_id = '$row_tingkat->tingkat_id' $where_kelas ORDER BY kelas_nama ASC")->result();
	}

	if ($list_kelas){
		foreach ($list_kelas as $row_kelas) {
		?>
		<tr>
			<th class="text-left" colspan="2">Kelas <?php echo $row_kelas->kelas_nama;?></th>
		</tr>
		<?php
		$where_siswa = "";
		if ($siswa_id != '-') {
			$where_siswa .= " AND siswa.siswa_id = '$siswa_id'";
		}
		$list_siswa = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE kelas_id = '$row_kelas->kelas_id' $where_siswa ORDER BY siswa.siswa_nama ASC")->result();
		if ($list_siswa){
			foreach ($list_siswa as $row_siswa) {
			$grid_kehadiran = array();
			$hadir = 0;
			$tidakhadir = 0;
			$sakit = 0;
			$izin = 0;
			$cuti = 0;
			$lainnya = 0;

			$j = 0;
			for ($i=0; $i < $days; $i++){
				if ($i == 0){
				$date = $presensi_tanggal_awal;
				} else {
				$date = date('Y-m-d', strtotime($presensi_tanggal_awal . " + $i days"));
				}

				$N = date('N', strtotime($date));
				$jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$departemen_id));
				
				if ($jadwal){
				$info_presensi = "&nbsp;";
				if ($this->Libur_model->count_all_libur('', array('libur_tanggal'=>$date)) > 0){
					$hadir++;
				} else {
					$list_presensi	= $this->Presensi_model->grid_all_presensi('*', "presensi_tanggal_masuk", "ASC", 0, 0, "presensi.presensi_user = '$row_siswa->siswa_user' AND DATE(presensi.presensi_tanggal_masuk) = '$date'");
					if ($list_presensi){
					$hadir++;
					$info_presensi = "";
					foreach ($list_presensi as $row_presensi) {
						$presensi_tanggal_masuk = ($row_presensi)?($row_presensi->presensi_tanggal_masuk)?$row_presensi->presensi_tanggal_masuk:'':'';
						$presensi_tanggal_keluar = ($row_presensi)?($row_presensi->presensi_tanggal_keluar)?$row_presensi->presensi_tanggal_keluar:'':'';
						$jam_masuk = ($presensi_tanggal_masuk)?date('H:i', strtotime($row_presensi->presensi_tanggal_masuk)):'';
						$jam_pulang = ($presensi_tanggal_keluar)?date('H:i', strtotime($row_presensi->presensi_tanggal_keluar)):'';
						$info_presensi .= $jam_masuk."-".$jam_pulang."<br />";
					}
					} else {
					$tidakhadir++;
					}

					$list_absensi	= $this->Absensi_model->get_absensi('*', "absensi.tahun_kode = '$tahun_kode' AND absensi.semester_id = '$semester_id' AND absensi.absensi_user = '$row_siswa->siswa_user' AND DATE(absensi.absensi_tanggal) = '$date'");
					if ($list_absensi){
					if ($list_absensi->absensi_info == "Sakit"){
						$sakit++;
					} else if ($list_absensi->absensi_info == "Izin"){
						$izin++;
					} else if ($list_absensi->absensi_info == "Cuti"){
						$cuti++;
					}
					}
				}
				$grid_kehadiran[$j] = "<td style=\"vertical-align:top;\" nowrap>
										<div style=\"text-align:center;background:#DDD !important;font-weight:bold;padding:3px 5px;\">".dateShort($date)." ".getDayShort($date)."</div>
										<div style=\"text-align:center;padding:3px 5px;\">$info_presensi</div>
										</td>";
				$j++;
				}
			}
			?>
			<tr>
				<td width="300">
				<table class="table-info-siswa" width="100%">
					<tr>
					<td width="50">NIS</td>
					<td style="border-bottom:1px solid #ddd"><?php echo $row_siswa->siswa_nis; ?></td>
					</tr>
					<tr>
					<td>Nama</td>
					<td style="border-bottom:1px solid #ddd"><?php echo $row_siswa->siswa_nama; ?></td>
					</tr>
				</table>
				<table class="no-border table-info-kehadiran" style="margin-top:5px;" width="100%">
					<tr>
					<td>Jumlah Kehadiran</td>
					<td style="text-align:right;"><?php echo $hadir; ?></td>
					<td>hari</td>
					<td style="text-align:right;">Tidak Hadir</td>
					<td style="text-align:right;"><?php echo $tidakhadir; ?></td>
					<td>hari</td>
					</tr>
					<tr>
					<td style="text-align:right;">Sakit</td>
					<td style="text-align:right;"><?php echo $sakit; ?></td>
					<td>hari</td>
					<td style="text-align:right;">Izin</td>
					<td style="text-align:right;"><?php echo $izin; ?></td>
					<td>hari</td>
					</tr>
					<tr>
					<td style="text-align:right;">Lainnya</td>
					<td style="text-align:right;"><?php echo $lainnya; ?></td>
					<td>hari</td>
					<td style="text-align:right;">&nbsp;</td>
					<td style="text-align:right;">&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
				</table>
				</td>
				<td>
				<table class="no-border table-grid-kehadiran" cellpadding="0" cellspacing="0" width="100%">
					<?php
					$jml_kehadiran = count($grid_kehadiran) - 1;
					foreach ($grid_kehadiran as $key_kehadiran => $value_kehadiran) {
					if ($key_kehadiran % 10 == 0){
						echo "<tr>";
					}
					echo $value_kehadiran;
					if ($key_kehadiran % 10 == 9 || $key_kehadiran == $jml_kehadiran){
						echo "</tr>";
					}
					}
					?>
				</table>
				</td>
			</tr>
			<?php
			}
		}
		}
	}
	}
} 
?>
</table>
</body>
</html>	