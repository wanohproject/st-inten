<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Biodata Mahasiswa | ' . profile('profil_website');
		$this->active_menu		= '380';
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Referensi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Dosen_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
	}
	
	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where['mahasiswa_id']	= userdata('mahasiswa_id'); 
		$mahasiswa		 		= $this->Mahasiswa_model->get_mahasiswa('*', $where);
		if (!$mahasiswa){
			redirect(site_url());
			exit();
		}
		if ($this->session->userdata('level') != 11 && $this->session->userdata('level') != 15){
			redirect(site_url());
			exit();
		}
		
		$data['mahasiswa_id'] = $mahasiswa->mahasiswa_id;
		$data['perguruan_tinggi_id'] = $mahasiswa->perguruan_tinggi_id;
		$data['program_studi_id'] = $mahasiswa->program_studi_id;
		$data['jenjang_kode'] = $mahasiswa->jenjang_kode;
		$data['mahasiswa_nim'] = $mahasiswa->mahasiswa_nim;
		$data['mahasiswa_nama'] = $mahasiswa->mahasiswa_nama;
		$data['mahasiswa_tempat_lahir'] = $mahasiswa->mahasiswa_tempat_lahir;
		$data['mahasiswa_tanggal_lahir'] = $mahasiswa->mahasiswa_tanggal_lahir;
		$data['mahasiswa_jenis_kelamin'] = $mahasiswa->mahasiswa_jenis_kelamin;
		$data['mahasiswa_tahun_masuk'] = $mahasiswa->mahasiswa_tahun_masuk;
		$data['mahasiswa_semester_awal'] = $mahasiswa->mahasiswa_semester_awal;
		$data['mahasiswa_batas_studi'] = $mahasiswa->mahasiswa_batas_studi;
		$data['mahasiswa_tanggal_masuk'] = $mahasiswa->mahasiswa_tanggal_masuk;
		$data['mahasiswa_tanggal_lulus'] = $mahasiswa->mahasiswa_tanggal_lulus;
		$data['mahasiswa_sks_diakui'] = $mahasiswa->mahasiswa_sks_diakui;
		$data['mahasiswa_pindahan_nim'] = $mahasiswa->mahasiswa_pindahan_nim;
		$data['mahasiswa_pindahan_pt'] = $mahasiswa->mahasiswa_pindahan_pt;
		$data['mahasiswa_pindahan_prodi'] = $mahasiswa->mahasiswa_pindahan_prodi;
		$data['mahasiswa_pindahan_jenjang'] = $mahasiswa->mahasiswa_pindahan_jenjang;
		$data['mahasiswa_user'] = $mahasiswa->mahasiswa_user;
		$data['mahasiswa_pin'] = $mahasiswa->mahasiswa_pin;
		$data['mahasiswa_dosen_wali'] = $mahasiswa->mahasiswa_dosen_wali;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/biodata', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['mahasiswa_id']		= userdata('mahasiswa_id'); 
		$mahasiswa 					= $this->Mahasiswa_model->get_mahasiswa('*', $where);

		$data['mahasiswa_id'] = ($this->input->post('mahasiswa_id'))?$this->input->post('mahasiswa_id'):$mahasiswa->mahasiswa_id;
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$mahasiswa->perguruan_tinggi_id;
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$mahasiswa->program_studi_id;
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):$mahasiswa->jenjang_kode;
		$data['mahasiswa_nim'] = ($this->input->post('mahasiswa_nim'))?$this->input->post('mahasiswa_nim'):$mahasiswa->mahasiswa_nim;
		$data['mahasiswa_nama'] = ($this->input->post('mahasiswa_nama'))?$this->input->post('mahasiswa_nama'):$mahasiswa->mahasiswa_nama;
		$data['mahasiswa_tempat_lahir'] = ($this->input->post('mahasiswa_tempat_lahir'))?$this->input->post('mahasiswa_tempat_lahir'):$mahasiswa->mahasiswa_tempat_lahir;
		$data['mahasiswa_tanggal_lahir'] = ($this->input->post('mahasiswa_tanggal_lahir'))?$this->input->post('mahasiswa_tanggal_lahir'):$mahasiswa->mahasiswa_tanggal_lahir;
		$data['mahasiswa_jenis_kelamin'] = ($this->input->post('mahasiswa_jenis_kelamin'))?$this->input->post('mahasiswa_jenis_kelamin'):$mahasiswa->mahasiswa_jenis_kelamin;
		$data['mahasiswa_tahun_masuk'] = ($this->input->post('mahasiswa_tahun_masuk'))?$this->input->post('mahasiswa_tahun_masuk'):$mahasiswa->mahasiswa_tahun_masuk;
		$data['mahasiswa_semester_awal'] = ($this->input->post('mahasiswa_semester_awal'))?$this->input->post('mahasiswa_semester_awal'):$mahasiswa->mahasiswa_semester_awal;
		$data['mahasiswa_batas_studi'] = ($this->input->post('mahasiswa_batas_studi'))?$this->input->post('mahasiswa_batas_studi'):$mahasiswa->mahasiswa_batas_studi;
		$data['mahasiswa_tanggal_masuk'] = ($this->input->post('mahasiswa_tanggal_masuk'))?$this->input->post('mahasiswa_tanggal_masuk'):$mahasiswa->mahasiswa_tanggal_masuk;
		$data['mahasiswa_tanggal_lulus'] = ($this->input->post('mahasiswa_tanggal_lulus'))?$this->input->post('mahasiswa_tanggal_lulus'):$mahasiswa->mahasiswa_tanggal_lulus;
		$data['mahasiswa_sks_diakui'] = ($this->input->post('mahasiswa_sks_diakui'))?$this->input->post('mahasiswa_sks_diakui'):$mahasiswa->mahasiswa_sks_diakui;
		$data['mahasiswa_pindahan_nim'] = ($this->input->post('mahasiswa_pindahan_nim'))?$this->input->post('mahasiswa_pindahan_nim'):$mahasiswa->mahasiswa_pindahan_nim;
		$data['mahasiswa_pindahan_pt'] = ($this->input->post('mahasiswa_pindahan_pt'))?$this->input->post('mahasiswa_pindahan_pt'):$mahasiswa->mahasiswa_pindahan_pt;
		$data['mahasiswa_pindahan_prodi'] = ($this->input->post('mahasiswa_pindahan_prodi'))?$this->input->post('mahasiswa_pindahan_prodi'):$mahasiswa->mahasiswa_pindahan_prodi;
		$data['mahasiswa_pindahan_jenjang'] = ($this->input->post('mahasiswa_pindahan_jenjang'))?$this->input->post('mahasiswa_pindahan_jenjang'):$mahasiswa->mahasiswa_pindahan_jenjang;
		$data['mahasiswa_user'] = ($this->input->post('mahasiswa_user'))?$this->input->post('mahasiswa_user'):$mahasiswa->mahasiswa_user;
		$data['mahasiswa_pin'] = ($this->input->post('mahasiswa_pin'))?$this->input->post('mahasiswa_pin'):$mahasiswa->mahasiswa_pin;
		$data['mahasiswa_dosen_wali'] = ($this->input->post('mahasiswa_dosen_wali'))?$this->input->post('mahasiswa_dosen_wali'):$mahasiswa->mahasiswa_dosen_wali;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('mahasiswa_user'))."' && pengguna_nama != '".$mahasiswa->mahasiswa_user."'") < 1 && 
				$this->Mahasiswa_model->count_all_mahasiswa("mahasiswa_user = '".validasi_sql($this->input->post('mahasiswa_user'))."' && mahasiswa_user != '".$mahasiswa->mahasiswa_user."'") < 1){
				
				$where_update['mahasiswa_id']	= validasi_sql($this->input->post('mahasiswa_id'));
				$update['mahasiswa_nama'] = validasi_input($this->input->post('mahasiswa_nama'));
				$update['mahasiswa_tempat_lahir'] = validasi_input($this->input->post('mahasiswa_tempat_lahir'));
				$update['mahasiswa_tanggal_lahir'] = validasi_input($this->input->post('mahasiswa_tanggal_lahir'));
				$update['mahasiswa_jenis_kelamin'] = validasi_input($this->input->post('mahasiswa_jenis_kelamin'));
				$this->Mahasiswa_model->update_mahasiswa($where_update, $update);
				
				$mahasiswa_id		= validasi_sql($this->input->post('mahasiswa_id'));
				$mahasiswa_nama		= validasi_sql($this->input->post('mahasiswa_nama'));
				$mahasiswa_user		= validasi_sql($this->input->post('mahasiswa_user'));
				$mahasiswa_pin		= validasi_sql($this->input->post('mahasiswa_pin'));

				$where_pengguna = array();
				$where_pengguna['pengguna.pengguna_nama']		= $mahasiswa->mahasiswa_user;
				$where_pengguna['pengguna.pengguna_level_id']	= 11;
				if ($this->Pengguna_model->count_all_pengguna($where_pengguna) > 0){
					$update_pengguna = array();
					$update_pengguna['pengguna_nama_depan']	= $mahasiswa_nama;
					if ($mahasiswa_pin != 'CHANGED' && $mahasiswa_pin != $mahasiswa->mahasiswa_pin && $mahasiswa_pin && strlen($mahasiswa_pin) >= 6){
						$update_pengguna['pengguna_kunci']	= hash_password($mahasiswa_pin);

						$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_id'=>$mahasiswa_id), array('mahasiswa_pin'=>$mahasiswa_pin));
					}
					if ($mahasiswa->mahasiswa_user != $mahasiswa_user){
						$update_pengguna['pengguna_nama']	= $mahasiswa_user;
					}
					$this->Pengguna_model->update_pengguna(array('pengguna_nama'=>$mahasiswa->mahasiswa_user, 'pengguna_level_id'=>11), $update_pengguna);
				}
				
				$this->session->set_flashdata('success','Mahasiswa telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User telah digunakan. Silahkan gunakan user yang lain.');
				redirect(module_url($this->uri->segment(2)));
			}
			
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/biodata', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function upload_foto()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'upload_foto';
		
		$data['dataExcel']				= array();		
		$data['filename']				= '';
		
		if ($this->input->post('uploadFoto')){
			$total = count($_FILES['fileFoto']['name']);
			$path		= "./asset/uploads/foto-mahasiswa/";
			$mkdir		= true;
			if(file_exists($path) && is_dir($path)){
			} else {
				if (mkdir($path, 0777, true)){
				} else {
					$mkdir = false;
				}
			}
			for($i=0; $i<$total; $i++) {
				$name		= $_FILES['fileFoto']['name'][$i];
				$size		= $_FILES['fileFoto']['size'][$i];
				$basename 	= pathinfo($name);

				$where_update['mahasiswa_user']	= $basename['filename'];
				if ($this->Mahasiswa_model->count_all_mahasiswa($where_update) > 0){
					if ($mkdir){
						$newname	= filename_seo($name);
						$fullname	= $path . '/' . filename_seo($name);
						$tmp = $_FILES['fileFoto']['tmp_name'][$i];
						if ($tmp){
							if (move_uploaded_file($tmp, $fullname)){
								$this->Mahasiswa_model->update_mahasiswa($where_update, array('mahasiswa_foto'=>$basename['basename']));
							}
						}
					}
				}
			}
			$this->session->set_flashdata('success','Materi telah berhasil ditambah.');
			redirect(current_url());
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/biodata', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
