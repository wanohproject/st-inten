<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('module_dir'))
{
	function module_dir($data=""){
		$obj =& get_instance();
		if ($data){
			return 'mahasiswa/' . $data;
		} else {
			return 'mahasiswa';
		}
	}
}

if ( ! function_exists('module_url'))
{
	function module_url($data=""){
		$obj =& get_instance();
		if ($data){
			return site_url('mahasiswa/' . $data);
		} else {
			return site_url('mahasiswa');
		}
	}
}

if ( ! function_exists('module_info'))
{
	function module_info($param1="modul_nama"){
		$obj =& get_instance();
		$obj->load->model('Modul_model');
		$module_name = $obj->router->fetch_module();
		$where['modul_dir'] = $module_name;
		$modul = $obj->Modul_model->get_modul('*', $where);
		if ($modul){
			if (isset($modul->$param1)){
				return $modul->$param1;
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}

if ( ! function_exists('getExtension'))
{
	function getExtension($str)
	{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
}

if ( ! function_exists('Parse_Data'))
{
	function Parse_Data($data,$p1,$p2){
		$data=" ".$data;
		$hasil="";
		$awal=strpos($data,$p1);
		if($awal!=""){
			$akhir=strpos(strstr($data,$p1),$p2);
			if($akhir!=""){
				$hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
			}
		}
		return $hasil;	
	}
}
/* End of file mod_constant_helper.php */
/* Location: ./application/helpers/mod_constant_helper.php */