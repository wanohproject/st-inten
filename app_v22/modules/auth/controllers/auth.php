<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {
	
	public function __construct()
    {
    	parent::__construct();
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		
        $this->load->model('Login_model');
        $this->load->model('Loginnew_model');
    }
	
	public function index()
	{
		redirect(module_url('login'), 'refresh');
	}
	
	public function login()
	{
		$callback = site_url('dashboard');
		$data['url'] = ($this->input->get('url') && $this->input->get('url') != base_url() && $this->input->get('url') != module_url())?"?url=".rawurlencode($this->input->get('url')):'';
		$this->authentication->validation(FALSE, $callback);

		$session_id = $this->session->userdata('session_id');
		
		$data['title']			= 'Login | ' . profile('profil_website');
		$signin = $this->input->post('signin');
		if ($signin){
			$exp 		= time()-1800;
			$sql_delete = "DELETE FROM captcha WHERE captcha_time < ".$exp."";
			$this->db->query($sql_delete);
			
			$sql_get 	= "SELECT COUNT(*) as count FROM captcha WHERE word=? AND session_id = '$session_id' AND captcha_time > ?";
			$datacap	= array($this->input->post('captcha'), $_SERVER['REMOTE_ADDR'], $exp);
			$query		= $this->db->query($sql_get, $datacap);
			$row		= $query->row();
			
			$username 	= $this->input->post('username');
			$password 	= $this->input->post('password');
			
			$where_login['pengguna_nama']	= $username;
			$where_login['pengguna_kunci']	= md5($password);
			
			if ($row->count != 0){
				if ($this->Loginnew_model->cek_login($where_login) === TRUE){
					$url = ($this->input->get('url') && $this->input->get('url') != base_url() && $this->input->get('url') != module_url())?$this->input->get('url'):site_url();
					redirect($url);
				} else {
					$this->session->set_flashdata('success', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-ban"></i> Gagal!</h4>Pengguna dan Kata Kunci tidak cocok!</div>');
					redirect(module_url('login').$data['url']);
				}
			} else {
				$this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-ban"></i> Gagal!</h4>Captcha tidak cocok!</div>');
				redirect(module_url('login').$data['url']);
			}
		} else {
			$data['captchaImage']	= $this->load_captcha();
		}
		$this->load->view(module_dir().'/page/login', $data);
	}
	
	public function logout(){
		$this->Loginnew_model->remov_session();
		$url = ($this->input->get('url') && $this->input->get('url') != base_url() && $this->input->get('url') != module_url())?"?url=".rawurlencode($this->input->get('url')):'';
		redirect(module_url('login').$url);
	}
	
	public function load_captcha()
	{
		//$this->load->helper('captcha');
		$alpha 	= '1234567890';
		$acak	= str_shuffle($alpha);
		$acak 	= substr($acak,0,4);
		$nilai 	= array(
					'word'			=> $acak,
					'img_path'		=> './asset/captcha/',
					'img_url'	 	=> base_url().'asset/captcha/',
					'font_path'		=> './system/fonts/texb.ttf',
					'img_width'		=> '150',
					'img_height'	=> 34,
					'expiration'	=> 1800);
					
		$captcha =  $this->create_captcha($nilai);
		$data	 = array(
					'captcha_time'	=> $captcha['time'],
					'ip_address'	=> $_SERVER['REMOTE_ADDR'],
					'session_id'	=> $this->session->userdata('session_id'),
					'word'			=> $captcha['word']);

		$this->session->set_userdata('validasi', $captcha['word']);
		$query = $this->db->insert_string('captcha',$data);
		$this->db->query($query);
		return $captcha['image'];
	}
	
	public function create_captcha($data = '', $img_path = '', $img_url = '', $font_path = '')
	{
		$defaults = array('word' => '', 'img_path' => '', 'img_url' => '', 'img_width' => '150', 'img_height' => '30', 'font_path' => '', 'expiration' => 7200);

		foreach ($defaults as $key => $val)
		{
			if ( ! is_array($data))
			{
				if ( ! isset($$key) OR $$key == '')
				{
					$$key = $val;
				}
			}
			else
			{
				$$key = ( ! isset($data[$key])) ? $val : $data[$key];
			}
		}

		if ($img_path == '' OR $img_url == '')
		{
			return FALSE;
		}

		if ( ! @is_dir($img_path))
		{
			return FALSE;
		}

		if ( ! is_writable($img_path))
		{
			return FALSE;
		}

		if ( ! extension_loaded('gd'))
		{
			return FALSE;
		}

		// -----------------------------------
		// Remove old images
		// -----------------------------------

		list($usec, $sec) = explode(" ", microtime());
		$now = ((float)$usec + (float)$sec);

		$current_dir = @opendir($img_path);

		while ($filename = @readdir($current_dir))
		{
			if ($filename != "." and $filename != ".." and $filename != "index.html")
			{
				$name = str_replace(".jpg", "", $filename);

				if (($name + $expiration) < $now)
				{
					@unlink($img_path.$filename);
				}
			}
		}

		@closedir($current_dir);

		// -----------------------------------
		// Do we have a "word" yet?
		// -----------------------------------

	   if ($word == '')
	   {
			$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

			$str = '';
			for ($i = 0; $i < 8; $i++)
			{
				$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
			}

			$word = $str;
	   }

		// -----------------------------------
		// Determine angle and position
		// -----------------------------------

		$length	= strlen($word);
		$angle	= ($length >= 6) ? rand(-($length-6), ($length-6)) : 0;
		$x_axis	= rand(6, (360/$length)-16);
		$y_axis = ($angle >= 0 ) ? rand($img_height, $img_width) : rand(6, $img_height);

		// -----------------------------------
		// Create image
		// -----------------------------------

		// PHP.net recommends imagecreatetruecolor(), but it isn't always available
		if (function_exists('imagecreatetruecolor'))
		{
			$im = imagecreatetruecolor($img_width, $img_height);
		}
		else
		{
			$im = imagecreate($img_width, $img_height);
		}

		// -----------------------------------
		//  Assign colors
		// -----------------------------------

		$bg_color		= imagecolorallocate ($im, 255, 255, 255);
		$border_color	= imagecolorallocate ($im, 100, 134, 149);
		$text_color		= imagecolorallocate ($im, 99, 154, 185);
		$grid_color		= imagecolorallocate($im, 118, 200, 230);
		$shadow_color	= imagecolorallocate($im, 237, 248, 252);

		// -----------------------------------
		//  Create the rectangle
		// -----------------------------------

		ImageFilledRectangle($im, 0, 0, $img_width, $img_height, $bg_color);

		// -----------------------------------
		//  Create the spiral pattern
		// -----------------------------------

		$theta		= 1;
		$thetac		= 7;
		$radius		= 16;
		$circles	= 20;
		$points		= 32;

		for ($i = 0; $i < ($circles * $points) - 1; $i++)
		{
			$theta = $theta + $thetac;
			$rad = $radius * ($i / $points );
			$x = ($rad * cos($theta)) + $x_axis;
			$y = ($rad * sin($theta)) + $y_axis;
			$theta = $theta + $thetac;
			$rad1 = $radius * (($i + 1) / $points);
			$x1 = ($rad1 * cos($theta)) + $x_axis;
			$y1 = ($rad1 * sin($theta )) + $y_axis;
			imageline($im, $x, $y, $x1, $y1, $grid_color);
			$theta = $theta - $thetac;
		}

		// -----------------------------------
		//  Write the text
		// -----------------------------------

		$use_font = ($font_path != '' AND file_exists($font_path) AND function_exists('imagettftext')) ? TRUE : FALSE;

		if ($use_font == FALSE)
		{
			$font_size = 18;
			$x = rand(0, $img_width/($length/3));
			$y = 0;
		}
		else
		{
			$font_size	= 16;
			$x = rand(0, $img_width/($length/1.5));
			$y = $font_size+2;
		}

		for ($i = 0; $i < strlen($word); $i++)
		{
			if ($use_font == FALSE)
			{
				$y = rand(0 , $img_height/2);
				imagestring($im, $font_size, $x, $y, substr($word, $i, 1), $text_color);
				$x += ($font_size*2);
			}
			else
			{
				$y = rand($img_height/2, $img_height-3);
				imagettftext($im, $font_size, $angle, $x, $y, $text_color, $font_path, substr($word, $i, 1));
				$x += $font_size;
			}
		}


		// -----------------------------------
		//  Create the border
		// -----------------------------------

		imagerectangle($im, 0, 0, $img_width-1, $img_height-1, $border_color);

		// -----------------------------------
		//  Generate the image
		// -----------------------------------

		$img_name = $now.'.jpg';

		ImageJPEG($im, $img_path.$img_name);

		$img = "<img src=\"$img_url$img_name\" width=\"$img_width\" height=\"$img_height\" style=\"border:0;width:100%\" alt=\" \" />";

		ImageDestroy($im);

		return array('word' => $word, 'time' => $now, 'image' => $img);
	}
}