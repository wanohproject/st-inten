<?php
statistik();
log_url();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3.0, minimum-scale=1, user-scalable=yes"/>
	  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('asset/profil/' . profile('profil_favicon')); ?>" sizes="16x16"/>
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/ionicons/css/ionicons.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/dist/css/AdminLTE.min.css');?>">
    <link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/iCheck/flat/blue.css');?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition <?php echo (profile('profil_boxed') == 'Y')?'layout-boxed':''; ?> login-page" style="background:url('<?php echo base_url('/asset/profil/'.profile('profil_background_login'))?>') no-repeat center center; background-size:cover;">
    <div class="login-box">
      <div class="login-logo">
		&nbsp;
      </div><!-- /.login-logo -->
	  <?php if ($this->session->flashdata('success')) { echo $this->session->flashdata('success');} else if ($this->session->flashdata('error')) { echo $this->session->flashdata('error');} ?>
      <div class="login-box-body">
		<h3 class="text-center"><?php echo profile('profil_website'); ?></h3>
        <br />
        <form action="<?php echo module_url('login').$url; ?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="username" id="username" class="form-control" placeholder="Akun Pengguna" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" id="password" class="form-control" placeholder="Kata Kunci" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
		  <div class="row">
				<div class="col-xs-6">
					<?php echo $captchaImage; ?>
				</div>
				<div class="col-xs-6">
					<div class="form-group has-feedback">
						<input type="text" name="captcha" id="captcha" class="form-control" placeholder="Kode" required>
						<span class="glyphicon glyphicon-eye-open form-control-feedback"></span>
					</div>
				</div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" name="signin" value="signin" class="btn btn-primary btn-block btn-flat">Sign in</button>
              <a href="<?php echo profile('profil_situs'); ?>" class="btn btn-default btn-block btn-flat">Ke Halaman Utama</a>
            </div><!-- /.col -->
          </div>
        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="<?php echo theme_dir('admin_v2/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <script src="<?php echo theme_dir('admin_v2/bootstrap/js/bootstrap.min.js');?>"></script>
	<script src="<?php echo theme_dir('admin_v2/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
