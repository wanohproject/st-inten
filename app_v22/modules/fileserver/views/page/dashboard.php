<!-- Content Wrapper. Contains page content -->
    <script>
		  function resizeIframe(obj) {
			obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
		  }
		</script>
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            FILE SERVER
            <small><?php echo profile('profil_institusi'); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-dashboard"></i>Beranda</a></li>
            <li class="active">File Server</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
			      <div class="col-md-12">
            <iframe src="<?php echo $iframe; ?>" scrolling='no'  width="100%" height="500" name="CHANGETHIS" id="CHANGETHIS" marginheight="0" frameborder="0" onload="autoResize(this);" onresize="autoResize(this);"></iframe>
            </div><!-- /.row -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->