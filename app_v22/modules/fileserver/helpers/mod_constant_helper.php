<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * 
 *
 * @package		
 * @author		
 * @copyright	
 * @license		
 * @link		
 * @since		
 * @filesource
 */

// ------------------------------------------------------------------------

if ( ! function_exists('module_dir'))
{
	function module_dir($data=""){
		$obj =& get_instance();
		if ($data){
			return 'fileserver/' . $data;
		} else {
			return 'fileserver';
		}
	}
}

if ( ! function_exists('module_url'))
{
	function module_url($data=""){
		$obj =& get_instance();
		if ($data){
			return site_url('fileserver/' . $data);
		} else {
			return site_url('fileserver');
		}
	}
}

if ( ! function_exists('getExtension'))
{
	function getExtension($str)
	{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
}
/* End of file MY_application_helper.php */
/* Location: ./application/helpers/MY_application_helper.php */