<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('pengguna/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "username", "width": "100" },
			{ "data": "nama_lengkap" },
			{ "data": "pengguna_level_nama" },
			{ "data": "Actions", "width": "100" },
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthPengguna": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthPengguna": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pengguna
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('pengguna'); ?>">Pengguna</a></li>
            <li class="active">Daftar Pengguna</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Pengguna</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>Nama Lengkap</th>
                        <th>Kelompok</th>
                        <th>&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pengguna
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('pengguna'); ?>">Pengguna</a></li>
            <li class="active">Tambah Pengguna</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Pengguna</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
					<input type="hidden" class="form-control" name="pengguna_id" id="pengguna_id" value="<?php echo $pengguna_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="pengguna_nama" class="col-sm-2 control-label">Username</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="pengguna_nama" id="pengguna_nama" value="<?php echo $pengguna_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_kunci" class="col-sm-2 control-label">Password</label>
							<div class="col-md-4 col-sm-6">
								<input type="password" class="form-control" name="pengguna_kunci" id="pengguna_kunci" value="" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_nama_depan" class="col-sm-2 control-label">Nama Lengkap</label>
							<div class="col-sm-8">
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-6">
										<input type="text" class="form-control" name="pengguna_nama_depan" id="pengguna_nama_depan" value="<?php echo $pengguna_nama_depan; ?>" placeholder="Nama Depan" required>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<input type="text" class="form-control" name="pengguna_nama_belakang" id="pengguna_nama_belakang" value="<?php echo $pengguna_nama_belakang; ?>" placeholder="Nama Belakang">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-6">
								<textarea class="form-control" name="pengguna_alamat" id="pengguna_alamat" rows="3"><?php echo $pengguna_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="pengguna_telepon" id="pengguna_telepon" value="" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_surel" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4 col-sm-6">
								<input type="email" class="form-control" name="pengguna_surel" id="pengguna_surel" value="" placeholder="example@example.com">
							</div>
						</div>
						<?php if ($this->session->userdata('level') == 1){?>
						<div class="form-group">
							<label for="pengguna_level_id" class="col-sm-2 control-label">Kelompok</label>
							<div class="col-md-4 col-sm-6">
								<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, '', '', 'class="form-control" required');?>
							</div>
						</div>
						<?php } else { ?>
						<div class="form-group">
							<label for="pengguna_level_id" class="col-sm-2 control-label">Kelompok</label>
							<div class="col-md-4 col-sm-6">
								<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level WHERE pengguna_level_id NOT IN (1)")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, '', '', 'class="form-control" required');?>
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label for="program_studi_id" class="col-sm-2 control-label">Program Studi</label>
							<div class="col-md-4 col-sm-6">
								<?php combobox('db', $this->db->query("SELECT * FROM akd_program_studi")->result(), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pengguna
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('pengguna'); ?>">Pengguna</a></li>
            <li class="active">Ubah Pengguna</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Pengguna</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$pengguna_id);?>" method="post">
					<input type="hidden" class="form-control" name="pengguna_id" id="pengguna_id" value="<?php echo $pengguna_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="pengguna_nama" class="col-sm-2 control-label">Username</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="pengguna_nama" id="pengguna_nama" value="<?php echo $pengguna_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_kunci" class="col-sm-2 control-label">Password</label>
							<div class="col-md-4 col-sm-6">
								<input type="password" class="form-control" name="pengguna_kunci" id="pengguna_kunci" value="" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_nama_depan" class="col-sm-2 control-label">Nama Lengkap</label>
							<div class="col-sm-8">
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-6">
										<input type="text" class="form-control" name="pengguna_nama_depan" id="pengguna_nama_depan" value="<?php echo $pengguna_nama_depan; ?>" placeholder="Nama Depan" required>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<input type="text" class="form-control" name="pengguna_nama_belakang" id="pengguna_nama_belakang" value="<?php echo $pengguna_nama_belakang; ?>" placeholder="Nama Belakang">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-6">
								<textarea class="form-control" name="pengguna_alamat" id="pengguna_alamat" rows="3"><?php echo $pengguna_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="pengguna_telepon" id="pengguna_telepon" value="<?php echo $pengguna_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="pengguna_surel" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4 col-sm-6">
								<input type="email" class="form-control" name="pengguna_surel" id="pengguna_surel" value="<?php echo $pengguna_surel; ?>" placeholder="example@example.com">
							</div>
						</div>
						<?php if ($this->session->userdata('level') == 1){?>
						<div class="form-group">
							<label for="pengguna_level_id" class="col-sm-2 control-label">Kelompok</label>
							<div class="col-md-4 col-sm-6">
								<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, '', '', 'class="form-control" required');?>
							</div>
						</div>
						<?php } else { ?>
						<div class="form-group">
							<label for="pengguna_level_id" class="col-sm-2 control-label">Kelompok</label>
							<div class="col-md-4 col-sm-6">
								<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level WHERE pengguna_level_id NOT IN (1)")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, '', '', 'class="form-control" required');?>
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label for="program_studi_id" class="col-sm-2 control-label">Program Studi</label>
							<div class="col-md-4 col-sm-6">
							<?php combobox('db', $this->db->query("SELECT * FROM akd_program_studi")->result(), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pengguna
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('pengguna'); ?>">Pengguna</a></li>
            <li class="active">Detail Pengguna</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Pengguna</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="pengguna_id" id="pengguna_id" value="<?php echo $pengguna_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group row">
							<label for="pengguna_nama" class="col-sm-2 control-label">Username</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $pengguna_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="pengguna_nama_depan" class="col-sm-2 control-label">Nama Lengkap</label>
							<div class="col-sm-8">
								<?php echo $pengguna_nama_depan.' '.$pengguna_nama_belakang; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="pengguna_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-6">
								<?php echo $pengguna_alamat; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="pengguna_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $pengguna_telepon; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="pengguna_surel" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $pengguna_surel; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="pengguna_level_id" class="col-sm-2 control-label">Kelompok</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $pengguna_level_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_id" class="col-sm-2 control-label">Staf</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $staf_nama; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$pengguna_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>