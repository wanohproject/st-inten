<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('menu/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "menu_nama"},
			{ "data": "menu_link" },
			{ "data": "modul_nama" },
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Menu
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('menu'); ?>">Menu</a></li>
            <li class="active">Daftar Menu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Menu</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Link</th>
                        <th>Modul</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Menu
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('menu'); ?>">Menu</a></li>
            <li class="active">Tambah Menu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-header">
                  <h3 class="box-title">Tambah Menu</h3>
                </div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_id" class="col-sm-2 control-label">Pilih Modul</label>
						<div class="col-md-4 col-sm-6">
							<?php combobox('db', $this->db->query("SELECT * FROM modul WHERE modul_status='A' ORDER BY modul_nama ASC")->result(), 'modul_id', 'modul_id', 'modul_nama', $modul_id, 'submit();', '', 'class="form-control"');?>
						</div>
					</div>					
					<div class="form-group">
						<label for="menu_utama" class="col-sm-2 control-label">Menu Utama</label>
						<div class="col-sm-4">
							<?php echo $this->Menu_model->combobox_menu($modul_id, '0', '', 0, 'menu_utama', 'menu_id', 'menu_nama', $menu_utama, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_nama" class="col-sm-2 control-label">Nama Menu</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="menu_nama" id="menu_nama" value="<?php echo $menu_nama; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_siteurl" class="col-sm-2 control-label">Tipe URL</label>
						<div class="col-sm-6">
							<div class="radio">
								<label>
									<input type="radio" name="menu_siteurl" value="A" <?php echo ($menu_siteurl == 'A')?'checked':''; ?> onClick="submit();" required> Site URL
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="menu_siteurl" value="B" <?php echo ($menu_siteurl == 'B')?'checked':''; ?> onClick="submit();" required> Base URL
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="menu_siteurl" value="M" <?php echo ($menu_siteurl == 'M')?'checked':''; ?> onClick="submit();" required> Modul URL
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="menu_siteurl" value="H" <?php echo ($menu_siteurl == 'H')?'checked':''; ?> onClick="submit();" required> Free URL
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_url" class="col-sm-2 control-label">URL</label>
						<div class="col-sm-6">
							<?php if ($menu_siteurl == 'A'){ ?>
							<div class="input-group">
								<span class="input-group-addon"><?php echo site_url();?></span>
								<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							</div>
							<?php } else if ($menu_siteurl == 'B'){ ?>
							<div class="input-group">
								<span class="input-group-addon"><?php echo base_url();?></span>
								<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							</div>
							<?php } else if ($menu_siteurl == 'M'){ 
								$modul = $this->Modul_model->get_modul("modul.*", array('modul_id'=>$modul_id));
								$modul_dir = ($modul)?$modul->modul_dir.'/':'';
								?>
							<div class="input-group">
								<span class="input-group-addon"><?php echo site_url().$modul_dir;?></span>
								<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							</div>
							<?php } else { ?>
							<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_class" class="col-sm-2 control-label">Class</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" name="menu_class" id="menu_class" value="<?php echo $menu_class; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="menu_icon" class="col-sm-2 control-label">Icon</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" name="menu_icon" id="menu_icon" value="<?php echo $menu_icon; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="menu_urutan" class="col-sm-2 control-label">Urutan</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" name="menu_urutan" id="menu_urutan" value="<?php echo $menu_urutan; ?>" min="0" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
			  </form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Menu
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('menu'); ?>">Menu</a></li>
            <li class="active">Ubah Menu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$menu_id);?>" method="post">
				<input type="hidden" class="form-control" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>" placeholder="">
                <div class="box-header">
                  <h3 class="box-title">Ubah Menu</h3>
                </div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_id" class="col-sm-2 control-label">Pilih Modul</label>
						<div class="col-md-4 col-sm-6">
							<?php combobox('db', $this->db->query("SELECT * FROM modul WHERE modul_status='A' ORDER BY modul_nama ASC")->result(), 'modul_id', 'modul_id', 'modul_nama', $modul_id, 'submit();', '', 'class="form-control"');?>
						</div>
					</div>					
					<div class="form-group">
						<label for="menu_utama" class="col-sm-2 control-label">Menu Utama</label>
						<div class="col-sm-4">
							<?php echo $this->Menu_model->combobox_menu($modul_id, '0', '', 0, 'menu_utama', 'menu_id', 'menu_nama', $menu_utama, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_nama" class="col-sm-2 control-label">Nama Menu</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="menu_nama" id="menu_nama" value="<?php echo $menu_nama; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_siteurl" class="col-sm-2 control-label">Tipe URL</label>
						<div class="col-sm-6">
							<div class="radio">
								<label>
									<input type="radio" name="menu_siteurl" value="A" <?php echo ($menu_siteurl == 'A')?'checked':''; ?> onClick="submit();" required> Site URL
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="menu_siteurl" value="B" <?php echo ($menu_siteurl == 'B')?'checked':''; ?> onClick="submit();" required> Base URL
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="menu_siteurl" value="M" <?php echo ($menu_siteurl == 'M')?'checked':''; ?> onClick="submit();" required> Modul URL
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="menu_siteurl" value="H" <?php echo ($menu_siteurl == 'H')?'checked':''; ?> onClick="submit();" required> Free URL
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_url" class="col-sm-2 control-label">URL</label>
						<div class="col-sm-6">
							<?php if ($menu_siteurl == 'A'){ ?>
							<div class="input-group">
								<span class="input-group-addon"><?php echo site_url();?></span>
								<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							</div>
							<?php } else if ($menu_siteurl == 'B'){ ?>
							<div class="input-group">
								<span class="input-group-addon"><?php echo base_url();?></span>
								<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							</div>
							<?php } else if ($menu_siteurl == 'M'){ 
								$modul = $this->Modul_model->get_modul("modul.*", array('modul_id'=>$modul_id));
								$modul_dir = ($modul)?$modul->modul_dir.'/':'';
								?>
							<div class="input-group">
								<span class="input-group-addon"><?php echo site_url().$modul_dir;?></span>
								<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							</div>
							<?php } else { ?>
							<input type="text" class="form-control" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" placeholder="" required>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label for="menu_class" class="col-sm-2 control-label">Class</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" name="menu_class" id="menu_class" value="<?php echo $menu_class; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="menu_icon" class="col-sm-2 control-label">Icon</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" name="menu_icon" id="menu_icon" value="<?php echo $menu_icon; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="menu_urutan" class="col-sm-2 control-label">Urutan</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" name="menu_urutan" id="menu_urutan" value="<?php echo $menu_urutan; ?>" min="0" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Menu
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('menu'); ?>">Menu</a></li>
            <li class="active">Detail Menu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Menu</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="menu_id" id="menu_id" value="<?php echo $menu_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group row">
							<label for="menu_tipe" class="col-sm-2 control-label">Modul</label>
							<div class="col-sm-6">
								<?php echo $modul_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="menu_utama" class="col-sm-2 control-label">Menu Utama</label>
							<div class="col-sm-4">
								<?php echo $menu_utama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="menu_nama" class="col-sm-2 control-label">Nama Menu</label>
							<div class="col-sm-6">
								<?php echo $menu_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="menu_url" class="col-sm-2 control-label">URL</label>
							<div class="col-sm-6">
								<?php 
								if ($menu_siteurl == 'A'){
									echo site_url($menu_url);
								} else if ($menu_siteurl == 'B'){
									echo base_url($menu_url);
								} else if ($menu_siteurl == 'M'){ 
									$modul = $this->Modul_model->get_modul("modul.*", array('modul_id'=>$modul_id));
									$modul_dir = ($modul)?$modul->modul_dir.'/':'';
									echo site_url($modul_dir.$menu_url);
								} else {
									echo $menu_url;
								} 
								?>
							</div>
						</div>
						<div class="form-group row">
							<label for="menu_class" class="col-sm-2 control-label">Class</label>
							<div class="col-sm-3">
								<?php echo $menu_class; ?>
							</div>
						<div class="form-group row">
							<label for="menu_icon" class="col-sm-2 control-label">Icon</label>
							<div class="col-sm-3">
								<?php echo $menu_icon; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="menu_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-sm-2">
								<?php echo $menu_urutan; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$menu_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>