<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>	
$(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('perguruan_tinggi/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "perguruan_tinggi_kode", "width": "100" },
			{ "data": "perguruan_tinggi_nama"},
			{ "data": "perguruan_tinggi_singkatan"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {
		if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
});
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Perguruan Tinggi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Perguruan Tinggi</a></li>
            <li class="active">Daftar Perguruan Tinggi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Perguruan Tinggi</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Singkat</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#perguruan_tinggi_tanggal_sk').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Perguruan Tinggi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Perguruan Tinggi</a></li>
            <li class="active">Tambah Perguruan Tinggi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Perguruan Tinggi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<label for="yayasan_id" class="col-md-2 control-label">Yayasan</label>
							<div class="col-md-6">
								<?php echo combobox('db', $this->Yayasan_model->grid_all_yayasan('', 'yayasan_nama', 'ASC'), 'yayasan_id', 'yayasan_id', 'yayasan_nama', $yayasan_id, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="perguruan_tinggi_kode" id="perguruan_tinggi_kode" value="<?php echo $perguruan_tinggi_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_nama" id="perguruan_tinggi_nama" value="<?php echo $perguruan_tinggi_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_singkatan" class="col-sm-2 control-label">Singkatan</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_singkatan" id="perguruan_tinggi_singkatan" value="<?php echo $perguruan_tinggi_singkatan; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-6">
								<textarea class="form-control" name="perguruan_tinggi_alamat" id="perguruan_tinggi_alamat"><?php echo $perguruan_tinggi_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kota" class="col-sm-2 control-label">Kota</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_kota" id="perguruan_tinggi_kota" value="<?php echo $perguruan_tinggi_kota; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="perguruan_tinggi_kodepos" id="perguruan_tinggi_kodepos" value="<?php echo $perguruan_tinggi_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_telepon" id="perguruan_tinggi_telepon" value="<?php echo $perguruan_tinggi_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_fax" class="col-sm-2 control-label">Faximile</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_fax" id="perguruan_tinggi_fax" value="<?php echo $perguruan_tinggi_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_email" id="perguruan_tinggi_email" value="<?php echo $perguruan_tinggi_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_website" id="perguruan_tinggi_website" value="<?php echo $perguruan_tinggi_website; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_nomor_sk" class="col-sm-2 control-label">Nomor SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_nomor_sk" id="perguruan_tinggi_nomor_sk" value="<?php echo $perguruan_tinggi_nomor_sk; ?>" placeholder="">
							</div>
							<label for="perguruan_tinggi_tanggal_sk" class="col-sm-2 control-label">Tanggal SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_tanggal_sk" id="perguruan_tinggi_tanggal_sk" value="<?php echo $perguruan_tinggi_tanggal_sk; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_aktif" class="col-md-2 control-label">Status Aktif</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(70, 'perguruan_tinggi_aktif', 'kode_value', 'kode_nama', $perguruan_tinggi_aktif, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#perguruan_tinggi_tanggal_sk').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Perguruan Tinggi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Perguruan Tinggi</a></li>
            <li class="active">Ubah Perguruan Tinggi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Perguruan Tinggi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$perguruan_tinggi_id);?>" method="post">
					<input type="hidden" class="form-control" name="perguruan_tinggi_id" id="perguruan_tinggi_id" value="<?php echo $perguruan_tinggi_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="yayasan_id" class="col-md-2 control-label">Yayasan</label>
							<div class="col-md-4">
								<?php echo combobox('db', $this->Yayasan_model->grid_all_yayasan('', 'yayasan_nama', 'ASC'), 'yayasan_id', 'yayasan_id', 'yayasan_nama', $yayasan_id, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="perguruan_tinggi_kode" id="perguruan_tinggi_kode" value="<?php echo $perguruan_tinggi_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_nama" id="perguruan_tinggi_nama" value="<?php echo $perguruan_tinggi_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_singkatan" class="col-sm-2 control-label">Singkatan</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_singkatan" id="perguruan_tinggi_singkatan" value="<?php echo $perguruan_tinggi_singkatan; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-6">
								<textarea class="form-control" name="perguruan_tinggi_alamat" id="perguruan_tinggi_alamat"><?php echo $perguruan_tinggi_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kota" class="col-sm-2 control-label">Kota</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_kota" id="perguruan_tinggi_kota" value="<?php echo $perguruan_tinggi_kota; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="perguruan_tinggi_kodepos" id="perguruan_tinggi_kodepos" value="<?php echo $perguruan_tinggi_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_telepon" id="perguruan_tinggi_telepon" value="<?php echo $perguruan_tinggi_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_fax" class="col-sm-2 control-label">Faximile</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_fax" id="perguruan_tinggi_fax" value="<?php echo $perguruan_tinggi_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_email" id="perguruan_tinggi_email" value="<?php echo $perguruan_tinggi_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_website" id="perguruan_tinggi_website" value="<?php echo $perguruan_tinggi_website; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_nomor_sk" class="col-sm-2 control-label">Nomor SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_nomor_sk" id="perguruan_tinggi_nomor_sk" value="<?php echo $perguruan_tinggi_nomor_sk; ?>" placeholder="">
							</div>
							<label for="perguruan_tinggi_tanggal_sk" class="col-sm-2 control-label">Tanggal SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_tanggal_sk" id="perguruan_tinggi_tanggal_sk" value="<?php echo $perguruan_tinggi_tanggal_sk; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_aktif" class="col-md-2 control-label">Status Aktif</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(70, 'perguruan_tinggi_aktif', 'kode_value', 'kode_nama', $perguruan_tinggi_aktif, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Perguruan Tinggi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Perguruan Tinggi</a></li>
            <li class="active">Detail Perguruan Tinggi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Perguruan Tinggi</h3>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
						<div class="form-group">
							<label for="perguruan_tinggi_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="perguruan_tinggi_kode" id="perguruan_tinggi_kode" value="<?php echo $perguruan_tinggi_kode; ?>" placeholder="" required readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_nama" id="perguruan_tinggi_nama" value="<?php echo $perguruan_tinggi_nama; ?>" placeholder="" required readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-6">
								<textarea class="form-control" name="perguruan_tinggi_alamat" id="perguruan_tinggi_alamat" readonly><?php echo $perguruan_tinggi_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kota" class="col-sm-2 control-label">Kota</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_kota" id="perguruan_tinggi_kota" value="<?php echo $perguruan_tinggi_kota; ?>" placeholder="" required readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="perguruan_tinggi_kodepos" id="perguruan_tinggi_kodepos" value="<?php echo $perguruan_tinggi_kodepos; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_telepon" id="perguruan_tinggi_telepon" value="<?php echo $perguruan_tinggi_telepon; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_fax" class="col-sm-2 control-label">Faximile</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_fax" id="perguruan_tinggi_fax" value="<?php echo $perguruan_tinggi_fax; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_email" id="perguruan_tinggi_email" value="<?php echo $perguruan_tinggi_email; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_website" id="perguruan_tinggi_website" value="<?php echo $perguruan_tinggi_website; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_nomor_sk" class="col-sm-2 control-label">Nomor SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_nomor_sk" id="perguruan_tinggi_nomor_sk" value="<?php echo $perguruan_tinggi_nomor_sk; ?>" placeholder="" readonly>
							</div>
							<label for="perguruan_tinggi_tanggal_sk" class="col-sm-2 control-label">Tanggal SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_tanggal_sk" id="perguruan_tinggi_tanggal_sk" value="<?php echo $perguruan_tinggi_tanggal_sk; ?>" placeholder="" autocomplete="off" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_nomor_bn" class="col-sm-2 control-label">Nomor BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_nomor_bn" id="perguruan_tinggi_nomor_bn" value="<?php echo $perguruan_tinggi_nomor_bn; ?>" placeholder="" readonly>
							</div>
							<label for="perguruan_tinggi_tanggal_bn" class="col-sm-2 control-label">Tanggal BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_tanggal_bn" id="perguruan_tinggi_tanggal_bn" value="<?php echo $perguruan_tinggi_tanggal_bn; ?>" placeholder="" autocomplete="off" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi_aktif" class="col-md-2 control-label">Status Aktif</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="perguruan_tinggi_aktif" id="perguruan_tinggi_aktif" value="<?php echo $this->Referensi_model->get_name('70', $perguruan_tinggi_aktif);?>" placeholder="" readonly>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$perguruan_tinggi_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>