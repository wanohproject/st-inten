<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>	
$(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('yayasan/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "yayasan_kode", "width": "100" },
			{ "data": "yayasan_nama"},
			{ "data": "yayasan_alamat"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {
		if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
});
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Yayasan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Yayasan</a></li>
            <li class="active">Daftar Yayasan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Yayasan</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#yayasan_tanggal_sk, #yayasan_tanggal_bn, #yayasan_tanggal_pendirian').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Yayasan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Yayasan</a></li>
            <li class="active">Tambah Yayasan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Yayasan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<label for="yayasan_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="yayasan_kode" id="yayasan_kode" value="<?php echo $yayasan_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="yayasan_nama" id="yayasan_nama" value="<?php echo $yayasan_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-6">
								<textarea class="form-control" name="yayasan_alamat" id="yayasan_alamat"><?php echo $yayasan_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_kota" class="col-sm-2 control-label">Kota</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="yayasan_kota" id="yayasan_kota" value="<?php echo $yayasan_kota; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="yayasan_kodepos" id="yayasan_kodepos" value="<?php echo $yayasan_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_telepon" id="yayasan_telepon" value="<?php echo $yayasan_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_fax" class="col-sm-2 control-label">Faximile</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_fax" id="yayasan_fax" value="<?php echo $yayasan_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_email" id="yayasan_email" value="<?php echo $yayasan_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_website" id="yayasan_website" value="<?php echo $yayasan_website; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nomor_sk" class="col-sm-2 control-label">Nomor SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_nomor_sk" id="yayasan_nomor_sk" value="<?php echo $yayasan_nomor_sk; ?>" placeholder="">
							</div>
							<label for="yayasan_tanggal_sk" class="col-sm-2 control-label">Tanggal SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_sk" id="yayasan_tanggal_sk" value="<?php echo $yayasan_tanggal_sk; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nomor_bn" class="col-sm-2 control-label">Nomor BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_nomor_bn" id="yayasan_nomor_bn" value="<?php echo $yayasan_nomor_bn; ?>" placeholder="">
							</div>
							<label for="yayasan_tanggal_bn" class="col-sm-2 control-label">Tanggal BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_bn" id="yayasan_tanggal_bn" value="<?php echo $yayasan_tanggal_bn; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_tanggal_pendirian" class="col-sm-2 control-label">Tanggal Pendirian</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_pendirian" id="yayasan_tanggal_pendirian" value="<?php echo $yayasan_tanggal_pendirian; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_aktif" class="col-md-2 control-label">Status Aktif</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(70, 'yayasan_aktif', 'kode_value', 'kode_nama', $yayasan_aktif, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#yayasan_tanggal_sk, #yayasan_tanggal_bn, #yayasan_tanggal_pendirian').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Yayasan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Yayasan</a></li>
            <li class="active">Ubah Yayasan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Yayasan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$yayasan_id);?>" method="post">
					<input type="hidden" class="form-control" name="yayasan_id" id="yayasan_id" value="<?php echo $yayasan_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="yayasan_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="yayasan_kode" id="yayasan_kode" value="<?php echo $yayasan_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="yayasan_nama" id="yayasan_nama" value="<?php echo $yayasan_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-6">
								<textarea class="form-control" name="yayasan_alamat" id="yayasan_alamat"><?php echo $yayasan_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_kota" class="col-sm-2 control-label">Kota</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="yayasan_kota" id="yayasan_kota" value="<?php echo $yayasan_kota; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="yayasan_kodepos" id="yayasan_kodepos" value="<?php echo $yayasan_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_telepon" id="yayasan_telepon" value="<?php echo $yayasan_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_fax" class="col-sm-2 control-label">Faximile</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_fax" id="yayasan_fax" value="<?php echo $yayasan_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_email" id="yayasan_email" value="<?php echo $yayasan_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_website" id="yayasan_website" value="<?php echo $yayasan_website; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nomor_sk" class="col-sm-2 control-label">Nomor SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_nomor_sk" id="yayasan_nomor_sk" value="<?php echo $yayasan_nomor_sk; ?>" placeholder="">
							</div>
							<label for="yayasan_tanggal_sk" class="col-sm-2 control-label">Tanggal SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_sk" id="yayasan_tanggal_sk" value="<?php echo $yayasan_tanggal_sk; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nomor_bn" class="col-sm-2 control-label">Nomor BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_nomor_bn" id="yayasan_nomor_bn" value="<?php echo $yayasan_nomor_bn; ?>" placeholder="">
							</div>
							<label for="yayasan_tanggal_bn" class="col-sm-2 control-label">Tanggal BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_bn" id="yayasan_tanggal_bn" value="<?php echo $yayasan_tanggal_bn; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_tanggal_pendirian" class="col-sm-2 control-label">Tanggal Pendirian</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_pendirian" id="yayasan_tanggal_pendirian" value="<?php echo $yayasan_tanggal_pendirian; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_aktif" class="col-md-2 control-label">Status Aktif</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(70, 'yayasan_aktif', 'kode_value', 'kode_nama', $yayasan_aktif, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Yayasan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Yayasan</a></li>
            <li class="active">Detail Yayasan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Yayasan</h3>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
						<div class="form-group">
							<label for="yayasan_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="yayasan_kode" id="yayasan_kode" value="<?php echo $yayasan_kode; ?>" placeholder="" required readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="yayasan_nama" id="yayasan_nama" value="<?php echo $yayasan_nama; ?>" placeholder="" required readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-6">
								<textarea class="form-control" name="yayasan_alamat" id="yayasan_alamat" readonly><?php echo $yayasan_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_kota" class="col-sm-2 control-label">Kota</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="yayasan_kota" id="yayasan_kota" value="<?php echo $yayasan_kota; ?>" placeholder="" required readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="yayasan_kodepos" id="yayasan_kodepos" value="<?php echo $yayasan_kodepos; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_telepon" id="yayasan_telepon" value="<?php echo $yayasan_telepon; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_fax" class="col-sm-2 control-label">Faximile</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_fax" id="yayasan_fax" value="<?php echo $yayasan_fax; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_email" id="yayasan_email" value="<?php echo $yayasan_email; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_website" id="yayasan_website" value="<?php echo $yayasan_website; ?>" placeholder="" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nomor_sk" class="col-sm-2 control-label">Nomor SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_nomor_sk" id="yayasan_nomor_sk" value="<?php echo $yayasan_nomor_sk; ?>" placeholder="" readonly>
							</div>
							<label for="yayasan_tanggal_sk" class="col-sm-2 control-label">Tanggal SK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_sk" id="yayasan_tanggal_sk" value="<?php echo $yayasan_tanggal_sk; ?>" placeholder="" autocomplete="off" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_nomor_bn" class="col-sm-2 control-label">Nomor BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_nomor_bn" id="yayasan_nomor_bn" value="<?php echo $yayasan_nomor_bn; ?>" placeholder="" readonly>
							</div>
							<label for="yayasan_tanggal_bn" class="col-sm-2 control-label">Tanggal BN</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_bn" id="yayasan_tanggal_bn" value="<?php echo $yayasan_tanggal_bn; ?>" placeholder="" autocomplete="off" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_tanggal_pendirian" class="col-sm-2 control-label">Tanggal Pendirian</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_tanggal_pendirian" id="yayasan_tanggal_pendirian" value="<?php echo $yayasan_tanggal_pendirian; ?>" placeholder="" autocomplete="off" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="yayasan_aktif" class="col-md-2 control-label">Status Aktif</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="yayasan_aktif" id="yayasan_aktif" value="<?php echo $this->Referensi_model->get_name('70', $yayasan_aktif);?>" placeholder="" readonly>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$yayasan_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>