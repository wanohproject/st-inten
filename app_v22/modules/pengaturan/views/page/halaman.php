<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('halaman/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "tulisan_nama"},
			{ "data": "Actions", "width": "100" },
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthHalaman": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthHalaman": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Halaman
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Halaman</a></li>
            <li class="active">Daftar Halaman</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Halaman</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Judul</th>
                        <th>&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<script type="text/javascript">
	function makeSlug(slugcontent)
	{
		// convert to lowercase (important: since on next step special chars are defined in lowercase only)
		slugcontent = slugcontent.toLowerCase();
		// convert special chars
		var   accents={a:/\u00e1/g,e:/u00e9/g,i:/\u00ed/g,o:/\u00f3/g,u:/\u00fa/g,n:/\u00f1/g}
		for (var i in accents) slugcontent = slugcontent.replace(accents[i],i);

		var slugcontent_hyphens = slugcontent.replace(/\s/g,'-');
		var finishedslug = slugcontent_hyphens.replace(/[^a-zA-Z0-9\-]/g,'');
		finishedslug = finishedslug.toLowerCase();
		return finishedslug;		
	}

	$(document).ready(function() {
		$("#tulisan_permalink").change(function() { // when focus out
			$("#message-check").html('checking permalink...'); //before AJAX response
			var form_data = {
				action: 'check_permalink',
				tulisan_permalink: $(this).val()
			};
			$.ajax({
				type: "POST",
				url: "<?php echo module_url($this->uri->segment(2)); ?>/check_permalink",
				data: form_data,
				dataType : 'json',
				success: function(result) {
					if (result.success == true){
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return true');
						document.getElementById('tulisan_permalink').style.borderColor='#abd600';
					} else {
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return false');
						document.getElementById('tulisan_permalink').style.borderColor='#d60000';
						document.getElementById('tulisan_permalink').focus();
					}
				}
			});
		});
		
		$("#tulisan_nama").change(function() { // when focus out
			document.getElementById("tulisan_permalink").value = makeSlug($(this).val());
			
			$("#message-check").html('checking permalink...'); //before AJAX response
			var form_data = {
				action: 'check_permalink',
				tulisan_permalink: document.getElementById("tulisan_permalink").value
			};
			$.ajax({
				type: "POST",
				url: "<?php echo module_url($this->uri->segment(2)); ?>/check_permalink",
				data: form_data,
				dataType : 'json',
				success: function(result) {
					if (result.success == true){
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return true');
						document.getElementById('tulisan_permalink').style.borderColor='#abd600';
					} else {
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return false');
						document.getElementById('tulisan_permalink').style.borderColor='#d60000';
						document.getElementById('tulisan_permalink').focus();
					}
					// $("#message-check").html(result);	
				}
			});
		});
	});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Halaman
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Halaman</a></li>
            <li class="active">Tambah Halaman</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Halaman</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form id="form-input" class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
					<input type="hidden" class="form-control" name="tulisan_id" id="tulisan_id" value="<?php echo $tulisan_id; ?>" placeholder="">
					<div class="box-body">
					
						<div class="form-group">
							<label for="tulisan_nama" class="col-sm-2 control-label">Judul</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="tulisan_nama" id="tulisan_nama" value="<?php echo $tulisan_nama; ?>" placeholder="" required>
							</div>
						</div>
						
						<div class="form-group">
							<label for="tulisan_permalink" class="col-sm-2 control-label">Permalink</label>
							<div class="col-md-8 col-sm-8">
								<div class="input-group">
									<span class="input-group-addon">#</span>
									<input type="text" class="form-control" name="tulisan_permalink" id="tulisan_permalink" value="<?php echo $tulisan_permalink; ?>" placeholder="" required>
								</div>
							</div>
							<div class="col-md-2 col-sm-2">
								<div id='message-check' style="margin-left:10px;width:150px;display:inline-block"></div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="tulisan_deskripsi" class="col-sm-2 control-label">Deskripsi</label>
							<div class="col-md-10 col-sm-10">
								<textarea class="form-control" rows="20" id="tulisan_deskripsi" name="tulisan_deskripsi" ><p>Deskripsi</p></textarea><?php echo $ckeditor;?>
							</div>
						</div>
						
						<div class="form-group">
							<label for="tulisan_thumbnail" class="col-sm-2 control-label">Thumbnail</label>
							<div class="col-md-4 col-sm-6">
								<input type="file" accept="image/jpeg, image/png, image/gif" class="form-control" name="tulisan_thumbnail" id="tulisan_thumbnail" value="<?php echo $tulisan_thumbnail; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<script type="text/javascript">
	function makeSlug(slugcontent)
	{
		// convert to lowercase (important: since on next step special chars are defined in lowercase only)
		slugcontent = slugcontent.toLowerCase();
		// convert special chars
		var   accents={a:/\u00e1/g,e:/u00e9/g,i:/\u00ed/g,o:/\u00f3/g,u:/\u00fa/g,n:/\u00f1/g}
		for (var i in accents) slugcontent = slugcontent.replace(accents[i],i);

		var slugcontent_hyphens = slugcontent.replace(/\s/g,'-');
		var finishedslug = slugcontent_hyphens.replace(/[^a-zA-Z0-9\-]/g,'');
		finishedslug = finishedslug.toLowerCase();
		return finishedslug;		
	}

	$(document).ready(function() {
		$("#tulisan_permalink").change(function() { // when focus out
			$("#message-check").html('checking permalink...'); //before AJAX response
			var form_data = {
				action: 'check_permalink',
				tulisan_permalink: $(this).val(),
				tulisan_id: document.getElementById('tulisan_id').value
			};
			$.ajax({
				type: "POST",
				url: "<?php echo module_url($this->uri->segment(2)); ?>/check_permalink",
				data: form_data,
				dataType : 'json',
				success: function(result) {
					if (result.success == true){
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return true');
						document.getElementById('tulisan_permalink').style.borderColor='#abd600';
					} else {
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return false');
						document.getElementById('tulisan_permalink').style.borderColor='#d60000';
						document.getElementById('tulisan_permalink').focus();
					}
				}
			});
		});
		
		$("#tulisan_nama").change(function() { // when focus out
			document.getElementById("tulisan_permalink").value = makeSlug($(this).val());
			
			$("#message-check").html('checking permalink...'); //before AJAX response
			var form_data = {
				action: 'check_permalink',
				tulisan_permalink: document.getElementById("tulisan_permalink").value,
				tulisan_id: document.getElementById('tulisan_id').value
			};
			$.ajax({
				type: "POST",
				url: "<?php echo module_url($this->uri->segment(2)); ?>/check_permalink",
				data: form_data,
				dataType : 'json',
				success: function(result) {
					if (result.success == true){
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return true');
						document.getElementById('tulisan_permalink').style.borderColor='#abd600';
					} else {
						$("#message-check").html(result.message);
						$("#form-input").attr('onSubmit', 'return false');
						document.getElementById('tulisan_permalink').style.borderColor='#d60000';
						document.getElementById('tulisan_permalink').focus();
					}
					// $("#message-check").html(result);	
				}
			});
		});
	});
</script>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Halaman
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('tulisan'); ?>">Halaman</a></li>
            <li class="active">Ubah Halaman</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Halaman</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$tulisan_id);?>" method="post" enctype="multipart/form-data">
					<input type="hidden" class="form-control" name="tulisan_id" id="tulisan_id" value="<?php echo $tulisan_id; ?>" placeholder="">
					<div class="box-body">
					
						<div class="form-group">
							<label for="tulisan_nama" class="col-sm-2 control-label">Judul</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="tulisan_nama" id="tulisan_nama" value="<?php echo $tulisan_nama; ?>" placeholder="" required>
							</div>
						</div>
						
						<div class="form-group">
							<label for="tulisan_permalink" class="col-sm-2 control-label">Permalink</label>
							<div class="col-md-8 col-sm-8">
								<div class="input-group">
									<span class="input-group-addon">#</span>
									<input type="text" class="form-control" name="tulisan_permalink" id="tulisan_permalink" value="<?php echo $tulisan_permalink; ?>" placeholder="" required>
								</div>
							</div>
							<div class="col-md-2 col-sm-2">
								<div id='message-check' style="margin-left:10px;width:150px;display:inline-block"></div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="tulisan_deskripsi" class="col-sm-2 control-label">Deskripsi</label>
							<div class="col-md-10 col-sm-10">
								<textarea class="form-control" rows="20" id="tulisan_deskripsi" name="tulisan_deskripsi" ><?php echo $tulisan_deskripsi; ?></textarea><?php echo $ckeditor;?>
							</div>
						</div>
						
						<div class="form-group">
							<label for="tulisan_thumbnail" class="col-sm-2 control-label">Thumbnail</label>
							<div class="col-md-4 col-sm-6">
								<input type="file" accept="image/jpeg, image/png, image/gif" class="form-control" name="tulisan_thumbnail" id="tulisan_thumbnail" value="" placeholder="">
							</div>
						</div>
						<?php if ($tulisan_thumbnail){ ?> 
						<div class="form-group">
							<label for="tulisan_thumbnail" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-md-4 col-sm-6">
								<img src="<?php echo base_url('asset/image/artikel/small_'.$tulisan_thumbnail);?>" style="width:300px;"/>
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Halaman
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('tulisan'); ?>">Halaman</a></li>
            <li class="active">Detail Halaman</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Halaman</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="tulisan_id" id="tulisan_id" value="<?php echo $tulisan_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group row">
							<label for="tulisan_nama" class="col-sm-2 control-label">Judul</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $tulisan_nama; ?>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="tulisan_permalink" class="col-sm-2 control-label">Link</label>
							<div class="col-md-8 col-sm-8">
								<?php echo site_url('page/view/'.$tulisan_nama);?>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="tulisan_deskripsi" class="col-sm-2 control-label">Deskripsi</label>
							<div class="col-md-10 col-sm-10">
								<textarea class="form-control" rows="20" id="tulisan_deskripsi" name="tulisan_deskripsi" ><?php echo $tulisan_deskripsi; ?></textarea><?php echo $ckeditor;?>
							</div>
						</div>
						<?php if ($tulisan_thumbnail){ ?> 
						<div class="form-group row">
							<label for="tulisan_thumbnail" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-md-4 col-sm-6">
								<img src="<?php echo base_url('asset/image/artikel/small_'.$tulisan_thumbnail);?>" style="width:300px;"/>
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$tulisan_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>