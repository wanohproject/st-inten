<?php
if ($action == '' || $action == 'hak_akses'){
?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Hak Akses Menu
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('menu'); ?>">Menu</a></li>
            <li class="active">Hak Akses Menu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-header">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-6">Pilih Kelompok Pengguna</label>
							<div class="col-md-6">
								<?php if ($menu_tipe == 'B'){?>
									<?php if ($this->session->userdata('level') == 1){?>
										<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level WHERE pengguna_level_id NOT IN (3)")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, 'submit();', '', 'class="form-control" required');?>
									<?php } else { ?>
										<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level WHERE pengguna_level_id NOT IN (1,3)")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, 'submit();', '', 'class="form-control" required');?>
									<?php } ?>
								<?php } else { ?>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-3">Pilih Modul</label>
							<div class="col-md-6">
								<?php combobox('db', $this->db->query("SELECT * FROM modul ORDER BY modul_nama ASC")->result(), 'modul_id', 'modul_id', 'modul_nama', $modul_id, 'submit();', '', 'class="form-control" required');?>
							</div>
						</div>
					</div>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-body">
						<?php
						$i = 1;
						$where_pengguna['menu_pengguna.menu_id']	= 0;
						$where_pengguna['pengguna_level.pengguna_level_id']	= $pengguna_level_id;
						$lisquery = $this->db->query("SELECT * FROM menu WHERE menu_level='1' AND menu_status='A' AND menu_utama='0' AND menu_tipe='$menu_tipe' AND modul_id='$modul_id' ORDER BY menu_urutan ASC");
						foreach ($lisquery->result() as $list){
							$i++;	
							$where_pengguna2['menu_pengguna.menu_id']				= $list->menu_id;
							$where_pengguna2['pengguna_level.pengguna_level_id']	= $pengguna_level_id;
							$ceklist = ($this->Menu_model->count_all_menu_pengguna($where_pengguna2) < 1) ? '' : 'checked';
							echo '
							<div class="form-group">
								<div class="col-xs-12">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="menu[]" value="'.$list->menu_id.'" '.$ceklist.'> '.$list->menu_nama.'
										</label>
									</div>
								</div>
							</div>';
							$lisquery2 = $this->db->query("SELECT * FROM menu WHERE menu_level='2' AND menu_status='A' AND menu_utama='".$list->menu_id."' AND menu_tipe='$menu_tipe' ORDER BY menu_urutan ASC");
							foreach ($lisquery2->result() as $list2){
								$i++;	
								$where_pengguna3['menu_pengguna.menu_id']				= $list2->menu_id;
								$where_pengguna3['pengguna_level.pengguna_level_id']	= $pengguna_level_id;
								$ceklist = ($this->Menu_model->count_all_menu_pengguna($where_pengguna3) < 1) ? '' : 'checked';
								echo '
								<div class="form-group">
									<div class="col-xs-12">
										<div class="checkbox">
											<label style="padding-left:60px">
												<input type="checkbox" name="menu[]" value="'.$list2->menu_id.'" '.$ceklist.'> '.$list2->menu_nama.'
											</label>
										</div>
									</div>
								</div>';
								$lisquery3 = $this->db->query("SELECT * FROM menu WHERE menu_level='3' AND menu_status='A' AND menu_utama='".$list2->menu_id."' AND menu_tipe='$menu_tipe' ORDER BY menu_urutan ASC");
								foreach ($lisquery3->result() as $list3){
									$i++;	
									$where_pengguna4['menu_pengguna.menu_id']				= $list3->menu_id;
									$where_pengguna4['pengguna_level.pengguna_level_id']	= $pengguna_level_id;
									$ceklist = ($this->Menu_model->count_all_menu_pengguna($where_pengguna4) < 1) ? '' : 'checked';
									echo '
									<div class="form-group">
										<div class="col-xs-2">&nbsp;</div>
										<div class="col-xs-10">
											<div class="checkbox">
												<label style="padding-left:120px">
													<input type="checkbox" name="menu[]" value="'.$list3->menu_id.'" '.$ceklist.'> '.$list3->menu_nama.'
												</label>
											</div>
										</div>
									</div>';
									$lisquery4 = $this->db->query("SELECT * FROM menu WHERE menu_level='4' AND menu_status='A' AND menu_utama='".$list3->menu_id."' AND menu_tipe='$menu_tipe' ORDER BY menu_urutan ASC");
									foreach ($lisquery4->result() as $list4){
										$i++;	
										$where_pengguna5['menu_pengguna.menu_id']				= $list4->menu_id;
										$where_pengguna5['pengguna_level.pengguna_level_id']	= $pengguna_level_id;
										$ceklist = ($this->Menu_model->count_all_menu_pengguna($where_pengguna5) < 1) ? '' : 'checked';
										echo '
										<div class="form-group">
											<div class="col-xs-3">&nbsp;</div>
											<div class="col-xs-9">
												<div class="checkbox">
													<label style="padding-left:180px">
														<input type="checkbox" name="menu[]" value="'.$list4->menu_id.'" '.$ceklist.'> '.$list4->menu_nama.'
													</label>
												</div>
											</div>
										</div>';
									}
								}
							}
						$i++;
						}
						?> 
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'hak_akses2'){ ?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Hak Akses Menu V2
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('menu'); ?>">Menu</a></li>
            <li class="active">Hak Akses Menu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-header">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-6">Pilih Kelompok Pengguna</label>
											<div class="col-md-6">
												<?php if ($menu_tipe == 'B'){?>
													<?php if ($this->session->userdata('level') == 1){?>
														<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level WHERE pengguna_level_id NOT IN (3)")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, 'submit();', '', 'class="form-control"');?>
													<?php } else { ?>
														<?php combobox('db', $this->db->query("SELECT * FROM pengguna_level WHERE pengguna_level_id NOT IN (1,3)")->result(), 'pengguna_level_id', 'pengguna_level_id', 'pengguna_level_nama', $pengguna_level_id, 'submit();', '', 'class="form-control"');?>
													<?php } ?>
												<?php } else { ?>
												<?php } ?>
											</div>
										</div>
									</div>
                </div><!-- /.box-header -->
                <div class="box-body" style="overflow-x:scroll">
									<?php echo $this->Menu_model->table_menu('0', '0', '', 0, $pengguna_level_id);?>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
								</div><!-- /.box-footer -->
											</div><!-- /.box-body -->
							</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>