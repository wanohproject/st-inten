<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('.set-datepicker').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm'
    });
  });
</script>
<?php
if ($action == '' || $action == 'index'){
?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Identitas Aplikasi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="active">Identitas Aplikasi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		  <form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Identitas Aplikasi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="profil_id" id="profil_id" value="<?php echo $profil_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="profil_website" class="col-sm-2 control-label">Nama Aplikasi</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="profil_website" id="profil_website" value="<?php echo $profil_website; ?>" placeholder="">
							</div>
						</div>
						<?php if ($profil_favicon){?>
						<div class="form-group">
							<label for="profil_favicon" class="col-sm-2 control-label">Favicon</label>
							<div class="col-sm-4">
								<img src="<?php echo base_url('asset/profil/'.$profil_favicon);?>" style="width:30px;"/>
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label for="profil_favicon" class="col-sm-2 control-label">Upload Favicon</label>
							<div class="col-sm-4">
								<input type="file" class="form-control" name="profil_favicon" id="profil_favicon">
							</div>
						</div>
						<?php if ($profil_logo){?>
						<div class="form-group">
							<label for="profil_logo" class="col-sm-2 control-label">Logo Sekolah</label>
							<div class="col-sm-4">
								<img src="<?php echo base_url('asset/profil/'.$profil_logo);?>" style="width:100px;"/>
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label for="profil_logo" class="col-sm-2 control-label">Upload Logo Sekolah</label>
							<div class="col-sm-4">
								<input type="file" class="form-control" name="profil_logo" id="profil_favicon">
							</div>
						</div>
						<?php if ($profil_background_login){?>
						<div class="form-group">
							<label for="profil_background_login" class="col-sm-2 control-label">Background Login</label>
							<div class="col-sm-4">
								<img src="<?php echo base_url('asset/profil/'.$profil_background_login);?>" style="width:300px;"/>
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label for="profil_background_login" class="col-sm-2 control-label">Upload Background Login</label>
							<div class="col-sm-4">
								<input type="file" class="form-control" name="profil_background_login" id="profil_favicon">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Identitas Sekolah</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="profil_institusi" class="col-sm-2 control-label">Nama</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="profil_institusi" id="profil_institusi" value="<?php echo $profil_institusi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="profil_singkatan" class="col-sm-2 control-label">Nama Singkatan</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="profil_singkatan" id="profil_singkatan" value="<?php echo $profil_singkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="profil_kontak" class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="profil_kontak" id="profil_kontak" rows="3"><?php echo $profil_kontak; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="profil_telp" class="col-sm-2 control-label">Telepon</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="profil_telp" id="profil_telp" value="<?php echo $profil_telp; ?>" placeholder="">
							</div>
							<label for="profil_fax" class="col-sm-2 control-label">Fax.</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="profil_fax" id="profil_fax" value="<?php echo $profil_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="profil_email" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-3">
								<input type="email" class="form-control" name="profil_email" id="profil_email" value="<?php echo $profil_email; ?>" placeholder="">
							</div>
							<label for="profil_situs" class="col-sm-2 control-label">Situs</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="profil_situs" id="profil_situs" value="<?php echo $profil_situs; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Tampilan</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="profil_institusi" class="col-sm-2 control-label">Nama</label>
							<div class="col-sm-8">
							<ul class="list-unstyled clearfix">
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-blue")?"checked":""; ?> value="skin-blue"></div>
										<a href="javascript:void(0)" for="profil_warna_skin_blue" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span>
														<span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;">Blue</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-black")?"checked":""; ?> value="skin-black"></div>
										<a href="javascript:void(0)" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
														<span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span>
														<span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #222"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;">Black</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-purple")?"checked":""; ?> value="skin-purple"></div>
										<a href="javascript:void(0)" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span>
														<span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;">Purple</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-green")?"checked":""; ?> value="skin-green"></div>
										<a href="javascript:void(0)" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span>
														<span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;">Green</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-red")?"checked":""; ?> value="skin-red"></div>
										<a href="javascript:void(0)" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span>
														<span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;">Red</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-yellow")?"checked":""; ?> value="skin-yellow"></div>
										<a href="javascript:void(0)" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span>
														<span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;">Yellow</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-blue-light")?"checked":""; ?> value="skin-blue-light"></div>
										<a href="javascript:void(0)" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)"
												class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span>
														<span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;" style="font-size: 12px">Blue Light</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-black-light")?"checked":""; ?> value="skin-black-light"></div>
										<a href="javascript:void(0)" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)"
												class="clearfix full-opacity-hover">
												<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
														<span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span>
														<span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;" style="font-size: 12px">Black Light</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-purple-light")?"checked":""; ?> value="skin-purple-light"></div>
										<a href="javascript:void(0)" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)"
												class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span>
														<span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;" style="font-size: 12px">Purple Light</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-green-light")?"checked":""; ?> value="skin-green-light"></div>
										<a href="javascript:void(0)" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)"
												class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span>
														<span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;" style="font-size: 12px">Green Light</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-red-light")?"checked":""; ?> value="skin-red-light"></div>
										<a href="javascript:void(0)" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span>
														<span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;" style="font-size: 12px">Red Light</p>
								</li>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="profil_warna" <?php echo ($profil_warna == "skin-yellow-light")?"checked":""; ?> value="skin-yellow-light"></div>
										<a href="javascript:void(0)" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)"
												class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span>
														<span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
												</div>
												<div>
														<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span>
														<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span>
												</div>
										</a>
										<p class="text-center no-margin" style="height:45px;" style="font-size: 12px">Yellow Light</p>
								</li>
							</ul>
							</div>
						</div>
						<div class="form-group">
							<label for="profil_boxed" class="col-sm-2 control-label">Layout Boxed</label>
							<div class="col-sm-6">
								<div class="radio">
									<label>
										<input type="radio" name="profil_boxed" value="Y" <?php echo ($profil_boxed == 'Y')?'checked':''; ?> required> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="profil_boxed" value="N" <?php echo ($profil_boxed == 'N')?'checked':''; ?> required> Tidak
									</label>
								</div>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Pengaturan Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if (settings_get_label('siswa_biodata_update_active')){ ?>
						<div class="form-group">
							<label for="settings_siswa_biodata_update_active" class="col-sm-2 control-label"><?php echo settings_get_label('siswa_biodata_update_active'); ?></label>
							<div class="col-sm-8">
								<div class="radio">
									<label>
										<input type="radio" name="settings[siswa_biodata_update_active]" value="Y" <?php echo (settings_get_value('siswa_biodata_update_active') == 'Y')?'checked':''; ?> required> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="settings[siswa_biodata_update_active]" value="N" <?php echo (settings_get_value('siswa_biodata_update_active') == 'N')?'checked':''; ?> required> Tidak
									</label>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if (settings_get_label('siswa_biodata_update_deadline')){ ?>
						<div class="form-group">
							<label for="settings_siswa_biodata_update_deadline" class="col-sm-2 control-label"><?php echo settings_get_label('siswa_biodata_update_deadline'); ?></label>
							<div class="col-sm-8">
								<input type="text" class="form-control set-datepicker" name="settings[siswa_biodata_update_deadline]" id="settings_siswa_biodata_update_deadline" value="<?php echo settings_get_value('siswa_biodata_update_deadline'); ?>" placeholder="">
							</div>
						</div>
						<?php } ?>
						<?php if (settings_get_label('staf_biodata_update_active')){ ?>
						<div class="form-group">
							<label for="settings_staf_biodata_update_active" class="col-sm-2 control-label"><?php echo settings_get_label('staf_biodata_update_active'); ?></label>
							<div class="col-sm-8">
								<div class="radio">
									<label>
										<input type="radio" name="settings[staf_biodata_update_active]" value="Y" <?php echo (settings_get_value('staf_biodata_update_active') == 'Y')?'checked':''; ?> required> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="settings[staf_biodata_update_active]" value="N" <?php echo (settings_get_value('staf_biodata_update_active') == 'N')?'checked':''; ?> required> Tidak
									</label>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if (settings_get_label('staf_biodata_update_deadline')){ ?>
						<div class="form-group">
							<label for="settings_staf_biodata_update_deadline" class="col-sm-2 control-label"><?php echo settings_get_label('staf_biodata_update_deadline'); ?></label>
							<div class="col-sm-8">
								<input type="text" class="form-control set-datepicker" name="settings[staf_biodata_update_deadline]" id="settings_staf_biodata_update_deadline" value="<?php echo settings_get_value('staf_biodata_update_deadline'); ?>" placeholder="">
							</div>
						</div>
						<?php } ?>
						<?php if (settings_get_label('rencana_studi_active')){ ?>
						<div class="form-group">
							<label for="settings_rencana_studi_active" class="col-sm-2 control-label"><?php echo settings_get_label('rencana_studi_active'); ?></label>
							<div class="col-sm-8">
								<div class="radio">
									<label>
										<input type="radio" name="settings[rencana_studi_active]" value="RS" <?php echo (settings_get_value('rencana_studi_active') == 'RS')?'checked':''; ?> required> Perwalian
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="settings[rencana_studi_active]" value="PRS" <?php echo (settings_get_value('rencana_studi_active') == 'PRS')?'checked':''; ?> required> PRS
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="settings[rencana_studi_active]" value="N" <?php echo (settings_get_value('rencana_studi_active') == 'N')?'checked':''; ?> required> Tidak Aktif
									</label>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if (settings_get_label('input_nilai_active')){ ?>
						<div class="form-group">
							<label for="settings_input_nilai_active" class="col-sm-2 control-label"><?php echo settings_get_label('input_nilai_active'); ?></label>
							<div class="col-sm-8">
								<div class="radio">
									<label>
										<input type="radio" name="settings[input_nilai_active]" value="Y" <?php echo (settings_get_value('input_nilai_active') == 'Y')?'checked':''; ?> required> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="settings[input_nilai_active]" value="N" <?php echo (settings_get_value('input_nilai_active') == 'N')?'checked':''; ?> required> Tidak
									</label>
								</div>
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		  </form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>