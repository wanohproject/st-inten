<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('modul/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "modul_nama"},
			{ "data": "modul_link" },
			{ "data": "modul_tipe" },
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthModul": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthModul": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Modul
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('modul'); ?>">Modul</a></li>
            <li class="active">Daftar Modul</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Modul</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Link</th>
                        <th>Tipe</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Modul
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('modul'); ?>">Modul</a></li>
            <li class="active">Tambah Modul</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-header">
                  <h3 class="box-title">Tambah Modul</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="form-group">
						<label for="modul_tipe" class="col-sm-2 control-label">Pilih Modul</label>
						<div class="col-sm-6">
							<div class="radio">
								<label>
									<input type="radio" name="modul_tipe" value="F" <?php echo $tipe_f = ($modul_tipe == 'F')?'checked':''; ?> required> Frontend
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="modul_tipe" value="B" <?php echo $tipe_b = ($modul_tipe == 'B')?'checked':''; ?> required> Backend
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="modul_nama" class="col-sm-2 control-label">Nama</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="modul_nama" id="modul_nama" value="<?php echo $modul_nama; ?>" placeholder="" required>
						</div>
					</div>						
					<div class="form-group">
						<label for="modul_dir" class="col-sm-2 control-label">Direktori</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="modul_dir" id="modul_dir" value="<?php echo $modul_dir; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="modul_url" class="col-sm-2 control-label">URL</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="modul_url" id="modul_url" value="<?php echo $modul_url; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="modul_icon" class="col-sm-2 control-label">Icon</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" name="modul_icon" id="modul_icon" value="<?php echo $modul_icon; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="modul_urutan" class="col-sm-2 control-label">Urutan</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" name="modul_urutan" id="modul_urutan" value="<?php echo $modul_urutan; ?>" min="0" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="modul_warna" class="col-sm-2 control-label">Warna</label>
						<div class="col-sm-10">
							<ul class="list-unstyled clearfix">
							<?php
							foreach ($list_warna as $key => $val) {
								?>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="modul_warna" <?php echo ($modul_warna == $key)?"checked":""; ?> value="<?php echo $key; ?>"></div>
										<a href="javascript:void(0)" data-skin="<?php echo $key; ?>" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 100%; float: left; height: 40px; background: #f4f5f7" class="<?php echo $key; ?>"></span>
												</div>
										</a>
										<p class="text-center no-margin"><?php echo $val; ?></p>
								</li>
								<?php
							}
							?>
							</ul>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Modul
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('modul'); ?>">Modul</a></li>
            <li class="active">Ubah Modul</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$modul_id);?>" method="post">
				<input type="hidden" class="form-control" name="modul_id" id="modul_id" value="<?php echo $modul_id; ?>" placeholder="">
                <div class="box-header">
                  <h3 class="box-title">Ubah Modul</h3>
                </div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="modul_tipe" class="col-sm-2 control-label">Pilih Modul</label>
						<div class="col-sm-6">
							<div class="radio">
								<label>
									<input type="radio" name="modul_tipe" value="F" <?php echo $tipe_f = ($modul_tipe == 'F')?'checked':''; ?> required> Frontend
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="modul_tipe" value="B" <?php echo $tipe_b = ($modul_tipe == 'B')?'checked':''; ?> required> Backend
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="modul_nama" class="col-sm-2 control-label">Nama</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="modul_nama" id="modul_nama" value="<?php echo $modul_nama; ?>" placeholder="" required>
						</div>
					</div>						
					<div class="form-group">
						<label for="modul_dir" class="col-sm-2 control-label">Direktori</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="modul_dir" id="modul_dir" value="<?php echo $modul_dir; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="modul_url" class="col-sm-2 control-label">URL</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="modul_url" id="modul_url" value="<?php echo $modul_url; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="modul_icon" class="col-sm-2 control-label">Icon</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" name="modul_icon" id="modul_icon" value="<?php echo $modul_icon; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="modul_urutan" class="col-sm-2 control-label">Urutan</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" name="modul_urutan" id="modul_urutan" value="<?php echo $modul_urutan; ?>" min="0" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="modul_warna" class="col-sm-2 control-label">Warna</label>
						<div class="col-sm-10">
							<ul class="list-unstyled clearfix">
							<?php
							foreach ($list_warna as $key => $val) {
								?>
								<li style="float:left; width: 60px; padding: 5px;">
										<div class="text-center"><input type="radio" name="modul_warna" <?php echo ($modul_warna == $key)?"checked":""; ?> value="<?php echo $key; ?>"></div>
										<a href="javascript:void(0)" data-skin="<?php echo $key; ?>" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">
												<div>
														<span style="display:block; width: 100%; float: left; height: 40px; background: #f4f5f7" class="<?php echo $key; ?>"></span>
												</div>
										</a>
										<p class="text-center no-margin"><?php echo $val; ?></p>
								</li>
								<?php
							}
							?>
							</ul>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Modul
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('modul'); ?>">Modul</a></li>
            <li class="active">Detail Modul</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Modul</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="modul_id" id="modul_id" value="<?php echo $modul_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group row">
							<label for="modul_tipe" class="col-sm-2 control-label">Modul</label>
							<div class="col-sm-6">
								<?php echo $modul_utama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="modul_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-sm-6">
								<?php echo $modul_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="modul_dir" class="col-sm-2 control-label">Direktori</label>
							<div class="col-sm-6">
								<?php echo $modul_dir; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="modul_url" class="col-sm-2 control-label">URL</label>
							<div class="col-sm-6">
								<?php echo $modul_url; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="modul_icon" class="col-sm-2 control-label">Icon</label>
							<div class="col-sm-3">
								<?php echo $modul_icon; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="modul_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-sm-2">
								<?php echo $modul_urutan; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$modul_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>