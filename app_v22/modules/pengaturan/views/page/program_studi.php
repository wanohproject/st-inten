<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>	
$(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('program_studi/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "program_studi_kode", "width": "100" },
			{ "data": "program_studi_nama"},
			{ "data": "program_studi_singkatan"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {
		if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
});
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Program Studi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Program Studi</a></li>
            <li class="active">Daftar Program Studi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Program Studi</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Singkat</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#program_studi_tanggal_sk_dikti, #program_studi_akhir_sk_dikti, #program_studi_tanggal_pendirian, #program_studi_tanggal_sk_akreditasi, #program_studi_akhir_sk_akreditasi').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Program Studi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Program Studi</a></li>
            <li class="active">Tambah Program Studi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Program Studi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<label for="perguruan_tinggi_id" class="col-md-2 control-label">Perguruan Tinggi</label>
							<div class="col-md-6">
								<?php echo combobox('db', $this->Perguruan_tinggi_model->grid_all_perguruan_tinggi('', 'perguruan_tinggi_nama', 'ASC'), 'perguruan_tinggi_id', 'perguruan_tinggi_id', 'perguruan_tinggi_nama', $perguruan_tinggi_id, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="jenjang_kode" class="col-md-2 control-label">Jenjang</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(4, 'jenjang_kode', 'kode_value', 'kode_nama', $jenjang_kode, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="program_studi_kode" id="program_studi_kode" value="<?php echo $program_studi_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="program_studi_nama" id="program_studi_nama" value="<?php echo $program_studi_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_singkatan" class="col-sm-2 control-label">Singkatan</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="program_studi_singkatan" id="program_studi_singkatan" value="<?php echo $program_studi_singkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_awal_lapor" class="col-sm-2 control-label">Semester Awal Lapor</label>
							<div class="col-md-6">
								<input type="text" maxlength="5" class="form-control" name="program_studi_awal_lapor" id="program_studi_awal_lapor" value="<?php echo $program_studi_awal_lapor; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nomor_sk_dikti" class="col-sm-2 control-label">Nomor SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nomor_sk_dikti" id="program_studi_nomor_sk_dikti" value="<?php echo $program_studi_nomor_sk_dikti; ?>" placeholder="">
							</div>
							<label for="program_studi_tanggal_sk_dikti" class="col-sm-2 control-label">Tanggal SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_sk_dikti" id="program_studi_tanggal_sk_dikti" value="<?php echo $program_studi_tanggal_sk_dikti; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_akhir_sk_dikti" class="col-sm-2 control-label">Tanggal Akhir SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_akhir_sk_dikti" id="program_studi_akhir_sk_dikti" value="<?php echo $program_studi_akhir_sk_dikti; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_sks" class="col-sm-2 control-label">Jumlah SKS</label>
							<div class="col-md-3">
								<input type="text" maxlength="3" class="form-control" name="program_studi_sks" id="program_studi_sks" value="<?php echo $program_studi_sks; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="status_program_studi_kode" class="col-md-2 control-label">Status Program Studi</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(14, 'status_program_studi_kode', 'kode_value', 'kode_nama', $status_program_studi_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_mulai_semester" class="col-sm-2 control-label">Tahun Semester Mulai</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="program_studi_mulai_semester" id="program_studi_mulai_semester" value="<?php echo $program_studi_mulai_semester; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_email" id="program_studi_email" value="<?php echo $program_studi_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_website" id="program_studi_website" value="<?php echo $program_studi_website; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_tanggal_pendirian" class="col-sm-2 control-label">Tanggal Pendirian</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_pendirian" id="program_studi_tanggal_pendirian" value="<?php echo $program_studi_tanggal_pendirian; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nomor_sk_akreditasi" class="col-sm-2 control-label">Nomor SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nomor_sk_akreditasi" id="program_studi_nomor_sk_akreditasi" value="<?php echo $program_studi_nomor_sk_akreditasi; ?>" placeholder="">
							</div>
							<label for="program_studi_tanggal_sk_akreditasi" class="col-sm-2 control-label">Tanggal SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_sk_akreditasi" id="program_studi_tanggal_sk_akreditasi" value="<?php echo $program_studi_tanggal_sk_akreditasi; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_akhir_sk_akreditasi" class="col-sm-2 control-label">Tanggal Akhir SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_akhir_sk_akreditasi" id="program_studi_akhir_sk_akreditasi" value="<?php echo $program_studi_akhir_sk_akreditasi; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="status_akreditasi_kode" class="col-md-2 control-label">Kode Status Akreditasi</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(7, 'status_akreditasi_kode', 'kode_value', 'kode_nama', $status_akreditasi_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="frekuensi_pemutakhiran_kode" class="col-md-2 control-label">Frekuensi Pemutakhiran Kurikulum</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(29, 'frekuensi_pemutakhiran_kode', 'kode_value', 'kode_nama', $frekuensi_pemutakhiran_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelaksanaan_pemutakhiran_kode" class="col-md-2 control-label">Pelaksanaan Pemutakhiran Kurikulum</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(30, 'pelaksanaan_pemutakhiran_kode', 'kode_value', 'kode_nama', $pelaksanaan_pemutakhiran_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nidn_ketua" class="col-sm-2 control-label">NIDN Ketua</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nidn_ketua" id="program_studi_nidn_ketua" value="<?php echo $program_studi_nidn_ketua; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon_ketua" class="col-sm-2 control-label">Telepon/HP Ketua</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon_ketua" id="program_studi_telepon_ketua" value="<?php echo $program_studi_telepon_ketua; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon" id="program_studi_telepon" value="<?php echo $program_studi_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_fax" class="col-sm-2 control-label">Fax</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_fax" id="program_studi_fax" value="<?php echo $program_studi_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nama_operator" class="col-sm-2 control-label">Nama Operator</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nama_operator" id="program_studi_nama_operator" value="<?php echo $program_studi_nama_operator; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon_operator" class="col-sm-2 control-label">Telepon/HP Operator</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon_operator" id="program_studi_telepon_operator" value="<?php echo $program_studi_telepon_operator; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
	<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#program_studi_tanggal_sk_dikti, #program_studi_akhir_sk_dikti, #program_studi_tanggal_pendirian, #program_studi_tanggal_sk_akreditasi, #program_studi_akhir_sk_akreditasi').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Program Studi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Program Studi</a></li>
            <li class="active">Ubah Program Studi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Program Studi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$program_studi_id);?>" method="post">
					<input type="hidden" class="form-control" name="program_studi_id" id="program_studi_id" value="<?php echo $program_studi_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="perguruan_tinggi_id" class="col-md-2 control-label">Perguruan Tinggi</label>
							<div class="col-md-6">
								<?php echo combobox('db', $this->Perguruan_tinggi_model->grid_all_perguruan_tinggi('', 'perguruan_tinggi_nama', 'ASC'), 'perguruan_tinggi_id', 'perguruan_tinggi_id', 'perguruan_tinggi_nama', $perguruan_tinggi_id, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="jenjang_kode" class="col-md-2 control-label">Jenjang</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(4, 'jenjang_kode', 'kode_value', 'kode_nama', $jenjang_kode, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="program_studi_kode" id="program_studi_kode" value="<?php echo $program_studi_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="program_studi_nama" id="program_studi_nama" value="<?php echo $program_studi_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_singkatan" class="col-sm-2 control-label">Singkatan</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="program_studi_singkatan" id="program_studi_singkatan" value="<?php echo $program_studi_singkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_awal_lapor" class="col-sm-2 control-label">Semester Awal Lapor</label>
							<div class="col-md-6">
								<input type="text" maxlength="5" class="form-control" name="program_studi_awal_lapor" id="program_studi_awal_lapor" value="<?php echo $program_studi_awal_lapor; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nomor_sk_dikti" class="col-sm-2 control-label">Nomor SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nomor_sk_dikti" id="program_studi_nomor_sk_dikti" value="<?php echo $program_studi_nomor_sk_dikti; ?>" placeholder="">
							</div>
							<label for="program_studi_tanggal_sk_dikti" class="col-sm-2 control-label">Tanggal SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_sk_dikti" id="program_studi_tanggal_sk_dikti" value="<?php echo $program_studi_tanggal_sk_dikti; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_akhir_sk_dikti" class="col-sm-2 control-label">Tanggal Akhir SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_akhir_sk_dikti" id="program_studi_akhir_sk_dikti" value="<?php echo $program_studi_akhir_sk_dikti; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_sks" class="col-sm-2 control-label">Jumlah SKS</label>
							<div class="col-md-3">
								<input type="text" maxlength="3" class="form-control" name="program_studi_sks" id="program_studi_sks" value="<?php echo $program_studi_sks; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="status_program_studi_kode" class="col-md-2 control-label">Status Program Studi</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(14, 'status_program_studi_kode', 'kode_value', 'kode_nama', $status_program_studi_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_mulai_semester" class="col-sm-2 control-label">Tahun Semester Mulai</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="program_studi_mulai_semester" id="program_studi_mulai_semester" value="<?php echo $program_studi_mulai_semester; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_email" id="program_studi_email" value="<?php echo $program_studi_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_website" id="program_studi_website" value="<?php echo $program_studi_website; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_tanggal_pendirian" class="col-sm-2 control-label">Tanggal Pendirian</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_pendirian" id="program_studi_tanggal_pendirian" value="<?php echo $program_studi_tanggal_pendirian; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nomor_sk_akreditasi" class="col-sm-2 control-label">Nomor SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nomor_sk_akreditasi" id="program_studi_nomor_sk_akreditasi" value="<?php echo $program_studi_nomor_sk_akreditasi; ?>" placeholder="">
							</div>
							<label for="program_studi_tanggal_sk_akreditasi" class="col-sm-2 control-label">Tanggal SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_sk_akreditasi" id="program_studi_tanggal_sk_akreditasi" value="<?php echo $program_studi_tanggal_sk_akreditasi; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_akhir_sk_akreditasi" class="col-sm-2 control-label">Tanggal Akhir SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_akhir_sk_akreditasi" id="program_studi_akhir_sk_akreditasi" value="<?php echo $program_studi_akhir_sk_akreditasi; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="status_akreditasi_kode" class="col-md-2 control-label">Kode Status Akreditasi</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(7, 'status_akreditasi_kode', 'kode_value', 'kode_nama', $status_akreditasi_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="frekuensi_pemutakhiran_kode" class="col-md-2 control-label">Frekuensi Pemutakhiran Kurikulum</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(29, 'frekuensi_pemutakhiran_kode', 'kode_value', 'kode_nama', $frekuensi_pemutakhiran_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelaksanaan_pemutakhiran_kode" class="col-md-2 control-label">Pelaksanaan Pemutakhiran Kurikulum</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(30, 'pelaksanaan_pemutakhiran_kode', 'kode_value', 'kode_nama', $pelaksanaan_pemutakhiran_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nidn_ketua" class="col-sm-2 control-label">NIDN Ketua</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nidn_ketua" id="program_studi_nidn_ketua" value="<?php echo $program_studi_nidn_ketua; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon_ketua" class="col-sm-2 control-label">Telepon/HP Ketua</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon_ketua" id="program_studi_telepon_ketua" value="<?php echo $program_studi_telepon_ketua; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon" id="program_studi_telepon" value="<?php echo $program_studi_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_fax" class="col-sm-2 control-label">Fax</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_fax" id="program_studi_fax" value="<?php echo $program_studi_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nama_operator" class="col-sm-2 control-label">Nama Operator</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nama_operator" id="program_studi_nama_operator" value="<?php echo $program_studi_nama_operator; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon_operator" class="col-sm-2 control-label">Telepon/HP Operator</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon_operator" id="program_studi_telepon_operator" value="<?php echo $program_studi_telepon_operator; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Program Studi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Program Studi</a></li>
            <li class="active">Detail Program Studi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Program Studi</h3>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
						<div class="form-group">
							<label for="perguruan_tinggi_id" class="col-md-2 control-label">Perguruan Tinggi</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="perguruan_tinggi_nama" id="perguruan_tinggi_nama" value="<?php echo $perguruan_tinggi_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="jenjang_kode" class="col-md-2 control-label">Jenjang</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(4, 'jenjang_kode', 'kode_value', 'kode_nama', $jenjang_kode, '', '', 'class="form-control select2" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-3">
								<input type="text" class="form-control" name="program_studi_kode" id="program_studi_kode" value="<?php echo $program_studi_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="program_studi_nama" id="program_studi_nama" value="<?php echo $program_studi_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_singkatan" class="col-sm-2 control-label">Singkatan</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="program_studi_singkatan" id="program_studi_singkatan" value="<?php echo $program_studi_singkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_awal_lapor" class="col-sm-2 control-label">Semester Awal Lapor</label>
							<div class="col-md-6">
								<input type="text" maxlength="5" class="form-control" name="program_studi_awal_lapor" id="program_studi_awal_lapor" value="<?php echo $program_studi_awal_lapor; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nomor_sk_dikti" class="col-sm-2 control-label">Nomor SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nomor_sk_dikti" id="program_studi_nomor_sk_dikti" value="<?php echo $program_studi_nomor_sk_dikti; ?>" placeholder="">
							</div>
							<label for="program_studi_tanggal_sk_dikti" class="col-sm-2 control-label">Tanggal SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_sk_dikti" id="program_studi_tanggal_sk_dikti" value="<?php echo $program_studi_tanggal_sk_dikti; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_akhir_sk_dikti" class="col-sm-2 control-label">Tanggal Akhir SK Dikti</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_akhir_sk_dikti" id="program_studi_akhir_sk_dikti" value="<?php echo $program_studi_akhir_sk_dikti; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_sks" class="col-sm-2 control-label">Jumlah SKS</label>
							<div class="col-md-3">
								<input type="text" maxlength="3" class="form-control" name="program_studi_sks" id="program_studi_sks" value="<?php echo $program_studi_sks; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="status_program_studi_kode" class="col-md-2 control-label">Status Program Studi</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(14, 'status_program_studi_kode', 'kode_value', 'kode_nama', $status_program_studi_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_mulai_semester" class="col-sm-2 control-label">Tahun Semester Mulai</label>
							<div class="col-md-3">
								<input type="text" maxlength="5" class="form-control" name="program_studi_mulai_semester" id="program_studi_mulai_semester" value="<?php echo $program_studi_mulai_semester; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_email" id="program_studi_email" value="<?php echo $program_studi_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_website" class="col-sm-2 control-label">Website</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_website" id="program_studi_website" value="<?php echo $program_studi_website; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_tanggal_pendirian" class="col-sm-2 control-label">Tanggal Pendirian</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_pendirian" id="program_studi_tanggal_pendirian" value="<?php echo $program_studi_tanggal_pendirian; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nomor_sk_akreditasi" class="col-sm-2 control-label">Nomor SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nomor_sk_akreditasi" id="program_studi_nomor_sk_akreditasi" value="<?php echo $program_studi_nomor_sk_akreditasi; ?>" placeholder="">
							</div>
							<label for="program_studi_tanggal_sk_akreditasi" class="col-sm-2 control-label">Tanggal SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_tanggal_sk_akreditasi" id="program_studi_tanggal_sk_akreditasi" value="<?php echo $program_studi_tanggal_sk_akreditasi; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_akhir_sk_akreditasi" class="col-sm-2 control-label">Tanggal Akhir SK Akreditasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_akhir_sk_akreditasi" id="program_studi_akhir_sk_akreditasi" value="<?php echo $program_studi_akhir_sk_akreditasi; ?>" placeholder="" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label for="status_akreditasi_kode" class="col-md-2 control-label">Kode Status Akreditasi</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(7, 'status_akreditasi_kode', 'kode_value', 'kode_nama', $status_akreditasi_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="frekuensi_pemutakhiran_kode" class="col-md-2 control-label">Frekuensi Pemutakhiran Kurikulum</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(29, 'frekuensi_pemutakhiran_kode', 'kode_value', 'kode_nama', $frekuensi_pemutakhiran_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelaksanaan_pemutakhiran_kode" class="col-md-2 control-label">Pelaksanaan Pemutakhiran Kurikulum</label>
							<div class="col-md-4">
								<?php echo $this->Referensi_model->combobox(30, 'pelaksanaan_pemutakhiran_kode', 'kode_value', 'kode_nama', $pelaksanaan_pemutakhiran_kode, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nidn_ketua" class="col-sm-2 control-label">NIDN Ketua</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nidn_ketua" id="program_studi_nidn_ketua" value="<?php echo $program_studi_nidn_ketua; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon_ketua" class="col-sm-2 control-label">Telepon/HP Ketua</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon_ketua" id="program_studi_telepon_ketua" value="<?php echo $program_studi_telepon_ketua; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon" id="program_studi_telepon" value="<?php echo $program_studi_telepon; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_fax" class="col-sm-2 control-label">Fax</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_fax" id="program_studi_fax" value="<?php echo $program_studi_fax; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_nama_operator" class="col-sm-2 control-label">Nama Operator</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_nama_operator" id="program_studi_nama_operator" value="<?php echo $program_studi_nama_operator; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="program_studi_telepon_operator" class="col-sm-2 control-label">Telepon/HP Operator</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="program_studi_telepon_operator" id="program_studi_telepon_operator" value="<?php echo $program_studi_telepon_operator; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$program_studi_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>