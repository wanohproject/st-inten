 <!-- Left side column. contains the logo and sidebar -->
 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU UTAMA</li>
			<?php
			$module_name = $this->router->fetch_module();
			$class_name = (!is_numeric($active))?$active:$this->router->fetch_class();

			$count_comment	= 0;
			$new_comment 	= "";
			$query 			= $this->db->query("SELECT menu.* FROM menu_pengguna LEFT JOIN menu ON menu_pengguna.menu_id=menu.menu_id LEFT JOIN modul ON menu.modul_id=modul.modul_id WHERE menu_pengguna.pengguna_level_id='".$this->session->userdata('level')."' AND menu.menu_level='1' AND menu.menu_status='A' AND modul.modul_dir='$module_name' ORDER BY menu.menu_urutan ASC");
			foreach ($query->result() as $row){
				if($row->menu_siteurl == "A"){
					$url = site_url($row->menu_url);
				} else if($row->menu_siteurl == "B"){
					$url = base_url($row->menu_url);
				} else if($row->menu_siteurl == "M"){
					$url = module_url($row->menu_url);
				} else {
					$url = $row->menu_url;
				}
				$menu_icon = ($row->menu_icon)?$row->menu_icon:'fa fa-arrow-right';
				$subquery 	= $this->db->query("SELECT menu.* FROM menu_pengguna LEFT JOIN menu ON menu_pengguna.menu_id=menu.menu_id WHERE menu_pengguna.pengguna_level_id='".$this->session->userdata('level')."' AND menu.menu_level='2' AND menu.menu_utama='".$row->menu_id."' AND menu.menu_status='A' ORDER BY menu.menu_urutan ASC");
				$activemain = $this->db->query("SELECT menu.* FROM menu WHERE menu_class='".$class_name."' AND menu_utama='".$row->menu_id."' AND menu.menu_status='A'");
				if ($subquery->num_rows() > 0){
					if ($activemain->num_rows() > 0){
						echo "<li class=\"treeview active\">";
							echo "<a href=\"".$url."\"><i class=\"".$menu_icon."\"></i> <span>".$row->menu_nama."</span> <i class=\"fa fa-angle-left pull-right\"></i></a>";
							echo "<ul class=\"treeview-menu\">";
								foreach ($subquery->result() as $row2){
									if($row2->menu_siteurl == "A"){
										$url2 = site_url($row2->menu_url);
									} else if($row2->menu_siteurl == "B"){
										$url2 = base_url($row2->menu_url);
									} else if($row2->menu_siteurl == "M"){
										$url2 = module_url($row2->menu_url);
									} else {
										$url2 = $row2->menu_url;
									}
									$menu_icon2 = ($row2->menu_icon)?$row2->menu_icon:'fa fa-circle-o';
									if ($row2->menu_class == $class_name){
										echo "<li class=\"active\"><a href=\"".$url2."\"><i class=\"".$menu_icon2."\"></i> ".$row2->menu_nama."</a></li>";
									} else {
										echo "<li><a href=\"".$url2."\"><i class=\"".$menu_icon2."\"></i> ".$row2->menu_nama."</a></li>";
									}
								}
							echo "</ul>";
						echo "</li>";
					} else {
						echo "<li class=\"treeview\">";
							echo "<a href=\"".$url."\"><i class=\"".$menu_icon."\"></i> <span>".$row->menu_nama."</span> <i class=\"fa fa-angle-left pull-right\"></i></a>";
							echo "<ul class=\"treeview-menu\">";
								foreach ($subquery->result() as $row2){
									if($row2->menu_siteurl == "A"){
										$url2 = site_url($row2->menu_url);
									} else if($row2->menu_siteurl == "B"){
										$url2 = base_url($row2->menu_url);
									} else if($row2->menu_siteurl == "M"){
										$url2 = module_url($row2->menu_url);
									} else {
										$url2 = $row2->menu_url;
									}
									$menu_icon2 = ($row2->menu_icon)?$row2->menu_icon:'fa fa-circle-o';
									if ($row2->menu_class == $class_name){
										echo "<li class=\"active\"><a href=\"".$url2."\"><i class=\"".$menu_icon2."\"></i> ".$row2->menu_nama."</a></li>";
									} else {
										echo "<li><a href=\"".$url2."\"><i class=\"".$menu_icon2."\"></i> ".$row2->menu_nama."</a></li>";
									}
								}
							echo "</ul>";
						echo "</li>";
					}
				} else {
					if ($row->menu_class == $class_name){
						echo "<li class=\"active\"><a href=\"".$url."\"><i class=\"".$menu_icon."\"></i> <span>".$row->menu_nama."</span></a></li>";
					} else {
						echo "<li><a href=\"".$url."\"><i class=\"".$menu_icon."\"></i> <span>".$row->menu_nama."</span></a></li>";
					}
				}
			}
			?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>