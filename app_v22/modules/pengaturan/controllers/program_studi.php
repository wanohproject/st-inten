<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Program_studi extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Program Studi | ' . profile('profil_website');
		$this->active_menu		= 333;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Referensi_model');
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('program_studi_id, program_studi_kode, program_studi_nama, program_studi_singkatan')
		->add_column('Actions', $this->get_buttons('$1'),'program_studi_id')
		->search_column('program_studi_kode, program_studi_nama, program_studi_singkatan')
		->from('akd_program_studi program_studi');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/program_studi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['program_studi_id']						= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):'';
		$data['perguruan_tinggi_id']					= ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):'';
		$data['jenjang_kode']							= ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):'';
		$data['status_program_studi_kode']				= ($this->input->post('status_program_studi_kode'))?$this->input->post('status_program_studi_kode'):'';
		$data['status_akreditasi_kode']					= ($this->input->post('status_akreditasi_kode'))?$this->input->post('status_akreditasi_kode'):'';
		$data['frekuensi_pemutakhiran_kode']			= ($this->input->post('frekuensi_pemutakhiran_kode'))?$this->input->post('frekuensi_pemutakhiran_kode'):'';
		$data['pelaksanaan_pemutakhiran_kode']			= ($this->input->post('pelaksanaan_pemutakhiran_kode'))?$this->input->post('pelaksanaan_pemutakhiran_kode'):'';
		$data['program_studi_kode']						= ($this->input->post('program_studi_kode'))?$this->input->post('program_studi_kode'):'';
		$data['program_studi_nama']						= ($this->input->post('program_studi_nama'))?$this->input->post('program_studi_nama'):'';
		$data['program_studi_singkatan']				= ($this->input->post('program_studi_singkatan'))?$this->input->post('program_studi_singkatan'):'';
		$data['program_studi_awal_lapor']				= ($this->input->post('program_studi_awal_lapor'))?$this->input->post('program_studi_awal_lapor'):'';
		$data['program_studi_nomor_sk_dikti']			= ($this->input->post('program_studi_nomor_sk_dikti'))?$this->input->post('program_studi_nomor_sk_dikti'):'';
		$data['program_studi_tanggal_sk_dikti']			= ($this->input->post('program_studi_tanggal_sk_dikti'))?$this->input->post('program_studi_tanggal_sk_dikti'):'';
		$data['program_studi_akhir_sk_dikti']			= ($this->input->post('program_studi_akhir_sk_dikti'))?$this->input->post('program_studi_akhir_sk_dikti'):'';
		$data['program_studi_sks']						= ($this->input->post('program_studi_sks'))?$this->input->post('program_studi_sks'):'';
		$data['program_studi_mulai_semester']			= ($this->input->post('program_studi_mulai_semester'))?$this->input->post('program_studi_mulai_semester'):'';
		$data['program_studi_telepon']					= ($this->input->post('program_studi_telepon'))?$this->input->post('program_studi_telepon'):'';
		$data['program_studi_email']					= ($this->input->post('program_studi_email'))?$this->input->post('program_studi_email'):'';
		$data['program_studi_website']					= ($this->input->post('program_studi_website'))?$this->input->post('program_studi_website'):'';
		$data['program_studi_tanggal_pendirian']		= ($this->input->post('program_studi_tanggal_pendirian'))?$this->input->post('program_studi_tanggal_pendirian'):'';
		$data['program_studi_nomor_sk_akreditasi']		= ($this->input->post('program_studi_nomor_sk_akreditasi'))?$this->input->post('program_studi_nomor_sk_akreditasi'):'';
		$data['program_studi_tanggal_sk_akreditasi']	= ($this->input->post('program_studi_tanggal_sk_akreditasi'))?$this->input->post('program_studi_tanggal_sk_akreditasi'):'';
		$data['program_studi_akhir_sk_akreditasi']		= ($this->input->post('program_studi_akhir_sk_akreditasi'))?$this->input->post('program_studi_akhir_sk_akreditasi'):'';
		$data['program_studi_nidn_ketua']				= ($this->input->post('program_studi_nidn_ketua'))?$this->input->post('program_studi_nidn_ketua'):'';
		$data['program_studi_telepon_ketua']			= ($this->input->post('program_studi_telepon_ketua'))?$this->input->post('program_studi_telepon_ketua'):'';
		$data['program_studi_fax']						= ($this->input->post('program_studi_fax'))?$this->input->post('program_studi_fax'):'';
		$data['program_studi_nama_operator']			= ($this->input->post('program_studi_nama_operator'))?$this->input->post('program_studi_nama_operator'):'';
		$data['program_studi_telepon_operator']			= ($this->input->post('program_studi_telepon_operator'))?$this->input->post('program_studi_telepon_operator'):'';
		$save											= $this->input->post('save');
		if ($save == 'save'){
			$insert['program_studi_id']							= $this->uuid->v4();
			$insert['perguruan_tinggi_id']						= validasi_input($this->input->post('perguruan_tinggi_id'));
			$insert['jenjang_kode']								= validasi_input($this->input->post('jenjang_kode'));
			$insert['status_program_studi_kode']				= validasi_input($this->input->post('status_program_studi_kode'));
			$insert['status_akreditasi_kode']					= validasi_input($this->input->post('status_akreditasi_kode'));
			$insert['frekuensi_pemutakhiran_kode']				= validasi_input($this->input->post('frekuensi_pemutakhiran_kode'));
			$insert['pelaksanaan_pemutakhiran_kode']			= validasi_input($this->input->post('pelaksanaan_pemutakhiran_kode'));
			$insert['program_studi_kode']						= validasi_input($this->input->post('program_studi_kode'));
			$insert['program_studi_nama']						= validasi_input($this->input->post('program_studi_nama'));
			$insert['program_studi_singkatan']					= validasi_input($this->input->post('program_studi_singkatan'));
			$insert['program_studi_awal_lapor']					= validasi_input($this->input->post('program_studi_awal_lapor'));
			$insert['program_studi_nomor_sk_dikti']				= validasi_input($this->input->post('program_studi_nomor_sk_dikti'));
			$insert['program_studi_tanggal_sk_dikti']			= validasi_input($this->input->post('program_studi_tanggal_sk_dikti'));
			$insert['program_studi_akhir_sk_dikti']				= validasi_input($this->input->post('program_studi_akhir_sk_dikti'));
			$insert['program_studi_sks']						= validasi_input($this->input->post('program_studi_sks'));
			$insert['program_studi_mulai_semester']				= validasi_input($this->input->post('program_studi_mulai_semester'));
			$insert['program_studi_telepon']					= validasi_input($this->input->post('program_studi_telepon'));
			$insert['program_studi_email']						= validasi_input($this->input->post('program_studi_email'));
			$insert['program_studi_website']					= validasi_input($this->input->post('program_studi_website'));
			$insert['program_studi_tanggal_pendirian']			= validasi_input($this->input->post('program_studi_tanggal_pendirian'));
			$insert['program_studi_nomor_sk_akreditasi']		= validasi_input($this->input->post('program_studi_nomor_sk_akreditasi'));
			$insert['program_studi_tanggal_sk_akreditasi']		= validasi_input($this->input->post('program_studi_tanggal_sk_akreditasi'));
			$insert['program_studi_akhir_sk_akreditasi']		= validasi_input($this->input->post('program_studi_akhir_sk_akreditasi'));
			$insert['program_studi_nidn_ketua']					= validasi_input($this->input->post('program_studi_nidn_ketua'));
			$insert['program_studi_telepon_ketua']				= validasi_input($this->input->post('program_studi_telepon_ketua'));
			$insert['program_studi_fax']						= validasi_input($this->input->post('program_studi_fax'));
			$insert['program_studi_nama_operator']				= validasi_input($this->input->post('program_studi_nama_operator'));
			$insert['program_studi_telepon_operator']			= validasi_input($this->input->post('program_studi_telepon_operator'));
			$insert['created_by']								= userdata('pengguna_id');
			$this->Program_studi_model->insert_program_studi($insert);
			
			$this->session->set_flashdata('success','Program Studi telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/program_studi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['program_studi_id']		= validasi_sql($this->uri->segment(4)); 
		$program_studi 					= $this->Program_studi_model->get_program_studi('*', $where);

		$data['program_studi_id']						= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi->program_studi_id;
		$data['perguruan_tinggi_id']					= ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$program_studi->perguruan_tinggi_id;
		$data['jenjang_kode']							= ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):$program_studi->jenjang_kode;
		$data['status_program_studi_kode']				= ($this->input->post('status_program_studi_kode'))?$this->input->post('status_program_studi_kode'):$program_studi->status_program_studi_kode;
		$data['status_akreditasi_kode']					= ($this->input->post('status_akreditasi_kode'))?$this->input->post('status_akreditasi_kode'):$program_studi->status_akreditasi_kode;
		$data['frekuensi_pemutakhiran_kode']			= ($this->input->post('frekuensi_pemutakhiran_kode'))?$this->input->post('frekuensi_pemutakhiran_kode'):$program_studi->frekuensi_pemutakhiran_kode;
		$data['pelaksanaan_pemutakhiran_kode']			= ($this->input->post('pelaksanaan_pemutakhiran_kode'))?$this->input->post('pelaksanaan_pemutakhiran_kode'):$program_studi->pelaksanaan_pemutakhiran_kode;
		$data['program_studi_kode']						= ($this->input->post('program_studi_kode'))?$this->input->post('program_studi_kode'):$program_studi->program_studi_kode;
		$data['program_studi_nama']						= ($this->input->post('program_studi_nama'))?$this->input->post('program_studi_nama'):$program_studi->program_studi_nama;
		$data['program_studi_singkatan']				= ($this->input->post('program_studi_singkatan'))?$this->input->post('program_studi_singkatan'):$program_studi->program_studi_singkatan;
		$data['program_studi_awal_lapor']				= ($this->input->post('program_studi_awal_lapor'))?$this->input->post('program_studi_awal_lapor'):$program_studi->program_studi_awal_lapor;
		$data['program_studi_nomor_sk_dikti']			= ($this->input->post('program_studi_nomor_sk_dikti'))?$this->input->post('program_studi_nomor_sk_dikti'):$program_studi->program_studi_nomor_sk_dikti;
		$data['program_studi_tanggal_sk_dikti']			= ($this->input->post('program_studi_tanggal_sk_dikti'))?$this->input->post('program_studi_tanggal_sk_dikti'):$program_studi->program_studi_tanggal_sk_dikti;
		$data['program_studi_akhir_sk_dikti']			= ($this->input->post('program_studi_akhir_sk_dikti'))?$this->input->post('program_studi_akhir_sk_dikti'):$program_studi->program_studi_akhir_sk_dikti;
		$data['program_studi_sks']						= ($this->input->post('program_studi_sks'))?$this->input->post('program_studi_sks'):$program_studi->program_studi_sks;
		$data['program_studi_mulai_semester']			= ($this->input->post('program_studi_mulai_semester'))?$this->input->post('program_studi_mulai_semester'):$program_studi->program_studi_mulai_semester;
		$data['program_studi_telepon']					= ($this->input->post('program_studi_telepon'))?$this->input->post('program_studi_telepon'):$program_studi->program_studi_telepon;
		$data['program_studi_email']					= ($this->input->post('program_studi_email'))?$this->input->post('program_studi_email'):$program_studi->program_studi_email;
		$data['program_studi_website']					= ($this->input->post('program_studi_website'))?$this->input->post('program_studi_website'):$program_studi->program_studi_website;
		$data['program_studi_tanggal_pendirian']		= ($this->input->post('program_studi_tanggal_pendirian'))?$this->input->post('program_studi_tanggal_pendirian'):$program_studi->program_studi_tanggal_pendirian;
		$data['program_studi_nomor_sk_akreditasi']		= ($this->input->post('program_studi_nomor_sk_akreditasi'))?$this->input->post('program_studi_nomor_sk_akreditasi'):$program_studi->program_studi_nomor_sk_akreditasi;
		$data['program_studi_tanggal_sk_akreditasi']	= ($this->input->post('program_studi_tanggal_sk_akreditasi'))?$this->input->post('program_studi_tanggal_sk_akreditasi'):$program_studi->program_studi_tanggal_sk_akreditasi;
		$data['program_studi_akhir_sk_akreditasi']		= ($this->input->post('program_studi_akhir_sk_akreditasi'))?$this->input->post('program_studi_akhir_sk_akreditasi'):$program_studi->program_studi_akhir_sk_akreditasi;
		$data['program_studi_nidn_ketua']				= ($this->input->post('program_studi_nidn_ketua'))?$this->input->post('program_studi_nidn_ketua'):$program_studi->program_studi_nidn_ketua;
		$data['program_studi_telepon_ketua']			= ($this->input->post('program_studi_telepon_ketua'))?$this->input->post('program_studi_telepon_ketua'):$program_studi->program_studi_telepon_ketua;
		$data['program_studi_fax']						= ($this->input->post('program_studi_fax'))?$this->input->post('program_studi_fax'):$program_studi->program_studi_fax;
		$data['program_studi_nama_operator']			= ($this->input->post('program_studi_nama_operator'))?$this->input->post('program_studi_nama_operator'):$program_studi->program_studi_nama_operator;
		$data['program_studi_telepon_operator']			= ($this->input->post('program_studi_telepon_operator'))?$this->input->post('program_studi_telepon_operator'):$program_studi->program_studi_telepon_operator;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$where_update['program_studi_id']				= $this->input->post('program_studi_id');
			$update['perguruan_tinggi_id']					= validasi_input($this->input->post('perguruan_tinggi_id'));
			$update['jenjang_kode']							= validasi_input($this->input->post('jenjang_kode'));
			$update['status_program_studi_kode']			= validasi_input($this->input->post('status_program_studi_kode'));
			$update['status_akreditasi_kode']				= validasi_input($this->input->post('status_akreditasi_kode'));
			$update['frekuensi_pemutakhiran_kode']			= validasi_input($this->input->post('frekuensi_pemutakhiran_kode'));
			$update['pelaksanaan_pemutakhiran_kode']		= validasi_input($this->input->post('pelaksanaan_pemutakhiran_kode'));
			$update['program_studi_kode']					= validasi_input($this->input->post('program_studi_kode'));
			$update['program_studi_nama']					= validasi_input($this->input->post('program_studi_nama'));
			$update['program_studi_singkatan']				= validasi_input($this->input->post('program_studi_singkatan'));
			$update['program_studi_awal_lapor']				= validasi_input($this->input->post('program_studi_awal_lapor'));
			$update['program_studi_nomor_sk_dikti']			= validasi_input($this->input->post('program_studi_nomor_sk_dikti'));
			$update['program_studi_tanggal_sk_dikti']		= validasi_input($this->input->post('program_studi_tanggal_sk_dikti'));
			$update['program_studi_akhir_sk_dikti']			= validasi_input($this->input->post('program_studi_akhir_sk_dikti'));
			$update['program_studi_sks']					= validasi_input($this->input->post('program_studi_sks'));
			$update['program_studi_mulai_semester']			= validasi_input($this->input->post('program_studi_mulai_semester'));
			$update['program_studi_telepon']				= validasi_input($this->input->post('program_studi_telepon'));
			$update['program_studi_email']					= validasi_input($this->input->post('program_studi_email'));
			$update['program_studi_website']				= validasi_input($this->input->post('program_studi_website'));
			$update['program_studi_tanggal_pendirian']		= validasi_input($this->input->post('program_studi_tanggal_pendirian'));
			$update['program_studi_nomor_sk_akreditasi']	= validasi_input($this->input->post('program_studi_nomor_sk_akreditasi'));
			$update['program_studi_tanggal_sk_akreditasi']	= validasi_input($this->input->post('program_studi_tanggal_sk_akreditasi'));
			$update['program_studi_akhir_sk_akreditasi']	= validasi_input($this->input->post('program_studi_akhir_sk_akreditasi'));
			$update['program_studi_nidn_ketua']				= validasi_input($this->input->post('program_studi_nidn_ketua'));
			$update['program_studi_telepon_ketua']			= validasi_input($this->input->post('program_studi_telepon_ketua'));
			$update['program_studi_fax']					= validasi_input($this->input->post('program_studi_fax'));
			$update['program_studi_nama_operator']			= validasi_input($this->input->post('program_studi_nama_operator'));
			$update['program_studi_telepon_operator']		= validasi_input($this->input->post('program_studi_telepon_operator'));
			$update['updated_by']							= userdata('pengguna_id');
			$this->Program_studi_model->update_program_studi($where_update, $update);
			
			$this->session->set_flashdata('success','Program Studi telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/program_studi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['program_studi_id']	= validasi_sql($this->uri->segment(4));
		$this->Program_studi_model->delete_program_studi($where_delete);
		
		$this->session->set_flashdata('success','Program Studi telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['program_studi_id']	= validasi_sql($this->uri->segment(4)); 
		$program_studi		= $this->Program_studi_model->get_program_studi('*', $where);
		
		$data['program_studi_id']						= $program_studi->program_studi_id;
		$data['perguruan_tinggi_id']					= $program_studi->perguruan_tinggi_id;
		$data['perguruan_tinggi_nama']					= $program_studi->perguruan_tinggi_nama;
		$data['jenjang_kode']							= $program_studi->jenjang_kode;
		$data['status_program_studi_kode']				= $program_studi->status_program_studi_kode;
		$data['status_akreditasi_kode']					= $program_studi->status_akreditasi_kode;
		$data['frekuensi_pemutakhiran_kode']			= $program_studi->frekuensi_pemutakhiran_kode;
		$data['pelaksanaan_pemutakhiran_kode']			= $program_studi->pelaksanaan_pemutakhiran_kode;
		$data['program_studi_kode']						= $program_studi->program_studi_kode;
		$data['program_studi_nama']						= $program_studi->program_studi_nama;
		$data['program_studi_singkatan']				= $program_studi->program_studi_singkatan;
		$data['program_studi_awal_lapor']				= $program_studi->program_studi_awal_lapor;
		$data['program_studi_nomor_sk_dikti']			= $program_studi->program_studi_nomor_sk_dikti;
		$data['program_studi_tanggal_sk_dikti']			= $program_studi->program_studi_tanggal_sk_dikti;
		$data['program_studi_akhir_sk_dikti']			= $program_studi->program_studi_akhir_sk_dikti;
		$data['program_studi_sks']						= $program_studi->program_studi_sks;
		$data['program_studi_mulai_semester']			= $program_studi->program_studi_mulai_semester;
		$data['program_studi_telepon']					= $program_studi->program_studi_telepon;
		$data['program_studi_email']					= $program_studi->program_studi_email;
		$data['program_studi_website']					= $program_studi->program_studi_website;
		$data['program_studi_tanggal_pendirian']		= $program_studi->program_studi_tanggal_pendirian;
		$data['program_studi_nomor_sk_akreditasi']		= $program_studi->program_studi_nomor_sk_akreditasi;
		$data['program_studi_tanggal_sk_akreditasi']	= $program_studi->program_studi_tanggal_sk_akreditasi;
		$data['program_studi_akhir_sk_akreditasi']		= $program_studi->program_studi_akhir_sk_akreditasi;
		$data['program_studi_nidn_ketua']				= $program_studi->program_studi_nidn_ketua;
		$data['program_studi_telepon_ketua']			= $program_studi->program_studi_telepon_ketua;
		$data['program_studi_fax']						= $program_studi->program_studi_fax;
		$data['program_studi_nama_operator']			= $program_studi->program_studi_nama_operator;
		$data['program_studi_telepon_operator']			= $program_studi->program_studi_telepon_operator;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/program_studi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
