<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Menu | ' . profile('profil_website');;
		$this->active_menu		= 23;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Menu_model');
		$this->load->model('Modul_model');
    }
	
	public function datatable()
    {
		$this->Datatable_model->set_table("(SELECT menu.*, modul_nama, modul_dir FROM menu LEFT JOIN modul ON menu.modul_id=modul.modul_id) menu");
		$this->Datatable_model->set_column_order(array('menu_nama', 'menu_url', 'modul_nama', null));
		$this->Datatable_model->set_column_search(array('menu_nama', 'menu_url', 'modul_nama'));
		$this->Datatable_model->set_order(array('staf_user', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
			$row['menu_nama'] = $record->menu_nama;
			if($record->menu_siteurl == "A"){
				$row['menu_link'] = site_url($record->menu_url);
			} else if($record->menu_siteurl == "B"){
				$row['menu_link'] = base_url($record->menu_url);
			} else if($record->menu_siteurl == "M"){
				$row['menu_link'] = site_url($record->modul_dir.'/'.$record->menu_url);
			} else {
				$row['menu_link'] = $record->menu_url;
			}
            $row['modul_nama'] = $record->modul_nama;
            $row['Actions'] = $this->get_buttons($record->menu_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/menu', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['modul_id']		= ($this->input->post('modul_id'))?$this->input->post('modul_id'):'';
		$modul 					= $this->Modul_model->get_modul("modul_tipe", array('modul_id'=>$data['modul_id']));
		$data['menu_tipe']		= ($modul)?$modul->modul_tipe:'';
		$data['menu_nama']		= ($this->input->post('menu_nama'))?$this->input->post('menu_nama'):'';
		$data['menu_url']		= ($this->input->post('menu_url'))?$this->input->post('menu_url'):'';
		$data['menu_siteurl']	= ($this->input->post('menu_siteurl'))?$this->input->post('menu_siteurl'):'A';
		$data['menu_icon']		= ($this->input->post('menu_icon'))?$this->input->post('menu_icon'):'';
		$data['menu_level']		= ($this->input->post('menu_level'))?$this->input->post('menu_level'):'1';
		$data['menu_utama']		= ($this->input->post('menu_utama'))?$this->input->post('menu_utama'):'0';				
		$data['menu_urutan']	= ($this->input->post('menu_urutan'))?$this->input->post('menu_urutan'):'';
		$data['menu_class']		= ($this->input->post('menu_class'))?$this->input->post('menu_class'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$insert['menu_tipe']		= $data['menu_tipe'];
			$insert['modul_id']			= validasi_sql($this->input->post('modul_id'));
			$insert['menu_nama']		= validasi_sql($this->input->post('menu_nama'));
			$insert['menu_url']			= validasi_sql($this->input->post('menu_url'));
			$insert['menu_siteurl']		= validasi_sql($this->input->post('menu_siteurl'));
			$insert['menu_icon']		= validasi_sql($this->input->post('menu_icon'));
			if ($this->input->post('menu_utama') == 0){
				$insert['menu_level']	= 1;
			} else {
				$level = $this->Menu_model->get_menu("", array('menu_id'=>validasi_sql($this->input->post('menu_utama'))));
				$insert['menu_level']	= $level->menu_level + 1;
			}
			$insert['menu_utama']		= validasi_sql($this->input->post('menu_utama'));
			$insert['menu_urutan']		= validasi_sql($this->input->post('menu_urutan'));					
			$insert['menu_class']		= validasi_sql($this->input->post('menu_class'));					
			$insert['menu_status']		= 'A';
			$this->Menu_model->insert_menu($insert);
			
			$this->session->set_flashdata('success','Menu berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/menu', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['menu_id']		= validasi_sql($this->uri->segment(4)); 
		$menu 					= $this->Menu_model->get_menu('*', $where);
		
		$data['menu_id']		= ($this->input->post('menu_id'))?$this->input->post('menu_id'):$menu->menu_id;
		$data['modul_id']		= ($this->input->post('modul_id'))?$this->input->post('modul_id'):$menu->modul_id;
		$modul 					= $this->Modul_model->get_modul("modul_tipe", array('modul_id'=>$data['modul_id']));
		$data['menu_tipe']		= ($modul)?$modul->modul_tipe:'';
		$data['menu_nama']		= ($this->input->post('menu_nama'))?$this->input->post('menu_nama'):$menu->menu_nama;
		$data['menu_url']		= ($this->input->post('menu_url'))?$this->input->post('menu_url'):$menu->menu_url;
		$data['menu_siteurl']	= ($this->input->post('menu_siteurl'))?$this->input->post('menu_siteurl'):$menu->menu_siteurl;
		$data['menu_icon']		= ($this->input->post('menu_icon'))?$this->input->post('menu_icon'):$menu->menu_icon;
		$data['menu_utama']		= ($this->input->post('menu_utama'))?$this->input->post('menu_utama'):$menu->menu_utama;
		$data['menu_level']		= ($this->input->post('menu_level'))?$this->input->post('menu_level'):$menu->menu_level;
		$data['menu_urutan']	= ($this->input->post('menu_urutan'))?$this->input->post('menu_urutan'):$menu->menu_urutan;
		$data['menu_class']		= ($this->input->post('menu_class'))?$this->input->post('menu_class'):$menu->menu_class;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['menu_id']	= validasi_sql($this->input->post('menu_id'));
			$edit['menu_tipe']		= $data['menu_tipe'];
			$edit['modul_id']		= validasi_sql($this->input->post('modul_id'));
			$edit['menu_nama']		= validasi_sql($this->input->post('menu_nama'));
			$edit['menu_url']		= validasi_sql($this->input->post('menu_url'));
			$edit['menu_siteurl']	= validasi_sql($this->input->post('menu_siteurl'));
			$edit['menu_icon']		= validasi_sql($this->input->post('menu_icon'));
			
			if ($this->input->post('menu_utama') == 0){
				$edit['menu_level']		= 1;
			} else {
				$level = $this->Menu_model->get_menu("", array('menu_id'=>validasi_sql($this->input->post('menu_utama'))));
				$edit['menu_level']		= $level->menu_level + 1;
			}
			$edit['menu_utama']		= validasi_sql($this->input->post('menu_utama'));
			$edit['menu_urutan']	= validasi_sql($this->input->post('menu_urutan'));
			$edit['menu_class']		= validasi_sql($this->input->post('menu_class'));
			$this->Menu_model->update_menu($where_edit, $edit);
			
			$this->session->set_flashdata('success','Menu berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/menu', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['menu_id']	= validasi_sql($this->uri->segment(4));
		if ($this->Menu_model->count_all_menu_pengguna(array('menu.menu_id'=>validasi_sql($this->uri->segment(4)))) < 1){
			$this->Menu_model->delete_menu($where_delete);
			$this->session->set_flashdata('success','Menu berhasil dihapus.');
		} else {
			$this->session->set_flashdata('error','Menu gagal dihapus. Menu digunakan hak akses.');
		}
		
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['menu_id']	= validasi_sql($this->uri->segment(4)); 
		$menu		 		= $this->Menu_model->get_menu('*', $where);
		
		$data['menu_id']		= $menu->menu_id;
		$data['menu_tipe']		= $menu->menu_tipe;
		$data['menu_nama']		= $menu->menu_nama;
		$data['menu_url']		= $menu->menu_url;
		$data['menu_siteurl']	= $menu->menu_siteurl;
		$data['menu_icon']		= $menu->menu_icon;
		$data['menu_utama']		= $menu->menu_utama;
		$data['menu_level']		= $menu->menu_level;
		$data['menu_urutan']	= $menu->menu_urutan;
		$data['menu_class']		= $menu->menu_class;
		$data['modul_id']		= $menu->modul_id;
		$data['modul_nama']		= $menu->modul_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/menu', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
