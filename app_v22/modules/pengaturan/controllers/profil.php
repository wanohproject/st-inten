<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Identitas Aplikasi | ' . profile('profil_website');;
		$this->active_menu		= 22;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Profil_model');
		$this->load->model('Settings_model');
    }
	
	
	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'index';
		
		$data['ckeditor'] 					= ckeditor('mini', 'profil_kontak', 150);
		$profil								= $this->Profil_model->get_profil('*');
		$data['profil_id']					= ($this->input->post('profil_id'))?$this->input->post('profil_id'):$profil->profil_id;				
		$data['profil_website']				= ($this->input->post('profil_website'))?$this->input->post('profil_website'):$profil->profil_website;
		$data['profil_favicon']				= ($this->input->post('profil_favicon'))?$this->input->post('profil_favicon'):$profil->profil_favicon;
		$data['profil_logo']				= ($this->input->post('profil_logo'))?$this->input->post('profil_logo'):$profil->profil_logo;
		$data['profil_background_login']	= ($this->input->post('profil_background_login'))?$this->input->post('profil_background_login'):$profil->profil_background_login;
		
		$data['profil_institusi']			= ($this->input->post('profil_institusi'))?$this->input->post('profil_institusi'):$profil->profil_institusi;
		$data['profil_singkatan']			= ($this->input->post('profil_singkatan'))?$this->input->post('profil_singkatan'):$profil->profil_singkatan;
		$data['profil_kontak']				= ($this->input->post('profil_kontak'))?$this->input->post('profil_kontak'):$profil->profil_kontak;
		$data['profil_latitude']			= ($this->input->post('profil_latitude'))?$this->input->post('profil_latitude'):$profil->profil_latitude;
		$data['profil_longitude']			= ($this->input->post('profil_longitude'))?$this->input->post('profil_longitude'):$profil->profil_longitude;
		$data['profil_telp']				= ($this->input->post('profil_telp'))?$this->input->post('profil_telp'):$profil->profil_telp;
		$data['profil_fax']					= ($this->input->post('profil_fax'))?$this->input->post('profil_fax'):$profil->profil_fax;
		$data['profil_email']				= ($this->input->post('profil_email'))?$this->input->post('profil_email'):$profil->profil_email;
		$data['profil_situs']				= ($this->input->post('profil_situs'))?$this->input->post('profil_situs'):$profil->profil_situs;
		$data['profil_ym']					= ($this->input->post('profil_ym'))?$this->input->post('profil_ym'):$profil->profil_ym;
		$data['profil_twitter']				= ($this->input->post('profil_twitter'))?$this->input->post('profil_twitter'):$profil->profil_twitter;
		$data['profil_fb']					= ($this->input->post('profil_fb'))?$this->input->post('profil_fb'):$profil->profil_fb;
		$data['profil_youtube']				= ($this->input->post('profil_youtube'))?$this->input->post('profil_youtube'):$profil->profil_youtube;
		$data['profil_linkedin']			= ($this->input->post('profil_linkedin'))?$this->input->post('profil_linkedin'):$profil->profil_linkedin;
		$data['profil_gplus']				= ($this->input->post('profil_gplus'))?$this->input->post('profil_gplus'):$profil->profil_gplus;
		$data['profil_rss']					= ($this->input->post('profil_rss'))?$this->input->post('profil_rss'):$profil->profil_rss;
		$data['kepala_sekolah']				= ($this->input->post('kepala_sekolah'))?$this->input->post('kepala_sekolah'):$profil->kepala_sekolah;
		$data['profil_warna']				= ($this->input->post('profil_warna'))?$this->input->post('profil_warna'):$profil->profil_warna;
		$data['profil_boxed']				= ($this->input->post('profil_boxed'))?$this->input->post('profil_boxed'):$profil->profil_boxed;
		$data['ttd_guru']					= ($this->input->post('ttd_guru'))?$this->input->post('ttd_guru'):$profil->ttd_guru;
		$data['ttd_tu']						= ($this->input->post('ttd_tu'))?$this->input->post('ttd_tu'):$profil->ttd_tu;
		$data['presensi_synctime']			= ($this->input->post('presensi_synctime'))?$this->input->post('presensi_synctime'):$profil->presensi_synctime;
		$save								= $this->input->post('save');
		if ($save == 'save'){
			$favicon	= upload_file("profil_favicon", "./asset/profil/");
			$data['profil_favicon']	= $favicon;

			$lambang	= upload_image("profil_logo", "./asset/profil/");
			$data['profil_logo']	= $lambang;

			$background_login	= upload_image("profil_background_login", "./asset/profil/");
			$data['profil_background_login']	= $background_login;
							
			$where_edit['profil_id']	= $this->input->post('profil_id');
			$edit['profil_website']		= $this->input->post('profil_website');
			$edit['profil_deskripsi']	= $this->input->post('profil_website');
			$edit['profil_keyword']		= $this->input->post('profil_website');
			
			$edit['profil_institusi']	= $this->input->post('profil_institusi');
			$edit['profil_singkatan']	= $this->input->post('profil_singkatan');
			$edit['profil_kontak']		= $this->input->post('profil_kontak');
			$edit['profil_telp']		= $this->input->post('profil_telp');
			$edit['profil_fax']			= $this->input->post('profil_fax');
			$edit['profil_email']		= $this->input->post('profil_email');
			$edit['profil_situs']		= $this->input->post('profil_situs');
			
			$edit['profil_latitude']	= $this->input->post('profil_latitude');
			$edit['profil_longitude']	= $this->input->post('profil_longitude');
			$edit['profil_ym']			= $this->input->post('profil_ym');
			$edit['profil_twitter']		= $this->input->post('profil_twitter');
			$edit['profil_fb']			= $this->input->post('profil_fb');
			$edit['profil_youtube']		= $this->input->post('profil_youtube');
			$edit['profil_linkedin']	= $this->input->post('profil_linkedin');
			$edit['profil_gplus']		= $this->input->post('profil_gplus');
			$edit['profil_rss']			= $this->input->post('profil_rss');
			$edit['kepala_sekolah']		= $this->input->post('kepala_sekolah');
			$edit['profil_warna']		= $this->input->post('profil_warna');
			$edit['profil_boxed']		= $this->input->post('profil_boxed');
			$edit['ttd_guru']			= $this->input->post('ttd_guru');
			$edit['ttd_tu']				= $this->input->post('ttd_tu');
			$edit['presensi_synctime']	= $this->input->post('presensi_synctime');
			
			if ($data['profil_favicon']) {
				$row = $this->Profil_model->get_profil('*');
				@unlink('./asset/profil/'.$row->profil_favicon);
				$edit['profil_favicon']	= $data['profil_favicon'];
			}
			
			if ($data['profil_logo']) {
				$row = $this->Profil_model->get_profil('*');
				@unlink('./asset/profil/'.$row->profil_logo);
				@unlink('./asset/profil/small_'.$row->profil_logo);
				$edit['profil_logo']	= $data['profil_logo'];
			}
			
			if ($data['profil_background_login']) {
				$row = $this->Profil_model->get_profil('*');
				@unlink('./asset/profil/'.$row->profil_background_login);
				@unlink('./asset/profil/small_'.$row->profil_background_login);
				$edit['profil_background_login']	= $data['profil_background_login'];
			}
			$this->Profil_model->update_profil($where_edit, $edit);

			$settings = $this->input->post('settings');
			if ($settings){
				foreach ($settings as $key => $value) {
					$this->Settings_model->set_value($key, $value);
				}
			}
			
			$this->session->set_flashdata('success','Identitas Aplikasi telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/profil', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
