<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Semester extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Semester | ' . profile('profil_website');
		$this->active_menu		= 258;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Referensi_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('tahun_kode, semester_kode, semester_nama, semester_aktif')
		->add_column('Actions', $this->get_buttons('$1'),'semester_kode')
		->search_column('tahun_kode, semester_kode, semester_nama, semester_aktif')
		->from('(SELECT tahun_kode, semester_kode, semester_nama, (case when (semester_aktif = \'Y\') THEN \'Aktif\' ELSE \'Tidak Aktif\' END) AS semester_aktif FROM akd_semester semester) semester');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/activated/'.$id) .'" class="btn btn-success btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Aktifkan"><i class="fa fa-check"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['tahun_kode']			= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):'';
		$data['semester_jenis']		= ($this->input->post('semester_jenis'))?$this->input->post('semester_jenis'):'';
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):'';
		$data['semester_nama']		= ($this->input->post('semester_nama'))?$this->input->post('semester_nama'):'';
		$data['semester_aktif']		= ($this->input->post('semester_aktif'))?$this->input->post('semester_aktif'):'Y';
		$data['semester_tampil']	= ($this->input->post('semester_tampil'))?$this->input->post('semester_tampil'):'N';
		$save						= $this->input->post('save');
		if ($save == 'save'){
			if ($data['semester_aktif'] == 'Y'){
				$this->db->query("UPDATE akd_semester SET semester_aktif = 'N'");

				$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'N'");
				$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'Y', tahun_tampil = 'Y' WHERE tahun_kode = '" . $data['tahun_kode'] . "'");
			}

			$insert['tahun_kode']			= validasi_input($this->input->post('tahun_kode'));
			$insert['semester_jenis']		= validasi_input($this->input->post('semester_jenis'));
			$insert['semester_kode']		= validasi_input($this->input->post('semester_kode'));
			$insert['semester_nama']		= validasi_input($this->input->post('semester_nama'));
			$insert['semester_aktif']		= validasi_input($this->input->post('semester_aktif'));
			$insert['semester_tampil']		= validasi_input($this->input->post('semester_tampil'));
			$this->Semester_model->insert_semester($insert);
			
			$this->session->set_flashdata('success','Semester telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['semester_kode']		= validasi_sql($this->uri->segment(4)); 
		$semester 					= $this->Semester_model->get_semester('*', $where);

		$data['tahun_kode']			= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$semester->tahun_kode;
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester->semester_kode;
		$data['semester_nama']		= ($this->input->post('semester_nama'))?$this->input->post('semester_nama'):$semester->semester_nama;
		$data['semester_jenis']		= ($this->input->post('semester_jenis'))?$this->input->post('semester_jenis'):$semester->semester_jenis;
		$data['semester_aktif']		= ($this->input->post('semester_aktif'))?$this->input->post('semester_aktif'):$semester->semester_aktif;
		$data['semester_tampil']	= ($this->input->post('semester_tampil'))?$this->input->post('semester_tampil'):$semester->semester_tampil;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			if ($data['semester_aktif'] == 'Y'){
				$this->db->query("UPDATE akd_semester SET semester_aktif = 'N'");

				$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'N'");
				$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'Y', tahun_tampil = 'Y' WHERE tahun_kode = '" . $data['tahun_kode'] . "'");
			}
			
			$where_edit['semester_kode']	= validasi_sql($this->input->post('semester_kode'));
			$edit['tahun_kode']				= validasi_input($this->input->post('tahun_kode'));
			$edit['semester_nama']			= validasi_input($this->input->post('semester_nama'));
			$edit['semester_jenis']			= validasi_input($this->input->post('semester_jenis'));
			$edit['semester_aktif']			= validasi_input($this->input->post('semester_aktif'));
			$edit['semester_tampil']		= validasi_input($this->input->post('semester_tampil'));
			$this->Semester_model->update_semester($where_edit, $edit);
			
			$this->session->set_flashdata('success','Semester telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['semester_kode']	= validasi_sql($this->uri->segment(4));
		$this->Semester_model->delete_semester($where_delete);
		
		$this->session->set_flashdata('success','Semester telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function activated()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->Semester_model->update_semester(array(), array('semester_aktif'=>'N'));

		$where['semester_kode']		= validasi_sql($this->uri->segment(4)); 
		$semester 					= $this->Semester_model->get_semester('*', $where);

		$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'N'");
		$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'Y' WHERE tahun_kode = '" . $semester->tahun_kode . "'");
		
		$where_edit['semester_kode']	= validasi_sql($this->uri->segment(4));
		$edit['semester_aktif']	= 'Y';
		$this->Semester_model->update_semester($where_edit, $edit);
		
		$this->session->set_flashdata('success','Semester telah berhasil diaktifkan.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['semester_kode']	= validasi_sql($this->uri->segment(4)); 
		$semester		 		= $this->Semester_model->get_semester('*', $where);
		
		$data['tahun_kode']			= $semester->tahun_kode;
		$data['semester_kode']		= $semester->semester_kode;
		$data['semester_nama']		= $semester->semester_nama;
		$data['semester_jenis']		= $semester->semester_jenis;
		$data['semester_aktif']		= $semester->semester_aktif;
		$data['semester_tampil']	= $semester->semester_tampil;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
