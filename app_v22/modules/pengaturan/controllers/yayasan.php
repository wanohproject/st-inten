<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Yayasan extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Yayasan | ' . profile('profil_website');
		$this->active_menu		= 333;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Yayasan_model');
		$this->load->model('Referensi_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('yayasan_id, yayasan_kode, yayasan_nama, yayasan_alamat')
		->add_column('Actions', $this->get_buttons('$1'),'yayasan_id')
		->search_column('yayasan_kode, yayasan_nama, yayasan_alamat')
		->from('akd_yayasan yayasan');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/yayasan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['yayasan_id']					= ($this->input->post('yayasan_id'))?$this->input->post('yayasan_id'):'';
		$data['yayasan_kode']				= ($this->input->post('yayasan_kode'))?$this->input->post('yayasan_kode'):'';
		$data['yayasan_nama']				= ($this->input->post('yayasan_nama'))?$this->input->post('yayasan_nama'):'';
		$data['yayasan_alamat']				= ($this->input->post('yayasan_alamat'))?$this->input->post('yayasan_alamat'):'';
		$data['yayasan_kota']				= ($this->input->post('yayasan_kota'))?$this->input->post('yayasan_kota'):'';
		$data['yayasan_kodepos']			= ($this->input->post('yayasan_kodepos'))?$this->input->post('yayasan_kodepos'):'';
		$data['yayasan_telepon']			= ($this->input->post('yayasan_telepon'))?$this->input->post('yayasan_telepon'):'';
		$data['yayasan_fax']				= ($this->input->post('yayasan_fax'))?$this->input->post('yayasan_fax'):'';
		$data['yayasan_email']				= ($this->input->post('yayasan_email'))?$this->input->post('yayasan_email'):'';
		$data['yayasan_website']			= ($this->input->post('yayasan_website'))?$this->input->post('yayasan_website'):'';
		$data['yayasan_tanggal_sk']			= ($this->input->post('yayasan_tanggal_sk'))?$this->input->post('yayasan_tanggal_sk'):'';
		$data['yayasan_nomor_sk']			= ($this->input->post('yayasan_nomor_sk'))?$this->input->post('yayasan_nomor_sk'):'';
		$data['yayasan_tanggal_bn']			= ($this->input->post('yayasan_tanggal_bn'))?$this->input->post('yayasan_tanggal_bn'):'';
		$data['yayasan_nomor_bn']			= ($this->input->post('yayasan_nomor_bn'))?$this->input->post('yayasan_nomor_bn'):'';
		$data['yayasan_tanggal_pendirian']	= ($this->input->post('yayasan_tanggal_pendirian'))?$this->input->post('yayasan_tanggal_pendirian'):'';
		$data['yayasan_aktif']				= ($this->input->post('yayasan_aktif'))?$this->input->post('yayasan_aktif'):'Y';
		$save								= $this->input->post('save');
		if ($save == 'save'){
			$insert['yayasan_id']					= $this->uuid->v4();
			$insert['yayasan_kode']					= validasi_input($this->input->post('yayasan_kode'));
			$insert['yayasan_nama']					= validasi_input($this->input->post('yayasan_nama'));
			$insert['yayasan_alamat']				= validasi_input($this->input->post('yayasan_alamat'));
			$insert['yayasan_kota']					= validasi_input($this->input->post('yayasan_kota'));
			$insert['yayasan_kodepos']				= validasi_input($this->input->post('yayasan_kodepos'));
			$insert['yayasan_telepon']				= validasi_input($this->input->post('yayasan_telepon'));
			$insert['yayasan_fax']					= validasi_input($this->input->post('yayasan_fax'));
			$insert['yayasan_email']				= validasi_input($this->input->post('yayasan_email'));
			$insert['yayasan_website']				= validasi_input($this->input->post('yayasan_website'));
			$insert['yayasan_tanggal_sk']			= validasi_input($this->input->post('yayasan_tanggal_sk'));
			$insert['yayasan_nomor_sk']				= validasi_input($this->input->post('yayasan_nomor_sk'));
			$insert['yayasan_tanggal_bn']			= validasi_input($this->input->post('yayasan_tanggal_bn'));
			$insert['yayasan_nomor_bn']				= validasi_input($this->input->post('yayasan_nomor_bn'));
			$insert['yayasan_tanggal_pendirian']	= validasi_input($this->input->post('yayasan_tanggal_pendirian'));
			$insert['yayasan_aktif']				= validasi_input($this->input->post('yayasan_aktif'));
			$insert['created_by']					= userdata('pengguna_id');
			$this->Yayasan_model->insert_yayasan($insert);
			
			$this->session->set_flashdata('success','Yayasan telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/yayasan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['yayasan_id']		= validasi_sql($this->uri->segment(4)); 
		$yayasan 					= $this->Yayasan_model->get_yayasan('*', $where);

		$data['yayasan_id']					= ($this->input->post('yayasan_id'))?$this->input->post('yayasan_id'):$yayasan->yayasan_id;
		$data['yayasan_kode']				= ($this->input->post('yayasan_kode'))?$this->input->post('yayasan_kode'):$yayasan->yayasan_kode;
		$data['yayasan_nama']				= ($this->input->post('yayasan_nama'))?$this->input->post('yayasan_nama'):$yayasan->yayasan_nama;
		$data['yayasan_alamat']				= ($this->input->post('yayasan_alamat'))?$this->input->post('yayasan_alamat'):$yayasan->yayasan_alamat;
		$data['yayasan_kota']				= ($this->input->post('yayasan_kota'))?$this->input->post('yayasan_kota'):$yayasan->yayasan_kota;
		$data['yayasan_kodepos']			= ($this->input->post('yayasan_kodepos'))?$this->input->post('yayasan_kodepos'):$yayasan->yayasan_kodepos;
		$data['yayasan_telepon']			= ($this->input->post('yayasan_telepon'))?$this->input->post('yayasan_telepon'):$yayasan->yayasan_telepon;
		$data['yayasan_fax']				= ($this->input->post('yayasan_fax'))?$this->input->post('yayasan_fax'):$yayasan->yayasan_fax;
		$data['yayasan_email']				= ($this->input->post('yayasan_email'))?$this->input->post('yayasan_email'):$yayasan->yayasan_email;
		$data['yayasan_website']			= ($this->input->post('yayasan_website'))?$this->input->post('yayasan_website'):$yayasan->yayasan_website;
		$data['yayasan_tanggal_sk']			= ($this->input->post('yayasan_tanggal_sk'))?$this->input->post('yayasan_tanggal_sk'):$yayasan->yayasan_tanggal_sk;
		$data['yayasan_nomor_sk']			= ($this->input->post('yayasan_nomor_sk'))?$this->input->post('yayasan_nomor_sk'):$yayasan->yayasan_nomor_sk;
		$data['yayasan_tanggal_bn']			= ($this->input->post('yayasan_tanggal_bn'))?$this->input->post('yayasan_tanggal_bn'):$yayasan->yayasan_tanggal_bn;
		$data['yayasan_nomor_bn']			= ($this->input->post('yayasan_nomor_bn'))?$this->input->post('yayasan_nomor_bn'):$yayasan->yayasan_nomor_bn;
		$data['yayasan_tanggal_pendirian']	= ($this->input->post('yayasan_tanggal_pendirian'))?$this->input->post('yayasan_tanggal_pendirian'):$yayasan->yayasan_tanggal_pendirian;
		$data['yayasan_aktif']				= ($this->input->post('yayasan_aktif'))?$this->input->post('yayasan_aktif'):$yayasan->yayasan_aktif;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$where_update['yayasan_id']				= $this->input->post('yayasan_id');
			$update['yayasan_kode']					= validasi_input($this->input->post('yayasan_kode'));
			$update['yayasan_nama']					= validasi_input($this->input->post('yayasan_nama'));
			$update['yayasan_alamat']				= validasi_input($this->input->post('yayasan_alamat'));
			$update['yayasan_kota']					= validasi_input($this->input->post('yayasan_kota'));
			$update['yayasan_kodepos']				= validasi_input($this->input->post('yayasan_kodepos'));
			$update['yayasan_telepon']				= validasi_input($this->input->post('yayasan_telepon'));
			$update['yayasan_fax']					= validasi_input($this->input->post('yayasan_fax'));
			$update['yayasan_email']				= validasi_input($this->input->post('yayasan_email'));
			$update['yayasan_website']				= validasi_input($this->input->post('yayasan_website'));
			$update['yayasan_tanggal_sk']			= validasi_input($this->input->post('yayasan_tanggal_sk'));
			$update['yayasan_nomor_sk']				= validasi_input($this->input->post('yayasan_nomor_sk'));
			$update['yayasan_tanggal_bn']			= validasi_input($this->input->post('yayasan_tanggal_bn'));
			$update['yayasan_nomor_bn']				= validasi_input($this->input->post('yayasan_nomor_bn'));
			$update['yayasan_tanggal_pendirian']	= validasi_input($this->input->post('yayasan_tanggal_pendirian'));
			$update['yayasan_aktif']				= validasi_input($this->input->post('yayasan_aktif'));
			$update['updated_by']					= userdata('pengguna_id');
			$this->Yayasan_model->update_yayasan($where_update, $update);
			
			$this->session->set_flashdata('success','Yayasan telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/yayasan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['yayasan_id']	= validasi_sql($this->uri->segment(4));
		$this->Yayasan_model->delete_yayasan($where_delete);
		
		$this->session->set_flashdata('success','Yayasan telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['yayasan_id']	= validasi_sql($this->uri->segment(4)); 
		$yayasan				= $this->Yayasan_model->get_yayasan('*', $where);
		
		$data['yayasan_id']					= $yayasan->yayasan_id;
		$data['yayasan_kode']				= $yayasan->yayasan_kode;
		$data['yayasan_nama']				= $yayasan->yayasan_nama;
		$data['yayasan_alamat']				= $yayasan->yayasan_alamat;
		$data['yayasan_kota']				= $yayasan->yayasan_kota;
		$data['yayasan_kodepos']			= $yayasan->yayasan_kodepos;
		$data['yayasan_telepon']			= $yayasan->yayasan_telepon;
		$data['yayasan_fax']				= $yayasan->yayasan_fax;
		$data['yayasan_email']				= $yayasan->yayasan_email;
		$data['yayasan_website']			= $yayasan->yayasan_website;
		$data['yayasan_tanggal_sk']			= $yayasan->yayasan_tanggal_sk;
		$data['yayasan_nomor_sk']			= $yayasan->yayasan_nomor_sk;
		$data['yayasan_tanggal_bn']			= $yayasan->yayasan_tanggal_bn;
		$data['yayasan_nomor_bn']			= $yayasan->yayasan_nomor_bn;
		$data['yayasan_tanggal_pendirian']	= $yayasan->yayasan_tanggal_pendirian;
		$data['yayasan_aktif']				= $yayasan->yayasan_aktif;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/yayasan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
