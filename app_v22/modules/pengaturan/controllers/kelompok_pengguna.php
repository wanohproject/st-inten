<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok_pengguna extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Kelompok Pengguna | ' . profile('profil_website');;
		$this->active_menu		= 20;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Pengguna_model');
    }
	
	function datatable()
	{
		header('Content-Type: application/json');
		$this->load->library('Datatables');
		if ($this->session->userdata('level') != '1'){
			$this->datatables->select('pengguna_level_id, pengguna_level_kode, pengguna_level_nama')
			->add_column('Actions', $this->get_buttons('$1'),'pengguna_level_id')
			->search_column('pengguna_level_kode, pengguna_level_nama')
			->from('pengguna_level')
			->where('pengguna_level_id NOT IN (1)');
		} else {
			$this->datatables->select('pengguna_level_id, pengguna_level_kode, pengguna_level_nama')
			->add_column('Actions', $this->get_buttons('$1'),'pengguna_level_id')
			->search_column('pengguna_level_kode, pengguna_level_nama')
			->from('pengguna_level');
		}
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelompok_pengguna', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['pengguna_level_kode']		= ($this->input->post('pengguna_level_kode'))?$this->input->post('pengguna_level_kode'):'';
		$data['pengguna_level_nama']		= ($this->input->post('pengguna_level_nama'))?$this->input->post('pengguna_level_nama'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$insert['pengguna_level_kode']		= validasi_sql($this->input->post('pengguna_level_kode'));
			$insert['pengguna_level_nama']		= validasi_sql($this->input->post('pengguna_level_nama'));
			$this->Pengguna_model->insert_pengguna_level($insert);
			
			$this->session->set_flashdata('success','Kelompok Pengguna telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelompok_pengguna', $data);
		$this->load->view(module_dir().'/separate/footer');
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['pengguna_level_id']		= validasi_sql($this->uri->segment(4)); 
		$pengguna_level 				= $this->Pengguna_model->get_pengguna_level('*', $where);
		
		$data['pengguna_level_id']		= ($this->input->post('pengguna_level_id'))?$this->input->post('pengguna_level_id'):$pengguna_level->pengguna_level_id;
		$data['pengguna_level_kode']	= ($this->input->post('pengguna_level_kode'))?$this->input->post('pengguna_level_kode'):$pengguna_level->pengguna_level_kode;
		$data['pengguna_level_nama']	= ($this->input->post('pengguna_level_nama'))?$this->input->post('pengguna_level_nama'):$pengguna_level->pengguna_level_nama;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['pengguna_level_id']	= validasi_sql($this->input->post('pengguna_level_id'));
			$edit['pengguna_level_kode']		= validasi_sql($this->input->post('pengguna_level_kode'));
			$edit['pengguna_level_nama']		= validasi_sql($this->input->post('pengguna_level_nama'));
			$this->Pengguna_model->update_pengguna_level($where_edit, $edit);
			
			$this->session->set_flashdata('success','Kelompok Pengguna telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelompok_pengguna', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['pengguna_level_id']	= validasi_sql($this->uri->segment(4));
		$this->Pengguna_model->delete_pengguna_level($where_delete);
		
		$this->session->set_flashdata('success','Kelompok Pengguna telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['pengguna_level_id']	= validasi_sql($this->uri->segment(4)); 
		$pengguna_level		 		= $this->Pengguna_model->get_pengguna_level('*', $where);
		
		$data['pengguna_level_id']		= $pengguna_level->pengguna_level_id;
		$data['pengguna_level_kode']	= $pengguna_level->pengguna_level_kode;
		$data['pengguna_level_nama']	= $pengguna_level->pengguna_level_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelompok_pengguna', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
