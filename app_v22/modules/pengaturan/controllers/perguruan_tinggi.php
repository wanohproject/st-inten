<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Perguruan_tinggi extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Perguruan Tinggi | ' . profile('profil_website');
		$this->active_menu		= 333;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Referensi_model');
		$this->load->model('Yayasan_model');
		$this->load->model('Perguruan_tinggi_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('perguruan_tinggi_id, perguruan_tinggi_kode, perguruan_tinggi_nama, perguruan_tinggi_singkatan')
		->add_column('Actions', $this->get_buttons('$1'),'perguruan_tinggi_id')
		->search_column('perguruan_tinggi_kode, perguruan_tinggi_nama, perguruan_tinggi_singkatan')
		->from('akd_perguruan_tinggi perguruan_tinggi');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/perguruan_tinggi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['perguruan_tinggi_id']			= ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):'';
		$data['yayasan_id']						= ($this->input->post('yayasan_id'))?$this->input->post('yayasan_id'):'';
		$data['perguruan_tinggi_kode']			= ($this->input->post('perguruan_tinggi_kode'))?$this->input->post('perguruan_tinggi_kode'):'';
		$data['perguruan_tinggi_nama']			= ($this->input->post('perguruan_tinggi_nama'))?$this->input->post('perguruan_tinggi_nama'):'';
		$data['perguruan_tinggi_singkatan']		= ($this->input->post('perguruan_tinggi_singkatan'))?$this->input->post('perguruan_tinggi_singkatan'):'';
		$data['perguruan_tinggi_alamat']		= ($this->input->post('perguruan_tinggi_alamat'))?$this->input->post('perguruan_tinggi_alamat'):'';
		$data['perguruan_tinggi_kota']			= ($this->input->post('perguruan_tinggi_kota'))?$this->input->post('perguruan_tinggi_kota'):'';
		$data['perguruan_tinggi_kodepos']		= ($this->input->post('perguruan_tinggi_kodepos'))?$this->input->post('perguruan_tinggi_kodepos'):'';
		$data['perguruan_tinggi_telepon']		= ($this->input->post('perguruan_tinggi_telepon'))?$this->input->post('perguruan_tinggi_telepon'):'';
		$data['perguruan_tinggi_fax']			= ($this->input->post('perguruan_tinggi_fax'))?$this->input->post('perguruan_tinggi_fax'):'';
		$data['perguruan_tinggi_email']			= ($this->input->post('perguruan_tinggi_email'))?$this->input->post('perguruan_tinggi_email'):'';
		$data['perguruan_tinggi_website']		= ($this->input->post('perguruan_tinggi_website'))?$this->input->post('perguruan_tinggi_website'):'';
		$data['perguruan_tinggi_nomor_sk']		= ($this->input->post('perguruan_tinggi_nomor_sk'))?$this->input->post('perguruan_tinggi_nomor_sk'):'';
		$data['perguruan_tinggi_tanggal_sk']	= ($this->input->post('perguruan_tinggi_tanggal_sk'))?$this->input->post('perguruan_tinggi_tanggal_sk'):'';
		$data['perguruan_tinggi_aktif']			= ($this->input->post('perguruan_tinggi_aktif'))?$this->input->post('perguruan_tinggi_aktif'):'Y';
		$save									= $this->input->post('save');
		if ($save == 'save'){
			$insert['perguruan_tinggi_id']			= $this->uuid->v4();
			$insert['yayasan_id']					= validasi_input($this->input->post('yayasan_id'));
			$insert['perguruan_tinggi_kode']		= validasi_input($this->input->post('perguruan_tinggi_kode'));
			$insert['perguruan_tinggi_nama']		= validasi_input($this->input->post('perguruan_tinggi_nama'));
			$insert['perguruan_tinggi_singkatan']	= validasi_input($this->input->post('perguruan_tinggi_singkatan'));
			$insert['perguruan_tinggi_alamat']		= validasi_input($this->input->post('perguruan_tinggi_alamat'));
			$insert['perguruan_tinggi_kota']		= validasi_input($this->input->post('perguruan_tinggi_kota'));
			$insert['perguruan_tinggi_kodepos']		= validasi_input($this->input->post('perguruan_tinggi_kodepos'));
			$insert['perguruan_tinggi_telepon']		= validasi_input($this->input->post('perguruan_tinggi_telepon'));
			$insert['perguruan_tinggi_fax']			= validasi_input($this->input->post('perguruan_tinggi_fax'));
			$insert['perguruan_tinggi_email']		= validasi_input($this->input->post('perguruan_tinggi_email'));
			$insert['perguruan_tinggi_website']		= validasi_input($this->input->post('perguruan_tinggi_website'));
			$insert['perguruan_tinggi_nomor_sk']	= validasi_input($this->input->post('perguruan_tinggi_nomor_sk'));
			$insert['perguruan_tinggi_tanggal_sk']	= validasi_input($this->input->post('perguruan_tinggi_tanggal_sk'));
			$insert['perguruan_tinggi_aktif']		= validasi_input($this->input->post('perguruan_tinggi_aktif'));
			$insert['perguruan_tinggi_singkatan']	= validasi_input($this->input->post('perguruan_tinggi_singkatan'));
			$insert['created_by']					= userdata('pengguna_id');
			$this->Perguruan_tinggi_model->insert_perguruan_tinggi($insert);
			
			$this->session->set_flashdata('success','Perguruan Tinggi telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/perguruan_tinggi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['perguruan_tinggi_id']		= validasi_sql($this->uri->segment(4)); 
		$perguruan_tinggi 					= $this->Perguruan_tinggi_model->get_perguruan_tinggi('*', $where);

		$data['perguruan_tinggi_id']			= ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$perguruan_tinggi->perguruan_tinggi_id;
		$data['yayasan_id']						= ($this->input->post('yayasan_id'))?$this->input->post('yayasan_id'):$perguruan_tinggi->yayasan_id;
		$data['perguruan_tinggi_kode']			= ($this->input->post('perguruan_tinggi_kode'))?$this->input->post('perguruan_tinggi_kode'):$perguruan_tinggi->perguruan_tinggi_kode;
		$data['perguruan_tinggi_nama']			= ($this->input->post('perguruan_tinggi_nama'))?$this->input->post('perguruan_tinggi_nama'):$perguruan_tinggi->perguruan_tinggi_nama;
		$data['perguruan_tinggi_singkatan']		= ($this->input->post('perguruan_tinggi_singkatan'))?$this->input->post('perguruan_tinggi_singkatan'):$perguruan_tinggi->perguruan_tinggi_singkatan;
		$data['perguruan_tinggi_alamat']		= ($this->input->post('perguruan_tinggi_alamat'))?$this->input->post('perguruan_tinggi_alamat'):$perguruan_tinggi->perguruan_tinggi_alamat;
		$data['perguruan_tinggi_kota']			= ($this->input->post('perguruan_tinggi_kota'))?$this->input->post('perguruan_tinggi_kota'):$perguruan_tinggi->perguruan_tinggi_kota;
		$data['perguruan_tinggi_kodepos']		= ($this->input->post('perguruan_tinggi_kodepos'))?$this->input->post('perguruan_tinggi_kodepos'):$perguruan_tinggi->perguruan_tinggi_kodepos;
		$data['perguruan_tinggi_telepon']		= ($this->input->post('perguruan_tinggi_telepon'))?$this->input->post('perguruan_tinggi_telepon'):$perguruan_tinggi->perguruan_tinggi_telepon;
		$data['perguruan_tinggi_fax']			= ($this->input->post('perguruan_tinggi_fax'))?$this->input->post('perguruan_tinggi_fax'):$perguruan_tinggi->perguruan_tinggi_fax;
		$data['perguruan_tinggi_email']			= ($this->input->post('perguruan_tinggi_email'))?$this->input->post('perguruan_tinggi_email'):$perguruan_tinggi->perguruan_tinggi_email;
		$data['perguruan_tinggi_website']		= ($this->input->post('perguruan_tinggi_website'))?$this->input->post('perguruan_tinggi_website'):$perguruan_tinggi->perguruan_tinggi_website;
		$data['perguruan_tinggi_nomor_sk']		= ($this->input->post('perguruan_tinggi_nomor_sk'))?$this->input->post('perguruan_tinggi_nomor_sk'):$perguruan_tinggi->perguruan_tinggi_nomor_sk;
		$data['perguruan_tinggi_tanggal_sk']	= ($this->input->post('perguruan_tinggi_tanggal_sk'))?$this->input->post('perguruan_tinggi_tanggal_sk'):$perguruan_tinggi->perguruan_tinggi_tanggal_sk;
		$data['perguruan_tinggi_aktif']			= ($this->input->post('perguruan_tinggi_aktif'))?$this->input->post('perguruan_tinggi_aktif'):$perguruan_tinggi->perguruan_tinggi_aktif;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$where_update['perguruan_tinggi_id']	= $this->input->post('perguruan_tinggi_id');
			$update['yayasan_id']					= validasi_input($this->input->post('yayasan_id'));
			$update['perguruan_tinggi_kode']		= validasi_input($this->input->post('perguruan_tinggi_kode'));
			$update['perguruan_tinggi_nama']		= validasi_input($this->input->post('perguruan_tinggi_nama'));
			$update['perguruan_tinggi_singkatan']	= validasi_input($this->input->post('perguruan_tinggi_singkatan'));
			$update['perguruan_tinggi_alamat']		= validasi_input($this->input->post('perguruan_tinggi_alamat'));
			$update['perguruan_tinggi_kota']		= validasi_input($this->input->post('perguruan_tinggi_kota'));
			$update['perguruan_tinggi_kodepos']		= validasi_input($this->input->post('perguruan_tinggi_kodepos'));
			$update['perguruan_tinggi_telepon']		= validasi_input($this->input->post('perguruan_tinggi_telepon'));
			$update['perguruan_tinggi_fax']			= validasi_input($this->input->post('perguruan_tinggi_fax'));
			$update['perguruan_tinggi_email']		= validasi_input($this->input->post('perguruan_tinggi_email'));
			$update['perguruan_tinggi_website']		= validasi_input($this->input->post('perguruan_tinggi_website'));
			$update['perguruan_tinggi_nomor_sk']	= validasi_input($this->input->post('perguruan_tinggi_nomor_sk'));
			$update['perguruan_tinggi_tanggal_sk']	= validasi_input($this->input->post('perguruan_tinggi_tanggal_sk'));
			$update['perguruan_tinggi_aktif']		= validasi_input($this->input->post('perguruan_tinggi_aktif'));
			$update['perguruan_tinggi_singkatan']	= validasi_input($this->input->post('perguruan_tinggi_singkatan'));
			$update['updated_by']					= userdata('pengguna_id');
			$this->Perguruan_tinggi_model->update_perguruan_tinggi($where_update, $update);
			
			$this->session->set_flashdata('success','Perguruan Tinggi telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/perguruan_tinggi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['perguruan_tinggi_id']	= validasi_sql($this->uri->segment(4));
		$this->Perguruan_tinggi_model->delete_perguruan_tinggi($where_delete);
		
		$this->session->set_flashdata('success','Perguruan Tinggi telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['perguruan_tinggi_id']	= validasi_sql($this->uri->segment(4)); 
		$perguruan_tinggi		= $this->Perguruan_tinggi_model->get_perguruan_tinggi('*', $where);
		
		$data['yayasan_id']						= $perguruan_tinggi->yayasan_id;
		$data['yayasan_nama']						= $perguruan_tinggi->yayasan_nama;
		$data['perguruan_tinggi_id']			= $perguruan_tinggi->perguruan_tinggi_id;
		$data['perguruan_tinggi_kode']			= $perguruan_tinggi->perguruan_tinggi_kode;
		$data['perguruan_tinggi_nama']			= $perguruan_tinggi->perguruan_tinggi_nama;
		$data['perguruan_tinggi_singkatan']		= $perguruan_tinggi->perguruan_tinggi_singkatan;
		$data['perguruan_tinggi_alamat']		= $perguruan_tinggi->perguruan_tinggi_alamat;
		$data['perguruan_tinggi_kota']			= $perguruan_tinggi->perguruan_tinggi_kota;
		$data['perguruan_tinggi_kodepos']		= $perguruan_tinggi->perguruan_tinggi_kodepos;
		$data['perguruan_tinggi_telepon']		= $perguruan_tinggi->perguruan_tinggi_telepon;
		$data['perguruan_tinggi_fax']			= $perguruan_tinggi->perguruan_tinggi_fax;
		$data['perguruan_tinggi_email']			= $perguruan_tinggi->perguruan_tinggi_email;
		$data['perguruan_tinggi_website']		= $perguruan_tinggi->perguruan_tinggi_website;
		$data['perguruan_tinggi_nomor_sk']		= $perguruan_tinggi->perguruan_tinggi_nomor_sk;
		$data['perguruan_tinggi_tanggal_sk']	= $perguruan_tinggi->perguruan_tinggi_tanggal_sk;
		$data['perguruan_tinggi_aktif']			= $perguruan_tinggi->perguruan_tinggi_aktif;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/perguruan_tinggi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
