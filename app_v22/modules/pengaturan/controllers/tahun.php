<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Tahun Ajaran | ' . profile('profil_website');
		$this->active_menu		= 255;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Referensi_model');
		$this->load->model('Tahun_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('tahun_kode, tahun_nama, tahun_aktif')
		->add_column('Actions', $this->get_buttons('$1'),'tahun_kode')
		->search_column('tahun_kode, tahun_nama, tahun_aktif')
		->from('(SELECT tahun_kode, tahun_nama, (case when (tahun_aktif = \'Y\') THEN \'Aktif\' ELSE \'Tidak Aktif\' END) AS tahun_aktif FROM akd_tahun tahun) tahun');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/activated/'.$id) .'" class="btn btn-success btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Aktifkan"><i class="fa fa-check"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):'';
		$data['tahun_nama']		= ($this->input->post('tahun_nama'))?$this->input->post('tahun_nama'):'';
		$data['tahun_aktif']	= ($this->input->post('tahun_aktif'))?$this->input->post('tahun_aktif'):'Y';
		$data['tahun_tampil']	= ($this->input->post('tahun_tampil'))?$this->input->post('tahun_tampil'):'N';
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($data['tahun_aktif'] == 'Y'){
				$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'N'");
			}
			
			$insert['tahun_kode']		= validasi_input($this->input->post('tahun_kode'));
			$insert['tahun_nama']		= validasi_input($this->input->post('tahun_nama'));
			$insert['tahun_aktif']		= validasi_input($this->input->post('tahun_aktif'));
			$insert['tahun_tampil']		= validasi_input($this->input->post('tahun_tampil'));
			$this->Tahun_model->insert_tahun($insert);
			
			$this->session->set_flashdata('success','Tahun Ajaran telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['tahun_kode']		= validasi_sql($this->uri->segment(4)); 
		$tahun 					= $this->Tahun_model->get_tahun('*', $where);

		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun->tahun_kode;
		$data['tahun_nama']		= ($this->input->post('tahun_nama'))?$this->input->post('tahun_nama'):$tahun->tahun_nama;
		$data['tahun_aktif']	= ($this->input->post('tahun_aktif'))?$this->input->post('tahun_aktif'):$tahun->tahun_aktif;
		$data['tahun_tampil']	= ($this->input->post('tahun_tampil'))?$this->input->post('tahun_tampil'):$tahun->tahun_tampil;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($data['tahun_aktif'] == 'Y'){
				$this->db->query("UPDATE akd_tahun SET tahun_aktif = 'N'");
			}
			
			$where_edit['tahun_kode']	= validasi_sql($this->input->post('tahun_kode'));
			$edit['tahun_nama']		= validasi_input($this->input->post('tahun_nama'));
			$edit['tahun_aktif']	= validasi_input($this->input->post('tahun_aktif'));
			$edit['tahun_tampil']	= validasi_input($this->input->post('tahun_tampil'));
			$this->Tahun_model->update_tahun($where_edit, $edit);
			
			$this->session->set_flashdata('success','Tahun Ajaran telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['tahun_kode']	= validasi_sql($this->uri->segment(4));
		$this->Tahun_model->delete_tahun($where_delete);
		
		$this->session->set_flashdata('success','Tahun Ajaran telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function activated()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->Tahun_model->update_tahun(array(), array('tahun_aktif'=>'H'));
		
		$where_edit['tahun_kode']	= validasi_sql($this->uri->segment(4));
		$edit['tahun_aktif']		= 'Y';
		$edit['tahun_tampil']		= 'Y';
		$this->Tahun_model->update_tahun($where_edit, $edit);
		
		$this->session->set_flashdata('success','Tahun Ajaran telah berhasil diaktifkan.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['tahun_kode']	= validasi_sql($this->uri->segment(4)); 
		$tahun		= $this->Tahun_model->get_tahun('*', $where);
		
		$data['tahun_kode']		= $tahun->tahun_kode;
		$data['tahun_nama']		= $tahun->tahun_nama;
		$data['tahun_aktif']	= $tahun->tahun_aktif;
		$data['tahun_tampil']	= $tahun->tahun_tampil;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
