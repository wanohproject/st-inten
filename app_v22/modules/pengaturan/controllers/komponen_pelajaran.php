<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Komponen_pelajaran extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Komponen Penilaian | ' . profile('profil_website');;
		$this->active_menu		= 270;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Staf_model');
		$this->load->model('Kelas_model');
		$this->load->model('Komponen_pelajaran_model');
		$this->load->model('Tahun_model');
		$this->load->model('Nilai_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{		
		$tahun_kode = validasi_sql($this->uri->segment(4));
		$this->load->library('Datatables');
		$this->datatables->select('komponen_nama, komponen_pelajaran.tahun_kode, tahun_nama')
		->add_column('Actions', $this->get_buttons('$1', '$2', '$3'),'tahun_kode')
		->search_column('komponen_nama, tahun_nama')
		->from('komponen_pelajaran')
		->join('komponen_penilaian', 'komponen_pelajaran.komponen_id=komponen_penilaian.komponen_id', 'left')
		->join('tahun_ajaran', 'komponen_pelajaran.tahun_kode=tahun_ajaran.tahun_kode', 'left')
		->where("komponen_pelajaran.tahun_kode = '{$tahun_kode}' AND komponen_pelajaran_status = 'A'");
        echo $this->datatables->generate();
    }
	
	function get_buttons($tahun_kode)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/add/'.$tahun_kode) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$tahun_kode = ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/komponen_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$tahun_kode = ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		
		$data['tahun_kode'] = ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$tahun_kode = $this->input->post('tahun_kode');
			$komponen_pelajaran = $this->input->post('komponen_pelajaran');
			$listAP = 0;
			if ($komponen_pelajaran){
				$listAP = implode(",", $komponen_pelajaran);
			}
			$forDelete = $this->db->query("SELECT komponen_id FROM komponen_pelajaran WHERE tahun_kode = '{$tahun_kode}' AND komponen_id NOT IN ($listAP)")->result();
			if ($komponen_pelajaran){
				foreach($komponen_pelajaran as $row){
					if ($this->Komponen_pelajaran_model->count_all_komponen_pelajaran(array('komponen_pelajaran.komponen_id'=>$row, 'komponen_pelajaran.tahun_kode'=>$tahun_kode, 'komponen_pelajaran_status'=>'D')) > 0){
						$this->Komponen_pelajaran_model->update_komponen_pelajaran(array('komponen_id'=>$row, 'komponen_pelajaran.tahun_kode'=>$tahun_kode, 'komponen_pelajaran_status'=>'D'), array('komponen_pelajaran_status'=>'A'));
					} else if ($this->Komponen_pelajaran_model->count_all_komponen_pelajaran(array('komponen_pelajaran.komponen_id'=>$row, 'komponen_pelajaran.tahun_kode'=>$tahun_kode)) < 1){
						$insert['tahun_kode'] 				= $tahun_kode;
						$insert['komponen_id'] 				= $row;
						$insert['komponen_pelajaran_tanggal'] 	= date('Y-m-d H:i:s');
						$this->Komponen_pelajaran_model->insert_komponen_pelajaran($insert);
					}
				}
			}
			
			if ($forDelete){
				foreach($forDelete as $row){
					if($this->Nilai_model->count_all_nilai(array('nilai.komponen_id'=>$row->komponen_id, 'nilai.tahun_kode'=>$tahun_kode)) > 0){
						$this->Komponen_pelajaran_model->update_komponen_pelajaran(array('komponen_id'=>$row->komponen_id, 'tahun_kode'=>$tahun_kode), array('komponen_pelajaran_status'=>'D'));
					} else {
						$this->Komponen_pelajaran_model->delete_komponen_pelajaran(array('komponen_id'=>$row->komponen_id, 'tahun_kode'=>$tahun_kode));
					}
				}
			}
			
			$this->session->set_flashdata('success','Komponen Penilaian telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$tahun_kode));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/komponen_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
