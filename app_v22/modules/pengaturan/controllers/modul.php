<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Modul extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Modul | ' . profile('profil_website');;
		$this->active_menu		= 281;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Menu_model');
		$this->load->model('Modul_model');
    }
	
	function datatable()
	{
		header('Content-Type: application/json');
		$this->load->library('Datatables');
		$this->datatables->select('modul_id, modul_nama, modul_url, modul_tipe, modul_link')
		->add_column('Actions', $this->get_buttons('$1'),'modul_id')
		->search_column('modul_nama, modul_url, modul_tipe')
		->from('(SELECT modul_id, modul_nama, modul_url, (case when (modul_tipe = \'B\') THEN \'Backend\' ELSE \'Frontend\' END) AS modul_tipe, (case when (modul_tipe = \'B\') THEN CONCAT(\''.module_url().'/\', modul_url ) ELSE CONCAT(\''.site_url().'\', modul_url ) END) AS modul_link FROM modul) modul');		
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/modul', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';

		$data['list_warna'] = array("bg-red"=>"Red", "bg-yellow"=>"Yellow", "bg-aqua"=>"Aqua", "bg-blue"=>"Blue", "bg-green"=>"Green", "bg-navy"=>"Navy", "bg-teal"=>"Teal", "bg-olive"=>"Olive", "bg-lime"=>"Lime", "bg-orange"=>"Orange", "bg-fuchsia"=>"Fuchsia", "bg-purple"=>"Purple", "bg-maroon"=>"Maroon", "bg-black"=>"Black", "bg-light-blue"=>"Light Blue");
		
		$data['modul_nama']		= ($this->input->post('modul_nama'))?$this->input->post('modul_nama'):'';
		$data['modul_dir']		= ($this->input->post('modul_dir'))?$this->input->post('modul_dir'):'';
		$data['modul_url']		= ($this->input->post('modul_url'))?$this->input->post('modul_url'):'';
		$data['modul_tipe']		= ($this->input->post('modul_tipe'))?$this->input->post('modul_tipe'):'';
		$data['modul_icon']		= ($this->input->post('modul_icon'))?$this->input->post('modul_icon'):'';
		$data['modul_urutan']	= ($this->input->post('modul_urutan'))?$this->input->post('modul_urutan'):'';
		$data['modul_warna']	= ($this->input->post('modul_warna'))?$this->input->post('modul_warna'):'bg-blue';
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$insert['modul_nama']		= validasi_sql($this->input->post('modul_nama'));
			$insert['modul_dir']		= validasi_sql($this->input->post('modul_dir'));
			$insert['modul_url']		= validasi_sql($this->input->post('modul_url'));
			$insert['modul_tipe']		= validasi_sql($this->input->post('modul_tipe'));
			$insert['modul_icon']		= validasi_sql($this->input->post('modul_icon'));
			$insert['modul_urutan']		= validasi_sql($this->input->post('modul_urutan'));
			$insert['modul_warna']		= validasi_sql($this->input->post('modul_warna'));
			$insert['modul_status']		= 'A';
			$this->Modul_model->insert_modul($insert);
			
			$this->session->set_flashdata('success','Modul berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/modul', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';

		$data['list_warna'] = array("bg-red"=>"Red", "bg-yellow"=>"Yellow", "bg-aqua"=>"Aqua", "bg-blue"=>"Blue", "bg-green"=>"Green", "bg-navy"=>"Navy", "bg-teal"=>"Teal", "bg-olive"=>"Olive", "bg-lime"=>"Lime", "bg-orange"=>"Orange", "bg-fuchsia"=>"Fuchsia", "bg-purple"=>"Purple", "bg-maroon"=>"Maroon", "bg-black"=>"Black", "bg-light-blue"=>"Light Blue");
		
		$where['modul_id']		= validasi_sql($this->uri->segment(4)); 
		$modul 					= $this->Modul_model->get_modul('*', $where);
		
		$data['modul_id']		= ($this->input->post('modul_id'))?$this->input->post('modul_id'):$modul->modul_id;
		$data['modul_nama']		= ($this->input->post('modul_nama'))?$this->input->post('modul_nama'):$modul->modul_nama;
		$data['modul_dir']		= ($this->input->post('modul_dir'))?$this->input->post('modul_dir'):$modul->modul_dir;
		$data['modul_url']		= ($this->input->post('modul_url'))?$this->input->post('modul_url'):$modul->modul_url;
		$data['modul_tipe']		= ($this->input->post('modul_tipe'))?$this->input->post('modul_tipe'):$modul->modul_tipe;
		$data['modul_icon']		= ($this->input->post('modul_icon'))?$this->input->post('modul_icon'):$modul->modul_icon;
		$data['modul_urutan']	= ($this->input->post('modul_urutan'))?$this->input->post('modul_urutan'):$modul->modul_urutan;
		$data['modul_warna']	= ($this->input->post('modul_warna'))?$this->input->post('modul_warna'):$modul->modul_warna;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['modul_id']	= validasi_sql($this->input->post('modul_id'));
			$edit['modul_nama']		= validasi_sql($this->input->post('modul_nama'));
			$edit['modul_dir']		= validasi_sql($this->input->post('modul_dir'));
			$edit['modul_url']		= validasi_sql($this->input->post('modul_url'));
			$edit['modul_tipe']		= validasi_sql($this->input->post('modul_tipe'));
			$edit['modul_icon']		= validasi_sql($this->input->post('modul_icon'));			
			$edit['modul_urutan']	= validasi_sql($this->input->post('modul_urutan'));			
			$edit['modul_warna']	= validasi_sql($this->input->post('modul_warna'));			
			$this->Modul_model->update_modul($where_edit, $edit);
			
			$this->session->set_flashdata('success','Modul berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/modul', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['modul_id']	= validasi_sql($this->uri->segment(4));
		if ($this->Menu_model->count_all_menu(array('menu.menu_id'=>validasi_sql($this->uri->segment(4)))) < 1){
			$this->Modul_model->delete_modul($where_delete);
			$this->session->set_flashdata('success','Modul berhasil dihapus.');
		} else {
			$this->session->set_flashdata('error','Modul gagal dihapus. Modul digunakan menu.');
		}
		
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['modul_id']	= validasi_sql($this->uri->segment(4)); 
		$modul		 		= $this->Modul_model->get_modul('*', $where);
		
		$data['modul_id']		= $modul->modul_id;
		$data['modul_nama']		= $modul->modul_nama;
		$data['modul_dir']		= $modul->modul_dir;
		$data['modul_url']		= $modul->modul_url;
		$data['modul_tipe']		= $modul->modul_tipe;
		$data['modul_icon']		= $modul->modul_icon;
		$data['modul_urutan']	= $modul->modul_urutan;
		$data['modul_warna']	= $modul->modul_warna;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/modul', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
