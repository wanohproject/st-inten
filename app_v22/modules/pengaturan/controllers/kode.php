<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kode extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Kode | ' . profile('profil_website');;
		$this->active_menu		= 19;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Referensi_model');
    }
	
	function datatable()
	{
		header('Content-Type: application/json');
		$this->load->library('Datatables');
		$this->datatables->select('kode_id, kode_value, kode_nama, jenis_value, jenis_nama')
		->add_column('Actions', $this->get_buttons('$1'),'kode_id')
		->search_column('kode_value, kode_nama, jenis_value, jenis_nama')
		->from('ref_kode kode')
		->join('ref_jenis jenis', 'kode.jenis_id=jenis.jenis_id', 'left');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kode', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['kode_id']	= ($this->input->post('kode_id'))?$this->input->post('kode_id'):'';
		$data['kode_value']	= ($this->input->post('kode_value'))?$this->input->post('kode_value'):'';
		$data['kode_nama']	= ($this->input->post('kode_nama'))?$this->input->post('kode_nama'):'';
		$data['jenis_id']	= ($this->input->post('jenis_id'))?$this->input->post('jenis_id'):'';
		$save							= $this->input->post('save');
		if ($save){
			$insert['kode_value']			= validasi_sql($this->input->post('kode_value'));
			$insert['kode_nama']			= validasi_sql($this->input->post('kode_nama'));
			$insert['jenis_id']				= validasi_sql($this->input->post('jenis_id'));
			$this->Referensi_model->insert_kode($insert);
			
			$this->session->set_flashdata('success','Kode telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kode', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['kode_id']	= validasi_sql($this->uri->segment(4)); 
		$kode 				= $this->Referensi_model->get_kode('*', $where);
		$data['kode_id']	= ($this->input->post('kode_id'))?$this->input->post('kode_id'):$kode->kode_id;
		$data['kode_value']	= ($this->input->post('kode_value'))?$this->input->post('kode_value'):$kode->kode_value;
		$data['kode_nama']	= ($this->input->post('kode_nama'))?$this->input->post('kode_nama'):$kode->kode_nama;
		$data['jenis_id']	= ($this->input->post('jenis_id'))?$this->input->post('jenis_id'):$kode->jenis_id;
		$save				= $this->input->post('save');
		if ($save){
			$where_edit['kode_id']		= validasi_sql($this->input->post('kode_id'));
			$edit['kode_value']			= validasi_sql($this->input->post('kode_value'));
			$edit['kode_nama']			= validasi_sql($this->input->post('kode_nama'));
			$edit['jenis_id']			= validasi_sql($this->input->post('jenis_id'));					
			$this->Referensi_model->update_kode($where_edit, $edit);
			
			$this->session->set_flashdata('success','Kode telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kode', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['kode_id']	= validasi_sql($this->uri->segment(4));
		$this->Referensi_model->delete_kode($where_delete);
		
		$this->session->set_flashdata('success','Kode telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['kode_id']	= validasi_sql($this->uri->segment(4)); 
		$kode		 		= $this->Referensi_model->get_kode('*', $where);
		
		$data['kode_id']		= $kode->kode_id;
		$data['kode_value']		= $kode->kode_value;
		$data['kode_nama']		= $kode->kode_nama;
		$data['jenis_nama']		= $kode->jenis_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kode', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
