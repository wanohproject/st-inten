<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Hak_akses extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Hak Akses | ' . profile('profil_website');;
		$this->active_menu		= 21;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'hak_akses';
		
		$data['pengguna_level_id']	= ($this->input->post('pengguna_level_id'))?$this->input->post('pengguna_level_id'):'';
		$data['modul_id']			= ($this->input->post('modul_id'))?$this->input->post('modul_id'):'';
		$data['menu_tipe']			= 'B';
		$menu_tipe					= 'B';
		$menu						= $this->input->post('menu');
		$pengguna_level				= $this->input->post('pengguna_level_id');
		$modul						= $this->input->post('modul_id');
		$save						= $this->input->post('save');
		
		if ($save){
			if (!empty($pengguna_level)){
				if (!empty($menu)) {
					foreach($menu as $item){
						$where['menu_pengguna.menu_id'] 			= $item;
						$where['menu_pengguna.pengguna_level_id'] 	= $pengguna_level;		
						$jmlRow = $this->Menu_model->count_all_menu_pengguna($where);
						if($jmlRow < 1) {
							$datamenu = array(
							   'menu_id' 			=> $item,
							   'pengguna_level_id' 	=> $pengguna_level
							);
							$this->Menu_model->insert_menu_pengguna($datamenu);
						}
					}
				}
				
				$sKd = 0;
				if (!empty($menu)) {
					foreach($menu as $item){
						$nKd[] = "'$item'";
					}
						
					$sKd = implode(",", $nKd);
				}
				/*
				if (empty($menu_id)){
					$query = $this->db->query("SELECT * FROM menu_pengguna mp JOIN menu m ON mp.menu_id=m.menu_id WHERE mp.menu_id NOT IN($sKd) AND mp.pengguna_level_id = '$pengguna_level' AND m.menu_level='1'");
					if ($query->num_rows() > 0)
					{
					   foreach ($query->result() as $row)
					   {
						  $this->db->query("DELETE FROM menu_pengguna WHERE menu_pengguna_id='".$row->menu_pengguna_id."'");
					   }
					}						
				
				}
				*/
					
				$query = $this->db->query("SELECT * FROM menu_pengguna mp JOIN menu m ON mp.menu_id=m.menu_id WHERE mp.menu_id NOT IN($sKd) AND mp.pengguna_level_id = '$pengguna_level' AND m.menu_utama='0' AND m.modul_id='$modul'");
				if ($query->num_rows() > 0){
					foreach ($query->result() as $row){
						$listquery = $this->db->query("SELECT * FROM menu_pengguna mp JOIN menu m ON mp.menu_id=m.menu_id WHERE mp.menu_id NOT IN($sKd) AND mp.pengguna_level_id = '$pengguna_level' AND m.menu_utama='".$row->menu_id."' AND m.modul_id='$modul'");
						if ($listquery->num_rows() > 0){
							foreach ($listquery->result() as $row2){
								$listquery2 = $this->db->query("SELECT * FROM menu_pengguna mp JOIN menu m ON mp.menu_id=m.menu_id WHERE mp.menu_id NOT IN($sKd) AND mp.pengguna_level_id = '$pengguna_level' AND m.menu_utama='".$row2->menu_id."' AND m.modul_id='$modul'");
								if ($listquery2->num_rows() > 0){
									foreach ($listquery2->result() as $row3){
										$listquery3 = $this->db->query("SELECT * FROM menu_pengguna mp JOIN menu m ON mp.menu_id=m.menu_id WHERE mp.menu_id NOT IN($sKd) AND mp.pengguna_level_id = '$pengguna_level' AND m.menu_utama='".$row3->menu_id."' AND m.modul_id='$modul'");
										if ($listquery3->num_rows() > 0){
											foreach ($listquery3->result() as $row4){
												$listquery4 = $this->db->query("SELECT * FROM menu_pengguna mp JOIN menu m ON mp.menu_id=m.menu_id WHERE mp.menu_id NOT IN($sKd) AND mp.pengguna_level_id = '$pengguna_level' AND m.menu_utama='".$row4->menu_id."' AND m.modul_id='$modul'");
												if ($listquery4->num_rows() > 0){
													foreach ($listquery4->result() as $row5){
														$this->db->query("DELETE FROM menu_pengguna WHERE menu_pengguna_id='".$row5->menu_pengguna_id."'");
													}
												}
												$this->db->query("DELETE FROM menu_pengguna WHERE menu_pengguna_id='".$row4->menu_pengguna_id."'");
											}
										}
										$this->db->query("DELETE FROM menu_pengguna WHERE menu_pengguna_id='".$row3->menu_pengguna_id."'");
									}
								}
								$this->db->query("DELETE FROM menu_pengguna WHERE menu_pengguna_id='".$row2->menu_pengguna_id."'");
							}
						}
						$this->db->query("DELETE FROM menu_pengguna WHERE menu_pengguna_id='".$row->menu_pengguna_id."'");
					}
				}
			}
			//$this->session->set_flashdata('success','Hak akses telah berhasil diubah.');
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/hak_akses', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function index2()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'hak_akses2';
		
		$data['pengguna_level_id']	= ($this->input->post('pengguna_level_id'))?$this->input->post('pengguna_level_id'):'';
		$data['modul_id']			= ($this->input->post('modul_id'))?$this->input->post('modul_id'):'';
		$data['menu_tipe']			= 'B';
		$menu_tipe					= 'B';
		$menu						= $this->input->post('menu');
		$pengguna_level				= $this->input->post('pengguna_level_id');
		$modul						= $this->input->post('modul_id');
		$save						= $this->input->post('save');
		
		if ($save){
			if ($pengguna_level){
				$this->db->query("DELETE FROM menu_pengguna WHERE pengguna_level_id = '$pengguna_level'");
			} else {
				$this->db->query("DELETE FROM menu_pengguna");
			}
			foreach ($menu as $key => $value) {
				foreach ($menu[$key] as $key2 => $value2) {
					$datamenu = array(
						'menu_id' 				=> $key,
						'pengguna_level_id' 	=> $key2
					 );
					 $this->Menu_model->insert_menu_pengguna($datamenu);
				}
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/hak_akses', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
