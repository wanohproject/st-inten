<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Jenis Kode | ' . profile('profil_website');;
		$this->active_menu		= 20;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Referensi_model');
    }
	
	function datatable()
	{
		header('Content-Type: application/json');
		$this->load->library('Datatables');
		if ($this->session->userdata('level') != '1'){
			$this->datatables->select('jenis_id, jenis_value, jenis_nama')
			->add_column('Actions', $this->get_buttons('$1'),'jenis_id')
			->search_column('jenis_value, jenis_nama')
			->from('ref_jenis jenis')
			->where('jenis_id NOT IN (1)');
		} else {
			$this->datatables->select('jenis_id, jenis_value, jenis_nama')
			->add_column('Actions', $this->get_buttons('$1'),'jenis_id')
			->search_column('jenis_value, jenis_nama')
			->from('ref_jenis jenis');
		}
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['jenis_value']		= ($this->input->post('jenis_value'))?$this->input->post('jenis_value'):'';
		$data['jenis_nama']		= ($this->input->post('jenis_nama'))?$this->input->post('jenis_nama'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$insert['jenis_value']		= validasi_sql($this->input->post('jenis_value'));
			$insert['jenis_nama']		= validasi_sql($this->input->post('jenis_nama'));
			$this->Referensi_model->insert_jenis($insert);
			
			$this->session->set_flashdata('success','Kelompok Pengguna telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis', $data);
		$this->load->view(module_dir().'/separate/footer');
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['jenis_id']		= validasi_sql($this->uri->segment(4)); 
		$jenis 				= $this->Referensi_model->get_jenis('*', $where);
		
		$data['jenis_id']		= ($this->input->post('jenis_id'))?$this->input->post('jenis_id'):$jenis->jenis_id;
		$data['jenis_value']	= ($this->input->post('jenis_value'))?$this->input->post('jenis_value'):$jenis->jenis_value;
		$data['jenis_nama']	= ($this->input->post('jenis_nama'))?$this->input->post('jenis_nama'):$jenis->jenis_nama;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['jenis_id']	= validasi_sql($this->input->post('jenis_id'));
			$edit['jenis_value']		= validasi_sql($this->input->post('jenis_value'));
			$edit['jenis_nama']		= validasi_sql($this->input->post('jenis_nama'));
			$this->Referensi_model->update_jenis($where_edit, $edit);
			
			$this->session->set_flashdata('success','Kelompok Pengguna telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['jenis_id']	= validasi_sql($this->uri->segment(4));
		$this->Referensi_model->delete_jenis($where_delete);
		
		$this->session->set_flashdata('success','Kelompok Pengguna telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['jenis_id']	= validasi_sql($this->uri->segment(4)); 
		$jenis		 		= $this->Referensi_model->get_jenis('*', $where);
		
		$data['jenis_id']		= $jenis->jenis_id;
		$data['jenis_value']	= $jenis->jenis_value;
		$data['jenis_nama']	= $jenis->jenis_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
