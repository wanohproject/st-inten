<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Halaman extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= profile('profil_institusi') . ' | Halaman';
		$this->active_menu		= 30;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Tulisan_model');
		$this->load->model('Menu_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('tulisan_id, tulisan_nama')
		// ->add_column('tulisan_link', $this->get_link('$1'),'tulisan_permalink')
		->add_column('Actions', $this->get_buttons('$1'),'tulisan_id')
		->search_column('tulisan_nama')
		->from('tulisan')
		->where('tulisan_tipe', 'page')
		->where('tulisan_status', 'Y');
        echo $this->datatables->generate();
    }
	
	function get_link($permalink)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		return site_url('page/view/'.$permalink);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$head['active']		= $this->active_menu;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/halaman', $data);
		$this->load->view(module_dir().'/separate/footer');
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$head['active']		= $this->active_menu;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['ckeditor'] 			= ckeditor('', 'tulisan_deskripsi', 300);
		$data['tulisan_id']			= ($this->input->post('tulisan_id'))?$this->input->post('tulisan_id'):'';
		$data['tulisan_nama']		= ($this->input->post('tulisan_nama'))?$this->input->post('tulisan_nama'):'';
		$data['tulisan_permalink']	= ($this->input->post('tulisan_permalink'))?$this->input->post('tulisan_permalink'):'';
		$data['tulisan_deskripsi']	= ($this->input->post('tulisan_deskripsi'))?$this->input->post('tulisan_deskripsi'):'';
		$data['tulisan_thumbnail']	= ($this->input->post('tulisan_thumbnail'))?$this->input->post('tulisan_thumbnail'):'';
		
		$data['menu']				= ($this->input->post('menu'))?$this->input->post('menu'):'';
		$save						= $this->input->post('save');
		if ($save){
			$thumbnail						= upload_image("tulisan_thumbnail", "./asset/image/artikel/", "240x240", seo($data['tulisan_nama']));
			$data['tulisan_thumbnail']		= $thumbnail;
		
			$insert['tulisan_nama']			= $this->input->post('tulisan_nama');
			$insert['tulisan_permalink']	= $this->input->post('tulisan_permalink');
			$insert['tulisan_deskripsi']	= html_encode(str_replace(base_url(), "http_base_url/", $this->input->post('tulisan_deskripsi')));
			if ($data['tulisan_thumbnail']){
				$insert['tulisan_thumbnail']	= $data['tulisan_thumbnail'];
			}
			
			$insert['tulisan_waktu']		= date("Y-m-d H:i:s");
			$insert['tulisan_tipe']			= 'page';
			$insert['tulisan_status']		= 'Y';
			$insert['pengguna_id']			= userdata('pengguna_id');
			$this->Tulisan_model->insert_tulisan($insert);
			
			if ($this->input->post('menu')){
				$where_menu['menu_id']			= $this->input->post('menu');
				$update_menu['menu_url']		= 'page/view/'.$this->input->post('tulisan_permalink');
				$update_menu['menu_siteurl']	= 'A';
				$this->Menu_model->update_menu($where_menu, $update_menu);
			}
			
			$this->session->set_flashdata('success','Halaman telah berhasil ditambah!,');
			redirect(module_url($this->uri->segment(2)));
		}
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/halaman', $data);
		$this->load->view(module_dir().'/separate/footer');
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$head['active']		= $this->active_menu;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$data['ckeditor'] 			= ckeditor('', 'tulisan_deskripsi', 300);
		$where['tulisan_id']		= validasi_sql($this->uri->segment(4)); 
		$tulisan 					= $this->Tulisan_model->get_tulisan('*', $where);
		$data['tulisan_id']			= ($this->input->post('tulisan_id'))?$this->input->post('tulisan_id'):$tulisan->tulisan_id;
		$data['tulisan_nama']		= ($this->input->post('tulisan_nama'))?$this->input->post('tulisan_nama'):$tulisan->tulisan_nama;
		$data['tulisan_permalink']	= ($this->input->post('tulisan_permalink'))?$this->input->post('tulisan_permalink'):$tulisan->tulisan_permalink;
		$data['tulisan_deskripsi']	= ($this->input->post('tulisan_deskripsi'))?$this->input->post('tulisan_deskripsi'):$tulisan->tulisan_deskripsi;
		$data['tulisan_thumbnail']	= ($this->input->post('tulisan_thumbnail'))?$this->input->post('tulisan_thumbnail'):$tulisan->tulisan_thumbnail;
		
		$get_menu = $this->Menu_model->get_menu('*', array('menu_url'=>'page/view/'.$tulisan->tulisan_permalink));
		if ($get_menu){
			$data['menu']				= ($this->input->post('menu'))?$this->input->post('menu'):$get_menu->menu_id;
		} else {
			$data['menu']				= ($this->input->post('menu'))?$this->input->post('menu'):'';
		}
		
		$save						= $this->input->post('save');
		if ($save){
			$thumbnail						= upload_image("tulisan_thumbnail", "./asset/image/artikel/", "240x240", seo($data['tulisan_nama']));
			$data['tulisan_thumbnail']		= $thumbnail;
		
			$update['tulisan_nama']			= $this->input->post('tulisan_nama');
			$update['tulisan_permalink']	= $this->input->post('tulisan_permalink');
			$update['tulisan_deskripsi']	= html_encode($this->input->post('tulisan_deskripsi'));
			if ($data['tulisan_thumbnail']){
				$update['tulisan_thumbnail']	= $data['tulisan_thumbnail'];
			}
			$update['tulisan_waktu']		= date("Y-m-d H:i:s");
			$update['pengguna_id']			= userdata('pengguna_id');
			
			$where_update['tulisan_id']		= $data['tulisan_id'];
			$this->Tulisan_model->update_tulisan($where_update, $update);
			
			if ($this->input->post('menu')){
				$where_menu['menu_id']			= $this->input->post('menu');
				$update_menu['menu_url']		= 'page/view/'.$this->input->post('tulisan_permalink');
				$update_menu['menu_siteurl']	= 'A';
				$this->Menu_model->update_menu($where_menu, $update_menu);
			}
			
			$this->session->set_flashdata('success','Halaman telah berhasil diubah!,');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/halaman', $data);
		$this->load->view(module_dir().'/separate/footer');
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$head['active']		= $this->active_menu;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['tulisan_id']	= validasi_sql($this->uri->segment(4));
		$this->Tulisan_model->delete_tulisan($where_delete);
		
		$this->session->set_flashdata('success','Halaman telah berhasil dihapus!,');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$head['active']		= $this->active_menu;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$data['ckeditor'] 			= ckeditor('notoolbar', 'tulisan_deskripsi', 300);
		
		$where['tulisan_id']		= validasi_sql($this->uri->segment(4)); 
		$tulisan		 			= $this->Tulisan_model->get_tulisan('*', $where);
		
		$data['tulisan_id']			= $tulisan->tulisan_id;
		$data['tulisan_nama']		= $tulisan->tulisan_nama;
		$data['tulisan_permalink']	= $tulisan->tulisan_permalink;
		$data['tulisan_deskripsi']	= $tulisan->tulisan_deskripsi;
		$data['tulisan_thumbnail']	= $tulisan->tulisan_thumbnail;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/halaman', $data);
		$this->load->view(module_dir().'/separate/footer');
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function check_permalink()
	{
		$action = $this->input->post('action');
		if($action == 'check_permalink')
		{
			$where['tulisan_permalink']	= $this->input->post('tulisan_permalink');
			if ($this->input->post('tulisan_id')){
				$where['tulisan_id !=']	= $this->input->post('tulisan_id');
			}
			if($this->Tulisan_model->count_all_tulisan($where) > 0)
			{
				$data['success'] = FALSE;
				$data['message'] = "<span class='error'><img src=\"".theme_dir('admin_v2/dist/img/error.png')."\" style=\"height: 15px\" /></span>";
			}
			else
			{
				$data['success'] = TRUE;
				$data['message'] = "<span class='success'><img src=\"".theme_dir('admin_v2/dist/img/success.png')."\" style=\"height: 15px\" /></span>";
			}
			echo json_encode($data);
		}
	}
}
