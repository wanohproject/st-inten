<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('module_dir'))
{
	function module_dir($data=""){
		$obj =& get_instance();
		if ($data){
			return 'presensi/' . $data;
		} else {
			return 'presensi';
		}
	}
}

if ( ! function_exists('module_url'))
{
	function module_url($data=""){
		$obj =& get_instance();
		if ($data){
			return site_url('presensi/' . $data);
		} else {
			return site_url('presensi');
		}
	}
}

if ( ! function_exists('module_info'))
{
	function module_info($param1="modul_nama"){
		$obj =& get_instance();
		$obj->load->model('Modul_model');
		$module_name = $obj->router->fetch_module();
		$where['modul_dir'] = $module_name;
		$modul = $obj->Modul_model->get_modul('*', $where);
		if ($modul){
			if (isset($modul->$param1)){
				return $modul->$param1;
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}

if ( ! function_exists('getExtension'))
{
	function getExtension($str)
	{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
}

if ( ! function_exists('Parse_Data'))
{
	function Parse_Data($data,$p1,$p2){
		$data=" ".$data;
		$hasil="";
		$awal=strpos($data,$p1);
		if($awal!=""){
			$akhir=strpos(strstr($data,$p1),$p2);
			if($akhir!=""){
				$hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
			}
		}
		return $hasil;	
	}
}

if ( ! function_exists('loadpresensi'))
{
	function loadpresensi($data){
		if ($data) {
			$tahun_kode 		= $data['tahun_kode'];
			$semester_id 	= $data['semester_id'];
			$PIN 			= $data['user_id'];
			$DateTime 		= $data['datetime'];
			$obj =& get_instance();
			$obj->load->model('Sidik_jari_model');
			$obj->load->model('Jadwal_model');
			$obj->load->model('Presensi_model');
			$obj->load->model('Log_model');
			$sidik_jari = $obj->Sidik_jari_model->get_sidik_jari('', array('sidik_jari.user_id'=>$PIN));
			if ($sidik_jari){
				$N = date('N', strtotime($DateTime));
				$date = substr($DateTime, 0, 10);
				$time = substr($DateTime, 11, 8);
				$jadwal = $obj->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari->sidik_jari_level));
				if ($jadwal){
					if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
						if ($count_presensi < 1){
							$insert_presensi = array();
							$insert_presensi['presensi_id'] = $obj->uuid->v4();
							$insert_presensi['tahun_kode'] = $tahun_kode;
							$insert_presensi['semester_id'] = $semester_id;
							$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
							$insert_presensi['staf_id'] = $sidik_jari->staf_id;
							$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
							$insert_presensi['created_by'] = userdata('pengguna_id');
							$insert_presensi['created_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->insert_presensi($insert_presensi);
							
							$res['status']	= 1;
							$res['message']	= 'Presensi Masuk Berhasil';

							return $res;
						} else {
							$res['status']	= 2;
							$res['message']	= 'Anda Sudah Presensi Masuk';

							return $res;
						}
					} else if (strtotime($DateTime) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
						if ($count_presensi < 1){
							$insert_presensi = array();
							$insert_presensi['presensi_id'] = $obj->uuid->v4();
							$insert_presensi['tahun_kode'] = $tahun_kode;
							$insert_presensi['semester_id'] = $semester_id;
							$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
							$insert_presensi['staf_id'] = $sidik_jari->staf_id;
							$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
							$insert_presensi['created_by'] = userdata('pengguna_id');
							$insert_presensi['created_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->insert_presensi($insert_presensi);
							
							$res['status']	= 9;
							$res['message']	= 'Anda Terlambat Presensi Masuk';

							return $res;
						} else {
							$res['status']	= 2;
							$res['message']	= 'Anda Sudah Presensi Masuk';

							return $res;
						}
					} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
						$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
						if ($count_presensi > 0){
							$update_presensi = array();
							$update_presensi['tahun_kode'] = $tahun_kode;
							$update_presensi['semester_id'] = $semester_id;
							$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$update_presensi['presensi_tanggal_keluar'] = $DateTime;
							$update_presensi['updated_by'] = userdata('pengguna_id');
							$update_presensi['updated_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
							
							$res['status']	= 3;
							$res['message']	= 'Presensi Pulang Berhasil';
							
							return $res;
						} else if ($count_presensi2 > 0){
							$res['status']	= 6;
							$res['message']	= 'Anda Sudah Presensi Pulang';
							
							return $res;
						} else {
							$res['status']	= 4;
							$res['message']	= 'Anda Tidak Presensi Masuk';
							return $res;
						}
					} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
						$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
						if ($count_presensi > 0){
							$update_presensi = array();
							$update_presensi['tahun_kode'] = $tahun_kode;
							$update_presensi['semester_id'] = $semester_id;
							$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$update_presensi['presensi_tanggal_keluar'] = $DateTime;
							$update_presensi['updated_by'] = userdata('pengguna_id');
							$update_presensi['updated_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
							
							$res['status']	= 10;
							$res['message']	= 'Anda Presensi Pulang Cepat';
							
							return $res;
						} else if ($count_presensi2 > 0){
							$res['status']	= 6;
							$res['message']	= 'Anda Sudah Presensi Pulang';
							
							return $res;
						} else {
							$res['status']	= 4;
							$res['message']	= 'Anda Tidak Presensi Masuk';
							return $res;
						}
					} else {
						$res['status']	= 5;
						$res['message']	= 'Belum Jadwal Presensi';
						return $res;
					}
				} else {
					$res['status']	= 7;
					$res['message']	= 'Tidak Ada Jadwal';
					return $res;
				}
			} else {
				$res['status']	= 8;
				$res['message']	= 'Sidik Jari Tidak Terdaftar';
				return $res;
			}
		} else {
			$res['status']	= 0;
			$res['message']	= 'Parameter Not Found';
			return $res;
		}
	}
}

if ( ! function_exists('loadmanual'))
{
	function loadmanual($data, $type='staf'){
		if ($data) {
			$tahun_kode 		= $data['tahun_kode'];
			$semester_id 	= $data['semester_id'];
			$PIN 			= $data['user_id'];
			$DateTime 		= $data['datetime'];
			$obj =& get_instance();
			$obj->load->model('Sidik_jari_model');
			$obj->load->model('Jadwal_model');
			$obj->load->model('Presensi_model');
			$obj->load->model('Log_model');
			if ($type == 'staf'){
				$sidik_jari = $obj->Sidik_jari_model->get_sidik_jari('', array('sidik_jari.staf_id'=>$PIN));
			} else if ($type == 'siswa'){
				$sidik_jari = $obj->Sidik_jari_model->get_sidik_jari('', array('sidik_jari.siswa_id'=>$PIN));
			}
			if ($sidik_jari){
				$N = date('N', strtotime($DateTime));
				$date = substr($DateTime, 0, 10);
				$time = substr($DateTime, 11, 8);
				$jadwal = $obj->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari->sidik_jari_level));
				if ($jadwal){
					if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
						if ($count_presensi < 1){
							$insert_presensi = array();
							$insert_presensi['presensi_id'] = $obj->uuid->v4();
							$insert_presensi['tahun_kode'] = $tahun_kode;
							$insert_presensi['semester_id'] = $semester_id;
							$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
							$insert_presensi['presensi_sumber'] = 'M';
							$insert_presensi['staf_id'] = $sidik_jari->staf_id;
							$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
							$insert_presensi['created_by'] = userdata('pengguna_id');
							$insert_presensi['created_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->insert_presensi($insert_presensi);
							
							$res['status']	= 1;
							$res['message']	= 'Presensi Masuk Berhasil';

							return $res;
						} else {
							$res['status']	= 2;
							$res['message']	= 'Anda Sudah Presensi Masuk';

							return $res;
						}
					} else if (strtotime($DateTime) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
						if ($count_presensi < 1){
							$insert_presensi = array();
							$insert_presensi['presensi_id'] = $obj->uuid->v4();
							$insert_presensi['tahun_kode'] = $tahun_kode;
							$insert_presensi['semester_id'] = $semester_id;
							$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
							$insert_presensi['presensi_sumber'] = 'M';
							$insert_presensi['staf_id'] = $sidik_jari->staf_id;
							$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
							$insert_presensi['created_by'] = userdata('pengguna_id');
							$insert_presensi['created_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->insert_presensi($insert_presensi);
							
							$res['status']	= 9;
							$res['message']	= 'Presensi Masuk Berhasil';

							return $res;
						} else {
							$res['status']	= 2;
							$res['message']	= 'Anda Sudah Presensi Masuk';

							return $res;
						}
					} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
						$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
						if ($count_presensi > 0){
							$update_presensi = array();
							$update_presensi['tahun_kode'] = $tahun_kode;
							$update_presensi['semester_id'] = $semester_id;
							$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$update_presensi['presensi_tanggal_keluar'] = $DateTime;
							$update_presensi['updated_by'] = userdata('pengguna_id');
							$update_presensi['updated_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
							
							$res['status']	= 3;
							$res['message']	= 'Presensi Pulang Berhasil';
							
							return $res;
						} else if ($count_presensi2 > 0){
							$res['status']	= 6;
							$res['message']	= 'Anda Sudah Presensi Pulang';
							
							return $res;
						} else {
							$res['status']	= 4;
							$res['message']	= 'Anda Tidak Presensi Masuk';
							return $res;
						}
					} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
						$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
						if ($count_presensi > 0){
							$update_presensi = array();
							$update_presensi['tahun_kode'] = $tahun_kode;
							$update_presensi['semester_id'] = $semester_id;
							$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$update_presensi['presensi_tanggal_keluar'] = $DateTime;
							$update_presensi['updated_by'] = userdata('pengguna_id');
							$update_presensi['updated_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
							
							$res['status']	= 10;
							$res['message']	= 'Presensi Pulang Berhasil';
							
							return $res;
						} else if ($count_presensi2 > 0){
							$res['status']	= 6;
							$res['message']	= 'Anda Sudah Presensi Pulang';
							
							return $res;
						} else {
							$res['status']	= 4;
							$res['message']	= 'Anda Tidak Presensi Masuk';
							return $res;
						}
					} else {
						$res['status']	= 5;
						$res['message']	= 'Belum Jadwal Presensi';
						return $res;
					}
				} else {
					$res['status']	= 7;
					$res['message']	= 'Tidak Ada Jadwal';
					return $res;
				}
			} else {
				$res['status']	= 8;
				$res['message']	= 'Sidik Jari Tidak Terdaftar';
				return $res;
			}
		} else {
			$res['status']	= 0;
			$res['message']	= 'Parameter Not Found';
			return $res;
		}
	}
}

if ( ! function_exists('loadsync'))
{
	function loadsync($data){
		if ($data) {
			$tahun_kode 		= $data['tahun_kode'];
			$semester_id 	= $data['semester_id'];
			$PIN 			= $data['user_id'];
			$DateTime 		= $data['datetime'];
			$obj =& get_instance();
			$obj->load->model('Sidik_jari_model');
			$obj->load->model('Jadwal_model');
			$obj->load->model('Presensi_model');
			$obj->load->model('Log_model');
			$sidik_jari = $obj->Sidik_jari_model->get_sidik_jari('', array('sidik_jari.user_id'=>$PIN));
			if ($sidik_jari){
				$N = date('N', strtotime($DateTime));
				$date = substr($DateTime, 0, 10);
				$time = substr($DateTime, 11, 8);
				$jadwal = $obj->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari->sidik_jari_level));
				if ($jadwal){
					if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
						if ($count_presensi < 1){
							$insert_presensi = array();
							$insert_presensi['presensi_id'] = $obj->uuid->v4();
							$insert_presensi['tahun_kode'] = $tahun_kode;
							$insert_presensi['semester_id'] = $semester_id;
							$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
							$insert_presensi['staf_id'] = $sidik_jari->staf_id;
							$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
							$insert_presensi['created_by'] = userdata('pengguna_id');
							$insert_presensi['created_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->insert_presensi($insert_presensi);
							
							$res['status']	= 1;
							$res['message']	= 'Presensi Masuk Berhasil';

							return $res;
						} else {
							$res['status']	= 2;
							$res['message']	= 'Anda Sudah Presensi Masuk';

							return $res;
						}
					} else if (strtotime($DateTime) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
						if ($count_presensi < 1){
							$insert_presensi = array();
							$insert_presensi['presensi_id'] = $obj->uuid->v4();
							$insert_presensi['tahun_kode'] = $tahun_kode;
							$insert_presensi['semester_id'] = $semester_id;
							$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
							$insert_presensi['staf_id'] = $sidik_jari->staf_id;
							$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
							$insert_presensi['created_by'] = userdata('pengguna_id');
							$insert_presensi['created_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->insert_presensi($insert_presensi);
							
							$res['status']	= 9;
							$res['message']	= 'Anda Terlambat Presensi Masuk';

							return $res;
						} else {
							$res['status']	= 2;
							$res['message']	= 'Anda Sudah Presensi Masuk';

							return $res;
						}
					} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
						$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
						if ($count_presensi > 0){
							$update_presensi = array();
							$update_presensi['tahun_kode'] = $tahun_kode;
							$update_presensi['semester_id'] = $semester_id;
							$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$update_presensi['presensi_tanggal_keluar'] = $DateTime;
							$update_presensi['updated_by'] = userdata('pengguna_id');
							$update_presensi['updated_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
							
							$res['status']	= 3;
							$res['message']	= 'Presensi Pulang Berhasil';
							
							return $res;
						} else if ($count_presensi2 > 0){
							$res['status']	= 6;
							$res['message']	= 'Anda Sudah Presensi Pulang';
							
							return $res;
						} else {
							$res['status']	= 4;
							$res['message']	= 'Anda Tidak Presensi Masuk';
							return $res;
						}
					} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
						$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
						$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
						if ($count_presensi > 0){
							$update_presensi = array();
							$update_presensi['tahun_kode'] = $tahun_kode;
							$update_presensi['semester_id'] = $semester_id;
							$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
							$update_presensi['presensi_tanggal_keluar'] = $DateTime;
							$update_presensi['updated_by'] = userdata('pengguna_id');
							$update_presensi['updated_at'] = date('Y-m-d H:i:s');
							$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
							
							$res['status']	= 10;
							$res['message']	= 'Anda Presensi Pulang Cepat';
							
							return $res;
						} else if ($count_presensi2 > 0){
							$res['status']	= 6;
							$res['message']	= 'Anda Sudah Presensi Pulang';
							
							return $res;
						} else {
							$res['status']	= 4;
							$res['message']	= 'Anda Tidak Presensi Masuk';
							return $res;
						}
					} else {
						$res['status']	= 5;
						$res['message']	= 'Belum Jadwal Presensi';
						return $res;
					}
				} else {
					$res['status']	= 7;
					$res['message']	= 'Tidak Ada Jadwal';
					return $res;
				}
			} else {
				$res['status']	= 8;
				$res['message']	= 'Sidik Jari Tidak Terdaftar';
				return $res;
			}
		} else {
			$res['status']	= 0;
			$res['message']	= 'Parameter Not Found';
			return $res;
		}
	}
}

if ( ! function_exists('loadpresensiV2'))
{
	function loadpresensiV2($data){
		if ($data) {
			$tahun_kode 		= $data['tahun_kode'];
			$semester_id 	= $data['semester_id'];
			$PIN 			= $data['user_id'];
			$DateTime 		= $data['datetime'];
			$Verified 		= $data['verified'];
			$Status 		= $data['status'];
			$obj =& get_instance();
			$obj->load->model('Sidik_jari_model');
			$obj->load->model('Jadwal_model');
			$obj->load->model('Presensi_model');
			$obj->load->model('Log_model');
			$sidik_jari = $obj->Sidik_jari_model->get_sidik_jari('', array('sidik_jari.user_id'=>$PIN));
			if ($sidik_jari){
				$N = date('N', strtotime($DateTime));
				$date = substr($DateTime, 0, 10);
				$time = substr($DateTime, 11, 8);
				$jadwal = $obj->Jadwal_model->get_jadwal('*', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari->sidik_jari_level));
				if ($jadwal){
					if ($jadwal->jadwal_jenis == 1){
						if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
							if ($count_presensi < 1){
								$insert_presensi = array();
								$insert_presensi['presensi_id'] = $obj->uuid->v4();
								$insert_presensi['tahun_kode'] = $tahun_kode;
								$insert_presensi['semester_id'] = $semester_id;
								$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
								$insert_presensi['staf_id'] = $sidik_jari->staf_id;
								$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
								$insert_presensi['jadwal_id'] = $jadwal->jadwal_id;
								$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
								$insert_presensi['created_by'] = userdata('pengguna_id');
								$insert_presensi['created_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->insert_presensi($insert_presensi);
								
								$res['status']	= 1;
								$res['message']	= 'Presensi Masuk Berhasil';
	
								return $res;
							} else {
								$res['status']	= 2;
								$res['message']	= 'Anda Sudah Presensi Masuk';
	
								return $res;
							}
						} else if (strtotime($DateTime) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
							if ($count_presensi < 1){
								$insert_presensi = array();
								$insert_presensi['presensi_id'] = $obj->uuid->v4();
								$insert_presensi['tahun_kode'] = $tahun_kode;
								$insert_presensi['semester_id'] = $semester_id;
								$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
								$insert_presensi['staf_id'] = $sidik_jari->staf_id;
								$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
								$insert_presensi['jadwal_id'] = $jadwal->jadwal_id;
								$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
								$insert_presensi['created_by'] = userdata('pengguna_id');
								$insert_presensi['created_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->insert_presensi($insert_presensi);
								
								$res['status']	= 9;
								$res['message']	= 'Anda Terlambat Presensi Masuk';
	
								return $res;
							} else {
								$res['status']	= 2;
								$res['message']	= 'Anda Sudah Presensi Masuk';
	
								return $res;
							}
						} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
							$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
							if ($count_presensi > 0){
								$update_presensi = array();
								$update_presensi['tahun_kode'] = $tahun_kode;
								$update_presensi['semester_id'] = $semester_id;
								$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$update_presensi['presensi_tanggal_keluar'] = $DateTime;
								$update_presensi['updated_by'] = userdata('pengguna_id');
								$update_presensi['updated_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
								
								$res['status']	= 3;
								$res['message']	= 'Presensi Pulang Berhasil';
								
								return $res;
							} else if ($count_presensi2 > 0){
								$res['status']	= 6;
								$res['message']	= 'Anda Sudah Presensi Pulang';
								
								return $res;
							} else {
								$res['status']	= 4;
								$res['message']	= 'Anda Tidak Presensi Masuk';
								return $res;
							}
						} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
							$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
							if ($count_presensi > 0){
								$update_presensi = array();
								$update_presensi['tahun_kode'] = $tahun_kode;
								$update_presensi['semester_id'] = $semester_id;
								$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$update_presensi['presensi_tanggal_keluar'] = $DateTime;
								$update_presensi['updated_by'] = userdata('pengguna_id');
								$update_presensi['updated_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
								
								$res['status']	= 10;
								$res['message']	= 'Anda Presensi Pulang Cepat';
								
								return $res;
							} else if ($count_presensi2 > 0){
								$res['status']	= 6;
								$res['message']	= 'Anda Sudah Presensi Pulang';
								
								return $res;
							} else {
								$res['status']	= 4;
								$res['message']	= 'Anda Tidak Presensi Masuk';
								return $res;
							}
						} else {
							$res['status']	= 5;
							$res['message']	= 'Belum Jadwal Presensi';
							return $res;
						}
					} else if ($jadwal->jadwal_jenis == 2){
						$status_nama = $obj->db->query("SELECT * FROM sat_kode WHERE kode_no = '$Status'")->row()->kode_nama;
						$jadwal_ = new stdClass();
						if ($status_nama == "Masuk"){
							$jadwal_ = $obj->Jadwal_model->get_jadwal('*', "jadwal_hari = '$N' AND jadwal_level = '$sidik_jari->sidik_jari_level' AND jadwal_masuk_awal <= '$time' AND jadwal_masuk_akhir >= '$time'");
							if ($jadwal_){
								if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_masuk_awal) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_masuk_tepat)){
									$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
									if ($count_presensi < 1){
										$insert_presensi = array();
										$insert_presensi['presensi_id'] = $obj->uuid->v4();
										$insert_presensi['tahun_kode'] = $tahun_kode;
										$insert_presensi['semester_id'] = $semester_id;
										$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
										$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
										$insert_presensi['staf_id'] = $sidik_jari->staf_id;
										$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
										$insert_presensi['jadwal_id'] = $jadwal_->jadwal_id;
										$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
										$insert_presensi['created_by'] = userdata('pengguna_id');
										$insert_presensi['created_at'] = date('Y-m-d H:i:s');
										$obj->Presensi_model->insert_presensi($insert_presensi);
										
										$res['status']	= 1;
										$res['message']	= 'Presensi Masuk Berhasil';
			
										return $res;
									} else {
										$res['status']	= 2;
										$res['message']	= 'Anda Sudah Presensi Masuk';
			
										return $res;
									}
								} else if (strtotime($DateTime) > strtotime($date.' '.$jadwal_->jadwal_masuk_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_masuk_akhir)){
									$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
									if ($count_presensi < 1){
										$insert_presensi = array();
										$insert_presensi['presensi_id'] = $obj->uuid->v4();
										$insert_presensi['tahun_kode'] = $tahun_kode;
										$insert_presensi['semester_id'] = $semester_id;
										$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
										$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
										$insert_presensi['staf_id'] = $sidik_jari->staf_id;
										$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
										$insert_presensi['jadwal_id'] = $jadwal_->jadwal_id;
										$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
										$insert_presensi['created_by'] = userdata('pengguna_id');
										$insert_presensi['created_at'] = date('Y-m-d H:i:s');
										$obj->Presensi_model->insert_presensi($insert_presensi);
										
										$res['status']	= 9;
										$res['message']	= 'Anda Terlambat Presensi Masuk';
			
										return $res;
									} else {
										$res['status']	= 2;
										$res['message']	= 'Anda Sudah Presensi Masuk';
			
										return $res;
									}
								} else {
									$res['status']	= 5;
									$res['message']	= 'Belum Jadwal Presensi';
									return $res;
								}
							} else {
								$res['status']	= 5;
								$res['message']	= 'Belum Jadwal Presensi';
								return $res;
							}
						} else if ($status_nama == "Pulang"){
							$jadwal_ = $obj->Jadwal_model->get_jadwal('*', "jadwal_hari = '$N' AND jadwal_level = '$sidik_jari->sidik_jari_level' AND jadwal_pulang_awal <= '$time' AND jadwal_pulang_akhir >= '$time'");
							if ($jadwal_){
								if (strtotime($jadwal_->jadwal_pulang_tepat) < strtotime($jadwal_->jadwal_masuk_tepat)){
									if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_pulang_akhir)){
										$date_ = date("Y-m-d", strtotime($date."-1 day"));
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 3;
											$res['message']	= 'Presensi Pulang Berhasil';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal_->jadwal_pulang_tepat)){
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 10;
											$res['message']	= 'Anda Presensi Pulang Cepat';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else {
										$res['status']	= 5;
										$res['message']	= 'Belum Jadwal Presensi';
										return $res;
									}
								} else {
									if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_pulang_akhir)){
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 3;
											$res['message']	= 'Presensi Pulang Berhasil';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal_->jadwal_pulang_tepat)){
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 10;
											$res['message']	= 'Anda Presensi Pulang Cepat';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else {
										$res['status']	= 5;
										$res['message']	= 'Belum Jadwal Presensi';
										return $res;
									}
								}
							} else {
								$res['status']	= 5;
								$res['message']	= 'Belum Jadwal Presensi';
								return $res;
							}
						}

					} else if ($jadwal->jadwal_jenis == 3){
						$status_nama = $obj->db->query("SELECT * FROM sat_kode WHERE kode_no = '$Status'")->row()->kode_nama;
						$jadwal_ = new stdClass();
						if ($status_nama == "Masuk"){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar) IS NULL ORDER BY created_at DESC")->num_rows();
							if ($count_presensi < 1){
								$insert_presensi = array();
								$insert_presensi['presensi_id'] = $obj->uuid->v4();
								$insert_presensi['tahun_kode'] = $tahun_kode;
								$insert_presensi['semester_id'] = $semester_id;
								$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
								$insert_presensi['staf_id'] = $sidik_jari->staf_id;
								$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
								$insert_presensi['jadwal_id'] = $jadwal->jadwal_id;
								$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
								$insert_presensi['created_by'] = userdata('pengguna_id');
								$insert_presensi['created_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->insert_presensi($insert_presensi);
								
								$res['status']	= 1;
								$res['message']	= 'Presensi Masuk Berhasil';
	
								return $res;
							} else {
								$res['status']	= 2;
								$res['message']	= 'Anda Sudah Presensi Masuk';
	
								return $res;
							}
						} else if ($status_nama == "Pulang"){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL ORDER BY created_at DESC")->num_rows();
							$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date' ORDER BY created_at DESC")->num_rows();
							if ($count_presensi > 0){
								$update_presensi = array();
								$update_presensi['tahun_kode'] = $tahun_kode;
								$update_presensi['semester_id'] = $semester_id;
								$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$update_presensi['presensi_tanggal_keluar'] = $DateTime;
								$update_presensi['updated_by'] = userdata('pengguna_id');
								$update_presensi['updated_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
								
								$res['status']	= 3;
								$res['message']	= 'Presensi Pulang Berhasil';
								
								return $res;
							} else if ($count_presensi2 > 0){
								$res['status']	= 6;
								$res['message']	= 'Anda Sudah Presensi Pulang';
								
								return $res;
							} else {
								$res['status']	= 4;
								$res['message']	= 'Anda Tidak Presensi Masuk';
								return $res;
							}
						}
					}
				} else {
					$res['status']	= 7;
					$res['message']	= 'Tidak Ada Jadwal';
					return $res;
				}
			} else {
				$res['status']	= 8;
				$res['message']	= 'Sidik Jari Tidak Terdaftar';
				return $res;
			}
		} else {
			$res['status']	= 0;
			$res['message']	= 'Parameter Not Found';
			return $res;
		}
	}
}

if ( ! function_exists('loadmanualV2'))
{
	function loadmanualV2($data, $type='staf'){
		if ($data) {
			$tahun_kode 		= $data['tahun_kode'];
			$semester_id 	= $data['semester_id'];
			$PIN 			= $data['user_id'];
			$DateTime 		= $data['datetime'];
			$Verified 		= $data['verified'];
			$Status 		= $data['status'];
			$obj =& get_instance();
			$obj->load->model('Sidik_jari_model');
			$obj->load->model('Jadwal_model');
			$obj->load->model('Presensi_model');
			$obj->load->model('Log_model');
			if ($type == 'staf'){
				$sidik_jari = $obj->Sidik_jari_model->get_sidik_jari('', array('sidik_jari.staf_id'=>$PIN));
			} else if ($type == 'siswa'){
				$sidik_jari = $obj->Sidik_jari_model->get_sidik_jari('', array('sidik_jari.siswa_id'=>$PIN));
			}
			if ($sidik_jari){
				$N = date('N', strtotime($DateTime));
				$date = substr($DateTime, 0, 10);
				$time = substr($DateTime, 11, 8);
				$jadwal = $obj->Jadwal_model->get_jadwal('*', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari->sidik_jari_level));
				if ($jadwal){
					if ($jadwal->jadwal_jenis == 1){
						if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
							if ($count_presensi < 1){
								$insert_presensi = array();
								$insert_presensi['presensi_id'] = $obj->uuid->v4();
								$insert_presensi['tahun_kode'] = $tahun_kode;
								$insert_presensi['semester_id'] = $semester_id;
								$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
								$insert_presensi['presensi_sumber'] = 'M';
								$insert_presensi['staf_id'] = $sidik_jari->staf_id;
								$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
								$insert_presensi['jadwal_id'] = $jadwal->jadwal_id;
								$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
								$insert_presensi['created_by'] = userdata('pengguna_id');
								$insert_presensi['created_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->insert_presensi($insert_presensi);
								
								$res['status']	= 1;
								$res['message']	= 'Presensi Masuk Berhasil';
	
								return $res;
							} else {
								$res['status']	= 2;
								$res['message']	= 'Anda Sudah Presensi Masuk';
	
								return $res;
							}
						} else if (strtotime($DateTime) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
							if ($count_presensi < 1){
								$insert_presensi = array();
								$insert_presensi['presensi_id'] = $obj->uuid->v4();
								$insert_presensi['tahun_kode'] = $tahun_kode;
								$insert_presensi['semester_id'] = $semester_id;
								$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
								$insert_presensi['presensi_sumber'] = 'M';
								$insert_presensi['staf_id'] = $sidik_jari->staf_id;
								$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
								$insert_presensi['jadwal_id'] = $jadwal->jadwal_id;
								$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
								$insert_presensi['created_by'] = userdata('pengguna_id');
								$insert_presensi['created_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->insert_presensi($insert_presensi);
								
								$res['status']	= 9;
								$res['message']	= 'Anda Terlambat Presensi Masuk';
	
								return $res;
							} else {
								$res['status']	= 2;
								$res['message']	= 'Anda Sudah Presensi Masuk';
	
								return $res;
							}
						} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
							$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
							if ($count_presensi > 0){
								$update_presensi = array();
								$update_presensi['tahun_kode'] = $tahun_kode;
								$update_presensi['semester_id'] = $semester_id;
								$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$update_presensi['presensi_tanggal_keluar'] = $DateTime;
								$update_presensi['updated_by'] = userdata('pengguna_id');
								$update_presensi['updated_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
								
								$res['status']	= 3;
								$res['message']	= 'Presensi Pulang Berhasil';
								
								return $res;
							} else if ($count_presensi2 > 0){
								$res['status']	= 6;
								$res['message']	= 'Anda Sudah Presensi Pulang';
								
								return $res;
							} else {
								$res['status']	= 4;
								$res['message']	= 'Anda Tidak Presensi Masuk';
								return $res;
							}
						} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
							$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
							if ($count_presensi > 0){
								$update_presensi = array();
								$update_presensi['tahun_kode'] = $tahun_kode;
								$update_presensi['semester_id'] = $semester_id;
								$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$update_presensi['presensi_tanggal_keluar'] = $DateTime;
								$update_presensi['updated_by'] = userdata('pengguna_id');
								$update_presensi['updated_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
								
								$res['status']	= 10;
								$res['message']	= 'Anda Presensi Pulang Cepat';
								
								return $res;
							} else if ($count_presensi2 > 0){
								$res['status']	= 6;
								$res['message']	= 'Anda Sudah Presensi Pulang';
								
								return $res;
							} else {
								$res['status']	= 4;
								$res['message']	= 'Anda Tidak Presensi Masuk';
								return $res;
							}
						} else {
							$res['status']	= 5;
							$res['message']	= 'Belum Jadwal Presensi';
							return $res;
						}
					} else if ($jadwal->jadwal_jenis == 2){
						$status_nama = $obj->db->query("SELECT * FROM sat_kode WHERE kode_no = '$Status'")->row()->kode_nama;
						$jadwal_ = new stdClass();
						if ($status_nama == "Masuk"){
							$jadwal_ = $obj->Jadwal_model->get_jadwal('*', "jadwal_hari = '$N' AND jadwal_level = '$sidik_jari->sidik_jari_level' AND jadwal_masuk_awal <= '$time' AND jadwal_masuk_akhir >= '$time'");
							if ($jadwal_){
								if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_masuk_awal) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_masuk_tepat)){
									$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
									if ($count_presensi < 1){
										$insert_presensi = array();
										$insert_presensi['presensi_id'] = $obj->uuid->v4();
										$insert_presensi['tahun_kode'] = $tahun_kode;
										$insert_presensi['semester_id'] = $semester_id;
										$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
										$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
										$insert_presensi['presensi_sumber'] = 'M';
										$insert_presensi['staf_id'] = $sidik_jari->staf_id;
										$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
										$insert_presensi['jadwal_id'] = $jadwal_->jadwal_id;
										$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
										$insert_presensi['created_by'] = userdata('pengguna_id');
										$insert_presensi['created_at'] = date('Y-m-d H:i:s');
										$obj->Presensi_model->insert_presensi($insert_presensi);
										
										$res['status']	= 1;
										$res['message']	= 'Presensi Masuk Berhasil';
			
										return $res;
									} else {
										$res['status']	= 2;
										$res['message']	= 'Anda Sudah Presensi Masuk';
			
										return $res;
									}
								} else if (strtotime($DateTime) > strtotime($date.' '.$jadwal_->jadwal_masuk_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_masuk_akhir)){
									$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date'")->num_rows();
									if ($count_presensi < 1){
										$insert_presensi = array();
										$insert_presensi['presensi_id'] = $obj->uuid->v4();
										$insert_presensi['tahun_kode'] = $tahun_kode;
										$insert_presensi['semester_id'] = $semester_id;
										$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
										$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
										$insert_presensi['presensi_sumber'] = 'M';
										$insert_presensi['staf_id'] = $sidik_jari->staf_id;
										$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
										$insert_presensi['jadwal_id'] = $jadwal_->jadwal_id;
										$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
										$insert_presensi['created_by'] = userdata('pengguna_id');
										$insert_presensi['created_at'] = date('Y-m-d H:i:s');
										$obj->Presensi_model->insert_presensi($insert_presensi);
										
										$res['status']	= 9;
										$res['message']	= 'Anda Terlambat Presensi Masuk';
			
										return $res;
									} else {
										$res['status']	= 2;
										$res['message']	= 'Anda Sudah Presensi Masuk';
			
										return $res;
									}
								} else {
									$res['status']	= 5;
									$res['message']	= 'Belum Jadwal Presensi';
									return $res;
								}
							} else {
								$res['status']	= 5;
								$res['message']	= 'Belum Jadwal Presensi';
								return $res;
							}
						} else if ($status_nama == "Pulang"){
							$jadwal_ = $obj->Jadwal_model->get_jadwal('*', "jadwal_hari = '$N' AND jadwal_level = '$sidik_jari->sidik_jari_level' AND jadwal_pulang_awal <= '$time' AND jadwal_pulang_akhir >= '$time'");
							if ($jadwal_){
								if (strtotime($jadwal_->jadwal_pulang_tepat) < strtotime($jadwal_->jadwal_masuk_tepat)){
									if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_pulang_akhir)){
										$date_ = date("Y-m-d", strtotime($date."-1 day"));
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 3;
											$res['message']	= 'Presensi Pulang Berhasil';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal_->jadwal_pulang_tepat)){
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date_' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 10;
											$res['message']	= 'Anda Presensi Pulang Cepat';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else {
										$res['status']	= 5;
										$res['message']	= 'Belum Jadwal Presensi';
										return $res;
									}
								} else {
									if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_tepat) && strtotime($DateTime) <= strtotime($date.' '.$jadwal_->jadwal_pulang_akhir)){
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 3;
											$res['message']	= 'Presensi Pulang Berhasil';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else if (strtotime($DateTime) >= strtotime($date.' '.$jadwal_->jadwal_pulang_awal) && strtotime($DateTime) < strtotime($date.' '.$jadwal_->jadwal_pulang_tepat)){
										$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL")->num_rows();
										$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date'")->num_rows();
										if ($count_presensi > 0){
											$update_presensi = array();
											$update_presensi['tahun_kode'] = $tahun_kode;
											$update_presensi['semester_id'] = $semester_id;
											$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
											$update_presensi['presensi_tanggal_keluar'] = $DateTime;
											$update_presensi['updated_by'] = userdata('pengguna_id');
											$update_presensi['updated_at'] = date('Y-m-d H:i:s');
											$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
											
											$res['status']	= 10;
											$res['message']	= 'Anda Presensi Pulang Cepat';
											
											return $res;
										} else if ($count_presensi2 > 0){
											$res['status']	= 6;
											$res['message']	= 'Anda Sudah Presensi Pulang';
											
											return $res;
										} else {
											$res['status']	= 4;
											$res['message']	= 'Anda Tidak Presensi Masuk';
											return $res;
										}
									} else {
										$res['status']	= 5;
										$res['message']	= 'Belum Jadwal Presensi';
										return $res;
									}
								}
							} else {
								$res['status']	= 5;
								$res['message']	= 'Belum Jadwal Presensi';
								return $res;
							}
						}

					} else if ($jadwal->jadwal_jenis == 3){
						$status_nama = $obj->db->query("SELECT * FROM sat_kode WHERE kode_no = '$Status'")->row()->kode_nama;
						$jadwal_ = new stdClass();
						if ($status_nama == "Masuk"){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar) IS NULL ORDER BY created_at DESC")->num_rows();
							if ($count_presensi < 1){
								$insert_presensi = array();
								$insert_presensi['presensi_id'] = $obj->uuid->v4();
								$insert_presensi['tahun_kode'] = $tahun_kode;
								$insert_presensi['semester_id'] = $semester_id;
								$insert_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$insert_presensi['presensi_tanggal_masuk'] = $DateTime;
								$insert_presensi['presensi_sumber'] = 'M';
								$insert_presensi['staf_id'] = $sidik_jari->staf_id;
								$insert_presensi['siswa_id'] = $sidik_jari->siswa_id;
								$insert_presensi['jadwal_id'] = $jadwal->jadwal_id;
								$insert_presensi['presensi_shift_ke'] = $jadwal->jadwal_shift_ke;
								$insert_presensi['created_by'] = userdata('pengguna_id');
								$insert_presensi['created_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->insert_presensi($insert_presensi);
								
								$res['status']	= 1;
								$res['message']	= 'Presensi Masuk Berhasil';
	
								return $res;
							} else {
								$res['status']	= 2;
								$res['message']	= 'Anda Sudah Presensi Masuk';
	
								return $res;
							}
						} else if ($status_nama == "Pulang"){
							$count_presensi = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL ORDER BY created_at DESC")->num_rows();
							$count_presensi2 = $obj->db->query("SELECT presensi_id FROM sat_presensi WHERE tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND DATE(presensi_tanggal_keluar)  = '$date' ORDER BY created_at DESC")->num_rows();
							if ($count_presensi > 0){
								$update_presensi = array();
								$update_presensi['tahun_kode'] = $tahun_kode;
								$update_presensi['semester_id'] = $semester_id;
								$update_presensi['presensi_user'] = $sidik_jari->sidik_jari_user;
								$update_presensi['presensi_tanggal_keluar'] = $DateTime;
								$update_presensi['updated_by'] = userdata('pengguna_id');
								$update_presensi['updated_at'] = date('Y-m-d H:i:s');
								$obj->Presensi_model->update_presensi("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND presensi_user = '$sidik_jari->sidik_jari_user' AND DATE(presensi_tanggal_masuk)  = '$date' AND presensi_tanggal_keluar IS NULL", $update_presensi);
								
								$res['status']	= 3;
								$res['message']	= 'Presensi Pulang Berhasil';
								
								return $res;
							} else if ($count_presensi2 > 0){
								$res['status']	= 6;
								$res['message']	= 'Anda Sudah Presensi Pulang';
								
								return $res;
							} else {
								$res['status']	= 4;
								$res['message']	= 'Anda Tidak Presensi Masuk';
								return $res;
							}
						}
					}
				} else {
					$res['status']	= 7;
					$res['message']	= 'Tidak Ada Jadwal';
					return $res;
				}
			} else {
				$res['status']	= 8;
				$res['message']	= 'Sidik Jari Tidak Terdaftar';
				return $res;
			}
		} else {
			$res['status']	= 0;
			$res['message']	= 'Parameter Not Found';
			return $res;
		}
	}
}
/* End of file mod_constant_helper.php */
/* Location: ./application/helpers/mod_constant_helper.php */