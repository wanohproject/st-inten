<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('mesin/datatable/'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "mesin_nama"},
			{ "data": "mesin_ip"},
			{ "data": "mesin_key"},
			{ "data": "mesin_status"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mesin
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mesin'); ?>">Mesin</a></li>
            <li class="active">Daftar Mesin</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Mesin</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add/'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Mesin</th>
                        <th>IP</th>
                        <th>Key</th>
                        <th>Status</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mesin
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mesin'); ?>">Mesin</a></li>
            <li class="active">Tambah Mesin</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Mesin</h3>
                </div><!-- /.box-header -->
				<form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-body form-horizontal">
                    <div class="form-group">
                        <label for="mesin_nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_nama" id="mesin_nama" value="<?php echo $mesin_nama; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_ip" class="col-sm-2 control-label">IP</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_ip" id="mesin_ip" value="<?php echo $mesin_ip; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_key" class="col-sm-2 control-label">Key</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_key" id="mesin_key" value="<?php echo $mesin_key; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_info" class="col-sm-2 control-label">Info</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_info" id="mesin_info" value="<?php echo $mesin_info; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_jari" class="col-sm-2 control-label">Max Jari</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="number" min="0" class="form-control" name="mesin_jari" id="mesin_jari" value="<?php echo $mesin_jari; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_log" class="col-sm-2 control-label">Max Log Presensi</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="number" min="0" class="form-control" name="mesin_log" id="mesin_log" value="<?php echo $mesin_log; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="mesin_status" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="mesin_status" value="A" <?php echo ($mesin_status == 'A')?'checked':''; ?> required> Aktif
                                </label>
                                &nbsp;&nbsp;&nbsp;
                                <label>
                                    <input type="radio" name="mesin_status" value="H" <?php echo ($mesin_status == 'H')?'checked':''; ?> required> Tidak Aktif
                                </label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'); ?>'" class="btn btn-default">Batalkan</button>
                </div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mesin
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mesin'); ?>">Mesin</a></li>
            <li class="active">Ubah Mesin</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Ubah Mesin</h3>
                </div><!-- /.box-header -->
				<form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$mesin_id);?>" method="post">
                <input type="hidden" name="mesin_id" value="<?php echo $mesin_id; ?>" />
                <div class="box-body form-horizontal">
                    <div class="form-group">
                        <label for="mesin_nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_nama" id="mesin_nama" value="<?php echo $mesin_nama; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_ip" class="col-sm-2 control-label">IP</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_ip" id="mesin_ip" value="<?php echo $mesin_ip; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_key" class="col-sm-2 control-label">Key</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_key" id="mesin_key" value="<?php echo $mesin_key; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_info" class="col-sm-2 control-label">Info</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="text" class="form-control" name="mesin_info" id="mesin_info" value="<?php echo $mesin_info; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_jari" class="col-sm-2 control-label">Max Jari</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="number" min="0" class="form-control" name="mesin_jari" id="mesin_jari" value="<?php echo $mesin_jari; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_log" class="col-sm-2 control-label">Max Log Presensi</label>
                        <div class="col-md-6 col-sm-8">
                            <input type="number" min="0" class="form-control" name="mesin_log" id="mesin_log" value="<?php echo $mesin_log; ?>" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_status" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="mesin_status" value="A" <?php echo ($mesin_status == 'A')?'checked':''; ?> required> Aktif
                                </label>
                                &nbsp;&nbsp;&nbsp;
                                <label>
                                    <input type="radio" name="mesin_status" value="H" <?php echo ($mesin_status == 'H')?'checked':''; ?> required> Tidak Aktif
                                </label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'); ?>'" class="btn btn-default">Batalkan</button>
                </div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } else if ($action == 'detail') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mesin
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mesin'); ?>">Mesin</a></li>
            <li class="active">Detail Mesin</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Mesin</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-body">
						<div class="form-group row">
							<label for="mesin_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $mesin_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="mesin_ip" class="col-sm-2 control-label">IP</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $mesin_ip; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="mesin_key" class="col-sm-2 control-label">Key</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $mesin_key; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="mesin_status" class="col-sm-2 control-label">Status</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $mesin_status; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="mesin_info" class="col-sm-2 control-label">Info</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $mesin_info; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="jumlah_jari" class="col-sm-2 control-label">Jumlah Jari</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $jumlah_jari . ' / ' . rupiah2($mesin_jari); ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="log_presensi" class="col-sm-2 control-label">Log Presensi</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $jumlah_log . ' / ' . rupiah2($mesin_log); ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<a href="<?php echo module_url($this->uri->segment(2).'/delete-log/'.$mesin_id); ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin akan hapus semua log transaksi di mesin ini?')">Hapus Log</a>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$mesin_id); ?>'" class="btn btn-warning" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

    <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>