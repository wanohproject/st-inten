$j = 0;
                                      for ($i=0; $i < $days; $i++){
                                        if ($i == 0){
                                          $date = $presensi_tanggal_awal;
                                        } else {
                                          $date = date('Y-m-d', strtotime($presensi_tanggal_awal . " + $i days"));
                                        }
                  
                                        $N = date('N', strtotime($date));
                                        $jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$row_departemen));
                                        
                                        if ($jadwal){
                                          $info_presensi = "&nbsp;";
                                          if ($this->Libur_model->count_all_libur('', array('libur_tanggal'=>$date)) < 1){
                                            $list_presensi	= $this->Presensi_model->grid_all_presensi('*', "presensi_tanggal_masuk", "ASC", 0, 0, "presensi.presensi_user = '$row_staf->staf_user' AND DATE(presensi.presensi_tanggal_masuk) = '$date'");
                                            if ($list_presensi){
                                              $info_presensi = "";
                                              foreach ($list_presensi as $row_presensi) {
                                                $presensi_tanggal_masuk = ($row_presensi)?($row_presensi->presensi_tanggal_masuk)?$row_presensi->presensi_tanggal_masuk:'':'';
                                                $presensi_tanggal_keluar = ($row_presensi)?($row_presensi->presensi_tanggal_keluar)?$row_presensi->presensi_tanggal_keluar:'':'';
                                                $jam_masuk = ($presensi_tanggal_masuk)?date('H:i', strtotime($row_presensi->presensi_tanggal_masuk)):'';
                                                $jam_pulang = ($presensi_tanggal_keluar)?date('H:i', strtotime($row_presensi->presensi_tanggal_keluar)):'';
                                                $info_presensi .= $jam_masuk."-".$jam_pulang."<br />";
                                              }
                                            }
                                          }
                                          if ($j % 10 == 0){
                                            echo "<tr>";
                                          }
                                          ?>
                                          <td style="vertical-align:top;" nowrap>
                                            <div style="text-align:center;background:#DDD;font-weight:bold;padding:3px 5px;"><?php echo dateShort($date); ?> <?php echo getDayShort($date); ?></div>
                                            <div style="text-align:center;padding:3px 5px;"><?php echo $info_presensi; ?></div>
                                          </td>
                                          <?php
                                          if ($j % 10 == 9 || $i == $days){
                                            echo "</tr>";
                                          }
                                          $j++;
                                        }
                                      }
                                      ?>