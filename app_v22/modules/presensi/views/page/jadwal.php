<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('jadwal/datatable/'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "departemen_kode"},
			{ "data": "departemen_nama"},
			{ "data": "departemen_tipe"},
			{ "data": "departemen_parent"},
			{ "data": "jadwal_status"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc'],
			[1, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Jadwal
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('jadwal'); ?>">Jadwal</a></li>
            <li class="active">Daftar Jadwal</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Jadwal</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Departemen</th>
                        <th>Tipe</th>
                        <th>Parent</th>
                        <th style="width:110px;">Jadwal</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'edit') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('.tanggal').datetimepicker({
        sideBySide: true,
        format: 'HH:mm:ss'
    });
    <?php
    foreach ($hari as $key => $value) {
      for ($i=1; $i <= $jadwal_shift_jumlah; $i++) { 
        ?>
        $('#hari_<?php echo $key; ?>_<?php echo $i; ?>').click(function() {
          if ($(this).is(':checked') == true){
            $('#jadwal_masuk_awal_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', false);
            $('#jadwal_masuk_tepat_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', false);
            $('#jadwal_masuk_akhir_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', false);
            $('#jadwal_pulang_awal_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', false);
            $('#jadwal_pulang_tepat_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', false);
            $('#jadwal_pulang_akhir_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', false);

            <?php if ($key > 1){ ?>
            if ($('#jadwal_masuk_awal_<?php echo $key; ?>_<?php echo $i; ?>').val() == ""){
              $('#jadwal_masuk_awal_<?php echo $key; ?>_<?php echo $i; ?>').val($('#jadwal_masuk_awal_1_<?php echo $i; ?>').val());
            }
            if ($('#jadwal_masuk_tepat_<?php echo $key; ?>_<?php echo $i; ?>').val() == ""){
              $('#jadwal_masuk_tepat_<?php echo $key; ?>_<?php echo $i; ?>').val($('#jadwal_masuk_tepat_1_<?php echo $i; ?>').val());
            }
            if ($('#jadwal_masuk_akhir_<?php echo $key; ?>_<?php echo $i; ?>').val() == ""){
              $('#jadwal_masuk_akhir_<?php echo $key; ?>_<?php echo $i; ?>').val($('#jadwal_masuk_akhir_1_<?php echo $i; ?>').val());
            }
            if ($('#jadwal_pulang_awal_<?php echo $key; ?>_<?php echo $i; ?>').val() == ""){
              $('#jadwal_pulang_awal_<?php echo $key; ?>_<?php echo $i; ?>').val($('#jadwal_pulang_awal_1_<?php echo $i; ?>').val());
            }
            if ($('#jadwal_pulang_tepat_<?php echo $key; ?>_<?php echo $i; ?>').val() == ""){
              $('#jadwal_pulang_tepat_<?php echo $key; ?>_<?php echo $i; ?>').val($('#jadwal_pulang_tepat_1_<?php echo $i; ?>').val());
            }
            if ($('#jadwal_pulang_akhir_<?php echo $key; ?>_<?php echo $i; ?>').val() == ""){
              $('#jadwal_pulang_akhir_<?php echo $key; ?>_<?php echo $i; ?>').val($('#jadwal_pulang_akhir_1_<?php echo $i; ?>').val());
            }
            <?php } ?>
          } else {
            $('#jadwal_masuk_awal_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', true);
            $('#jadwal_masuk_tepat_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', true);
            $('#jadwal_masuk_akhir_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', true);
            $('#jadwal_pulang_awal_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', true);
            $('#jadwal_pulang_tepat_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', true);
            $('#jadwal_pulang_akhir_<?php echo $key; ?>_<?php echo $i; ?>').prop('disabled', true);
          }
        });
        <?php
      }
    }
    ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Jadwal
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('jadwal'); ?>">Jadwal</a></li>
            <li class="active">Atur Jadwal</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Atur Jadwal - <?php echo $departemen_nama; ?></h3>
                  <div class="pull-right">
										<?php if ($this->input->get('refId')){ ?>
                      <a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4));?>" class="btn btn-danger">Hapus Referensi</a>
                    <?php } else { ?>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">Salin Referensi</button>
                    <?php } ?>
									</div>
                </div><!-- /.box-header -->
				        <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$departemen_id);?>" method="post">
                <input type="hidden" name="departemen_id" value="<?php echo $departemen_id; ?>" />
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="jadwal_jenis" class="control-label">Jenis Jadwal</label>
                        <div class="">
                          <?php combobox('2d', array(1=>"NON SHIFT", 2=>"MULTI SHIFT", 3=>"FREE SHIFT"), 'jadwal_jenis', '', '', $jadwal_jenis, 'submit();', 'none', 'class="form-control select2" required');?>
                        </div>		
                      </div>
                    </div>
                    <?php if ($jadwal_jenis == 2){?>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="jadwal_shift_jumlah" class="control-label">Jumlah Shift</label>
                        <div class="">
                          <input type="number" min="1" name="jadwal_shift_jumlah" class="form-control" onchange="submit()" value="<?php echo $jadwal_shift_jumlah; ?>" autocomplete="off" />
                        </div>		
                      </div>
                    </div>
                    <?php } else { ?>
                    <input type="hidden" name="jadwal_shift_jumlah" value="<?php echo $jadwal_shift_jumlah; ?>" />
                    <?php } ?>

                    <?php if ($jadwal_jenis == 3){?>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="jadwal_batas_minimal" class="control-label">Batas Minimal Masuk ke Pulang</label>
                        <div class="">
                          <div class="input-group">
                            <input type="number" name="jadwal_batas_minimal" class="form-control" value="<?php echo $jadwal_batas_minimal; ?>" autocomplete="off" />
                            <span class="input-group-addon">detik</span>
                          </div>
                          <small>Dan Sebaliknya</small>
                        </div>		
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                  <?php if ($jadwal_jenis == 1){?>
                  <div style="overflow-x:auto;">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th width="50" rowspan="2">#</th>
                          <th width="100" rowspan="2">Hari</th>
                          <th colspan="3">Masuk</th>
                          <th colspan="3">Pulang</th>
                        </tr>
                        <tr>
                          <th>Awal</th>
                          <th>Tepat</th>
                          <th>Akhir</th>
                          <th>Awal</th>
                          <th>Tepat</th>
                          <th>Akhir</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                      foreach ($hari as $key => $value) {
                        if ($this->input->get('refId')){
                          $where['jadwal_hari'] 			= $key;
                          $where['jadwal_shift_ke'] 	= $jadwal_shift_jumlah;
                          $where['departemen_id'] 		= $this->input->get('refId');
                          if ($this->Jadwal_model->count_all_jadwal($where) < 1){
                            $jadwal_masuk_awal = "";
                            $jadwal_masuk_tepat = "";
                            $jadwal_masuk_akhir = "";
                            $jadwal_pulang_awal = "";
                            $jadwal_pulang_tepat = "";
                            $jadwal_pulang_akhir = "";
  
                            $disabled = "disabled";
                            $checked = "";
                          } else {
                            $jadwal = $this->Jadwal_model->get_jadwal("", $where);
                            $jadwal_masuk_awal = $jadwal->jadwal_masuk_awal;
                            $jadwal_masuk_tepat = $jadwal->jadwal_masuk_tepat;
                            $jadwal_masuk_akhir = $jadwal->jadwal_masuk_akhir;
                            $jadwal_pulang_awal = $jadwal->jadwal_pulang_awal;
                            $jadwal_pulang_tepat = $jadwal->jadwal_pulang_tepat;
                            $jadwal_pulang_akhir = $jadwal->jadwal_pulang_akhir;
                            $disabled = "";
                            $checked = "checked";
                          }
                        } else {

                          $where['jadwal_hari'] 			= $key;
                          $where['jadwal_shift_ke'] 	= $jadwal_shift_jumlah;
                          $where['departemen_id'] 		= $departemen_id;
                          if ($this->Jadwal_model->count_all_jadwal($where) < 1){
                            $jadwal_masuk_awal = "";
                            $jadwal_masuk_tepat = "";
                            $jadwal_masuk_akhir = "";
                            $jadwal_pulang_awal = "";
                            $jadwal_pulang_tepat = "";
                            $jadwal_pulang_akhir = "";
  
                            $disabled = "disabled";
                            $checked = "";
                          } else {
                            $jadwal = $this->Jadwal_model->get_jadwal("", $where);
                            $jadwal_masuk_awal = $jadwal->jadwal_masuk_awal;
                            $jadwal_masuk_tepat = $jadwal->jadwal_masuk_tepat;
                            $jadwal_masuk_akhir = $jadwal->jadwal_masuk_akhir;
                            $jadwal_pulang_awal = $jadwal->jadwal_pulang_awal;
                            $jadwal_pulang_tepat = $jadwal->jadwal_pulang_tepat;
                            $jadwal_pulang_akhir = $jadwal->jadwal_pulang_akhir;
                            $disabled = "";
                            $checked = "checked";
                          }
                        }
                        ?>
                        <tr>
                          <td><input type="checkbox" name="hari[<?php echo $key; ?>][<?php echo $jadwal_shift_jumlah; ?>]" id="hari_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" <?php echo $checked; ?>></td>
                          <td><?php echo $value; ?></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_awal[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_masuk_awal_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_masuk_awal; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Awal"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_tepat[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_masuk_tepat_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_masuk_tepat; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Tepat"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_akhir[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_masuk_akhir_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_masuk_akhir; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Akhir"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_awal[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_pulang_awal_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_pulang_awal; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Awal"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_tepat[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_pulang_tepat_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_pulang_tepat; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Tepat"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_akhir[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_pulang_akhir_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_pulang_akhir; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Akhir"></td>
                        </tr>
                        <?php
                      }                      
                      ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } else if ($jadwal_jenis == 2){ ?>
                  <div style="overflow-x:auto;">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th width="50" rowspan="2">#</th>
                          <th width="100" rowspan="2">Hari</th>
                          <th width="100" rowspan="2">Shift</th>
                          <th colspan="3">Masuk</th>
                          <th colspan="3">Pulang</th>
                        </tr>
                        <tr>
                          <th>Awal</th>
                          <th>Tepat</th>
                          <th>Akhir</th>
                          <th>Awal</th>
                          <th>Tepat</th>
                          <th>Akhir</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                      foreach ($hari as $key => $value) {
                        for ($i=1; $i <= $jadwal_shift_jumlah; $i++) {
                          if ($this->input->get('refId')){
                            $where['jadwal_hari'] 			= $key;
                            $where['jadwal_shift_ke'] 	= $i;
                            $where['jadwal_level'] 			= $this->input->get('refId');
                            if ($this->Jadwal_model->count_all_jadwal($where) < 1){
                              $jadwal_masuk_awal = "";
                              $jadwal_masuk_tepat = "";
                              $jadwal_masuk_akhir = "";
                              $jadwal_pulang_awal = "";
                              $jadwal_pulang_tepat = "";
                              $jadwal_pulang_akhir = "";
    
                              $disabled = "disabled";
                              $checked = "";
                            } else {
                              $jadwal = $this->Jadwal_model->get_jadwal("", $where);
                              $jadwal_masuk_awal = $jadwal->jadwal_masuk_awal;
                              $jadwal_masuk_tepat = $jadwal->jadwal_masuk_tepat;
                              $jadwal_masuk_akhir = $jadwal->jadwal_masuk_akhir;
                              $jadwal_pulang_awal = $jadwal->jadwal_pulang_awal;
                              $jadwal_pulang_tepat = $jadwal->jadwal_pulang_tepat;
                              $jadwal_pulang_akhir = $jadwal->jadwal_pulang_akhir;
                              $disabled = "";
                              $checked = "checked";
                            }
                          } else {

                            $where['jadwal_hari'] 			= $key;
                            $where['jadwal_shift_ke'] 	= $i; 
                            $where['jadwal_level'] 			= $departemen_id;
                            if ($this->Jadwal_model->count_all_jadwal($where) < 1){
                              $jadwal_masuk_awal = "";
                              $jadwal_masuk_tepat = "";
                              $jadwal_masuk_akhir = "";
                              $jadwal_pulang_awal = "";
                              $jadwal_pulang_tepat = "";
                              $jadwal_pulang_akhir = "";
    
                              $disabled = "disabled";
                              $checked = "";
                            } else {
                              $jadwal = $this->Jadwal_model->get_jadwal("", $where);
                              $jadwal_masuk_awal = $jadwal->jadwal_masuk_awal;
                              $jadwal_masuk_tepat = $jadwal->jadwal_masuk_tepat;
                              $jadwal_masuk_akhir = $jadwal->jadwal_masuk_akhir;
                              $jadwal_pulang_awal = $jadwal->jadwal_pulang_awal;
                              $jadwal_pulang_tepat = $jadwal->jadwal_pulang_tepat;
                              $jadwal_pulang_akhir = $jadwal->jadwal_pulang_akhir;
                              $disabled = "";
                              $checked = "checked";
                            }
                          }
                          ?>
                          <?php if ($i == 1){ ?>
                           <tr>
                           <tH colspan="9"><?php echo $value; ?></tH>
                          </tr>
                          <?php } ?>
                          <tr>
                            <td><input type="checkbox" name="hari[<?php echo $key; ?>][<?php echo $i; ?>]" id="hari_<?php echo $key; ?>_<?php echo $i; ?>" <?php echo $checked; ?>></td>
                            <td><?php echo $value; ?></td>
                            <td class="text-center"><?php echo $i; ?></td>
                            <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_awal[<?php echo $key?>][<?php echo $i; ?>]" id="jadwal_masuk_awal_<?php echo $key; ?>_<?php echo $i; ?>" value="<?php echo $jadwal_masuk_awal; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Awal"></td>
                            <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_tepat[<?php echo $key?>][<?php echo $i; ?>]" id="jadwal_masuk_tepat_<?php echo $key; ?>_<?php echo $i; ?>" value="<?php echo $jadwal_masuk_tepat; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Tepat"></td>
                            <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_akhir[<?php echo $key?>][<?php echo $i; ?>]" id="jadwal_masuk_akhir_<?php echo $key; ?>_<?php echo $i; ?>" value="<?php echo $jadwal_masuk_akhir; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Akhir"></td>
                            <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_awal[<?php echo $key?>][<?php echo $i; ?>]" id="jadwal_pulang_awal_<?php echo $key; ?>_<?php echo $i; ?>" value="<?php echo $jadwal_pulang_awal; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Awal"></td>
                            <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_tepat[<?php echo $key?>][<?php echo $i; ?>]" id="jadwal_pulang_tepat_<?php echo $key; ?>_<?php echo $i; ?>" value="<?php echo $jadwal_pulang_tepat; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Tepat"></td>
                            <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_akhir[<?php echo $key?>][<?php echo $i; ?>]" id="jadwal_pulang_akhir_<?php echo $key; ?>_<?php echo $i; ?>" value="<?php echo $jadwal_pulang_akhir; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Akhir"></td>
                          </tr>
                          <?php 
                        }
                      }                      
                      ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } else if ($jadwal_jenis == 3){ ?>
                  <div style="overflow-x:auto;">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th width="50" rowspan="2">#</th>
                          <th width="100" rowspan="2">Hari</th>
                          <th colspan="2">Batas Awal Masuk</th>
                          <th colspan="2">Batas Akhir Pulang</th>
                        </tr>
                        <tr>
                          <th>Awal Presensi</th>
                          <th>Jadwal Masuk</th>
                          <th>Jadwal Pulang</th>
                          <th>Akhir Presensi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                      foreach ($hari as $key => $value) {
                        if ($this->input->get('refId')){
                          $where['jadwal_hari'] 			= $key;
                          $where['jadwal_shift_ke'] 	= $jadwal_shift_jumlah;
                          $where['departemen_id'] 		= $this->input->get('refId');
                          if ($this->Jadwal_model->count_all_jadwal($where) < 1){
                            $jadwal_masuk_awal = "";
                            $jadwal_masuk_tepat = "";
                            $jadwal_pulang_tepat = "";
                            $jadwal_pulang_akhir = "";
  
                            $disabled = "disabled";
                            $checked = "";
                          } else {
                            $jadwal = $this->Jadwal_model->get_jadwal("", $where);
                            $jadwal_masuk_awal = $jadwal->jadwal_masuk_awal;
                            $jadwal_masuk_tepat = $jadwal->jadwal_masuk_tepat;
                            $jadwal_pulang_tepat = $jadwal->jadwal_pulang_tepat;
                            $jadwal_pulang_akhir = $jadwal->jadwal_pulang_akhir;
                            $disabled = "";
                            $checked = "checked";
                          }
                        } else {

                          $where['jadwal_hari'] 			= $key;
                          $where['jadwal_shift_ke'] 	= $jadwal_shift_jumlah;
                          $where['departemen_id'] 		= $departemen_id;
                          if ($this->Jadwal_model->count_all_jadwal($where) < 1){
                            $jadwal_masuk_awal = "";
                            $jadwal_masuk_tepat = "";
                            $jadwal_pulang_tepat = "";
                            $jadwal_pulang_akhir = "";
  
                            $disabled = "disabled";
                            $checked = "";
                          } else {
                            $jadwal = $this->Jadwal_model->get_jadwal("", $where);
                            $jadwal_masuk_awal = $jadwal->jadwal_masuk_awal;
                            $jadwal_masuk_tepat = $jadwal->jadwal_masuk_tepat;
                            $jadwal_pulang_tepat = $jadwal->jadwal_pulang_tepat;
                            $jadwal_pulang_akhir = $jadwal->jadwal_pulang_akhir;
                            $disabled = "";
                            $checked = "checked";
                          }
                        }
                        ?>
                        <tr>
                          <td><input type="checkbox" name="hari[<?php echo $key; ?>][<?php echo $jadwal_shift_jumlah; ?>]" id="hari_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" <?php echo $checked; ?>></td>
                          <td><?php echo $value; ?></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_awal[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_masuk_awal_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_masuk_awal; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Awal"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_masuk_tepat[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_masuk_tepat_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_masuk_tepat; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Masuk Tepat"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_tepat[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_pulang_tepat_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_pulang_tepat; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Tepat"></td>
                          <td><input style="min-width:80px;" type="text" maxlength="8" class="form-control tanggal" name="jadwal_pulang_akhir[<?php echo $key?>][<?php echo $jadwal_shift_jumlah; ?>]" id="jadwal_pulang_akhir_<?php echo $key; ?>_<?php echo $jadwal_shift_jumlah; ?>" value="<?php echo $jadwal_pulang_akhir; ?>" <?php echo $disabled; ?> autocomplete="off" placeholder="Pulang Akhir"></td>
                        </tr>
                        <?php
                      }                      
                      ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } ?>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'); ?>'" class="btn btn-default">Batalkan</button>
                </div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pilih Referensi</h4>
      </div>
      <div class="modal-body">
        <table id="datagrid" class="table table-bordered table-striped">
          <thead>
            <tr>
            <th>Kode</th>
            <th>Departemen</th>
            <th>Tipe</th>
            <th>Parent</th>
              <th style="width:50px;">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $grid_departemen = $this->db->query("SELECT departemen.departemen_id, departemen.departemen_kode, departemen.departemen_nama, departemen.departemen_tipe, utama.departemen_nama AS departemen_parent FROM departemen LEFT JOIN departemen utama ON departemen.departemen_id=utama.departemen_id WHERE (SELECT COUNT(jadwal_id) FROM sat_jadwal WHERE sat_jadwal.departemen_id = departemen.departemen_id AND sat_jadwal.departemen_id != '$departemen_id') > 0 ORDER BY departemen.departemen_kode")->result();
          if ($grid_departemen){
            foreach ($grid_departemen as $row_departemen){
            ?>
              <tr>
                <td><?php echo $row_departemen->departemen_kode; ?></td>
                <td><?php echo $row_departemen->departemen_nama; ?></td>
                <td><?php echo $row_departemen->departemen_tipe; ?></td>
                <td><?php echo $row_departemen->departemen_parent; ?></td>
                <td><a href="<?php echo current_url()."?refId=".$row_departemen->departemen_id; ?>" class="btn btn-sm btn-success"><i class="fa fa-copy"></i></a></td>
              </tr>
            <?php
            }
          }
          ?>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>