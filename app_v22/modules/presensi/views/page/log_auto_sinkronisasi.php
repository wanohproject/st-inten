<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	var tbl_auto = $("#datagrid").DataTable({
		"processing": false,
    "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('log-auto-sinkronisasi/datatable/'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "log_tanggal"},
			{ "data": "user_id"},
			{ "data": "sidik_jari_user"},
			{ "data": "sidik_jari_nama"},
			{ "data": "mesin_nama"}
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'desc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});

  // setInterval( function () {
  //   tbl_auto.ajax.reload();
  // }, 3000 );

	<?php 
	if ($mesin){
		foreach ($mesin as $row) {
			?>
			ajaxLoad('<?php echo $row->mesin_id; ?>');
			// var params<?php echo $row->mesin_id; ?> = {
			// 	mesin: <?php echo $row->mesin_id; ?>
			// };
			// setInterval( function () {
			// 	$.ajax({
			// 			url: "<?php echo module_url('log-auto-sinkronisasi/dataload'); ?>",
			// 			dataType: 'json',
			// 			type: 'POST',
			// 			data: params<?php echo $row->mesin_id; ?>,
			// 			success:
			// 			function(data){
			// 					// console.log(data);
			// 			},
			// 			complete: function(){
			// 				tbl_auto.ajax.reload();
			// 			}
			// 	});
			// }, 3000 );
			<?php
		}
	}
	?>

	setInterval(function(){ 
		tbl_auto.ajax.reload();
	}, 5000);

	function ajaxLoad(mesin){
		setTimeout(function() {
			$.ajax({
					url: "<?php echo module_url('log-auto-sinkronisasi/dataload_get'); ?>?mesin=" + mesin,
					dataType: 'json',
					type: 'POST',
					success:
					function(data){
							// console.log(data);
							if (data.response == true){
								ajaxLoad(mesin);
							} else if (data.response == false){
								ajaxLoad(mesin);
							}
					},
					complete: function(){
					}
			});
		}, 5000);
	}

	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show');
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Log Presensi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mesin'); ?>">Log Presensi</a></li>
            <li class="active">Auto Sinkronisasi Log</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Auto Sinkronisasi Log</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>User ID</th>
                        <th>User</th>
                        <th>Nama</th>
                        <th>Mesin</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>