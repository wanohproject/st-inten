<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('sidik_jari_siswa/datatable/'.$departemen_id.'/'.$tahun_kode.'/'.$tingkat_id.'/'.$kelas_id); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "siswa_nama"},
			{ "data": "siswa_user"},
			{ "data": "user_id"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sidik Jari Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('sidik_jari_siswa'); ?>">Sidik Jari Siswa</a></li>
            <li class="active">Daftar Sidik Jari Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Sidik Jari Siswa</h3>
                  <div class="pull-right">
                  <a href="<?php echo module_url($this->uri->segment(2).'/generate/'); ?>" class="btn btn-primary"><span class="fa fa-refresh"></span> Generate User Finger</a>
                  <a href="<?php echo module_url($this->uri->segment(2).'/download/'); ?>" class="btn btn-primary"><span class="fa fa-download"></span> Download</a>
                  <a href="<?php echo module_url($this->uri->segment(2).'/upload/'); ?>" class="btn btn-primary"><span class="fa fa-upload"></span> Upload</a>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4));?>" method="post">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="departemen_id" class="control-label">Departemen</label>
                          <div class="">
                            <?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Alumni') ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
                          </div>		
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="tingkat_id" class="control-label">Tahun Ajaran</label>
                          <div class="">
                            <?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
                          </div>		
                        </div>
                      </div>
                      <?php if ($this->session->userdata('level') == 10){?>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="tingkat_id" class="control-label">Tingkat</label>
                          <div class="">
                            <?php combobox('db', $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tingkat.departemen_id = '$departemen_id' AND kelas.tahun_kode = '$tahun_kode' AND kelas.siswa_id = '$siswa_id' GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2" required');?>
                          </div>		
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="kelas_id" class="control-label">Kelas</label>
                          <div class="">
                            <?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$tingkat_id' AND kelas.siswa_id = '$siswa_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2" required');?>
                          </div>		
                        </div>
                      </div>
                      <?php } else { ?>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="tingkat_id" class="control-label">Tingkat</label>
                          <div class="">
                            <?php combobox('db', $this->db->query("SELECT * FROM tingkat WHERE tingkat.departemen_id = '$departemen_id' ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2" required');?>
                          </div>		
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="kelas_id" class="control-label">Kelas</label>
                          <div class="">
                            <?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '$tahun_kode' AND tingkat_id = '$tingkat_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2" required');?>
                          </div>		
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                  </form>
                </div>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>User Login</th>
                        <th>User ID Finger</th>
                        <th style="width:70px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sidik Jari Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('sidik_jari_siswa'); ?>">Sidik Jari Siswa</a></li>
            <li class="active">Ubah Sidik Jari Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
				  <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$siswa_id);?>" method="post">
          <input type="hidden" name="siswa_id" value="<?php echo $siswa_id; ?>" />
          <input type="hidden" name="sidik_jari_user" value="<?php echo $sidik_jari_user; ?>" />
          <input type="hidden" name="sidik_jari_id" value="<?php echo $sidik_jari_id; ?>" />
          <input type="hidden" name="sidik_jari_level" value="<?php echo $sidik_jari_level; ?>" />
          <div class="row">
            <div class="col-md-6">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Data Sidik Jari Siswa</h3>
                  <div class="pull-right">
                    <?php echo ($sidik_jari)?'<button class="btn btn-sm btn-success" onclick="return false;"><span class="fa fa-check"></span> Terdaftar</button>':'<button class="btn btn-sm btn-danger" name="reg" value="reg" onclick="return confirm(\'Apakah Anda yakin akan mendaftarkan user ini?\')"><span class="fa fa-remove"></span> Tidak Terdaftar</button>';?>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
                  <div class="form-group">
                      <label for="user_id" class="col-md-3 control-label">User ID</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="user_id" id="user_id" value="<?php echo $user_id; ?>" placeholder="" required>
                          <small>User ID digunakan pada Mesin</small>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="sidik_jari_nama" class="col-md-3 control-label">Nama</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="sidik_jari_nama" id="sidik_jari_nama" value="<?php echo $sidik_jari_nama; ?>" placeholder="" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="sidik_jari_card" class="col-md-3 control-label">Kartu RFID</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="sidik_jari_card" id="sidik_jari_card" value="<?php echo $sidik_jari_card; ?>" placeholder="" required readonly>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="sidik_jari_level" class="col-md-3 control-label">Departemen</label>
                      <div class="col-md-8">
                        <?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Alumni') ORDER BY departemen_urutan ASC")->result(), 'sidik_jari_level_', 'departemen_id', 'departemen_nama', $sidik_jari_level, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" disabled');?>
                      </div>
                  </div>
                  <!-- <div class="form-group">
                      <label for="sidik_jari_sms" class="col-md-3 control-label">Kirim Pesan</label>
                      <div class="col-sm-9">
                          <div class="radio">
                              <label>
                                  <input type="radio" name="sidik_jari_sms" value="Y" <?php echo ($sidik_jari_sms == 'Y')?'checked':''; ?> required> Aktif
                              </label>
                              &nbsp;&nbsp;&nbsp;
                              <label>
                                  <input type="radio" name="sidik_jari_sms" value="N" <?php echo ($sidik_jari_sms == 'N')?'checked':''; ?> required> Tidak
                              </label>
                          </div>
                      </div>
                  </div> -->
                </div><!-- /.box-body -->
                <div class="box-header">
                  <h3 class="box-title">Mesin</h3>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th width="20">#</th>
                        <th width="100">Mesin</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $grid_mesin = $this->Mesin_model->grid_all_mesin("", "mesin.mesin_nama", "ASC");
                    $i = 1;
                    if ($grid_mesin) {
                      foreach($grid_mesin as $row_mesin){
                        $mesin_sidik = $this->Sidik_mesin_model->count_all_sidik_mesin(array('sidik_mesin.mesin_id'=>$row_mesin->mesin_id, 'sidik_mesin.siswa_id'=>$siswa_id));
                        $check = ($mesin_sidik > 0)?'checked':'';
                        echo "<tr>
                            <td class=\"text-center\"><input type=\"checkbox\" name=\"mesin[]\" value=\"$row_mesin->mesin_id\" $check /></td>
                            <td><label>".$row_mesin->mesin_nama."</label></td>
                            </tr>";
                        $i++;
                      }
                    }
                    ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-6">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Jari</h3>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th width="20">#</th>
                        <th>Jari</th>
                        <th width="50">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $grid_sidik_template = $this->Sidik_template_model->grid_all_sidik_template("", "sidik_template.finger_id", "ASC", 0, 0, "sidik_template.siswa_id = '$siswa_id' AND sidik_template.finger_id IS NOT NULL", "", "sidik_template.finger_id");
                    $i = 1;
                    if ($grid_sidik_template) {
                      foreach($grid_sidik_template as $row_sidik_template){
                        echo "<tr>
                            <td class=\"text-center\">$i</td>
                            <td><label>".$row_sidik_template->finger_nama."</label></td>
                            <td><a href=\"".module_url($this->uri->segment(2) . "/delete_template/".$siswa_id."/".$row_sidik_template->sidik_template_id) ."\" class=\"btn btn-primary btn-sm\" style=\"margin-right:5px;margin-bottom:5px;\" title=\"Hapus Sidik Jari\" onclick=\"return confirm('Apakah Anda yakin? \nAkan menghapus sidik jari ini.');\"><i class=\"fa fa-trash-o\"></i></a></td>
                            </tr>";
                        $i++;
                      }
                    }
                    ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-header">
                  <h3 class="box-title">Kartu</h3>
                  <div class="pull-right">
                    <a href="<?php echo module_url($this->uri->segment(2).'/add-card/'.$siswa_id); ?>" class="btn btn-sm btn-success" <?php echo ($sidik_jari)?'':'onclick="alert(\'User Belum Terdaftar\'); return false;"';?>><span class="fa fa-plus"></span> Tambah Kartu</a>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th width="20">#</th>
                        <th>Kartu</th>
                        <th>Nomor</th>
                        <th>Utama</th>
                        <th width="100">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $grid_sidik_kartu = $this->Sidik_kartu_model->grid_all_sidik_kartu("", "sidik_kartu.sidik_kartu_nama", "ASC", 0, 0, "sidik_kartu.siswa_id = '$siswa_id'");
                    $i = 1;
                    if ($grid_sidik_kartu) {
                      foreach($grid_sidik_kartu as $row_sidik_kartu){
                        echo "<tr>
                            <td class=\"text-center\">$i</td>
                            <td><label>".$row_sidik_kartu->sidik_kartu_nama."</label></td>
                            <td><label>".$row_sidik_kartu->sidik_kartu_nomor."</label></td>
                            <td class=\"text-center\"><label>".$row_sidik_kartu->sidik_kartu_utama."</label></td>
                            <td>
                              <a href=\"".module_url($this->uri->segment(2) . "/main_card/".$siswa_id."/".$row_sidik_kartu->sidik_kartu_id) ."\" class=\"btn btn-success btn-sm\" style=\"margin-right:5px;margin-bottom:5px;\" title=\"Atur Kartu Utama\" onclick=\"return confirm('Apakah Anda yakin akan mengatur kartu ini sebagai kartu utama?');\"><i class=\"fa fa-check\"></i></a>
                              <a href=\"".module_url($this->uri->segment(2) . "/delete_card/".$siswa_id."/".$row_sidik_kartu->sidik_kartu_id) ."\" class=\"btn btn-danger btn-sm\" style=\"margin-right:5px;margin-bottom:5px;\" title=\"Hapus Kartu\" onclick=\"return confirm('Apakah Anda yakin akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i></a>
                            </td>
                            </tr>";
                        $i++;
                      }
                    }
                    ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'); ?>'" class="btn btn-default">Kembali</button>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
				  </form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
    <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Berhasil</h4>
        </div>
        <div class="modal-body">
        <?php if ($this->session->flashdata('success')) {
          echo $this->session->flashdata('success');
        } ?>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
    
    <div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Gagal</h4>
        </div>
        <div class="modal-body">
        <?php if ($this->session->flashdata('error')) {
          echo $this->session->flashdata('error');
        } ?>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>

<?php } else if ($action == 'add_card') {?>
<script>
  $(function () {
    $('#form-card').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        return false;
      }
    });
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sidik Jari Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('sidik_jari_guru'); ?>">Sidik Jari Siswa</a></li>
            <li class="active">Data Sidik Jari Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
				  <form id="form-card" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$siswa_id);?>" method="post">
          <input type="hidden" name="siswa_id" value="<?php echo $siswa_id; ?>" />
          <input type="hidden" name="sidik_jari_user" value="<?php echo $sidik_jari_user; ?>" />
          <input type="hidden" name="sidik_jari_id" value="<?php echo $sidik_jari_id; ?>" />
          <div class="row">
            <div class="col-md-6">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Kartu Siswa</h3>
                  <div class="pull-right">
                    <?php echo ($sidik_jari)?'<button class="btn btn-sm btn-success"><span class="fa fa-check"></span> Terdaftar</button>':'<button class="btn btn-sm btn-danger"><span class="fa fa-remove"></span> Tidak Terdaftar</button>';?>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
                  <div class="form-group">
                      <label for="user_id" class="col-md-3 control-label">User ID</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="user_id" id="user_id" value="<?php echo $user_id; ?>" placeholder="" required readonly>
                          <small>User ID digunakan pada Mesin</small>
                      </div>
                  </div>
                  <!-- <div class="form-group">
                      <label for="sidik_jari_user" class="col-md-3 control-label">User</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="sidik_jari_user" id="sidik_jari_user" value="<?php echo $sidik_jari_user; ?>" placeholder="" required readonly>
                          <small>User digunakan pada Siswa/Karyawan</small>
                      </div>
                  </div> -->
                  <div class="form-group">
                      <label for="sidik_jari_nama" class="col-md-3 control-label">Nama</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="sidik_jari_nama" id="sidik_jari_nama" value="<?php echo $sidik_jari_nama; ?>" placeholder="" required readonly>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="sidik_jari_level" class="col-md-3 control-label">Departemen</label>
                      <div class="col-md-8">
                        <?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Alumni') ORDER BY departemen_urutan ASC")->result(), 'sidik_jari_level_', 'departemen_id', 'departemen_nama', $sidik_jari_level, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" disabled');?>
                      </div>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-6">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Kartu</h3>
                </div><!-- /.box-header -->
                <div class="box-body form-horizontal">
                  <div class="form-group">
                      <label for="sidik_kartu_nama" class="col-md-3 control-label">Kartu Nama</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="sidik_kartu_nama" id="sidik_kartu_nama" value="<?php echo $sidik_kartu_nama; ?>" placeholder="" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="sidik_kartu_utama" class="col-md-3 control-label">Kartu Utama</label>
                      <div class="col-md-8">
                        <div class="radio">
                          <label>
                            <input type="radio" name="sidik_kartu_utama" value="Y" <?php echo ($sidik_kartu_utama == 'Y')?'checked':''; ?> required> Ya
                          </label>
                          &nbsp;&nbsp;&nbsp;
                          <label>
                            <input type="radio" name="sidik_kartu_utama" value="N" <?php echo ($sidik_kartu_utama == 'N')?'checked':''; ?> required> Tidak
                          </label>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="sidik_kartu_nomor" class="col-md-3 control-label">Kartu Nomor</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="sidik_kartu_nomor" id="sidik_kartu_nomor" value="<?php echo $sidik_kartu_nomor; ?>" placeholder="" required autocomplete="off">
                      </div>
                  </div>
                </div><!-- /.box-body -->             
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$siswa_id); ?>'" class="btn btn-default">Kembali</button>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
				  </form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'upload') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sidik Jari Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('sidik_jari_siswa'); ?>">Sidik Jari Siswa</a></li>
            <li class="active">Upload Sidik Jari Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Upload Sidik Jari Siswa</h3>
                </div><!-- /.box-header -->
                <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-body form-horizontal">
                    <div class="form-group">
                        <label for="mesin_id" class="col-sm-2 control-label">Mesin</label>
                        <div class="col-md-6 col-sm-8">
                        <?php combobox('db', $this->db->query("SELECT * FROM sat_mesin ORDER BY mesin_nama ASC")->result(), 'mesin_id', 'mesin_id', 'mesin_nama', $mesin_id, '', 'Pilih Mesin', 'class="form-control select2" required');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sidik_jari" class="col-sm-2 control-label">Template Jari</label>
                        <div class="col-sm-10">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sidik_jari" value="Y" <?php echo ($sidik_jari == 'Y')?'checked':''; ?> required> Ya
                                </label>
                                &nbsp;&nbsp;&nbsp;
                                <label>
                                    <input type="radio" name="sidik_jari" value="N" <?php echo ($sidik_jari == 'N')?'checked':''; ?> required> Tidak
                                </label>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Upload</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'); ?>'" class="btn btn-default">Kembali</button>
                </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php } else if ($action == 'download') {?>
  <script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sidik Jari Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('sidik_jari_siswa'); ?>">Sidik Jari Siswa</a></li>
            <li class="active">Download Sidik Jari Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Download Sidik Jari Siswa</h3>
                </div><!-- /.box-header -->
                <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-body form-horizontal">
                    <div class="form-group">
                        <label for="mesin_id" class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-md-6 col-sm-8">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th class="text-left">Mesin</th>
                                <th class="text-center">Jumlah User</th>
                                <th class="text-center" width="150">Download</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                            $mesin = $this->db->query("SELECT * FROM sat_mesin ORDER BY mesin_nama ASC")->result();
                            if ($mesin){
                              foreach ($mesin as $row) {
                                $mesin_sidik = $this->db->query("SELECT * FROM sat_sidik_mesin sidik_mesin 
                                                                        LEFT JOIN (SELECT * FROM sat_sidik_jari GROUP BY sat_sidik_jari.sidik_jari_user) sidik_jari ON sidik_mesin.sidik_jari_user=sidik_jari.sidik_jari_user 
                                                                WHERE sidik_mesin.mesin_id = '".$row->mesin_id."' 
                                                                  AND sidik_mesin.siswa_id IS NOT NULL
                                                                  AND sidik_jari.sidik_jari_level = 'Siswa'")->num_rows();
                                ?>
                                <tr>
                                  <td><?php echo $row->mesin_nama; ?></td>
                                  <td class="text-center"><?php echo $mesin_sidik; ?></td>
                                  <td class="text-center"><a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3)).'?mesin='.$row->mesin_id;?>" class="btn btn-primary">Download</a></td>
                                </tr>
                                <?php
                              }
                            }
                            ?>
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <!-- <button type="submit" class="btn btn-primary" name="save" value="save">Download</button> -->
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'); ?>'" class="btn btn-default">Kembali</button>
                </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'generate') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sidik Jari Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('sidik_jari_siswa'); ?>">Sidik Jari Siswa</a></li>
            <li class="active">Sidik Jari Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Generate User Finger dari User Login</h3>
                </div><!-- /.box-header -->
                <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4));?>" method="post">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="departemen_id" class="control-label">Departemen</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Alumni') ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="tingkat_id" class="control-label">Tahun Ajaran</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
                        </div>		
                      </div>
                    </div>
                    <?php if ($this->session->userdata('level') == 10){?>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="tingkat_id" class="control-label">Tingkat</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tingkat.departemen_id = '$departemen_id' AND kelas.tahun_kode = '$tahun_kode' AND kelas.siswa_id = '$siswa_id' GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', 'Pilih Tingkat', 'class="form-control select2" required');?>
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="kelas_id" class="control-label">Kelas</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$tingkat_id' AND kelas.siswa_id = '$siswa_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', 'Semua Kelas', 'class="form-control select2"');?>
                        </div>		
                      </div>
                    </div>
                    <?php } else { ?>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="tingkat_id" class="control-label">Tingkat</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM tingkat WHERE tingkat.departemen_id = '$departemen_id' ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', 'Pilih Tingkat', 'class="form-control select2" required');?>
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="kelas_id" class="control-label">Kelas</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '$tahun_kode' AND tingkat_id = '$tingkat_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', 'Semua Kelas', 'class="form-control select2"');?>
                        </div>		
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="box-body form-horizontal">
                    <div class="form-group">
                        <label for="mesin_id" class="col-sm-2 control-label">Ke Mesin</label>
                        <div class="col-md-6">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th width="20">#</th>
                              <th width="100">Mesin</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php
                          $grid_mesin = $this->Mesin_model->grid_all_mesin("", "mesin.mesin_nama", "ASC");
                          $i = 1;
                          if ($grid_mesin) {
                            foreach($grid_mesin as $row_mesin){
                              $where_kelas = "";
                              if ($kelas_id){
                                $where_kelas .= " AND kelas.kelas_id = '$kelas_id'";
                              }
                              $mesin_sidik = $this->db->query("SELECT * FROM sat_sidik_mesin sidik_mesin 
                                                                        LEFT JOIN (SELECT * FROM sat_sidik_jari GROUP BY sat_sidik_jari.siswa_id) sidik_jari ON sidik_mesin.sidik_jari_id=sidik_jari.sidik_jari_id 
                                                                        LEFT JOIN siswa_kelas ON sidik_jari.siswa_id=siswa_kelas.siswa_id
                                                                WHERE sidik_mesin.mesin_id = '".$row_mesin->mesin_id."' 
                                                                  AND siswa_kelas.tahun_kode = '$tahun_kode'
                                                                  AND siswa_kelas.kelas_id IN (SELECT kelas.kelas_id FROM kelas WHERE kelas.tingkat_id = '$tingkat_id' $where_kelas)
                                                                  AND sidik_mesin.siswa_id IS NOT NULL
                                                                  AND sidik_jari.siswa_id IS NOT NULL
                                                                  AND sidik_jari.staf_id IS NULL")->num_rows();
                              $check = ($mesin_sidik > 0)?'checked':'';
                              echo "<tr>
                                  <td class=\"text-center\"><input type=\"checkbox\" name=\"mesin[]\" id=\"mesin_$row_mesin->mesin_id\" value=\"$row_mesin->mesin_id\" $check /></td>
                                  <td><label for=\"mesin_$row_mesin->mesin_id\">".$row_mesin->mesin_nama." (".$mesin_sidik.")</label></td>
                                  </tr>";
                              $i++;
                            }
                          }
                          ?>
                          </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mesin_id" class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-md-10 text-bold">
                        Proses ini akan dilakukan apabila kolom user login pada data Siswa telah tersedia dan hanya angka saja.<br />
                        Proses ini tidak mengirimkan data User Finger ke Alat, Hanya untuk memberikan User Finger dan Pemetaan Mesin saja. Untuk mengupload ke Mesin silahkan gunakan menu upload pada halaman Sidik Jari Siswa.
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Generate</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'); ?>'" class="btn btn-default">Kembali</button>
                </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>