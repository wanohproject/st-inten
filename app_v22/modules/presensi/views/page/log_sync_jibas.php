<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>


<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datepicker/datepicker3.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/datepicker/bootstrap-datepicker.js');?>"></script>

<script>
  $(function () {
	var tbl_auto = $("#datagrid").DataTable({
		"processing": false,
    "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('log-sync-jibas/datatable/'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "log_tanggal"},
			{ "data": "user_id"},
			{ "data": "sidik_jari_user"},
			{ "data": "sidik_jari_nama"},
			{ "data": "mesin_nama"}
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'desc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});

  $('#presensi_tanggal_awal').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true,
      endDate: '0d'
  });
  $('#presensi_tanggal_akhir').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true,
      startDate: $('#presensi_tanggal_awal').val(),
      endDate: '0d'
  });

	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show');
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>
});
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sync Log JIBAS
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('presensi_guru'); ?>">Sync Log JIBAS</a></li>
            <li class="active">Daftar Sync Log JIBAS</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Sync Log JIBAS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$tahun_kode.'/'.$semester_id);?>" method="post">
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
								
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="semester_id" class="control-label">Semester</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="presensi_tanggal_awal" class="control-label">Awal</label>
                        <div class="">
                          <input type="text" class="form-control" name="presensi_tanggal_awal" id="presensi_tanggal_awal" value="<?php echo $presensi_tanggal_awal; ?>" placeholder="" onchange="submit();">
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="presensi_tanggal_akhir" class="control-label">Akhir</label>
                        <div class="">
                          <input type="text" class="form-control" name="presensi_tanggal_akhir" id="presensi_tanggal_akhir" value="<?php echo $presensi_tanggal_akhir; ?>" placeholder="" onchange="submit();">
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="" class="control-label">&nbsp;</label>
                        <div class="">
                          <button type="submit" class="btn btn-primary">Tampilkan</button>
                        </div>		
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-body -->
								</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>User ID</th>
                        <th>User</th>
                        <th>Nama</th>
                        <th>Mesin</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>