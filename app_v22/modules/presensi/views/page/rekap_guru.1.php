<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datepicker/datepicker3.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script>
$(function () {
  $('#presensi_tanggal_awal').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true,
      endDate: '0d'
  });
  $('#presensi_tanggal_akhir').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true,
      startDate: $('#presensi_tanggal_awal').val(),
      endDate: '0d'
  });
});
</script>
<style>
#datagrid,
#datagrid td,
#datagrid th {
  border-color: #000;
  padding: 3px 5px;
}
</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Rekap Presensi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('presensi_guru'); ?>">Rekap Presensi</a></li>
            <li class="active">Daftar Rekap Presensi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Rekap Presensi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$staf_user);?>" method="post">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="departemen_id" class="control-label">Departemen</label>
												<div class="">
													<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_id", "departemen_id", "departemen_nama", $departemen_id, 'submit()', '-- PILIH DEPARTEMEN --', 'class="form-control select2"');?>
												</div>		
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
								
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="semester_id" class="control-label">Semester</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
                        </div>		
                      </div>
                    </div>

                    <div class="col-md-4">
											<div class="form-group">
												<label for="staf_user" class="control-label">Staf</label>
												<div class="">
                          <?php combobox('db', $this->Staf_model->grid_staf_departemen($departemen_id), 'staf_id', 'staf_id', 'staf_nama', $staf_id, '', 'Pilih Staf', 'class="form-control select2" required');?>
												</div>
											</div>
										</div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="presensi_tanggal_awal" class="control-label">Awal</label>
                        <div class="">
                          <input type="text" class="form-control" name="presensi_tanggal_awal" id="presensi_tanggal_awal" value="<?php echo $presensi_tanggal_awal; ?>" placeholder="" onchange="submit();" autocomplete="off">
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="presensi_tanggal_akhir" class="control-label">Akhir</label>
                        <div class="">
                          <input type="text" class="form-control" name="presensi_tanggal_akhir" id="presensi_tanggal_akhir" value="<?php echo $presensi_tanggal_akhir; ?>" placeholder="" onchange="submit();" autocomplete="off">
                        </div>		
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="" class="control-label">&nbsp;</label>
                        <div class="">
                          <button type="submit" class="btn btn-primary">Tampilkan</button>
                        </div>		
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-body -->
								</form>
                <?php if ($days && $staf_user != '-'){ ?>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Hari</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                        <th>Jam Masuk</th>
                        <th>Jam Pulang</th>
                        <th>Lama Kerja</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $hadir = 0;
                    $terlambat = 0;
                    $tidakhadir = 0;
                    $j = 1;
                    for ($i=0; $i < $days; $i++){
                      if ($i == 0){
                        $date = $presensi_tanggal_awal;
                      } else {
                        $date = date('Y-m-d', strtotime($presensi_tanggal_awal . " + $i days"));
                      }

                      $N = date('N', strtotime($date));
                      $jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari_level));
                      
                      if ($jadwal){
                        $color = "FBCBCB";
                        $color_masuk = "FBCBCB";
                        $color_pulang = "FBCBCB";
                        $status = "TK";

                        if ($this->Libur_model->count_all_libur('', array('libur_tanggal'=>$date)) < 1){
                          $presensi	= $this->Presensi_model->get_presensi('*', "presensi.tahun_kode = '$tahun_kode' AND presensi.semester_id = '$semester_id' AND presensi.presensi_user = '$staf_user' AND DATE(presensi.presensi_tanggal_masuk) = '$date'");                        
                          if ($presensi){
                            $status = "Hadir";
                          }
                          
                          $presensi_tanggal_masuk = ($presensi)?($presensi->presensi_tanggal_masuk)?$presensi->presensi_tanggal_masuk:'':'';
                          $presensi_tanggal_keluar = ($presensi)?($presensi->presensi_tanggal_keluar)?$presensi->presensi_tanggal_keluar:'':'';
                          $jam_masuk = ($presensi_tanggal_masuk)?date('H:i:s', strtotime($presensi->presensi_tanggal_masuk)):'';
                          $jam_pulang = ($presensi_tanggal_keluar)?date('H:i:s', strtotime($presensi->presensi_tanggal_keluar)):'';
                          
                          $jam_masuk_ = $jam_masuk;
                          $jam_pulang_ = $jam_pulang;
                          if ($presensi_tanggal_masuk){
                            if (strtotime($presensi_tanggal_masuk) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
                              $color = "#B3DE81";
                              $color_masuk = "#B3DE81";
                              $hadir++;
                            } else if (strtotime($presensi_tanggal_masuk) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
                              $color = "#E3E887";
                              $color_masuk = "#E3E887";
                              $terlambat++;
                            }
                          }
                          
                          if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($presensi_tanggal_keluar) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
                            $color_pulang = "#B3DE81";
                          } else if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($presensi_tanggal_keluar) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
                            $color_pulang = "#E3E887";
                          }

                          $absensi	= $this->Absensi_model->get_absensi('*', "absensi.tahun_kode = '$tahun_kode' AND absensi.semester_id = '$semester_id' AND absensi.absensi_user = '$staf_user' AND DATE(absensi.absensi_tanggal) = '$date'");
                          if ($absensi){
                            $status = $absensi->absensi_info;
                            $color = "FBCBCB";
                          }

                          if ($status != "Hadir"){
                            $tidakhadir++;
                          }

                          $lama_kerja = ($jam_masuk_ && $jam_pulang_)?selisihDate($jam_masuk_, $jam_pulang_):'';
                          ?>
                          <tr>
                            <td class="text-center"><?php echo $j; ?></td>
                            <td class="text-center"><?php echo inday($date); ?></td>
                            <td class="text-center"><?php echo dateIndo($date); ?></td>
                            <td class="text-center" bgcolor="<?php echo $color; ?>"><?php echo $status; ?></td>
                            <td class="text-center" bgcolor="<?php echo $color_masuk; ?>"><?php echo $jam_masuk; ?></td>
                            <td class="text-center" bgcolor="<?php echo $color_pulang; ?>"><?php echo $jam_pulang; ?></td>
                            <td class="text-center"><?php echo $lama_kerja; ?></td>
                          </tr>
                          <?php
                        } else {
                          $status = "Libur";
                          ?>
                          <tr>
                            <td class="text-center"><?php echo $j; ?></td>
                            <td class="text-center"><?php echo inday($date); ?></td>
                            <td class="text-center"><?php echo dateIndo($date); ?></td>
                            <td class="text-center" bgcolor="<?php echo $color; ?>"><?php echo $status; ?></td>
                            <td class="text-center" bgcolor="<?php echo $color; ?>">-</td>
                            <td class="text-center" bgcolor="<?php echo $color; ?>">-</td>
                            <td class="text-center">-</td>
                          </tr>
                          <?php
                        }
                        $j++;
                      }
                    }
                    ?>
                      <tr>
                        <th class="text-left" colspan="2">Hadir</th>
                        <th class="text-left" colspan="5">: <?php echo $hadir; ?></th>
                      </tr>
                      <tr>
                        <th class="text-left" colspan="2">Terlambat</th>
                        <th class="text-left" colspan="5">: <?php echo $terlambat; ?></th>
                      </tr>
                      <tr>
                        <th class="text-left" colspan="2">Tidak Hadir</th>
                        <th class="text-left" colspan="5">: <?php echo $tidakhadir; ?></th>
                      </tr>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
                <?php } else { ?>
                <?php
                $day = array();
                for ($i=0; $i < $days; $i++) {
                  if ($i == 0){
                    $date = $presensi_tanggal_awal;
                  } else {
                    $date = date('Y-m-d', strtotime($presensi_tanggal_awal . " + $i days"));
                  }
                  $N = date('N', strtotime($date));
                  $jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari_level));
                  
                  if ($jadwal){
                    $day[] = $date;
                  }
                }  
                ?>
                <div class="box-body" style="overflow-x:scroll">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="text-center" rowspan="2" width="35">No</th>
                        <th class="text-center" rowspan="2" width="200">Nama</th>
                        <th class="text-center" colspan="<?php echo count($day); ?>"><?php echo getBulan(date('m', strtotime($presensi_tanggal_awal))); ?></th>
                        <th class="text-center" rowspan="2" width="120">Lama Kerja</th>
                      </tr>
                      <tr>
                      <?php
                      foreach ($day as $value) {
                        echo "<th class=\"text-center\" width=\"40\">".date('d', strtotime($value))."</th>";
                      }
                      ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $hadir = 0;
                    $terlambat = 0;
                    $tidakhadir = 0;
                    $grid_staf = $this->Staf_model->grid_all_staf("", "staf_nama", "ASC", 0, 0, "staf_tst = '0000-00-00' OR staf_tst >= '$presensi_tanggal_awal' OR staf_tst IS NULL");
                    $i = 1;
                    foreach ($grid_staf as $row) {
                      $staf_user = $row->staf_user;
                      echo "<tr>";
                      echo "<td class=\"text-center\">$i</td>";
                      echo "<td class=\"text-left\">$row->staf_nama</td>";
                      $lama_kerja = 0;
                      foreach ($day as $value) {
                        $date = $value;
                        $color = "FBCBCB";
                        $color_masuk = "FBCBCB";
                        $color_pulang = "FBCBCB";
                        $status = "TK";

                        if ($this->Libur_model->count_all_libur('', array('libur_tanggal'=>$date)) < 1){
                          $presensi	= $this->Presensi_model->get_presensi('*', "presensi.tahun_kode = '$tahun_kode' AND presensi.semester_id = '$semester_id' AND presensi.presensi_user = '$staf_user' AND DATE(presensi.presensi_tanggal_masuk) = '$date'");                        
                          if ($presensi){
                            $status = "H";
                          }
                          
                          $presensi_tanggal_masuk = ($presensi)?($presensi->presensi_tanggal_masuk)?$presensi->presensi_tanggal_masuk:'':'';
                          $presensi_tanggal_keluar = ($presensi)?($presensi->presensi_tanggal_keluar)?$presensi->presensi_tanggal_keluar:'':'';
                          $jam_masuk = ($presensi_tanggal_masuk)?date('H:i:s', strtotime($presensi->presensi_tanggal_masuk)):'';
                          $jam_pulang = ($presensi_tanggal_keluar)?date('H:i:s', strtotime($presensi->presensi_tanggal_keluar)):'';
                          
                          $jam_masuk_ = $jam_masuk;
                          $jam_pulang_ = $jam_pulang;
                          if ($presensi_tanggal_masuk){
                            if (strtotime($presensi_tanggal_masuk) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
                              $color = "#B3DE81";
                              $color_masuk = "#B3DE81";
                              $hadir++;
                            } else if (strtotime($presensi_tanggal_masuk) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
                              $color = "#E3E887";
                              $color_masuk = "#E3E887";
                              $terlambat++;
                            }
                          }
                          
                          if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($presensi_tanggal_keluar) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
                            $color_pulang = "#B3DE81";
                          } else if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($presensi_tanggal_keluar) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
                            $color_pulang = "#E3E887";
                          }

                          $absensi	= $this->Absensi_model->get_absensi('*', "absensi.tahun_kode = '$tahun_kode' AND absensi.semester_id = '$semester_id' AND absensi.absensi_user = '$staf_user' AND DATE(absensi.absensi_tanggal) = '$date'");
                          if ($absensi){
                            $status = substr($absensi->absensi_info, 0, 1);
                            $color = "FBCBCB";
                          }

                          if ($status != "H"){
                            $tidakhadir++;
                          }

                          // if ($jam_masuk_ && !$jam_pulang_){
                          //   $status = "H";
                          //   $color = "FBCBCB";
                          // }

                          $lama_kerja_ = ($jam_masuk_ && $jam_pulang_)?selisihDate2($jam_masuk_, $jam_pulang_):0;
                          $lama_kerja = $lama_kerja + $lama_kerja_;

                          echo "<td class=\"text-center\" bgcolor=\"$color\">$status</td>";
                        } else {
                          $status = "L";
                          echo "<td class=\"text-center\" bgcolor=\"$color\">$status</td>";
                        }
                      }
                      echo "<td class=\"text-center\">".stoh($lama_kerja)."</td>";
                      // echo "<td class=\"text-center\">".$lama_kerja."</td>";
                      echo "</tr>";
                      $i++;
                    }
                    ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
                <?php }  ?>
                <div class="box-footer">
                  <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
                  <?php
                    if ($semester_id && $semester_id != '-' && $staf_id != '-'){
                      ?>
                      <a href="<?php echo module_url($this->uri->segment(2).'/cetak/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$staf_id.'/'.$presensi_tanggal_awal.'/'.$presensi_tanggal_akhir); ?>" target="_blank" class="btn btn-info">Cetak</a>
                      <?php if ($this->session->userdata('level') != 10){?>
                      <a href="<?php echo module_url($this->uri->segment(2).'/word/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$staf_id.'/'.$presensi_tanggal_awal.'/'.$presensi_tanggal_akhir); ?>" target="_blank" class="btn btn-primary">Export Word Rekap Presensi</a>
                      <?php } ?>
                      <?php
                    } else if ($semester_id && $semester_id != '-'){
                      ?>
                      <a href="<?php echo module_url($this->uri->segment(2).'/word_all/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$presensi_tanggal_awal.'/'.$presensi_tanggal_akhir.'/guru'); ?>" target="_blank" class="btn btn-primary">Export Word Presensi</a>
                      <!-- <a href="<?php echo module_url($this->uri->segment(2).'/word_all/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$presensi_tanggal_awal.'/'.$presensi_tanggal_akhir.'/tu'); ?>" target="_blank" class="btn btn-primary">Export Word Presensi TU</a> -->
                      <a href="<?php echo module_url($this->uri->segment(2).'/excel_all/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$presensi_tanggal_awal.'/'.$presensi_tanggal_akhir.'/guru'); ?>" target="_blank" class="btn btn-success">Export Excel Presensi</a>
                      <!-- <a href="<?php echo module_url($this->uri->segment(2).'/excel_all/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$presensi_tanggal_awal.'/'.$presensi_tanggal_akhir.'/tu'); ?>" target="_blank" class="btn btn-success">Export Excel Presensi TU</a> -->
                      <?php
                    }
                  ?>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>