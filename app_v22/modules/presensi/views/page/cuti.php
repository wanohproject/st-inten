<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datepicker/datepicker3.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('cuti/datatable/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$cuti_tanggal_mulai); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "cuti_hari"},
			{ "data": "cuti_tanggal"},
			{ "data": "staf_user"},
			{ "data": "staf_nama"},
			{ "data": "cuti_status"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
  $('#cuti_tanggal_mulai').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true
  });
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Cuti
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('cuti'); ?>">Cuti</a></li>
            <li class="active">Daftar Cuti</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Cuti</h3>
									<div class="pull-right">
										<a href="<?php echo module_url($this->uri->segment(2).'/add/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$cuti_tanggal_mulai); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
									</div>
                </div><!-- /.box-header -->
                <form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$cuti_tanggal_mulai);?>" method="post">
                <div class="box-body">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="departemen_id" class="control-label">Departemen</label>
												<div class="">
													<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_id", "departemen_id", "departemen_nama", $departemen_id, 'submit()', '-- PILIH DEPARTEMEN --', 'class="form-control select2"');?>
												</div>		
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
								
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="semester_id" class="control-label">Semester</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
                        </div>		
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="cuti_tanggal_mulai" class="control-label">Tanggal</label>
                        <div class="">
                          <input type="text" class="form-control" name="cuti_tanggal_mulai" id="cuti_tanggal_mulai" value="<?php echo $cuti_tanggal_mulai; ?>" placeholder="" onchange="submit();">
                        </div>		
                      </div>
                    </div>
                    
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="" class="control-label">&nbsp;</label>
                        <div class="">
                          <button type="submit" class="btn btn-primary">Tampilkan</button>
                        </div>		
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-body -->
								</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Hari</th>
                        <th>Tanggal</th>
                        <th>User ID</th>
                        <th>Nama</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	<?php } else if ($action == 'add') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datepicker/datepicker3.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script>
$(function () {
  $('#cuti_tanggal_mulai').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true
  });
  $('#cuti_tanggal_selesai').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true
  });
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Cuti
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mesin'); ?>">Cuti</a></li>
            <li class="active">Tambah Cuti</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Cuti</h3>
                </div><!-- /.box-header -->
								<form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$cuti_tanggal_mulai);?>" method="post" enctype="multipart/form-data">
                <div class="box-body">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="departemen_id" class="control-label">Departemen</label>
												<div class="">
													<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_id", "departemen_id", "departemen_nama", $departemen_id, 'submit()', '-- PILIH DEPARTEMEN --', 'class="form-control select2"');?>
												</div>		
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
								
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="semester_id" class="control-label">Semester</label>
                        <div class="">
                          <?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
                        </div>		
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-body -->
								<div class="box-body form-horizontal">
									<div class="form-group">
										<label for="staf_id" class="col-md-2 control-label">Staf</label>
										<div class="col-md-8">
											<?php combobox('db', $this->Staf_model->grid_staf_departemen($departemen_id), 'staf_id', 'staf_id', 'staf_nama', $staf_id, 'submit();', 'Pilih Staf', 'class="form-control select2" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="cuti_jabatan" class="col-md-2 control-label">Jabatan</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="cuti_jabatan" id="cuti_jabatan" value="<?php echo $cuti_jabatan; ?>" placeholder="" required>
										</div>
									</div>
									<div class="form-group">
										<label for="cuti_jabatan" class="col-md-2 control-label">Jabatan</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="cuti_jabatan" id="cuti_jabatan" value="<?php echo $cuti_jabatan; ?>" placeholder="" required>
										</div>
									</div>
									<div class="form-group">
										<label for="cuti_tanggal_mulai" class="col-md-2 control-label">Unit</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="cuti_tanggal_mulai" id="cuti_tanggal_mulai" value="<?php echo $cuti_tanggal_mulai; ?>" placeholder="" required>
										</div>
									</div>
									<div class="form-group">
										<label for="cuti_tanggal_mulai" class="col-md-2 control-label">Awal</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="cuti_tanggal_mulai" id="cuti_tanggal_mulai" value="<?php echo $cuti_tanggal_mulai; ?>" placeholder="" required>
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" name="cuti_tanggal_selesai" id="cuti_tanggal_selesai" value="<?php echo $cuti_tanggal_selesai; ?>" placeholder="" required>
										</div>		
									</div>
									<div class="form-group">
										<label for="cuti_info" class="col-md-2 control-label">Status</label>
										<div class="col-md-8">
										<?php combobox('1d', array('Izin', 'Sakit', 'Cuti', 'TK', 'Dinas'), 'cuti_info', '', '', $cuti_info, '', 'none', 'class="form-control select2" required'); ?>
										</div>
									</div>
									<div class="form-group">
										<label for="cuti_deskripsi" class="col-md-2 control-label">Keterangan</label>
										<div class="col-md-8">
											<textarea class="form-control" name="cuti_deskripsi" id="cuti_deskripsi" value="<?php echo $cuti_deskripsi; ?>" placeholder=""></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="cuti_file" class="col-md-2 control-label">File</label>
										<div class="col-md-8">
											<input type="file" class="form-control" name="cuti_file" id="cuti_file" value="" placeholder="">
										</div>
									</div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
                    <button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$cuti_tanggal_mulai); ?>'" class="btn btn-default">Batalkan</button>
                </div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>