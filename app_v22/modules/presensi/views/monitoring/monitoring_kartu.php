<script src="<?php echo theme_dir('admin_v2/plugins/highcharts/code/highcharts.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/rfid/jquery.rfid.js');?>"></script>
<style>
.logpresensi {
    color: #FFFFFF;
}
.logpresensi .time span {
    font-size: 20px;
    float:left;
}
.logpresensi .time span.small {
    font-size: 14px;
    float:left;
    padding: 2px 0px 0px 3px;
}
.logpresensi h4 {
    padding: 5px 0px;
    margin: 0px;
}
.logpresensi h5 {
    padding: 0px;
    margin: 0px;
}
.logpresensi td,
.logpresensi th {
    border: 1px solid #fefefe !important;
    /* color: #000000 !important; */
}
.logpresensi th {
    border: 1px solid #fefefe !important;
    color: #000000 !important;
}

#my_camera {
    /* visibility:hidden; */
}
#my_camera,
#my_camera video{
    width:100% !important;
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            MONITORING PRESENSI STAF
            <small><?php echo profile('profil_institusi'); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-dashboard"></i>Beranda</a></li>
            <li class="active">Monitoring Presensi Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-6">
                <div class="box box-info">
                    <form id="form-pesensi" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Kehadiran Harian Staf</h3>
                        <div class="pull-right"><input type="text" class="form-control" name="tanggal" id="tanggal" value="<?php echo $tanggal; ?>" placeholder="" autocomplete="off"></div>
                    </div><!-- /.box-header -->
                    </form>
                    <div class="box-body" style="height: 600px;position:relative;overflow-y:scroll">
                        <table id="datagrid" class="table logpresensi">
                            <thead>
                                <tr bgcolor="#F9F9F9">
                                    <th style="text-align:center;" width="120">NIP</th>
                                    <th style="text-align:center;">NAMA</th>
                                    <th style="text-align:center;" width="120">MASUK</th>
                                    <th style="text-align:center;" width="120">PULANG</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.row -->
            <div class="col-md-6">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Kehadiran Guru /Staf</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6"><div id="container_1" style="height: 200px; margin: 0 auto"></div></div>
                        <div class="col-sm-6"><div id="container_2" style="height: 200px; margin: 0 auto"></div></div>
                    </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Presensi Kartu</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="infopresensi" class="table logpresensi">
                    <tbody></tbody>
                </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Preview Foto</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-sm-12" style="overflow:hidden;"><div id="my_camera" style="width:100%"></div></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.row -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <input type="hidden" id="dataset_x" value="">
      
<script src="<?php echo theme_dir('admin_v2/plugins/webcamjs/webcam.min.js');?>"></script>
<script language="JavaScript">
    var cameraIsOn = false;    
    
    if (navigator.mediaDevices.getUserMedia) {               
        
        navigator.mediaDevices.getUserMedia({video: true})
        .then(function(stream) {
            Webcam.set({
                width: 320,
                height: 240,
                image_format: 'jpeg',
                jpeg_quality: 90,
                debug: function(type, message){ 
                    alert(message);
                }
            });
            Webcam.attach( '#my_camera' );
            
            cameraIsOn = true
        })
        .catch(function(err0r) {
            console.log("Something went wrong!");
        });
    }
</script>
<script>
  $(function () {
      // Parses raw scan into name and ID number
	var rfidParser = function (rawData) {
		// console.log(rawData);
	    if (rawData.length != 11) return null;
		else return rawData;
	    
	};

	// Called on a good scan (company card recognized)
	var goodScan = function (cardData) {
        // $("#rfid_card").val(cardData.substr(0,10));
        var dataFoto = "";
        if (cameraIsOn == true){
            Webcam.snap( function(data_uri) {
                dataFoto = data_uri;
            });
        }

        // console.log(dataFoto);
        
        var params = {
            cardNumber: cardData,
            dataFoto: dataFoto
        };
    
        $.ajax({
            url: "<?php echo module_url('monitoring-kartu/presensi'); ?>",
            dataType: 'json',
            type: 'POST',
            data: params,
            beforeSend: function() {
                $("#infopresensi tbody").html("");
            },
            success: function(data){
                console.log(data);
                if (data.response == true){
                    $("#infopresensi tbody").html(data.log);

                    var tanggal = '<?php echo date('Y-m-d')?>';
                    $('#tanggal').val('<?php echo date('Y-m-d')?>');
                    loadLog(tanggal);
                    loadGrafik(tanggal);
                    loadGrafik2(tanggal);
                }
            },
        });
    };

	// Called on a bad scan (company card not recognized)
	var badScan = function() {
	    console.log("Bad Scan.");
	};

	// Initialize the plugin.
	$.rfidscan({
	    parser: rfidParser,
	    success: goodScan,
	    error: badScan
	});

	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show');
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>
  });
</script>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#tanggal').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            var tanggal = $('#tanggal').val();
            loadLog(tanggal);
            loadGrafik(tanggal);
            loadGrafik2(tanggal);
        }
    );
  });
</script>
<script type="text/javascript">
var dataset;
$(document).ready(function() {
    // setInterval( function () {
    //     location.reload();
    // }, 3600000 );

    var tanggal = $('#tanggal').val();
    loadLog(tanggal);
    loadGrafik(tanggal);
    loadGrafik2(tanggal);
});

function loadLog(tanggal){
    $.ajax({
        url: "<?php echo module_url('monitoring-kartu/dataload-table'); ?>/" + tanggal,
        dataType: 'json',
        // beforeSend: function() {
        //     $("#datagrid tbody").html("");
        // },
        success: function(data2){
            $("#datagrid tbody").html(data2);
        }
    });
}

function loadGrafik(tanggal){
    var chart_;
    var seriesData_;
    var xCategories_;

    $.ajax({
        url: "<?php echo module_url('monitoring-kartu/get-guru/'.$tahun_kode.'/'.$semester_id); ?>/" + tanggal,
        dataType: 'json',
        async: false,
        success: function(data){
            dataset = data;
        }
    });
    
    Highcharts.chart('container_1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.val}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.val}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: dataset
        }]
    });
}

function loadGrafik2(tanggal){
    var chart_;
    var seriesData_;
    var xCategories_;

    $.ajax({
        url: "<?php echo module_url('monitoring-kartu/get-tendik/'.$tahun_kode.'/'.$semester_id); ?>/" + tanggal,
        dataType: 'json',
        async: false,
        success: function(data){
            dataset = data;
        }
    });
    
    Highcharts.chart('container_2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.val}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.val}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: dataset
        }]
    });
}
</script>