<script src="<?php echo theme_dir('admin_v2/plugins/highcharts/code/highcharts.js');?>"></script>
<style>
.logpresensi {
    color: #FFFFFF;
}
.logpresensi .time span {
    font-size: 20px;
    float:left;
}
.logpresensi .time span.small {
    font-size: 14px;
    float:left;
    padding: 2px 0px 0px 3px;
}
.logpresensi h4 {
    padding: 5px 0px;
    margin: 0px;
}
.logpresensi h5 {
    padding: 0px;
    margin: 0px;
}
.logpresensi td,
.logpresensi th {
    border: 1px solid #fefefe !important;
    /* color: #000000 !important; */
}
.logpresensi th {
    border: 1px solid #fefefe !important;
    color: #000000 !important;
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            MONITORING PRESENSI STAF
            <small><?php echo profile('profil_institusi'); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-dashboard"></i>Beranda</a></li>
            <li class="active">Monitoring Presensi Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="small-box bg-green">
                        <div class="inner">
                            <h3 id="guru_hadir">0</h3>

                            <p>Guru Hadir</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp;</a>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="small-box bg-red">
                        <div class="inner">
                            <h3 id="guru_thadir">0</h3>

                            <p>Guru Belum Hadir</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp;</a>
                        </div>
                    </div>
                </div>
              <div class="box box-success">
                <div class="box-header">
                     <h3 class="box-title">Daftar Kehadiran Harian Guru</h3>
                    <div class="pull-right"><input type="text" class="form-control" name="tanggal_guru" id="tanggal_guru" value="<?php echo $tanggal; ?>" placeholder="" autocomplete="off"></div>
                </div><!-- /.box-header -->
                <div class="box-body" style="height: 600px;position:relative;overflow-y:scroll">
                    <table id="datagridGuru" class="table logpresensi">
                        <thead>
                            <tr bgcolor="#F9F9F9">
                                <th style="text-align:center;" width="120">NIP</th>
                                <th style="text-align:center;">NAMA</th>
                                <th style="text-align:center;" width="120">MASUK</th>
                                <th style="text-align:center;" width="120">PULANG</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="small-box bg-green">
                        <div class="inner">
                            <h3 id="tendik_hadir">0</h3>

                            <p>Pegawai Hadir</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp;</a>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="small-box bg-red">
                        <div class="inner">
                            <h3 id="tendik_thadir">0</h3>

                            <p>Pegawai Belum Hadir</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">&nbsp;</a>
                        </div>
                    </div>
                </div>
              <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Daftar Kehadiran Harian Tendik</h3>
                    <div class="pull-right"><input type="text" class="form-control" name="tanggal_tendik" id="tanggal_tendik" value="<?php echo $tanggal; ?>" placeholder="" autocomplete="off"></div>
                </div><!-- /.box-header -->
                <div class="box-body" style="height: 600px;position:relative;overflow-y:scroll">
                    <table id="datagridTendik" class="table logpresensi">
                        <thead>
                            <tr bgcolor="#F9F9F9">
                                <th style="text-align:center;" width="120">NIP</th>
                                <th style="text-align:center;">NAMA</th>
                                <th style="text-align:center;" width="120">MASUK</th>
                                <th style="text-align:center;" width="120">PULANG</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <input type="hidden" id="dataset_x" value="">
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show');
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>
  });
</script>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#tanggal_guru').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            var tanggal = $('#tanggal_guru').val();
            loadLogGuru(tanggal);
            loadCountGuru(tanggal);
        }
    );

    $('#tanggal_tendik').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            var tanggal = $('#tanggal_tendik').val();
            loadLogTendik(tanggal);
            loadCountTendik(tanggal);
        }
    );
  });
</script>
<script type="text/javascript">
var dataset;
$(document).ready(function() {
    // setInterval( function () {
    //     location.reload();
    // }, 3600000 );

    var tanggal_guru = $('#tanggal_guru').val();
    var tanggal_tendik = $('#tanggal_tendik').val();
    loadLogGuru(tanggal_guru);
    loadLogTendik(tanggal_tendik);
    loadCountGuru(tanggal_guru);
    loadCountTendik(tanggal_tendik);
});

function loadLogGuru(tanggal){
    $.ajax({
        url: "<?php echo module_url($this->uri->segment(2).'/dataload-table'); ?>/" + tanggal + "/Guru",
        dataType: 'json',
        // beforeSend: function() {
        //     $("#datagrid tbody").html("");
        // },
        success: function(data2){
            $("#datagridGuru tbody").html(data2);
        }
    });
}

function loadLogTendik(tanggal){
    $.ajax({
        url: "<?php echo module_url($this->uri->segment(2).'/dataload-table'); ?>/" + tanggal + "/Tendik",
        dataType: 'json',
        // beforeSend: function() {
        //     $("#datagrid tbody").html("");
        // },
        success: function(data2){
            $("#datagridTendik tbody").html(data2);
        }
    });
}

function loadCountGuru(tanggal){
    $.ajax({
        url: "<?php echo module_url($this->uri->segment(2).'/get-guru'); ?>/" + tanggal + "/Guru",
        dataType: 'json',
        // beforeSend: function() {
        //     $("#datagrid tbody").html("");
        // },
        success: function(data2){
            $("#guru_hadir").html(data2.hadir);
            $("#guru_thadir").html(data2.thadir);
        }
    });
}

function loadCountTendik(tanggal){
    $.ajax({
        url: "<?php echo module_url($this->uri->segment(2).'/get-guru'); ?>/" + tanggal + "/Tendik",
        dataType: 'json',
        // beforeSend: function() {
        //     $("#datagrid tbody").html("");
        // },
        success: function(data2){
            $("#tendik_hadir").html(data2.hadir);
            $("#tendik_thadir").html(data2.thadir);
        }
    });
}

$(function () {
    var synctime = <?php echo profile('presensi_synctime'); ?> * 1000;
    if (synctime >= 15000){
        <?php 
        if ($mesin){
            foreach ($mesin as $row) {
                ?>
                ajaxLoad(<?php echo $row->mesin_id; ?>);
                <?php
            }
        }
        ?>

        setInterval(function(){ 
            var tanggal_guru = $('#tanggal_guru').val();
            var tanggal_tendik = $('#tanggal_tendik').val();
            loadLogGuru(tanggal_guru);
            loadLogTendik(tanggal_tendik);
            loadCountGuru(tanggal_guru);
            loadCountTendik(tanggal_tendik);
        }, synctime);

        function ajaxLoad(mesin){
            setTimeout(function() {
                $.ajax({
                    url: "<?php echo module_url('log-auto-sinkronisasi/dataload_get'); ?>?mesin=" + mesin,
                    dataType: 'json',
                    type: 'POST',
                    success:
                    function(data){
                        // console.log(data);
                        if (data.response == true){
                            ajaxLoad(mesin);
                        } else if (data.response == false){
                            ajaxLoad(mesin);
                        }
                    },
                    complete: function(){
                    }
                });
            }, synctime);
        }
    }
});
</script>