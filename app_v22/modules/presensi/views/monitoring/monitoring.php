<style>
.info-box {
    display: block;
    min-height: 120px;
    background: #fff;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    border-radius: 2px;
    margin-bottom: 15px;
}
.info-box-content {
    padding: 5px 10px;
    margin-left: 120px;
}
.info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
    display: block;
    float: left;
    height: 120px;
    width: 120px;
    text-align: center;
    font-size: 65px;
    line-height: 120px;
    background: rgba(0,0,0,0.2);
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            MONITORING PRESENSI
            <small><?php echo profile('profil_institusi'); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-dashboard"></i>Beranda</a></li>
            <li class="active">Presensi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
			      <div class="col-md-3">
				      <!-- Info Boxes Style 2 -->
              <div class="info-box bg-yellow" style="cursor:pointer;" onclick="location.href='<?php echo module_url('monitoring-siswa-grafik'); ?>'">
                <span class="info-box-icon"><i class="fa fa-bar-chart"></i></span>

                <div class="info-box-content">
                  <h3 style="margin:0px;padding:0px;">Monitoring Siswa (Grafik)</h3>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3">
              <div class="info-box bg-green" style="cursor:pointer;" onclick="location.href='<?php echo module_url('monitoring-siswa-table'); ?>'">
                <span class="info-box-icon"><i class="fa fa-table"></i></span>

                <div class="info-box-content">
                  <h3 style="margin:0px;padding:0px;">Monitoring Siswa (Table)</h3>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3">
              <div class="info-box bg-red" style="cursor:pointer;" onclick="location.href='<?php echo module_url('monitoring-guru'); ?>'">
                <span class="info-box-icon"><i class="fa fa-pie-chart"></i></span>

                <div class="info-box-content" style="vertical-align:middle;">
                  <h3 style="margin:0px;padding:0px;">Monitoring Guru & TU</h3>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3">
              <div class="info-box bg-aqua" style="cursor:pointer;" onclick="location.href='<?php echo module_url('monitoring-guru-log'); ?>'">
                <span class="info-box-icon"><i class="fa fa-eye"></i></span>

                <div class="info-box-content" style="vertical-align:middle;">
                  <h3 style="margin:0px;padding:0px;">Monitoring Log Guru & TU</h3>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3">
              <div class="info-box bg-blue" style="cursor:pointer;" onclick="location.href='<?php echo module_url('monitoring-kartu'); ?>'">
                <span class="info-box-icon"><i class="fa fa-credit-card"></i></span>

                <div class="info-box-content" style="vertical-align:middle;">
                  <h3 style="margin:0px;padding:0px;">Presensi Kartu</h3>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->