<script src="<?php echo theme_dir('admin_v2/plugins/highcharts/code/highcharts.js');?>"></script>
<style>
.logpresensi {
    color: #FFFFFF;
}
.logpresensi .time span {
    font-size: 20px;
    float:left;
}
.logpresensi .time span.small {
    font-size: 14px;
    float:left;
    padding: 2px 0px 0px 3px;
}
.logpresensi h4 {
    padding: 5px 0px;
    margin: 0px;
}
.logpresensi h5 {
    padding: 0px;
    margin: 0px;
}
.logpresensi td,
.logpresensi th {
    border: 1px solid #fefefe !important;
    /* color: #000000 !important; */
}
.logpresensi th {
    border: 1px solid #fefefe !important;
    color: #000000 !important;
}
.dashpresensi tbody td {
    color: #FFFFFF;
    font-weight: bold;
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            MONITORING PRESENSI SISWA
            <small><?php echo profile('profil_institusi'); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-dashboard"></i>Beranda</a></li>
            <li class="active">Monitoring Presensi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id);?>" method="post">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="departemen_id" class="control-label">Departemen</label>
                                    <div class="">
                                        <?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Akademi') AND departemen_utama IS NULL ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
                                    </div>		
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box -->
			</div><!-- /.col -->
            <div class="col-md-6">
                <div class="box box-info">
                    <form id="form-pesensi" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                    <div class="box-header">
                        <h3 class="box-title">Log Presensi</h3>
                        <div class="pull-right"><input type="text" class="form-control" name="tanggal" id="tanggal" value="<?php echo $tanggal; ?>" placeholder="" autocomplete="off"></div>
                    </div><!-- /.box-header -->
                    <div class="box-body" id="list-log" style="height: 600px;position:relative;overflow-y:scroll">
                        <table id="datagrid" class="table table-bordered logpresensi">
                            <thead>
                                <tr bgcolor="#F9F9F9">
                                    <th style="text-align:center;" width="80">NIS</th>
                                    <th style="text-align:center;">NAMA</th>
                                    <th style="text-align:center;" width="80">KELAS</th>
                                    <th style="text-align:center;" width="120">MASUK</th>
                                    <th style="text-align:center;" width="120">PULANG</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.row -->
            <div class="col-md-6">
              <?php
              if ($tingkat){
                foreach ($tingkat as $row_tingkat) {
                ?>
                <div class="box box-success">
                    <div class="box-body" style="overflow-x:auto;">
                        <table class="table dashpresensi" id="table-kelas<?php echo $row_tingkat->tingkat_id; ?>">
                            <thead>
                                <tr>
                                    <th>Kelas <?php echo $row_tingkat->tingkat_romawi; ?></th>
                                    <?php
                                    $kelas = $this->db->query("SELECT * FROM kelas WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$row_tingkat->tingkat_id'")->result();
                                    foreach ($kelas as $row) {
                                        echo "<th>".$row->kelas_nama."</th>";
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <?php
                }
              }
              ?>
            </div><!-- /.row -->
            </form>
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <input type="hidden" id="dataset_x" value="">
    
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#tanggal').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            var tanggal = $('#tanggal').val();
            loadLog(tanggal);
            loadTable(tanggal);
        }
    );

    var winHeight = $(window).height();
    var docHeight = $(document).height();
    if (winHeight > docHeight){
        winHeight = winHeight - 200;
        $("#list-log").css({ 'height': winHeight + "px" });
    } else {
        docHeight = docHeight - 200;
        $("#list-log").css({ 'height': docHeight + "px" });

    }
  });
</script>
<script>
$(function () {
    var tanggal = $('#tanggal').val();
    loadLog(tanggal);
    loadTable(tanggal);
});

function loadLog(tanggal){
    $.ajax({
        url: "<?php echo module_url($this->uri->segment(2).'/dataload-table'); ?>/<?php echo $departemen_id; ?>/" + tanggal,
        dataType: 'json',
        // beforeSend: function() {
        //     $("#datagrid tbody").html("");
        // },
        success: function(data2){
            $("#datagrid tbody").html(data2);
        }
    });
}

function loadTable(tanggal){
    <?php
    foreach ($tingkat as $row_tingkat) {
        ?>
        $.ajax({
            url: "<?php echo module_url($this->uri->segment(2).'/get_kelas/'.$departemen_id.'/'.$row_tingkat->tingkat_id); ?>/" + tanggal,
            dataType: 'json',
            beforeSend: function() {
                $("#table-kelas<?php echo $row_tingkat->tingkat_id; ?> tbody").html("");
            },
            success: function(data3){
                html_kelas = data3.hadir;
                html_kelas += data3.belum;
                html_kelas += data3.total;
                $("#table-kelas<?php echo $row_tingkat->tingkat_id; ?> tbody").html(html_kelas);
            }
        });
        <?php
    }  
    ?>
}

$(function () {
    var synctime = <?php echo profile('presensi_synctime'); ?> * 1000;
    if (synctime >= 15000){
        <?php 
        if ($mesin){
            foreach ($mesin as $row) {
                ?>
                ajaxLoad(<?php echo $row->mesin_id; ?>);
                <?php
            }
        }
        ?>

        setInterval(function(){ 
            var tanggal = $('#tanggal').val();
            loadLog(tanggal);
            loadTable(tanggal);
        }, synctime);

        function ajaxLoad(mesin){
            setTimeout(function() {
                $.ajax({
                    url: "<?php echo module_url('log-auto-sinkronisasi/dataload_get'); ?>?mesin=" + mesin,
                    dataType: 'json',
                    type: 'POST',
                    success:
                    function(data){
                        // console.log(data);
                        if (data.response == true){
                            ajaxLoad(mesin);
                        } else if (data.response == false){
                            ajaxLoad(mesin);
                        }
                    },
                    complete: function(){
                    }
                });
            }, synctime);
        }
    }
});
</script>