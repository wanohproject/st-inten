<script src="<?php echo theme_dir('admin_v2/plugins/highcharts/code/highcharts.js');?>"></script>
<style>
.logpresensi {
    color: #FFFFFF;
}
.logpresensi .time span {
    font-size: 20px;
    float:left;
}
.logpresensi .time span.small {
    font-size: 14px;
    float:left;
    padding: 2px 0px 0px 3px;
}
.logpresensi h4 {
    padding: 5px 0px;
    margin: 0px;
}
.logpresensi h5 {
    padding: 0px;
    margin: 0px;
}
.logpresensi td,
.logpresensi th {
    border: 1px solid #fefefe !important;
    /* color: #000000 !important; */
}
.logpresensi th {
    border: 1px solid #fefefe !important;
    color: #000000 !important;
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            MONITORING PRESENSI SISWA
            <small><?php echo profile('profil_institusi'); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-dashboard"></i>Beranda</a></li>
            <li class="active">Monitoring Presensi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id);?>" method="post">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="departemen_id" class="control-label">Departemen</label>
                                    <div class="">
                                        <?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Akademi') AND departemen_utama IS NULL ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
                                    </div>		
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box -->
			</div><!-- /.col -->
            <div class="col-md-6">
                <div class="box box-info">                    
                    <div class="box-header">
                        <h3 class="box-title">
                            Presensi
                        </h3>
                        <div class="pull-right">
                            <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?php echo $tanggal; ?>" placeholder="" autocomplete="off">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body" id="list-log" style="height: 600px;position:relative;overflow-y:scroll">
                        <table id="datagrid" class="table table-bordered logpresensi">
                            <thead>
                                <tr bgcolor="#F9F9F9">
                                    <th style="text-align:center;" width="80">NIS</th>
                                    <th style="text-align:center;">NAMA</th>
                                    <th style="text-align:center;" width="80">KELAS</th>
                                    <th style="text-align:center;" width="120">MASUK</th>
                                    <th style="text-align:center;" width="120">PULANG</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.row -->
            <div class="col-md-6">
                <?php
                if ($tingkat){
                    foreach ($tingkat as $row_tingkat) {
                    ?>
                    <div class="box box-success">
                        <div class="box-header">
                        <h3 class="box-title">Kelas <?php echo $row_tingkat->tingkat_romawi; ?></h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                        <div id="container_<?php echo $row_tingkat->tingkat_id; ?>" style="height: 200px; margin: 0 auto"></div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <?php
                    }
                }
                ?>
            </div><!-- /.row -->
            </form>
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <input type="hidden" id="dataset_x" value="">
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show');
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>

    var winHeight = $(window).height();
    var docHeight = $(document).height();
    if (winHeight > docHeight){
        winHeight = winHeight - 200;
        $("#list-log").css({ 'height': winHeight + "px" });
    } else {
        docHeight = docHeight - 200;
        $("#list-log").css({ 'height': docHeight + "px" });

    }
  });
</script>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#tanggal').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            var tanggal = $('#tanggal').val();
            loadLog(tanggal);
            loadGrafik(tanggal);
        }
    );
  });
</script>
<script type="text/javascript">
var dataset;
$(document).ready(function() {
    // setInterval( function () {
    //     location.reload();
    // }, 3600000 );

    var tanggal = $('#tanggal').val();
    loadLog(tanggal);
    loadGrafik(tanggal);
});

function loadLog(tanggal){
    $.ajax({
        url: "<?php echo module_url('monitoring-siswa-table/dataload-table'); ?>/<?php echo $departemen_id; ?>/" + tanggal,
        dataType: 'json',
        // beforeSend: function() {
        //     $("#datagrid tbody").html("");
        // },
        success: function(data2){
            $("#datagrid tbody").html(data2);
        }
    });
}

function loadGrafik(tanggal){
    <?php
    foreach ($tingkat as $row) {
        ?>
        var chart_<?php echo $row->tingkat_id; ?>;
        var seriesData_<?php echo $row->tingkat_id; ?>;
        var xCategories_<?php echo $row->tingkat_id; ?>;

        $.ajax({
            url: "<?php echo module_url($this->uri->segment(2).'/get_siswa/'.$departemen_id.'/'.$tahun_kode.'/'.$semester_id.'/'.$row->tingkat_id); ?>/" + tanggal,
            dataType: 'json',
            async: false,
            success: function(data){
                dataset = data;
            }
        });

        seriesData_<?php echo $row->tingkat_id; ?> = [];
        xCategories_<?php echo $row->tingkat_id; ?> = [];
        var i, cat;
        if (dataset){
            for(i = 0; i < dataset.length; i++){
                cat = dataset[i].kelas;
                if(xCategories_<?php echo $row->tingkat_id; ?>.indexOf(cat) === -1){
                    xCategories_<?php echo $row->tingkat_id; ?>[xCategories_<?php echo $row->tingkat_id; ?>.length] = cat;
                }
            }
            for(i = 0; i < dataset.length; i++){
                if(seriesData_<?php echo $row->tingkat_id; ?>){
                    var currSeries = seriesData_<?php echo $row->tingkat_id; ?>.filter(function(seriesObject){ return seriesObject.name == dataset[i].status;});
                    if(currSeries.length === 0){
                        seriesData_<?php echo $row->tingkat_id; ?>[seriesData_<?php echo $row->tingkat_id; ?>.length] = currSeries = {name: dataset[i].status, data: [], color: dataset[i].color};
                    } else {
                        currSeries = currSeries[0];
                    }
                    var index = currSeries.data.length;
                    currSeries.data[index] = dataset[i].val;
                } else {
                    seriesData_<?php echo $row->tingkat_id; ?>[0] = {name: dataset[i].status, data: [dataset[i].val], color: dataset[i].color}
                }
            }
        }
        
        Highcharts.chart('container_<?php echo $row->tingkat_id; ?>', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: xCategories_<?php echo $row->tingkat_id; ?>
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: { 
                enabled: false
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                },
                series: {
                    animation: false
                }
            },
            series: seriesData_<?php echo $row->tingkat_id; ?>
        });
        <?php
    }
    ?>
}

$(function () {
    var synctime = <?php echo profile('presensi_synctime'); ?> * 1000;
    if (synctime >= 15000){
        <?php 
        if ($mesin){
            foreach ($mesin as $row) {
                ?>
                ajaxLoad(<?php echo $row->mesin_id; ?>);
                <?php
            }
        }
        ?>

        setInterval(function(){ 
            var tanggal = $('#tanggal').val();
            loadLog(tanggal);
            loadGrafik(tanggal);
        }, synctime);

        function ajaxLoad(mesin){
            setTimeout(function() {
                $.ajax({
                    url: "<?php echo module_url('log-auto-sinkronisasi/dataload_get'); ?>?mesin=" + mesin,
                    dataType: 'json',
                    type: 'POST',
                    success:
                    function(data){
                        // console.log(data);
                        if (data.response == true){
                            ajaxLoad(mesin);
                        } else if (data.response == false){
                            ajaxLoad(mesin);
                        }
                    },
                    complete: function(){
                    }
                });
            }, synctime);
        }
    }
});
</script>