<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log_sync_jibas extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Sync Database | ' . profile('profil_website');
		$this->active_menu		= 340;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Staf_model');
		$this->load->model('Mesin_model');
		$this->load->model('Libur_model');
		$this->load->model('Absensi_model');
		$this->load->model('Log_model');
		$this->load->model('Sidik_jari_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Presensi_model');
		$this->load->model('Jadwal_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		$DB2 = $this->load->database('jbssat', TRUE);
		$DB2->close();

		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;

		$where = "";
		if ($where){
			$where = " WHERE db_presensi.sat_log.tahun_kode='$tahun_kode' AND db_presensi.sat_log.semester_id='$semester_id' ";
		}
		
		$this->Datatable_model->set_table("(SELECT db_presensi.sat_log.*, sidik_jari_user, sidik_jari_nama FROM db_presensi.sat_log LEFT JOIN db_presensi.sat_sidik_jari sidik_jari ON db_presensi.sat_log.user_id=sidik_jari.user_id $where ORDER BY log_tanggal DESC LIMIT 100) log");
		$this->Datatable_model->set_column_order(array('log_tanggal', 'log.user_id', 'sidik_jari_user', 'sidik_jari_nama', 'log_mesin'));
		$this->Datatable_model->set_column_search(array('log_tanggal', 'log.user_id', 'sidik_jari_user', 'sidik_jari_nama', 'log_mesin'));
		$this->Datatable_model->set_order(array('log_tanggal', 'desc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['mesin_nama'] = $record->log_mesin;
            $row['user_id'] = $record->user_id;
            $row['sidik_jari_user'] = $record->sidik_jari_user;
            $row['sidik_jari_nama'] = $record->sidik_jari_nama;
            $row['log_tanggal'] = $record->log_tanggal;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$tahun_kode				= ($this->uri->segment(4))?$this->uri->segment(4):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(5))?$this->uri->segment(5):$semester_id;

		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;

		$data['presensi_tanggal_awal']		= ($this->input->post('presensi_tanggal_awal'))?$this->input->post('presensi_tanggal_awal'):date('Y-m-d');
		$data['presensi_tanggal_akhir']		= ($this->input->post('presensi_tanggal_akhir'))?$this->input->post('presensi_tanggal_akhir'):date('Y-m-d');

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/log_sync_jibas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function dataload(){
		$data = array();
		$mesin_id = $this->input->post('mesin');
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;

		if ($mesin_id){
			$mesin = $this->db->query("SELECT * FROM sat_mesin mesin WHERE mesin_id = '" . $mesin_id . "'")->row();
			if ($mesin){
				if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
					$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
					if($Connect){
						$soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
						$newLine="\r\n";
						fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						fputs($Connect, "Content-Type: text/xml".$newLine);
						fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						fputs($Connect, $soap_request.$newLine);
						$buffer="";
						while($Response=fgets($Connect, 1024)){
							$buffer=$buffer.$Response;
						}
					}
					$buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
					$buffer=explode("\r\n",$buffer);
					$jml_data = count($buffer) - 1;
					for($a=$mesin->mesin_sinkronisasi;$a<count($buffer);$a++){
						$data_=Parse_Data($buffer[$a],"<Row>","</Row>");
						$PIN=Parse_Data($data_,"<PIN>","</PIN>");
						$DateTime=Parse_Data($data_,"<DateTime>","</DateTime>");
						$Verified=Parse_Data($data_,"<Verified>","</Verified>");
						$Status=Parse_Data($data_,"<Status>","</Status>");
						if ($data_){
							$insert_log = array();
							$insert_log['log_id']			= $this->uuid->v4();
							$insert_log['tahun_kode']			= $tahun_kode;
							$insert_log['semester_id']		= $semester_id;
							$insert_log['mesin_id']			= $mesin->mesin_id;
							$insert_log['log_mesin']		= $mesin->mesin_nama;
							$insert_log['user_id']			= $PIN;
							$insert_log['log_tanggal']		= $DateTime;
							$insert_log['log_verifikasi']	= $Verified;
							$insert_log['log_status']		= $Status;
							$this->Log_model->insert_log($insert_log);

							$param = array();
							$param['tahun_kode'] = $tahun_kode;
							$param['semester_id'] = $semester_id;
							$param['user_id'] = $PIN;
							$param['datetime'] = $DateTime;
							$res = loadpresensiV2($param);
							
							$this->Log_model->update_log("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND user_id = '$PIN' AND log_tanggal = '$DateTime' AND log_res_code IS NULL AND log_res_message IS NULL", array('log_res_code'=>$res['status'], 'log_res_message'=>$res['message']));
						}
					}
					$this->Mesin_model->update_mesin(array('mesin_id'=>$mesin->mesin_id), array('mesin_sinkronisasi'=>$jml_data));
					
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $mesin;
				} else {
					$data['response']	= false;
					$data['message']	= "Mesin Off";
					$data['data']		= $mesin;
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Mesin tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function dataload_guru(){
		$data = array();
		$mesin_id = $this->input->post('mesin');
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;

		$where_mesin = "";
		if ($mesin_id){
			$where_mesin = "WHERE mesin_id = '$mesin_id' AND mesin_status = 'A' AND mesin_nama LIKE '%GURU%'";
		}

		$mesinList = $this->db->query("SELECT * FROM sat_mesin mesin $where_mesin")->result();
		if ($mesinList){
			foreach ($mesinList as $mesin) {
				if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
					$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
					if($Connect){
						$soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
						$newLine="\r\n";
						fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						fputs($Connect, "Content-Type: text/xml".$newLine);
						fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						fputs($Connect, $soap_request.$newLine);
						$buffer="";
						while($Response=fgets($Connect, 1024)){
							$buffer=$buffer.$Response;
						}
					}
					$buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
					$buffer=explode("\r\n",$buffer);
					$jml_data = count($buffer) - 1;
					for($a=$mesin->mesin_sinkronisasi;$a<count($buffer);$a++){
						$data_=Parse_Data($buffer[$a],"<Row>","</Row>");
						$PIN=Parse_Data($data_,"<PIN>","</PIN>");
						$DateTime=Parse_Data($data_,"<DateTime>","</DateTime>");
						$Verified=Parse_Data($data_,"<Verified>","</Verified>");
						$Status=Parse_Data($data_,"<Status>","</Status>");
						if ($data_){
							$insert_log = array();
							$insert_log['log_id']			= $this->uuid->v4();
							$insert_log['tahun_kode']			= $tahun_kode;
							$insert_log['semester_id']		= $semester_id;
							$insert_log['mesin_id']			= $mesin->mesin_id;
							$insert_log['log_mesin']		= $mesin->mesin_nama;
							$insert_log['user_id']			= $PIN;
							$insert_log['log_tanggal']		= $DateTime;
							$insert_log['log_verifikasi']	= $Verified;
							$insert_log['log_status']		= $Status;
							$this->Log_model->insert_log($insert_log);
	
							$param = array();
							$param['tahun_kode'] = $tahun_kode;
							$param['semester_id'] = $semester_id;
							$param['user_id'] = $PIN;
							$param['datetime'] = $DateTime;
							$res = loadpresensiV2($param);
							
							$this->Log_model->update_log("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND user_id = '$PIN' AND log_tanggal = '$DateTime' AND log_res_code IS NULL AND log_res_message IS NULL", array('log_res_code'=>$res['status'], 'log_res_message'=>$res['message']));
						}
					}
					$this->Mesin_model->update_mesin(array('mesin_id'=>$mesin->mesin_id), array('mesin_sinkronisasi'=>$jml_data));
					
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $mesin;
				} else {
					$data['response']	= false;
					$data['message']	= "Mesin Off";
					$data['data']		= $mesin;
				}
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Mesin tidak ada.";
		}
		echo json_encode($data);
	}

	public function dataload_get(){
		$data = array();
		$mesin_id = $this->input->get('mesin');
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;

		if ($mesin_id){
			$mesin = $this->db->query("SELECT * FROM sat_mesin mesin WHERE mesin_id = '" . $mesin_id . "'")->row();
			if ($mesin){
				if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
					$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
					if($Connect){
						$soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
						$newLine="\r\n";
						fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						fputs($Connect, "Content-Type: text/xml".$newLine);
						fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						fputs($Connect, $soap_request.$newLine);
						$buffer="";
						while($Response=fgets($Connect, 1024)){
							$buffer=$buffer.$Response;
						}
					}
					$buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
					$buffer=explode("\r\n",$buffer);
					$jml_data = count($buffer) - 1;
					for($a=$mesin->mesin_sinkronisasi;$a<count($buffer);$a++){
						$data_=Parse_Data($buffer[$a],"<Row>","</Row>");
						$PIN=Parse_Data($data_,"<PIN>","</PIN>");
						$DateTime=Parse_Data($data_,"<DateTime>","</DateTime>");
						$Verified=Parse_Data($data_,"<Verified>","</Verified>");
						$Status=Parse_Data($data_,"<Status>","</Status>");
						if ($data_){
							$insert_log = array();
							$insert_log['log_id']			= $this->uuid->v4();
							$insert_log['tahun_kode']			= $tahun_kode;
							$insert_log['semester_id']		= $semester_id;
							$insert_log['mesin_id']			= $mesin->mesin_id;
							$insert_log['log_mesin']		= $mesin->mesin_nama;
							$insert_log['user_id']			= $PIN;
							$insert_log['log_tanggal']		= $DateTime;
							$insert_log['log_verifikasi']	= $Verified;
							$insert_log['log_status']		= $Status;
							$this->Log_model->insert_log($insert_log);

							$param = array();
							$param['tahun_kode'] = $tahun_kode;
							$param['semester_id'] = $semester_id;
							$param['user_id'] = $PIN;
							$param['datetime'] = $DateTime;
							$res = loadpresensiV2($param);
							
							$this->Log_model->update_log("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND user_id = '$PIN' AND log_tanggal = '$DateTime' AND log_res_code IS NULL AND log_res_message IS NULL", array('log_res_code'=>$res['status'], 'log_res_message'=>$res['message']));
						}
					}
					$this->Mesin_model->update_mesin(array('mesin_id'=>$mesin->mesin_id), array('mesin_sinkronisasi'=>$jml_data));
					
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $mesin;
				} else {
					$data['response']	= false;
					$data['message']	= "Mesin Off";
					$data['data']		= $mesin;
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Mesin tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}
