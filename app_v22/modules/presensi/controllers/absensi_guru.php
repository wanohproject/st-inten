<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi_guru extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	public $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Absensi Staf | ' . profile('profil_website');
		$this->active_menu		= 347;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Departemen_model');
		$this->load->model('Staf_model');
		$this->load->model('Absensi_model');
		$this->load->model('Mesin_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		$departemen_id			= ($this->uri->segment(4))?$this->uri->segment(4):0;
		$where = " AND staf_status = 'Guru'";
		if ($departemen_id){
			$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
			$departemen_list_ = array();
			foreach ($departemen_list as $key => $value) {
				$departemen_list_[] = "	staf.departemen_id = '$value' ";
			}
			$departemen_list_ = implode(" OR ", $departemen_list_);
			$where .= " AND ($departemen_list_)";
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$tanggal_awal			= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$this->Datatable_model->set_table("(SELECT absensi.*, staf_nama, staf_user FROM sat_absensi absensi LEFT JOIN staf ON absensi.staf_id=staf.staf_id LEFT JOIN sat_sidik_jari sidik_jari ON absensi.staf_id=sidik_jari.staf_id WHERE absensi.tahun_kode = '$tahun_kode' AND absensi.semester_id = '$semester_id' AND DATE(absensi.absensi_tanggal) = '$tanggal_awal' $where) absensi");
		$this->Datatable_model->set_column_order(array('absensi_tanggal', 'absensi_tanggal', 'staf_user', 'staf_nama', 'absensi_status'));
		$this->Datatable_model->set_column_search(array('absensi_tanggal', 'absensi_tanggal', 'staf_user', 'staf_nama', 'absensi_status'));
		$this->Datatable_model->set_order(array('staf_user', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['absensi_hari'] = inday($record->absensi_tanggal);
            $row['absensi_tanggal'] = dateIndo($record->absensi_tanggal);
            $row['absensi_status'] = $record->absensi_status;
            $row['staf_user'] = $record->staf_user;
            $row['staf_nama'] = $record->staf_nama;
            $row['Actions'] = $this->get_buttons($record->absensi_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$absensi_tanggal_awal	= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$data['tahun_kode']				= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']			= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['absensi_tanggal_awal']	= ($this->input->post('absensi_tanggal_awal'))?$this->input->post('absensi_tanggal_awal'):$absensi_tanggal_awal;

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/absensi_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}

		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$absensi_tanggal_awal	= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):'';
        
		$data['absensi_tanggal_awal']	= ($this->input->post('absensi_tanggal_awal'))?$this->input->post('absensi_tanggal_awal'):$absensi_tanggal_awal;
		$data['absensi_tanggal_akhir']	= ($this->input->post('absensi_tanggal_akhir'))?$this->input->post('absensi_tanggal_akhir'):$absensi_tanggal_awal;
		$data['absensi_info']			= ($this->input->post('absensi_info'))?$this->input->post('absensi_info'):'';
		$data['absensi_deskripsi']		= ($this->input->post('absensi_deskripsi'))?$this->input->post('absensi_deskripsi'):'';
		$data['absensi_file']			= ($this->input->post('absensi_file'))?$this->input->post('absensi_file'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$staf	= $this->Staf_model->get_staf("staf.*", array("staf_id"=>$data['staf_id']));
			$staf_user = ($staf)?$staf->staf_user:null;
			
			$absensi_file = upload_file('absensi_file', './asset/uploads/presensi/absensi-file/');

			$absensi_tanggal_awal = strtotime($data['absensi_tanggal_awal']);
			$absensi_tanggal_akhir = strtotime($data['absensi_tanggal_akhir']);
			$datediff = $absensi_tanggal_akhir - $absensi_tanggal_awal;
			$days = round($datediff / (60 * 60 * 24)) + 1;

			for ($i=0; $i < $days; $i++) {
				if ($i == 0){
					$date = $data['absensi_tanggal_awal'];
				} else {
					$date = date('Y-m-d', strtotime($data['absensi_tanggal_awal'] . " + $i days"));
				}
				if ($this->Absensi_model->count_all_absensi(array('absensi_tanggal'=>$date, 'tahun_kode'=>$this->input->post('tahun_kode'), 'semester_id'=>$this->input->post('semester_id'), 'absensi.staf_id'=>$this->input->post('staf_id'))) < 1){
					$insert = array();
					$insert['absensi_id'] 			= $this->uuid->v4();
					$insert['tahun_kode'] 			= $this->input->post('tahun_kode');
					$insert['semester_id'] 			= $this->input->post('semester_id');
					$insert['staf_id'] 				= $this->input->post('staf_id');
					$insert['absensi_user'] 		= $staf_user;
					$insert['absensi_tanggal'] 		= $date;
					$insert['absensi_info'] 		= $this->input->post('absensi_info');
					$insert['absensi_deskripsi'] 	= ($this->input->post('absensi_deskripsi'))?$this->input->post('absensi_deskripsi'):null;
					if ($absensi_file){
						$insert['absensi_file'] 	= $absensi_file;
					}
					$insert['created_by'] 			= userdata('pengguna_id');
					$insert['created_at'] 			= date('Y-m-d H:i:s');
					$this->Absensi_model->insert_absensi($insert);
				}
			}
			
			$this->session->set_flashdata('success','Absensi Staf telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id'].'/'.$data['tahun_kode'].'/'.$data['semester_id'].'/'.$data['absensi_tanggal_awal']));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/absensi_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$absensi_id = validasi_sql($this->uri->segment(4));
		$where_delete['absensi_id']	= $absensi_id;
		$this->Absensi_model->delete_absensi($where_delete);

		$this->session->set_flashdata('success','Absensi Staf telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
}