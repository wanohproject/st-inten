<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Hari_libur extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	public $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Hari Libur | ' . profile('profil_website');
		$this->active_menu		= 354;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Staf_model');
		$this->load->model('presensi/Presensi_model');
		$this->load->model('presensi/Mesin_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('presensi/Libur_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		$tahun_kode				= ($this->uri->segment(4))?$this->uri->segment(4):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(5))?$this->uri->segment(5):$semester_id;
		
		$this->Datatable_model->set_table("(SELECT libur.* FROM sat_libur libur WHERE libur.tahun_kode = '$tahun_kode' AND libur.semester_id = '$semester_id') libur");
		$this->Datatable_model->set_column_order(array('libur_tanggal', 'libur_tanggal', 'libur_nama', 'libur_aktif'));
		$this->Datatable_model->set_column_search(array('libur_tanggal', 'libur_tanggal', 'libur_nama', 'libur_aktif'));
		$this->Datatable_model->set_order(array('libur_tanggal', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['libur_hari'] = inday($record->libur_tanggal);
            $row['libur_tanggal'] = dateIndo($record->libur_tanggal);
            $row['libur_aktif'] = ($record->libur_aktif == 'A')?'Aktif':'Tidak Aktif';
            $row['libur_nama'] = $record->libur_nama;
            $row['Actions'] = $this->get_buttons($record->libur_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$tahun_kode				= ($this->uri->segment(4))?$this->uri->segment(4):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(5))?$this->uri->segment(5):$semester_id;

		$data['tahun_kode']				= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']			= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/hari_libur', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$tahun_kode				= ($this->uri->segment(4))?$this->uri->segment(4):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(5))?$this->uri->segment(5):$semester_id;
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		
		$data['libur_tanggal']	= ($this->input->post('libur_tanggal'))?$this->input->post('libur_tanggal'):'';
		$data['libur_nama']		= ($this->input->post('libur_nama'))?$this->input->post('libur_nama'):'';
		$data['libur_aktif']	= ($this->input->post('libur_aktif'))?$this->input->post('libur_aktif'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$insert = array();
			$insert['tahun_kode'] 			= $this->input->post('tahun_kode');
			$insert['semester_id'] 			= $this->input->post('semester_id');
			$insert['libur_tanggal'] 		= $this->input->post('libur_tanggal');
			$insert['libur_aktif'] 		= $this->input->post('libur_aktif');
			$insert['libur_nama'] 			= $this->input->post('libur_nama');
			
			$this->Libur_model->insert_libur($insert);
			
			$this->session->set_flashdata('success','Hari Libur telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['tahun_kode'].'/'.$data['semester_id'].'/'.$data['libur_tanggal']));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/hari_libur', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$libur_id = validasi_sql($this->uri->segment(4));
		$where_delete['libur_id']	= $libur_id;
		$this->Libur_model->delete_libur($where_delete);

		$this->session->set_flashdata('success','Hari Libur telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
}