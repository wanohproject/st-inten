<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Jadwal | ' . profile('profil_website');
		$this->active_menu		= 327;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Departemen_model');
		$this->load->model('presensi/Jadwal_model');
		$this->load->model('presensi/Sidik_jari_model');
	}
	
	public function datatable()
    {
		$this->Datatable_model->set_table("(SELECT departemen.departemen_id, departemen.departemen_kode, departemen.departemen_nama, departemen.departemen_tipe, utama.departemen_nama AS departemen_parent FROM departemen LEFT JOIN departemen utama ON departemen.departemen_id=utama.departemen_id) departemen");
		$this->Datatable_model->set_column_order(array('departemen.departemen_kode', 'departemen.departemen_nama', 'departemen.departemen_tipe', 'departemen_parent', 'departemen_nama', 'departemen_nama'));
		$this->Datatable_model->set_column_search(array('departemen.departemen_kode', 'departemen.departemen_nama', 'departemen.departemen_tipe', 'departemen_parent', 'departemen_nama', 'departemen_nama'));
		$this->Datatable_model->set_order(array('departemen.departemen_kode', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
			$jadwal = $this->Jadwal_model->count_all_jadwal(array('jadwal.departemen_id'=>$record->departemen_id));
            $no++;
            $row = array();
            $row['departemen_kode'] = $record->departemen_kode;
            $row['departemen_nama'] = $record->departemen_nama;
            $row['departemen_tipe'] = $record->departemen_tipe;
            $row['departemen_parent'] = $record->departemen_parent;
			if ($jadwal){
				$row['jadwal_status'] = "<div class=\"text-center\"><button type=\"button\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-check\"></i></button></div>";
			} else {
				$row['jadwal_status'] = "<div class=\"text-center\"><button type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-remove\"></i></button></div>";
			}
            $row['Actions'] = $this->get_buttons($record->departemen_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i> Atur Jadwal</a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
        
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jadwal', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';

		$departemen_id = $this->uri->segment(4);
		$departemen	= $this->Departemen_model->get_departemen('*', "departemen_id = '$departemen_id'");
		$jadwal		= $this->Jadwal_model->get_jadwal('*', "jadwal.departemen_id = '$departemen_id'");
		
		$data['hari'] = array(1=>'Senin', 2=>'Selasa', 3=>'Rabu', 4=>'Kamis', 5=>'Jumat', 6=>'Sabtu', 7=>'Minggu');

		$jadwal_jenis = ($jadwal)?$jadwal->jadwal_jenis:1;
		$jadwal_shift_jumlah = ($jadwal)?$jadwal->jadwal_shift_jumlah:1;
		$jadwal_batas_minimal = ($jadwal)?$jadwal->jadwal_batas_minimal:0;
		
		$where_ref['departemen_id'] 	= $this->input->get('refId');
		$jadwal_ref = $this->Jadwal_model->get_jadwal("", $where_ref);

		if ($jadwal_ref){
			$jadwal_jenis = ($jadwal_ref)?$jadwal_ref->jadwal_jenis:$jadwal_jenis;
			$jadwal_shift_jumlah = ($jadwal_ref)?$jadwal_ref->jadwal_shift_jumlah:$jadwal_shift_jumlah;
			$jadwal_batas_minimal = ($jadwal_ref)?$jadwal_ref->jadwal_batas_minimal:$jadwal_batas_minimal;
		}
		
		$data['departemen_id'] = ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		$data['departemen_nama'] = $departemen->departemen_nama;
		$data['jadwal_jenis'] = ($this->input->post('jadwal_jenis'))?$this->input->post('jadwal_jenis'):$jadwal_jenis;
		$data['jadwal_shift_jumlah'] = ($this->input->post('jadwal_shift_jumlah'))?$this->input->post('jadwal_shift_jumlah'):$jadwal_shift_jumlah;
		$data['jadwal_batas_minimal'] = ($this->input->post('jadwal_batas_minimal'))?$this->input->post('jadwal_batas_minimal'):$jadwal_batas_minimal;
        
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$hari = $this->input->post('hari');
			$jadwal_masuk_awal = $this->input->post('jadwal_masuk_awal');
			$jadwal_masuk_tepat = $this->input->post('jadwal_masuk_tepat');
			$jadwal_masuk_akhir = $this->input->post('jadwal_masuk_akhir');
			$jadwal_pulang_awal = $this->input->post('jadwal_pulang_awal');
			$jadwal_pulang_tepat = $this->input->post('jadwal_pulang_tepat');
			$jadwal_pulang_akhir = $this->input->post('jadwal_pulang_akhir');
			if ($data['departemen_id']){
				$this->Jadwal_model->delete_jadwal(array('departemen_id'=>$data['departemen_id']));
				foreach ($data['hari'] as $key => $value) {
					for ($i=1; $i <= $data['jadwal_shift_jumlah']; $i++) { 
						if (array_key_exists($key, $hari)){
							if (array_key_exists($i, $hari[$key])){
								$where['jadwal_hari'] 			= $key;
								$where['jadwal_shift_ke'] 		= $i;
								$where['departemen_id'] 		= $data['departemen_id'];
								if ($this->Jadwal_model->count_all_jadwal($where) < 1){
									$insert = array();
									$insert['jadwal_shift_ke'] 			= $i;
									$insert['jadwal_hari'] 				= $key;
									$insert['jadwal_level'] 			= $data['departemen_id'];
									$insert['departemen_id'] 			= $data['departemen_id'];
									$insert['jadwal_jenis'] 			= $data['jadwal_jenis'];
									$insert['jadwal_shift_jumlah'] 		= $data['jadwal_shift_jumlah'];
									$insert['jadwal_batas_minimal'] 	= $data['jadwal_batas_minimal'];
									$insert['jadwal_masuk_awal'] 		= $jadwal_masuk_awal[$key][$i];
									$insert['jadwal_masuk_tepat'] 		= $jadwal_masuk_tepat[$key][$i];
									if ($data['jadwal_jenis'] == 3){
										$insert['jadwal_masuk_akhir'] 	= null;
										$insert['jadwal_pulang_awal'] 	= null;
									} else {
										$insert['jadwal_masuk_akhir'] 	= $jadwal_masuk_akhir[$key][$i];
										$insert['jadwal_pulang_awal'] 	= $jadwal_pulang_awal[$key][$i];
									}
									$insert['jadwal_pulang_akhir'] 		= $jadwal_pulang_akhir[$key][$i];
									$insert['jadwal_pulang_tepat'] 		= $jadwal_pulang_tepat[$key][$i];
									$this->Jadwal_model->insert_jadwal($insert);
								} else {
									$update = array();
									$update['jadwal_jenis'] 			= $data['jadwal_jenis'];
									$update['jadwal_shift_jumlah'] 		= $data['jadwal_shift_jumlah'];
									$update['jadwal_batas_minimal'] 	= $data['jadwal_batas_minimal'];
									$update['jadwal_masuk_awal'] 		= $jadwal_masuk_awal[$key][$i];
									$update['jadwal_masuk_tepat'] 		= $jadwal_masuk_tepat[$key][$i];
									if ($data['jadwal_jenis'] == 3){
										$update['jadwal_masuk_akhir'] 	= null;
										$update['jadwal_pulang_awal'] 	= null;
									} else {
										$update['jadwal_masuk_akhir'] 	= $jadwal_masuk_akhir[$key][$i];
										$update['jadwal_pulang_awal'] 	= $jadwal_pulang_awal[$key][$i];
									}
									$update['jadwal_pulang_akhir'] 		= $jadwal_pulang_akhir[$key][$i];
									$update['jadwal_pulang_tepat'] 		= $jadwal_pulang_tepat[$key][$i];
									$this->Jadwal_model->update_jadwal($where, $update);
								}
							}
						}
					}
				}			
				$this->session->set_flashdata('success','Jadwal telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','Jadwal gagal diubah.');
				redirect(module_url($this->uri->segment(2)));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jadwal', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
}
