<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_guru extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	public $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Rekap Presensi Staf | ' . profile('profil_website');
		$this->active_menu		= 352;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Staf_model');
		$this->load->model('Presensi_model');
		$this->load->model('Absensi_model');
		$this->load->model('Mesin_model');
		$this->load->model('Jadwal_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Libur_model');
		$this->load->model('Departemen_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		if (userdata('pengguna_level_id') == 10){
			$departemen_id			= ($this->uri->segment(4))?$this->uri->segment(4):userdata('departemen_id');
			$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
			$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
			$staf_id				= ($this->uri->segment(7))?$this->uri->segment(7):userdata('staf_id');
	
			$data['departemen_id']	= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):userdata('departemen_id');
			$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
			$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
			$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):userdata('staf_id');
			$data['show']			= ($this->input->post('show'))?$this->input->post('show'):'';
		} else {
			if (userdata('departemen_id')){
				$data['departemen_id']		= userdata('departemen_id');
			} else {
				$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
				$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
			}
			
			$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
			$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
			$staf_id				= ($this->uri->segment(7))?$this->uri->segment(7):'-';
	
			$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
			$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
			$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):$staf_id;
			$data['show']			= ($this->input->post('show'))?$this->input->post('show'):'';
		}
		

		$data['presensi_tanggal_awal']		= ($this->input->post('presensi_tanggal_awal'))?$this->input->post('presensi_tanggal_awal'):date('Y-m-d', strtotime(date("Y-m-d")." -1 months "));
		$data['presensi_tanggal_akhir']		= ($this->input->post('presensi_tanggal_akhir'))?$this->input->post('presensi_tanggal_akhir'):date('Y-m-d');

		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/rekap_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	
	public function cetak()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('pengguna_level_id') == 10){
			$data['departemen_id']				= userdata('departemen_id');
			$data['tahun_kode']					= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$this->tahun_kode;
			$data['semester_id']				= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):$semester_id;
			$data['staf_id']					= userdata('staf_id');
			$data['presensi_tanggal_awal']		= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):date('Y-m-d');
			$data['presensi_tanggal_akhir']		= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):date('Y-m-d');
		} else {
			$data['departemen_id']				= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):'-';
			$data['tahun_kode']					= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$this->tahun_kode;
			$data['semester_id']				= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):$semester_id;
			$data['staf_id']					= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
			$data['presensi_tanggal_awal']		= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):date('Y-m-d');
			$data['presensi_tanggal_akhir']		= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):date('Y-m-d');
		}
		
		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;

		$this->load->view(module_dir().'/export/staf_rekap_presensi_print', $data);
	}
	
	public function word()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']					= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']				= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$data['staf_id']					= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):'-';
		$data['presensi_tanggal_awal']		= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):date('Y-m-d');
		$data['presensi_tanggal_akhir']		= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):date('Y-m-d');
		
		$where_staf['staf_id']	= $data['staf_id'];
		$get_staf 				= $this->Staf_model->get_staf("", $where_staf);
		$data['staf']			= $get_staf;

		$staf_foto = ($data['staf'])?$data['staf']->staf_foto:'';
		$data['foto'] = ($staf_foto && file_exists('./asset/foto-staf/'.$staf_foto))?base_url('asset/foto-staf/'.$staf_foto):base_url('asset/profil/users.jpg');

		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;

		$sidik_jari	= $this->db->query("SELECT * FROM sat_sidik_jari sidik_jari LEFT JOIN staf ON sidik_jari.sidik_jari_user=staf.staf_user WHERE staf.staf_id = '".$data['staf_id']."'")->row();

		$data['sidik_jari_id'] = ($sidik_jari)?$sidik_jari->sidik_jari_id:'';
		$data['finger_id'] = ($sidik_jari)?$sidik_jari->finger_id:'';
		$data['mesin_id'] = ($sidik_jari)?$sidik_jari->mesin_id:'';
		$data['user_id'] = ($sidik_jari)?$sidik_jari->user_id:'';
		$data['sidik_jari_user'] = ($sidik_jari)?$sidik_jari->sidik_jari_user:'';
		$data['sidik_jari_nama'] = ($sidik_jari)?$sidik_jari->sidik_jari_nama:'';
		$data['sidik_jari_level'] = ($sidik_jari)?$sidik_jari->sidik_jari_level:'Guru';
		$data['sidik_jari_sms'] = ($sidik_jari)?$sidik_jari->sidik_jari_sms:'N';
		$data['sidik_jari_tanggal'] = ($sidik_jari)?$sidik_jari->sidik_jari_tanggal:'';
		$data['staf_id'] = ($sidik_jari)?$sidik_jari->staf_id:'';
		$data['staf_user'] = ($sidik_jari)?$sidik_jari->staf_user:'';
		
		$data['nama_sekolah']	= profile('profil_institusi');
		$data['alamat_sekolah']	= profile('profil_kontak');
		
		$data['kepala_sekolah']	= $this->Staf_model->get_staf('*', array('staf_id'=>profile('kepala_sekolah')));
		$data['ttd_tu']			= $this->Staf_model->get_staf('*', array('staf_id'=>profile('ttd_tu')));
		$data['ttd_guru']		= $this->Staf_model->get_staf('*', array('staf_id'=>profile('ttd_guru')));
		$this->load->view(module_dir().'/export/staf_rekap_presensi_word', $data);
	}

	public function word_all()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']					= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']				= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$data['presensi_tanggal_awal']		= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):date('Y-m-d');
		$data['presensi_tanggal_akhir']		= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):date('Y-m-d');
		
		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;
		
		$data['nama_sekolah']	= profile('profil_institusi');
		$data['alamat_sekolah']	= profile('profil_kontak');

		if ($this->uri->segment(8) == 'tu'){
			$data['staf_status']	= 'Tendik';
		} else {
			$data['staf_status']	= 'Guru';
		}
	
		$data['kepala_sekolah']	= $this->Staf_model->get_staf('*', array('staf_id'=>profile('kepala_sekolah')));
		$data['ttd_tu']			= $this->Staf_model->get_staf('*', array('staf_id'=>profile('ttd_tu')));
		$data['ttd_guru']		= $this->Staf_model->get_staf('*', array('staf_id'=>profile('ttd_guru')));
		$this->load->view(module_dir().'/export/staf_rekap_presensi_word_all', $data);
	}

	public function excel_all()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']					= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']				= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$data['presensi_tanggal_awal']		= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):date('Y-m-d');
		$data['presensi_tanggal_akhir']		= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):date('Y-m-d');
		
		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;
		
		$data['nama_sekolah']	= profile('profil_institusi');
		$data['alamat_sekolah']	= profile('profil_kontak');

		if ($this->uri->segment(8) == 'tu'){
			$data['staf_status']	= 'Tenaga Kependidikan';
		} else {
			$data['staf_status']	= 'Guru';
		}

		$data['sidik_jari_level'] = 'Guru';
		$data['excel']			= true;
	
		$data['kepala_sekolah']	= $this->Staf_model->get_staf('*', array('staf_id'=>profile('kepala_sekolah')));
		$data['ttd_tu']			= $this->Staf_model->get_staf('*', array('staf_id'=>profile('ttd_tu')));
		$data['ttd_guru']		= $this->Staf_model->get_staf('*', array('staf_id'=>profile('ttd_guru')));
		$this->load->view(module_dir().'/export/staf_rekap_presensi_excel_all', $data);
	}
}
