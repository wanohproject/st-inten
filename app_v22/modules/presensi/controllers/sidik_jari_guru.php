<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sidik_jari_guru extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Sidik Jari | ' . profile('profil_website');
		$this->active_menu		= 334;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Departemen_model');
		$this->load->model('Staf_model');
		$this->load->model('Sidik_jari_model');
		$this->load->model('Sidik_mesin_model');
		$this->load->model('Sidik_template_model');
		$this->load->model('Sidik_kartu_model');
		$this->load->model('Mesin_model');
    }
	
	public function datatable()
    {
		$departemen_id 	= validasi_sql($this->uri->segment(4));
		$where = " staf_status = 'Guru'";
		if ($departemen_id){
			$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
			$departemen_list_ = array();
			foreach ($departemen_list as $key => $value) {
				$departemen_list_[] = "	departemen.departemen_id = '$value' ";
			}
			$departemen_list_ = implode(" OR ", $departemen_list_);
			$where .= " AND ($departemen_list_)";
		}
		
		$this->Datatable_model->set_table("(SELECT staf.*, departemen.departemen_kode, departemen.departemen_nama FROM staf LEFT JOIN departemen ON staf.departemen_id=departemen.departemen_id WHERE $where AND (staf_tst = '0000-00-00' OR staf_tst >= '".date('Y-m-d')."' OR staf_tst IS NULL)) staf");
		$this->Datatable_model->set_column_order(array('staf_nama', 'staf_user', 'staf_user', 'staf_user'));
		$this->Datatable_model->set_column_search(array('staf_nama', 'staf_user', 'staf_user', 'staf_user'));
		$this->Datatable_model->set_order(array('staf_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
			$sidik_jari	= $this->Sidik_jari_model->get_sidik_jari('*', array('sidik_jari.staf_id'=>$record->staf_id));
        
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['staf_id'] = $record->staf_id;
            $row['staf_nama'] = $record->staf_nama;
            $row['departemen_kode'] = $record->departemen_kode;
            $row['departemen_nama'] = $record->departemen_nama;
            $row['staf_user'] = $record->staf_user;
            $row['user_id'] = ($sidik_jari)?$sidik_jari->user_id:'';
            $row['Actions'] = $this->get_buttons($record->staf_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Info Sidik Jari"><i class="fa fa-info-circle"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Hapus Sidik Jari" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus sidik jari ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/sidik_jari_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
        
        $where_staf['staf.staf_id']	= validasi_sql($this->uri->segment(4)); 
		$staf 						= $this->Staf_model->get_staf('*', $where_staf);
		if (!$staf){
			redirect(module_url($this->uri->segment(2).''));
		}
		$sidik_jari					= $this->Sidik_jari_model->get_sidik_jari('*', $where_staf);
		$data['sidik_jari'] 		= $sidik_jari;

		$data['sidik_jari_id'] 		= ($sidik_jari)?$sidik_jari->sidik_jari_id:$this->uuid->v4();
		$data['staf_id'] 			= ($sidik_jari)?$sidik_jari->staf_id:$staf->staf_id;
		$data['user_id'] 			= ($sidik_jari)?$sidik_jari->user_id:$staf->staf_user;
		$data['sidik_jari_user'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_user:$staf->staf_user;
		$data['sidik_jari_nama'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_nama:$staf->staf_nama;
		$data['sidik_jari_card'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_card:'';
		$data['sidik_jari_level'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_level:$staf->departemen_id;
		$data['sidik_jari_sms'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_sms:'N';
		
		$save	= $this->input->post('save');
		$reg	= $this->input->post('reg');
		$del	= $this->input->post('del');
		if ($save == 'save' || $reg == 'reg'){
			if ($sidik_jari){
				$where_update['staf_id']	= validasi_sql($this->input->post('staf_id'));
				$update = array();
				$update['user_id'] 			= $this->input->post('user_id');
				$update['sidik_jari_nama'] 	= $this->input->post('sidik_jari_nama');
				$update['sidik_jari_level'] = $this->input->post('sidik_jari_level');
				$update['sidik_jari_sms'] 	= "N";
				$update['updated_by']		= userdata('pengguna_id');
				$update['updated_at']		= date('Y-m-d H:i:s');
				$this->Sidik_jari_model->update_sidik_jari($where_update, $update);

				$mesin = $this->input->post('mesin');
				$this->Sidik_mesin_model->delete_sidik_mesin(array('staf_id'=>$this->input->post('staf_id')));
				if ($mesin) {
					foreach ($mesin as $key => $value) {
						$insert_mesin = array();
						$insert_mesin['sidik_mesin_id'] 	= $this->uuid->v4();
						$insert_mesin['sidik_jari_id'] 		= $data['sidik_jari_id'];
						$insert_mesin['user_id'] 			= $this->input->post('user_id');
						$insert_mesin['mesin_id'] 			= $value;
						$insert_mesin['staf_id'] 			= $this->input->post('staf_id');
						$insert_mesin['sidik_jari_user']	= $this->input->post('sidik_jari_user');
						$insert_mesin['created_by']			= userdata('pengguna_id');
						$insert_mesin['created_at']			= date('Y-m-d H:i:s');
						$this->Sidik_mesin_model->insert_sidik_mesin($insert_mesin);
					}
				}
				
				$this->session->set_flashdata('success','Sidik Jari telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2).'/edit/'.$data['staf_id']));
			} else {
				$insert = array();
				$insert['sidik_jari_id'] 	= $data['sidik_jari_id'];
				$insert['user_id'] 			= $this->input->post('user_id');
				$insert['staf_id'] 			= $this->input->post('staf_id');
				$insert['sidik_jari_user'] 	= $this->input->post('sidik_jari_user');
				$insert['sidik_jari_nama'] 	= $this->input->post('sidik_jari_nama');
				$insert['sidik_jari_level']	= $this->input->post('sidik_jari_level');
				$insert['sidik_jari_sms']	= "N";
				$insert['created_by']		= userdata('pengguna_id');
				$insert['created_at']		= date('Y-m-d H:i:s');
				$this->Sidik_jari_model->insert_sidik_jari($insert);

				$mesin = $this->input->post('mesin');
				$this->Sidik_mesin_model->delete_sidik_mesin(array('staf_id'=>$this->input->post('staf_id')));
				if ($mesin) {
					foreach ($mesin as $key => $value) {
						$insert_mesin = array();
						$insert_mesin['sidik_mesin_id'] 	= $this->uuid->v4();
						$insert_mesin['sidik_jari_id'] 		= $data['sidik_jari_id'];
						$insert_mesin['user_id'] 			= $this->input->post('user_id');
						$insert_mesin['mesin_id'] 			= $value;
						$insert_mesin['staf_id'] 			= $this->input->post('staf_id');
						$insert_mesin['sidik_jari_user'] 	= $this->input->post('sidik_jari_user');
						$insert_mesin['created_by']			= userdata('pengguna_id');
						$insert_mesin['created_at']			= date('Y-m-d H:i:s');
						$this->Sidik_mesin_model->insert_sidik_mesin($insert_mesin);
					}
				}
				
				$this->session->set_flashdata('success','Sidik jari telah berhasil ditambah.');
				redirect(module_url($this->uri->segment(2).'/edit/'.$data['staf_id']));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/sidik_jari_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add_card()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add_card';
        
        $where_staf['staf.staf_id']	= validasi_sql($this->uri->segment(4)); 
		$staf 						= $this->Staf_model->get_staf('*', $where_staf);
		if (!$staf){
			redirect(module_url($this->uri->segment(2).''));
		}
		$sidik_jari					= $this->Sidik_jari_model->get_sidik_jari('*', $where_staf);
		if (!$sidik_jari){
			redirect(module_url($this->uri->segment(2).'/edit/'.$staf->staf_id));
		}
		$data['sidik_jari'] 		= $sidik_jari;
        
		$data['sidik_jari_id'] 		= ($sidik_jari)?$sidik_jari->sidik_jari_id:'';
		$data['staf_id'] 			= ($sidik_jari)?$sidik_jari->staf_id:$staf->staf_id;
		$data['user_id'] 			= ($sidik_jari)?$sidik_jari->user_id:$staf->staf_user;
		$data['sidik_jari_user'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_user:$staf->staf_user;
		$data['sidik_jari_nama'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_nama:$staf->staf_nama;
		$data['sidik_jari_level'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_level:$staf->departemen_id;
		$data['sidik_jari_sms'] 	= ($sidik_jari)?$sidik_jari->sidik_jari_sms:'N';

		$data['sidik_kartu_nama'] = ($this->input->post('sidik_kartu_nama'))?$this->input->post('sidik_kartu_nama'):'';
		$data['sidik_kartu_nomor'] = ($this->input->post('sidik_kartu_nomor'))?$this->input->post('sidik_kartu_nomor'):'';
		$data['sidik_kartu_utama'] = ($this->input->post('sidik_kartu_utama'))?$this->input->post('sidik_kartu_utama'):'Y';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($sidik_jari && $this->Sidik_kartu_model->count_all_sidik_kartu(array('sidik_kartu_nomor'=>validasi_sql($this->input->post('sidik_kartu_nomor')))) < 1){

				if ($this->input->post('sidik_kartu_utama') == 'Y'){
					$this->db->query("UPDATE sat_sidik_kartu SET sidik_kartu_utama = 'N' WHERE staf_id = '".$this->input->post('staf_id')."'");

					$where_sidik_jari['staf_id']	= $this->input->post('staf_id');
					$this->Sidik_jari_model->update_sidik_jari($where_sidik_jari, array('sidik_jari_card'=>validasi_sql($this->input->post('sidik_kartu_nomor'))));
				}
				
				$insert = array();
				$insert['sidik_kartu_id'] 		= $this->uuid->v4();
				$insert['sidik_jari_id'] 		= $data['sidik_jari_id'];
				$insert['user_id'] 				= $this->input->post('user_id');
				$insert['staf_id'] 				= $this->input->post('staf_id');
				$insert['sidik_jari_user'] 		= $this->input->post('sidik_jari_user');
				$insert['sidik_kartu_nama'] 	= validasi_sql($this->input->post('sidik_kartu_nama'));
				$insert['sidik_kartu_nomor'] 	= validasi_sql($this->input->post('sidik_kartu_nomor'));
				$insert['sidik_kartu_utama'] 	= validasi_sql($this->input->post('sidik_kartu_utama'));
				$insert['sidik_kartu_valid'] 	= 1;
				$sidik_kartu_file = base64_encode(validasi_sql($this->input->post('sidik_kartu_nomor')));
				$insert['sidik_kartu_file'] 	= $sidik_kartu_file;
				$insert['sidik_kartu_ukuran'] 	= strlen($sidik_kartu_file);
				$insert['created_by']			= userdata('pengguna_id');
				$insert['created_at']			= date('Y-m-d H:i:s');
				$this->Sidik_kartu_model->insert_sidik_kartu($insert);

				$this->session->set_flashdata('success','Kartu telah berhasil ditambah.');
				redirect(module_url($this->uri->segment(2).'/edit/'.$data['staf_id']));
			} else {
				$this->session->set_flashdata('success','Kartu sudah digunakan.');
				redirect(module_url($this->uri->segment(2).'/edit/'.$data['staf_id']));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/sidik_jari_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$staf_id = validasi_sql($this->uri->segment(4));
		$where_delete['staf_id']	= $staf_id;
		$this->Sidik_jari_model->delete_sidik_jari($where_delete);
		$this->Sidik_mesin_model->delete_sidik_mesin($where_delete);
		$this->Sidik_kartu_model->delete_sidik_kartu($where_delete);
		$this->Sidik_template_model->delete_sidik_template($where_delete);

		$this->session->set_flashdata('success','Sidik Jari telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}

	public function delete_card()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$staf_id = validasi_sql($this->uri->segment(4));
		$sidik_kartu_id = validasi_sql($this->uri->segment(5));

		$sidik_kartu = $this->Sidik_kartu_model->get_sidik_kartu("", array('sidik_kartu_id'=>$sidik_kartu_id));
		if ($sidik_kartu){
			if ($this->Sidik_jari_model->count_all_sidik_jari(array('sidik_jari.staf_id'=>$staf_id, 'sidik_jari_card'=>$sidik_kartu->sidik_kartu_nomor)) > 0){
				$where_sidik_jari['staf_id']	= $staf_id;
				$this->Sidik_jari_model->update_sidik_jari($where_sidik_jari, array('sidik_jari_card'=>null));
			}
			$where_delete['sidik_kartu_id']	= $sidik_kartu_id;
			$this->Sidik_kartu_model->delete_sidik_kartu($where_delete);
	
			$this->session->set_flashdata('success','Kartu telah berhasil dihapus.');
			redirect(module_url($this->uri->segment(2).'/edit/'.$staf_id));
		} else {
			$this->session->set_flashdata('errir','Kartu gagal dihapus.');
			redirect(module_url($this->uri->segment(2).'/edit/'.$staf_id));
		}
	}

	public function main_card()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$staf_id = validasi_sql($this->uri->segment(4));
		$sidik_kartu_id = validasi_sql($this->uri->segment(5));

		$sidik_kartu = $this->Sidik_kartu_model->get_sidik_kartu("", array('sidik_kartu_id'=>$sidik_kartu_id));
		if ($sidik_kartu){
			$where_set['staf_id']	= $staf_id;
			$this->Sidik_kartu_model->update_sidik_kartu($where_set, array('sidik_kartu_utama'=>'N'));
	
			$where_sidik_jari['staf_id']	= $staf_id;
			$this->Sidik_jari_model->update_sidik_jari($where_sidik_jari, array('sidik_jari_card'=>$sidik_kartu->sidik_kartu_nomor));
			
			$where_edit['sidik_kartu_id']	= $sidik_kartu_id;
			$edit['sidik_kartu_utama']	= 'Y';
			$this->Sidik_kartu_model->update_sidik_kartu($where_edit, $edit);
			
			$this->session->set_flashdata('success','Kartu telah berhasil diatur sebagai kartu utama.');
			redirect(module_url($this->uri->segment(2).'/edit/'.$staf_id));
		} else {
			$this->session->set_flashdata('errir','Kartu gagal diatur sebagai kartu utama.');
			redirect(module_url($this->uri->segment(2).'/edit/'.$staf_id));
		}
	}

	public function upload()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'upload';
        
		$data['departemen_id'] = ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):'';
		$data['mesin_id'] = ($this->input->post('mesin_id'))?$this->input->post('mesin_id'):'';
		$data['sidik_jari'] = ($this->input->post('sidik_jari'))?$this->input->post('sidik_jari'):'N';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$mesin = $this->db->query("SELECT * FROM sat_mesin mesin WHERE mesin_id = '" . $data['mesin_id'] . "'")->row();
			if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
				$grid = $this->db->query("SELECT sidik_jari.* FROM sat_sidik_mesin sidik_mesin 
											LEFT JOIN sat_sidik_jari sidik_jari ON sidik_mesin.sidik_jari_id=sidik_jari.sidik_jari_id
											WHERE sidik_mesin.mesin_id = '" . $data['mesin_id'] . "' AND sidik_jari.staf_id IS NOT NULL AND sidik_jari.siswa_id IS NULL GROUP BY sidik_jari.staf_id, sidik_mesin.mesin_id")->result();
				if ($grid){
					foreach ($grid as $row) {
						if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
							$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
							if($Connect){
								$card_upload = "<Card>".$row->sidik_jari_card."</Card>";
								$soap_request="<SetUserInfo><ArgComKey Xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN>".$row->user_id."</PIN><Name>".$row->sidik_jari_nama."</Name>$card_upload</Arg></SetUserInfo>";
								$newLine="\r\n";
								fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
								fputs($Connect, "Content-Type: text/xml".$newLine);
								fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
								fputs($Connect, $soap_request.$newLine);
								$buffer="";
								while($Response=fgets($Connect, 1024)){
									$buffer=$buffer.$Response;
								}
								fclose($Connect);

								$grid_jari= $this->db->query("SELECT sidik_template.* FROM sat_sidik_template sidik_template 
													LEFT JOIN sat_sidik_jari sidik_jari ON sidik_template.sidik_jari_id=sidik_jari.sidik_jari_id
											WHERE sidik_template.staf_id = '$row->staf_id' AND sidik_template.sidik_template_ukuran > 0 AND sidik_template.sidik_template_file IS NOT NULL AND sidik_jari.staf_id IS NOT NULL AND sidik_jari.siswa_id IS NULL GROUP BY sidik_template.finger_id")->result();

								if ($data['sidik_jari'] == 'Y' && $grid_jari){
									foreach ($grid_jari as $row_jari) {
										$Connect_jari = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
										if($Connect_jari){
											$soap_request="<SetUserTemplate><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$row->user_id."</PIN><FingerID xsi:type=\"xsd:integer\">".$row_jari->finger_id."</FingerID><Size>".$row_jari->sidik_template_ukuran."</Size><Valid>1</Valid><Template>".$row_jari->sidik_template_file."</Template></Arg></SetUserTemplate>";
											$newLine="\r\n";
											fputs($Connect_jari, "POST /iWsService HTTP/1.0".$newLine);
											fputs($Connect_jari, "Content-Type: text/xml".$newLine);
											fputs($Connect_jari, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
											fputs($Connect_jari, $soap_request.$newLine);
											$buffer="";
											while($Response=fgets($Connect_jari, 1024)){
												$buffer=$buffer.$Response;
											}
											fclose($Connect_jari);
										}
									}
								}
							}
						} else {
							break;
						}
					}	
				}
				$this->session->set_flashdata('success','Sidik Jari telah berhasil diupload.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','Sidik Jari gagal diupload.');
				redirect(module_url($this->uri->segment(2)));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/sidik_jari_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function download()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'download';
        
		$data['departemen_id'] = ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):'';
		$data['mesin_id'] = ($this->input->get('mesin'))?$this->input->get('mesin'):'';
		
		if ($data['mesin_id']){
			$mesin = $this->db->query("SELECT * FROM sat_mesin mesin WHERE mesin_id = '" . $data['mesin_id'] . "'")->row();
			if ($mesin){
				
				if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
					$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
					if($Connect){
						$soap_request="<GetUserTemplate><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN><FingerID xsi:type=\"xsd:integer\">All</FingerID></Arg></GetUserTemplate>";
						$newLine="\r\n";
						fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						fputs($Connect, "Content-Type: text/xml".$newLine);
						fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						fputs($Connect, $soap_request.$newLine);
						$buffer="";
						while($Response=fgets($Connect, 1024)){
							$buffer=$buffer.$Response;
						}
					}

					$buffer=Parse_Data($buffer,"<GetUserTemplateResponse>","</GetUserTemplateResponse>");
					$buffer=explode("\r\n",$buffer);

					$jml_jari = 0;
					for($a=0;$a<count($buffer);$a++){
						$data_=Parse_Data($buffer[$a],"<Row>","</Row>");
						if ($data_){
							$PIN=Parse_Data($data_,"<PIN>","</PIN>");
							$FingerID=Parse_Data($data_,"<FingerID>","</FingerID>");
							$Size=Parse_Data($data_,"<Size>","</Size>");
							$Valid=Parse_Data($data_,"<Valid>","</Valid>");
							$Template=Parse_Data($data_,"<Template>","</Template>");

							$jml_jari++;

							$sidik_jari = $this->Sidik_jari_model->get_sidik_jari('*', array('sidik_jari.user_id'=>$PIN));
							if ($sidik_jari && $this->Sidik_template_model->count_all_sidik_template("sidik_template.user_id = '$PIN' AND sidik_template.finger_id='$FingerID'") > 0){
								$sidik_template = $this->Sidik_template_model->get_sidik_template('*', "sidik_template.user_id = '$PIN' AND sidik_template.finger_id='$FingerID'");
								$update = array();
								$update['mesin_id'] 				= $mesin->mesin_id;
								$update['sidik_template_ukuran'] 	= $Size;
								$update['sidik_template_valid'] 	= $Valid;
								$update['sidik_template_file'] 		= $Template;
								$update['updated_by']				= userdata('pengguna_id');
								$update['updated_at']				= date('Y-m-d H:i:s');
								$this->Sidik_template_model->update_sidik_template(array('sidik_template_id'=>$sidik_template->sidik_template_id), $update);
							} else if ($sidik_jari && $this->Sidik_template_model->count_all_sidik_template("sidik_template.user_id = '$PIN' AND sidik_template.finger_id='$FingerID'") < 1){
								$insert = array();
								$insert['sidik_template_id'] 		= $this->uuid->v4();
								$insert['mesin_id'] 				= $mesin->mesin_id;
								$insert['user_id'] 					= $PIN;
								$insert['finger_id'] 				= $FingerID;
								$insert['staf_id'] 					= $sidik_jari->staf_id;
								$insert['sidik_jari_id'] 			= $sidik_jari->sidik_jari_id;
								$insert['sidik_jari_user'] 			= $sidik_jari->sidik_jari_user;
								$insert['sidik_template_ukuran'] 	= $Size;
								$insert['sidik_template_valid'] 	= $Valid;
								$insert['sidik_template_file'] 		= $Template;
								$insert['created_by']				= userdata('pengguna_id');
								$insert['created_at']				= date('Y-m-d H:i:s');
								$this->Sidik_template_model->insert_sidik_template($insert);
							}
						}
					}
					$this->session->set_flashdata('success','Sidik Jari telah berhasil didownload.<br /><strong>'.$jml_jari.' Jari</strong>');
					redirect(module_url($this->uri->segment(2).'/'.$this->uri->segment(3)));
				} else {
					$this->session->set_flashdata('error','Sidik Jari gagal didownload.');
					redirect(module_url($this->uri->segment(2).'/'.$this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Mesin tidak ditemukan');
				redirect(module_url($this->uri->segment(2).'/'.$this->uri->segment(3)));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/sidik_jari_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
	
	public function generate()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'generate';
        
		$data['departemen_id'] = ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):'';
		$mesin = ($this->input->post('mesin'))?$this->input->post('mesin'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where = "WHERE staf_user != '' AND (staf_tst = '0000-00-00' OR staf_tst >= '".date('Y-m-d')."' OR staf_tst IS NULL)";
			if ($this->input->post('departemen_id')){
				$departemen_list = $this->Departemen_model->recursive_departemen_child($this->input->post('departemen_id'));
				$departemen_list = implode(", ", $departemen_list);
				$where .= " AND staf.departemen_id IN ($departemen_list)";
			}
			$grid = $this->db->query("SELECT * FROM staf $where")->result();
			if ($grid){
				foreach ($grid as $row) {
					if ($row->staf_user && is_numeric($row->staf_user)) {
						$sidik_jari = $this->Sidik_jari_model->get_sidik_jari('*', array('sidik_jari.staf_id'=>$row->staf_id));
						if (!$sidik_jari){
							$insert['sidik_jari_id'] 	= $this->uuid->v4();
							$insert['user_id'] 			= $row->staf_user;
							$insert['staf_id']			= $row->staf_id;
							$insert['sidik_jari_user'] 	= $row->staf_user;
							$insert['sidik_jari_nama'] 	= $row->staf_nama;
							$insert['sidik_jari_level']	= $row->departemen_id;
							$insert['sidik_jari_sms'] 	= 'N';
							$insert['created_by']		= userdata('pengguna_id');
							$insert['created_at']		= date('Y-m-d H:i:s');
							$this->Sidik_jari_model->insert_sidik_jari($insert);
							
							$sidik_jari = $this->Sidik_jari_model->get_sidik_jari('*', array('sidik_jari.staf_id'=>$row->staf_id));
						}

						if ($sidik_jari){
							$this->Sidik_mesin_model->delete_sidik_mesin(array('staf_id'=>$row->staf_id));
							if ($mesin){
								foreach ($mesin as $key => $value) {
									$insert_mesin = array();
									$insert_mesin['sidik_mesin_id'] 	= $this->uuid->v4();
									$insert_mesin['sidik_jari_id'] 		= $sidik_jari->sidik_jari_id;
									$insert_mesin['user_id'] 			= $row->staf_user;
									$insert_mesin['mesin_id'] 			= $value;
									$insert_mesin['staf_id'] 			= $row->staf_id;
									$insert_mesin['sidik_jari_user']	= $row->staf_user;
									$insert_mesin['created_by']			= userdata('pengguna_id');
									$insert_mesin['created_at']			= date('Y-m-d H:i:s');
									$this->Sidik_mesin_model->insert_sidik_mesin($insert_mesin);
								}
							}
						}
					}
				}	
			}
			$this->session->set_flashdata('success','Sidik Jari telah berhasil di-generate.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/sidik_jari_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
}
