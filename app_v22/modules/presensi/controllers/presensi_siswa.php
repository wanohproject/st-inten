<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi_siswa extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Presensi Siswa | ' . profile('profil_website');
		$this->active_menu		= 337;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Staf_model');
		$this->load->model('Departemen_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Siswa_kelas_model');
		$this->load->model('Siswa_model');
		$this->load->model('Kelas_model');
		$this->load->model('Presensi_model');
		$this->load->model('Mesin_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		$where = "";
		$departemen_id 	= ($this->uri->segment(4))?$this->uri->segment(4):0;
		if ($departemen_id){
			$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
			$departemen_list_ = array();
			foreach ($departemen_list as $key => $value) {
				$departemen_list_[] = "	siswa.departemen_id = '$value' ";
			}
			$departemen_list_ = implode(" OR ", $departemen_list_);
			$where .= " AND ($departemen_list_)";
		}

		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$tingkat_id				= ($this->uri->segment(7))?$this->uri->segment(7):'-';
		$kelas_id				= ($this->uri->segment(8))?$this->uri->segment(8):'-';
		$tanggal_awal			= ($this->uri->segment(9))?$this->uri->segment(9):date('Y-m-d');
		
		$this->Datatable_model->set_table("(SELECT siswa.siswa_id, siswa_nama, siswa_nis, siswa_user, presensi.presensi_tanggal_masuk, presensi.presensi_tanggal_keluar FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id LEFT JOIN (SELECT * FROM sat_presensi WHERE DATE(sat_presensi.presensi_tanggal_masuk) = '$tanggal_awal') presensi ON siswa.siswa_user=presensi.presensi_user LEFT JOIN kelas ON siswa_kelas.kelas_id=kelas.kelas_id WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.kelas_id = '$kelas_id' $where) siswa_kelas");
		$this->Datatable_model->set_column_order(array('siswa_nama', 'siswa_user', 'presensi_tanggal_masuk', 'presensi_tanggal_keluar'));
		$this->Datatable_model->set_column_search(array('siswa_nama', 'siswa_user', 'presensi_tanggal_masuk', 'presensi_tanggal_keluar'));
		$this->Datatable_model->set_order(array('siswa_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
			// $presensi	= $this->Presensi_model->get_presensi('*', "presensi.tahun_kode = '$tahun_kode' AND presensi.semester_id = '$semester_id' AND presensi.presensi_user = '$record->siswa_user' AND DATE(presensi.presensi_tanggal_masuk) = '$tanggal_awal'");
        
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['siswa_id'] = $record->siswa_id;
            $row['siswa_nama'] = $record->siswa_nama;
            $row['siswa_user'] = $record->siswa_user;
            $row['presensi_tanggal_masuk'] = $record->presensi_tanggal_masuk;
            $row['presensi_tanggal_keluar'] = $record->presensi_tanggal_keluar;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$tingkat_id				= ($this->uri->segment(7))?$this->uri->segment(7):'-';
		$kelas_id				= ($this->uri->segment(8))?$this->uri->segment(8):'-';
		$presensi_tanggal_awal	= ($this->uri->segment(9))?$this->uri->segment(9):date('Y-m-d');

		$data['tahun_kode']				= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
		$data['semester_id']			= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['tingkat_id']				= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$tingkat_id;
		$data['kelas_id']				= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):$kelas_id;
		$data['presensi_tanggal_awal']	= ($this->input->post('presensi_tanggal_awal'))?$this->input->post('presensi_tanggal_awal'):$presensi_tanggal_awal;
		$data['staf_id']				= userdata('staf_id');
        
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/presensi_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
