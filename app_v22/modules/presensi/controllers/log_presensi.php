<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log_presensi extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Log Presensi | ' . profile('profil_website');
		$this->active_menu		= 339;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Mesin_model');
		$this->load->model('Log_model');
		$this->load->model('Sidik_jari_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		$tahun_kode	= ($this->uri->segment(4))?$this->uri->segment(4):$this->tahun_kode;
		$semester_id	= ($this->uri->segment(5))?$this->uri->segment(5):$semester_id;
		$mesin_id	= ($this->uri->segment(6))?$this->uri->segment(6):'-';

		$this->Datatable_model->set_table("(SELECT sat_log.*, sat_mesin.mesin_nama, sidik_jari_user, sidik_jari_nama FROM sat_log LEFT JOIN (SELECT * FROM sat_sidik_jari GROUP BY sat_sidik_jari.user_id) sidik_jari ON sat_log.user_id=sidik_jari.user_id LEFT JOIN sat_mesin ON sat_log.mesin_id=sat_mesin.mesin_id WHERE sat_log.tahun_kode='$tahun_kode' AND sat_log.semester_id='$semester_id' AND sat_log.mesin_id='$mesin_id') log");
		$this->Datatable_model->set_column_order(array('log_tanggal', 'log.user_id', 'sidik_jari_user', 'sidik_jari_nama', 'mesin_nama'));
		$this->Datatable_model->set_column_search(array('log_tanggal', 'log.user_id', 'sidik_jari_user', 'sidik_jari_nama', 'mesin_nama'));
		$this->Datatable_model->set_order(array('log_tanggal', 'desc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['mesin_nama'] = $record->mesin_nama;
            $row['user_id'] = $record->user_id;
            $row['sidik_jari_user'] = $sidik_jari_user;
            $row['sidik_jari_nama'] = $sidik_jari_nama;
            $row['log_tanggal'] = $record->log_tanggal;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['mesin_id']		= ($this->input->post('mesin_id'))?$this->input->post('mesin_id'):'0';
        
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/log_presensi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function rekap()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;
		
		$where['mesin_status']	= 'A'; 
		$mesin					= $this->Mesin_model->grid_all_mesin('*', 'mesin_id', 'ASC', 0, 0, $where);

		if ($mesin){
			foreach ($mesin as $row) {
				if ($row->mesin_ip && ping($row->mesin_ip) == "OK"){
					$Connect = fsockopen($row->mesin_ip, "80", $errno, $errstr, 1);
					if($Connect){
						$soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$row->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
						$newLine="\r\n";
						fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						fputs($Connect, "Content-Type: text/xml".$newLine);
						fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						fputs($Connect, $soap_request.$newLine);
						$buffer="";
						while($Response=fgets($Connect, 1024)){
							$buffer=$buffer.$Response;
						}
						fclose();
					}

					$buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
					$buffer=explode("\r\n",$buffer);
					$jml_data = count($buffer) - 1;
					for($a=0;$a<count($buffer);$a++){
						$data_=Parse_Data($buffer[$a],"<Row>","</Row>");
						$PIN=Parse_Data($data_,"<PIN>","</PIN>");
						$DateTime=Parse_Data($data_,"<DateTime>","</DateTime>");
						$Verified=Parse_Data($data_,"<Verified>","</Verified>");
						$Status=Parse_Data($data_,"<Status>","</Status>");

						$where_log['log.user_id']			= $PIN;
						$where_log['log.log_tanggal']		= $DateTime;
						$where_log['log.log_verifikasi']	= $Verified;
						$where_log['log.log_status']		= $Status;
						if ($this->Log_model->count_all_log($where_log) < 1){
							if ($data_){
								$insert_log = array();
								$insert_log['tahun_kode']			=$tahun_kode;
								$insert_log['semester_id']		=$semester_id;
								$insert_log['mesin_id']			=$row->mesin_id;
								$insert_log['user_id']			=$PIN;
								$insert_log['log_tanggal']		=$DateTime;
								$insert_log['log_verifikasi']	=$Verified;
								$insert_log['log_status']		=$Status;
								$this->Log_model->insert_log($insert_log);

								$param = array();
								$param['tahun_kode'] = $tahun_kode;
								$param['semester_id'] = $semester_id;
								$param['user_id'] = $PIN;
								$param['datetime'] = $DateTime;
								$res = loadpresensiV2($param);

								$this->Log_model->update_log("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND user_id = '$PIN' AND log_tanggal = '$DateTime' AND log_res_code IS NULL AND log_res_message IS NULL", array('log_res_code'=>$res['status'], 'log_res_message'=>$res['message']));
							}
						}
					}
					$this->Mesin_model->update_mesin(array('mesin_id'=>$row->mesin_id), array('mesin_sinkronisasi'=>$jml_data));
				}
			}
		}

		$this->session->set_flashdata('success','Log berhasil direkap.');
		redirect(module_url($this->uri->segment(2)));
	}

	public function rekap_from_log()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;

		$where['semester_id']	= $semester_id;
		$where['tahun_kode']	= $tahun_kode;
		$log = $this->Log_model->grid_all_log('', 'log_id', 'ASC', 0, 0, $where);
		$i = 1;
		if ($log) {
			foreach ($log as $row_log) {
				$param = array();
				$param['tahun_kode'] = $tahun_kode;
				$param['semester_id'] = $semester_id;
				$param['user_id'] = $row_log->user_id;
				$param['datetime'] = $row_log->log_tanggal;
				$res = loadpresensiV2($param);
				
				$this->Log_model->update_log("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND user_id = '$row_log->user_id' AND log_tanggal = '$row_log->log_tanggal' AND log_res_code IS NULL AND log_res_message IS NULL", array('log_res_code'=>$res['status'], 'log_res_message'=>$res['message']));
				$i++;
			}
		}
		print_r($i);
	}
}
