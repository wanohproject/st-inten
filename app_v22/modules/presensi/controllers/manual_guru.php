<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Manual_guru extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	public $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Manual Staf | ' . profile('profil_website');
		$this->active_menu		= 350;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Departemen_model');
		$this->load->model('Staf_model');
		$this->load->model('Presensi_model');
		$this->load->model('Mesin_model');
		$this->load->model('Sidik_jari_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Manual_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		$departemen_id			= ($this->uri->segment(4))?$this->uri->segment(4):0;
		$where = " AND staf_status = 'Guru'";
		if ($departemen_id){
			$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
			$departemen_list_ = array();
			foreach ($departemen_list as $key => $value) {
				$departemen_list_[] = "	staf.departemen_id = '$value' ";
			}
			$departemen_list_ = implode(" OR ", $departemen_list_);
			$where .= " AND ($departemen_list_)";
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$manual_tanggal			= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$this->Datatable_model->set_table("(SELECT manual.*, staf_user, staf_nama FROM sat_manual manual LEFT JOIN staf ON manual.staf_id=staf.staf_id LEFT JOIN sat_sidik_jari sidik_jari ON sidik_jari.staf_id=staf.staf_id WHERE manual.tahun_kode = '$tahun_kode' AND manual.semester_id = '$semester_id' AND DATE(manual.manual_tanggal) = '$manual_tanggal' $where) manual");
		$this->Datatable_model->set_column_order(array('manual_tanggal', 'manual_tanggal', 'staf_user', 'staf_nama', 'manual_status'));
		$this->Datatable_model->set_column_search(array('manual_tanggal', 'manual_tanggal', 'staf_user', 'staf_nama', 'manual_status'));
		$this->Datatable_model->set_order(array('staf_user', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['manual_hari'] = inday($record->manual_tanggal);
            $row['manual_tanggal'] = dateIndo($record->manual_tanggal);
            $row['staf_user'] = $record->staf_user;
            $row['manual_status'] = ($record->manual_status == 1)?'Pulang':'Masuk';
            $row['staf_nama'] = $record->staf_nama;
            $row['Actions'] = $this->get_buttons($record->manual_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$manual_tanggal			= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['manual_tanggal']	= ($this->input->post('manual_tanggal'))?$this->input->post('manual_tanggal'):$manual_tanggal;

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/manual_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$manual_tanggal			= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):'';
        
		$data['manual_tanggal']			= ($this->input->post('manual_tanggal'))?$this->input->post('manual_tanggal'):$manual_tanggal;
		$data['manual_tanggal_masuk']	= ($this->input->post('manual_tanggal_masuk'))?$this->input->post('manual_tanggal_masuk'):'';
		$data['manual_tanggal_pulang']	= ($this->input->post('manual_tanggal_pulang'))?$this->input->post('manual_tanggal_pulang'):'';
		$data['manual_keterangan']		= ($this->input->post('manual_keterangan'))?$this->input->post('manual_keterangan'):'';
		$data['manual_file']			= ($this->input->post('manual_file'))?$this->input->post('manual_file'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$sidik_jari	= $this->Sidik_jari_model->get_sidik_jari("sidik_jari.*, staf_user", array("sidik_jari.staf_id"=>$this->input->post('staf_id')));
			$user_id 	= ($sidik_jari)?$sidik_jari->user_id:null;
			$staf_user 	= ($sidik_jari)?$sidik_jari->staf_user:null;

			$manual_file = upload_file('manual_file', './asset/uploads/presensi/manual-file/');

			if ($this->input->post('manual_check_masuk')){
				$count_presensi = $this->db->query("SELECT presensi_id FROM sat_presensi presensi WHERE tahun_kode = '".$this->input->post('tahun_kode')."' AND semester_id = '".$this->input->post('semester_id')."' AND presensi.staf_id = '".$this->input->post('staf_id')."' AND DATE(presensi_tanggal_masuk)  = '".$data['manual_tanggal']."' AND presensi_tanggal_keluar IS NULL")->num_rows();
				if ($count_presensi < 1){
					$insert = array();
					$insert['manual_id'] 			= $this->uuid->v4();
					$insert['staf_id'] 				= $this->input->post('staf_id');
					$insert['tahun_kode'] 			= $this->input->post('tahun_kode');
					$insert['semester_id'] 			= $this->input->post('semester_id');
					$insert['manual_user'] 			= $staf_user;
					$insert['manual_tanggal'] 		= $this->input->post('manual_tanggal') . ' ' . $data['manual_tanggal_masuk'] . ':00';
					$insert['manual_status'] 		= 0;
					$insert['manual_keterangan'] 	= ($this->input->post('manual_keterangan'))?$this->input->post('manual_keterangan'):null;
					if ($manual_file){
						$insert['manual_file'] 	= $manual_file;
					}
					$insert['created_by'] 			= userdata('pengguna_id');
					$insert['created_at'] 			= date('Y-m-d H:i:s');
					$this->Manual_model->insert_manual($insert);

					$param = array();
					$param['tahun_kode'] 		= $this->input->post('tahun_kode');
					$param['semester_id'] 	= $this->input->post('semester_id');
					$param['user_id'] 		= $this->input->post('staf_id');
					$param['datetime'] 		= $this->input->post('manual_tanggal') . ' ' . $data['manual_tanggal_masuk'] . ':00';
					$res = loadmanualV2($param, 'staf');
					
					$this->Manual_model->update_manual("tahun_kode = '".$this->input->post('tahun_kode')."' AND semester_id = '".$this->input->post('semester_id')."' AND staf_id = '".$this->input->post('staf_id')."' AND manual_tanggal = '".$this->input->post('manual_tanggal') . ' ' . $data['manual_tanggal_masuk'] . ':00'."' AND manual_res_code IS NULL AND manual_res_message IS NULL", array('manual_res_code'=>$res['status'], 'manual_res_message'=>$res['message']));
				}
			}

			if ($this->input->post('manual_check_pulang')){
				$count_presensi = $this->db->query("SELECT presensi_id FROM sat_presensi presensi WHERE tahun_kode = '".$this->input->post('tahun_kode')."' AND semester_id = '".$this->input->post('semester_id')."' AND presensi.staf_id = '".$this->input->post('staf_id')."' AND DATE(presensi_tanggal_masuk)  = '".$data['manual_tanggal']."' AND presensi_tanggal_keluar IS NULL")->num_rows();
				if ($count_presensi > 0){
					$insert = array();
					$insert['manual_id'] 			= $this->uuid->v4();
					$insert['staf_id'] 				= $this->input->post('staf_id');
					$insert['tahun_kode'] 			= $this->input->post('tahun_kode');
					$insert['semester_id'] 			= $this->input->post('semester_id');
					$insert['manual_user'] 			= $staf_user;
					$insert['manual_tanggal'] 		= $this->input->post('manual_tanggal') . ' ' . $data['manual_tanggal_pulang'] . ':00';
					$insert['manual_status'] 		= 1;
					$insert['manual_keterangan'] 	= ($this->input->post('manual_keterangan'))?$this->input->post('manual_keterangan'):null;
					if ($manual_file){
						$insert['manual_file'] 	= $manual_file;
					}
					$insert['created_by'] 			= userdata('pengguna_id');
					$insert['created_at'] 			= date('Y-m-d H:i:s');
					$this->Manual_model->insert_manual($insert);

					$param = array();
					$param['tahun_kode'] = $this->input->post('tahun_kode');
					$param['semester_id'] = $this->input->post('semester_id');
					$param['user_id'] = $this->input->post('staf_id');
					$param['datetime'] = $this->input->post('manual_tanggal') . ' ' . $data['manual_tanggal_pulang'] . ':00';
					$res = loadmanualV2($param, 'staf');
					
					$this->Manual_model->update_manual("tahun_kode = '".$this->input->post('tahun_kode')."' AND semester_id = '".$this->input->post('semester_id')."' AND staf_id = '".$this->input->post('staf_id')."' AND manual_tanggal = '".$this->input->post('manual_tanggal') . ' ' . $data['manual_tanggal_pulang'] . ':00'."' AND manual_res_code IS NULL AND manual_res_message IS NULL", array('manual_res_code'=>$res['status'], 'manual_res_message'=>$res['message']));
				}
			}
			
			$this->session->set_flashdata('success','Manual Staf telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id'].'/'.$data['tahun_kode'].'/'.$data['semester_id'].'/'.$data['manual_tanggal']));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/manual_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$manual_id = validasi_sql($this->uri->segment(4));
		$where_delete['manual_id']	= $manual_id;
		$manual = $this->Manual_model->get_manual("manual.*", $where_delete);
		$manual_tanggal = ($manual)?date('Y-m-d', strtotime($manual->manual_tanggal)):'';
		if ($manual->manual_status == 0){
			$delete_manual = $this->db->query("DELETE FROM sat_manual WHERE tahun_kode = '".$manual->tahun_kode."' AND semester_id = '".$manual->semester_id."' AND staf_id = '".$manual->staf_id."' AND DATE(manual_tanggal)  = '".$manual_tanggal."' AND manual_status = 1");			
			$delete_presensi = $this->db->query("DELETE FROM sat_presensi WHERE tahun_kode = '".$manual->tahun_kode."' AND semester_id = '".$manual->semester_id."' AND staf_id = '".$manual->staf_id."' AND DATE(presensi_tanggal_masuk)  = '".$manual_tanggal."' AND presensi_sumber = 'M'");			
		} else if ($manual->manual_status == 1){
			$delete_presensi = $this->db->query("UPDATE sat_presensi SET presensi_tanggal_keluar = NULL WHERE tahun_kode = '".$manual->tahun_kode."' AND semester_id = '".$manual->semester_id."' AND staf_id = '".$manual->staf_id."' AND DATE(presensi_tanggal_masuk)  = '".$manual_tanggal."' AND presensi_sumber = 'M'");			
		}
		$this->Manual_model->delete_manual($where_delete);

		$this->session->set_flashdata('success','Manual Staf telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
}