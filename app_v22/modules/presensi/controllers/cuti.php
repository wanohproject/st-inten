<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cuti extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	public $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Cuti | ' . profile('profil_website');
		$this->active_menu		= 347;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Departemen_model');
		$this->load->model('Staf_model');
		$this->load->model('Absensi_model');
		$this->load->model('Cuti_model');
		$this->load->model('Mesin_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		$departemen_id			= ($this->uri->segment(4))?$this->uri->segment(4):0;
		$where = " AND staf_status = 'Guru'";
		if ($departemen_id){
			$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
			$departemen_list_ = array();
			foreach ($departemen_list as $key => $value) {
				$departemen_list_[] = "	staf.departemen_id = '$value' ";
			}
			$departemen_list_ = implode(" OR ", $departemen_list_);
			$where .= " AND ($departemen_list_)";
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$tanggal_awal			= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$this->Datatable_model->set_table("(SELECT cuti.*, staf_nama, staf_user FROM sat_cuti cuti LEFT JOIN staf ON cuti.staf_id=staf.staf_id LEFT JOIN sat_sidik_jari sidik_jari ON cuti.staf_id=sidik_jari.staf_id WHERE cuti.tahun_kode = '$tahun_kode' AND cuti.semester_id = '$semester_id' AND DATE(cuti.cuti_tanggal) = '$tanggal_awal' $where) cuti");
		$this->Datatable_model->set_column_order(array('cuti_tanggal', 'cuti_tanggal', 'staf_user', 'staf_nama', 'cuti_status'));
		$this->Datatable_model->set_column_search(array('cuti_tanggal', 'cuti_tanggal', 'staf_user', 'staf_nama', 'cuti_status'));
		$this->Datatable_model->set_order(array('staf_user', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['cuti_hari'] = inday($record->cuti_tanggal);
            $row['cuti_tanggal'] = dateIndo($record->cuti_tanggal);
            $row['cuti_status'] = $record->cuti_status;
            $row['staf_user'] = $record->staf_user;
            $row['staf_nama'] = $record->staf_nama;
            $row['Actions'] = $this->get_buttons($record->cuti_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$cuti_tanggal_mulai	= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$data['tahun_kode']				= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']			= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['cuti_tanggal_mulai']	= ($this->input->post('cuti_tanggal_mulai'))?$this->input->post('cuti_tanggal_mulai'):$cuti_tanggal_mulai;

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/cuti', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$cuti_tanggal_mulai	= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$data['cuti_id'] = ($this->input->post('cuti_id'))?$this->input->post('cuti_id'):'';
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):'';
		$get_staf = $this->Staf_model->get_staf("", array("staf_id"=>$data['staf_id']));
		if ($get_staf){
			$data['cuti_user'] = ($this->input->post('cuti_user'))?$this->input->post('cuti_user'):$get_staf->staf_user;
			$data['cuti_nama'] = ($this->input->post('cuti_nama'))?$this->input->post('cuti_nama'):$get_staf->staf_nama;
			$data['cuti_unit'] = ($this->input->post('cuti_unit'))?$this->input->post('cuti_unit'):$get_staf->departemen_nama;
			$data['cuti_jabatan'] = ($this->input->post('cuti_jabatan'))?$this->input->post('cuti_jabatan'):$get_staf->staf_jabatan;
			$data['cuti_alamat'] = ($this->input->post('cuti_alamat'))?$this->input->post('cuti_alamat'):$get_staf->staf_alamat;
			$data['cuti_telepon'] = ($this->input->post('cuti_telepon'))?$this->input->post('cuti_telepon'):$get_staf->staf_hp;

			$this->Departemen_model->get_departemen_parent();
		} else {
			$data['cuti_user'] = ($this->input->post('cuti_user'))?$this->input->post('cuti_user'):'';
			$data['cuti_nama'] = ($this->input->post('cuti_nama'))?$this->input->post('cuti_nama'):'';
			$data['cuti_unit'] = ($this->input->post('cuti_unit'))?$this->input->post('cuti_unit'):'';
			$data['cuti_jabatan'] = ($this->input->post('cuti_jabatan'))?$this->input->post('cuti_jabatan'):'';
			$data['cuti_alamat'] = ($this->input->post('cuti_alamat'))?$this->input->post('cuti_alamat'):'';
			$data['cuti_telepon'] = ($this->input->post('cuti_telepon'))?$this->input->post('cuti_telepon'):'';
		}
        
		$data['cuti_tanggal'] = ($this->input->post('cuti_tanggal'))?$this->input->post('cuti_tanggal'):'';
		$data['cuti_pengajuan_lama'] = ($this->input->post('cuti_pengajuan_lama'))?$this->input->post('cuti_pengajuan_lama'):'';
		$data['cuti_tanggal_mulai'] = ($this->input->post('cuti_tanggal_mulai'))?$this->input->post('cuti_tanggal_mulai'):'';
		$data['cuti_tanggal_selesai'] = ($this->input->post('cuti_tanggal_selesai'))?$this->input->post('cuti_tanggal_selesai'):'';
		$data['cuti_lama_disetujui'] = ($this->input->post('cuti_lama_disetujui'))?$this->input->post('cuti_lama_disetujui'):'';
		$data['cuti_lama_sisa'] = ($this->input->post('cuti_lama_sisa'))?$this->input->post('cuti_lama_sisa'):'';
		$data['cuti_kepala'] = ($this->input->post('cuti_kepala'))?$this->input->post('cuti_kepala'):'';
		$data['cuti_atasan'] = ($this->input->post('cuti_atasan'))?$this->input->post('cuti_atasan'):'';
		$data['cuti_pengurus'] = ($this->input->post('cuti_pengurus'))?$this->input->post('cuti_pengurus'):'';
		$data['cuti_status'] = ($this->input->post('cuti_status'))?$this->input->post('cuti_status'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$staf	= $this->Staf_model->get_staf("staf.*", array("staf_id"=>$data['staf_id']));
			$staf_user = ($staf)?$staf->staf_user:null;
			
			$cuti_file = upload_file('cuti_file', './asset/uploads/presensi/cuti-file/');

			$cuti_tanggal_mulai = strtotime($data['cuti_tanggal_mulai']);
			$cuti_tanggal_selesai = strtotime($data['cuti_tanggal_selesai']);
			$datediff = $cuti_tanggal_selesai - $cuti_tanggal_mulai;
			$days = round($datediff / (60 * 60 * 24)) + 1;

			for ($i=0; $i < $days; $i++) {
				if ($i == 0){
					$date = $data['cuti_tanggal_mulai'];
				} else {
					$date = date('Y-m-d', strtotime($data['cuti_tanggal_mulai'] . " + $i days"));
				}
				if ($this->Cuti_model->count_all_cuti(array('cuti_tanggal'=>$date, 'tahun_kode'=>$this->input->post('tahun_kode'), 'semester_id'=>$this->input->post('semester_id'), 'cuti.staf_id'=>$this->input->post('staf_id'))) < 1){
					$insert = array();
					$insert['cuti_id'] 			= $this->uuid->v4();
					$insert['tahun_kode'] 			= $this->input->post('tahun_kode');
					$insert['semester_id'] 			= $this->input->post('semester_id');
					$insert['staf_id'] 				= $this->input->post('staf_id');
					$insert['cuti_user'] 		= $staf_user;
					$insert['cuti_tanggal'] 		= $date;
					$insert['cuti_info'] 		= $this->input->post('cuti_info');
					$insert['cuti_deskripsi'] 	= ($this->input->post('cuti_deskripsi'))?$this->input->post('cuti_deskripsi'):null;
					if ($cuti_file){
						$insert['cuti_file'] 	= $cuti_file;
					}
					$insert['created_by'] 			= userdata('pengguna_id');
					$insert['created_at'] 			= date('Y-m-d H:i:s');
					$this->Cuti_model->insert_cuti($insert);
				}
			}
			
			$this->session->set_flashdata('success','Cuti telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id'].'/'.$data['tahun_kode'].'/'.$data['semester_id'].'/'.$data['cuti_tanggal_mulai']));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/cuti', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$cuti_id = validasi_sql($this->uri->segment(4));
		$where_delete['cuti_id']	= $cuti_id;
		$this->Cuti_model->delete_cuti($where_delete);

		$this->session->set_flashdata('success','Cuti telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
}