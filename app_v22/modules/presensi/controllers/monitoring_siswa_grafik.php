<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_siswa_grafik extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Monitoring Presensi Siswa (Grafik) | ' . profile('profil_website');;
		$this->active_menu		= 'monitoring';
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Sidik_jari_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Mesin_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data	= '';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$data['tahun_kode']		= $this->tahun_kode;
		$data['semester_id']	= $semester_id;
		$data['tingkat']		= $this->Tingkat_model->grid_all_tingkat("", "tingkat_kode", "ASC", "", "", array("tingkat.departemen_id"=>$data['departemen_id']));
		$data['tanggal']		= date('Y-m-d');
		// $data['tanggal']		= "2018-05-22";

		$where['mesin_status']	= 'A'; 
		$data['mesin'] = $this->Mesin_model->grid_all_mesin('*', 'mesin_id', 'ASC', 0, 0, $where);
		
		$this->load->view(module_dir().'/monitoring/head', $head);
		$this->load->view(module_dir().'/monitoring/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/monitoring/monitoring_siswa_grafik', $data);
		$this->load->view(module_dir().'/monitoring/foot');
	}

	public function get_siswa(){
		$departemen_id 	= $this->uri->segment(4);
		$tahun_kode 		= $this->uri->segment(5);
		$semester_id 	= $this->uri->segment(6);
		$tingkat_id 	= $this->uri->segment(7);
		$tanggal		= ($this->uri->segment(8))?$this->uri->segment(8):date('Y-m-d');

		// $kelas = $this->db->query("SELECT COUNT(siswa_kelas.siswa_id) as siswa_jumlah, kelas.kelas_nama, kelas.kelas_id FROM siswa_kelas LEFT JOIN kelas ON siswa_kelas.kelas_id=kelas.kelas_id WHERE siswa_kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$tingkat_id' GROUP BY siswa_kelas.kelas_id")->result(); 
		$kelas = $this->db->query("SELECT COUNT(siswa_kelas.siswa_id) as siswa_jumlah, kelas.kelas_nama, kelas.kelas_id, GROUP_CONCAT(siswa_user SEPARATOR ', ') AS siswa_list 
									FROM siswa_kelas 
									LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id 
									LEFT JOIN kelas ON siswa_kelas.kelas_id=kelas.kelas_id 
									WHERE siswa_kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$tingkat_id' GROUP BY siswa_kelas.kelas_id")->result(); 
		$data = array();
		$i = 0;
        foreach ($kelas as $row) {
			$presensi	= $this->db->query("SELECT COUNT(presensi_id) presensi_jumlah
											FROM sat_presensi presensi
											WHERE presensi.presensi_user IN ($row->siswa_list)
												AND DATE(presensi.presensi_tanggal_masuk) = '$tanggal'")->row();
			$presensi_jumlah = ($presensi)?(int) $presensi->presensi_jumlah:0;

			$data[$i]['kelas'] = $row->kelas_nama;
			$data[$i]['status'] = "Jumlah Siswa";
			$data[$i]['val'] = (int) ($row->siswa_jumlah - $presensi_jumlah);
			$data[$i]['color'] = 'red';
			$i++;
			$data[$i]['kelas'] = $row->kelas_nama;
			$data[$i]['status'] = "Siswa Hadir";
			$data[$i]['val'] = (int) $presensi_jumlah;
			$data[$i]['color'] = 'green';
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function dataload()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;
		$departemen_id			= ($this->uri->segment(4))?$this->uri->segment(4):0;
		$tanggal				= ($this->uri->segment(5))?$this->uri->segment(5):date('Y-m-d');
		
        $list = $this->db->query("SELECT sat_log.* FROM sat_log WHERE sat_log.tahun_kode='$tahun_kode' AND sat_log.semester_id='$semester_id' AND DATE(log_tanggal) = '$tanggal' ORDER BY log_tanggal DESC LIMIT 20")->result();		
		$data = array();
		foreach ($list as $record) {
			$sidik_jari = $this->Sidik_jari_model->get_sidik_jari("sidik_jari_nama, sidik_jari_level, sidik_jari_user", array('sidik_jari.user_id'=>$record->user_id));
			$row = array();
			if ($sidik_jari){
				$sidik_jari_nama = $sidik_jari->sidik_jari_nama;
				$sidik_jari_level = $sidik_jari->sidik_jari_level;
				$sidik_jari_user = $sidik_jari->sidik_jari_user;
				if ($sidik_jari_level == 'Siswa'){
					$siswa = $this->db->query("SELECT siswa_foto FROM siswa WHERE siswa_user='$sidik_jari_user'")->row();
					$siswa_foto = ($siswa)?$siswa->siswa_foto:'';
	
					$foto = ($siswa_foto && file_exists('./asset/foto-siswa/'.$siswa_foto))?base_url('asset/foto-siswa/'.$siswa_foto):base_url('asset/profil/users.jpg');
				} else {
					$staf = $this->db->query("SELECT staf_foto FROM staf WHERE staf_user='$sidik_jari_user'")->row();
					$staf_foto = ($staf)?$staf->staf_foto:'';
	
					$foto = ($staf_foto && file_exists('./asset/foto-staf/'.$staf_foto))?base_url('asset/foto-staf/'.$staf_foto):base_url('asset/profil/users.jpg');
				}
				
				if ($record->log_res_code == 1){
					$data[] = "<tr bgcolor=\"#028002\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
				} else if ($record->log_res_code == 3){
					$data[] = "<tr bgcolor=\"#880250\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
					
				} else if ($record->log_res_code == 9 || $record->log_res_code == 10){
					$data[] = "<tr bgcolor=\"#A0A002\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
					
				} else {
					$data[] = "<tr bgcolor=\"#A00202\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
				}
			}
        }
		
		header('Content-Type: application/json');
        echo json_encode($data);
	}
}