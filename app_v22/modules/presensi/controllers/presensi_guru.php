<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi_guru extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	public $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Presensi Staf | ' . profile('profil_website');
		$this->active_menu		= 336;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Staf_model');
		$this->load->model('Departemen_model');
		$this->load->model('Presensi_model');
		$this->load->model('Mesin_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	public function datatable()
    {
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		$departemen_id			= ($this->uri->segment(4))?$this->uri->segment(4):0;
		$where = " staf_status = 'Guru'";
		if ($departemen_id){
			$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
			$departemen_list_ = array();
			foreach ($departemen_list as $key => $value) {
				$departemen_list_[] = "	departemen.departemen_id = '$value' ";
			}
			$departemen_list_ = implode(" OR ", $departemen_list_);
			$where .= " AND ($departemen_list_)";
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$tanggal_awal			= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$this->Datatable_model->set_table("(SELECT staf.*, departemen.departemen_kode, presensi.presensi_tanggal_masuk, presensi.presensi_tanggal_keluar FROM staf LEFT JOIN (SELECT * FROM sat_presensi WHERE DATE(sat_presensi.presensi_tanggal_masuk) = '$tanggal_awal') presensi ON staf.staf_user=presensi.presensi_user LEFT JOIN departemen ON staf.departemen_id=departemen.departemen_id WHERE $where AND (staf_tst = '0000-00-00' OR staf_tst >= '$tanggal_awal' OR staf_tst IS NULL)) staf");
		$this->Datatable_model->set_column_order(array('departemen_kode', 'staf_nama', 'staf_user', 'presensi_tanggal_masuk', 'presensi_tanggal_keluar'));
		$this->Datatable_model->set_column_search(array('departemen_kode', 'staf_nama', 'staf_user', 'presensi_tanggal_masuk', 'presensi_tanggal_keluar'));
		$this->Datatable_model->set_order(array('presensi_tanggal_masuk', 'DESC'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
			// $presensi	= $this->Presensi_model->get_presensi('*', "presensi.tahun_kode = '$tahun_kode' AND presensi.semester_id = '$semester_id' AND presensi.presensi_user = '$record->staf_user' AND DATE(presensi.presensi_tanggal_masuk) = '$tanggal_awal'");
        
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['staf_id'] = $record->staf_id;
            $row['departemen_kode'] = $record->departemen_kode;
            $row['staf_nama'] = $record->staf_nama;
            $row['staf_user'] = $record->staf_user;
            $row['presensi_tanggal_masuk'] = $record->presensi_tanggal_masuk;
            $row['presensi_tanggal_keluar'] = $record->presensi_tanggal_keluar;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
		$presensi_tanggal_awal	= ($this->uri->segment(7))?$this->uri->segment(7):date('Y-m-d');
		
		$data['tahun_kode']				= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']			= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['presensi_tanggal_awal']	= ($this->input->post('presensi_tanggal_awal'))?$this->input->post('presensi_tanggal_awal'):$presensi_tanggal_awal;

		$where['mesin_status']	= 'A'; 
		$data['mesin'] = $this->Mesin_model->grid_all_mesin('*', 'mesin_id', 'ASC', 0, 0, $where);
        
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/presensi_guru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
