<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_guru extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Monitoring Presensi Staf | ' . profile('profil_website');;
		$this->active_menu		= 'monitoring';
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Sidik_jari_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Mesin_model');
		$this->load->model('Jadwal_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data	= '';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']		= $this->tahun_kode;
		$data['semester_id']	= $semester_id;
		$data['tingkat']		= $this->Tingkat_model->grid_all_tingkat("", "tingkat_kode", "ASC");
		$data['tanggal']		= date('Y-m-d');
		// $data['tanggal']		= "2018-05-22";

		$where['mesin_status']	= 'A'; 
		$data['mesin'] = $this->Mesin_model->grid_all_mesin('*', 'mesin_id', 'ASC', 0, 0, $where);
		
		$this->load->view(module_dir().'/monitoring/head', $head);
		$this->load->view(module_dir().'/monitoring/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/monitoring/monitoring_guru', $data);
		$this->load->view(module_dir().'/monitoring/foot');
	}

	public function get_guru(){
		$tanggal		= ($this->uri->segment(4))?$this->uri->segment(4):date('Y-m-d');
		$jenis_ptk		= ($this->uri->segment(5))?$this->uri->segment(5):'Guru';

		$data = array();
		$staf_jumlah = $this->db->query("SELECT sidik_jari_id FROM sat_sidik_jari LEFT JOIN staf ON sat_sidik_jari.sidik_jari_user=staf.staf_user WHERE sidik_jari_level = 'Guru' AND staf_status = '$jenis_ptk' GROUP BY sidik_jari_user")->num_rows();

		$presensi_jumlah	= $this->db->query("SELECT presensi_id
										FROM sat_presensi presensi
										LEFT JOIN staf ON presensi.presensi_user=staf.staf_user
										LEFT JOIN (SELECT sidik_jari_user, sidik_jari_level FROM sat_sidik_jari GROUP BY sat_sidik_jari.user_id) sidik_jari ON presensi.presensi_user=sidik_jari.sidik_jari_user
										WHERE sidik_jari.sidik_jari_level = 'Guru'
											AND DATE(presensi.presensi_tanggal_masuk) = '$tanggal'
											AND staf_status = '$jenis_ptk'
											GROUP BY sidik_jari.sidik_jari_user")->num_rows();

		$data['thadir'] = (int) ($staf_jumlah - $presensi_jumlah);
		$data['hadir'] = (int) $presensi_jumlah;

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function dataload2()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;
		$tanggal				= ($this->uri->segment(4))?$this->uri->segment(4):date('Y-m-d');
		
		$list = $this->db->query("SELECT sat_log.* FROM sat_log WHERE DATE(log_tanggal) = '$tanggal' ORDER BY log_tanggal DESC")->result();
		$data = array();
		foreach ($list as $record) {
			$sidik_jari = $this->Sidik_jari_model->get_sidik_jari("sidik_jari_nama, sidik_jari_level, sidik_jari_user", array('sidik_jari.user_id'=>$record->user_id, 'sidik_jari_level'=>'Guru'));

			if ($sidik_jari){
				$sidik_jari_nama = $sidik_jari->sidik_jari_nama;
				$sidik_jari_level = $sidik_jari->sidik_jari_level;
				$sidik_jari_user = $sidik_jari->sidik_jari_user;

				$staf = $this->db->query("SELECT staf_foto FROM staf WHERE staf_user='$sidik_jari_user'")->row();
				$staf_foto = ($staf)?$staf->staf_foto:'';

				$foto = ($staf_foto && file_exists('./asset/foto-staf/'.$staf_foto))?base_url('asset/foto-staf/'.$staf_foto):base_url('asset/profil/users.jpg');
				
				if ($record->log_res_code == 1){
					$data[] = "<tr bgcolor=\"#028002\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
				} else if ($record->log_res_code == 3){
					$data[] = "<tr bgcolor=\"#880250\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
					
				} else if ($record->log_res_code == 9 || $record->log_res_code == 10){
					$data[] = "<tr bgcolor=\"#A0A002\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
					
				} else {
					$data[] = "<tr bgcolor=\"#A00202\">
										<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
										<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
										<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
									</tr>";
				}
			}
        }
		
		header('Content-Type: application/json');
        echo json_encode($data);
	}

	public function dataload()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;
		$tanggal				= ($this->uri->segment(4))?$this->uri->segment(4):date('Y-m-d');
		
        $list = $this->db->query("SELECT sat_log.*, sidik_jari_user, sidik_jari_nama, sidik_jari_level FROM sat_log LEFT JOIN (SELECT sidik_jari_user, sidik_jari_nama, sidik_jari_level, user_id FROM sat_sidik_jari GROUP BY sat_sidik_jari.user_id) sat_sidik_jari ON sat_log.user_id=sat_sidik_jari.user_id WHERE DATE(log_tanggal) = '$tanggal' AND sat_sidik_jari.sidik_jari_level = 'Guru' ORDER BY log_tanggal DESC LIMIT 20")->result();		
		$data = array();
		foreach ($list as $record) {
			$sidik_jari_nama = $record->sidik_jari_nama;
			$sidik_jari_level = $record->sidik_jari_level;
			$sidik_jari_user = $record->sidik_jari_user;

			$staf = $this->db->query("SELECT staf_foto FROM staf WHERE staf_user='$sidik_jari_user'")->row();
			$staf_foto = ($staf)?$staf->staf_foto:'';

			$foto = ($staf_foto && file_exists('./asset/foto-staf/'.$staf_foto))?base_url('asset/foto-staf/'.$staf_foto):base_url('asset/profil/users.jpg');
			
			if ($record->log_res_code == 1){
				$data[] = "<tr bgcolor=\"#028002\">
									<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
									<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
									<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
								</tr>";
			} else if ($record->log_res_code == 3){
				$data[] = "<tr bgcolor=\"#880250\">
									<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
									<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
									<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
								</tr>";
				
			} else if ($record->log_res_code == 9 || $record->log_res_code == 10){
				$data[] = "<tr bgcolor=\"#A0A002\">
									<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
									<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
									<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
								</tr>";
				
			} else {
				$data[] = "<tr bgcolor=\"#A00202\">
									<td width=\"100\" class=\"time\"><span>".substr($record->log_tanggal, -8,5)."</span><span class=\"small\">".substr($record->log_tanggal, -2)."</span></td>
									<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
									<td><h5>".$record->user_id."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
								</tr>";
			}
        }
		
		header('Content-Type: application/json');
        echo json_encode($data);
	}

	public function dataload_table()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;
		$tanggal				= ($this->uri->segment(4))?$this->uri->segment(4):date('Y-m-d');
		$jenis_ptk				= ($this->uri->segment(5))?$this->uri->segment(5):'Guru';

		$N = date('N', strtotime($tanggal));
		$date = substr($tanggal, 0, 10);
		$time = substr($tanggal, 11, 8);
		$jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>'Guru'));

		$list = $this->db->query("SELECT sat_presensi.presensi_tanggal_masuk, sat_presensi.presensi_tanggal_keluar, staf_nama, staf_nip FROM sat_presensi LEFT JOIN (SELECT * FROM sat_sidik_jari GROUP BY sidik_jari_user) sat_sidik_jari ON sat_presensi.presensi_user=sat_sidik_jari.sidik_jari_user LEFT JOIN staf ON sat_presensi.presensi_user=staf.staf_user WHERE DATE(sat_presensi.presensi_tanggal_masuk) = '$tanggal' AND sidik_jari_level = 'Guru' AND staf_status = '$jenis_ptk' ORDER BY presensi_tanggal_masuk DESC LIMIT 100")->result();
		$data = array();
		foreach ($list as $record) {
			if (strtotime($record->presensi_tanggal_masuk) <= strtotime($tanggal.' '.$jadwal->jadwal_masuk_tepat)){
				$data[] = "<tr bgcolor=\"#028002\">
					<td style=\"text-align:center;\">".$record->staf_nip."</td>
					<td style=\"text-align:left;\">".$record->staf_nama."</td>
					<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_masuk, 0, 16))."</td>
					<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_keluar, 0, 16))."</td>
				</tr>";
			} else {
				$data[] = "<tr bgcolor=\"#A0A002\">
					<td style=\"text-align:center;\">".$record->staf_nip."</td>
					<td style=\"text-align:left;\">".$record->staf_nama."</td>
					<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_masuk, 0, 16))."</td>
					<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_keluar, 0, 16))."</td>
				</tr>";
			}
        }
		
		header('Content-Type: application/json');
        echo json_encode($data);
	}
}