<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_kartu extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Monitoring Presensi Kartu | ' . profile('profil_website');;
		$this->active_menu		= 'monitoring';
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		// $this->authentication->set_menu($this->active_menu);
		// $this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Sidik_jari_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Mesin_model');

		$this->load->model('presensi/Jadwal_model');
		$this->load->model('presensi/Sidik_kartu_model');
		$this->load->model('presensi/Log_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data	= '';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']		= $this->tahun_kode;
		$data['semester_id']	= $semester_id;
		$data['tingkat']		= $this->Tingkat_model->grid_all_tingkat("", "tingkat_kode", "ASC");
		$data['tanggal']		= date('Y-m-d');
		// $data['tanggal']		= "2018-05-22";

		$where['mesin_status']	= 'A'; 
		$data['mesin'] = $this->Mesin_model->grid_all_mesin('*', 'mesin_id', 'ASC', 0, 0, $where);
		
		$this->load->view(module_dir().'/monitoring/head', $head);
		$this->load->view(module_dir().'/monitoring/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/monitoring/monitoring_kartu', $data);
		$this->load->view(module_dir().'/monitoring/foot');
	}

	public function get_guru(){
		$tahun_kode 		= $this->uri->segment(4);
		$semester_id 	= $this->uri->segment(5);
		$tanggal		= ($this->uri->segment(6))?$this->uri->segment(6):date('Y-m-d');

		$data = array();
		$staf_jumlah = $this->db->query("SELECT sidik_jari_id FROM sat_sidik_jari LEFT JOIN staf ON sat_sidik_jari.staf_id=staf.staf_id WHERE sidik_jari_level = 'Guru' AND staf_status = 'Tendik' GROUP BY sidik_jari_user")->num_rows();

		$presensi_jumlah	= $this->db->query("SELECT presensi_id
										FROM sat_presensi presensi
										LEFT JOIN sat_sidik_jari sidik_jari ON presensi.presensi_user=sidik_jari.sidik_jari_user
										LEFT JOIN staf ON sidik_jari.staf_id=staf.staf_id
										WHERE staf_status = 'Guru'
											AND DATE(presensi.presensi_tanggal_masuk) = '$tanggal'
											GROUP BY sidik_jari.sidik_jari_user")->num_rows();

		$i = 0;
		$data[$i]['name'] = "Tidak Hadir";
		$data[$i]['val'] = (int) ($staf_jumlah - $presensi_jumlah);
		$data[$i]['y'] = (int) ($staf_jumlah - $presensi_jumlah);
		$data[$i]['color'] = 'red';
		$data[$i]['date'] = $tanggal;
		$i++;
		$data[$i]['name'] = "Guru Hadir";
		$data[$i]['val'] = (int) $presensi_jumlah;
		$data[$i]['y'] = (int) $presensi_jumlah;
		$data[$i]['color'] = 'green';
		$data[$i]['date'] = $tanggal;
		// $data[$i]['sliced'] = true;
		// $data[$i]['selected'] = true;
		$i++;

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function get_tendik(){
		$tahun_kode 		= $this->uri->segment(4);
		$semester_id 	= $this->uri->segment(5);
		$tanggal		= ($this->uri->segment(6))?$this->uri->segment(6):date('Y-m-d');

		$data = array();
		$staf_jumlah = $this->db->query("SELECT sidik_jari_id FROM sat_sidik_jari LEFT JOIN staf ON sat_sidik_jari.staf_id=staf.staf_id WHERE sidik_jari_level = 'Guru' AND staf_status = 'Tendik' GROUP BY sidik_jari_user")->num_rows();

		$presensi_jumlah	= $this->db->query("SELECT presensi_id
										FROM sat_presensi presensi
										LEFT JOIN sat_sidik_jari sidik_jari ON presensi.presensi_user=sidik_jari.sidik_jari_user
										LEFT JOIN staf ON sidik_jari.staf_id=staf.staf_id
										WHERE sidik_jari.sidik_jari_level = 'Guru'
											AND staf_status = 'Tendik'
											AND DATE(presensi.presensi_tanggal_masuk) = '$tanggal'
											GROUP BY sidik_jari.sidik_jari_user")->num_rows();

		$i = 0;
		$data[$i]['name'] = "Tidak Hadir";
		$data[$i]['val'] = (int) ($staf_jumlah - $presensi_jumlah);
		$data[$i]['y'] = (int) ($staf_jumlah - $presensi_jumlah);
		$data[$i]['color'] = 'red';
		$data[$i]['date'] = $tanggal;
		$i++;
		$data[$i]['name'] = "Staf Hadir";
		$data[$i]['val'] = (int) $presensi_jumlah;
		$data[$i]['y'] = (int) $presensi_jumlah;
		$data[$i]['color'] = 'green';
		$data[$i]['date'] = $tanggal;
		// $data[$i]['sliced'] = true;
		// $data[$i]['selected'] = true;
		$i++;

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function presensi(){
		$data = array();
		$semester 			= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id		= $semester->semester_id;
		$tahun_kode			= $this->tahun_kode;

		$cardNumber = trim($this->input->post('cardNumber'));
		$dataFoto = $this->input->post('dataFoto');
		$log_foto = "";
		$blobData = "";
		if ($dataFoto) {
			$pos = strpos($dataFoto, 'base64,');
			$blobData = base64_decode(substr($dataFoto, $pos + 7));
		}

		if ($cardNumber){
			$sidik_kartu = $this->Sidik_kartu_model->get_sidik_kartu('*', array('sidik_kartu_nomor'=>$cardNumber));
			if ($sidik_kartu){
				$PIN = $sidik_kartu->user_id;
				$DateTime = date('Y-m-d H:i:s');
				$Verified = 9;
				$Status = 9;

				$insert_log = array();
				$insert_log['log_id']			= $this->uuid->v4();
				$insert_log['tahun_kode']			= $tahun_kode;
				$insert_log['semester_id']		= $semester_id;
				$insert_log['log_mesin']		= 'KARTU';
				$insert_log['user_id']			= $PIN;
				$insert_log['log_tanggal']		= $DateTime;
				$insert_log['log_verifikasi']	= $Verified;
				$insert_log['log_status']		= $Status;
				$insert_log['log_foto']			= $blobData;
				$this->Log_model->insert_log($insert_log);
		
				$param = array();
				$param['tahun_kode'] = $tahun_kode;
				$param['semester_id'] = $semester_id;
				$param['user_id'] = $PIN;
				$param['datetime'] = $DateTime;
				$res = loadpresensiV2($param);
				
				$this->Log_model->update_log("tahun_kode = '$tahun_kode' AND semester_id = '$semester_id' AND user_id = '$PIN' AND log_tanggal = '$DateTime' AND log_res_code IS NULL AND log_res_message IS NULL", array('log_res_code'=>$res['status'], 'log_res_message'=>$res['message']));

				$sidik_jari_nama = $sidik_kartu->sidik_jari_nama;
				$sidik_jari_level = $sidik_kartu->sidik_jari_level;
				$sidik_jari_user = $sidik_kartu->sidik_jari_user;

				$staf = $this->db->query("SELECT staf_foto FROM staf WHERE staf_user='$sidik_jari_user'")->row();
				$staf_foto = ($staf)?$staf->staf_foto:'';

				// $foto = ($staf_foto && file_exists('./asset/uploads/foto-staf/'.$staf_foto))?base_url('asset/foto-staf/'.$staf_foto):base_url('asset/profil/users.jpg');
				if ($dataFoto){
					$foto = $dataFoto;
				} else {					
					$foto = ($staf_foto && file_exists('./asset/uploads/foto-staf/'.$staf_foto))?base_url('asset/foto-staf/'.$staf_foto):base_url('asset/profil/users.jpg');
				}
				
				if ($res['status'] == 1){
					$res_ = "<tr bgcolor=\"#028002\">
								<td colspan=\"3\" style=\"text-align:center\"><h4><strong>".$res['message']."</strong></h4></td>
							</tr>
							<tr bgcolor=\"#028002\">
								<td width=\"100\" class=\"time\"><span>".substr($DateTime, -8,5)."</span><span class=\"small\">".substr($DateTime, -2)."</span></td>
								<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
								<td><h5>".$sidik_jari_user."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
							</tr>";
				} else if ($res['status'] == 3){
					$res_ = "<tr bgcolor=\"#880250\">
								<td colspan=\"3\" style=\"text-align:center\"><h4><strong>".$res['message']."</strong></h4></td>
							</tr>
							<tr bgcolor=\"#880250\">
								<td width=\"100\" class=\"time\"><span>".substr($DateTime, -8,5)."</span><span class=\"small\">".substr($DateTime, -2)."</span></td>
								<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
								<td><h5>".$sidik_jari_user."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
							</tr>";
					
				} else if ($res['status'] == 9 || $res['status'] == 10){
					$res_ = "<tr bgcolor=\"#A0A002\">
								<td colspan=\"3\" style=\"text-align:center\"><h4><strong>".$res['message']."</strong></h4></td>
							</tr>
							<tr bgcolor=\"#A0A002\">
								<td width=\"100\" class=\"time\"><span>".substr($DateTime, -8,5)."</span><span class=\"small\">".substr($DateTime, -2)."</span></td>
								<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
								<td><h5>".$sidik_jari_user."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
							</tr>";
					
				} else {
					$res_ = "<tr bgcolor=\"#A00202\">
								<td colspan=\"3\" style=\"text-align:center\"><h4><strong>".$res['message']."</strong></h4></td>
							</tr>
							<tr bgcolor=\"#A00202\">
								<td width=\"100\" class=\"time\"><span>".substr($DateTime, -8,5)."</span><span class=\"small\">".substr($DateTime, -2)."</span></td>
								<td width=\"55\"><img src=\"".$foto."\" alt=\"\" width=\"60\" height=\"60\"></td>
								<td><h5>".$sidik_jari_user."</h5><h4>".strtoupper($sidik_jari_nama)."</h4><h5>".$sidik_jari_level."</h5></td>
							</tr>";
				}
				
				$data['response']	= true;
				$data['message']	= "Data ada.";
				$data['data']		= $sidik_kartu;
				$data['log']		= $res_;

			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function dataload_table()
    {
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		$tahun_kode				= $this->tahun_kode;
		$tanggal				= ($this->uri->segment(4))?$this->uri->segment(4):date('Y-m-d');
		$jenis_ptk				= ($this->uri->segment(5))?$this->uri->segment(5):'Guru';

		$N = date('N', strtotime($tanggal));
		$date = substr($tanggal, 0, 10);
		$time = substr($tanggal, 11, 8);
		$jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>'Guru'));

		$list = $this->db->query("SELECT sat_presensi.presensi_tanggal_masuk, sat_presensi.presensi_tanggal_keluar, staf_nama, staf_nip FROM sat_presensi LEFT JOIN sat_sidik_jari ON sat_presensi.staf_id=sat_sidik_jari.staf_id LEFT JOIN staf ON sat_presensi.staf_id=staf.staf_id WHERE DATE(sat_presensi.presensi_tanggal_masuk) = '$tanggal' AND staf_status = '$jenis_ptk' ORDER BY presensi_tanggal_masuk DESC LIMIT 100")->result();
		$data = array();
		foreach ($list as $record) {
			// if (strtotime($record->presensi_tanggal_masuk) <= strtotime($tanggal.' '.$jadwal->jadwal_masuk_tepat)){
				$data[] = "<tr bgcolor=\"#028002\">
					<td style=\"text-align:center;\">".$record->staf_nip."</td>
					<td style=\"text-align:left;\">".$record->staf_nama."</td>
					<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_masuk, 0, 16))."</td>
					<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_keluar, 0, 16))."</td>
				</tr>";
			// } else {
			// 	$data[] = "<tr bgcolor=\"#A0A002\">
			// 		<td style=\"text-align:center;\">".$record->staf_nip."</td>
			// 		<td style=\"text-align:left;\">".$record->staf_nama."</td>
			// 		<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_masuk, 0, 16))."</td>
			// 		<td style=\"text-align:center;\" nowrap>".dateIndo3(substr($record->presensi_tanggal_keluar, 0, 16))."</td>
			// 	</tr>";
			// }
        }
		
		header('Content-Type: application/json');
        echo json_encode($data);
	}
}