<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mesin extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Mesin | ' . profile('profil_website');
		$this->active_menu		= 325;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('presensi/Mesin_model');
		$this->load->model('presensi/Sidik_mesin_model');
    }
	
	public function datatable()
    {
		$this->Datatable_model->set_table("sat_mesin");
		$this->Datatable_model->set_column_order(array('mesin_nama', 'mesin_ip', 'mesin_key', 'mesin_status'));
		$this->Datatable_model->set_column_search(array('mesin_nama', 'mesin_ip', 'mesin_key', 'mesin_status'));
		$this->Datatable_model->set_order(array('mesin_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['mesin_nama'] = $record->mesin_nama;
            $row['mesin_ip'] = $record->mesin_ip;
            $row['mesin_key'] = $record->mesin_key;
            $row['mesin_status'] = $record->mesin_status;
            $row['Actions'] = $this->get_buttons($record->mesin_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Info Mesin"><i class="fa fa-info-circle"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
        
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mesin', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
        
		$data['mesin_id']        	= ($this->input->post('mesin_id'))?$this->input->post('mesin_id'):'';
		$data['mesin_ip']        	= ($this->input->post('mesin_ip'))?$this->input->post('mesin_ip'):'';
		$data['mesin_key']        	= ($this->input->post('mesin_key'))?$this->input->post('mesin_key'):'';
		$data['mesin_nama']        	= ($this->input->post('mesin_nama'))?$this->input->post('mesin_nama'):'';
		$data['mesin_status']		= ($this->input->post('mesin_status'))?$this->input->post('mesin_status'):'A';
		$data['mesin_jari']			= ($this->input->post('mesin_jari'))?$this->input->post('mesin_jari'):'0';
		$data['mesin_log']			= ($this->input->post('mesin_log'))?$this->input->post('mesin_log'):'0';
		$data['mesin_info']			= ($this->input->post('mesin_info'))?$this->input->post('mesin_info'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$insert['mesin_id'] 		= $this->uuid->v4();
			$insert['mesin_ip'] 		= $this->input->post('mesin_ip');
			$insert['mesin_key'] 		= $this->input->post('mesin_key');
			$insert['mesin_nama'] 		= $this->input->post('mesin_nama');
			$insert['mesin_status'] 	= $this->input->post('mesin_status');
			$insert['mesin_jari'] 		= $this->input->post('mesin_jari');
			$insert['mesin_log'] 		= $this->input->post('mesin_log');
			$insert['mesin_info'] 		= $this->input->post('mesin_info');
            $this->Mesin_model->insert_mesin($insert);
			
			$this->session->set_flashdata('success','Mesin telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mesin', $data);
		$this->load->view(module_dir().'/separate/foot');
    }

	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
        
        $where['mesin_id']		= validasi_sql($this->uri->segment(4)); 
		$mesin 			= $this->Mesin_model->get_mesin('*', $where);
        
		$data['mesin_id']		= ($this->input->post('mesin_id'))?$this->input->post('mesin_id'):$mesin->mesin_id;
		$data['mesin_ip']		= ($this->input->post('mesin_ip'))?$this->input->post('mesin_ip'):$mesin->mesin_ip;
		$data['mesin_key']		= ($this->input->post('mesin_key'))?$this->input->post('mesin_key'):$mesin->mesin_key;
		$data['mesin_nama']		= ($this->input->post('mesin_nama'))?$this->input->post('mesin_nama'):$mesin->mesin_nama;
		$data['mesin_status']	= ($this->input->post('mesin_status'))?$this->input->post('mesin_status'):$mesin->mesin_status;
		$data['mesin_jari']		= ($this->input->post('mesin_jari'))?$this->input->post('mesin_jari'):$mesin->mesin_jari;
		$data['mesin_log']		= ($this->input->post('mesin_log'))?$this->input->post('mesin_log'):$mesin->mesin_log;
		$data['mesin_info']		= ($this->input->post('mesin_info'))?$this->input->post('mesin_info'):$mesin->mesin_info;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_update['mesin_id']	= validasi_sql($this->input->post('mesin_id'));
			$update['mesin_ip'] 		= $this->input->post('mesin_ip');
			$update['mesin_key'] 		= $this->input->post('mesin_key');
			$update['mesin_nama'] 		= $this->input->post('mesin_nama');
			$update['mesin_status'] 	= $this->input->post('mesin_status');
			$update['mesin_jari'] 		= $this->input->post('mesin_jari');
			$update['mesin_log'] 		= $this->input->post('mesin_log');
			$update['mesin_info'] 		= $this->input->post('mesin_info');
            $this->Mesin_model->update_mesin($where_update, $update);
			
			$this->session->set_flashdata('success','Mesin telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mesin', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$mesin_id = validasi_sql($this->uri->segment(4));
		$where_delete['mesin_id']	= $mesin_id;
		$mesin = $this->Mesin_model->get_mesin("", $where_delete);
		if ($this->Sidik_mesin_model->count_all_sidik_mesin(array('sidik_mesin.mesin_id'=>$mesin_id)) < 1 && $mesin){
			$this->Mesin_model->delete_mesin($where_delete);

			$this->session->set_flashdata('success','Mesin telah berhasil dihapus.');
			redirect(module_url($this->uri->segment(2)));
		} else {
			$this->session->set_flashdata('error','Mesin gagal dihapus. Data digunakan oleh sidik jari.');
			redirect(module_url($this->uri->segment(2)));
		}
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['mesin_id']	= validasi_sql($this->uri->segment(4)); 
		$mesin		= $this->Mesin_model->get_mesin('*', $where);
		
		$data['mesin_id']		= $mesin->mesin_id;
		$data['mesin_nama']		= $mesin->mesin_nama;
		$data['mesin_ip']		= $mesin->mesin_ip;
		$data['mesin_key']		= $mesin->mesin_key;
		$data['mesin_status']	= $mesin->mesin_status;
		$data['mesin_jari']		= $mesin->mesin_jari;
		$data['mesin_log']		= $mesin->mesin_log;
		$data['mesin_info']		= $mesin->mesin_info;

		$jumlah_jari = 0;
		if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
			$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
			if($Connect){
				$soap_request="<GetUserTemplate><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN><FingerID xsi:type=\"xsd:integer\">All</FingerID></Arg></GetUserTemplate>";
				$newLine="\r\n";
				fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($Connect, "Content-Type: text/xml".$newLine);
				fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($Connect, $soap_request.$newLine);
				$buffer="";
				while($Response=fgets($Connect, 1024)){
					$buffer=$buffer.$Response;
				}
			}
			$buffer=Parse_Data($buffer,"<GetUserTemplateResponse>","</GetUserTemplateResponse>");
			$buffer=explode("\r\n",$buffer);
			for($a=0;$a<count($buffer);$a++){
				$data_=Parse_Data($buffer[$a],"<Row>","</Row>");
				if ($data_){
					$jumlah_jari++;
				}
			}
		}

		$jumlah_log = 0;
		if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
			$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
			if($Connect){
				$soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
				$newLine="\r\n";
				fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($Connect, "Content-Type: text/xml".$newLine);
				fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($Connect, $soap_request.$newLine);
				$buffer="";
				while($Response=fgets($Connect, 1024)){
					$buffer=$buffer.$Response;
				}
			}
			$buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
			$buffer=explode("\r\n",$buffer);
			for($a=0;$a<count($buffer);$a++){
				$data_=Parse_Data($buffer[$a],"<Row>","</Row>");
				if ($data_){
					$jumlah_log++;
				}
			}
		}

		$this->Mesin_model->update_mesin(array('mesin_id'=>$mesin->mesin_id), array('mesin_sinkronisasi'=>($jumlah_log + 1)));

		$data['jumlah_jari']	= rupiah2($jumlah_jari);
		$data['jumlah_log']		= rupiah2($jumlah_log);
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mesin', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete_log()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$mesin_id = validasi_sql($this->uri->segment(4));
		$where_delete['mesin_id']	= $mesin_id;
		$mesin = $this->Mesin_model->get_mesin("", $where_delete);
		$status = "";
		if ($mesin->mesin_ip && ping($mesin->mesin_ip) == "OK"){
			$Connect = fsockopen($mesin->mesin_ip, "80", $errno, $errstr, 1);
			if($Connect){
				$soap_request="<ClearData><ArgComKey xsi:type=\"xsd:integer\">".$mesin->mesin_key."</ArgComKey><Arg><Value xsi:type=\"xsd:integer\">3</Value></Arg></ClearData>";
				$newLine="\r\n";
				fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($Connect, "Content-Type: text/xml".$newLine);
				fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($Connect, $soap_request.$newLine);
				$buffer="";
				while($Response=fgets($Connect, 1024)){
					$buffer=$buffer.$Response;
				}
			}

			$status = Parse_Data($buffer,"<Information>","</Information>");
		}

		if ($status == "Successfully!"){
			$this->Mesin_model->update_mesin(array('mesin_id'=>$mesin->mesin_id), array('mesin_sinkronisasi'=>0));
			$this->session->set_flashdata('success','Log berhasil dihapus.');
			redirect(module_url($this->uri->segment(2).'/detail/'.$mesin_id));
		} else {
			$this->session->set_flashdata('error','Log Gagal di Hapus');
			redirect(module_url($this->uri->segment(2).'/detail/'.$mesin_id));
		}
	}
}
