<?php
class Libur_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel libur
	public function insert_libur($data){
        return $this->db->insert("sat_libur",$data);
    }
    
    public function update_libur($where,$data){
        return $this->db->update("sat_libur",$data,$where);
    }

    public function delete_libur($where){
        return $this->db->delete("sat_libur", $where);
    }

	public function get_libur($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("sat_libur libur");
		if ($where){$this->db->where($where);}
		$this->db->order_by('libur_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_libur($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("sat_libur libur");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_libur($where="", $like=""){
        $this->db->select("libur.libur_id");
        $this->db->from("sat_libur libur");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file libur_model.php */
/* Location: ./application/models/libur_model.php */