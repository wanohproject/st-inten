<?php
class Sidik_mesin_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel sidik_mesin
	public function insert_sidik_mesin($sidik_mesin){
        return $this->db->insert("sat_sidik_mesin",$sidik_mesin);
    }
    
    public function update_sidik_mesin($where,$sidik_mesin){
        return $this->db->update("sat_sidik_mesin",$sidik_mesin,$where);
    }

    public function delete_sidik_mesin($where){
        return $this->db->delete("sat_sidik_mesin", $where);
    }

	public function get_sidik_mesin($select, $where=""){
        $sidik_mesin = "";
		$this->db->select($select);
        $this->db->from("sat_sidik_mesin sidik_mesin");
		$this->db->join("sat_sidik_jari sidik_jari", "sidik_mesin.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		$this->db->join("sat_mesin mesin", "sidik_mesin.mesin_id=mesin.mesin_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('sidik_mesin.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$sidik_mesin = $Q->row();
		}
		$Q->free_result();
		return $sidik_mesin;
	}

    public function grid_all_sidik_mesin($select, $sidx, $sord, $limit="", $start="", $where="", $like="", $group_by=""){
        $sidik_mesin = "";
        $this->db->select($select);
        $this->db->from("sat_sidik_mesin sidik_mesin");
		$this->db->join("sat_sidik_jari sidik_jari", "sidik_mesin.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		$this->db->join("sat_mesin mesin", "sidik_mesin.mesin_id=mesin.mesin_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
        }
        if ($group_by){$this->db->order_by($group_by);}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $sidik_mesin=$Q->result();
        }
        $Q->free_result();
        return $sidik_mesin;
    }

    public function count_all_sidik_mesin($where="", $like=""){
        $this->db->select("sidik_mesin.sidik_mesin_id");
        $this->db->from("sat_sidik_mesin sidik_mesin");
		$this->db->join("sat_sidik_jari sidik_jari", "sidik_mesin.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		$this->db->join("sat_mesin mesin", "sidik_mesin.mesin_id=mesin.mesin_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $sidik_mesin = $Q->num_rows();
        return $sidik_mesin;
    }
}

/* End of file sidik_mesin_model.php */
/* Location: ./application/models/sidik_mesin_model.php */