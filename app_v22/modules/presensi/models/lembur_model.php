<?php
class Lembur_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel lembur
	public function insert_lembur($lembur){
        return $this->db->insert("sat_lembur",$lembur);
    }
    
    public function update_lembur($where,$lembur){
        return $this->db->update("sat_lembur",$lembur,$where);
    }

    public function delete_lembur($where){
        return $this->db->delete("sat_lembur", $where);
    }

	public function get_lembur($select, $where=""){
        $lembur = "";
		$this->db->select($select);
        $this->db->from("sat_lembur lembur");
		$this->db->join("staf", "lembur.staf_id=staf.staf_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('lembur.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$lembur = $Q->row();
		}
		$Q->free_result();
		return $lembur;
	}

    public function grid_all_lembur($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $lembur = "";
        $this->db->select($select);
        $this->db->from("sat_lembur lembur");
		$this->db->join("staf", "lembur.staf_id=staf.staf_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $lembur=$Q->result();
        }
        $Q->free_result();
        return $lembur;
    }

    public function count_all_lembur($where="", $like=""){
        $this->db->select("lembur.lembur_id");
        $this->db->from("sat_lembur lembur");
		$this->db->join("staf", "lembur.staf_id=staf.staf_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $lembur = $Q->num_rows();
        return $lembur;
    }
}

/* End of file lembur_model.php */
/* Location: ./application/models/lembur_model.php */