<?php
class Cuti_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel cuti
	public function insert_cuti($cuti){
        return $this->db->insert("sat_cuti",$cuti);
    }
    
    public function update_cuti($where,$cuti){
        return $this->db->update("sat_cuti",$cuti,$where);
    }

    public function delete_cuti($where){
        return $this->db->delete("sat_cuti", $where);
    }

	public function get_cuti($select, $where=""){
        $cuti = "";
		$this->db->select($select);
        $this->db->from("sat_cuti cuti");
		$this->db->join("staf", "cuti.staf_id=staf.staf_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('cuti.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$cuti = $Q->row();
		}
		$Q->free_result();
		return $cuti;
	}

    public function grid_all_cuti($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $cuti = "";
        $this->db->select($select);
        $this->db->from("sat_cuti cuti");
		$this->db->join("staf", "cuti.staf_id=staf.staf_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $cuti=$Q->result();
        }
        $Q->free_result();
        return $cuti;
    }

    public function count_all_cuti($where="", $like=""){
        $this->db->select("cuti.cuti_id");
        $this->db->from("sat_cuti cuti");
		$this->db->join("staf", "cuti.staf_id=staf.staf_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $cuti = $Q->num_rows();
        return $cuti;
    }
}

/* End of file cuti_model.php */
/* Location: ./application/models/cuti_model.php */