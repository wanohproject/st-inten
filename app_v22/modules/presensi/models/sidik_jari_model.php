<?php
class Sidik_jari_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel sidik_jari
	public function insert_sidik_jari($sidik_jari){
        return $this->db->insert("sat_sidik_jari",$sidik_jari);
    }
    
    public function update_sidik_jari($where,$sidik_jari){
        return $this->db->update("sat_sidik_jari",$sidik_jari,$where);
    }

    public function delete_sidik_jari($where){
        return $this->db->delete("sat_sidik_jari", $where);
    }

	public function get_sidik_jari($select, $where=""){
        $sidik_jari = "";
		$this->db->select($select);
        $this->db->from("sat_sidik_jari sidik_jari");
		$this->db->join("staf", "sidik_jari.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "sidik_jari.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('sidik_jari.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$sidik_jari = $Q->row();
		}
		$Q->free_result();
		return $sidik_jari;
	}

    public function grid_all_sidik_jari($select, $sidx, $sord, $limit="", $start="", $where="", $like="", $group_by=""){
        $sidik_jari = "";
        $this->db->select($select);
        $this->db->from("sat_sidik_jari sidik_jari");
		$this->db->join("staf", "sidik_jari.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "sidik_jari.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
        }
        if ($group_by){$this->db->order_by($group_by);}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $sidik_jari=$Q->result();
        }
        $Q->free_result();
        return $sidik_jari;
    }

    public function count_all_sidik_jari($where="", $like=""){
        $this->db->select("sidik_jari.sidik_jari_id");
        $this->db->from("sat_sidik_jari sidik_jari");
		$this->db->join("staf", "sidik_jari.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "sidik_jari.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $sidik_jari = $Q->num_rows();
        return $sidik_jari;
    }
}

/* End of file sidik_jari_model.php */
/* Location: ./application/models/sidik_jari_model.php */