<?php
class Sidik_kartu_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel sidik_kartu
	public function insert_sidik_kartu($sidik_kartu){
        return $this->db->insert("sat_sidik_kartu",$sidik_kartu);
    }
    
    public function update_sidik_kartu($where,$sidik_kartu){
        return $this->db->update("sat_sidik_kartu",$sidik_kartu,$where);
    }

    public function delete_sidik_kartu($where){
        return $this->db->delete("sat_sidik_kartu", $where);
    }

	public function get_sidik_kartu($select, $where=""){
        $sidik_kartu = "";
		$this->db->select($select);
        $this->db->from("sat_sidik_kartu sidik_kartu");
		$this->db->join("sat_sidik_jari sidik_jari", "sidik_kartu.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('sidik_kartu.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$sidik_kartu = $Q->row();
		}
		$Q->free_result();
		return $sidik_kartu;
	}

    public function grid_all_sidik_kartu($select, $sidx, $sord, $limit="", $start="", $where="", $like="", $group_by=""){
        $sidik_kartu = "";
        $this->db->select($select);
        $this->db->from("sat_sidik_kartu sidik_kartu");
		$this->db->join("sat_sidik_jari sidik_jari", "sidik_kartu.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
        }
        if ($group_by){$this->db->order_by($group_by);}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $sidik_kartu=$Q->result();
        }
        $Q->free_result();
        return $sidik_kartu;
    }

    public function count_all_sidik_kartu($where="", $like=""){
        $this->db->select("sidik_kartu.sidik_kartu_id");
        $this->db->from("sat_sidik_kartu sidik_kartu");
		$this->db->join("sat_sidik_jari sidik_jari", "sidik_kartu.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $sidik_kartu = $Q->num_rows();
        return $sidik_kartu;
    }
}

/* End of file sidik_kartu_model.php */
/* Location: ./application/models/sidik_kartu_model.php */