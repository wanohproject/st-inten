<?php
class Mesin_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel mesin
	public function insert_mesin($data){
        return $this->db->insert("sat_mesin",$data);
    }
    
    public function update_mesin($where,$data){
        return $this->db->update("sat_mesin",$data,$where);
    }

    public function delete_mesin($where){
        return $this->db->delete("sat_mesin", $where);
    }

	public function get_mesin($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("sat_mesin mesin");
		if ($where){$this->db->where($where);}
		$this->db->order_by('mesin_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_mesin($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("sat_mesin mesin");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_mesin($where="", $like=""){
        $this->db->select("mesin.mesin_id");
        $this->db->from("sat_mesin mesin");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file mesin_model.php */
/* Location: ./application/models/mesin_model.php */