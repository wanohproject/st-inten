<?php
class Manual_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel manual
	public function insert_manual($data){
        return $this->db->insert("sat_manual",$data);
    }
    
    public function update_manual($where,$data){
        return $this->db->update("sat_manual",$data,$where);
    }

    public function delete_manual($where){
        return $this->db->delete("sat_manual", $where);
    }

	public function get_manual($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("sat_manual manual");
		$this->db->join("staf", "manual.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "manual.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('manual.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_manual($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("sat_manual manual");
		$this->db->join("staf", "manual.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "manual.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        // $this->db->group_by("manual.manual_user");
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_manual($where="", $like=""){
        $this->db->select("manual.manual_id");
        $this->db->from("sat_manual manual");
		$this->db->join("staf", "manual.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "manual.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->group_by("manual.manual_user");
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file manual_model.php */
/* Location: ./application/models/manual_model.php */