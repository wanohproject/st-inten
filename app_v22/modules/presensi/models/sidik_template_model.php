<?php
class Sidik_template_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel sidik_template
	public function insert_sidik_template($sidik_template){
        return $this->db->insert("sat_sidik_template",$sidik_template);
    }
    
    public function update_sidik_template($where,$sidik_template){
        return $this->db->update("sat_sidik_template",$sidik_template,$where);
    }

    public function delete_sidik_template($where){
        return $this->db->delete("sat_sidik_template", $where);
    }

	public function get_sidik_template($select, $where=""){
        $sidik_template = "";
		$this->db->select($select);
        $this->db->from("sat_sidik_template sidik_template");
		$this->db->join("(SELECT * FROM sat_sidik_jari sj GROUP BY sidik_jari_user) sidik_jari", "sidik_template.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		$this->db->join("sat_finger finger", "sidik_template.finger_id=finger.finger_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('sidik_template_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$sidik_template = $Q->row();
		}
		$Q->free_result();
		return $sidik_template;
	}

    public function grid_all_sidik_template($select, $sidx, $sord, $limit="", $start="", $where="", $like="", $group_by=""){
        $sidik_template = "";
        $this->db->select($select);
        $this->db->from("sat_sidik_template sidik_template");
		$this->db->join("(SELECT * FROM sat_sidik_jari sj GROUP BY sidik_jari_user) sidik_jari", "sidik_template.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		$this->db->join("sat_finger finger", "sidik_template.finger_id=finger.finger_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
        }
        if ($group_by){$this->db->order_by($group_by);}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $sidik_template=$Q->result();
        }
        $Q->free_result();
        return $sidik_template;
    }

    public function count_all_sidik_template($where="", $like=""){
        $this->db->select("sidik_template.sidik_template_id");
        $this->db->from("sat_sidik_template sidik_template");
		$this->db->join("(SELECT * FROM sat_sidik_jari sj GROUP BY sidik_jari_user) sidik_jari", "sidik_template.sidik_jari_id=sidik_jari.sidik_jari_id", "left");
		$this->db->join("sat_finger finger", "sidik_template.finger_id=finger.finger_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $sidik_template = $Q->num_rows();
        return $sidik_template;
    }
}

/* End of file sidik_template_model.php */
/* Location: ./application/models/sidik_template_model.php */