<?php
class Jadwal_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel jadwal
	public function insert_jadwal($data){
        return $this->db->insert("sat_jadwal",$data);
    }
    
    public function update_jadwal($where,$data){
        return $this->db->update("sat_jadwal",$data,$where);
    }

    public function delete_jadwal($where){
        return $this->db->delete("sat_jadwal", $where);
    }

	public function get_jadwal($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("sat_jadwal jadwal");
		if ($where){$this->db->where($where);}
		$this->db->order_by('jadwal_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_jadwal($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("sat_jadwal jadwal");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_jadwal($where="", $like=""){
        $this->db->select("jadwal.jadwal_id");
        $this->db->from("sat_jadwal jadwal");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file jadwal_model.php */
/* Location: ./application/models/jadwal_model.php */