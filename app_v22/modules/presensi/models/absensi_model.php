<?php
class Absensi_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel absensi
	public function insert_absensi($absensi){
        return $this->db->insert("sat_absensi",$absensi);
    }
    
    public function update_absensi($where,$absensi){
        return $this->db->update("sat_absensi",$absensi,$where);
    }

    public function delete_absensi($where){
        return $this->db->delete("sat_absensi", $where);
    }

	public function get_absensi($select, $where=""){
        $absensi = "";
		$this->db->select($select);
        $this->db->from("sat_absensi absensi");
		$this->db->join("staf", "absensi.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "absensi.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('absensi.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$absensi = $Q->row();
		}
		$Q->free_result();
		return $absensi;
	}

    public function grid_all_absensi($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $absensi = "";
        $this->db->select($select);
        $this->db->from("sat_absensi absensi");
		$this->db->join("staf", "absensi.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "absensi.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $absensi=$Q->result();
        }
        $Q->free_result();
        return $absensi;
    }

    public function count_all_absensi($where="", $like=""){
        $this->db->select("absensi.absensi_id");
        $this->db->from("sat_absensi absensi");
		$this->db->join("staf", "absensi.staf_id=staf.staf_id", "left");
		$this->db->join("siswa", "absensi.siswa_id=siswa.siswa_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $absensi = $Q->num_rows();
        return $absensi;
    }
}

/* End of file absensi_model.php */
/* Location: ./application/models/absensi_model.php */