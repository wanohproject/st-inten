<?php
class Presensi_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel presensi
	public function insert_presensi($presensi){
        return $this->db->insert("sat_presensi",$presensi);
    }
    
    public function update_presensi($where,$presensi){
        return $this->db->update("sat_presensi",$presensi,$where);
    }

    public function delete_presensi($where){
        return $this->db->delete("sat_presensi", $where);
    }

	public function get_presensi($select, $where=""){
        $presensi = "";
		$this->db->select($select);
        $this->db->from("sat_presensi presensi");
		$this->db->join("sat_sidik_jari sidik_jari", "presensi.presensi_user=sidik_jari.sidik_jari_user", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('presensi.created_at','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$presensi = $Q->row();
		}
		$Q->free_result();
		return $presensi;
	}

    public function grid_all_presensi($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $presensi = "";
        $this->db->select($select);
        $this->db->from("sat_presensi presensi");
		$this->db->join("sat_sidik_jari sidik_jari", "presensi.presensi_user=sidik_jari.sidik_jari_user", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $presensi=$Q->result();
        }
        $Q->free_result();
        return $presensi;
    }

    public function count_all_presensi($where="", $like=""){
        $this->db->select("presensi.presensi_id");
        $this->db->from("sat_presensi presensi");
		$this->db->join("sat_sidik_jari sidik_jari", "presensi.presensi_user=sidik_jari.sidik_jari_user", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $Q=$this->db->get();
        $presensi = $Q->num_rows();
        return $presensi;
    }
}

/* End of file presensi_model.php */
/* Location: ./application/models/presensi_model.php */