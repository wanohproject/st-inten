<?php
class Log_model extends CI_Model  {

    public function __contsruct(){
        parent::Model();
    }
	
	// konfigurasi tabel log
	public function insert_log($data){
        return $this->db->insert("sat_log",$data);
    }
    
    public function update_log($where,$data){
        return $this->db->update("sat_log",$data,$where);
    }

    public function delete_log($where){
        return $this->db->delete("sat_log", $where);
    }

	public function get_log($select, $where=""){
        $data = "";
		$this->db->select($select);
        $this->db->from("sat_log log");
		$this->db->join("sat_mesin mesin", "log.mesin_id=mesin.mesin_id", "left");
		$this->db->join("sat_sidik_jari sidik_jari", "log.user_id=sidik_jari.user_id", "left");
		if ($where){$this->db->where($where);}
		$this->db->order_by('log_id','DESC');
		$this->db->limit(1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0){
			$data = $Q->row();
		}
		$Q->free_result();
		return $data;
	}

    public function grid_all_log($select, $sidx, $sord, $limit="", $start="", $where="", $like=""){
        $data = "";
        $this->db->select($select);
        $this->db->from("sat_log log");
		$this->db->join("sat_mesin mesin", "log.mesin_id=mesin.mesin_id", "left");
		$this->db->join("sat_sidik_jari sidik_jari", "log.user_id=sidik_jari.user_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->order_by($sidx,$sord);
        if (!empty($limit)) {$this->db->limit($limit,$start);}
        $Q = $this->db->get();
        if ($Q->num_rows() > 0){
            $data=$Q->result();
        }
        $Q->free_result();
        return $data;
    }

    public function count_all_log($where="", $like=""){
        $this->db->select("log.log_id");
        $this->db->from("sat_log log");
		$this->db->join("sat_mesin mesin", "log.mesin_id=mesin.mesin_id", "left");
		$this->db->join("sat_sidik_jari sidik_jari", "log.user_id=sidik_jari.user_id", "left");
		if ($where){$this->db->where($where);}
		if ($like){
			foreach($like as $key => $value){ 
			$this->db->like($key, $value); 
			}
		}
        $this->db->group_by("sidik_jari.user_id");
        $Q=$this->db->get();
        $data = $Q->num_rows();
        return $data;
    }
}

/* End of file log_model.php */
/* Location: ./application/models/log_model.php */