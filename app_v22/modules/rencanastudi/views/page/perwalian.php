<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('perwalian/datatable/'.$program_studi_id.'/'.$tahun_kode.'/'.$semester_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "mahasiswa_tahun_masuk"},
			{ "data": "mahasiswa_nim"},
			{ "data": "mahasiswa_nama"},
			{ "data": "status_perwalian"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1, -2]
		}, {
			"searchable": false,
			"targets": [-1, -2]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
					<?php if (settings_get_value('rencana_studi_active') == 'RS'){ ?>
            Perwalian <small><span class="label label-success">Rencana Studi dan Perwalian Aktif</span></small>
						<?php } else { ?>
            Perwalian <small><span class="label label-danger">Rencana Studi dan Perwalian Tidak Aktif</span></small>
						<?php } ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Perwalian</a></li>
            <li class="active">Daftar Perwalian</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Perwalian</h3>
                </div><!-- /.box-header -->
				<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$tahun_kode.'/'.$semester_kode);?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="program_studi_id" class="control-label">Program Studi</label>
								<div class="">
									<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="tingkat_id" class="control-label">Tahun Masuk</label>
								<div class="">	
									<?php combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_kode', 'DESC'), 'tahun_kode', 'tahun_kode', 'tahun_kode', $tahun_kode, 'submit();', '-- PILIH TAHUN -- ', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="semester_kode" class="control-label">Semester</label>
								<div class="">
									<?php combobox('db', $this->Semester_model->grid_all_semester('', 'semester_kode', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', '-- PILIH SEMESTER --', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
					</div>
				</div>
				</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width:90px;">Tahun Masuk</th>
                        <th style="width:90px;">NIM</th>
                        <th>Nama</th>
                        <th style="width:110px;">Status Perwalian</th>
                        <th style="width:60px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<style>
.direct-chat-text {
    margin-right: 15%;
    margin-left: 10px;
}
.right .direct-chat-text {
    margin-right: 10px;
    margin-left: 15%;
		float: right;
}

.right .direct-chat-timestamp {
    margin-right: 10px;
    margin-left: 0;
}

.direct-chat-timestamp {
	font-style: italic;
	color: #DEDEDE;
}
</style>
<script>
  $(function () {

		<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
			<?php if ($this->session->flashdata('success')) { ?>
				$('#successModal').modal('show');
			<?php } else { ?>
				$('#errorModal').modal('show');
			<?php } ?>
		<?php } ?>
		
		$( "#btn_refresh_catatan" ).click(function() {
			loadCatatan();
		});

		loadCatatan();
  });

function loadCatatan(){
	var params = {
		program_studi: '<?php echo $perwalian->program_studi_id; ?>',
		semester: '<?php echo $perwalian->semester_kode; ?>',
		mahasiswa: '<?php echo $perwalian->mahasiswa_id; ?>'
	};
	
	$.ajax({
		url: "<?php echo module_url($this->uri->segment(2).'/get-catatan'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				var last = "";
				for(var i = 0;i < data.data.length;i++){
					console.log(data.data[i]);
					if (data.data[i].perwalian_catatan_jenis == "Mahasiswa"){
						innerHTML += "<div class=\"direct-chat-msg right\"><div class=\"direct-chat-text\"><span class=\"direct-chat-name\">Mahasiswa<em> - " + data.data[i].mahasiswa_nama + "</em></span><br />" + data.data[i].perwalian_catatan_isi + " - <span class=\"direct-chat-timestamp\">" + data.data[i].created_at + "</span></div></div>";
					} else if (data.data[i].perwalian_catatan_jenis == "Dosen"){
						innerHTML += "<div class=\"direct-chat-msg\"><div class=\"direct-chat-text\"><span class=\"direct-chat-name\">Dosen Wali<em> - " + data.data[i].dosen_nama + "</em></span><br />" + data.data[i].perwalian_catatan_isi + " - <span class=\"direct-chat-timestamp\">" + data.data[i].created_at + "</span></div></div>";						
					}
					last = data.data[i].perwalian_catatan_jenis;
				}
			} else {
				innerHTML = "";
			}
			$("#direct-chat-messages").html(innerHTML);
			if ($('#direct-chat-messages').length > 0) {
				$('#direct-chat-messages').scrollTop($('#direct-chat-messages')[0].scrollHeight - $('#direct-chat-messages')[0].clientHeight);
		}
		},
	});
}
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Perwalian
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Perwalian</a></li>
            <li class="active">Detail Perwalian</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
					<div class="row">
            <div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
                  <h3 class="box-title">Form Perwalian</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<div class="col-md-9">
											<div class="row">
												<label for="mahasiswa_nim" class="col-xs-4 col-md-2 control-label" style="text-align:left;">NIM</label>
												<div class="col-xs-8 col-md-4">
													<label for="mahasiswa_nim" style="text-align:left;">: <?php echo $perwalian->mahasiswa_nim; ?></label>
												</div>
												<label for="mahasiswa_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Nama</label>
												<div class="col-xs-8 col-md-4">
													<label for="mahasiswa_nim" style="text-align:left;">: <?php echo $perwalian->mahasiswa_nama; ?></label>
												</div>
											</div>
											<div class="row">
												<label for="program_studi_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Program Studi</label>
												<div class="col-xs-8 col-md-4">
													<label for="program_studi_nama" style="text-align:left;">: <?php echo $perwalian->program_studi_nama; ?></label>
												</div>
												<label for="mahasiswa_tahun_masuk" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Angkatan</label>
												<div class="col-xs-8 col-md-4">
													<label for="mahasiswa_tahun_masuk" style="text-align:left;">: <?php echo $perwalian->mahasiswa_tahun_masuk; ?></label>
												</div>
											</div>
											<div class="row">
												<label for="semester_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Periode</label>
												<div class="col-xs-8 col-md-4">
													<label for="semester_nama" style="text-align:left;">: <?php echo $perwalian->semester_nama; ?></label>										
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="row">
												<label for="mahasiswa_nim" class="col-xs-12 control-label" style="text-align:left;"><?php echo ($perwalian->perwalian_submit_status == "Y")?'<span><i class="fa fa-check-circle text-success"></i></span>':'<span><i class="fa fa-times-circle text-danger"></i></span>'; ?> Submit Siswa</label>
											</div>
											<div class="row">
												<label for="mahasiswa_nim" class="col-xs-12 control-label" style="text-align:left;"><?php echo ($perwalian->approve_wali_status == "Y")?'<span><i class="fa fa-check-circle text-success"></i></span>':'<span><i class="fa fa-times-circle text-danger"></i></span>'; ?> Persetujuan Dosen Wali</label>
											</div>
											<div class="row">
												<label for="mahasiswa_nim" class="col-xs-12 control-label" style="text-align:left;"><?php echo ($perwalian->approve_keuangan_status == "Y")?'<span><i class="fa fa-check-circle text-success"></i></span>':'<span><i class="fa fa-times-circle text-danger"></i></span>'; ?> Persetujuan Keuangan</label>
											</div>
											<div class="row">
												<label for="mahasiswa_nim" class="col-xs-12 control-label" style="text-align:left;"><?php echo ($perwalian->approve_akademik_status == "Y")?'<span><i class="fa fa-check-circle text-success"></i></span>':'<span><i class="fa fa-times-circle text-danger"></i></span>'; ?> Persetujuan Akademik</label>
											</div>
										</div>
									</div>
								</div>
								<div class="box-header with-border">
									<h3 class="box-title">Daftar Mata Kuliah</h3>
									<?php if (settings_get_value('rencana_studi_active') == 'RS'){?>
									<div class="pull-right">
										<a href="<?php echo module_url($this->uri->segment(2).'/add_matakuliah/'.$perwalian->perwalian_id); ?>"><span class="fa fa-plus"></span> Tambah Mata Kuliah</a>
									</div>
									<?php } ?>
								</div><!-- /.box-header -->
								<div class="box-body">
									<table id="datagrid_krs" class="table table-bordered table-striped" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th width="100">Kode</th>
												<th>MATA KULIAH</th>
												<th width="100">JUMLAH SKS</th>
												<th width="50">&nbsp;</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$matakuliah = $this->db->query("SELECT akd_matakuliah.*, akd_perwalian_matakuliah.perwalian_matakuliah_id FROM akd_perwalian_matakuliah LEFT JOIN akd_matakuliah ON akd_perwalian_matakuliah.matakuliah_id=akd_matakuliah.matakuliah_id WHERE akd_perwalian_matakuliah.perwalian_id = '$perwalian->perwalian_id' ORDER BY matakuliah_kode ASC")->result();
										if ($matakuliah){
											foreach ($matakuliah as $row) {
												$count_matakuliah_nilai = $this->db->query("SELECT akd_matakuliah.* FROM akd_perwalian_matakuliah LEFT JOIN akd_matakuliah ON akd_perwalian_matakuliah.matakuliah_id=akd_matakuliah.matakuliah_id WHERE akd_perwalian_matakuliah.perwalian_id = '$perwalian->perwalian_id' AND akd_perwalian_matakuliah.matakuliah_id = '$row->matakuliah_id'")->num_rows()
												?>
												<tr>
													<td><?php echo $row->matakuliah_kode; ?></td>
													<td><?php echo $row->matakuliah_nama; ?></td>
													<td class="text-center"><?php echo $row->matakuliah_sks; ?></td>
													<?php if (settings_get_value('rencana_studi_active') == 'RS'){?>
														<?php if ($perwalian->approve_akademik_status == 'N'){ ?>
															<td class="text-center"><a href="<?php echo module_url($this->uri->segment(2) . '/delete_krs/' . $row->perwalian_matakuliah_id) ?>" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm('Apakah Anda yakin? \nAkan menghapus data ini.');"><i class="fa fa-trash-o"></i></a></td>
														<?php } else { ?>
															<td class="text-center">&nbsp;</td>
														<?php } ?>
													<?php } else { ?>
														<td class="text-center">&nbsp;</td>
													<?php } ?>
												</tr>
												<?php
											}
										} else {
											?>
											<tr>
												<td colspan="4">Belum ada mata kuliah yang ditambahkan.</td>
											</tr>
											<?php
										}
										?>
										</tbody>
									</table>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
            </div><!-- /.col -->
						<div class="col-xs-12">
							<!-- DIRECT CHAT PRIMARY -->
							<div class="box box-primary direct-chat direct-chat-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Percakapan dengan Dosen Wali</h3>
									<div class="pull-right">
										<a href="javascript:void(0)" title="Klik untuk Refresh Percakapan" id="btn_refresh_catatan"><i class="fa fa-refresh"></i></a>
									</div>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<!-- Conversations are loaded here -->
									<div class="direct-chat-messages" id="direct-chat-messages" style="min-height:250px;height:auto;max-height:300px;overflow-y:scroll;">
									</div>
									<!--/.direct-chat-messages-->
								</div>
								<!-- /.box-body -->
							</div>
							<!--/.direct-chat -->
						</div>
						<!-- /.col -->
          </div><!-- /.row -->
					<div class="row">
            <div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<?php if (settings_get_value('rencana_studi_active') == 'RS'){?>
										<?php if ($perwalian->approve_akademik_status == 'Y'){ ?>
										<a href="<?php echo module_url($this->uri->segment(2).'/cancel/'.$perwalian->perwalian_id); ?>?approve=akademik" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin akan batal submit KRS?')">Batal Approve Akademik</a>
										<?php } else if ($perwalian->approve_akademik_status == 'N'){ ?>
										<a href="<?php echo module_url($this->uri->segment(2).'/submit/'.$perwalian->perwalian_id); ?>?approve=akademik" class="btn btn-success" onclick="return confirm('Apakah Anda yakin akan submit KRS?')">Approve Akademik</a>
										<?php } ?>
									<?php } ?>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$perwalian->program_studi_id.'/'.$perwalian->mahasiswa_tahun_masuk.'/'.$perwalian->semester_kode); ?>'" class="btn btn-default">Kembali</button>
								</div><!-- /.box-footer -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'add_matakuliah') {?>
<script>
  $(function () {
		$( "a.btn-add" ).click(function() {
			var data_id = $(this).attr("data-id");
			var data_action = $(this).attr("data-action");
			var params = {
				action: data_action,
				matakuliah: data_id,
				mahasiswa: '<?php echo $perwalian->mahasiswa_id; ?>',
				perwalian: '<?php echo $perwalian->perwalian_id; ?>'
			};

			if (data_action == 'delete'){
				if (confirm("Apakah yakin akan membatalkan mata kuliah tersebut?") == false) {
					return false;
				}
			}
			
			$.ajax({
				url: "<?php echo module_url($this->uri->segment(2).'/save_krs'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				beforeSend: function() {},
				success: function(data){
					if (data.response == true){
						if (data.status == 'add'){
							var btn_id = "#btn-" + data_id;
							$(btn_id).attr("data-action", "delete");
							$(btn_id).html("Batal");
							$(btn_id).removeClass("btn-success");
							$(btn_id).addClass("btn-danger");
						} else if (data.status == 'delete'){
							var btn_id = "#btn-" + data_id;
							$(btn_id).attr("data-action", "add");
							$(btn_id).html("Tambah");
							$(btn_id).addClass("btn-success");
							$(btn_id).removeClass("btn-danger");
						}
					}
				},
				complete: function() {},
			});
		});
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Perwalian
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Perwalian</a></li>
            <li class="active">Daftar Mata Kuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
					<div class="row">
            <div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-header with-border">
                  <h3 class="box-title">Daftar Mata Kuliah</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<table id="datagrid_krs" class="table table-bordered table-striped" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th width="150">KODE</th>
												<th>NAMA MATA KUIAH</th>
												<th width="100">JUMLAH SKS</th>
												<th width="150">&nbsp;</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$grid_semester = $this->db->query("SELECT akd_matakuliah.* FROM akd_matakuliah WHERE akd_matakuliah.program_studi_id = '$perwalian->program_studi_id' AND akd_matakuliah.semester_kode = '$perwalian->semester_kode' GROUP BY matakuliah_semester_no ORDER BY matakuliah_semester_no ASC")->result();
										if ($grid_semester){
											foreach ($grid_semester as $row_semester) {
												?>
												<tr>
													<th colspan="4">Semester <?php echo $row_semester->matakuliah_semester_no; ?></th>
												</tr>
												<?php
												$matakuliah = $this->db->query("SELECT akd_matakuliah.* FROM akd_matakuliah WHERE akd_matakuliah.program_studi_id = '$perwalian->program_studi_id' AND akd_matakuliah.semester_kode = '$perwalian->semester_kode' AND akd_matakuliah.matakuliah_semester_no = '$row_semester->matakuliah_semester_no' ORDER BY matakuliah_kode ASC")->result();
												if ($matakuliah){
													foreach ($matakuliah as $row) {
														$count_matakuliah_nilai = $this->db->query("SELECT akd_matakuliah.* FROM akd_perwalian_matakuliah LEFT JOIN akd_matakuliah ON akd_perwalian_matakuliah.matakuliah_id=akd_matakuliah.matakuliah_id WHERE akd_perwalian_matakuliah.perwalian_id = '$perwalian->perwalian_id' AND akd_perwalian_matakuliah.matakuliah_id = '$row->matakuliah_id'")->num_rows()
														?>
														<tr>
															<td><?php echo $row->matakuliah_kode; ?></td>
															<td><?php echo $row->matakuliah_nama; ?></td>
															<td class="text-center"><?php echo $row->matakuliah_sks; ?></td>
															<?php if (settings_get_value('rencana_studi_active') == 'RS'){?>
																<?php if ($perwalian->approve_akademik_status == 'N'){ ?>
																	<?php if ($count_matakuliah_nilai > 0){ ?>
																	<td class="text-center"><div class="box-btn box-btn-<?php echo $row->matakuliah_id; ?>"><a href="javascript:void(0);" data-id="<?php echo $row->matakuliah_id; ?>" data-action="delete" class="btn btn-sm btn-danger btn-add" id="btn-<?php echo $row->matakuliah_id; ?>">Batal</a></div></td>
																	<?php } else { ?>
																	<td class="text-center"><div class="box-btn box-btn-<?php echo $row->matakuliah_id; ?>"><a href="javascript:void(0);" data-id="<?php echo $row->matakuliah_id; ?>" data-action="add" class="btn btn-sm btn-success btn-add" id="btn-<?php echo $row->matakuliah_id; ?>">Tambah</a></div></td>
																	<?php } ?>
																<?php } else { ?>
																	<td class="text-center">&nbsp;</td>
																<?php } ?>
															<?php } else { ?>
																<td class="text-center">&nbsp;</td>
															<?php } ?>
														</tr>
														<?php
													}
												}
											}
										}
										?>
										</tbody>
									</table>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
					<div class="row">
            <div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/detail/'.$perwalian->perwalian_id); ?>'" class="btn btn-primary">Kembali ke Tampilan Perwalian</button>
								</div><!-- /.box-footer -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>