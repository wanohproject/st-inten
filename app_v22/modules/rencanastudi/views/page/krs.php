<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('krs/datatable/'.$program_studi_id.'/'.$tahun_kode.'/'.$semester_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "mahasiswa_tahun_masuk"},
			{ "data": "mahasiswa_nim"},
			{ "data": "mahasiswa_nama"},
			{ "data": "mahasiswa_kuliah_sks"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1, -2]
		}, {
			"searchable": false,
			"targets": [-1, -2]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Kartu Rencana Studi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Kartu Rencana Studi</a></li>
            <li class="active">Daftar Kartu Rencana Studi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Kartu Rencana Studi</h3>
                </div><!-- /.box-header -->
				<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$tahun_kode.'/'.$semester_kode);?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="program_studi_id" class="control-label">Program Studi</label>
								<div class="">
									<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="tingkat_id" class="control-label">Tahun Masuk</label>
								<div class="">	
									<?php combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_kode', 'DESC'), 'tahun_kode', 'tahun_kode', 'tahun_kode', $tahun_kode, 'submit();', '-- PILIH TAHUN -- ', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="semester_kode" class="control-label">Semester</label>
								<div class="">
									<?php combobox('db', $this->Semester_model->grid_all_semester('', 'semester_kode', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', '-- PILIH SEMESTER --', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
					</div>
				</div>
				</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width:90px;">Tahun Masuk</th>
                        <th style="width:90px;">NIM</th>
                        <th>Nama</th>
                        <th style="width:110px;">Jumlah SKS</th>
                        <th style="width:60px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<style>
.direct-chat-text {
    margin-right: 15%;
    margin-left: 10px;
}
.right .direct-chat-text {
    margin-right: 10px;
    margin-left: 15%;
		float: right;
}

.right .direct-chat-timestamp {
    margin-right: 10px;
    margin-left: 0;
}

.direct-chat-timestamp {
	font-style: italic;
	color: #DEDEDE;
}
</style>
<script>
  $(function () {
		<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
			<?php if ($this->session->flashdata('success')) { ?>
				$('#successModal').modal('show');
			<?php } else { ?>
				$('#errorModal').modal('show');
			<?php } ?>
		<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Kartu Rencana Studi
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Kartu Rencana Studi</a></li>
            <li class="active">Detail Kartu Rencana Studi</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
					<div class="row">
            <div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
                  <h3 class="box-title">Detail Kartu Rencana Studi</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<label for="mahasiswa_nim" class="col-xs-4 col-md-2 control-label" style="text-align:left;">NIM</label>
										<div class="col-xs-8 col-md-4">
											<label for="mahasiswa_nim" style="text-align:left;">: <?php echo $mahasiswa_kuliah->mahasiswa_nim; ?></label>
										</div>
										<label for="mahasiswa_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Nama</label>
										<div class="col-xs-8 col-md-4">
											<label for="mahasiswa_nim" style="text-align:left;">: <?php echo $mahasiswa_kuliah->mahasiswa_nama; ?></label>
										</div>
									</div>
									<div class="row">
										<label for="program_studi_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Program Studi</label>
										<div class="col-xs-8 col-md-4">
											<label for="program_studi_nama" style="text-align:left;">: <?php echo $mahasiswa_kuliah->program_studi_nama; ?></label>
										</div>
										<label for="mahasiswa_tahun_masuk" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Angkatan</label>
										<div class="col-xs-8 col-md-4">
											<label for="mahasiswa_tahun_masuk" style="text-align:left;">: <?php echo $mahasiswa_kuliah->mahasiswa_tahun_masuk; ?></label>
										</div>
									</div>
									<div class="row">
										<label for="semester_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Periode</label>
										<div class="col-xs-8 col-md-4">
											<label for="semester_nama" style="text-align:left;">: <?php echo $mahasiswa_kuliah->semester_nama; ?></label>										
										</div>
									</div>
								</div>
								<div class="box-header with-border">
									<h3 class="box-title">Daftar Mata Kuliah</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<table id="datagrid_krs" class="table table-bordered table-striped" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th width="100">Kode</th>
												<th>MATA KULIAH</th>
												<th width="100">JUMLAH SKS</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$matakuliah = $this->db->query("SELECT akd_matakuliah.*, akd_mahasiswa_nilai.mahasiswa_nilai_id FROM akd_mahasiswa_nilai LEFT JOIN akd_matakuliah ON akd_mahasiswa_nilai.matakuliah_id=akd_matakuliah.matakuliah_id WHERE akd_mahasiswa_nilai.mahasiswa_kuliah_id = '$mahasiswa_kuliah->mahasiswa_kuliah_id' ORDER BY matakuliah_kode ASC")->result();
										if ($matakuliah){
											foreach ($matakuliah as $row) {
												$count_mahasiswa_nilai = $this->db->query("SELECT akd_matakuliah.* FROM akd_mahasiswa_nilai LEFT JOIN akd_matakuliah ON akd_mahasiswa_nilai.matakuliah_id=akd_matakuliah.matakuliah_id WHERE akd_mahasiswa_nilai.mahasiswa_kuliah_id = '$mahasiswa_kuliah->mahasiswa_kuliah_id' AND akd_mahasiswa_nilai.matakuliah_id = '$row->matakuliah_id'")->num_rows()
												?>
												<tr>
													<td><?php echo $row->matakuliah_kode; ?></td>
													<td><?php echo $row->matakuliah_nama; ?></td>
													<td class="text-center"><?php echo $row->matakuliah_sks; ?></td>
												</tr>
												<?php
											}
										} else {
											?>
											<tr>
												<td colspan="4">Belum ada mata kuliah yang ditambahkan.</td>
											</tr>
											<?php
										}
										?>
										</tbody>
									</table>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
					<div class="row">
            <div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$mahasiswa_kuliah->program_studi_id.'/'.$mahasiswa_kuliah->mahasiswa_tahun_masuk.'/'.$mahasiswa_kuliah->semester_kode); ?>'" class="btn btn-default">Kembali</button>
								</div><!-- /.box-footer -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>