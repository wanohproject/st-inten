<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Prs extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $semester_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'PRS | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Datatable_model');
		$this->load->model('Referensi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Matakuliah_model');
		$this->load->model('Perwalian_model');
		
		$this->semester_kode = $this->Semester_model->get_semester_aktif()->semester_kode;
    }

	public function datatable()
    {
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$where = "(mahasiswa.mahasiswa_tanggal_lulus IS NULL OR mahasiswa.mahasiswa_tanggal_lulus = '0000-00-00') AND mahasiswa.program_studi_id = '$program_studi_id' AND perwalian.perwalian_jenis = 'PRS'";

		$tahun_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		if ($tahun_kode != '-'){
			$where .= " AND mahasiswa_tahun_masuk = '$tahun_kode'";
		}
		
		$semester_kode = ($this->uri->segment(6))?$this->uri->segment(6):'-';
		$where .= " AND perwalian.semester_kode = '$semester_kode'";

		$this->Datatable_model->set_table("(SELECT perwalian.*, mahasiswa_nama, mahasiswa_nim, mahasiswa_user, mahasiswa_tahun_masuk FROM akd_perwalian perwalian LEFT JOIN akd_mahasiswa mahasiswa ON perwalian.mahasiswa_id=mahasiswa.mahasiswa_id WHERE $where) perwalian");
		$this->Datatable_model->set_column_order(array('mahasiswa_nim', 'mahasiswa_nama', 'mahasiswa_tahun_masuk', null, null));
		$this->Datatable_model->set_column_search(array('mahasiswa_nim', 'mahasiswa_nama', 'mahasiswa_tahun_masuk'));
		$this->Datatable_model->set_order(array('mahasiswa_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
			$row = array();
            $row['nomor'] = $no;
			$row['status_perwalian'] = "";
			if ($record->approve_akademik_status == "Y"){
				$row['status_perwalian'] = "<span class=\"label label-success\">SUDAH DIVERIFIKASI</span>";
			} else {
				$row['status_perwalian'] = "<span class=\"label label-primary\">PROSES PERWALIAN</span>";
			}
            $row['perwalian_id'] = $record->perwalian_id;
            $row['mahasiswa_id'] = $record->mahasiswa_id;
            $row['mahasiswa_nama'] = $record->mahasiswa_nama;
            $row['mahasiswa_nim'] = $record->mahasiswa_nim;
			$row['mahasiswa_tahun_masuk'] = $record->mahasiswa_tahun_masuk;
            $row['Actions'] = $this->get_buttons($record->perwalian_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-success btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-plus"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		
		$semester = $this->Semester_model->get_semester_aktif()->semester_kode;
		$semester_kode				= ($this->uri->segment(6))?$this->uri->segment(6):$semester;
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/prs', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['perwalian_id']	= validasi_sql($this->uri->segment(4)); 
		$data['perwalian'] = $this->Perwalian_model->get_perwalian('perwalian.*, mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, perwalian.semester_kode, program_studi_nama, semester_nama', $where);
		if (!$data['perwalian']){
			redirect(module_url($this->uri->segment(2)));
		}

		$data['semester_kode'] = $this->semester_kode; 
		$data['mahasiswa_id'] = userdata('mahasiswa_id'); 
		$data['program_studi_id'] = userdata('program_studi_id'); 
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/prs', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function add_matakuliah()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add_matakuliah';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['perwalian_id']	= validasi_sql($this->uri->segment(4)); 
		$data['perwalian'] = $this->Perwalian_model->get_perwalian('perwalian.*, mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, perwalian.semester_kode, program_studi_nama, semester_nama', $where);
		if (!$data['perwalian']){
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/prs', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function save_krs(){
		$data = array();
		$action = $this->input->post('action');
		$matakuliah = $this->input->post('matakuliah');
		$mahasiswa = $this->input->post('mahasiswa');
		$perwalian = $this->input->post('perwalian');
		$perwalian = $this->Perwalian_model->get_perwalian("perwalian.*", array('perwalian_id'=>$perwalian));

		if ($action == 'add' && $perwalian && $matakuliah){
			$insert['perwalian_matakuliah_id'] = $this->uuid->v4();
			$insert['perguruan_tinggi_id'] = $perwalian->perguruan_tinggi_id;
			$insert['program_studi_id'] = $perwalian->program_studi_id;
			$insert['jenjang_kode'] = $perwalian->jenjang_kode;
			$insert['semester_kode'] = $perwalian->semester_kode;
			$insert['perwalian_id'] = $perwalian->perwalian_id;
			$insert['matakuliah_id'] = $matakuliah;
			$insert['mahasiswa_id'] = $mahasiswa;
			$insert['created_by'] = userdata('pengguna_id');
			$res_insert = $this->Perwalian_model->insert_perwalian_matakuliah($insert);

			if ($res_insert){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['status']		= "add";
				$data['data']		= $res_insert;
			} else {
				$data['response']	= false;
				$data['status']		= "add";
				$data['message']	= "User telah digunakan ada.";
			}
		} else if ($action == 'delete' && $perwalian && $matakuliah){
			$where['perwalian_id'] = $perwalian->perwalian_id;
			$where['matakuliah_id'] = $matakuliah;
			$res_delete = $this->Perwalian_model->delete_perwalian_matakuliah($where);

			if ($res_delete){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['status']		= "delete";
				$data['data']		= $res_delete;
			} else {
				$data['response']	= false;
				$data['status']		= "delete";
				$data['message']	= "User telah digunakan ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function delete_krs()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$perwalian_matakuliah_id = $this->uri->segment(4);
		$perwalian_matakuliah = $this->Perwalian_model->get_perwalian_matakuliah('perwalian_matakuliah.*', array('perwalian_matakuliah.perwalian_matakuliah_id'=>$perwalian_matakuliah_id));

		$this->Perwalian_model->delete_perwalian_matakuliah(array('perwalian_matakuliah_id'=>validasi_sql($perwalian_matakuliah_id)));
		
		$this->session->set_flashdata('success','Data telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2).'/detail/'.$perwalian_matakuliah->perwalian_id.'?tabs=1'));
	}

	public function get_catatan(){
		$data = array();
		$program_studi_id = $this->input->post('program_studi');
		$semester_kode = $this->input->post('semester');
		$mahasiswa_id = $this->input->post('mahasiswa');
		if ($mahasiswa_id && $program_studi_id && $semester_kode){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$semester = $this->Semester_model->get_semester("*", array('semester_kode'=>$semester_kode));
			$mahasiswa = $this->Mahasiswa_model->get_mahasiswa("*", array('mahasiswa_id'=>$mahasiswa_id));

			if ($program_studi && $semester && $mahasiswa){
				$perwalian_catatan = $this->Perwalian_model->grid_all_perwalian_catatan('perwalian_catatan.*, mahasiswa_nama, dosen_nama', 'perwalian_catatan.created_at', 'ASC', '', '', array('perwalian_catatan.mahasiswa_id'=>$mahasiswa_id));
				$res = array();
				$i = 0;
				foreach ($perwalian_catatan as $row_catatan) {
					$res[$i] = new stdClass();
					$res[$i]->mahasiswa_nama = $row_catatan->mahasiswa_nama;
					$res[$i]->dosen_nama = $row_catatan->dosen_nama;
					$res[$i]->perwalian_catatan_jenis = $row_catatan->perwalian_catatan_jenis;
					$res[$i]->perwalian_catatan_isi = html_decode($row_catatan->perwalian_catatan_isi);
					$res[$i]->created_at = dateIndo5($row_catatan->created_at);
					$i++;
				}
				if ($perwalian_catatan){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					// $data['data']		= $perwalian_catatan;
					$data['data']		= $res;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function submit(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$perwalian_id = $this->uri->segment(4);
		$perwalian = $this->Perwalian_model->get_perwalian("perwalian.*", array('perwalian_id'=>$perwalian_id));

		if ($perwalian->approve_akademik_status == 'N'){
			$update = array();
			if ($perwalian->perwalian_submit_status == 'N'){
				$update['perwalian_submit_by']		= userdata('pengguna_id');
				$update['perwalian_submit_status']	= 'Y';
				$update['perwalian_submit_tanggal']	= date('Y-m-d H:i:s');
			}
			if ($perwalian->approve_wali_status == 'N'){
				$update['approve_wali_by']		= userdata('pengguna_id');
				$update['approve_wali_status']	= 'Y';
				$update['approve_wali_tanggal']	= date('Y-m-d H:i:s');
			}
			if ($perwalian->approve_keuangan_status == 'N'){
				$update['approve_keuangan_by']		= userdata('pengguna_id');
				$update['approve_keuangan_status']	= 'Y';
				$update['approve_keuangan_tanggal']	= date('Y-m-d H:i:s');
			}

			$update['approve_akademik_by']		= userdata('pengguna_id');
			$update['approve_akademik_status']	= 'Y';
			$update['approve_akademik_tanggal']	= date('Y-m-d H:i:s');
			$update['updated_by']				= userdata('pengguna_id');
			$this->Perwalian_model->update_perwalian(array('perwalian_id'=>validasi_sql($perwalian_id)), $update);

			$get_perwalian_matakuliah = $this->Perwalian_model->get_perwalian_matakuliah("SUM(matakuliah_sks) as jumlah_sks", array('perwalian_matakuliah.perwalian_id'=>$perwalian_id));
			$jumlah_sks = ($get_perwalian_matakuliah)?$get_perwalian_matakuliah->jumlah_sks:0;

			$get_mahasiswa_kuliah = $this->Mahasiswa_model->get_mahasiswa_kuliah("SUM(mahasiswa_kuliah_sks) as total_sks", array('mahasiswa_kuliah.mahasiswa_id'=>$perwalian->mahasiswa_id));
			$total_sks = ($get_mahasiswa_kuliah)?$get_mahasiswa_kuliah->total_sks:0;
			
			if ($this->Mahasiswa_model->count_all_mahasiswa_kuliah(array('mahasiswa_kuliah.semester_kode'=>$perwalian->semester_kode, 'mahasiswa_kuliah.mahasiswa_id'=>$perwalian->mahasiswa_id)) < 1){
				$insert_kuliah = array();
				$mahasiswa_kuliah_id = $this->uuid->v4();
				$insert_kuliah['perguruan_tinggi_id']		= $perwalian->perguruan_tinggi_id;
				$insert_kuliah['program_studi_id']			= $perwalian->program_studi_id;
				$insert_kuliah['jenjang_kode']				= $perwalian->jenjang_kode;
				$insert_kuliah['semester_kode']				= $perwalian->semester_kode;
				$insert_kuliah['perwalian_id']				= $perwalian->perwalian_id;
				$insert_kuliah['mahasiswa_id']				= $perwalian->mahasiswa_id;
				$insert_kuliah['mahasiswa_kuliah_id']		= $mahasiswa_kuliah_id;
				$insert_kuliah['mahasiswa_kuliah_sks']		= $jumlah_sks;
				$insert_kuliah['mahasiswa_kuliah_total']	= $total_sks;
				$insert_kuliah['created_by']				= userdata('pengguna_id');
				$this->Mahasiswa_model->insert_mahasiswa_kuliah($insert_kuliah);

				$this->Mahasiswa_model->delete_mahasiswa_nilai(array('semester_kode'=>$perwalian->semester_kode, 'mahasiswa_id'=>$perwalian->mahasiswa_id, 'mahasiswa_kuliah_id'=>$mahasiswa_kuliah_id));
				$grid_perwalian_matakuliah = $this->Perwalian_model->grid_all_perwalian_matakuliah('', 'perwalian_matakuliah.created_at', 'ASC', 0, 0, array('perwalian_matakuliah.semester_kode'=>$perwalian->semester_kode, 'perwalian_matakuliah.mahasiswa_id'=>$perwalian->mahasiswa_id, 'perwalian_matakuliah.perwalian_id'=>$perwalian->perwalian_id));
				if ($grid_perwalian_matakuliah) {
					foreach ($grid_perwalian_matakuliah as $row_perwalian_matakuliah) {
						$insert_nilai = array();
						$insert_nilai['perguruan_tinggi_id'] = $perwalian->perguruan_tinggi_id;
						$insert_nilai['program_studi_id'] = $perwalian->program_studi_id;
						$insert_nilai['jenjang_kode'] = $perwalian->jenjang_kode;
						$insert_nilai['semester_kode'] = $perwalian->semester_kode;
						$insert_nilai['mahasiswa_id'] = $perwalian->mahasiswa_id;
						$insert_nilai['matakuliah_id'] = $row_perwalian_matakuliah->matakuliah_id;
						$insert_nilai['mahasiswa_kuliah_id'] = $mahasiswa_kuliah_id;
						$insert_nilai['mahasiswa_nilai_id'] = $this->uuid->v4();
						$insert_nilai['created_by'] = userdata('pengguna_id');
						$this->Mahasiswa_model->insert_mahasiswa_nilai($insert_nilai);
					}
				}
			} else if ($this->Mahasiswa_model->count_all_mahasiswa_kuliah(array('mahasiswa_kuliah.semester_kode'=>$perwalian->semester_kode, 'mahasiswa_kuliah.mahasiswa_id'=>$perwalian->mahasiswa_id)) > 0){
				$get_mahasiswa_kuliah = $this->Mahasiswa_model->get_mahasiswa_kuliah("", array('mahasiswa_kuliah.semester_kode'=>$perwalian->semester_kode, 'mahasiswa_kuliah.mahasiswa_id'=>$perwalian->mahasiswa_id));
				if ($get_mahasiswa_kuliah){
					$mahasiswa_kuliah_id = $get_mahasiswa_kuliah->mahasiswa_kuliah_id;
					$update_kuliah = array();
					$update_kuliah['perwalian_id']				= $perwalian->perwalian_id;
					$update_kuliah['mahasiswa_kuliah_sks']		= $jumlah_sks;
					$update_kuliah['mahasiswa_kuliah_total']	= $total_sks;
					$update_kuliah['created_by']				= userdata('pengguna_id');
					$this->Mahasiswa_model->update_mahasiswa_kuliah(array('mahasiswa_kuliah_id'=>$mahasiswa_kuliah_id), $update_kuliah);

					$this->Mahasiswa_model->delete_mahasiswa_nilai(array('semester_kode'=>$perwalian->semester_kode, 'mahasiswa_id'=>$perwalian->mahasiswa_id, 'mahasiswa_kuliah_id'=>$mahasiswa_kuliah_id));
					$grid_perwalian_matakuliah = $this->Perwalian_model->grid_all_perwalian_matakuliah('', 'perwalian_matakuliah.created_at', 'ASC', 0, 0, array('perwalian_matakuliah.semester_kode'=>$perwalian->semester_kode, 'perwalian_matakuliah.mahasiswa_id'=>$perwalian->mahasiswa_id, 'perwalian_matakuliah.perwalian_id'=>$perwalian->perwalian_id));
					if ($grid_perwalian_matakuliah) {
						foreach ($grid_perwalian_matakuliah as $row_perwalian_matakuliah) {
							$insert_nilai = array();
							$insert_nilai['perguruan_tinggi_id'] = $perwalian->perguruan_tinggi_id;
							$insert_nilai['program_studi_id'] = $perwalian->program_studi_id;
							$insert_nilai['jenjang_kode'] = $perwalian->jenjang_kode;
							$insert_nilai['semester_kode'] = $perwalian->semester_kode;
							$insert_nilai['mahasiswa_id'] = $perwalian->mahasiswa_id;
							$insert_nilai['matakuliah_id'] = $row_perwalian_matakuliah->matakuliah_id;
							$insert_nilai['mahasiswa_kuliah_id'] = $mahasiswa_kuliah_id;
							$insert_nilai['mahasiswa_nilai_id'] = $this->uuid->v4();
							$insert_nilai['created_by'] = userdata('pengguna_id');
							$this->Mahasiswa_model->insert_mahasiswa_nilai($insert_nilai);
						}
					}
				}
			}

			$this->session->set_flashdata('success','Perwalian telah berhasil di submit.');
		} else {
			$this->session->set_flashdata('error','Perwalian gagal di submit.');
		}
		redirect(module_url($this->uri->segment(2).'/detail/'.$perwalian->perwalian_id.'?tabs=1'));
	}
	
	public function cancel(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$perwalian_id = $this->uri->segment(4);
		$perwalian = $this->Perwalian_model->get_perwalian("perwalian.*", array('perwalian_id'=>$perwalian_id));

		if ($perwalian->approve_akademik_status == 'Y'){
			$update = array();
			if ($perwalian->perwalian_submit_by == userdata('pengguna_id')){
				$update['perwalian_submit_by']		= userdata('pengguna_id');
				$update['perwalian_submit_status']	= 'N';
				$update['perwalian_submit_tanggal']	= date('Y-m-d H:i:s');
			}
			if ($perwalian->approve_wali_by == userdata('pengguna_id')){
				$update['approve_wali_by']		= userdata('pengguna_id');
				$update['approve_wali_status']	= 'N';
				$update['approve_wali_tanggal']	= date('Y-m-d H:i:s');
			}
			if ($perwalian->approve_keuangan_by == userdata('pengguna_id')){
				$update['approve_keuangan_by']		= userdata('pengguna_id');
				$update['approve_keuangan_status']	= 'N';
				$update['approve_keuangan_tanggal']	= date('Y-m-d H:i:s');
			}
			
			$update['approve_akademik_by']		= userdata('pengguna_id');
			$update['approve_akademik_status']	= 'N';
			$update['approve_akademik_tanggal']	= date('Y-m-d H:i:s');
			$update['updated_by']				= userdata('pengguna_id');

			$this->Perwalian_model->update_perwalian(array('perwalian_id'=>validasi_sql($perwalian_id)), $update);

			$this->session->set_flashdata('success','Perwalian telah berhasil di batal submit.');
		} else {
			$this->session->set_flashdata('error','Perwalian gagal di batal submit.');
		}
		redirect(module_url($this->uri->segment(2).'/detail/'.$perwalian->perwalian_id.'?tabs=1'));
	}
}