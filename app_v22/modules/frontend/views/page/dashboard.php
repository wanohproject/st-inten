<script>
$(function () {
<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
	<?php if ($this->session->flashdata('success')) { ?>
		$('#successModal').modal('show');
	<?php } else { ?>
		$('#errorModal').modal('show');
	<?php } ?>
<?php } ?>
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  <div class="container-fluid">
	  <div class="row">
		
		<section class="content">
          <div class="row">
            <div class="col-sm-8">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Selamat Datang di <?php echo profile('profil_institusi');?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
									<?php echo html_decode(static_page('sambutan')); ?>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
				
						<div class="col-sm-4">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Login</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
									<form action="<?php echo site_url('auth/login'); ?>" method="post">
										<div class="form-group has-feedback">
											<input type="text" name="username" id="username" class="form-control" placeholder="Akun Pengguna" required>
											<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
										</div>
										<div class="form-group has-feedback">
											<input type="password" name="password" id="password" class="form-control" placeholder="Kata Kunci" required>
											<span class="glyphicon glyphicon-lock form-control-feedback"></span>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<?php echo $captchaImage; ?>
											</div>
											<div class="col-xs-6">
												<div class="form-group has-feedback">
													<input type="text" name="captcha" id="captcha" class="form-control" placeholder="Kode" required>
													<span class="glyphicon glyphicon-eye-open form-control-feedback"></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<button type="submit" name="signin" value="signin" class="btn btn-primary btn-block btn-flat">Login</button>
											</div><!-- /.col -->
										</div><!-- /.row -->
									</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
		
      </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>