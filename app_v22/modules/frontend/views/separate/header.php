<!-- skin-green layout-boxed  -->
<body class="hold-transition <?php echo (profile('profil_warna'))?profile('profil_warna'):'skin-blue'; ?> layout-boxed layout-top-nav">
    <div class="wrapper">
	  <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <a href="<?php echo site_url();?>" class="navbar-brand"><b><?php echo profile('profil_institusi'); ?></b></a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>
          </div><!-- /.container-fluid -->
        </nav>
      </header>