<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('module_dir'))
{
	function module_dir($data=""){
		$obj =& get_instance();
		if ($data){
			return 'frontend/' . $data;
		} else {
			return 'frontend';
		}
	}
}

if ( ! function_exists('module_url'))
{
	function module_url($data=""){
		$obj =& get_instance();
		if ($data){
			return site_url('frontend/' . $data);
		} else {
			return site_url('frontend');
		}
	}
}
/* End of file mod_constant_helper.php */
/* Location: ./application/helpers/mod_constant_helper.php */