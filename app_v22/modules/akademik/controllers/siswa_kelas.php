<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa_kelas extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Siswa Kelas | ' . profile('profil_website');
		$this->active_menu		= 272;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Siswa_model');
		$this->load->model('Kelas_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Tahun_model');
		$this->load->model('Siswa_kelas_model');
		$this->load->model('Nilai_model');
		$this->load->model('Tingkat_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$departemen_id	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$tahun_kode		= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$tingkat_id		= ($this->uri->segment(6))?$this->uri->segment(6):'-';
		$kelas_id		= ($this->uri->segment(7))?$this->uri->segment(7):'-';
		
		$this->load->library('Datatables');
		$this->datatables->select('siswa.siswa_id, siswa_nama, siswa_nis, siswa_pin, siswa_pin_orangtua, siswa_kelas_sekarang')
		->add_column('Actions', $this->get_buttons('$1', '$2'),'siswa_id, siswa_nis')
		->search_column('siswa_nama, siswa_nis, siswa_kelas_sekarang')
		->from('siswa_kelas')
		->join('siswa', 'siswa_kelas.siswa_id=siswa.siswa_id', 'left')
		->join('kelas', 'siswa_kelas.kelas_id=kelas.kelas_id', 'left')
		->where("kelas.tahun_kode = '$tahun_kode' AND kelas.kelas_id = '$kelas_id'");
        echo $this->datatables->generate();
    }
	
	function get_buttons($id, $nis)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';			
				
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= $this->uri->segment(4);
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):'-';
		$data['kelas_id']		= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):'-';
		$data['staf_id']		= userdata('staf_id');		
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/siswa_kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['siswa_id']	= validasi_sql($this->uri->segment(4)); 
		$siswa		 			= $this->Siswa_model->get_siswa('*', $where);
		
		$data['siswa_id'] = $siswa->siswa_id;
		$data['siswa_nama'] = $siswa->siswa_nama;
		$data['siswa_nis'] = $siswa->siswa_nis;
		$data['siswa_jenis_kelamin'] = $siswa->siswa_jenis_kelamin;
		$data['siswa_nisn'] = $siswa->siswa_nisn;
		$data['siswa_tempat_lahir'] = $siswa->siswa_tempat_lahir;
		$data['siswa_tanggal_lahir'] = $siswa->siswa_tanggal_lahir;
		$data['siswa_nik'] = $siswa->siswa_nik;
		$data['siswa_agama'] = $siswa->siswa_agama;
		$data['siswa_alamat'] = $siswa->siswa_alamat;
		$data['siswa_rt'] = $siswa->siswa_rt;
		$data['siswa_rw'] = $siswa->siswa_rw;
		$data['siswa_dusun'] = $siswa->siswa_dusun;
		$data['siswa_deskel'] = $siswa->siswa_deskel;
		$data['siswa_kecamatan'] = $siswa->siswa_kecamatan;
		$data['siswa_kodepos'] = $siswa->siswa_kodepos;
		$data['siswa_jenis_tinggal'] = $siswa->siswa_jenis_tinggal;
		$data['siswa_alat_transportasi'] = $siswa->siswa_alat_transportasi;
		$data['siswa_telepon'] = $siswa->siswa_telepon;
		$data['siswa_hp'] = $siswa->siswa_hp;
		$data['siswa_email'] = $siswa->siswa_email;
		$data['siswa_no_skhun'] = $siswa->siswa_no_skhun;
		$data['siswa_penerima_kps'] = $siswa->siswa_penerima_kps;
		$data['siswa_no_kps'] = $siswa->siswa_no_kps;
		$data['siswa_nama_ayah'] = $siswa->siswa_nama_ayah;
		$data['siswa_tahun_ayah'] = $siswa->siswa_tahun_ayah;
		$data['siswa_pendidikan_ayah'] = $siswa->siswa_pendidikan_ayah;
		$data['siswa_pekerjaan_ayah'] = $siswa->siswa_pekerjaan_ayah;
		$data['siswa_penghasilan_ayah'] = $siswa->siswa_penghasilan_ayah;
		$data['siswa_nik_ayah'] = $siswa->siswa_nik_ayah;
		$data['siswa_nama_ibu'] = $siswa->siswa_nama_ibu;
		$data['siswa_tahun_ibu'] = $siswa->siswa_tahun_ibu;
		$data['siswa_pendidikan_ibu'] = $siswa->siswa_pendidikan_ibu;
		$data['siswa_pekerjaan_ibu'] = $siswa->siswa_pekerjaan_ibu;
		$data['siswa_penghasilan_ibu'] = $siswa->siswa_penghasilan_ibu;
		$data['siswa_nik_ibu'] = $siswa->siswa_nik_ibu;
		$data['siswa_nama_wali'] = $siswa->siswa_nama_wali;
		$data['siswa_tahun_wali'] = $siswa->siswa_tahun_wali;
		$data['siswa_pendidikan_wali'] = $siswa->siswa_pendidikan_wali;
		$data['siswa_pekerjaan_wali'] = $siswa->siswa_pekerjaan_wali;
		$data['siswa_penghasilan_wali'] = $siswa->siswa_penghasilan_wali;
		$data['siswa_nik_wali'] = $siswa->siswa_nik_wali;
		$data['siswa_kelas_sekarang'] = $siswa->siswa_kelas_sekarang;
		$data['siswa_no_un'] = $siswa->siswa_no_un;
		$data['siswa_no_ijazah'] = $siswa->siswa_no_ijazah;
		$data['siswa_penerima_kip'] = $siswa->siswa_penerima_kip;
		$data['siswa_nomor_kip'] = $siswa->siswa_nomor_kip;
		$data['siswa_nama_kip'] = $siswa->siswa_nama_kip;
		$data['siswa_nomor_kks'] = $siswa->siswa_nomor_kks;
		$data['siswa_no_akta_lahir'] = $siswa->siswa_no_akta_lahir;
		$data['siswa_bank'] = $siswa->siswa_bank;
		$data['siswa_nomor_rekening_bank'] = $siswa->siswa_nomor_rekening_bank;
		$data['siswa_rekening_atas_nama'] = $siswa->siswa_rekening_atas_nama;
		$data['siswa_layak_pip'] = $siswa->siswa_layak_pip;
		$data['siswa_alasan_layak_pip'] = $siswa->siswa_alasan_layak_pip;
		$data['siswa_kebutuhan_khusus'] = $siswa->siswa_kebutuhan_khusus;
		$data['siswa_sekolah_asal'] = $siswa->siswa_sekolah_asal;
		$data['siswa_anak_ke'] = $siswa->siswa_anak_ke;
		$data['siswa_user'] = $siswa->siswa_user;
		$data['siswa_pin'] = $siswa->siswa_pin;
		$data['siswa_pin_orangtua'] = $siswa->siswa_pin_orangtua;
		$data['siswa_reg'] = $siswa->siswa_reg;
		$data['siswa_foto'] = $siswa->siswa_foto;
		$data['siswa_website'] = $siswa->siswa_website;
		$data['siswa_kabkota'] = $siswa->siswa_kabkota;
		$data['siswa_provinsi'] = $siswa->siswa_provinsi;
		$data['siswa_hp_orangtua'] = $siswa->siswa_hp_orangtua;
		$data['siswa_telepon_orangtua'] = $siswa->siswa_telepon_orangtua;
		$data['siswa_tahun'] = $siswa->siswa_tahun;
		$data['siswa_no_raport'] = $siswa->siswa_no_raport;
		$data['siswa_status'] = $siswa->siswa_status;
		$data['siswa_baru'] = $siswa->siswa_baru;
		$data['siswa_kelas_sekarang_id'] = $siswa->siswa_kelas_sekarang_id;
		$data['siswa_golongan_darah'] = $siswa->siswa_golongan_darah;
		$data['siswa_kewarganegaraan'] = $siswa->siswa_kewarganegaraan;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/siswa_kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function cetak_pin()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$tahun_kode	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$tingkat_id	= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$kelas_id	= ($this->uri->segment(6))?$this->uri->segment(6):'-';
		
		$where['siswa_kelas.kelas_id'] = $kelas_id;
		$data['siswa']	= $this->Siswa_kelas_model->grid_all_siswa_kelas('*', 'siswa_nama', 'ASC', 0, 0, $where);
		$this->load->view(module_dir().'/export/list_pin_siswa', $data);
	}

	public function cetak_pin_excel()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$tahun_kode	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$tingkat_id	= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$kelas_id	= ($this->uri->segment(6))?$this->uri->segment(6):'-';
		
		$where['siswa_kelas.kelas_id'] = $kelas_id;
		$data['siswa']	= $this->Siswa_kelas_model->grid_all_siswa_kelas('*', 'kelas_nama, siswa_nama', 'ASC', 0, 0, $where);
		$this->load->view(module_dir().'/export/list_pin_siswa_excel', $data);
	}
	
	public function cetak_kartu()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['tahun_kode']	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['tingkat_id']	= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['kelas_id']	= ($this->uri->segment(6))?$this->uri->segment(6):'-';
		
		$where['siswa_kelas.kelas_id'] = $data['kelas_id'];
		$data['siswa']	= $this->Siswa_kelas_model->grid_all_siswa_kelas('*', 'siswa_nama', 'ASC', 0, 0, $where);
		$this->load->view(module_dir().'/export/list_pin_siswa_kartu', $data);
	}
}
