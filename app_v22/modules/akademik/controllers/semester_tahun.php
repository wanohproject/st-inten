<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Semester_tahun extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Semester_tahun | ' . profile('profil_website');
		$this->active_menu		= 258;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Semester_tahun_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
	}
	
	public function datatable()
    {
		$tahun_kode = validasi_sql($this->uri->segment(4));

		$this->Datatable_model->set_table("semester");
		$this->Datatable_model->set_column_order(array('semester_kode', 'semester_nama', 'semester_kode', 'semester_kode', 'semester_status'));
		$this->Datatable_model->set_column_search(array('semester_kode', 'semester_nama', 'semester_kode', 'semester_kode', 'semester_status'));
		$this->Datatable_model->set_order(array('semester_kode', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
			$semester_tahun = $this->Semester_tahun_model->get_semester_tahun("", array("semester.semester_id"=>$record->semester_id, "tahun_ajaran.tahun_kode"=>$tahun_kode));
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['semester_kode'] = $record->semester_kode;
            $row['semester_nama'] = $record->semester_nama;
            $row['tanggal_awal'] = ($semester_tahun)?$semester_tahun->semester_tahun_awal:"";
            $row['tanggal_akhir'] = ($semester_tahun)?$semester_tahun->semester_tahun_akhir:"";
            $row['semester_status'] = ($record->semester_status == 'A')?'AKTIF':'TIDAK AKTIF';
            $row['Actions'] = $this->get_buttons($record->semester_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/activated/'.$id) .'" class="btn btn-success btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Aktifkan"><i class="fa fa-check"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		// $html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester_tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):'';
		$data['semester_nama']		= ($this->input->post('semester_nama'))?$this->input->post('semester_nama'):'';
		$data['semester_status']	= ($this->input->post('semester_status'))?$this->input->post('semester_status'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($data['semester_status'] == 'A'){
				$this->db->query("UPDATE semester SET semester_status = 'H'");
			}
			
			$insert['semester_kode']		= validasi_sql($this->input->post('semester_kode'));
			$insert['semester_nama']		= validasi_sql($this->input->post('semester_nama'));
			$insert['semester_status']		= validasi_sql($this->input->post('semester_status'));
			$this->Semester_tahun_model->insert_semester($insert);
			
			$this->session->set_flashdata('success','Semester_tahun telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester_tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['semester_id']		= validasi_sql($this->uri->segment(4)); 
		$semester 					= $this->Semester_tahun_model->get_semester('*', $where);

		$data['semester_id']		= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester->semester_id;
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester->semester_kode;
		$data['semester_nama']		= ($this->input->post('semester_nama'))?$this->input->post('semester_nama'):$semester->semester_nama;
		$data['semester_status']	= ($this->input->post('semester_status'))?$this->input->post('semester_status'):$semester->semester_status;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($data['semester_status'] == 'A'){
				$this->db->query("UPDATE semester SET semester_status = 'H'");
			}
			
			$where_edit['semester_id']	= validasi_sql($this->input->post('semester_id'));
			$edit['semester_kode']		= validasi_sql($this->input->post('semester_kode'));
			$edit['semester_nama']		= validasi_sql($this->input->post('semester_nama'));
			$edit['semester_status']	= validasi_sql($this->input->post('semester_status'));
			$this->Semester_tahun_model->update_semester($where_edit, $edit);
			
			$this->session->set_flashdata('success','Semester_tahun telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester_tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['semester_id']	= validasi_sql($this->uri->segment(4));
		$this->Semester_tahun_model->delete_semester($where_delete);
		
		$this->session->set_flashdata('success','Semester_tahun telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function activated()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->Semester_tahun_model->update_semester(array(), array('semester_status'=>'H'));
		
		$where_edit['semester_id']	= validasi_sql($this->uri->segment(4));
		$edit['semester_status']	= 'A';
		$this->Semester_tahun_model->update_semester($where_edit, $edit);
		
		$this->session->set_flashdata('success','Semester_tahun telah berhasil diaktifkan.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['semester_id']	= validasi_sql($this->uri->segment(4)); 
		$semester		 		= $this->Semester_tahun_model->get_semester('*', $where);
		
		$data['semester_id']		= $semester->semester_id;
		$data['semester_kode']		= $semester->semester_kode;
		$data['semester_nama']		= $semester->semester_nama;
		$data['semester_status']	= $semester->semester_status;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/semester_tahun', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
