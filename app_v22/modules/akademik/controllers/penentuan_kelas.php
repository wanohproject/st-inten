<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penentuan_kelas extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Penentuan Kelas | ' . profile('profil_website');
		$this->active_menu		= 264;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Tahun_model');
		$this->load->model('Kelas_model');
		$this->load->model('Siswa_model');
		$this->load->model('Siswa_kelas_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$tahun_ajaran = $this->Tahun_model->get_tahun_ajaran("tahun_kode, tahun_nama, tahun_angkatan", array("tahun_aktif"=>"A"));
		$data['tahun_kode']		= $tahun_ajaran->tahun_kode;
		
		$tahun_ajaran_depan = $this->db->query("SELECT tahun_kode FROM tahun_ajaran WHERE tahun_angkatan = '".($tahun_ajaran->tahun_angkatan + 1)."'")->row();
		$data['tahun_depan_id']	= ($tahun_ajaran_depan)?$tahun_ajaran_depan->tahun_kode:$tahun_ajaran->tahun_kode;
		
		$data['kenaikan_jenis']	= ($this->input->post('kenaikan_jenis'))?$this->input->post('kenaikan_jenis'):'';
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$data['tahun_kode'];
		$data['tahun_depan_id']	= ($this->input->post('tahun_depan_id'))?$this->input->post('tahun_depan_id'):$data['tahun_depan_id'];
		$data['kelas_id']		= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):'';
		$data['kelas_asal']		= ($this->input->post('kelas_asal'))?$this->input->post('kelas_asal'):'';
		$data['kelas_tujuan']	= ($this->input->post('kelas_tujuan'))?$this->input->post('kelas_tujuan'):'';
		$data['data_asal']		= ($this->input->post('data_asal'))?$this->input->post('data_asal'):'';
		$data['data_tujuan']	= ($this->input->post('data_tujuan'))?$this->input->post('data_tujuan'):'';
		
		$save	= $this->input->post('save');
		if ($save){
			// $listSiswa = 0;
			// if ($data['data_asal']){
				// $listSiswa = implode(",", $data['data_asal']);
			// }
			// $forDelete = $this->db->query("SELECT siswa_kelas_id FROM siswa_kelas WHERE tahun_kode = '".$data['tahun_kode']."' AND siswa NOT IN ($listMP)")->result();
			
			if ($data['kenaikan_jenis'] == ''){
				if ($data['data_asal']){
					foreach($data['data_asal'] as $key => $row){
						if ($this->Siswa_kelas_model->count_all_siswa_kelas(array('siswa_kelas.siswa_id'=>$key, 'siswa_kelas.tahun_kode'=>$data['tahun_depan_id'])) < 1){
							$insert['kelas_id']	= $row;
							$insert['tahun_kode']	= $data['tahun_kode'];
							$insert['siswa_id']	= $key;
							$this->Siswa_kelas_model->insert_siswa_kelas($insert);
						} else {
							$this->Siswa_kelas_model->update_siswa_kelas(array('siswa_kelas.siswa_id'=>$key, 'siswa_kelas.tahun_kode'=>$data['tahun_depan_id']), array('kelas_id'=>$row));
						}
						$get_kelas	= $this->Kelas_model->get_kelas("*", array("kelas_id"=>$row, "kelas.tahun_kode"=>$data['tahun_kode']));
						$update_siswa['siswa_kelas_sekarang'] 	= ($get_kelas)?$get_kelas->kelas_nama:'';
						$update_siswa['siswa_kelas_sekarang_id']= ($get_kelas)?$get_kelas->kelas_id:'';
						$this->Siswa_model->update_siswa(array('siswa_id'=>$key), $update_siswa);
					}
				}
			} else {
				if ($data['data_asal']){
					foreach($data['data_asal'] as $key => $row){
						if ($this->Siswa_kelas_model->count_all_siswa_kelas(array('siswa_kelas.siswa_id'=>$key, 'siswa_kelas.tahun_kode'=>$data['tahun_depan_id'])) < 1){
							$insert['kelas_id']	= $row;
							$insert['tahun_kode']	= $data['tahun_depan_id'];
							$insert['siswa_id']	= $key;
							$this->Siswa_kelas_model->insert_siswa_kelas($insert);
						} else {
							$this->Siswa_kelas_model->update_siswa_kelas(array('siswa_kelas.siswa_id'=>$key, 'siswa_kelas.tahun_kode'=>$data['tahun_depan_id']), array('kelas_id'=>$row));
						}
						$get_kelas	= $this->Kelas_model->get_kelas("*", array("kelas_id"=>$row, "kelas.tahun_kode"=>$data['tahun_kode']));
						$update_siswa['siswa_kelas_sekarang'] 	= ($get_kelas)?$get_kelas->kelas_nama:'';
						$update_siswa['siswa_kelas_sekarang_id']= ($get_kelas)?$get_kelas->kelas_id:'';
						$this->Siswa_model->update_siswa(array('siswa_id'=>$key), $update_siswa);
					}
				}
			}
		}
		/*
		$pindah_asal_tujuan	= $this->input->post('pindah_asal_tujuan');
		$pindah_tujuan_asal	= $this->input->post('pindah_tujuan_asal');
		if ($pindah_asal_tujuan){
			if ($data['kenaikan_jenis'] == 1 || $data['kenaikan_jenis'] == 2 || $data['kenaikan_jenis'] == 3){
				if ($data['data_asal']){
					foreach($data['data_asal'] as $row){
						if ($this->Siswa_kelas_model->count_all_siswa_kelas(array('siswa_kelas.siswa_id'=>$row, 'siswa_kelas.tahun_kode'=>$data['tahun_depan_id'])) < 1){
							$insert['kelas_id']	= $data['kelas_tujuan'];
							$insert['tahun_kode']	= $data['tahun_depan_id'];
							$insert['siswa_id']	= $row;
							$this->Siswa_kelas_model->insert_siswa_kelas($insert);
						}
					}
				}
			} else if ($data['kenaikan_jenis'] == 4){
				if ($data['data_asal']){
					foreach($data['data_asal'] as $row){
						$update['siswa_status']	= 'Alumni';
						$this->siswa->update_siswa(array('siswa_id'=>$row), $update);
					}
				}
			}
		}
		
		if ($pindah_tujuan_asal){
			if ($data['kenaikan_jenis'] == 1 || $data['kenaikan_jenis'] == 2 || $data['kenaikan_jenis'] == 3){
				if ($data['data_tujuan']){						
					foreach($data['data_tujuan'] as $key => $row){
						$count_nilai = $this->nilai->count_all_nilai(array('nilai.siswa_kelas_id'=>$key));
						if ($count_nilai < 1){
							$this->Siswa_kelas_model->delete_siswa_kelas(array('siswa_kelas_id'=>$key));
						}
					}
				}
			} else if ($data['kenaikan_jenis'] == 4){
				if ($data['data_asal']){
					foreach($data['data_asal'] as $row){
						$update['siswa_status']	= 'Siswa';
						$this->Siswa_model->update_siswa(array('siswa_id'=>$row), $update);
					}
				}
			}
		}
		*/
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/penentuan_kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
