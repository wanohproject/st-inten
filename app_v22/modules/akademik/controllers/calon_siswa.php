<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Calon_siswa extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Calon Siswa | ' . profile('profil_website');
		$this->active_menu		= 307;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Calon_siswa_model');
		$this->load->model('Kelas_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Tahun_model');
		$this->load->model('Nilai_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$tahun_kode = ($this->uri->segment(4))?$this->uri->segment(4):$this->tahun_kode;
		$tahun = $this->Tahun_model->get_tahun_ajaran("tahun_angkatan", array("tahun_kode"=>$tahun_kode));
		$tahun_angkatan = "";
		if ($tahun){
			$tahun_angkatan = $tahun->tahun_angkatan;
		}
		$this->load->library('Datatables');
		$this->datatables->select('siswa_id, siswa_nama, siswa_reg')
		->add_column('Actions', $this->get_buttons('$1', '$2'),'siswa_id, siswa_reg')
		->search_column('siswa_nama, siswa_reg')
		->from('calon_siswa')
		->where("siswa_tahun = '$tahun_angkatan' AND siswa_status = 'Calon Siswa'");
        echo $this->datatables->generate();
    }
	
	function get_buttons($id, $reg)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id.'/'.$reg) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$tahun_kode				= ($this->uri->segment(4))?$this->uri->segment(4):$this->tahun_kode;
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/calon_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['siswa_id']				= ($this->input->post('siswa_id'))?$this->input->post('siswa_id'):'';
		$data['siswa_reg']				= ($this->input->post('siswa_reg'))?$this->input->post('siswa_reg'):'';
		$data['siswa_nisn']				= ($this->input->post('siswa_nisn'))?$this->input->post('siswa_nisn'):'';
		$data['siswa_nik']				= ($this->input->post('siswa_nik'))?$this->input->post('siswa_nik'):'';
		$data['siswa_nama']				= ($this->input->post('siswa_nama'))?$this->input->post('siswa_nama'):'';
		$data['siswa_tanggal_lahir']	= ($this->input->post('siswa_tanggal_lahir'))?$this->input->post('siswa_tanggal_lahir'):'';
		$data['siswa_tempat_lahir']		= ($this->input->post('siswa_tempat_lahir'))?$this->input->post('siswa_tempat_lahir'):'';
		$data['siswa_jenis_kelamin']	= ($this->input->post('siswa_jenis_kelamin'))?$this->input->post('siswa_jenis_kelamin'):'';
		$data['siswa_alamat']			= ($this->input->post('siswa_alamat'))?$this->input->post('siswa_alamat'):'';
		$data['siswa_golongan_darah']	= ($this->input->post('siswa_golongan_darah'))?$this->input->post('siswa_golongan_darah'):'';
		$data['siswa_agama']			= ($this->input->post('siswa_agama'))?$this->input->post('siswa_agama'):'';
		$data['siswa_warganegara']		= ($this->input->post('siswa_warganegara'))?$this->input->post('siswa_warganegara'):'WNI';
		$data['siswa_foto']				= ($this->input->post('siswa_foto'))?$this->input->post('siswa_foto'):'';
		$data['siswa_hp']				= ($this->input->post('siswa_hp'))?$this->input->post('siswa_hp'):'';
		$data['siswa_telepon']			= ($this->input->post('siswa_telepon'))?$this->input->post('siswa_telepon'):'';
		$data['siswa_email']			= ($this->input->post('siswa_email'))?$this->input->post('siswa_email'):'';
		$data['siswa_rt']				= ($this->input->post('siswa_rt'))?$this->input->post('siswa_rt'):'';
		$data['siswa_rw']				= ($this->input->post('siswa_rw'))?$this->input->post('siswa_rw'):'';
		$data['siswa_deskel']			= ($this->input->post('siswa_deskel'))?$this->input->post('siswa_deskel'):'';
		$data['siswa_kecamatan']		= ($this->input->post('siswa_kecamatan'))?$this->input->post('siswa_kecamatan'):'';
		$data['siswa_kabkota']			= ($this->input->post('siswa_kabkota'))?$this->input->post('siswa_kabkota'):'';
		$data['siswa_provinsi']			= ($this->input->post('siswa_provinsi'))?$this->input->post('siswa_provinsi'):'';
		$data['siswa_kodepos']			= ($this->input->post('siswa_kodepos'))?$this->input->post('siswa_kodepos'):'';
		$data['siswa_nama_ayah']		= ($this->input->post('siswa_nama_ayah'))?$this->input->post('siswa_nama_ayah'):'';
		$data['siswa_pekerjaan_ayah']	= ($this->input->post('siswa_pekerjaan_ayah'))?$this->input->post('siswa_pekerjaan_ayah'):'';
		$data['siswa_nama_ibu']			= ($this->input->post('siswa_nama_ibu'))?$this->input->post('siswa_nama_ibu'):'';
		$data['siswa_pekerjaan_ibu']	= ($this->input->post('siswa_pekerjaan_ibu'))?$this->input->post('siswa_pekerjaan_ibu'):'';
		$data['siswa_hp_orangtua']		= ($this->input->post('siswa_hp_orangtua'))?$this->input->post('siswa_hp_orangtua'):'';
		$data['siswa_telepon_orangtua']	= ($this->input->post('siswa_telepon_orangtua'))?$this->input->post('siswa_telepon_orangtua'):'';
		$data['siswa_tahun']			= ($this->input->post('siswa_tahun'))?$this->input->post('siswa_tahun'):$this->tahun_kode;
		$data['siswa_no_skhun']			= ($this->input->post('siswa_no_skhun'))?$this->input->post('siswa_no_skhun'):'';
		$data['siswa_no_raport']		= ($this->input->post('siswa_no_raport'))?$this->input->post('siswa_no_raport'):'';
		$data['siswa_status']			= 'Calon Siswa';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$insert['siswa_reg'] = validasi_sql($this->input->post('siswa_reg'));
			$insert['siswa_nis'] = validasi_sql($this->input->post('siswa_reg'));
			$insert['siswa_nisn'] = validasi_sql($this->input->post('siswa_nisn'));
			$insert['siswa_nik'] = validasi_sql($this->input->post('siswa_nik'));
			$insert['siswa_nama'] = validasi_sql($this->input->post('siswa_nama'));
			$insert['siswa_tanggal_lahir'] = validasi_sql($this->input->post('siswa_tanggal_lahir'));
			$insert['siswa_tempat_lahir'] = validasi_sql($this->input->post('siswa_tempat_lahir'));
			$insert['siswa_jenis_kelamin'] = validasi_sql($this->input->post('siswa_jenis_kelamin'));
			$insert['siswa_alamat'] = validasi_sql($this->input->post('siswa_alamat'));
			$insert['siswa_golongan_darah'] = validasi_sql($this->input->post('siswa_golongan_darah'));
			$insert['siswa_agama'] = validasi_sql($this->input->post('siswa_agama'));
			$insert['siswa_warganegara'] = validasi_sql($this->input->post('siswa_warganegara'));
			$insert['siswa_foto'] = validasi_sql($this->input->post('siswa_foto'));
			$insert['siswa_hp'] = validasi_sql($this->input->post('siswa_hp'));
			$insert['siswa_telepon'] = validasi_sql($this->input->post('siswa_telepon'));
			$insert['siswa_email'] = validasi_sql($this->input->post('siswa_email'));
			$insert['siswa_rt'] = validasi_sql($this->input->post('siswa_rt'));
			$insert['siswa_rw'] = validasi_sql($this->input->post('siswa_rw'));
			$insert['siswa_deskel'] = validasi_sql($this->input->post('siswa_deskel'));
			$insert['siswa_kecamatan'] = validasi_sql($this->input->post('siswa_kecamatan'));
			$insert['siswa_kabkota'] = validasi_sql($this->input->post('siswa_kabkota'));
			$insert['siswa_provinsi'] = validasi_sql($this->input->post('siswa_provinsi'));
			$insert['siswa_kodepos'] = validasi_sql($this->input->post('siswa_kodepos'));
			$insert['siswa_nama_ayah'] = validasi_sql($this->input->post('siswa_nama_ayah'));
			$insert['siswa_pekerjaan_ayah'] = validasi_sql($this->input->post('siswa_pekerjaan_ayah'));
			$insert['siswa_nama_ibu'] = validasi_sql($this->input->post('siswa_nama_ibu'));
			$insert['siswa_pekerjaan_ibu'] = validasi_sql($this->input->post('siswa_pekerjaan_ibu'));
			$insert['siswa_hp_orangtua'] = validasi_sql($this->input->post('siswa_hp_orangtua'));
			$insert['siswa_telepon_orangtua'] = validasi_sql($this->input->post('siswa_telepon_orangtua'));
			$insert['siswa_tahun'] = validasi_sql($this->input->post('siswa_tahun'));
			$insert['siswa_no_skhun'] = validasi_sql($this->input->post('siswa_no_skhun'));
			$insert['siswa_no_raport'] = validasi_sql($this->input->post('siswa_no_raport'));
			$insert['siswa_status'] = 'Calon Siswa';
					
			$this->Calon_siswa_model->insert_calon_siswa($insert);
			
			$this->session->set_flashdata('success','Calon Siswa telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/calon_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['siswa_id']		= validasi_sql($this->uri->segment(4)); 
		$siswa 					= $this->Calon_siswa_model->get_calon_siswa('*', $where);

		$data['siswa_id']	= ($this->input->post('siswa_id'))?$this->input->post('siswa_id'):$siswa->siswa_id;
		$data['siswa_reg']	= ($this->input->post('siswa_reg'))?$this->input->post('siswa_reg'):$siswa->siswa_reg;
		$data['siswa_nisn']	= ($this->input->post('siswa_nisn'))?$this->input->post('siswa_nisn'):$siswa->siswa_nisn;
		$data['siswa_nik']	= ($this->input->post('siswa_nik'))?$this->input->post('siswa_nik'):$siswa->siswa_nik;
		$data['siswa_nama']	= ($this->input->post('siswa_nama'))?$this->input->post('siswa_nama'):$siswa->siswa_nama;
		$data['siswa_tanggal_lahir']	= ($this->input->post('siswa_tanggal_lahir'))?$this->input->post('siswa_tanggal_lahir'):$siswa->siswa_tanggal_lahir;
		$data['siswa_tempat_lahir']	= ($this->input->post('siswa_tempat_lahir'))?$this->input->post('siswa_tempat_lahir'):$siswa->siswa_tempat_lahir;
		$data['siswa_jenis_kelamin']	= ($this->input->post('siswa_jenis_kelamin'))?$this->input->post('siswa_jenis_kelamin'):$siswa->siswa_jenis_kelamin;
		$data['siswa_alamat']	= ($this->input->post('siswa_alamat'))?$this->input->post('siswa_alamat'):$siswa->siswa_alamat;
		$data['siswa_golongan_darah']	= ($this->input->post('siswa_golongan_darah'))?$this->input->post('siswa_golongan_darah'):$siswa->siswa_golongan_darah;
		$data['siswa_agama']	= ($this->input->post('siswa_agama'))?$this->input->post('siswa_agama'):$siswa->siswa_agama;
		$data['siswa_warganegara']	= ($this->input->post('siswa_warganegara'))?$this->input->post('siswa_warganegara'):$siswa->siswa_warganegara;
		$data['siswa_foto']	= ($this->input->post('siswa_foto'))?$this->input->post('siswa_foto'):$siswa->siswa_foto;
		$data['siswa_hp']	= ($this->input->post('siswa_hp'))?$this->input->post('siswa_hp'):$siswa->siswa_hp;
		$data['siswa_telepon']	= ($this->input->post('siswa_telepon'))?$this->input->post('siswa_telepon'):$siswa->siswa_telepon;
		$data['siswa_email']	= ($this->input->post('siswa_email'))?$this->input->post('siswa_email'):$siswa->siswa_email;
		$data['siswa_rt']	= ($this->input->post('siswa_rt'))?$this->input->post('siswa_rt'):$siswa->siswa_rt;
		$data['siswa_rw']	= ($this->input->post('siswa_rw'))?$this->input->post('siswa_rw'):$siswa->siswa_rw;
		$data['siswa_deskel']	= ($this->input->post('siswa_deskel'))?$this->input->post('siswa_deskel'):$siswa->siswa_deskel;
		$data['siswa_kecamatan']	= ($this->input->post('siswa_kecamatan'))?$this->input->post('siswa_kecamatan'):$siswa->siswa_kecamatan;
		$data['siswa_kabkota']	= ($this->input->post('siswa_kabkota'))?$this->input->post('siswa_kabkota'):$siswa->siswa_kabkota;
		$data['siswa_provinsi']	= ($this->input->post('siswa_provinsi'))?$this->input->post('siswa_provinsi'):$siswa->siswa_provinsi;
		$data['siswa_kodepos']	= ($this->input->post('siswa_kodepos'))?$this->input->post('siswa_kodepos'):$siswa->siswa_kodepos;
		$data['siswa_nama_ayah']	= ($this->input->post('siswa_nama_ayah'))?$this->input->post('siswa_nama_ayah'):$siswa->siswa_nama_ayah;
		$data['siswa_pekerjaan_ayah']	= ($this->input->post('siswa_pekerjaan_ayah'))?$this->input->post('siswa_pekerjaan_ayah'):$siswa->siswa_pekerjaan_ayah;
		$data['siswa_nama_ibu']	= ($this->input->post('siswa_nama_ibu'))?$this->input->post('siswa_nama_ibu'):$siswa->siswa_nama_ibu;
		$data['siswa_pekerjaan_ibu']	= ($this->input->post('siswa_pekerjaan_ibu'))?$this->input->post('siswa_pekerjaan_ibu'):$siswa->siswa_pekerjaan_ibu;
		$data['siswa_hp_orangtua']	= ($this->input->post('siswa_hp_orangtua'))?$this->input->post('siswa_hp_orangtua'):$siswa->siswa_hp_orangtua;
		$data['siswa_telepon_orangtua']	= ($this->input->post('siswa_telepon_orangtua'))?$this->input->post('siswa_telepon_orangtua'):$siswa->siswa_telepon_orangtua;
		$data['siswa_tahun']	= ($this->input->post('siswa_tahun'))?$this->input->post('siswa_tahun'):$siswa->siswa_tahun;
		$data['siswa_no_skhun']	= ($this->input->post('siswa_no_skhun'))?$this->input->post('siswa_no_skhun'):$siswa->siswa_no_skhun;
		$data['siswa_no_raport']	= ($this->input->post('siswa_no_raport'))?$this->input->post('siswa_no_raport'):$siswa->siswa_no_raport;
		$data['siswa_status']	= ($this->input->post('siswa_status'))?$this->input->post('siswa_status'):$siswa->siswa_status;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['siswa_id']	= validasi_sql($this->input->post('siswa_id'));
			$edit['siswa_reg'] = validasi_sql($this->input->post('siswa_reg'));
			$edit['siswa_nis'] = validasi_sql($this->input->post('siswa_reg'));
			$edit['siswa_nisn'] = validasi_sql($this->input->post('siswa_nisn'));
			$edit['siswa_nik'] = validasi_sql($this->input->post('siswa_nik'));
			$edit['siswa_nama'] = validasi_sql($this->input->post('siswa_nama'));
			$edit['siswa_tanggal_lahir'] = validasi_sql($this->input->post('siswa_tanggal_lahir'));
			$edit['siswa_tempat_lahir'] = validasi_sql($this->input->post('siswa_tempat_lahir'));
			$edit['siswa_jenis_kelamin'] = validasi_sql($this->input->post('siswa_jenis_kelamin'));
			$edit['siswa_alamat'] = validasi_sql($this->input->post('siswa_alamat'));
			$edit['siswa_golongan_darah'] = validasi_sql($this->input->post('siswa_golongan_darah'));
			$edit['siswa_agama'] = validasi_sql($this->input->post('siswa_agama'));
			$edit['siswa_warganegara'] = validasi_sql($this->input->post('siswa_warganegara'));
			$edit['siswa_foto'] = validasi_sql($this->input->post('siswa_foto'));
			$edit['siswa_hp'] = validasi_sql($this->input->post('siswa_hp'));
			$edit['siswa_telepon'] = validasi_sql($this->input->post('siswa_telepon'));
			$edit['siswa_email'] = validasi_sql($this->input->post('siswa_email'));
			$edit['siswa_rt'] = validasi_sql($this->input->post('siswa_rt'));
			$edit['siswa_rw'] = validasi_sql($this->input->post('siswa_rw'));
			$edit['siswa_deskel'] = validasi_sql($this->input->post('siswa_deskel'));
			$edit['siswa_kecamatan'] = validasi_sql($this->input->post('siswa_kecamatan'));
			$edit['siswa_kabkota'] = validasi_sql($this->input->post('siswa_kabkota'));
			$edit['siswa_provinsi'] = validasi_sql($this->input->post('siswa_provinsi'));
			$edit['siswa_kodepos'] = validasi_sql($this->input->post('siswa_kodepos'));
			$edit['siswa_nama_ayah'] = validasi_sql($this->input->post('siswa_nama_ayah'));
			$edit['siswa_pekerjaan_ayah'] = validasi_sql($this->input->post('siswa_pekerjaan_ayah'));
			$edit['siswa_nama_ibu'] = validasi_sql($this->input->post('siswa_nama_ibu'));
			$edit['siswa_pekerjaan_ibu'] = validasi_sql($this->input->post('siswa_pekerjaan_ibu'));
			$edit['siswa_hp_orangtua'] = validasi_sql($this->input->post('siswa_hp_orangtua'));
			$edit['siswa_telepon_orangtua'] = validasi_sql($this->input->post('siswa_telepon_orangtua'));
			$edit['siswa_tahun'] = validasi_sql($this->input->post('siswa_tahun'));
			$edit['siswa_no_skhun'] = validasi_sql($this->input->post('siswa_no_skhun'));
			$edit['siswa_no_raport'] = validasi_sql($this->input->post('siswa_no_raport'));
			$edit['siswa_status'] = validasi_sql($this->input->post('siswa_status'));
			
			$this->Calon_siswa_model->update_calon_siswa($where_edit, $edit);
			
			$this->session->set_flashdata('success','Calon Siswa telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/calon_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_pengguna['pengguna_nama']	= validasi_sql($this->uri->segment(5));
		$this->Pengguna_model->delete_pengguna($where_pengguna);
		
		$where_delete['siswa_id']		= validasi_sql($this->uri->segment(4));
		$this->Calon_siswa_model->delete_calon_siswa($where_delete);
		
		$this->session->set_flashdata('success','Calon Siswa telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function import()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'import';
		
		$data['dataExcel']				= array();		
		$data['filename']				= '';
		
		if ($this->input->post('importSiswa')){
			if ($_FILES['userfile']['tmp_name']){
				$ext = end(explode(".", basename($_FILES['userfile']['name'])));
				if ($ext == 'xls'){
				
					$timestamp = explode(" ",microtime());
					$filename = time().str_shuffle('123456ABCDEF');
					
					$uploaddir = './asset/temp_upload/';
					$uploadfname = 'siswa_'.$filename . '.' . end(explode(".", basename($_FILES['userfile']['name'])));
					$uploadfile = $uploaddir . $uploadfname;
					$data['filename'] = $uploadfname;
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					
						$this->load->library('excel_reader');
						$this->excel_reader->setOutputEncoding('CP1251');
						$this->excel_reader->read($uploadfile);
						$dataFile = $this->excel_reader->sheets[0];
						
						$paramsReal = array("NO","TAHUN AJARAN","TAHUN MASUK","NO REG","NISN","NAMA","TEMPAT LAHIR","TANGGAL LAHIR","JENIS KELAMIN","AGAMA","WARGA NEGARA","GOLDAR","TELP. SISWA","HP SISWA","EMAIL SISWA","NAMA AYAH","PEKERJAAN AYAH","NAMA IBU","PEKERJAAN IBU","ALAMAT ORANG TUA","RT","RW","DESA/KELURAHAN","KECAMATAN","KAB./KOTA","PROVINSI","KODE POS","TELP. ORTU","HP. ORTU","NO. RAPORT","NO. SKHUN");
						$paramsReal = $iOne = array_combine(range(1, count($paramsReal)), array_values($paramsReal));
						$paramsFile = $dataFile['cells'][2];
						$result 	= array_diff($paramsReal,$paramsFile);
						
						if (count($result) == 0){
							
							for ($i = 4; $i <= $dataFile['numRows']; $i++) {
								if($dataFile['cells'][$i][1]){
									$data['dataExcel'][$i]['siswa_id'] 					= (isset($dataFile['cells'][$i][1]))?$dataFile['cells'][$i][1]:'';
									$data['dataExcel'][$i]['siswa_tahun_ajaran'] 		= (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
									$data['dataExcel'][$i]['siswa_tahun_masuk'] 		= (isset($dataFile['cells'][$i][3]))?$dataFile['cells'][$i][3]:'';
									$data['dataExcel'][$i]['siswa_reg'] 				= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
									$data['dataExcel'][$i]['siswa_nisn'] 				= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
									$data['dataExcel'][$i]['siswa_nama'] 				= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
									$data['dataExcel'][$i]['siswa_tempat_lahir'] 		= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
									$data['dataExcel'][$i]['siswa_tanggal_lahir'] 		= (isset($dataFile['cells'][$i][8]))?$dataFile['cells'][$i][8]:'';
									$data['dataExcel'][$i]['siswa_jenis_kelamin'] 		= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
									$data['dataExcel'][$i]['siswa_agama'] 				= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
									$data['dataExcel'][$i]['siswa_warganegara'] 		= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';
									$data['dataExcel'][$i]['siswa_golongan_darah'] 		= (isset($dataFile['cells'][$i][12]))?$dataFile['cells'][$i][12]:'';
									$data['dataExcel'][$i]['siswa_telepon'] 			= (isset($dataFile['cells'][$i][13]))?$dataFile['cells'][$i][13]:'';
									$data['dataExcel'][$i]['siswa_hp'] 					= (isset($dataFile['cells'][$i][14]))?$dataFile['cells'][$i][14]:'';
									$data['dataExcel'][$i]['siswa_email'] 				= (isset($dataFile['cells'][$i][15]))?$dataFile['cells'][$i][15]:'';
									$data['dataExcel'][$i]['siswa_nama_ayah'] 			= (isset($dataFile['cells'][$i][16]))?$dataFile['cells'][$i][16]:'';
									$data['dataExcel'][$i]['siswa_pekerjaan_ayah'] 		= (isset($dataFile['cells'][$i][17]))?$dataFile['cells'][$i][17]:'';
									$data['dataExcel'][$i]['siswa_nama_ibu'] 			= (isset($dataFile['cells'][$i][18]))?$dataFile['cells'][$i][18]:'';
									$data['dataExcel'][$i]['siswa_pekerjaan_ibu'] 		= (isset($dataFile['cells'][$i][19]))?$dataFile['cells'][$i][19]:'';
									$data['dataExcel'][$i]['siswa_alamat'] 				= (isset($dataFile['cells'][$i][20]))?$dataFile['cells'][$i][20]:'';
									$data['dataExcel'][$i]['siswa_rt'] 					= (isset($dataFile['cells'][$i][21]))?$dataFile['cells'][$i][21]:'';
									$data['dataExcel'][$i]['siswa_rw'] 					= (isset($dataFile['cells'][$i][22]))?$dataFile['cells'][$i][22]:'';
									$data['dataExcel'][$i]['siswa_deskel'] 				= (isset($dataFile['cells'][$i][23]))?$dataFile['cells'][$i][23]:'';
									$data['dataExcel'][$i]['siswa_kecamatan'] 			= (isset($dataFile['cells'][$i][24]))?$dataFile['cells'][$i][24]:'';
									$data['dataExcel'][$i]['siswa_kabkota'] 			= (isset($dataFile['cells'][$i][25]))?$dataFile['cells'][$i][25]:'';
									$data['dataExcel'][$i]['siswa_provinsi'] 			= (isset($dataFile['cells'][$i][26]))?$dataFile['cells'][$i][26]:'';
									$data['dataExcel'][$i]['siswa_kodepos'] 			= (isset($dataFile['cells'][$i][27]))?$dataFile['cells'][$i][27]:'';
									$data['dataExcel'][$i]['siswa_telepon_orangtua'] 	= (isset($dataFile['cells'][$i][28]))?$dataFile['cells'][$i][28]:'';
									$data['dataExcel'][$i]['siswa_hp_orangtua'] 		= (isset($dataFile['cells'][$i][29]))?$dataFile['cells'][$i][29]:'';
									$data['dataExcel'][$i]['siswa_no_raport'] 			= (isset($dataFile['cells'][$i][30]))?$dataFile['cells'][$i][30]:'';
									$data['dataExcel'][$i]['siswa_no_skhun'] 			= (isset($dataFile['cells'][$i][31]))?$dataFile['cells'][$i][31]:'';
								}
							}
						} else {
							$this->session->set_flashdata('error','Format file salah.');
							redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
						}
					} else {
						$this->session->set_flashdata('error','File gagal di-upload.');
						redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
					}
				} else {
					$this->session->set_flashdata('error','Format file yang Anda gunakan salah. Silahkan gunakan format file Excel 97-2003 (.xls).');
					redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Silahkan masukkan file yang akan di-import.');
				redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
			}
		}
		
		if ($this->input->post('save')){
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$this->excel_reader->read('./asset/temp_upload/'.$this->input->post('filename'));
			$dataFile = $this->excel_reader->sheets[0];
			
			for ($i = 4; $i <= $dataFile['numRows']; $i++) {
				if($dataFile['cells'][$i][1]){
					$siswa_reg = (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
					
					if ($this->Calon_siswa_model->count_all_calon_siswa(array("calon_siswa.siswa_reg"=>$siswa_reg)) < 1){
						$tahun_nama = (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
						$get_tahun	= $this->Tahun_model->get_tahun_ajaran("*", array("tahun_nama"=>$tahun_nama));
						if ($get_tahun){
							$insert_siswa['siswa_tahun'] 				= (isset($dataFile['cells'][$i][3]))?$dataFile['cells'][$i][3]:'';
							$insert_siswa['siswa_reg'] 					= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
							$insert_siswa['siswa_nis'] 					= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
							$insert_siswa['siswa_nisn'] 				= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
							$insert_siswa['siswa_nama'] 				= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
							$insert_siswa['siswa_tempat_lahir'] 		= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
							$insert_siswa['siswa_tanggal_lahir'] 		= (isset($dataFile['cells'][$i][8]))?dateIndo4($dataFile['cells'][$i][8]):'';
							$insert_siswa['siswa_jenis_kelamin'] 		= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
							$insert_siswa['siswa_agama'] 				= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
							$insert_siswa['siswa_warganegara'] 			= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';
							$insert_siswa['siswa_golongan_darah'] 		= (isset($dataFile['cells'][$i][12]))?$dataFile['cells'][$i][12]:'';
							$insert_siswa['siswa_telepon'] 				= (isset($dataFile['cells'][$i][13]))?$dataFile['cells'][$i][13]:'';
							$insert_siswa['siswa_hp'] 					= (isset($dataFile['cells'][$i][14]))?$dataFile['cells'][$i][14]:'';
							$insert_siswa['siswa_email'] 				= (isset($dataFile['cells'][$i][15]))?$dataFile['cells'][$i][15]:'';
							$insert_siswa['siswa_nama_ayah'] 			= (isset($dataFile['cells'][$i][16]))?$dataFile['cells'][$i][16]:'';
							$insert_siswa['siswa_pekerjaan_ayah'] 		= (isset($dataFile['cells'][$i][17]))?$dataFile['cells'][$i][17]:'';
							$insert_siswa['siswa_nama_ibu'] 			= (isset($dataFile['cells'][$i][18]))?$dataFile['cells'][$i][18]:'';
							$insert_siswa['siswa_pekerjaan_ibu'] 		= (isset($dataFile['cells'][$i][19]))?$dataFile['cells'][$i][19]:'';
							$insert_siswa['siswa_alamat'] 				= (isset($dataFile['cells'][$i][20]))?$dataFile['cells'][$i][20]:'';
							$insert_siswa['siswa_rt'] 					= (isset($dataFile['cells'][$i][21]))?$dataFile['cells'][$i][21]:'';
							$insert_siswa['siswa_rw'] 					= (isset($dataFile['cells'][$i][22]))?$dataFile['cells'][$i][22]:'';
							$insert_siswa['siswa_deskel'] 				= (isset($dataFile['cells'][$i][23]))?$dataFile['cells'][$i][23]:'';
							$insert_siswa['siswa_kecamatan'] 			= (isset($dataFile['cells'][$i][24]))?$dataFile['cells'][$i][24]:'';
							$insert_siswa['siswa_kabkota'] 				= (isset($dataFile['cells'][$i][25]))?$dataFile['cells'][$i][25]:'';
							$insert_siswa['siswa_provinsi'] 			= (isset($dataFile['cells'][$i][26]))?$dataFile['cells'][$i][26]:'';
							$insert_siswa['siswa_kodepos'] 				= (isset($dataFile['cells'][$i][27]))?$dataFile['cells'][$i][27]:'';
							$insert_siswa['siswa_telepon_orangtua'] 	= (isset($dataFile['cells'][$i][28]))?$dataFile['cells'][$i][28]:'';
							$insert_siswa['siswa_hp_orangtua'] 			= (isset($dataFile['cells'][$i][29]))?$dataFile['cells'][$i][29]:'';
							$insert_siswa['siswa_no_raport'] 			= (isset($dataFile['cells'][$i][30]))?$dataFile['cells'][$i][30]:'';
							$insert_siswa['siswa_no_skhun'] 			= (isset($dataFile['cells'][$i][31]))?$dataFile['cells'][$i][31]:'';
							$this->Calon_siswa_model->insert_calon_siswa($insert_siswa);
						}
					} else {
						$tahun_nama = (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
						$get_tahun	= $this->Tahun_model->get_tahun_ajaran("*", array("tahun_nama"=>$tahun_nama));
						if ($get_tahun){
							$update_siswa['siswa_tahun'] 				= (isset($dataFile['cells'][$i][3]))?$dataFile['cells'][$i][3]:'';
							$update_siswa['siswa_nisn'] 				= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
							$update_siswa['siswa_nama'] 				= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
							$update_siswa['siswa_tempat_lahir'] 		= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
							$update_siswa['siswa_tanggal_lahir'] 		= (isset($dataFile['cells'][$i][8]))?dateIndo4($dataFile['cells'][$i][8]):'';
							$update_siswa['siswa_jenis_kelamin'] 		= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
							$update_siswa['siswa_agama'] 				= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
							$update_siswa['siswa_warganegara'] 			= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';
							$update_siswa['siswa_golongan_darah'] 		= (isset($dataFile['cells'][$i][12]))?$dataFile['cells'][$i][12]:'';
							$update_siswa['siswa_telepon'] 				= (isset($dataFile['cells'][$i][13]))?$dataFile['cells'][$i][13]:'';
							$update_siswa['siswa_hp'] 					= (isset($dataFile['cells'][$i][14]))?$dataFile['cells'][$i][14]:'';
							$update_siswa['siswa_email'] 				= (isset($dataFile['cells'][$i][15]))?$dataFile['cells'][$i][15]:'';
							$update_siswa['siswa_nama_ayah'] 			= (isset($dataFile['cells'][$i][16]))?$dataFile['cells'][$i][16]:'';
							$update_siswa['siswa_pekerjaan_ayah'] 		= (isset($dataFile['cells'][$i][17]))?$dataFile['cells'][$i][17]:'';
							$update_siswa['siswa_nama_ibu'] 			= (isset($dataFile['cells'][$i][18]))?$dataFile['cells'][$i][18]:'';
							$update_siswa['siswa_pekerjaan_ibu'] 		= (isset($dataFile['cells'][$i][19]))?$dataFile['cells'][$i][19]:'';
							$update_siswa['siswa_alamat'] 				= (isset($dataFile['cells'][$i][20]))?$dataFile['cells'][$i][20]:'';
							$update_siswa['siswa_rt'] 					= (isset($dataFile['cells'][$i][21]))?$dataFile['cells'][$i][21]:'';
							$update_siswa['siswa_rw'] 					= (isset($dataFile['cells'][$i][22]))?$dataFile['cells'][$i][22]:'';
							$update_siswa['siswa_deskel'] 				= (isset($dataFile['cells'][$i][23]))?$dataFile['cells'][$i][23]:'';
							$update_siswa['siswa_kecamatan'] 			= (isset($dataFile['cells'][$i][24]))?$dataFile['cells'][$i][24]:'';
							$update_siswa['siswa_kabkota'] 				= (isset($dataFile['cells'][$i][25]))?$dataFile['cells'][$i][25]:'';
							$update_siswa['siswa_provinsi'] 			= (isset($dataFile['cells'][$i][26]))?$dataFile['cells'][$i][26]:'';
							$update_siswa['siswa_kodepos'] 				= (isset($dataFile['cells'][$i][27]))?$dataFile['cells'][$i][27]:'';
							$update_siswa['siswa_telepon_orangtua'] 	= (isset($dataFile['cells'][$i][28]))?$dataFile['cells'][$i][28]:'';
							$update_siswa['siswa_hp_orangtua'] 			= (isset($dataFile['cells'][$i][29]))?$dataFile['cells'][$i][29]:'';
							$update_siswa['siswa_no_raport'] 			= (isset($dataFile['cells'][$i][30]))?$dataFile['cells'][$i][30]:'';
							$update_siswa['siswa_no_skhun'] 			= (isset($dataFile['cells'][$i][31]))?$dataFile['cells'][$i][31]:'';
							$this->Calon_siswa_model->update_calon_siswa(array('siswa_nis'=>$siswa_nis), $update_siswa);
						}
					}
				}
			}
			
			$this->session->set_flashdata('success','Calon Siswa telah berhasil di-import.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/calon_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
	
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['siswa_id']	= validasi_sql($this->uri->segment(4)); 
		$siswa		 		= $this->Calon_siswa_model->get_calon_siswa('*', $where);
		
		$data['siswa_id'] = $siswa->siswa_id;
		$data['siswa_reg'] = $siswa->siswa_reg;
		$data['siswa_nis'] = $siswa->siswa_nis;
		$data['siswa_nisn'] = $siswa->siswa_nisn;
		$data['siswa_nik'] = $siswa->siswa_nik;
		$data['siswa_nama'] = $siswa->siswa_nama;
		$data['siswa_tanggal_lahir'] = $siswa->siswa_tanggal_lahir;
		$data['siswa_tempat_lahir'] = $siswa->siswa_tempat_lahir;
		$data['siswa_jenis_kelamin'] = $siswa->siswa_jenis_kelamin;
		$data['siswa_alamat'] = $siswa->siswa_alamat;
		$data['siswa_golongan_darah'] = $siswa->siswa_golongan_darah;
		$data['siswa_agama'] = $siswa->siswa_agama;
		$data['siswa_warganegara'] = $siswa->siswa_warganegara;
		$data['siswa_foto'] = $siswa->siswa_foto;
		$data['siswa_hp'] = $siswa->siswa_hp;
		$data['siswa_telepon'] = $siswa->siswa_telepon;
		$data['siswa_email'] = $siswa->siswa_email;
		$data['siswa_rt'] = $siswa->siswa_rt;
		$data['siswa_rw'] = $siswa->siswa_rw;
		$data['siswa_deskel'] = $siswa->siswa_deskel;
		$data['siswa_kecamatan'] = $siswa->siswa_kecamatan;
		$data['siswa_kabkota'] = $siswa->siswa_kabkota;
		$data['siswa_provinsi'] = $siswa->siswa_provinsi;
		$data['siswa_kodepos'] = $siswa->siswa_kodepos;
		$data['siswa_nama_ayah'] = $siswa->siswa_nama_ayah;
		$data['siswa_pekerjaan_ayah'] = $siswa->siswa_pekerjaan_ayah;
		$data['siswa_nama_ibu'] = $siswa->siswa_nama_ibu;
		$data['siswa_pekerjaan_ibu'] = $siswa->siswa_pekerjaan_ibu;
		$data['siswa_hp_orangtua'] = $siswa->siswa_hp_orangtua;
		$data['siswa_telepon_orangtua'] = $siswa->siswa_telepon_orangtua;
		$data['siswa_tahun'] = $siswa->siswa_tahun;
		$data['siswa_no_skhun'] = $siswa->siswa_no_skhun;
		$data['siswa_no_raport'] = $siswa->siswa_no_raport;
		$data['siswa_status'] = $siswa->siswa_status;
		$data['siswa_baru'] = $siswa->siswa_baru;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/calon_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}