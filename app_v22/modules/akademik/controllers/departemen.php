<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Departemen extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Departemen | ' . profile('profil_website');
		$this->active_menu		= 257;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Departemen_model');
		$this->load->model('Tahun_model');
		$this->load->model('Peminatan_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Staf_model');
		$this->load->model('Siswa_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{	
		$this->load->library('Datatables');
		$this->datatables->select('departemen.departemen_id, departemen.departemen_kode, departemen.departemen_nama, departemen.departemen_tipe, utama.departemen_nama AS departemen_parent')
		->add_column('Actions', $this->get_buttons('$1'), 'departemen_id')
		->search_column('departemen.departemen_nama')
		->from('departemen')
		->join('departemen utama', 'departemen.departemen_utama=utama.departemen_id', 'left');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/departemen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):'';
		$data['departemen_kode']	= ($this->input->post('departemen_kode'))?$this->input->post('departemen_kode'):'';
		$data['departemen_nama']	= ($this->input->post('departemen_nama'))?$this->input->post('departemen_nama'):'';
		$data['departemen_utama']	= ($this->input->post('departemen_utama'))?$this->input->post('departemen_utama'):'';
		$data['departemen_tipe']	= ($this->input->post('departemen_tipe'))?$this->input->post('departemen_tipe'):'';
		$data['departemen_urutan']	= ($this->input->post('departemen_urutan'))?$this->input->post('departemen_urutan'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){			
			$insert['departemen_kode']		= validasi_sql($this->input->post('departemen_kode'));
			$insert['departemen_nama']		= validasi_sql($this->input->post('departemen_nama'));
			$insert['departemen_utama']		= ($this->input->post('departemen_utama'))?validasi_sql($this->input->post('departemen_utama')):null;
			$insert['departemen_tipe']		= validasi_sql($this->input->post('departemen_tipe'));
			$insert['departemen_urutan']	= validasi_sql($this->input->post('departemen_urutan'));
			$this->Departemen_model->insert_departemen($insert);
			
			$this->session->set_flashdata('success','Departemen telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/departemen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['departemen_id']		= validasi_sql($this->uri->segment(4)); 
		$departemen 					= $this->Departemen_model->get_departemen('*', $where);

		$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen->departemen_id;
		$data['departemen_kode']	= ($this->input->post('departemen_kode'))?$this->input->post('departemen_kode'):$departemen->departemen_kode;
		$data['departemen_nama']	= ($this->input->post('departemen_nama'))?$this->input->post('departemen_nama'):$departemen->departemen_nama;
		$data['departemen_utama']	= ($this->input->post('departemen_utama'))?$this->input->post('departemen_utama'):$departemen->departemen_utama;
		$data['departemen_tipe']	= ($this->input->post('departemen_tipe'))?$this->input->post('departemen_tipe'):$departemen->departemen_tipe;
		$data['departemen_urutan']	= ($this->input->post('departemen_urutan'))?$this->input->post('departemen_urutan'):$departemen->departemen_urutan;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['departemen_id']	= validasi_sql($this->input->post('departemen_id'));
			$edit['departemen_kode']		= validasi_sql($this->input->post('departemen_kode'));
			$edit['departemen_nama']		= validasi_sql($this->input->post('departemen_nama'));
			$edit['departemen_utama']		= ($this->input->post('departemen_utama'))?validasi_sql($this->input->post('departemen_utama')):null;
			$edit['departemen_tipe']		= validasi_sql($this->input->post('departemen_tipe'));
			$edit['departemen_urutan']		= validasi_sql($this->input->post('departemen_urutan'));
			$this->Departemen_model->update_departemen($where_edit, $edit);
			
			$this->session->set_flashdata('success','Departemen telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/departemen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['departemen_id']	= validasi_sql($this->uri->segment(4));
		$this->Departemen_model->delete_departemen($where_delete);
		
		$this->session->set_flashdata('success','Departemen telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['departemen_id']	= validasi_sql($this->uri->segment(4)); 
		$departemen		 		= $this->Departemen_model->get_departemen('*', $where);
		
		$data['departemen_id']		= $departemen->departemen_id;
		$data['departemen_kode']	= $departemen->departemen_kode;
		$data['departemen_nama']	= $departemen->departemen_nama;
		$data['departemen_utama']	= $departemen->departemen_utama;
		$data['departemen_tipe']	= $departemen->departemen_tipe;
		$data['departemen_urutan']	= $departemen->departemen_urutan;

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/departemen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}