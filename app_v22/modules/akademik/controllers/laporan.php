<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Laporan | ' . profile('profil_website');
		$this->active_menu		= 273;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Nilai_model');
		$this->load->model('Deskripsi_nilai_model');
		$this->load->model('Tahun_model');
		$this->load->model('Kelas_model');
		$this->load->model('Semester_model');
		$this->load->model('Aspek_penilaian_model');
		$this->load->model('Aspek_pelajaran_model');
		$this->load->model('Komponen_penilaian_model');
		$this->load->model('Komponen_pelajaran_model');
		$this->load->model('Mata_pelajaran_model');
		$this->load->model('Antar_mata_pelajaran_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Staf_model');
		$this->load->model('Siswa_model');
		$this->load->model('Siswa_kelas_model');
		$this->load->model('Kompetensi_dasar_model');
		$this->load->model('Guru_pelajaran_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'index';
		
		if ($this->session->userdata('level') == 11 || $this->session->userdata('level') == 15){
			redirect(module_url('raport'));
			exit();
		}
		
		$data['tahun_kode']		= $this->tahun_kode;
		$data['jumlah_siswa']	= $this->db->query("SELECT COUNT(siswa_id) AS jumlah_siswa FROM siswa_kelas WHERE tahun_kode = '{$this->tahun_kode}'")->row()->jumlah_siswa;
		$data['jumlah_guru']	= $this->db->query("SELECT COUNT(staf_id) AS jumlah_guru FROM guru_pelajaran WHERE tahun_kode = '{$this->tahun_kode}' GROUP BY staf_id")->num_rows();
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/laporan/laporan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function legger()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'legger';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$tahun_kode				= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$pelajaran_id			= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):'-';
		$tingkat_id				= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
		$kelas_id				= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):'-';
		$komponen_id			= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):'-';
		$tampilkan				= ($this->uri->segment(10))?validasi_sql($this->uri->segment(10)):'-';
		
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$tingkat_id;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['pelajaran_id']	= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):$pelajaran_id;
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['kelas_id']		= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):$kelas_id;
		$data['komponen_id']	= $this->input->post('komponen_id');
		$data['pengujian_id']	= ($this->input->post('pengujian_id'))?$this->input->post('pengujian_id'):'';
		$data['tampilkan']		= $this->input->post('tampilkan');

		$where_gp['guru_pelajaran.pelajaran_id'] 			= $data['pelajaran_id'];
		$where_gp['guru_pelajaran.tahun_kode'] 				= $data['tahun_kode'];
		$where_gp['guru_pelajaran.kelas_id'] 				= $data['kelas_id'];
		$where_gp['guru_pelajaran.guru_pelajaran_status'] 	= 'A';
		$guru_pelajaran = $this->Guru_pelajaran_model->get_guru_pelajaran('', $where_gp);
		$data['staf_id'] = ($guru_pelajaran)?$guru_pelajaran->staf_id:'-';
		$data['staf_nama'] = ($guru_pelajaran)?$guru_pelajaran->staf_nama:'-';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/laporan/legger', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function export_legger_pelajaran(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['tahun_kode']		= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']	= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):'-';
		$data['pelajaran_id']	= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):'-';
		$data['tingkat_id']		= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
		$data['kelas_id']		= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):'-';
		$data['komponen_id']	= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):'-';
		$komponen = $this->Komponen_penilaian_model->get_komponen_penilaian('komponen_kode', array('komponen_id'=>$data['komponen_id']));
		$data['komponen_kode']	= ($komponen)?$komponen->komponen_kode:'';

		$where_gp['guru_pelajaran.pelajaran_id'] 			= $data['pelajaran_id'];
		$where_gp['guru_pelajaran.tahun_kode'] 				= $data['tahun_kode'];
		$where_gp['guru_pelajaran.kelas_id'] 				= $data['kelas_id'];
		$where_gp['guru_pelajaran.guru_pelajaran_status'] 	= 'A';
		$guru_pelajaran = $this->Guru_pelajaran_model->get_guru_pelajaran('', $where_gp);
		$data['staf_id'] = ($guru_pelajaran)?$guru_pelajaran->staf_id:'-';
		$data['staf_nama'] = ($guru_pelajaran)?$guru_pelajaran->staf_nama:'-';
		
		if ($data['pelajaran_id'] == 'semua-pelajaran'){
			$tahun 					= $this->Tahun_model->get_tahun_ajaran('', array('tahun_kode'=>$data['tahun_kode']));
			$kelas 					= $this->Kelas_model->get_kelas('', array('kelas_id'=>$data['kelas_id']));
			$komponen 				= $this->Komponen_penilaian_model->get_komponen_penilaian('', array('komponen_id'=>$data['komponen_id']));
			$guru 					= $this->Staf_model->get_staf('', array('staf_id'=>$data['staf_id']));
			$data['tahun_nama']		= ($tahun)?$tahun->tahun_nama:'';
			$data['kelas_nama']		= ($kelas)?$kelas->kelas_nama:'';
			$data['komponen_nama']	= ($komponen)?$komponen->komponen_nama:'';
			$data['guru_nama']		= ($guru)?$guru->staf_nama:'';
			$data['excel']			= true;
			
			$data['grid_siswa_kelas']	= $this->Siswa_kelas_model->grid_all_siswa_kelas("", "siswa.siswa_nama", "ASC", 0, 0, array("siswa_kelas.kelas_id"=>$data['kelas_id'], "siswa_kelas.tahun_kode"=>$data['tahun_kode']));
			$this->load->view(module_dir().'/export/legger_walikelas', $data);
		} else {
			$tahun 					= $this->Tahun_model->get_tahun_ajaran('', array('tahun_kode'=>$data['tahun_kode']));
			$kelas 					= $this->Kelas_model->get_kelas('', array('kelas_id'=>$data['kelas_id']));
			$pelajaran 				= $this->Mata_pelajaran_model->get_mata_pelajaran('', array('pelajaran_id'=>$data['pelajaran_id']));
			$guru 					= $this->Staf_model->get_staf('', array('staf_id'=>$data['staf_id']));
			$data['tahun_nama']		= ($tahun)?$tahun->tahun_nama:'';
			$data['kelas_nama']		= ($kelas)?$kelas->kelas_nama:'';
			$data['pelajaran_nama']	= ($pelajaran)?$pelajaran->pelajaran_nama:'';
			$data['guru_nama']		= ($guru)?$guru->staf_nama:'';
			$data['excel']			= true;
			
			$data['grid_siswa_kelas']	= $this->Siswa_kelas_model->grid_all_siswa_kelas("", "siswa.siswa_nama", "ASC", 0, 0, array("siswa_kelas.kelas_id"=>$data['kelas_id'], "siswa_kelas.tahun_kode"=>$data['tahun_kode']));
			$this->load->view(module_dir().'/export/legger_pelajaran', $data);
		}
	}
	
	public function legger_semester()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'legger';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$tahun_kode				= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$semester_id			= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$pelajaran_id			= 'semua-pelajaran';
		$tingkat_id				= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
		$kelas_id				= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):'-';
		$komponen_id			= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):'-';
		$tampilkan				= ($this->uri->segment(10))?validasi_sql($this->uri->segment(10)):'-';
		
		$data['komponen_id']	= ($this->input->post('komponen_id'))?$this->input->post('komponen_id'):$komponen_id;
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$tingkat_id;
		$data['kelas_id']		= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):$kelas_id;
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
		$data['pelajaran_id']	= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):$pelajaran_id;
		$data['pengujian_id']	= ($this->input->post('pengujian_id'))?$this->input->post('pengujian_id'):'';
		$data['tampilkan']		= $this->input->post('tampilkan');
		
		$where_gp['guru_pelajaran.pelajaran_id'] 			= $data['pelajaran_id'];
		$where_gp['guru_pelajaran.tahun_kode'] 				= $data['tahun_kode'];
		$where_gp['guru_pelajaran.kelas_id'] 				= $data['kelas_id'];
		$where_gp['guru_pelajaran.guru_pelajaran_status'] 	= 'A';
		$guru_pelajaran = $this->Guru_pelajaran_model->get_guru_pelajaran('', $where_gp);
		$data['staf_id'] = ($guru_pelajaran)?$guru_pelajaran->staf_id:'-';
		$data['staf_nama'] = ($guru_pelajaran)?$guru_pelajaran->staf_nama:'-';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/laporan/legger_semester', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function export_legger_seluruh(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['tahun_kode']		= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']	= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):'-';
		$data['pelajaran_id']	= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):'-';
		$data['tingkat_id']		= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
		$data['kelas_id']		= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):'-';
		$data['komponen_id']	= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):'-';

		$where_gp['guru_pelajaran.pelajaran_id'] 			= $data['pelajaran_id'];
		$where_gp['guru_pelajaran.tahun_kode'] 				= $data['tahun_kode'];
		$where_gp['guru_pelajaran.kelas_id'] 				= $data['kelas_id'];
		$where_gp['guru_pelajaran.guru_pelajaran_status'] 	= 'A';
		$guru_pelajaran = $this->Guru_pelajaran_model->get_guru_pelajaran('', $where_gp);
		$data['staf_id'] = ($guru_pelajaran)?$guru_pelajaran->staf_id:'-';
		$data['staf_nama'] = ($guru_pelajaran)?$guru_pelajaran->staf_nama:'-';
		
		if ($data['pelajaran_id'] == 'semua-pelajaran'){
			$tahun 					= $this->Tahun_model->get_tahun_ajaran('', array('tahun_kode'=>$data['tahun_kode']));
			$kelas 					= $this->Kelas_model->get_kelas('', array('kelas_id'=>$data['kelas_id']));
			$komponen 				= $this->Komponen_penilaian_model->get_komponen_penilaian('', array('komponen_id'=>$data['komponen_id']));
			$guru 					= $this->Staf_model->get_staf('', array('staf_id'=>$data['staf_id']));
			$data['tahun_nama']		= ($tahun)?$tahun->tahun_nama:'';
			$data['kelas_nama']		= ($kelas)?$kelas->kelas_nama:'';
			$data['komponen_nama']	= ($komponen)?$komponen->komponen_nama:'';
			$data['guru_nama']		= ($guru)?$guru->staf_nama:'';
			$data['excel']			= true;
			
			$data['grid_siswa_kelas']	= $this->Siswa_kelas_model->grid_all_siswa_kelas("", "siswa.siswa_nama", "ASC", 0, 0, array("siswa_kelas.kelas_id"=>$data['kelas_id'], "siswa_kelas.tahun_kode"=>$data['tahun_kode']));
			$this->load->view(module_dir().'/export/legger_seluruh', $data);
		} 
	}
}
