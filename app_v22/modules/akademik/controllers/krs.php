<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Krs extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $semester_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'KRS | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Datatable_model');
		$this->load->model('Referensi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Matakuliah_model');
		$this->load->model('Perwalian_model');
		
		$this->semester_kode = $this->Semester_model->get_semester_aktif()->semester_kode;
    }

	public function datatable()
    {
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$where = "(mahasiswa.mahasiswa_tanggal_lulus IS NULL OR mahasiswa.mahasiswa_tanggal_lulus = '0000-00-00') AND mahasiswa.program_studi_id = '$program_studi_id'";

		$tahun_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		if ($tahun_kode != '-'){
			$where .= " AND mahasiswa_tahun_masuk = '$tahun_kode'";
		}
		
		$semester_kode = ($this->uri->segment(6))?$this->uri->segment(6):'-';
		$where .= " AND mahasiswa_kuliah.semester_kode = '$semester_kode'";

		$this->Datatable_model->set_table("(SELECT mahasiswa_kuliah.*, mahasiswa_nama, mahasiswa_nim, mahasiswa_user, mahasiswa_tahun_masuk FROM akd_mahasiswa_kuliah mahasiswa_kuliah LEFT JOIN akd_mahasiswa mahasiswa ON mahasiswa_kuliah.mahasiswa_id=mahasiswa.mahasiswa_id WHERE $where) mahasiswa_kuliah");
		$this->Datatable_model->set_column_order(array('mahasiswa_nim', 'mahasiswa_nama', 'mahasiswa_tahun_masuk', 'mahasiswa_kuliah_sks', null));
		$this->Datatable_model->set_column_search(array('mahasiswa_nim', 'mahasiswa_nama', 'mahasiswa_tahun_masuk', 'mahasiswa_kuliah_sks'));
		$this->Datatable_model->set_order(array('mahasiswa_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
			$row = array();
            $row['nomor'] = $no;
            $row['mahasiswa_id'] = $record->mahasiswa_id;
            $row['mahasiswa_nama'] = $record->mahasiswa_nama;
            $row['mahasiswa_nim'] = $record->mahasiswa_nim;
			$row['mahasiswa_tahun_masuk'] = $record->mahasiswa_tahun_masuk;
			$row['mahasiswa_kuliah_sks'] = $record->mahasiswa_kuliah_sks;
            $row['Actions'] = $this->get_buttons($record->mahasiswa_kuliah_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-success btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-plus"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		
		$semester = $this->Semester_model->get_semester_aktif()->semester_kode;
		$semester_kode				= ($this->uri->segment(6))?$this->uri->segment(6):$semester;
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/krs', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['mahasiswa_kuliah_id']	= validasi_sql($this->uri->segment(4)); 
		$data['mahasiswa_kuliah'] = $this->Mahasiswa_model->get_mahasiswa_kuliah('mahasiswa_kuliah.*, mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, mahasiswa_kuliah.semester_kode, program_studi_nama, semester_nama', $where);
		if (!$data['mahasiswa_kuliah']){
			redirect(module_url($this->uri->segment(2)));
		}		

		$data['semester_kode'] = $this->semester_kode; 
		$data['mahasiswa_id'] = userdata('mahasiswa_id'); 
		$data['program_studi_id'] = userdata('program_studi_id'); 
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/krs', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}