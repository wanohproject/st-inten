<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_kelas extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Jenis Kelas | ' . profile('profil_website');
		$this->active_menu		= 263;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Jenis_kelas_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('jenis_kelas_id, jenis_kelas_kode, jenis_kelas_nama')
		->add_column('Actions', $this->get_buttons('$1'),'jenis_kelas_id')
		->search_column('jenis_kelas_kode, jenis_kelas_nama')
		->from('jenis_kelas');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis_kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['jenis_kelas_nama']	= ($this->input->post('jenis_kelas_nama'))?$this->input->post('jenis_kelas_nama'):'';
		$data['jenis_kelas_kode']	= ($this->input->post('jenis_kelas_kode'))?$this->input->post('jenis_kelas_kode'):'';
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$insert['jenis_kelas_nama']		= validasi_sql($this->input->post('jenis_kelas_nama'));
			$insert['jenis_kelas_kode']		= validasi_sql($this->input->post('jenis_kelas_kode'));
			$this->Jenis_kelas_model->insert_jenis_kelas($insert);
			
			$this->session->set_flashdata('success','Jenis Kelas telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis_kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['jenis_kelas_id']		= validasi_sql($this->uri->segment(4)); 
		$jenis_kelas 					= $this->Jenis_kelas_model->get_jenis_kelas('*', $where);

		$data['jenis_kelas_id']		= ($this->input->post('jenis_kelas_id'))?$this->input->post('jenis_kelas_id'):$jenis_kelas->jenis_kelas_id;
		$data['jenis_kelas_nama']	= ($this->input->post('jenis_kelas_nama'))?$this->input->post('jenis_kelas_nama'):$jenis_kelas->jenis_kelas_nama;
		$data['jenis_kelas_kode']	= ($this->input->post('jenis_kelas_kode'))?$this->input->post('jenis_kelas_kode'):$jenis_kelas->jenis_kelas_kode;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['jenis_kelas_id']	= validasi_sql($this->input->post('jenis_kelas_id'));
			$edit['jenis_kelas_nama']		= validasi_sql($this->input->post('jenis_kelas_nama'));
			$edit['jenis_kelas_kode']		= validasi_sql($this->input->post('jenis_kelas_kode'));
			$this->Jenis_kelas_model->update_jenis_kelas($where_edit, $edit);
			
			$this->session->set_flashdata('success','Jenis Kelas telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis_kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['jenis_kelas_id']	= validasi_sql($this->uri->segment(4));
		$this->Jenis_kelas_model->delete_jenis_kelas($where_delete);
		
		$this->session->set_flashdata('success','Jenis Kelas telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['jenis_kelas_id']	= validasi_sql($this->uri->segment(4)); 
		$jenis_kelas		= $this->Jenis_kelas_model->get_jenis_kelas('*', $where);
		
		$data['jenis_kelas_id']		= $jenis_kelas->jenis_kelas_id;
		$data['jenis_kelas_nama']	= $jenis_kelas->jenis_kelas_nama;
		$data['jenis_kelas_kode']	= $jenis_kelas->jenis_kelas_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jenis_kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
