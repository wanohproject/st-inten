<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tendik extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Tendik | ' . profile('profil_website');
		$this->active_menu		= 261;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Staf_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Guru_pelajaran_model');
		$this->load->model('Nilai_model');
		$this->load->model('Pengguna_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('staf_id, staf_nama, staf_nip, staf_user, staf_pin, staf_user_aktif')
		->add_column('Actions', $this->get_buttons('$1'),'staf_id')
		->add_column('staf_pin', $this->get_genpinstaf('$1', '$2', '$3'),'staf_id, staf_pin, staf_user')
		->search_column('staf_nama, staf_nip, staf_user')
		->from('staf')
		->where("staf_status = 'Tendik'");
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete-user/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus user staf ini.\');" title="Hapus User"><i class="fa fa-remove"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');" title="Hapus"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}
	
	function get_genpinstaf($id, $pin, $nip){
		$ci= & get_instance();
		$ci->load->helper('url');
		$html = $pin . ' <a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/genpinstaf/'.$id.'/'.$nip) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan mereset pin staf ini.\');" title="Reload"><i class="fa fa-refresh"></i></a>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tendik', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['staf_id'] = ($this->input->post('staf_id'))?$this->input->post('staf_id'):'';
		$data['staf_nama'] = ($this->input->post('staf_nama'))?$this->input->post('staf_nama'):'';
		$data['staf_nuptk'] = ($this->input->post('staf_nuptk'))?$this->input->post('staf_nuptk'):'';
		$data['staf_jenis_kelamin'] = ($this->input->post('staf_jenis_kelamin'))?$this->input->post('staf_jenis_kelamin'):'';
		$data['staf_tempat_lahir'] = ($this->input->post('staf_tempat_lahir'))?$this->input->post('staf_tempat_lahir'):'';
		$data['staf_tanggal_lahir'] = ($this->input->post('staf_tanggal_lahir'))?$this->input->post('staf_tanggal_lahir'):'';
		$data['staf_nip'] = ($this->input->post('staf_nip'))?$this->input->post('staf_nip'):'';
		$data['staf_status_kepegawaian'] = ($this->input->post('staf_status_kepegawaian'))?$this->input->post('staf_status_kepegawaian'):'';
		$data['staf_jenis_ptk'] = ($this->input->post('staf_jenis_ptk'))?$this->input->post('staf_jenis_ptk'):'';
		$data['staf_agama'] = ($this->input->post('staf_agama'))?$this->input->post('staf_agama'):'';
		$data['staf_alamat'] = ($this->input->post('staf_alamat'))?$this->input->post('staf_alamat'):'';
		$data['staf_rt'] = ($this->input->post('staf_rt'))?$this->input->post('staf_rt'):'';
		$data['staf_rw'] = ($this->input->post('staf_rw'))?$this->input->post('staf_rw'):'';
		$data['staf_dusun'] = ($this->input->post('staf_dusun'))?$this->input->post('staf_dusun'):'';
		$data['staf_deskel'] = ($this->input->post('staf_deskel'))?$this->input->post('staf_deskel'):'';
		$data['staf_kecamatan'] = ($this->input->post('staf_kecamatan'))?$this->input->post('staf_kecamatan'):'';
		$data['staf_kodepos'] = ($this->input->post('staf_kodepos'))?$this->input->post('staf_kodepos'):'';
		$data['staf_kabkota'] = ($this->input->post('staf_kabkota'))?$this->input->post('staf_kabkota'):'';
		$data['staf_provinsi'] = ($this->input->post('staf_provinsi'))?$this->input->post('staf_provinsi'):'';
		$data['staf_telepon'] = ($this->input->post('staf_telepon'))?$this->input->post('staf_telepon'):'';
		$data['staf_hp'] = ($this->input->post('staf_hp'))?$this->input->post('staf_hp'):'';
		$data['staf_email'] = ($this->input->post('staf_email'))?$this->input->post('staf_email'):'';
		$data['staf_tugas_tambahan'] = ($this->input->post('staf_tugas_tambahan'))?$this->input->post('staf_tugas_tambahan'):'';
		$data['staf_sk_cpns'] = ($this->input->post('staf_sk_cpns'))?$this->input->post('staf_sk_cpns'):'';
		$data['staf_tanggal_cpns'] = ($this->input->post('staf_tanggal_cpns'))?$this->input->post('staf_tanggal_cpns'):'';
		$data['staf_sk_pengangkatan'] = ($this->input->post('staf_sk_pengangkatan'))?$this->input->post('staf_sk_pengangkatan'):'';
		$data['staf_tmt_pengangkatan'] = ($this->input->post('staf_tmt_pengangkatan'))?$this->input->post('staf_tmt_pengangkatan'):'';
		$data['staf_lembaga_pengangkatan'] = ($this->input->post('staf_lembaga_pengangkatan'))?$this->input->post('staf_lembaga_pengangkatan'):'';
		$data['staf_pangkat_golongan'] = ($this->input->post('staf_pangkat_golongan'))?$this->input->post('staf_pangkat_golongan'):'';
		$data['staf_sumber_gaji'] = ($this->input->post('staf_sumber_gaji'))?$this->input->post('staf_sumber_gaji'):'';
		$data['staf_nama_ibu_kandung'] = ($this->input->post('staf_nama_ibu_kandung'))?$this->input->post('staf_nama_ibu_kandung'):'';
		$data['staf_status_perkawinan'] = ($this->input->post('staf_status_perkawinan'))?$this->input->post('staf_status_perkawinan'):'';
		$data['staf_nama_suami_istri'] = ($this->input->post('staf_nama_suami_istri'))?$this->input->post('staf_nama_suami_istri'):'';
		$data['staf_nip_suami_istri'] = ($this->input->post('staf_nip_suami_istri'))?$this->input->post('staf_nip_suami_istri'):'';
		$data['staf_pekerjaan_suami_istri'] = ($this->input->post('staf_pekerjaan_suami_istri'))?$this->input->post('staf_pekerjaan_suami_istri'):'';
		$data['staf_tmt_pns'] = ($this->input->post('staf_tmt_pns'))?$this->input->post('staf_tmt_pns'):'';
		$data['staf_sudah_lisensi_kepala_sekolah'] = ($this->input->post('staf_sudah_lisensi_kepala_sekolah'))?$this->input->post('staf_sudah_lisensi_kepala_sekolah'):'';
		$data['staf_pernah_diklat_kepengawasan'] = ($this->input->post('staf_pernah_diklat_kepengawasan'))?$this->input->post('staf_pernah_diklat_kepengawasan'):'';
		$data['staf_keahlian_braille'] = ($this->input->post('staf_keahlian_braille'))?$this->input->post('staf_keahlian_braille'):'';
		$data['staf_keahlian_bahasa_isyarat'] = ($this->input->post('staf_keahlian_bahasa_isyarat'))?$this->input->post('staf_keahlian_bahasa_isyarat'):'';
		$data['staf_npwp'] = ($this->input->post('staf_npwp'))?$this->input->post('staf_npwp'):'';
		$data['staf_nama_wajib_pajak'] = ($this->input->post('staf_nama_wajib_pajak'))?$this->input->post('staf_nama_wajib_pajak'):'';
		$data['staf_kewarganegaraan'] = ($this->input->post('staf_kewarganegaraan'))?$this->input->post('staf_kewarganegaraan'):'';
		$data['staf_bank'] = ($this->input->post('staf_bank'))?$this->input->post('staf_bank'):'';
		$data['staf_nomor_rekening_bank'] = ($this->input->post('staf_nomor_rekening_bank'))?$this->input->post('staf_nomor_rekening_bank'):'';
		$data['staf_rekening_atas_nama'] = ($this->input->post('staf_rekening_atas_nama'))?$this->input->post('staf_rekening_atas_nama'):'';
		$data['staf_nik'] = ($this->input->post('staf_nik'))?$this->input->post('staf_nik'):'';
		$data['staf_niy'] = ($this->input->post('staf_niy'))?$this->input->post('staf_niy'):'';
		$data['staf_user'] = ($this->input->post('staf_user'))?$this->input->post('staf_user'):'';
		$data['staf_pin'] = ($this->input->post('staf_pin'))?$this->input->post('staf_pin'):'';
		$data['staf_pelajaran'] = ($this->input->post('staf_pelajaran'))?$this->input->post('staf_pelajaran'):'';
		$data['staf_tmt'] = ($this->input->post('staf_tmt'))?$this->input->post('staf_tmt'):'';
		$data['staf_tst'] = ($this->input->post('staf_tst'))?$this->input->post('staf_tst'):'';
		$data['staf_status'] = ($this->input->post('staf_status'))?$this->input->post('staf_status'):'Tendik';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('staf_user'))."'") < 1 && 
				$this->Staf_model->count_all_staf("staf_user = '".validasi_sql($this->input->post('staf_user'))."'") < 1){
				$insert['staf_id'] = validasi_sql($this->input->post('staf_id'));
				$insert['staf_nama'] = validasi_sql($this->input->post('staf_nama'));
				$insert['staf_nuptk'] = validasi_sql($this->input->post('staf_nuptk'));
				$insert['staf_jenis_kelamin'] = validasi_sql($this->input->post('staf_jenis_kelamin'));
				$insert['staf_tempat_lahir'] = validasi_sql($this->input->post('staf_tempat_lahir'));
				$insert['staf_tanggal_lahir'] = (validasi_sql($this->input->post('staf_tanggal_lahir')))?validasi_sql($this->input->post('staf_tanggal_lahir')):null;
				$insert['staf_nip'] = validasi_sql($this->input->post('staf_nip'));
				$insert['staf_status_kepegawaian'] = validasi_sql($this->input->post('staf_status_kepegawaian'));
				$insert['staf_jenis_ptk'] = validasi_sql($this->input->post('staf_jenis_ptk'));
				$insert['staf_agama'] = validasi_sql($this->input->post('staf_agama'));
				$insert['staf_alamat'] = validasi_sql($this->input->post('staf_alamat'));
				$insert['staf_rt'] = validasi_sql($this->input->post('staf_rt'));
				$insert['staf_rw'] = validasi_sql($this->input->post('staf_rw'));
				$insert['staf_dusun'] = validasi_sql($this->input->post('staf_dusun'));
				$insert['staf_deskel'] = validasi_sql($this->input->post('staf_deskel'));
				$insert['staf_kecamatan'] = validasi_sql($this->input->post('staf_kecamatan'));
				$insert['staf_kodepos'] = validasi_sql($this->input->post('staf_kodepos'));
				$insert['staf_kabkota'] = validasi_sql($this->input->post('staf_kabkota'));
				$insert['staf_provinsi'] = validasi_sql($this->input->post('staf_provinsi'));
				$insert['staf_telepon'] = validasi_sql($this->input->post('staf_telepon'));
				$insert['staf_hp'] = validasi_sql($this->input->post('staf_hp'));
				$insert['staf_email'] = validasi_sql($this->input->post('staf_email'));
				$insert['staf_tugas_tambahan'] = validasi_sql($this->input->post('staf_tugas_tambahan'));
				$insert['staf_sk_cpns'] = validasi_sql($this->input->post('staf_sk_cpns'));
				$insert['staf_tanggal_cpns'] = (validasi_sql($this->input->post('staf_tanggal_cpns')))?validasi_sql($this->input->post('staf_tanggal_cpns')):null;
				$insert['staf_sk_pengangkatan'] = validasi_sql($this->input->post('staf_sk_pengangkatan'));
				$insert['staf_tmt_pengangkatan'] = (validasi_sql($this->input->post('staf_tmt_pengangkatan')))?validasi_sql($this->input->post('staf_tmt_pengangkatan')):null;
				$insert['staf_lembaga_pengangkatan'] = validasi_sql($this->input->post('staf_lembaga_pengangkatan'));
				$insert['staf_pangkat_golongan'] = validasi_sql($this->input->post('staf_pangkat_golongan'));
				$insert['staf_sumber_gaji'] = validasi_sql($this->input->post('staf_sumber_gaji'));
				$insert['staf_nama_ibu_kandung'] = validasi_sql($this->input->post('staf_nama_ibu_kandung'));
				$insert['staf_status_perkawinan'] = validasi_sql($this->input->post('staf_status_perkawinan'));
				$insert['staf_nama_suami_istri'] = validasi_sql($this->input->post('staf_nama_suami_istri'));
				$insert['staf_nip_suami_istri'] = validasi_sql($this->input->post('staf_nip_suami_istri'));
				$insert['staf_pekerjaan_suami_istri'] = validasi_sql($this->input->post('staf_pekerjaan_suami_istri'));
				$insert['staf_tmt_pns'] = (validasi_sql($this->input->post('staf_tmt_pns')))?validasi_sql($this->input->post('staf_tmt_pns')):null;
				$insert['staf_sudah_lisensi_kepala_sekolah'] = validasi_sql($this->input->post('staf_sudah_lisensi_kepala_sekolah'));
				$insert['staf_pernah_diklat_kepengawasan'] = validasi_sql($this->input->post('staf_pernah_diklat_kepengawasan'));
				$insert['staf_keahlian_braille'] = validasi_sql($this->input->post('staf_keahlian_braille'));
				$insert['staf_keahlian_bahasa_isyarat'] = validasi_sql($this->input->post('staf_keahlian_bahasa_isyarat'));
				$insert['staf_npwp'] = validasi_sql($this->input->post('staf_npwp'));
				$insert['staf_nama_wajib_pajak'] = validasi_sql($this->input->post('staf_nama_wajib_pajak'));
				$insert['staf_kewarganegaraan'] = validasi_sql($this->input->post('staf_kewarganegaraan'));
				$insert['staf_bank'] = validasi_sql($this->input->post('staf_bank'));
				$insert['staf_nomor_rekening_bank'] = validasi_sql($this->input->post('staf_nomor_rekening_bank'));
				$insert['staf_rekening_atas_nama'] = validasi_sql($this->input->post('staf_rekening_atas_nama'));
				$insert['staf_nik'] = validasi_sql($this->input->post('staf_nik'));
				$insert['staf_niy'] = validasi_sql($this->input->post('staf_niy'));
				$insert['staf_user'] = validasi_sql($this->input->post('staf_user'));
				$insert['staf_pin'] = validasi_sql($this->input->post('staf_pin'));
				$insert['staf_pelajaran'] = validasi_sql($this->input->post('staf_pelajaran'));
				$insert['staf_tmt'] = validasi_sql($this->input->post('staf_tmt'));
				$insert['staf_tst'] = validasi_sql($this->input->post('staf_tst'));
				$insert['staf_status'] = validasi_sql($this->input->post('staf_status'));
				$insert['staf_last_update'] = date('Y-m-d H:i:s');
						
				$this->Staf_model->insert_staf($insert);
				
				$this->session->set_flashdata('success','Tendik telah berhasil ditambah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User telah digunakan. Silahkan gunakan user yang lain.');
				redirect(module_url($this->uri->segment(2)));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tendik', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['staf_id']		= validasi_sql($this->uri->segment(4)); 
		$staf 					= $this->Staf_model->get_staf('*', $where);

		$data['staf_id'] = ($this->input->post('staf_id'))?$this->input->post('staf_id'):$staf->staf_id;
		$data['staf_nama'] = ($this->input->post('staf_nama'))?$this->input->post('staf_nama'):$staf->staf_nama;
		$data['staf_nuptk'] = ($this->input->post('staf_nuptk'))?$this->input->post('staf_nuptk'):$staf->staf_nuptk;
		$data['staf_jenis_kelamin'] = ($this->input->post('staf_jenis_kelamin'))?$this->input->post('staf_jenis_kelamin'):$staf->staf_jenis_kelamin;
		$data['staf_tempat_lahir'] = ($this->input->post('staf_tempat_lahir'))?$this->input->post('staf_tempat_lahir'):$staf->staf_tempat_lahir;
		$data['staf_tanggal_lahir'] = ($this->input->post('staf_tanggal_lahir'))?$this->input->post('staf_tanggal_lahir'):$staf->staf_tanggal_lahir;
		$data['staf_nip'] = ($this->input->post('staf_nip'))?$this->input->post('staf_nip'):$staf->staf_nip;
		$data['staf_status_kepegawaian'] = ($this->input->post('staf_status_kepegawaian'))?$this->input->post('staf_status_kepegawaian'):$staf->staf_status_kepegawaian;
		$data['staf_jenis_ptk'] = ($this->input->post('staf_jenis_ptk'))?$this->input->post('staf_jenis_ptk'):$staf->staf_jenis_ptk;
		$data['staf_agama'] = ($this->input->post('staf_agama'))?$this->input->post('staf_agama'):$staf->staf_agama;
		$data['staf_alamat'] = ($this->input->post('staf_alamat'))?$this->input->post('staf_alamat'):$staf->staf_alamat;
		$data['staf_rt'] = ($this->input->post('staf_rt'))?$this->input->post('staf_rt'):$staf->staf_rt;
		$data['staf_rw'] = ($this->input->post('staf_rw'))?$this->input->post('staf_rw'):$staf->staf_rw;
		$data['staf_dusun'] = ($this->input->post('staf_dusun'))?$this->input->post('staf_dusun'):$staf->staf_dusun;
		$data['staf_deskel'] = ($this->input->post('staf_deskel'))?$this->input->post('staf_deskel'):$staf->staf_deskel;
		$data['staf_kecamatan'] = ($this->input->post('staf_kecamatan'))?$this->input->post('staf_kecamatan'):$staf->staf_kecamatan;
		$data['staf_kodepos'] = ($this->input->post('staf_kodepos'))?$this->input->post('staf_kodepos'):$staf->staf_kodepos;
		$data['staf_kabkota'] = ($this->input->post('staf_kabkota'))?$this->input->post('staf_kabkota'):$staf->staf_kabkota;
		$data['staf_provinsi'] = ($this->input->post('staf_provinsi'))?$this->input->post('staf_provinsi'):$staf->staf_provinsi;
		$data['staf_telepon'] = ($this->input->post('staf_telepon'))?$this->input->post('staf_telepon'):$staf->staf_telepon;
		$data['staf_hp'] = ($this->input->post('staf_hp'))?$this->input->post('staf_hp'):$staf->staf_hp;
		$data['staf_email'] = ($this->input->post('staf_email'))?$this->input->post('staf_email'):$staf->staf_email;
		$data['staf_tugas_tambahan'] = ($this->input->post('staf_tugas_tambahan'))?$this->input->post('staf_tugas_tambahan'):$staf->staf_tugas_tambahan;
		$data['staf_sk_cpns'] = ($this->input->post('staf_sk_cpns'))?$this->input->post('staf_sk_cpns'):$staf->staf_sk_cpns;
		$data['staf_tanggal_cpns'] = ($this->input->post('staf_tanggal_cpns'))?$this->input->post('staf_tanggal_cpns'):$staf->staf_tanggal_cpns;
		$data['staf_sk_pengangkatan'] = ($this->input->post('staf_sk_pengangkatan'))?$this->input->post('staf_sk_pengangkatan'):$staf->staf_sk_pengangkatan;
		$data['staf_tmt_pengangkatan'] = ($this->input->post('staf_tmt_pengangkatan'))?$this->input->post('staf_tmt_pengangkatan'):$staf->staf_tmt_pengangkatan;
		$data['staf_lembaga_pengangkatan'] = ($this->input->post('staf_lembaga_pengangkatan'))?$this->input->post('staf_lembaga_pengangkatan'):$staf->staf_lembaga_pengangkatan;
		$data['staf_pangkat_golongan'] = ($this->input->post('staf_pangkat_golongan'))?$this->input->post('staf_pangkat_golongan'):$staf->staf_pangkat_golongan;
		$data['staf_sumber_gaji'] = ($this->input->post('staf_sumber_gaji'))?$this->input->post('staf_sumber_gaji'):$staf->staf_sumber_gaji;
		$data['staf_nama_ibu_kandung'] = ($this->input->post('staf_nama_ibu_kandung'))?$this->input->post('staf_nama_ibu_kandung'):$staf->staf_nama_ibu_kandung;
		$data['staf_status_perkawinan'] = ($this->input->post('staf_status_perkawinan'))?$this->input->post('staf_status_perkawinan'):$staf->staf_status_perkawinan;
		$data['staf_nama_suami_istri'] = ($this->input->post('staf_nama_suami_istri'))?$this->input->post('staf_nama_suami_istri'):$staf->staf_nama_suami_istri;
		$data['staf_nip_suami_istri'] = ($this->input->post('staf_nip_suami_istri'))?$this->input->post('staf_nip_suami_istri'):$staf->staf_nip_suami_istri;
		$data['staf_pekerjaan_suami_istri'] = ($this->input->post('staf_pekerjaan_suami_istri'))?$this->input->post('staf_pekerjaan_suami_istri'):$staf->staf_pekerjaan_suami_istri;
		$data['staf_tmt_pns'] = ($this->input->post('staf_tmt_pns'))?$this->input->post('staf_tmt_pns'):$staf->staf_tmt_pns;
		$data['staf_sudah_lisensi_kepala_sekolah'] = ($this->input->post('staf_sudah_lisensi_kepala_sekolah'))?$this->input->post('staf_sudah_lisensi_kepala_sekolah'):$staf->staf_sudah_lisensi_kepala_sekolah;
		$data['staf_pernah_diklat_kepengawasan'] = ($this->input->post('staf_pernah_diklat_kepengawasan'))?$this->input->post('staf_pernah_diklat_kepengawasan'):$staf->staf_pernah_diklat_kepengawasan;
		$data['staf_keahlian_braille'] = ($this->input->post('staf_keahlian_braille'))?$this->input->post('staf_keahlian_braille'):$staf->staf_keahlian_braille;
		$data['staf_keahlian_bahasa_isyarat'] = ($this->input->post('staf_keahlian_bahasa_isyarat'))?$this->input->post('staf_keahlian_bahasa_isyarat'):$staf->staf_keahlian_bahasa_isyarat;
		$data['staf_npwp'] = ($this->input->post('staf_npwp'))?$this->input->post('staf_npwp'):$staf->staf_npwp;
		$data['staf_nama_wajib_pajak'] = ($this->input->post('staf_nama_wajib_pajak'))?$this->input->post('staf_nama_wajib_pajak'):$staf->staf_nama_wajib_pajak;
		$data['staf_kewarganegaraan'] = ($this->input->post('staf_kewarganegaraan'))?$this->input->post('staf_kewarganegaraan'):$staf->staf_kewarganegaraan;
		$data['staf_bank'] = ($this->input->post('staf_bank'))?$this->input->post('staf_bank'):$staf->staf_bank;
		$data['staf_nomor_rekening_bank'] = ($this->input->post('staf_nomor_rekening_bank'))?$this->input->post('staf_nomor_rekening_bank'):$staf->staf_nomor_rekening_bank;
		$data['staf_rekening_atas_nama'] = ($this->input->post('staf_rekening_atas_nama'))?$this->input->post('staf_rekening_atas_nama'):$staf->staf_rekening_atas_nama;
		$data['staf_nik'] = ($this->input->post('staf_nik'))?$this->input->post('staf_nik'):$staf->staf_nik;
		$data['staf_niy'] = ($this->input->post('staf_niy'))?$this->input->post('staf_niy'):$staf->staf_niy;
		$data['staf_user'] = ($this->input->post('staf_user'))?$this->input->post('staf_user'):$staf->staf_user;
		$data['staf_pin'] = ($this->input->post('staf_pin'))?$this->input->post('staf_pin'):$staf->staf_pin;
		$data['staf_pelajaran'] = ($this->input->post('staf_pelajaran'))?$this->input->post('staf_pelajaran'):$staf->staf_pelajaran;
		$data['staf_tmt'] = ($this->input->post('staf_tmt'))?$this->input->post('staf_tmt'):$staf->staf_tmt;
		$data['staf_tst'] = ($this->input->post('staf_tst'))?$this->input->post('staf_tst'):$staf->staf_tst;
		$data['staf_status'] = ($this->input->post('staf_status'))?$this->input->post('staf_status'):$staf->staf_status;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('staf_user'))."' && pengguna_nama != '".$staf->staf_user."'") < 1 && 
				$this->Staf_model->count_all_staf("staf_user = '".validasi_sql($this->input->post('staf_user'))."' && staf_user != '".$staf->staf_user."'") < 1){
				
				$where_edit['staf_id']	= validasi_sql($this->input->post('staf_id'));
				$edit['staf_nama'] = validasi_sql($this->input->post('staf_nama'));
				$edit['staf_nuptk'] = validasi_sql($this->input->post('staf_nuptk'));
				$edit['staf_jenis_kelamin'] = validasi_sql($this->input->post('staf_jenis_kelamin'));
				$edit['staf_tempat_lahir'] = validasi_sql($this->input->post('staf_tempat_lahir'));
				$edit['staf_tanggal_lahir'] = (validasi_sql($this->input->post('staf_tanggal_lahir')))?validasi_sql($this->input->post('staf_tanggal_lahir')):null;
				$edit['staf_nip'] = validasi_sql($this->input->post('staf_nip'));
				$edit['staf_status_kepegawaian'] = validasi_sql($this->input->post('staf_status_kepegawaian'));
				$edit['staf_jenis_ptk'] = validasi_sql($this->input->post('staf_jenis_ptk'));
				$edit['staf_agama'] = validasi_sql($this->input->post('staf_agama'));
				$edit['staf_alamat'] = validasi_sql($this->input->post('staf_alamat'));
				$edit['staf_rt'] = validasi_sql($this->input->post('staf_rt'));
				$edit['staf_rw'] = validasi_sql($this->input->post('staf_rw'));
				$edit['staf_dusun'] = validasi_sql($this->input->post('staf_dusun'));
				$edit['staf_deskel'] = validasi_sql($this->input->post('staf_deskel'));
				$edit['staf_kecamatan'] = validasi_sql($this->input->post('staf_kecamatan'));
				$edit['staf_kodepos'] = validasi_sql($this->input->post('staf_kodepos'));
				$edit['staf_kabkota'] = validasi_sql($this->input->post('staf_kabkota'));
				$edit['staf_provinsi'] = validasi_sql($this->input->post('staf_provinsi'));
				$edit['staf_telepon'] = validasi_sql($this->input->post('staf_telepon'));
				$edit['staf_hp'] = validasi_sql($this->input->post('staf_hp'));
				$edit['staf_email'] = validasi_sql($this->input->post('staf_email'));
				$edit['staf_tugas_tambahan'] = validasi_sql($this->input->post('staf_tugas_tambahan'));
				$edit['staf_sk_cpns'] = validasi_sql($this->input->post('staf_sk_cpns'));
				$edit['staf_tanggal_cpns'] = (validasi_sql($this->input->post('staf_tanggal_cpns')))?validasi_sql($this->input->post('staf_tanggal_cpns')):null;
				$edit['staf_sk_pengangkatan'] = validasi_sql($this->input->post('staf_sk_pengangkatan'));
				$edit['staf_tmt_pengangkatan'] = (validasi_sql($this->input->post('staf_tmt_pengangkatan')))?validasi_sql($this->input->post('staf_tmt_pengangkatan')):null;
				$edit['staf_lembaga_pengangkatan'] = validasi_sql($this->input->post('staf_lembaga_pengangkatan'));
				$edit['staf_pangkat_golongan'] = validasi_sql($this->input->post('staf_pangkat_golongan'));
				$edit['staf_sumber_gaji'] = validasi_sql($this->input->post('staf_sumber_gaji'));
				$edit['staf_nama_ibu_kandung'] = validasi_sql($this->input->post('staf_nama_ibu_kandung'));
				$edit['staf_status_perkawinan'] = validasi_sql($this->input->post('staf_status_perkawinan'));
				$edit['staf_nama_suami_istri'] = validasi_sql($this->input->post('staf_nama_suami_istri'));
				$edit['staf_nip_suami_istri'] = validasi_sql($this->input->post('staf_nip_suami_istri'));
				$edit['staf_pekerjaan_suami_istri'] = validasi_sql($this->input->post('staf_pekerjaan_suami_istri'));
				$edit['staf_tmt_pns'] = (validasi_sql($this->input->post('staf_tmt_pns')))?validasi_sql($this->input->post('staf_tmt_pns')):null;
				$edit['staf_sudah_lisensi_kepala_sekolah'] = validasi_sql($this->input->post('staf_sudah_lisensi_kepala_sekolah'));
				$edit['staf_pernah_diklat_kepengawasan'] = validasi_sql($this->input->post('staf_pernah_diklat_kepengawasan'));
				$edit['staf_keahlian_braille'] = validasi_sql($this->input->post('staf_keahlian_braille'));
				$edit['staf_keahlian_bahasa_isyarat'] = validasi_sql($this->input->post('staf_keahlian_bahasa_isyarat'));
				$edit['staf_npwp'] = validasi_sql($this->input->post('staf_npwp'));
				$edit['staf_nama_wajib_pajak'] = validasi_sql($this->input->post('staf_nama_wajib_pajak'));
				$edit['staf_kewarganegaraan'] = validasi_sql($this->input->post('staf_kewarganegaraan'));
				$edit['staf_bank'] = validasi_sql($this->input->post('staf_bank'));
				$edit['staf_nomor_rekening_bank'] = validasi_sql($this->input->post('staf_nomor_rekening_bank'));
				$edit['staf_rekening_atas_nama'] = validasi_sql($this->input->post('staf_rekening_atas_nama'));
				$edit['staf_nik'] = validasi_sql($this->input->post('staf_nik'));
				$edit['staf_niy'] = validasi_sql($this->input->post('staf_niy'));
				$edit['staf_user'] = validasi_sql($this->input->post('staf_user'));
				$edit['staf_pin'] = validasi_sql($this->input->post('staf_pin'));
				$edit['staf_pelajaran'] = validasi_sql($this->input->post('staf_pelajaran'));
				$edit['staf_tmt'] = validasi_sql($this->input->post('staf_tmt'));
				$edit['staf_tst'] = validasi_sql($this->input->post('staf_tst'));
				$edit['staf_status'] = validasi_sql($this->input->post('staf_status'));
				
				$this->Staf_model->update_staf($where_edit, $edit);
				
				$staf_id	= validasi_sql($this->input->post('staf_id'));
				$staf_user	= validasi_sql($this->input->post('staf_user'));
				$where_pengguna['staf.staf_id']		= $staf_id;
				if ($this->Pengguna_model->count_all_pengguna($where_pengguna) > 0){
					$update_pengguna['pengguna_nama']	= $staf_user;
					$this->Pengguna_model->update_pengguna(array('staf_id'=>$staf_id), array('pengguna_nama'=>$staf_user));
				}
				$this->session->set_flashdata('success','Tendik telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User telah digunakan. Silahkan gunakan user yang lain.');
				redirect(module_url($this->uri->segment(2)));
			}
			
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tendik', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$staf_id			= validasi_sql($this->uri->segment(4));
		if ($this->Guru_pelajaran_model->count_all_guru_pelajaran(array('guru_pelajaran.staf_id'=>$staf_id)) < 1 
			&& $this->Nilai_model->count_all_nilai(array('nilai.staf_id'=>$staf_id)) < 1){

			$where_delete_staf['staf_id']	= $staf_id;
			$this->Staf_model->delete_staf($where_delete_staf);
			
			$where_delete_pengguna['staf_id']	= $staf_id;
			$where_delete_pengguna['pengguna_level_id']	= 10;
			$this->Pengguna_model->delete_pengguna($where_delete_pengguna);
				
			$this->session->set_flashdata('success','Tendik telah berhasil dihapus.');
		} else {
			$this->session->set_flashdata('error','Tendik gagal dihapus.');
		}
		
		redirect(module_url($this->uri->segment(2)));
	}

	public function delete_user()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$staf_id			= validasi_sql($this->uri->segment(4));
		
		$update_pin['staf_user_aktif']			= 'N';
		$this->Staf_model->update_staf(array('staf_id'=>$staf_id), $update_pin);

		$where_delete_pengguna['staf_id']	= $staf_id;
		$where_delete_pengguna['pengguna_level_id']	= 10;
		$this->Pengguna_model->delete_pengguna($where_delete_pengguna);
			
		$this->session->set_flashdata('success','Tendik telah berhasil dihapus.');
		
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function import()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'import';
		
		$data['dataExcel']				= array();		
		$data['filename']				= '';		

		if ($this->input->post('importStaf')){
			if ($_FILES['userfile']['tmp_name']){
				$ext = end(explode(".", basename($_FILES['userfile']['name'])));
				if ($ext == 'xls'){
					
					$timestamp = explode(" ",microtime());
					$filename = time().str_shuffle('123456ABCDEF');
					
					$uploaddir = './asset/temp_upload/';
					$uploadfname = 'staf_'.$filename . '.' . end(explode(".", basename($_FILES['userfile']['name'])));
					$uploadfile = $uploaddir . $uploadfname;
					$data['filename'] = $uploadfname;
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
						$this->load->library('excel_reader');
						$this->excel_reader->setOutputEncoding('CP1251');
						$this->excel_reader->read($uploadfile);
						$dataFile = $this->excel_reader->sheets[0];

						// print_r('<pre>');
						// print_r($dataFile);
						// print_r('</pre>');
						
						for ($i = 4; $i <= $dataFile['numRows']; $i++) {
							if($dataFile['cells'][$i][1]){
								$data['dataExcel'][$i]['staf_user'] 						= (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
								$data['dataExcel'][$i]['staf_pin'] 							= (isset($dataFile['cells'][$i][3]))?$dataFile['cells'][$i][3]:'';
								$data['dataExcel'][$i]['staf_nama'] 						= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
								$data['dataExcel'][$i]['staf_nuptk'] 						= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
								$data['dataExcel'][$i]['staf_jenis_kelamin'] 				= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
								$data['dataExcel'][$i]['staf_tempat_lahir'] 				= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
								$data['dataExcel'][$i]['staf_tanggal_lahir'] 				= (isset($dataFile['cells'][$i][8]))?$dataFile['cells'][$i][8]:'';
								$data['dataExcel'][$i]['staf_nip'] 							= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
								$data['dataExcel'][$i]['staf_status_kepegawaian'] 			= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
								$data['dataExcel'][$i]['staf_jenis_ptk'] 					= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';
								$data['dataExcel'][$i]['staf_agama'] 						= (isset($dataFile['cells'][$i][12]))?$dataFile['cells'][$i][12]:'';
								$data['dataExcel'][$i]['staf_alamat'] 						= (isset($dataFile['cells'][$i][13]))?$dataFile['cells'][$i][13]:'';
								$data['dataExcel'][$i]['staf_rt'] 							= (isset($dataFile['cells'][$i][14]))?$dataFile['cells'][$i][14]:'';
								$data['dataExcel'][$i]['staf_rw'] 							= (isset($dataFile['cells'][$i][15]))?$dataFile['cells'][$i][15]:'';
								$data['dataExcel'][$i]['staf_dusun'] 						= (isset($dataFile['cells'][$i][16]))?$dataFile['cells'][$i][16]:'';
								$data['dataExcel'][$i]['staf_deskel'] 						= (isset($dataFile['cells'][$i][17]))?$dataFile['cells'][$i][17]:'';
								$data['dataExcel'][$i]['staf_kecamatan'] 					= (isset($dataFile['cells'][$i][18]))?$dataFile['cells'][$i][18]:'';
								$data['dataExcel'][$i]['staf_kodepos'] 						= (isset($dataFile['cells'][$i][19]))?$dataFile['cells'][$i][19]:'';
								$data['dataExcel'][$i]['staf_telepon'] 						= (isset($dataFile['cells'][$i][20]))?$dataFile['cells'][$i][20]:'';
								$data['dataExcel'][$i]['staf_hp'] 							= (isset($dataFile['cells'][$i][21]))?$dataFile['cells'][$i][21]:'';
								$data['dataExcel'][$i]['staf_email'] 						= (isset($dataFile['cells'][$i][22]))?$dataFile['cells'][$i][22]:'';
								$data['dataExcel'][$i]['staf_tugas_tambahan'] 				= (isset($dataFile['cells'][$i][23]))?$dataFile['cells'][$i][23]:'';
								$data['dataExcel'][$i]['staf_sk_cpns'] 						= (isset($dataFile['cells'][$i][24]))?$dataFile['cells'][$i][24]:'';
								$data['dataExcel'][$i]['staf_tanggal_cpns'] 				= (isset($dataFile['cells'][$i][25]))?$dataFile['cells'][$i][25]:'';
								$data['dataExcel'][$i]['staf_sk_pengangkatan'] 				= (isset($dataFile['cells'][$i][26]))?$dataFile['cells'][$i][26]:'';
								$data['dataExcel'][$i]['staf_tmt_pengangkatan'] 			= (isset($dataFile['cells'][$i][27]))?$dataFile['cells'][$i][27]:'';
								$data['dataExcel'][$i]['staf_lembaga_pengangkatan'] 		= (isset($dataFile['cells'][$i][28]))?$dataFile['cells'][$i][28]:'';
								$data['dataExcel'][$i]['staf_pangkat_golongan'] 			= (isset($dataFile['cells'][$i][29]))?$dataFile['cells'][$i][29]:'';
								$data['dataExcel'][$i]['staf_sumber_gaji'] 					= (isset($dataFile['cells'][$i][30]))?$dataFile['cells'][$i][30]:'';
								$data['dataExcel'][$i]['staf_nama_ibu_kandung'] 			= (isset($dataFile['cells'][$i][31]))?$dataFile['cells'][$i][31]:'';
								$data['dataExcel'][$i]['staf_status_perkawinan'] 			= (isset($dataFile['cells'][$i][32]))?$dataFile['cells'][$i][32]:'';
								$data['dataExcel'][$i]['staf_nama_suami_istri'] 			= (isset($dataFile['cells'][$i][33]))?$dataFile['cells'][$i][33]:'';
								$data['dataExcel'][$i]['staf_nip_suami_istri'] 				= (isset($dataFile['cells'][$i][34]))?$dataFile['cells'][$i][34]:'';
								$data['dataExcel'][$i]['staf_pekerjaan_suami_istri'] 		= (isset($dataFile['cells'][$i][35]))?$dataFile['cells'][$i][35]:'';
								$data['dataExcel'][$i]['staf_tmt_pns'] 						= (isset($dataFile['cells'][$i][36]))?$dataFile['cells'][$i][36]:'';
								$data['dataExcel'][$i]['staf_sudah_lisensi_kepala_sekolah'] = (isset($dataFile['cells'][$i][37]))?$dataFile['cells'][$i][37]:'';
								$data['dataExcel'][$i]['staf_pernah_diklat_kepengawasan'] 	= (isset($dataFile['cells'][$i][38]))?$dataFile['cells'][$i][38]:'';
								$data['dataExcel'][$i]['staf_keahlian_braille'] 			= (isset($dataFile['cells'][$i][39]))?$dataFile['cells'][$i][39]:'';
								$data['dataExcel'][$i]['staf_keahlian_bahasa_isyarat'] 		= (isset($dataFile['cells'][$i][40]))?$dataFile['cells'][$i][40]:'';
								$data['dataExcel'][$i]['staf_npwp'] 						= (isset($dataFile['cells'][$i][41]))?$dataFile['cells'][$i][41]:'';
								$data['dataExcel'][$i]['staf_nama_wajib_pajak'] 			= (isset($dataFile['cells'][$i][42]))?$dataFile['cells'][$i][42]:'';
								$data['dataExcel'][$i]['staf_kewarganegaraan'] 				= (isset($dataFile['cells'][$i][43]))?$dataFile['cells'][$i][43]:'';
								$data['dataExcel'][$i]['staf_bank'] 						= (isset($dataFile['cells'][$i][44]))?$dataFile['cells'][$i][44]:'';
								$data['dataExcel'][$i]['staf_nomor_rekening_bank'] 			= (isset($dataFile['cells'][$i][45]))?$dataFile['cells'][$i][45]:'';
								$data['dataExcel'][$i]['staf_rekening_atas_nama'] 			= (isset($dataFile['cells'][$i][46]))?$dataFile['cells'][$i][46]:'';
								$data['dataExcel'][$i]['staf_nik'] 							= (isset($dataFile['cells'][$i][47]))?$dataFile['cells'][$i][47]:'';
							}
						}
					} else {
						$this->session->set_flashdata('error','File gagal di-upload.');
						redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
					}
				} else {
					$this->session->set_flashdata('error','Format file yang Anda gunakan salah. Silahkan gunakan format file Excel 97-2003 (.xls).');
					redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Silahkan masukkan file yang akan di-import.');
				redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
			}
		}
		
		if ($this->input->post('save')){
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$this->excel_reader->read('./asset/temp_upload/'.$this->input->post('filename'));
			$dataFile = $this->excel_reader->sheets[0];
			
			for ($i = 4; $i <= $dataFile['numRows']; $i++) {
				if($dataFile['cells'][$i][1]){
					$staf_user = (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
					
					if ($this->Staf_model->count_all_staf(array("staf.staf_user"=>$staf_user)) < 1){
						$insert_staf['staf_user'] 						= (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
						$insert_staf['staf_pin'] 						= (isset($dataFile['cells'][$i][3]))?onlyNumber($dataFile['cells'][$i][3]):null;
						$insert_staf['staf_nama'] 						= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
						$insert_staf['staf_nuptk'] 						= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
						$insert_staf['staf_jenis_kelamin'] 				= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
						$insert_staf['staf_tempat_lahir'] 				= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
						$insert_staf['staf_tanggal_lahir'] 				= (isset($dataFile['cells'][$i][8]))?dateForDB($dataFile['cells'][$i][8]):null;
						$insert_staf['staf_nip'] 						= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
						$insert_staf['staf_status_kepegawaian'] 		= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
						$insert_staf['staf_jenis_ptk'] 					= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';
						$insert_staf['staf_agama'] 						= (isset($dataFile['cells'][$i][12]))?$dataFile['cells'][$i][12]:'';
						$insert_staf['staf_alamat'] 					= (isset($dataFile['cells'][$i][13]))?$dataFile['cells'][$i][13]:'';
						$insert_staf['staf_rt'] 						= (isset($dataFile['cells'][$i][14]))?$dataFile['cells'][$i][14]:'';
						$insert_staf['staf_rw'] 						= (isset($dataFile['cells'][$i][15]))?$dataFile['cells'][$i][15]:'';
						$insert_staf['staf_dusun'] 						= (isset($dataFile['cells'][$i][16]))?$dataFile['cells'][$i][16]:'';
						$insert_staf['staf_deskel'] 					= (isset($dataFile['cells'][$i][17]))?$dataFile['cells'][$i][17]:'';
						$insert_staf['staf_kecamatan'] 					= (isset($dataFile['cells'][$i][18]))?$dataFile['cells'][$i][18]:'';
						$insert_staf['staf_kodepos'] 					= (isset($dataFile['cells'][$i][19]))?$dataFile['cells'][$i][19]:'';
						$insert_staf['staf_telepon'] 					= (isset($dataFile['cells'][$i][20]))?$dataFile['cells'][$i][20]:'';
						$insert_staf['staf_hp'] 						= (isset($dataFile['cells'][$i][21]))?$dataFile['cells'][$i][21]:'';
						$insert_staf['staf_email'] 						= (isset($dataFile['cells'][$i][22]))?$dataFile['cells'][$i][22]:'';
						$insert_staf['staf_tugas_tambahan'] 			= (isset($dataFile['cells'][$i][23]))?$dataFile['cells'][$i][23]:'';
						$insert_staf['staf_sk_cpns'] 					= (isset($dataFile['cells'][$i][24]))?$dataFile['cells'][$i][24]:'';
						$insert_staf['staf_tanggal_cpns'] 				= (isset($dataFile['cells'][$i][25]))?dateForDB($dataFile['cells'][$i][25]):null;
						$insert_staf['staf_sk_pengangkatan'] 			= (isset($dataFile['cells'][$i][26]))?$dataFile['cells'][$i][26]:'';
						$insert_staf['staf_tmt_pengangkatan'] 			= (isset($dataFile['cells'][$i][27]))?dateForDB($dataFile['cells'][$i][27]):null;
						$insert_staf['staf_lembaga_pengangkatan'] 		= (isset($dataFile['cells'][$i][28]))?$dataFile['cells'][$i][28]:'';
						$insert_staf['staf_pangkat_golongan'] 			= (isset($dataFile['cells'][$i][29]))?$dataFile['cells'][$i][29]:'';
						$insert_staf['staf_sumber_gaji'] 				= (isset($dataFile['cells'][$i][30]))?$dataFile['cells'][$i][30]:'';
						$insert_staf['staf_nama_ibu_kandung'] 			= (isset($dataFile['cells'][$i][31]))?$dataFile['cells'][$i][31]:'';
						$insert_staf['staf_status_perkawinan'] 			= (isset($dataFile['cells'][$i][32]))?$dataFile['cells'][$i][32]:'';
						$insert_staf['staf_nama_suami_istri'] 			= (isset($dataFile['cells'][$i][33]))?$dataFile['cells'][$i][33]:'';
						$insert_staf['staf_nip_suami_istri'] 			= (isset($dataFile['cells'][$i][34]))?$dataFile['cells'][$i][34]:'';
						$insert_staf['staf_pekerjaan_suami_istri'] 		= (isset($dataFile['cells'][$i][35]))?$dataFile['cells'][$i][35]:'';
						$insert_staf['staf_tmt_pns'] 					= (isset($dataFile['cells'][$i][36]))?dateForDB($dataFile['cells'][$i][36]):null;
						$insert_staf['staf_sudah_lisensi_kepala_sekolah'] = (isset($dataFile['cells'][$i][37]))?$dataFile['cells'][$i][37]:'';
						$insert_staf['staf_pernah_diklat_kepengawasan'] = (isset($dataFile['cells'][$i][38]))?$dataFile['cells'][$i][38]:'';
						$insert_staf['staf_keahlian_braille'] 			= (isset($dataFile['cells'][$i][39]))?$dataFile['cells'][$i][39]:'';
						$insert_staf['staf_keahlian_bahasa_isyarat'] 	= (isset($dataFile['cells'][$i][40]))?$dataFile['cells'][$i][40]:'';
						$insert_staf['staf_npwp'] 						= (isset($dataFile['cells'][$i][41]))?$dataFile['cells'][$i][41]:'';
						$insert_staf['staf_nama_wajib_pajak'] 			= (isset($dataFile['cells'][$i][42]))?$dataFile['cells'][$i][42]:'';
						$insert_staf['staf_kewarganegaraan'] 			= (isset($dataFile['cells'][$i][43]))?$dataFile['cells'][$i][43]:'';
						$insert_staf['staf_bank'] 						= (isset($dataFile['cells'][$i][44]))?$dataFile['cells'][$i][44]:'';
						$insert_staf['staf_nomor_rekening_bank'] 		= (isset($dataFile['cells'][$i][45]))?$dataFile['cells'][$i][45]:'';
						$insert_staf['staf_rekening_atas_nama'] 		= (isset($dataFile['cells'][$i][46]))?$dataFile['cells'][$i][46]:'';
						$insert_staf['staf_nik'] 						= (isset($dataFile['cells'][$i][47]))?$dataFile['cells'][$i][47]:'';
						$insert_staf['staf_last_update'] 				= date('Y-m-d H:i:s');
						$insert_staf['staf_status'] 					= 'Tendik';
						$this->Staf_model->insert_staf($insert_staf);
					} else {
						$update_staf['staf_pin'] 						= (isset($dataFile['cells'][$i][3]))?onlyNumber($dataFile['cells'][$i][3]):'';
						$update_staf['staf_nama'] 						= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
						$update_staf['staf_nuptk'] 						= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
						$update_staf['staf_jenis_kelamin'] 				= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
						$update_staf['staf_tempat_lahir'] 				= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
						$update_staf['staf_tanggal_lahir'] 				= (isset($dataFile['cells'][$i][8]))?dateForDB($dataFile['cells'][$i][8]):null;
						$update_staf['staf_nip'] 						= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
						$update_staf['staf_status_kepegawaian'] 		= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
						$update_staf['staf_jenis_ptk'] 					= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';
						$update_staf['staf_agama'] 						= (isset($dataFile['cells'][$i][12]))?$dataFile['cells'][$i][12]:'';
						$update_staf['staf_alamat'] 					= (isset($dataFile['cells'][$i][13]))?$dataFile['cells'][$i][13]:'';
						$update_staf['staf_rt'] 						= (isset($dataFile['cells'][$i][14]))?$dataFile['cells'][$i][14]:'';
						$update_staf['staf_rw'] 						= (isset($dataFile['cells'][$i][15]))?$dataFile['cells'][$i][15]:'';
						$update_staf['staf_dusun'] 						= (isset($dataFile['cells'][$i][16]))?$dataFile['cells'][$i][16]:'';
						$update_staf['staf_deskel'] 					= (isset($dataFile['cells'][$i][17]))?$dataFile['cells'][$i][17]:'';
						$update_staf['staf_kecamatan'] 					= (isset($dataFile['cells'][$i][18]))?$dataFile['cells'][$i][18]:'';
						$update_staf['staf_kodepos'] 					= (isset($dataFile['cells'][$i][19]))?$dataFile['cells'][$i][19]:'';
						$update_staf['staf_telepon'] 					= (isset($dataFile['cells'][$i][20]))?$dataFile['cells'][$i][20]:'';
						$update_staf['staf_hp'] 						= (isset($dataFile['cells'][$i][21]))?$dataFile['cells'][$i][21]:'';
						$update_staf['staf_email'] 						= (isset($dataFile['cells'][$i][22]))?$dataFile['cells'][$i][22]:'';
						$update_staf['staf_tugas_tambahan'] 			= (isset($dataFile['cells'][$i][23]))?$dataFile['cells'][$i][23]:'';
						$update_staf['staf_sk_cpns'] 					= (isset($dataFile['cells'][$i][24]))?$dataFile['cells'][$i][24]:'';
						$update_staf['staf_tanggal_cpns'] 				= (isset($dataFile['cells'][$i][25]))?dateForDB($dataFile['cells'][$i][25]):null;
						$update_staf['staf_sk_pengangkatan'] 			= (isset($dataFile['cells'][$i][26]))?$dataFile['cells'][$i][26]:'';
						$update_staf['staf_tmt_pengangkatan'] 			= (isset($dataFile['cells'][$i][27]))?dateForDB($dataFile['cells'][$i][27]):null;
						$update_staf['staf_lembaga_pengangkatan'] 		= (isset($dataFile['cells'][$i][28]))?$dataFile['cells'][$i][28]:'';
						$update_staf['staf_pangkat_golongan'] 			= (isset($dataFile['cells'][$i][29]))?$dataFile['cells'][$i][29]:'';
						$update_staf['staf_sumber_gaji'] 				= (isset($dataFile['cells'][$i][30]))?$dataFile['cells'][$i][30]:'';
						$update_staf['staf_nama_ibu_kandung'] 			= (isset($dataFile['cells'][$i][31]))?$dataFile['cells'][$i][31]:'';
						$update_staf['staf_status_perkawinan'] 			= (isset($dataFile['cells'][$i][32]))?$dataFile['cells'][$i][32]:'';
						$update_staf['staf_nama_suami_istri'] 			= (isset($dataFile['cells'][$i][33]))?$dataFile['cells'][$i][33]:'';
						$update_staf['staf_nip_suami_istri'] 			= (isset($dataFile['cells'][$i][34]))?$dataFile['cells'][$i][34]:'';
						$update_staf['staf_pekerjaan_suami_istri'] 		= (isset($dataFile['cells'][$i][35]))?$dataFile['cells'][$i][35]:'';
						$update_staf['staf_tmt_pns'] 					= (isset($dataFile['cells'][$i][36]))?dateForDB($dataFile['cells'][$i][36]):null;
						$update_staf['staf_sudah_lisensi_kepala_sekolah'] = (isset($dataFile['cells'][$i][37]))?$dataFile['cells'][$i][37]:'';
						$update_staf['staf_pernah_diklat_kepengawasan'] = (isset($dataFile['cells'][$i][38]))?$dataFile['cells'][$i][38]:'';
						$update_staf['staf_keahlian_braille'] 			= (isset($dataFile['cells'][$i][39]))?$dataFile['cells'][$i][39]:'';
						$update_staf['staf_keahlian_bahasa_isyarat'] 	= (isset($dataFile['cells'][$i][40]))?$dataFile['cells'][$i][40]:'';
						$update_staf['staf_npwp'] 						= (isset($dataFile['cells'][$i][41]))?$dataFile['cells'][$i][41]:'';
						$update_staf['staf_nama_wajib_pajak'] 			= (isset($dataFile['cells'][$i][42]))?$dataFile['cells'][$i][42]:'';
						$update_staf['staf_kewarganegaraan'] 			= (isset($dataFile['cells'][$i][43]))?$dataFile['cells'][$i][43]:'';
						$update_staf['staf_bank'] 						= (isset($dataFile['cells'][$i][44]))?$dataFile['cells'][$i][44]:'';
						$update_staf['staf_nomor_rekening_bank'] 		= (isset($dataFile['cells'][$i][45]))?$dataFile['cells'][$i][45]:'';
						$update_staf['staf_rekening_atas_nama'] 		= (isset($dataFile['cells'][$i][46]))?$dataFile['cells'][$i][46]:'';
						$update_staf['staf_nik'] 						= (isset($dataFile['cells'][$i][47]))?$dataFile['cells'][$i][47]:'';
						$update_staf['staf_status'] 					= 'Tendik';
						$this->Staf_model->update_staf(array('staf_user'=>$staf_user), $update_staf);
					}
				}
			}
			
			$this->session->set_flashdata('success','Data guru telah berhasil di-import.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tendik', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function upload_foto()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'upload_foto';
		
		$data['dataExcel']				= array();		
		$data['filename']				= '';
		
		if ($this->input->post('uploadFoto')){
			$total = count($_FILES['fileFoto']['name']);
			$path		= "./asset/uploads/foto-staf/";
			$mkdir		= true;
			if(file_exists($path) && is_dir($path)){
			} else {
				if (mkdir($path, 0777, true)){
				} else {
					$mkdir = false;
				}
			}
			for($i=0; $i<$total; $i++) {
				$name		= $_FILES['fileFoto']['name'][$i];
				$size		= $_FILES['fileFoto']['size'][$i];
				$basename 	= pathinfo($name);

				$where_update['staf_user']	= $basename['filename'];
				if ($this->Staf_model->count_all_staf($where_update) > 0){
					if ($mkdir){
						$newname	= filename_seo($name);
						$fullname	= $path . '/' . filename_seo($name);
						$tmp = $_FILES['fileFoto']['tmp_name'][$i];
						if ($tmp){
							if (move_uploaded_file($tmp, $fullname)){
								$this->Staf_model->update_staf($where_update, array('staf_foto'=>$basename['basename']));
							}
						}
					}
				}
			}
			$this->session->set_flashdata('success','Foto telah berhasil di-upload.');
			redirect(current_url());
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tendik', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['staf_id']	= validasi_sql($this->uri->segment(4)); 
		$staf		 			= $this->Staf_model->get_staf('*', $where);
		
		$data['staf_id'] = $staf->staf_id;
		$data['staf_nama'] = $staf->staf_nama;
		$data['staf_nuptk'] = $staf->staf_nuptk;
		$data['staf_jenis_kelamin'] = $staf->staf_jenis_kelamin;
		$data['staf_tempat_lahir'] = $staf->staf_tempat_lahir;
		$data['staf_tanggal_lahir'] = $staf->staf_tanggal_lahir;
		$data['staf_nip'] = $staf->staf_nip;
		$data['staf_status_kepegawaian'] = $staf->staf_status_kepegawaian;
		$data['staf_jenis_ptk'] = $staf->staf_jenis_ptk;
		$data['staf_agama'] = $staf->staf_agama;
		$data['staf_alamat'] = $staf->staf_alamat;
		$data['staf_rt'] = $staf->staf_rt;
		$data['staf_rw'] = $staf->staf_rw;
		$data['staf_dusun'] = $staf->staf_dusun;
		$data['staf_deskel'] = $staf->staf_deskel;
		$data['staf_kecamatan'] = $staf->staf_kecamatan;
		$data['staf_kodepos'] = $staf->staf_kodepos;
		$data['staf_kabkota'] = $staf->staf_kabkota;
		$data['staf_provinsi'] = $staf->staf_provinsi;
		$data['staf_telepon'] = $staf->staf_telepon;
		$data['staf_hp'] = $staf->staf_hp;
		$data['staf_email'] = $staf->staf_email;
		$data['staf_tugas_tambahan'] = $staf->staf_tugas_tambahan;
		$data['staf_sk_cpns'] = $staf->staf_sk_cpns;
		$data['staf_tanggal_cpns'] = $staf->staf_tanggal_cpns;
		$data['staf_sk_pengangkatan'] = $staf->staf_sk_pengangkatan;
		$data['staf_tmt_pengangkatan'] = $staf->staf_tmt_pengangkatan;
		$data['staf_lembaga_pengangkatan'] = $staf->staf_lembaga_pengangkatan;
		$data['staf_pangkat_golongan'] = $staf->staf_pangkat_golongan;
		$data['staf_sumber_gaji'] = $staf->staf_sumber_gaji;
		$data['staf_nama_ibu_kandung'] = $staf->staf_nama_ibu_kandung;
		$data['staf_status_perkawinan'] = $staf->staf_status_perkawinan;
		$data['staf_nama_suami_istri'] = $staf->staf_nama_suami_istri;
		$data['staf_nip_suami_istri'] = $staf->staf_nip_suami_istri;
		$data['staf_pekerjaan_suami_istri'] = $staf->staf_pekerjaan_suami_istri;
		$data['staf_tmt_pns'] = $staf->staf_tmt_pns;
		$data['staf_sudah_lisensi_kepala_sekolah'] = $staf->staf_sudah_lisensi_kepala_sekolah;
		$data['staf_pernah_diklat_kepengawasan'] = $staf->staf_pernah_diklat_kepengawasan;
		$data['staf_keahlian_braille'] = $staf->staf_keahlian_braille;
		$data['staf_keahlian_bahasa_isyarat'] = $staf->staf_keahlian_bahasa_isyarat;
		$data['staf_npwp'] = $staf->staf_npwp;
		$data['staf_nama_wajib_pajak'] = $staf->staf_nama_wajib_pajak;
		$data['staf_kewarganegaraan'] = $staf->staf_kewarganegaraan;
		$data['staf_bank'] = $staf->staf_bank;
		$data['staf_nomor_rekening_bank'] = $staf->staf_nomor_rekening_bank;
		$data['staf_rekening_atas_nama'] = $staf->staf_rekening_atas_nama;
		$data['staf_nik'] = $staf->staf_nik;
		$data['staf_niy'] = $staf->staf_niy;
		$data['staf_user'] = $staf->staf_user;
		$data['staf_pin'] = $staf->staf_pin;
		$data['staf_pelajaran'] = $staf->staf_pelajaran;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tendik', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function generate_account()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		foreach($this->Staf_model->grid_all_staf('', 'staf_id', 'ASC', '', '', array('staf_status'=>'Tendik')) as $row){
			if ($row->staf_user){
				if ($this->Pengguna_model->count_all_pengguna(array('pengguna_nama'=>$row->staf_user)) < 1){
					if ($row->staf_pin && strlen($row->staf_pin) >= 6){
						$acak 	= $row->staf_pin;
						
						$update_pin['staf_user_aktif']			= 'Y';
						$this->Staf_model->update_staf(array('staf_id'=>$row->staf_id), $update_pin);
					} else {
						$alpha 	= "0123456789";
						$acak	= str_shuffle($alpha);
						$acak 	= substr($acak,0,6);
						
						$update_pin['staf_pin']					= $acak;
						$update_pin['staf_user_aktif']			= 'Y';
						$this->Staf_model->update_staf(array('staf_id'=>$row->staf_id), $update_pin);
					}
					
					$insert_pengguna 						= array();
					$insert_pengguna['staf_id']				= $row->staf_id;
					$insert_pengguna['pengguna_id']			= $this->uuid->v4();
					$insert_pengguna['pengguna_nama']		= $row->staf_user;
					$insert_pengguna['pengguna_nama_depan']	= $row->staf_nama;
					$insert_pengguna['pengguna_kunci']		= hash_password($acak);
					$insert_pengguna['pengguna_level_id']	= 10;
					$insert_pengguna['pengguna_terdaftar']	= date("Y-m-d H:i:s");
					$insert_pengguna['pengguna_status']		= 'A';
					$this->Pengguna_model->insert_pengguna($insert_pengguna);
				}
			}
		}
		
		$this->session->set_flashdata('success','User guru telah berhasil di-generate.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function genpinstaf(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$staf_id 			= validasi_sql($this->uri->segment(4));
		$staf_user 			= validasi_sql($this->uri->segment(5));
		if ($this->Pengguna_model->count_all_pengguna(array('pengguna_nama'=>$staf_user, 'p.pengguna_level_id'=>10)) > 0){
			$alpha 	= "0123456789";
			$acak	= str_shuffle($alpha);
			$acak 	= substr($acak,0,6);
			
			$update_pengguna['pengguna_kunci']		= hash_password($acak);
			$this->Pengguna_model->update_pengguna(array('pengguna_nama'=>$staf_user, 'pengguna_level_id'=>10), $update_pengguna);
			
			$update_pin['staf_pin']				= $acak;
			$update_pin['staf_user_aktif']		= 'Y';
			$this->Staf_model->update_staf(array('staf_id'=>$staf_id), $update_pin);

			$this->session->set_flashdata('success','Pin staf telah berhasil di-reset.<br /><strong>Pin Baru : '.$acak.'</strong>');
			redirect(module_url($this->uri->segment(2)));
		} else {
			$where['staf_id']		= $staf_id; 
			$row 					= $this->Staf_model->get_staf('*', $where);
			if ($row->staf_pin && strlen($row->staf_pin) >= 6){
				$acak 	= $row->staf_pin;
				
				$update_pin['staf_user_aktif']				= 'Y';
				$this->Staf_model->update_staf(array('staf_id'=>$row->staf_id), $update_pin);
			} else {
				$alpha 	= "0123456789";
				$acak	= str_shuffle($alpha);
				$acak 	= substr($acak,0,6);
				
				$update_pin['staf_pin']					= $acak;
				$update_pin['staf_user_aktif']				= 'Y';
				$this->Staf_model->update_staf(array('staf_id'=>$row->staf_id), $update_pin);
			}
			
			$insert_pengguna 						= array();
			$insert_pengguna['staf_id']				= $row->staf_id;
			$insert_pengguna['pengguna_id']			= $this->uuid->v4();
			$insert_pengguna['pengguna_nama']		= $row->staf_user;
			$insert_pengguna['pengguna_nama_depan']	= $row->staf_nama;
			$insert_pengguna['pengguna_kunci']		= hash_password($acak);
			$insert_pengguna['pengguna_level_id']	= 10;
			$insert_pengguna['pengguna_terdaftar']	= date("Y-m-d H:i:s");
			$insert_pengguna['pengguna_status']		= 'A';
			$this->Pengguna_model->insert_pengguna($insert_pengguna);

			$this->session->set_flashdata('success','Pin staf telah berhasil dibuat.<br /><strong>Pin Anda : '.$acak.'</strong>');
			redirect(module_url($this->uri->segment(2)));
		}
		
	}
	
	public function pin_staf()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['staf']	= $this->Staf_model->grid_all_staf('*', 'staf_user', 'ASC', '', '', "staf_status = 'Tendik' AND (staf_tst = '0000-00-00' OR staf_tst >= '".date('Y-m-d')."' OR staf_tst IS NULL)");
		$this->load->view(module_dir().'/export/list_user_staf', $data);
	}
}
