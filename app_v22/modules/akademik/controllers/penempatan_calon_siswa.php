<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penempatan_calon_siswa extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Penempatan Siswa Baru | ' . profile('profil_website');
		$this->active_menu		= 301;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Tahun_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Kelas_model');
		$this->load->model('Siswa_model');
		$this->load->model('Calon_siswa_model');
		$this->load->model('Siswa_kelas_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$tahun_ajaran = $this->Tahun_model->get_tahun_ajaran("tahun_kode, tahun_nama, tahun_angkatan", array("tahun_aktif"=>"A"));
		$data['tahun_kode']		= $tahun_ajaran->tahun_kode;
		$data['tahun_angkatan']	= $tahun_ajaran->tahun_angkatan;
		
		$tahun_ajaran_depan = $this->db->query("SELECT tahun_kode FROM tahun_ajaran WHERE tahun_angkatan = '".($tahun_ajaran->tahun_angkatan + 1)."'")->row();
		$data['tahun_depan_id']	= ($tahun_ajaran_depan)?$tahun_ajaran_depan->tahun_kode:$tahun_ajaran->tahun_kode;
		
		$data['tahun_kode']			= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$data['tahun_kode'];
		$data['tahun_tujuan_id']	= ($this->input->post('tahun_tujuan_id'))?$this->input->post('tahun_tujuan_id'):'';
		$data['tingkat_id']			= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):'';
		$data['kelas_id']			= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):'';
		
		$save	= $this->input->post('save');
		if ($save){
			
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/penempatan_calon_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function get_calon_siswa(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun_ajaran("tahun_angkatan", array('tahun_kode'=>$tahun_kode));
			if ($tahun){
				$siswa = $this->db->query("SELECT siswa_id, siswa_reg, siswa_nama FROM calon_siswa WHERE siswa_status = 'Calon Siswa' AND siswa_tahun='".$tahun->tahun_angkatan."' ORDER BY siswa_nama ASC")->result();
				if ($siswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $siswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_calon_siswa_filter(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$filter = $this->input->post('filter');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun_ajaran("tahun_angkatan", array('tahun_kode'=>$tahun_kode));
			if ($tahun){
				$siswa = $this->db->query("SELECT siswa_id, siswa_reg, siswa_nama FROM calon_siswa WHERE siswa_status = 'Calon Siswa' AND siswa_tahun='".$tahun->tahun_angkatan."' AND (siswa_nama LIKE '%$filter%' OR siswa_reg LIKE '%$filter%') ORDER BY siswa_nama ASC")->result();
				if ($siswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $siswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_siswa_baru(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$tingkat_id = $this->input->post('tingkat');
		$kelas_id = $this->input->post('kelas');
		if ($tahun_kode && $tingkat_id && $kelas_id){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			$tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_id));
			$kelas = $this->Kelas_model->get_kelas("*", array('kelas_id'=>$kelas_id));
			if ($tahun && $tingkat && $kelas){
				$siswa = $this->db->query("SELECT siswa.siswa_id, siswa_nis, siswa_reg, siswa_nama FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_status = 'Siswa' AND siswa_kelas.tahun_kode='".$tahun_kode."' AND siswa_kelas.kelas_id='".$kelas_id."' ORDER BY siswa_nama ASC")->result();
				if ($siswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $siswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_kelas(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$tingkat_id = $this->input->post('tingkat');
		if ($tahun_kode && $tingkat_id){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			$tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_id));
			if ($tahun && $tingkat){
				$kelas = $this->Kelas_model->grid_all_kelas("kelas.kelas_id, kelas.kelas_nama", "kelas_nama", "ASC", 0, 0,array('kelas.tahun_kode'=>$tahun_kode, 'kelas.tingkat_id'=>$tingkat_id));
				if ($kelas){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $kelas;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function set_siswa(){
		$data = array();
		$siswa_id 	= $this->input->post('siswa');
		$tahun_kode 	= $this->input->post('tahun');
		$tingkat_id	= $this->input->post('tingkat');
		$kelas_id 	= $this->input->post('kelas');
		
		if ($tahun_kode && $tingkat_id && $kelas_id){
			$get_tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_id));
			$get_tahun	= $this->Tahun_model->get_tahun_ajaran("*", array("tahun_kode"=>$tahun_kode));
			$get_kelas	= $this->Kelas_model->get_kelas("*", array("kelas_id"=>$kelas_id, "kelas.tahun_kode"=>$tahun_kode));
			$get_calon_siswa = $this->Calon_siswa_model->get_calon_siswa("", array("calon_siswa.siswa_id"=>$siswa_id));
			if ($get_tahun && $get_tingkat && $get_kelas && $get_calon_siswa){
				$count_siswa = $this->Siswa_model->count_all_siswa(array("siswa_reg"=>$get_calon_siswa->siswa_reg));
				if ($count_siswa < 1){
					$insert['siswa_reg'] = $get_calon_siswa->siswa_reg;
					$insert['siswa_nis'] = $get_calon_siswa->siswa_reg;
					$insert['siswa_nisn'] = $get_calon_siswa->siswa_nisn;
					$insert['siswa_nik'] = $get_calon_siswa->siswa_nik;
					$insert['siswa_nama'] = $get_calon_siswa->siswa_nama;
					$insert['siswa_tanggal_lahir'] = $get_calon_siswa->siswa_tanggal_lahir;
					$insert['siswa_tempat_lahir'] = $get_calon_siswa->siswa_tempat_lahir;
					$insert['siswa_jenis_kelamin'] = $get_calon_siswa->siswa_jenis_kelamin;
					$insert['siswa_golongan_darah'] = $get_calon_siswa->siswa_golongan_darah;
					$insert['siswa_agama'] = $get_calon_siswa->siswa_agama;
					$insert['siswa_warganegara'] = $get_calon_siswa->siswa_warganegara;
					$insert['siswa_foto'] = $get_calon_siswa->siswa_foto;
					$insert['siswa_hp'] = $get_calon_siswa->siswa_hp;
					$insert['siswa_telepon'] = $get_calon_siswa->siswa_telepon;
					$insert['siswa_email'] = $get_calon_siswa->siswa_email;
					$insert['siswa_alamat'] = $get_calon_siswa->siswa_alamat;
					$insert['siswa_rt'] = $get_calon_siswa->siswa_rt;
					$insert['siswa_rw'] = $get_calon_siswa->siswa_rw;
					$insert['siswa_deskel'] = $get_calon_siswa->siswa_deskel;
					$insert['siswa_kecamatan'] = $get_calon_siswa->siswa_kecamatan;
					$insert['siswa_kabkota'] = $get_calon_siswa->siswa_kabkota;
					$insert['siswa_provinsi'] = $get_calon_siswa->siswa_provinsi;
					$insert['siswa_kodepos'] = $get_calon_siswa->siswa_kodepos;
					$insert['siswa_nama_ayah'] = $get_calon_siswa->siswa_nama_ayah;
					$insert['siswa_pekerjaan_ayah'] = $get_calon_siswa->siswa_pekerjaan_ayah;
					$insert['siswa_nama_ibu'] = $get_calon_siswa->siswa_nama_ibu;
					$insert['siswa_pekerjaan_ibu'] = $get_calon_siswa->siswa_pekerjaan_ibu;
					$insert['siswa_hp_orangtua'] = $get_calon_siswa->siswa_hp_orangtua;
					$insert['siswa_telepon_orangtua'] = $get_calon_siswa->siswa_telepon_orangtua;
					$insert['siswa_tahun'] = $get_tahun->tahun_angkatan;
					$insert['siswa_no_skhun'] = $get_calon_siswa->siswa_no_skhun;
					$insert['siswa_no_raport'] = $get_calon_siswa->siswa_no_raport;
					$insert['siswa_status'] = "Siswa";
					$insert['siswa_baru'] = "Y";
					$insert['siswa_kelas_sekarang'] = ($get_kelas)?$get_kelas->kelas_nama:'';
					$insert['siswa_kelas_sekarang_id'] = ($get_kelas)?$get_kelas->kelas_id:'';
					$query_siswa = $this->Siswa_model->insert_siswa($insert);
				}
				
				$get_siswa	= $this->Siswa_model->get_siswa("*", array("siswa_reg"=>$get_calon_siswa->siswa_reg));
				if ($get_siswa){
					$where_siswa_kelas['siswa_kelas.siswa_id'] = $get_siswa->siswa_id;
					$where_siswa_kelas['siswa_kelas.tahun_kode'] = $get_tahun->tahun_kode;
					if ($this->Siswa_kelas_model->count_all_siswa_kelas($where_siswa_kelas) < 1){
						$insert_siswa_kelas['siswa_id']			= $get_siswa->siswa_id;
						$insert_siswa_kelas['kelas_id']			= $get_kelas->kelas_id;
						$insert_siswa_kelas['tahun_kode']			= $get_tahun->tahun_kode;
						$this->Siswa_kelas_model->insert_siswa_kelas($insert_siswa_kelas);
					} else {
						$update_siswa_kelas['kelas_id']			= $get_kelas->kelas_id;
						$this->Siswa_kelas_model->update_siswa_kelas($where_siswa_kelas, $update_siswa_kelas);
					}
					
					$update_calon['siswa_baru_id'] = $get_siswa->siswa_id;
					$update_calon['siswa_status'] = "Siswa";
					$this->Calon_siswa_model->update_calon_siswa(array("siswa_reg"=>$get_calon_siswa->siswa_reg), $update_calon);
					
					$data['response']	= true;
					$data['message']	= "Data berhasil disimpan.";
					$data['params']		= array("siswa_id"=>$siswa_id,
												"tahun_kode"=>$tahun_kode,
												"kelas_id"=>$kelas_id,
												"tingkat_id"=>$tingkat_id);
				} else {
					$data['response']	= false;
					$data['message']	= "Data siswa tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tahun, tingkat, kelas atau calon siswa tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function remove_siswa(){
		$data = array();
		$siswa_id 	= $this->input->post('siswa');
		$tahun_kode 	= $this->input->post('tahun');
		$tingkat_id	= $this->input->post('tingkat');
		$kelas_id 	= $this->input->post('kelas');
		
		if ($tahun_kode && $tingkat_id && $kelas_id){
			$tahun 				= $this->Tahun_model->count_all_tahun_ajaran(array('tahun_kode'=>$tahun_kode));
			$tingkat 			= $this->Tingkat_model->count_all_tingkat(array('tingkat_id'=>$tingkat_id));
			$kelas 				= $this->Kelas_model->count_all_kelas(array('kelas_id'=>$kelas_id));
			$siswa_kelas 		= $this->Siswa_kelas_model->count_all_siswa_kelas(array('siswa_kelas.siswa_id'=>$siswa_id));
			$siswa_kelas_param 	= $this->Siswa_kelas_model->count_all_siswa_kelas(array('siswa_kelas.siswa_id'=>$siswa_id, 'siswa_kelas.tahun_kode'=>$tahun_kode, 'siswa_kelas.kelas_id'=>$kelas_id));
			$siswa 				= $this->Siswa_model->count_all_siswa(array('siswa_id'=>$siswa_id));
			if ($tahun && $tingkat && $kelas && $siswa_kelas && $siswa_kelas_param && $siswa){
				if ($siswa_kelas == 1){
					$query_delete_siswa_kelas = $this->Siswa_kelas_model->delete_siswa_kelas(array('siswa_id'=>$siswa_id, 'tahun_kode'=>$tahun_kode, 'kelas_id'=>$kelas_id));
					// $query_delete_siswa = $this->Siswa_model->delete_siswa(array('siswa_id'=>$siswa_id));
					
					$update_calon['siswa_baru_id'] = "";
					$update_calon['siswa_status'] = "Calon Siswa";
					$this->Calon_siswa_model->update_calon_siswa(array("siswa_baru_id"=>$siswa_id), $update_calon);
							
					$data['response']	= true;
					$data['message']	= "Data berhasil disimpan.";
					$data['params']		= array("siswa_id"=>$siswa_id,
												"tahun_kode"=>$tahun_kode,
												"kelas_id"=>$kelas_id,
												"tingkat_id"=>$tingkat_id);
				} else {
					$data['response']	= false;
					$data['message']	= "Bukan siswa baru.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}
