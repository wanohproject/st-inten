<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penempatan_siswa_baru extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Penempatan Siswa Baru | ' . profile('profil_website');
		$this->active_menu		= 301;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Tahun_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Kelas_model');
		$this->load->model('Siswa_model');
		$this->load->model('Siswa_kelas_model');
		$this->load->model('Departemen_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$tahun_ajaran = $this->Tahun_model->get_tahun_ajaran("tahun_kode, tahun_nama, tahun_angkatan", array("tahun_aktif"=>"A"));
		$data['tahun_kode']		= $tahun_ajaran->tahun_kode;
		$data['tahun_angkatan']	= $tahun_ajaran->tahun_angkatan;
		
		$tahun_ajaran_depan = $this->db->query("SELECT tahun_kode FROM tahun_ajaran WHERE tahun_angkatan = '".($tahun_ajaran->tahun_angkatan + 1)."'")->row();
		$data['tahun_depan_id']	= ($tahun_ajaran_depan)?$tahun_ajaran_depan->tahun_kode:$tahun_ajaran->tahun_kode;
		
		$data['tahun_kode']			= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$data['tahun_kode'];
		$data['tahun_tujuan_id']	= ($this->input->post('tahun_tujuan_id'))?$this->input->post('tahun_tujuan_id'):'';
		$data['tingkat_id']			= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):'';
		$data['kelas_id']			= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):'';
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= $this->uri->segment(4);
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$save	= $this->input->post('save');
		if ($save){
			
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/penempatan_siswa_baru', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function get_calon_siswa(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$departemen_id = $this->input->post('departemen');
		if ($tahun_kode && $departemen_id){
			$tahun = $this->Tahun_model->get_tahun_ajaran("tahun_angkatan", array('tahun_kode'=>$tahun_kode));
			$departemen = $this->Departemen_model->get_departemen("departemen_id", array('departemen_id'=>$departemen_id));
			if ($tahun && $departemen){
				$siswa = $this->db->query("SELECT siswa_id, siswa_nis, siswa_nama FROM siswa WHERE siswa_status = 'Siswa' AND siswa_tahun='".$tahun->tahun_angkatan."' AND siswa.departemen_id = '$departemen->departemen_id' AND (SELECT COUNT(siswa_id) FROM siswa_kelas WHERE siswa_kelas.siswa_id=siswa.siswa_id) < 1 ORDER BY siswa_nama ASC")->result();
				if ($siswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $siswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_calon_siswa_filter(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$filter = $this->input->post('filter');
		$departemen_id = $this->input->post('departemen');
		if ($tahun_kode && $departemen_id){
			$tahun = $this->Tahun_model->get_tahun_ajaran("tahun_angkatan", array('tahun_kode'=>$tahun_kode));
			$departemen = $this->Departemen_model->get_departemen("departemen_id", array('departemen_id'=>$departemen_id));
			if ($tahun && $departemen){
				$siswa = $this->db->query("SELECT siswa_id, siswa_nis, siswa_nama FROM siswa WHERE siswa_status = 'Siswa' AND siswa_tahun='".$tahun->tahun_angkatan."' AND siswa.departemen_id = '$departemen->departemen_id' AND (SELECT COUNT(siswa_id) FROM siswa_kelas WHERE siswa_kelas.siswa_id=siswa.siswa_id) < 1 AND (siswa_nama LIKE '%$filter%' OR siswa_nis LIKE '%$filter%') ORDER BY siswa_nama ASC")->result();
				if ($siswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $siswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_siswa_baru(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$tingkat_id = $this->input->post('tingkat');
		$kelas_id = $this->input->post('kelas');
		$departemen_id = $this->input->post('departemen');
		if ($tahun_kode && $tingkat_id && $kelas_id && $departemen_id){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			$tingkat = $this->Tingkat_model->get_tingkat("*", array('departemen.departemen_id'=>$departemen_id, 'tingkat_id'=>$tingkat_id));
			$kelas = $this->Kelas_model->get_kelas("*", array('departemen.departemen_id'=>$departemen_id, 'kelas_id'=>$kelas_id));
			$departemen = $this->Departemen_model->get_departemen("departemen_id", array('departemen_id'=>$departemen_id));
			if ($tahun && $tingkat && $kelas && $departemen){
				$siswa = $this->db->query("SELECT siswa.siswa_id, siswa_nis, siswa_nis, siswa_nama FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_status = 'Siswa' AND siswa_kelas.tahun_kode='".$tahun_kode."' AND siswa_kelas.kelas_id='".$kelas_id."' AND siswa.departemen_id = '$departemen->departemen_id' ORDER BY siswa_nama ASC")->result();
				if ($siswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $siswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_kelas(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$tingkat_id = $this->input->post('tingkat');
		$departemen_id = $this->input->post('departemen');
		if ($tahun_kode && $tingkat_id && $departemen_id){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			$tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_id));
			$departemen = $this->Departemen_model->get_departemen("departemen_id", array('departemen_id'=>$departemen_id));
			if ($tahun && $tingkat && $departemen){
				$kelas = $this->Kelas_model->grid_all_kelas("kelas.kelas_id, kelas.kelas_nama", "kelas_nama", "ASC", 0, 0,array('kelas.tahun_kode'=>$tahun_kode, 'kelas.tingkat_id'=>$tingkat_id, 'kelas.departemen_id'=>$departemen->departemen_id));
				if ($kelas){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $kelas;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function set_siswa(){
		$data = array();
		$siswa_id 	= $this->input->post('siswa');
		$tahun_kode 	= $this->input->post('tahun');
		$tingkat_id	= $this->input->post('tingkat');
		$kelas_id 	= $this->input->post('kelas');
		$departemen_id = $this->input->post('departemen');
		
		if ($tahun_kode && $tingkat_id && $kelas_id && $departemen_id){
			$get_tahun = $this->Tahun_model->get_tahun_ajaran("*", array("tahun_kode"=>$tahun_kode));
			$get_tingkat = $this->Tingkat_model->get_tingkat("*", array('departemen.departemen_id'=>$departemen_id, 'tingkat_id'=>$tingkat_id));
			$get_kelas = $this->Kelas_model->get_kelas("*", array('departemen.departemen_id'=>$departemen_id, "kelas_id"=>$kelas_id, "kelas.tahun_kode"=>$tahun_kode));
			$get_siswa = $this->Siswa_model->get_siswa("", array('departemen.departemen_id'=>$departemen_id, "siswa.siswa_id"=>$siswa_id));
			if ($get_tahun && $get_tingkat && $get_kelas && $get_siswa){
				$where_siswa_kelas['siswa_kelas.siswa_id'] = $get_siswa->siswa_id;
				$where_siswa_kelas['siswa_kelas.tahun_kode'] = $get_tahun->tahun_kode;
				if ($this->Siswa_kelas_model->count_all_siswa_kelas($where_siswa_kelas) < 1){
					$insert_siswa_kelas['siswa_id']			= $get_siswa->siswa_id;
					$insert_siswa_kelas['kelas_id']			= $get_kelas->kelas_id;
					$insert_siswa_kelas['tahun_kode']			= $get_tahun->tahun_kode;
					$this->Siswa_kelas_model->insert_siswa_kelas($insert_siswa_kelas);
				} else {
					$update_siswa_kelas['kelas_id']			= $get_kelas->kelas_id;
					$this->Siswa_kelas_model->update_siswa_kelas($where_siswa_kelas, $update_siswa_kelas);
				}
				
				$update_siswa['siswa_kelas_sekarang']	 = $get_kelas->kelas_nama;
				$update_siswa['siswa_kelas_sekarang_id'] = $get_kelas->kelas_id;
				$this->Siswa_model->update_siswa(array("siswa.siswa_id"=>$get_siswa->siswa_id), $update_siswa);
				
				$data['response']	= true;
				$data['message']	= "Data berhasil disimpan.";
				$data['params']		= array("siswa_id"=>$siswa_id,
											"tahun_kode"=>$tahun_kode,
											"kelas_id"=>$kelas_id,
											"tingkat_id"=>$tingkat_id);
			} else {
				$data['response']	= false;
				$data['message']	= "Data tahun, tingkat, kelas atau siswa tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function remove_siswa(){
		$data = array();
		$siswa_id 	= $this->input->post('siswa');
		$tahun_kode 	= $this->input->post('tahun');
		$tingkat_id	= $this->input->post('tingkat');
		$kelas_id 	= $this->input->post('kelas');
		$departemen_id = $this->input->post('departemen');
		
		if ($tahun_kode && $tingkat_id && $kelas_id && $departemen_id){
			$tahun 				= $this->Tahun_model->count_all_tahun_ajaran(array('tahun_kode'=>$tahun_kode));
			$tingkat 			= $this->Tingkat_model->count_all_tingkat(array('departemen.departemen_id'=>$departemen_id, 'tingkat_id'=>$tingkat_id));
			$kelas 				= $this->Kelas_model->count_all_kelas(array('departemen.departemen_id'=>$departemen_id, 'kelas_id'=>$kelas_id));
			$siswa_kelas 		= $this->Siswa_kelas_model->count_all_siswa_kelas(array('siswa_kelas.siswa_id'=>$siswa_id));
			$siswa_kelas_param 	= $this->Siswa_kelas_model->count_all_siswa_kelas(array('siswa_kelas.siswa_id'=>$siswa_id, 'siswa_kelas.tahun_kode'=>$tahun_kode, 'siswa_kelas.kelas_id'=>$kelas_id));
			$siswa 				= $this->Siswa_model->count_all_siswa(array('departemen.departemen_id'=>$departemen_id, 'siswa_id'=>$siswa_id));
			if ($tahun && $tingkat && $kelas && $siswa_kelas && $siswa_kelas_param && $siswa){
				if ($siswa_kelas == 1){
					$query_delete_siswa_kelas = $this->Siswa_kelas_model->delete_siswa_kelas(array('siswa_id'=>$siswa_id, 'tahun_kode'=>$tahun_kode, 'kelas_id'=>$kelas_id));
					
					$update_siswa['siswa_kelas_sekarang']	 = "";
					$update_siswa['siswa_kelas_sekarang_id'] = "";
					$this->Siswa_model->update_siswa(array("siswa.siswa_id"=>$siswa_id), $update_siswa);
							
					$data['response']	= true;
					$data['message']	= "Data berhasil disimpan.";
					$data['params']		= array("siswa_id"=>$siswa_id,
												"tahun_kode"=>$tahun_kode,
												"kelas_id"=>$kelas_id,
												"tingkat_id"=>$tingkat_id);
				} else {
					$data['response']	= false;
					$data['message']	= "Bukan siswa baru.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}
