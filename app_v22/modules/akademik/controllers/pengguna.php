<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Pengguna | ' . profile('profil_website');
		$this->active_menu		= 19;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Pengguna_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		if ($this->session->userdata('level') == 1){
			$this->datatables->select('pengguna_id, pengguna_nama as username, pengguna_nama_depan, pengguna_nama_belakang, pengguna_level_nama')
			->add_column('nama_lengkap', '$1 $2','pengguna_nama_depan, pengguna_nama_belakang')
			->add_column('Actions', $this->get_buttons('$1'),'pengguna_id')
			->search_column('pengguna_nama, pengguna_nama_depan')
			->from('pengguna')
			->join('pengguna_level', 'pengguna.pengguna_level_id=pengguna_level.pengguna_level_id', 'left');
		} else {
			$this->datatables->select('pengguna_id, pengguna_nama as username, pengguna_nama_depan, pengguna_nama_belakang, pengguna_level_nama')
			->add_column('nama_lengkap', '$1 $2','pengguna_nama_depan, pengguna_nama_belakang')
			->add_column('Actions', $this->get_buttons('$1'),'pengguna_id')
			->search_column('pengguna_nama, pengguna_nama_depan')
			->from('pengguna')
			->join('pengguna_level', 'pengguna.pengguna_level_id=pengguna_level.pengguna_level_id', 'left')
			->where("pengguna.pengguna_level_id NOT IN (1)");
		}
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/pengguna', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['pengguna_id']			= ($this->input->post('pengguna_id'))?$this->input->post('pengguna_id'):$this->uuid->v4();
		$data['pengguna_nama']			= ($this->input->post('pengguna_nama'))?$this->input->post('pengguna_nama'):'';
		$data['pengguna_kunci']			= ($this->input->post('pengguna_kunci'))?$this->input->post('pengguna_kunci'):'';
		$data['pengguna_nama_depan']	= ($this->input->post('pengguna_nama_depan'))?$this->input->post('pengguna_nama_depan'):'';
		$data['pengguna_nama_belakang']	= ($this->input->post('pengguna_nama_belakang'))?$this->input->post('pengguna_nama_belakang'):'';
		$data['pengguna_alamat']		= ($this->input->post('pengguna_alamat'))?$this->input->post('pengguna_alamat'):'';
		$data['pengguna_telepon']		= ($this->input->post('pengguna_telepon'))?$this->input->post('pengguna_telepon'):'';
		$data['pengguna_surel']			= ($this->input->post('pengguna_surel'))?$this->input->post('pengguna_surel'):'';				
		$data['pengguna_level_id']		= ($this->input->post('pengguna_level_id'))?$this->input->post('pengguna_level_id'):'';
		$data['staf_id']				= ($this->input->post('staf_id'))?$this->input->post('staf_id'):'';
		$save							= $this->input->post('save');
		if ($save){
			$insert['pengguna_id']				= validasi_sql($this->input->post('pengguna_id'));
			$insert['pengguna_nama']			= validasi_sql($this->input->post('pengguna_nama'));
			$insert['pengguna_kunci'] 			= do_hash($this->input->post('pengguna_kunci'), md5);
			$insert['pengguna_nama_depan']		= validasi_sql($this->input->post('pengguna_nama_depan'));
			$insert['pengguna_nama_belakang']	= validasi_sql($this->input->post('pengguna_nama_belakang'));
			$insert['pengguna_alamat']			= validasi_sql($this->input->post('pengguna_alamat'));
			$insert['pengguna_telepon']			= validasi_sql($this->input->post('pengguna_telepon'));
			$insert['pengguna_surel']			= validasi_sql($this->input->post('pengguna_surel'));
			$insert['pengguna_level_id']		= validasi_sql($this->input->post('pengguna_level_id'));					
			$insert['staf_id']					= validasi_sql($this->input->post('staf_id'));					
			$insert['pengguna_terdaftar']		= date("Y-m-d H:i:s");
			$insert['pengguna_status']			= 'A';
			$this->Pengguna_model->insert_pengguna($insert);
			
			$this->session->set_flashdata('success','Pengguna telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/pengguna', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['pengguna_id']			= validasi_sql($this->uri->segment(4)); 
		$pengguna 						= $this->Pengguna_model->get_pengguna('*', $where);
		$data['pengguna_id']			= ($this->input->post('pengguna_id'))?$this->input->post('pengguna_id'):$pengguna->pengguna_id;
		$data['pengguna_nama']			= ($this->input->post('pengguna_nama'))?$this->input->post('pengguna_nama'):$pengguna->pengguna_nama;
		$data['pengguna_kunci']			= ($this->input->post('pengguna_kunci'))?$this->input->post('pengguna_kunci'):'';
		$data['pengguna_nama_depan']	= ($this->input->post('pengguna_nama_depan'))?$this->input->post('pengguna_nama_depan'):$pengguna->pengguna_nama_depan;
		$data['pengguna_nama_belakang']	= ($this->input->post('pengguna_nama_belakang'))?$this->input->post('pengguna_nama_belakang'):$pengguna->pengguna_nama_belakang;
		$data['pengguna_alamat']		= ($this->input->post('pengguna_alamat'))?$this->input->post('pengguna_alamat'):$pengguna->pengguna_alamat;
		$data['pengguna_surel']			= ($this->input->post('pengguna_surel'))?$this->input->post('pengguna_surel'):$pengguna->pengguna_surel;
		$data['pengguna_telepon']		= ($this->input->post('pengguna_telepon'))?$this->input->post('pengguna_telepon'):$pengguna->pengguna_telepon;
		$data['pengguna_level_id']		= ($this->input->post('pengguna_level_id'))?$this->input->post('pengguna_level_id'):$pengguna->pengguna_level_id;
		$data['staf_id']				= ($this->input->post('staf_id'))?$this->input->post('staf_id'):$pengguna->staf_id;
		$save							= $this->input->post('save');
		if ($save){
			$where_edit['pengguna_id']		= validasi_sql($this->input->post('pengguna_id'));
			$edit['pengguna_nama']			= validasi_sql($this->input->post('pengguna_nama'));
			if ($this->input->post('pengguna_kunci')){
				$edit['pengguna_kunci']		= do_hash($this->input->post('pengguna_kunci'), md5);
			}
			$edit['pengguna_nama_depan']	= validasi_sql($this->input->post('pengguna_nama_depan'));
			$edit['pengguna_nama_belakang']	= validasi_sql($this->input->post('pengguna_nama_belakang'));
			$edit['pengguna_alamat']		= validasi_sql($this->input->post('pengguna_alamat'));
			$edit['pengguna_telepon']		= validasi_sql($this->input->post('pengguna_telepon'));
			$edit['pengguna_surel']			= validasi_sql($this->input->post('pengguna_surel'));
			$edit['pengguna_level_id']		= validasi_sql($this->input->post('pengguna_level_id'));					
			$edit['staf_id']				= validasi_sql($this->input->post('staf_id'));					
			$this->Pengguna_model->update_pengguna($where_edit, $edit);
			
			$this->session->set_flashdata('success','Pengguna telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/pengguna', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['pengguna_id']	= validasi_sql($this->uri->segment(4));
		$this->Pengguna_model->delete_pengguna($where_delete);
		
		$this->session->set_flashdata('success','Pengguna telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['pengguna_id']	= validasi_sql($this->uri->segment(4)); 
		$pengguna		 		= $this->Pengguna_model->get_pengguna('*', $where);
		
		$data['pengguna_id']			= $pengguna->pengguna_id;
		$data['pengguna_nama']			= $pengguna->pengguna_nama;
		$data['pengguna_nama_depan']	= $pengguna->pengguna_nama_depan;
		$data['pengguna_nama_belakang']	= $pengguna->pengguna_nama_belakang;
		$data['pengguna_alamat']		= $pengguna->pengguna_alamat;
		$data['pengguna_surel']			= $pengguna->pengguna_surel;
		$data['pengguna_telepon']		= $pengguna->pengguna_telepon;
		$data['pengguna_level_nama']	= $pengguna->pengguna_level_nama;
		$data['staf_nama']				= $pengguna->staf_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/pengguna', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
