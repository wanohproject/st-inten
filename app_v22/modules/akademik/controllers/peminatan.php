<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Peminatan extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Peminatan | ' . profile('profil_website');
		$this->active_menu		= 333;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Peminatan_model');
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('peminatan_id, peminatan_singkat, peminatan_nama, peminatan_urutan')
		->add_column('Actions', $this->get_buttons('$1'),'peminatan_id')
		->search_column('peminatan_singkat, peminatan_nama')
		->from('peminatan');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/peminatan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['peminatan_singkat']	= ($this->input->post('peminatan_singkat'))?$this->input->post('peminatan_singkat'):'';
		$data['peminatan_nama']		= ($this->input->post('peminatan_nama'))?$this->input->post('peminatan_nama'):'';
		$data['peminatan_urutan']	= ($this->input->post('peminatan_urutan'))?$this->input->post('peminatan_urutan'):'';
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$insert['peminatan_singkat']		= validasi_sql($this->input->post('peminatan_singkat'));
			$insert['peminatan_nama']			= validasi_sql($this->input->post('peminatan_nama'));
			$insert['peminatan_urutan']			= validasi_sql($this->input->post('peminatan_urutan'));
			$this->Peminatan_model->insert_peminatan($insert);
			
			$this->session->set_flashdata('success','Peminatan telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/peminatan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['peminatan_id']		= validasi_sql($this->uri->segment(4)); 
		$peminatan 					= $this->Peminatan_model->get_peminatan('*', $where);

		$data['peminatan_id']		= ($this->input->post('peminatan_id'))?$this->input->post('peminatan_id'):$peminatan->peminatan_id;
		$data['peminatan_nama']		= ($this->input->post('peminatan_nama'))?$this->input->post('peminatan_nama'):$peminatan->peminatan_nama;
		$data['peminatan_singkat']	= ($this->input->post('peminatan_singkat'))?$this->input->post('peminatan_singkat'):$peminatan->peminatan_singkat;
		$data['peminatan_urutan']	= ($this->input->post('peminatan_urutan'))?$this->input->post('peminatan_urutan'):$peminatan->peminatan_urutan;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['peminatan_id']		= validasi_sql($this->input->post('peminatan_id'));
			$edit['peminatan_nama']			= validasi_sql($this->input->post('peminatan_nama'));
			$edit['peminatan_singkat']		= validasi_sql($this->input->post('peminatan_singkat'));
			$edit['peminatan_urutan']		= validasi_sql($this->input->post('peminatan_urutan'));
			$this->Peminatan_model->update_peminatan($where_edit, $edit);
			
			$this->session->set_flashdata('success','Peminatan telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/peminatan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['peminatan_id']	= validasi_sql($this->uri->segment(4));
		$this->Peminatan_model->delete_peminatan($where_delete);
		
		$this->session->set_flashdata('success','Peminatan telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['peminatan_id']	= validasi_sql($this->uri->segment(4)); 
		$peminatan		= $this->Peminatan_model->get_peminatan('*', $where);
		
		$data['peminatan_id']		= $peminatan->peminatan_id;
		$data['peminatan_nama']		= $peminatan->peminatan_nama;
		$data['peminatan_singkat']	= $peminatan->peminatan_singkat;
		$data['peminatan_urutan']	= $peminatan->peminatan_urutan;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/peminatan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
