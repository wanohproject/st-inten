<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bobot_nilai extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $semester_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Bobot Nilai | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Referensi_model');
		$this->load->model('Bobot_nilai_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		
		$this->semester_kode = $this->Semester_model->get_semester_aktif()->semester_kode;
    }
	
	function datatable()
	{
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$where = "bobot_nilai.program_studi_id = '$program_studi_id'";

		$semester_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		if ($semester_kode){
			$where .= " AND bobot_nilai.semester_kode = '$semester_kode'";
		}

		$this->load->library('Datatables');
		$this->datatables->select('bobot_nilai_id, bobot_nilai_huruf, bobot_nilai_value')
		->add_column('Actions', $this->get_buttons('$1', '$2', '$3'), 'bobot_nilai_id, program_studi_id, semester_kode')
		->search_column('bobot_nilai_huruf, bobot_nilai_value')
		->from('akd_bobot_nilai bobot_nilai')
		->where($where);
        echo $this->datatables->generate();
    }
	
	function get_buttons($id, $program_studi_id, $semester_kode)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id.'/'.$program_studi_id.'/'.$semester_kode) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$semester_kode					= ($this->uri->segment(5))?$this->uri->segment(5):$this->semester_kode;
		$data['semester_kode']			= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/bobot_nilai', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):'';
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):'';
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):'';
		$data['semester_kode'] = ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):'';
		$data['bobot_nilai_id'] = ($this->input->post('bobot_nilai_id'))?$this->input->post('bobot_nilai_id'):'';
		$data['bobot_nilai_value'] = ($this->input->post('bobot_nilai_value'))?$this->input->post('bobot_nilai_value'):'';
		$data['bobot_nilai_huruf'] = ($this->input->post('bobot_nilai_huruf'))?$this->input->post('bobot_nilai_huruf'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");
			if ($get_program_studi && $this->Bobot_nilai_model->count_all_bobot_nilai(array('bobot_nilai.program_studi_id'=>$this->input->post('program_studi_id'), 'bobot_nilai_huruf'=>$this->input->post('bobot_nilai_huruf'), 'bobot_nilai.semester_kode'=>$this->input->post('semester_kode'))) < 1){
				$insert = array();
				$insert['bobot_nilai_id'] = $this->uuid->v4();
				$insert['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
				$insert['program_studi_id'] = $get_program_studi->program_studi_id;
				$insert['jenjang_kode'] = $get_program_studi->jenjang_kode;
				$insert['semester_kode'] = validasi_input($this->input->post('semester_kode'));
				$insert['bobot_nilai_value'] = validasi_input($this->input->post('bobot_nilai_value'));
				$insert['bobot_nilai_huruf'] = validasi_input($this->input->post('bobot_nilai_huruf'));
				$insert['created_by'] = userdata('pengguna_id');
						
				$this->Bobot_nilai_model->insert_bobot_nilai($insert);

				$this->session->set_flashdata('success','Bobot Nilai telah berhasil ditambah.');
				redirect(module_url($this->uri->segment(2).'/index/'.$data['program_studi_id'].'/'.$data['semester_kode']));
			} else {
				$this->session->set_flashdata('error','Huruf Bobot Nilai telah terdaftar.');
				redirect(module_url($this->uri->segment(2).'/index/'.$data['program_studi_id'].'/'.$data['semester_kode']));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/bobot_nilai', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['bobot_nilai_id']		= validasi_sql($this->uri->segment(4)); 
		$bobot_nilai 				= $this->Bobot_nilai_model->get_bobot_nilai('*', $where);

		$data['bobot_nilai_id'] = ($this->input->post('bobot_nilai_id'))?$this->input->post('bobot_nilai_id'):$bobot_nilai->bobot_nilai_id;
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$bobot_nilai->perguruan_tinggi_id;
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$bobot_nilai->program_studi_id;
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):$bobot_nilai->jenjang_kode;
		$data['semester_kode'] = ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$bobot_nilai->semester_kode;
		$data['bobot_nilai_value'] = ($this->input->post('bobot_nilai_value'))?$this->input->post('bobot_nilai_value'):$bobot_nilai->bobot_nilai_value;
		$data['bobot_nilai_huruf'] = ($this->input->post('bobot_nilai_huruf'))?$this->input->post('bobot_nilai_huruf'):$bobot_nilai->bobot_nilai_huruf;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");
			if ($get_program_studi && $this->Bobot_nilai_model->count_all_bobot_nilai(array('bobot_nilai_value'=>$this->input->post('bobot_nilai_value'), 'bobot_nilai_id !='=>$this->input->post('bobot_nilai_id'), 'bobot_nilai.program_studi_id'=>$this->input->post('program_studi_id'), 'bobot_nilai.semester_kode'=>$this->input->post('semester_kode'))) < 1){
	
				$where_update['bobot_nilai_id']	= validasi_sql($this->input->post('bobot_nilai_id'));
				$update['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
				$update['program_studi_id'] = $get_program_studi->program_studi_id;
				$update['jenjang_kode'] = $get_program_studi->jenjang_kode;
				$update['semester_kode'] = validasi_input($this->input->post('semester_kode'));
				$update['bobot_nilai_value'] = validasi_input($this->input->post('bobot_nilai_value'));
				$update['bobot_nilai_huruf'] = validasi_input($this->input->post('bobot_nilai_huruf'));
				$update['updated_by'] = userdata('pengguna_id');
	
				$this->Bobot_nilai_model->update_bobot_nilai($where_update, $update);
				
				$this->session->set_flashdata('success','Bobot Nilai telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2).'/index/'.$data['program_studi_id'].'/'.$data['semester_kode']));
			} else {
				$this->session->set_flashdata('error','Kode Bobot Nilai telah terdaftar.');
				redirect(module_url($this->uri->segment(2).'/index/'.$data['program_studi_id'].'/'.$data['semester_kode']));
			}
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/bobot_nilai', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$bobot_nilai_id		= validasi_sql($this->uri->segment(4));
		$program_studi_id	= validasi_sql($this->uri->segment(5));
		$semester_kode		= validasi_sql($this->uri->segment(6));
		
		$where_delete_bobot_nilai['bobot_nilai_id']	= $bobot_nilai_id;
		$this->Bobot_nilai_model->delete_bobot_nilai($where_delete_bobot_nilai);
		$this->session->set_flashdata('success','Bobot Nilai telah berhasil dihapus.');
		
		redirect(module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$semester_kode));
	}

	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['bobot_nilai_id']	= validasi_sql($this->uri->segment(4)); 
		$bobot_nilai		 			= $this->Bobot_nilai_model->get_bobot_nilai('*', $where);
		if (!$bobot_nilai){
			redirect(module_url($this->uri->segment(2)));
		}
		
		$data['bobot_nilai_id'] = $bobot_nilai->bobot_nilai_id;
		$data['perguruan_tinggi_id'] = $bobot_nilai->perguruan_tinggi_id;
		$data['program_studi_id'] = $bobot_nilai->program_studi_id;
		$data['jenjang_kode'] = $bobot_nilai->jenjang_kode;
		$data['semester_kode'] = $bobot_nilai->semester_kode;
		$data['bobot_nilai_value'] = $bobot_nilai->bobot_nilai_value;
		$data['bobot_nilai_huruf'] = $bobot_nilai->bobot_nilai_huruf;
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/bobot_nilai', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function check_kode(){
		$data = array();
		$id = $this->input->post('id');
		$kode = $this->input->post('kode');
		$action = $this->input->post('action');
		$semester = $this->input->post('semester');
		$program_studi = $this->input->post('program_studi');
		if ($action == 'add' && $kode && $semester){
			$bobot_nilai = $this->Bobot_nilai_model->get_bobot_nilai("bobot_nilai_id", array('bobot_nilai_huruf'=>$kode, 'bobot_nilai.semester_kode'=>$semester, 'bobot_nilai.program_studi_id'=>$program_studi));
			if (!$bobot_nilai){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $bobot_nilai;
			} else {
				$data['response']	= false;
				$data['message']	= "Kode telah digunakan ada.";
			}
		} else if ($action == 'edit' && $kode && $id && $semester){
			$bobot_nilai = $this->Bobot_nilai_model->get_bobot_nilai("bobot_nilai_id", array('bobot_nilai_huruf'=>$kode, 'bobot_nilai.semester_kode'=>$semester, 'bobot_nilai.program_studi_id'=>$program_studi, 'bobot_nilai_id !='=>$id));
			if (!$bobot_nilai){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $bobot_nilai;
			} else {
				$data['response']	= false;
				$data['message']	= "Kode telah digunakan ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}