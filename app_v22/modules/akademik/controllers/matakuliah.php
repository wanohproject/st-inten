<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Matakuliah extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Matakuliah | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Referensi_model');
		$this->load->model('Matakuliah_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Dosen_model');
		$this->load->model('Dosen_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$where = "matakuliah.program_studi_id = '$program_studi_id'";

		$semester_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		if ($semester_kode){
			$where .= " AND matakuliah.semester_kode = '$semester_kode'";
		}

		$this->load->library('Datatables');
		$this->datatables->select('matakuliah_id, matakuliah_nama, matakuliah_kode, matakuliah_sks, matakuliah_semester_no, semester_kode')
		->add_column('Actions', $this->get_buttons('$1', '$2'),'matakuliah_id, matakuliah_kode')
		->search_column('matakuliah_nama, matakuliah_kode, matakuliah_sks, matakuliah_semester_no, semester_kode')
		->from('akd_matakuliah matakuliah')
		->where($where);
        echo $this->datatables->generate();
    }
	
	function get_buttons($id, $kode)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id.'/'.$kode) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$semester = $this->Semester_model->get_semester_aktif();
		$semester_kode					= ($this->uri->segment(5))?$this->uri->segment(5):($semester)?$semester->semester_kode:'';
		$data['semester_kode']			= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/matakuliah', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):'';
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):'';
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):'';
		$data['semester_kode'] = ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):'';
		$data['dosen_id'] = ($this->input->post('dosen_id'))?$this->input->post('dosen_id'):'';
		$data['matakuliah_id'] = ($this->input->post('matakuliah_id'))?$this->input->post('matakuliah_id'):'';
		$data['matakuliah_kode'] = ($this->input->post('matakuliah_kode'))?$this->input->post('matakuliah_kode'):'';
		$data['matakuliah_nama'] = ($this->input->post('matakuliah_nama'))?$this->input->post('matakuliah_nama'):'';
		$data['matakuliah_sks'] = ($this->input->post('matakuliah_sks'))?$this->input->post('matakuliah_sks'):'';
		$data['matakuliah_sks_tatap_muka'] = ($this->input->post('matakuliah_sks_tatap_muka'))?$this->input->post('matakuliah_sks_tatap_muka'):'';
		$data['matakuliah_sks_praktikum'] = ($this->input->post('matakuliah_sks_praktikum'))?$this->input->post('matakuliah_sks_praktikum'):'';
		$data['matakuliah_sks_lapangan'] = ($this->input->post('matakuliah_sks_lapangan'))?$this->input->post('matakuliah_sks_lapangan'):'';
		$data['matakuliah_semester_no'] = ($this->input->post('matakuliah_semester_no'))?$this->input->post('matakuliah_semester_no'):'';
		$data['matakuliah_silabus'] = ($this->input->post('matakuliah_silabus'))?$this->input->post('matakuliah_silabus'):'T';
		$data['matakuliah_sap'] = ($this->input->post('matakuliah_sap'))?$this->input->post('matakuliah_sap'):'T';
		$data['matakuliah_bahan'] = ($this->input->post('matakuliah_bahan'))?$this->input->post('matakuliah_bahan'):'T';
		$data['matakuliah_diktat'] = ($this->input->post('matakuliah_diktat'))?$this->input->post('matakuliah_diktat'):'T';
		$data['kelompok_matakuliah_kode'] = ($this->input->post('kelompok_matakuliah_kode'))?$this->input->post('kelompok_matakuliah_kode'):'';
		$data['kurikulum_jenis_kode'] = ($this->input->post('kurikulum_jenis_kode'))?$this->input->post('kurikulum_jenis_kode'):'';
		$data['matakuliah_jenis_kode'] = ($this->input->post('matakuliah_jenis_kode'))?$this->input->post('matakuliah_jenis_kode'):'';
		$data['matakuliah_status_kode'] = ($this->input->post('matakuliah_status_kode'))?$this->input->post('matakuliah_status_kode'):'';

		$data['dosen_mengajar_rencana_pertemuan'] = ($this->input->post('dosen_mengajar_rencana_pertemuan'))?$this->input->post('dosen_mengajar_rencana_pertemuan'):'';
		$data['dosen_mengajar_realisasi_pertemuan'] = ($this->input->post('dosen_mengajar_realisasi_pertemuan'))?$this->input->post('dosen_mengajar_realisasi_pertemuan'):'';
		$data['dosen_mengajar_kelas'] = ($this->input->post('dosen_mengajar_kelas'))?$this->input->post('dosen_mengajar_kelas'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");
			if ($get_program_studi && $this->Matakuliah_model->count_all_matakuliah(array('matakuliah_kode'=>$this->input->post('matakuliah_kode'), 'matakuliah.semester_kode'=>$this->input->post('semester_kode'), 'matakuliah.program_studi_id'=>$this->input->post('program_studi_id'))) < 1){
				$matakuliah_id = $this->uuid->v4();
				$insert = array();
				$insert['matakuliah_id'] = $matakuliah_id;
				$insert['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
				$insert['program_studi_id'] = $get_program_studi->program_studi_id;
				$insert['jenjang_kode'] = $get_program_studi->jenjang_kode;
				$insert['semester_kode'] = validasi_input($this->input->post('semester_kode'));
				$insert['dosen_id'] = validasi_input($this->input->post('dosen_id'));
				$insert['matakuliah_kode'] = validasi_input($this->input->post('matakuliah_kode'));
				$insert['matakuliah_nama'] = validasi_input($this->input->post('matakuliah_nama'));
				$insert['matakuliah_sks'] = validasi_input($this->input->post('matakuliah_sks'));
				$insert['matakuliah_sks_tatap_muka'] = validasi_input($this->input->post('matakuliah_sks_tatap_muka'));
				$insert['matakuliah_sks_praktikum'] = validasi_input($this->input->post('matakuliah_sks_praktikum'));
				$insert['matakuliah_sks_lapangan'] = validasi_input($this->input->post('matakuliah_sks_lapangan'));
				$insert['matakuliah_semester_no'] = validasi_input($this->input->post('matakuliah_semester_no'));
				$insert['matakuliah_silabus'] = validasi_input($this->input->post('matakuliah_silabus'));
				$insert['matakuliah_sap'] = validasi_input($this->input->post('matakuliah_sap'));
				$insert['matakuliah_bahan'] = validasi_input($this->input->post('matakuliah_bahan'));
				$insert['matakuliah_diktat'] = validasi_input($this->input->post('matakuliah_diktat'));
				$insert['kelompok_matakuliah_kode'] = validasi_input($this->input->post('kelompok_matakuliah_kode'));
				$insert['kurikulum_jenis_kode'] = validasi_input($this->input->post('kurikulum_jenis_kode'));
				$insert['matakuliah_jenis_kode'] = validasi_input($this->input->post('matakuliah_jenis_kode'));
				$insert['matakuliah_status_kode'] = validasi_input($this->input->post('matakuliah_status_kode'));
				$insert['created_by'] = userdata('pengguna_id');
						
				$this->Matakuliah_model->insert_matakuliah($insert);

				if ($this->input->post('dosen_id')){
					$insert_dosen = array();
					$insert_dosen['dosen_mengajar_id'] = $this->uuid->v4();
					$insert_dosen['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
					$insert_dosen['program_studi_id'] = $get_program_studi->program_studi_id;
					$insert_dosen['jenjang_kode'] = $get_program_studi->jenjang_kode;
					$insert_dosen['semester_kode'] = validasi_input($this->input->post('semester_kode'));
					$insert_dosen['dosen_id'] = validasi_input($this->input->post('dosen_id'));
					$insert_dosen['matakuliah_id'] = $matakuliah_id;
					$insert_dosen['dosen_mengajar_rencana_pertemuan'] = validasi_input($this->input->post('dosen_mengajar_rencana_pertemuan'));
					$insert_dosen['dosen_mengajar_realisasi_pertemuan'] = validasi_input($this->input->post('dosen_mengajar_realisasi_pertemuan'));
					$insert_dosen['dosen_mengajar_kelas'] = validasi_input($this->input->post('dosen_mengajar_kelas'));
					$insert_dosen['created_by'] = userdata('pengguna_id');
					$this->Dosen_model->insert_dosen_mengajar($insert_dosen);
				}

				$this->session->set_flashdata('success','Matakuliah telah berhasil ditambah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','Kode Matakuliah telah digunakan.');
				redirect(module_url($this->uri->segment(2)));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/matakuliah', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['matakuliah_id']		= validasi_sql($this->uri->segment(4)); 
		$matakuliah 				= $this->Matakuliah_model->get_matakuliah('*', $where);

		$data['matakuliah_id'] = ($this->input->post('matakuliah_id'))?$this->input->post('matakuliah_id'):$matakuliah->matakuliah_id;
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$matakuliah->perguruan_tinggi_id;
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$matakuliah->program_studi_id;
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):$matakuliah->jenjang_kode;
		$data['semester_kode'] = ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$matakuliah->semester_kode;
		$data['dosen_id'] = ($this->input->post('dosen_id'))?$this->input->post('dosen_id'):$matakuliah->dosen_id;
		$data['matakuliah_kode'] = ($this->input->post('matakuliah_kode'))?$this->input->post('matakuliah_kode'):$matakuliah->matakuliah_kode;
		$data['matakuliah_nama'] = ($this->input->post('matakuliah_nama'))?$this->input->post('matakuliah_nama'):$matakuliah->matakuliah_nama;
		$data['matakuliah_sks'] = ($this->input->post('matakuliah_sks'))?$this->input->post('matakuliah_sks'):$matakuliah->matakuliah_sks;
		$data['matakuliah_sks_tatap_muka'] = ($this->input->post('matakuliah_sks_tatap_muka'))?$this->input->post('matakuliah_sks_tatap_muka'):$matakuliah->matakuliah_sks_tatap_muka;
		$data['matakuliah_sks_praktikum'] = ($this->input->post('matakuliah_sks_praktikum'))?$this->input->post('matakuliah_sks_praktikum'):$matakuliah->matakuliah_sks_praktikum;
		$data['matakuliah_sks_lapangan'] = ($this->input->post('matakuliah_sks_lapangan'))?$this->input->post('matakuliah_sks_lapangan'):$matakuliah->matakuliah_sks_lapangan;
		$data['matakuliah_semester_no'] = ($this->input->post('matakuliah_semester_no'))?$this->input->post('matakuliah_semester_no'):$matakuliah->matakuliah_semester_no;
		$data['matakuliah_silabus'] = ($this->input->post('matakuliah_silabus'))?$this->input->post('matakuliah_silabus'):$matakuliah->matakuliah_silabus;
		$data['matakuliah_sap'] = ($this->input->post('matakuliah_sap'))?$this->input->post('matakuliah_sap'):$matakuliah->matakuliah_sap;
		$data['matakuliah_bahan'] = ($this->input->post('matakuliah_bahan'))?$this->input->post('matakuliah_bahan'):$matakuliah->matakuliah_bahan;
		$data['matakuliah_diktat'] = ($this->input->post('matakuliah_diktat'))?$this->input->post('matakuliah_diktat'):$matakuliah->matakuliah_diktat;
		$data['kelompok_matakuliah_kode'] = ($this->input->post('kelompok_matakuliah_kode'))?$this->input->post('kelompok_matakuliah_kode'):$matakuliah->kelompok_matakuliah_kode;
		$data['kurikulum_jenis_kode'] = ($this->input->post('kurikulum_jenis_kode'))?$this->input->post('kurikulum_jenis_kode'):$matakuliah->kurikulum_jenis_kode;
		$data['matakuliah_jenis_kode'] = ($this->input->post('matakuliah_jenis_kode'))?$this->input->post('matakuliah_jenis_kode'):$matakuliah->matakuliah_jenis_kode;
		$data['matakuliah_status_kode'] = ($this->input->post('matakuliah_status_kode'))?$this->input->post('matakuliah_status_kode'):$matakuliah->matakuliah_status_kode;
		
		$where_dosen_mengajar['dosen_mengajar.matakuliah_id']			= $matakuliah->matakuliah_id;
		$where_dosen_mengajar['dosen_mengajar.perguruan_tinggi_id']	= $matakuliah->perguruan_tinggi_id;
		$where_dosen_mengajar['dosen_mengajar.program_studi_id']		= $matakuliah->program_studi_id;
		$where_dosen_mengajar['dosen_mengajar.jenjang_kode']			= $matakuliah->jenjang_kode;
		$where_dosen_mengajar['dosen_mengajar.semester_kode']			= $matakuliah->semester_kode;
		$where_dosen_mengajar['dosen_mengajar.dosen_id']				= $matakuliah->dosen_id;
		$dosen_mengajar	= $this->Dosen_model->get_dosen_mengajar('*', $where_dosen_mengajar);

		$data['dosen_mengajar_rencana_pertemuan'] = ($this->input->post('dosen_mengajar_rencana_pertemuan'))?$this->input->post('dosen_mengajar_rencana_pertemuan'):($dosen_mengajar)?$dosen_mengajar->dosen_mengajar_rencana_pertemuan:'';
		$data['dosen_mengajar_realisasi_pertemuan'] = ($this->input->post('dosen_mengajar_realisasi_pertemuan'))?$this->input->post('dosen_mengajar_realisasi_pertemuan'):($dosen_mengajar)?$dosen_mengajar->dosen_mengajar_realisasi_pertemuan:'';
		$data['dosen_mengajar_kelas'] = ($this->input->post('dosen_mengajar_kelas'))?$this->input->post('dosen_mengajar_kelas'):($dosen_mengajar)?$dosen_mengajar->dosen_mengajar_kelas:'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");
			if ($get_program_studi && $this->Matakuliah_model->count_all_matakuliah(array('matakuliah_kode'=>$this->input->post('matakuliah_kode'), 'matakuliah_id !='=>$this->input->post('matakuliah_id'), 'matakuliah.semester_kode'=>$this->input->post('semester_kode'), 'matakuliah.program_studi_id'=>$this->input->post('program_studi_id'))) < 1){
	
				$where_update['matakuliah_id']	= validasi_sql($this->input->post('matakuliah_id'));
				$update['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
				$update['program_studi_id'] = $get_program_studi->program_studi_id;
				$update['jenjang_kode'] = $get_program_studi->jenjang_kode;
				$update['semester_kode'] = validasi_input($this->input->post('semester_kode'));
				$update['dosen_id'] = validasi_input($this->input->post('dosen_id'));
				$update['matakuliah_kode'] = validasi_input($this->input->post('matakuliah_kode'));
				$update['matakuliah_nama'] = validasi_input($this->input->post('matakuliah_nama'));
				$update['matakuliah_sks'] = validasi_input($this->input->post('matakuliah_sks'));
				$update['matakuliah_sks_tatap_muka'] = validasi_input($this->input->post('matakuliah_sks_tatap_muka'));
				$update['matakuliah_sks_praktikum'] = validasi_input($this->input->post('matakuliah_sks_praktikum'));
				$update['matakuliah_sks_lapangan'] = validasi_input($this->input->post('matakuliah_sks_lapangan'));
				$update['matakuliah_semester_no'] = validasi_input($this->input->post('matakuliah_semester_no'));
				$update['matakuliah_silabus'] = validasi_input($this->input->post('matakuliah_silabus'));
				$update['matakuliah_sap'] = validasi_input($this->input->post('matakuliah_sap'));
				$update['matakuliah_bahan'] = validasi_input($this->input->post('matakuliah_bahan'));
				$update['matakuliah_diktat'] = validasi_input($this->input->post('matakuliah_diktat'));
				$update['kelompok_matakuliah_kode'] = validasi_input($this->input->post('kelompok_matakuliah_kode'));
				$update['kurikulum_jenis_kode'] = validasi_input($this->input->post('kurikulum_jenis_kode'));
				$update['matakuliah_jenis_kode'] = validasi_input($this->input->post('matakuliah_jenis_kode'));
				$update['matakuliah_status_kode'] = validasi_input($this->input->post('matakuliah_status_kode'));
				$update['updated_by'] = userdata('pengguna_id');
	
				$this->Matakuliah_model->update_matakuliah($where_update, $update);

				if ($this->input->post('dosen_id')){
					if ($this->Dosen_model->count_all_dosen_mengajar(array('dosen_mengajar.dosen_id'=>$this->input->post('dosen_id'), 'dosen_mengajar.matakuliah_id'=>$this->input->post('matakuliah_id'), 'dosen_mengajar.program_studi_id'=>$this->input->post('program_studi_id'), 'dosen_mengajar.semester_kode'=>$this->input->post('semester_kode'))) < 1){
						$insert_dosen = array();
						$insert_dosen['dosen_mengajar_id'] = $this->uuid->v4();
						$insert_dosen['matakuliah_id'] = $this->input->post('matakuliah_id');
						$insert_dosen['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
						$insert_dosen['program_studi_id'] = $get_program_studi->program_studi_id;
						$insert_dosen['jenjang_kode'] = $get_program_studi->jenjang_kode;
						$insert_dosen['semester_kode'] = validasi_input($this->input->post('semester_kode'));
						$insert_dosen['dosen_id'] = validasi_input($this->input->post('dosen_id'));
						$insert_dosen['dosen_mengajar_rencana_pertemuan'] = validasi_input($this->input->post('dosen_mengajar_rencana_pertemuan'));
						$insert_dosen['dosen_mengajar_realisasi_pertemuan'] = validasi_input($this->input->post('dosen_mengajar_realisasi_pertemuan'));
						$insert_dosen['dosen_mengajar_kelas'] = validasi_input($this->input->post('dosen_mengajar_kelas'));
						$insert_dosen['created_by'] = userdata('pengguna_id');
						$this->Dosen_model->insert_dosen_mengajar($insert_dosen);
					} else {
						$update_dosen = array();
						$update_dosen['dosen_mengajar_rencana_pertemuan'] = validasi_input($this->input->post('dosen_mengajar_rencana_pertemuan'));
						$update_dosen['dosen_mengajar_realisasi_pertemuan'] = validasi_input($this->input->post('dosen_mengajar_realisasi_pertemuan'));
						$update_dosen['dosen_mengajar_kelas'] = validasi_input($this->input->post('dosen_mengajar_kelas'));
						$update_dosen['updated_by'] = userdata('pengguna_id');
						$this->Dosen_model->update_dosen_mengajar(array('matakuliah_id'=>$this->input->post('matakuliah_id'), 'program_studi_id'=>$this->input->post('program_studi_id'), 'semester_kode'=>$this->input->post('semester_kode')), $update_dosen);
					}
				}
				
				$this->session->set_flashdata('success','Matakuliah telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','Kode Matakuliah telah digunakan.');
				redirect(module_url($this->uri->segment(2)));
			}
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/matakuliah', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$matakuliah_id		= validasi_sql($this->uri->segment(4));
		$matakuliah_user		= validasi_sql($this->uri->segment(5));
		
		$where_delete_matakuliah['matakuliah_id']	= $matakuliah_id;
		$this->Matakuliah_model->delete_matakuliah($where_delete_matakuliah);
		$this->session->set_flashdata('success','Matakuliah telah berhasil dihapus.');
		
		redirect(module_url($this->uri->segment(2)));
	}

	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['matakuliah_id']	= validasi_sql($this->uri->segment(4)); 
		$matakuliah		 			= $this->Matakuliah_model->get_matakuliah('*', $where);
		if (!$matakuliah){
			redirect(module_url($this->uri->segment(2)));
		}
		
		$data['matakuliah_id'] = $matakuliah->matakuliah_id;
		$data['perguruan_tinggi_id'] = $matakuliah->perguruan_tinggi_id;
		$data['program_studi_id'] = $matakuliah->program_studi_id;
		$data['jenjang_kode'] = $matakuliah->jenjang_kode;
		$data['semester_kode'] = $matakuliah->semester_kode;
		$data['dosen_id'] = $matakuliah->dosen_id;
		$data['matakuliah_kode'] = $matakuliah->matakuliah_kode;
		$data['matakuliah_nama'] = $matakuliah->matakuliah_nama;
		$data['matakuliah_sks'] = $matakuliah->matakuliah_sks;
		$data['matakuliah_sks_tatap_muka'] = $matakuliah->matakuliah_sks_tatap_muka;
		$data['matakuliah_sks_praktikum'] = $matakuliah->matakuliah_sks_praktikum;
		$data['matakuliah_sks_lapangan'] = $matakuliah->matakuliah_sks_lapangan;
		$data['matakuliah_semester_no'] = $matakuliah->matakuliah_semester_no;
		$data['matakuliah_silabus'] = $matakuliah->matakuliah_silabus;
		$data['matakuliah_sap'] = $matakuliah->matakuliah_sap;
		$data['matakuliah_bahan'] = $matakuliah->matakuliah_bahan;
		$data['matakuliah_diktat'] = $matakuliah->matakuliah_diktat;
		$data['kelompok_matakuliah_kode'] = $matakuliah->kelompok_matakuliah_kode;
		$data['kurikulum_jenis_kode'] = $matakuliah->kurikulum_jenis_kode;
		$data['matakuliah_jenis_kode'] = $matakuliah->matakuliah_jenis_kode;
		$data['matakuliah_status_kode'] = $matakuliah->matakuliah_status_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/matakuliah', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function import()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'import';
		
		$data['dataExcel']	= array();		
		$data['filename']	= '';
		
		if ($this->input->post('importMatakuliah')){
			if ($_FILES['userfile']['tmp_name']){
				$ext = end(explode(".", basename($_FILES['userfile']['name'])));
				if ($ext == 'xls'){
					
					$timestamp = explode(" ",microtime());
					$filename = time().str_shuffle('123456ABCDEF');
					
					$uploaddir = './asset/temp_upload/';
					$uploadfname = 'matakuliah_'.$filename . '.' . end(explode(".", basename($_FILES['userfile']['name'])));
					$uploadfile = $uploaddir . $uploadfname;
					$data['filename'] = $uploadfname;
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					
						$this->load->library('excel_reader');
						$this->excel_reader->setOutputEncoding('CP1251');
						$this->excel_reader->read($uploadfile);
						$dataFile = $this->excel_reader->sheets[0];
						
						$paramsReal = array("NO", "PROGRAM STUDI", "SEMESTER PELAPORAN", "KODE MATA KULIAH", "NAMA MATA KULIAH", "SEMESTER", "KELOMPOK MATA KULIAH", "JENIS KURIKULUM", "JENIS MATA KULIAH", "STATUS MATA KULIAH", "SKS MATA KULIAH", "SKS TATAP MUKA", "SKS PRAKTIKUM", "SKS PRAKTIK LAPANGAN", "ADA SILABUS", "ADA SAP", "ADA BAHAN", "ADA DIKTAT", "DOSEN PENGAMPU");
						$paramsReal = $iOne = array_combine(range(1, count($paramsReal)), array_values($paramsReal));
						$paramsFile = $dataFile['cells'][2];
						$result 	= array_diff($paramsReal,$paramsFile);
						
						if (count($result) == 0){
							for ($i = 4; $i <= $dataFile['numRows']; $i++) {
								if($dataFile['cells'][$i][1]){
									$data['dataExcel'][$i]['matakuliah_id'] 			= (isset($dataFile['cells'][$i][1]))?trim($dataFile['cells'][$i][1]):'';
									$data['dataExcel'][$i]['matakuliah_prodi'] 			= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
									$data['dataExcel'][$i]['semester_kode'] 			= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
									$data['dataExcel'][$i]['matakuliah_kode'] 			= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
									$data['dataExcel'][$i]['matakuliah_nama'] 			= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
									$data['dataExcel'][$i]['matakuliah_semester_no'] 	= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';
									$data['dataExcel'][$i]['kelompok_matakuliah_kode'] 	= (isset($dataFile['cells'][$i][7]))?trim($dataFile['cells'][$i][7]):'';
									$data['dataExcel'][$i]['kurikulum_jenis_kode'] 		= (isset($dataFile['cells'][$i][8]))?trim($dataFile['cells'][$i][8]):'';
									$data['dataExcel'][$i]['matakuliah_jenis_kode'] 	= (isset($dataFile['cells'][$i][9]))?trim($dataFile['cells'][$i][9]):'';
									$data['dataExcel'][$i]['matakuliah_status_kode'] 	= (isset($dataFile['cells'][$i][10]))?trim($dataFile['cells'][$i][10]):'';
									$data['dataExcel'][$i]['matakuliah_sks'] 			= (isset($dataFile['cells'][$i][11]))?trim($dataFile['cells'][$i][11]):'';
									$data['dataExcel'][$i]['matakuliah_sks_tatap_muka'] = (isset($dataFile['cells'][$i][12]))?trim($dataFile['cells'][$i][12]):'';
									$data['dataExcel'][$i]['matakuliah_sks_praktikum'] 	= (isset($dataFile['cells'][$i][13]))?trim($dataFile['cells'][$i][13]):'';
									$data['dataExcel'][$i]['matakuliah_sks_lapangan'] 	= (isset($dataFile['cells'][$i][14]))?trim($dataFile['cells'][$i][14]):'';
									$data['dataExcel'][$i]['matakuliah_silabus'] 		= (isset($dataFile['cells'][$i][15]))?trim($dataFile['cells'][$i][15]):'';
									$data['dataExcel'][$i]['matakuliah_sap'] 			= (isset($dataFile['cells'][$i][16]))?trim($dataFile['cells'][$i][16]):'';
									$data['dataExcel'][$i]['matakuliah_bahan'] 			= (isset($dataFile['cells'][$i][17]))?trim($dataFile['cells'][$i][17]):'';
									$data['dataExcel'][$i]['matakuliah_diktat'] 		= (isset($dataFile['cells'][$i][18]))?trim($dataFile['cells'][$i][18]):'';
									$data['dataExcel'][$i]['dosen_id'] 					= (isset($dataFile['cells'][$i][19]))?trim($dataFile['cells'][$i][19]):'';
									
									$data['dataExcel'][$i]['data_prodi'] 	= 'PRODI INVALID';
									if ($this->Program_studi_model->count_all_program_studi("(program_studi_kode = '".$data['dataExcel'][$i]['matakuliah_prodi']."' OR program_studi_nama = '".$data['dataExcel'][$i]['matakuliah_prodi']."')") > 0){
										$data['dataExcel'][$i]['data_prodi'] 	= 'PRODI VALID';
									}
								}
							}
						} else {
							$this->session->set_flashdata('error','Format file salah.');
							redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
						}
					} else {
						$this->session->set_flashdata('error','File gagal di-upload.');
						redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
					}
				} else {
					$this->session->set_flashdata('error','Format file yang Anda gunakan salah. Silahkan gunakan format file Excel 97-2003 (.xls).');
					redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Silahkan masukkan file yang akan di-import.');
				redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
			}
		}
		
		if ($this->input->post('save')){
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$this->excel_reader->read('./asset/temp_upload/'.$this->input->post('filename'));
			$dataFile = $this->excel_reader->sheets[0];
			
			for ($i = 4; $i <= $dataFile['numRows']; $i++) {
				if($dataFile['cells'][$i][1]){
					$matakuliah_id 				= (isset($dataFile['cells'][$i][1]))?trim($dataFile['cells'][$i][1]):'';
					$matakuliah_prodi 			= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
					$semester_kode 				= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
					$matakuliah_kode 			= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
					$matakuliah_nama 			= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
					$matakuliah_semester_no 	= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';
					$kelompok_matakuliah_kode 	= (isset($dataFile['cells'][$i][7]))?trim($dataFile['cells'][$i][7]):'';
					$kurikulum_jenis_kode 		= (isset($dataFile['cells'][$i][8]))?trim($dataFile['cells'][$i][8]):'';
					$matakuliah_jenis_kode 		= (isset($dataFile['cells'][$i][9]))?trim($dataFile['cells'][$i][9]):'';
					$matakuliah_status_kode 	= (isset($dataFile['cells'][$i][10]))?trim($dataFile['cells'][$i][10]):'';
					$matakuliah_sks 			= (isset($dataFile['cells'][$i][11]))?trim($dataFile['cells'][$i][11]):'';
					$matakuliah_sks_tatap_muka 	= (isset($dataFile['cells'][$i][12]))?trim($dataFile['cells'][$i][12]):'';
					$matakuliah_sks_praktikum 	= (isset($dataFile['cells'][$i][13]))?trim($dataFile['cells'][$i][13]):'';
					$matakuliah_sks_lapangan 	= (isset($dataFile['cells'][$i][14]))?trim($dataFile['cells'][$i][14]):'';
					$matakuliah_silabus 		= (isset($dataFile['cells'][$i][15]))?trim($dataFile['cells'][$i][15]):'';
					$matakuliah_sap 			= (isset($dataFile['cells'][$i][16]))?trim($dataFile['cells'][$i][16]):'';
					$matakuliah_bahan 			= (isset($dataFile['cells'][$i][17]))?trim($dataFile['cells'][$i][17]):'';
					$matakuliah_diktat 			= (isset($dataFile['cells'][$i][18]))?trim($dataFile['cells'][$i][18]):'';
					$dosen_id 					= (isset($dataFile['cells'][$i][19]))?trim($dataFile['cells'][$i][19]):'';
					
					$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "(program_studi_kode = '$matakuliah_prodi' OR program_studi_nama = '$matakuliah_prodi')");
					$get_dosen_id		= $this->Dosen_model->get_dosen("*", "(dosen_nidn = '$dosen_id' OR dosen_nama = '$dosen_id')");
					
					if ($get_program_studi && $matakuliah_kode){
						if ($this->Matakuliah_model->count_all_matakuliah(array("matakuliah.matakuliah_kode"=>$matakuliah_kode, "matakuliah.semester_kode"=>$semester_kode, "matakuliah.program_studi_id"=>$get_program_studi->program_studi_id)) < 1){
							$matakuliah_id = $this->uuid->v4();
							$insert['matakuliah_id'] = $matakuliah_id;
							$insert['perguruan_tinggi_id'] = validasi_input($get_program_studi->perguruan_tinggi_id);
							$insert['program_studi_id'] = validasi_input($get_program_studi->program_studi_id);
							$insert['jenjang_kode'] = validasi_input($get_program_studi->jenjang_kode);
							$insert['semester_kode'] = validasi_input($semester_kode);
							$insert['dosen_id'] = validasi_input(($get_dosen_id)?$get_dosen_id->dosen_id:'');
							$insert['matakuliah_kode'] = validasi_input($matakuliah_kode);
							$insert['matakuliah_nama'] = validasi_input($matakuliah_nama);
							$insert['matakuliah_sks'] = validasi_input($matakuliah_sks);
							$insert['matakuliah_sks_tatap_muka'] = validasi_input($matakuliah_sks_tatap_muka);
							$insert['matakuliah_sks_praktikum'] = validasi_input($matakuliah_sks_praktikum);
							$insert['matakuliah_sks_lapangan'] = validasi_input($matakuliah_sks_lapangan);
							$insert['matakuliah_semester_no'] = validasi_input($matakuliah_semester_no);
							$insert['matakuliah_silabus'] = validasi_input($matakuliah_silabus);
							$insert['matakuliah_sap'] = validasi_input($matakuliah_sap);
							$insert['matakuliah_bahan'] = validasi_input($matakuliah_bahan);
							$insert['matakuliah_diktat'] = validasi_input($matakuliah_diktat);
							$insert['kelompok_matakuliah_kode'] = validasi_input($kelompok_matakuliah_kode);
							$insert['kurikulum_jenis_kode'] = validasi_input($kurikulum_jenis_kode);
							$insert['matakuliah_jenis_kode'] = validasi_input($matakuliah_jenis_kode);
							$insert['matakuliah_status_kode'] = validasi_input($matakuliah_status_kode);
							$insert['created_by'] = userdata('pengguna_id');

							if ($dosen_id){
								if ($this->Dosen_model->count_all_dosen_mengajar(array('dosen_mengajar.dosen_id'=>$dosen_id, 'dosen_mengajar.matakuliah_id'=>$matakuliah_id, 'dosen_mengajar.program_studi_id'=>$get_program_studi->program_studi_id, 'dosen_mengajar.semester_kode'=>$semester_kode)) < 1){
									$insert_dosen = array();
									$insert_dosen['dosen_mengajar_id'] = $this->uuid->v4();
									$insert_dosen['matakuliah_id'] = $matakuliah_id;
									$insert_dosen['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
									$insert_dosen['program_studi_id'] = $get_program_studi->program_studi_id;
									$insert_dosen['jenjang_kode'] = $get_program_studi->jenjang_kode;
									$insert_dosen['semester_kode'] = validasi_input($semester_kode);
									$insert_dosen['dosen_id'] = validasi_input($dosen_id);
									$insert_dosen['created_by'] = userdata('pengguna_id');
									$this->Dosen_model->insert_dosen_mengajar($insert_dosen);
								} else {
									$update_dosen = array();
									$update_dosen['updated_by'] = userdata('pengguna_id');
									$this->Dosen_model->update_dosen_mengajar(array('matakuliah_id'=>$matakuliah_id, 'program_studi_id'=>$get_program_studi->program_studi_id, 'semester_kode'=>$semester_kode), $update_dosen);
								}
							}
									
							$this->Matakuliah_model->insert_matakuliah($insert);
						} else if ($this->Matakuliah_model->count_all_matakuliah(array("matakuliah.matakuliah_kode"=>$matakuliah_kode, "matakuliah.semester_kode"=>$semester_kode, "matakuliah.program_studi_id"=>$get_program_studi->program_studi_id)) > 0){
							$get_matakuliah = $this->Matakuliah_model->get_matakuliah("", array("matakuliah.matakuliah_kode"=>$matakuliah_kode, "matakuliah.semester_kode"=>$semester_kode));

							$update['perguruan_tinggi_id'] = validasi_input($get_program_studi->perguruan_tinggi_id);
							$update['program_studi_id'] = validasi_input($get_program_studi->program_studi_id);
							$update['jenjang_kode'] = validasi_input($get_program_studi->jenjang_kode);
							$update['dosen_id'] = validasi_input(($get_dosen_id)?$get_dosen_id->dosen_id:'');
							$update['matakuliah_nama'] = validasi_input($matakuliah_nama);
							$update['matakuliah_sks'] = validasi_input($matakuliah_sks);
							$update['matakuliah_sks_tatap_muka'] = validasi_input($matakuliah_sks_tatap_muka);
							$update['matakuliah_sks_praktikum'] = validasi_input($matakuliah_sks_praktikum);
							$update['matakuliah_sks_lapangan'] = validasi_input($matakuliah_sks_lapangan);
							$update['matakuliah_semester_no'] = validasi_input($matakuliah_semester_no);
							$update['matakuliah_silabus'] = validasi_input($matakuliah_silabus);
							$update['matakuliah_sap'] = validasi_input($matakuliah_sap);
							$update['matakuliah_bahan'] = validasi_input($matakuliah_bahan);
							$update['matakuliah_diktat'] = validasi_input($matakuliah_diktat);
							$update['kelompok_matakuliah_kode'] = validasi_input($kelompok_matakuliah_kode);
							$update['kurikulum_jenis_kode'] = validasi_input($kurikulum_jenis_kode);
							$update['matakuliah_jenis_kode'] = validasi_input($matakuliah_jenis_kode);
							$update['matakuliah_status_kode'] = validasi_input($matakuliah_status_kode);
							$update['updated_by'] = userdata('pengguna_id');
							$this->Matakuliah_model->update_matakuliah(array('matakuliah_id'=>$get_matakuliah->matakuliah_id, 'semester_kode'=>$semester_kode), $update);

							if ($dosen_id){
								if ($this->Dosen_model->count_all_dosen_mengajar(array('dosen_id'=>$get_matakuliah->dosen_id, 'dosen_id'=>$get_matakuliah->matakuliah_id, 'program_studi_id'=>$get_program_studi->program_studi_id, 'semester_kode'=>$semester_kode)) < 1){
									$insert_dosen = array();
									$insert_dosen['dosen_mengajar_id'] = $this->uuid->v4();
									$insert_dosen['matakuliah_id'] = $get_matakuliah->matakuliah_id;
									$insert_dosen['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
									$insert_dosen['program_studi_id'] = $get_program_studi->program_studi_id;
									$insert_dosen['jenjang_kode'] = $get_program_studi->jenjang_kode;
									$insert_dosen['semester_kode'] = validasi_input($semester_kode);
									$insert_dosen['dosen_id'] = validasi_input($dosen_id);
									$insert_dosen['created_by'] = userdata('pengguna_id');
									$this->Dosen_model->insert_dosen_mengajar($insert_dosen);
								}
							}
						}
					}
				}
			}

			$this->session->set_flashdata('success','Matakuliah telah berhasil di-import.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/matakuliah', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	// pengajar
	public function datatable_pengajar()
    {
		$matakuliah_id = $this->uri->segment(4);
		$this->Datatable_model->set_table("(SELECT dosen_mengajar.*, dosen.dosen_nama FROM akd_dosen_mengajar dosen_mengajar LEFT JOIN akd_dosen dosen ON dosen_mengajar.dosen_id=dosen.dosen_id WHERE matakuliah_id = '$matakuliah_id') dosen_mengajar");
		$this->Datatable_model->set_column_order(array('dosen_nama', 'dosen_mengajar_rencana_pertemuan', 'dosen_mengajar_realisasi_pertemuan', 'dosen_mengajar_kelas', null));
		$this->Datatable_model->set_column_search(array('dosen_nama', 'dosen_mengajar_rencana_pertemuan', 'dosen_mengajar_realisasi_pertemuan', 'dosen_mengajar_kelas'));
		$this->Datatable_model->set_order(array('dosen.dosen_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['dosen_nama'] = $record->dosen_nama;
            $row['dosen_mengajar_rencana_pertemuan'] = $record->dosen_mengajar_rencana_pertemuan;
            $row['dosen_mengajar_realisasi_pertemuan'] = $record->dosen_mengajar_realisasi_pertemuan;
            $row['dosen_mengajar_kelas'] = $record->dosen_mengajar_kelas;
            $row['matakuliah_id'] = $record->matakuliah_id;
            $row['dosen_mengajar_id'] = $record->dosen_mengajar_id;
			
			$html  = '<div class="text-center">';
			if (check_permission("W")){
				$html .= '<a href="javascript:void(0);" data-id="'.$record->dosen_mengajar_id.'" data-tabs="3" data-action="edit" class="btn btn-warning btn-sm btn-edit" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
				$html .= '<a href="'. site_url($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/delete_pengajar/'.$record->dosen_mengajar_id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
			}
			$html .= '</div>';
			
            $row['Actions'] = $html;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function add_pengajar()
	{	
		$matakuliah_id = $this->input->post('matakuliah_id');
		$matakuliah = $this->Matakuliah_model->get_matakuliah('matakuliah.*', array('matakuliah.matakuliah_id'=>$matakuliah_id));
		$save = $this->input->post('save');
		if ($save && $matakuliah){
			$insert['dosen_mengajar_id'] = $this->uuid->v4();
			$insert['perguruan_tinggi_id'] = $matakuliah->perguruan_tinggi_id;
			$insert['program_studi_id'] = $matakuliah->program_studi_id;
			$insert['jenjang_kode'] = $matakuliah->jenjang_kode;
			$insert['semester_kode'] = $matakuliah->semester_kode;
			$insert['matakuliah_id'] = $matakuliah->matakuliah_id;
			$insert['dosen_id'] = validasi_sql($this->input->post('ak_dosen_id'));
			$insert['dosen_mengajar_rencana_pertemuan'] = validasi_sql($this->input->post('ak_rencana_pertemuan'));
			$insert['dosen_mengajar_realisasi_pertemuan'] = validasi_sql($this->input->post('ak_realisasi_pertemuan'));
			$insert['dosen_mengajar_kelas'] = validasi_sql($this->input->post('ak_mengajar_kelas'));
			$insert['created_by'] = userdata('pengguna_id');
			$this->Dosen_model->insert_dosen_mengajar($insert);
			
			$this->session->set_flashdata('success','Data telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/detail/'.$matakuliah_id.'?tabs=1'));
		}
	}
	
	public function edit_pengajar()
	{	
		$matakuliah_id = $this->input->post('matakuliah_id');
		$matakuliah = $this->Matakuliah_model->get_matakuliah('matakuliah.*', array('matakuliah.matakuliah_id'=>$matakuliah_id));
		
		$dosen_mengajar_id = $this->input->post('ek_id');
		$dosen_mengajar = $this->Dosen_model->get_dosen_mengajar("dosen_mengajar.*", array('dosen_mengajar_id'=>$dosen_mengajar_id));

		$save = $this->input->post('save');
		if ($save && $matakuliah && $dosen_mengajar){
			$update['perguruan_tinggi_id'] = $matakuliah->perguruan_tinggi_id;
			$update['program_studi_id'] = $matakuliah->program_studi_id;
			$update['jenjang_kode'] = $matakuliah->jenjang_kode;
			$update['semester_kode'] = $matakuliah->semester_kode;
			$update['matakuliah_id'] = $matakuliah->matakuliah_id;
			$update['dosen_id'] = validasi_sql($this->input->post('ek_dosen_id'));
			$update['dosen_mengajar_rencana_pertemuan'] = validasi_sql($this->input->post('ek_rencana_pertemuan'));
			$update['dosen_mengajar_realisasi_pertemuan'] = validasi_sql($this->input->post('ek_realisasi_pertemuan'));
			$update['dosen_mengajar_kelas'] = validasi_sql($this->input->post('ek_mengajar_kelas'));
			$update['created_by'] = userdata('pengguna_id');
			$this->Dosen_model->update_dosen_mengajar(array('dosen_mengajar_id'=>$dosen_mengajar_id), $update);
			
			$this->session->set_flashdata('success','Data telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/detail/'.$matakuliah_id.'?tabs=1'));
		}
	}

	public function delete_pengajar()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$dosen_mengajar_id = $this->uri->segment(4);
		$dosen_mengajar = $this->Dosen_model->get_dosen_mengajar('dosen_mengajar.matakuliah_id', array('dosen_mengajar.dosen_mengajar_id'=>$dosen_mengajar_id));

		$this->Dosen_model->delete_dosen_mengajar(array('dosen_mengajar_id'=>validasi_sql($dosen_mengajar_id)));
		
		$this->session->set_flashdata('success','Data telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2).'/detail/'.$dosen_mengajar->matakuliah_id.'?tabs=1'));
	}
	
	public function get_pengajar(){
		header('Content-Type: application/json');
		$data = array();
		$dosen_mengajar_id = validasi_sql($this->uri->segment(4));
		if ($dosen_mengajar_id){
			$dosen_mengajar 	= $this->Dosen_model->get_dosen_mengajar("dosen_mengajar.*", array('dosen_mengajar_id'=>$dosen_mengajar_id));
			if ($dosen_mengajar){
				$data['response']	= true;
				$data['message']	= "Data Found";
				$data['data']		= $dosen_mengajar;
			} else {
				$data['response']	= false;
				$data['message']	= "Data Not Found";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Paramater Not Complete";
		}
		echo json_encode($data);
	}
	
	public function check_kode(){
		$data = array();
		$id = $this->input->post('id');
		$kode = $this->input->post('kode');
		$action = $this->input->post('action');
		$semester = $this->input->post('semester');
		$program_studi = $this->input->post('program_studi');
		if ($action == 'add' && $kode && $semester && $program_studi){
			$matakuliah = $this->Matakuliah_model->get_matakuliah("matakuliah_id", array('matakuliah_kode'=>$kode, 'matakuliah.semester_kode'=>$semester, 'matakuliah.program_studi_id'=>$program_studi));
			if (!$matakuliah){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $matakuliah;
			} else {
				$data['response']	= false;
				$data['message']	= "Kode telah digunakan ada.";
			}
		} else if ($action == 'edit' && $kode && $id && $semester){
			$matakuliah = $this->Matakuliah_model->get_matakuliah("matakuliah_id", array('matakuliah_kode'=>$kode, 'matakuliah.semester_kode'=>$semester, 'matakuliah.program_studi_id'=>$program_studi, 'matakuliah_id !='=>$id));
			if (!$matakuliah){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $matakuliah;
			} else {
				$data['response']	= false;
				$data['message']	= "Kode telah digunakan ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}
