<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Mahasiswa | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Referensi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Dosen_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$where = "mahasiswa.program_studi_id = '$program_studi_id'";

		$tahun_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		if ($tahun_kode != '-'){
			$where .= " AND mahasiswa_tahun_masuk = '$tahun_kode'";
		}

		$this->load->library('Datatables');
		$this->datatables->select('mahasiswa_id, mahasiswa_nama, mahasiswa_nim, mahasiswa_user, mahasiswa_pin, mahasiswa_tahun_masuk')
		->add_column('Actions', $this->get_buttons('$1', '$2'),'mahasiswa_id, mahasiswa_user')
		->add_column('mahasiswa_pin', $this->get_genpinmahasiswa('$1', '$2', '$3'),'mahasiswa_id, mahasiswa_pin, mahasiswa_user')
		->search_column('mahasiswa_nama, mahasiswa_nim, mahasiswa_user, mahasiswa_pin, mahasiswa_tahun_masuk')
		->from('akd_mahasiswa mahasiswa')
		->join("akd_tahun tahun", "mahasiswa.mahasiswa_tahun_masuk=tahun.tahun_kode", "left")
		->where($where);
        echo $this->datatables->generate();
    }
	
	function get_genpinmahasiswa($id, $pin, $nis){
		$ci= & get_instance();
		$ci->load->helper('url');
		$html = $pin . ' <a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/genpinmahasiswa/'.$id.'/'.$nis) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan mereset pin mahasiswa ini.\');" title="Reload"><i class="fa fa-refresh"></i></a>';
		return $html;
	}
	
	function get_genpinortu($id, $pin, $nis){
		$ci= & get_instance();
		$ci->load->helper('url');
		$html = $pin . ' <a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/genpinortu/'.$id.'/'.$nis) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan mereset pin orang tua mahasiswa ini.\');" title="Reload"><i class="fa fa-refresh"></i></a>';
		return $html;
	}
	
	function get_buttons($id, $nis)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete-user/'.$id.'/'.$nis) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus user mahasiswa ini.\');" title="Hapus User"><i class="fa fa-remove"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id.'/'.$nis) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['mahasiswa_id'] = ($this->input->post('mahasiswa_id'))?$this->input->post('mahasiswa_id'):'';
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):'';
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):'';
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):'';
		$data['mahasiswa_nim'] = ($this->input->post('mahasiswa_nim'))?$this->input->post('mahasiswa_nim'):'';
		$data['mahasiswa_nama'] = ($this->input->post('mahasiswa_nama'))?$this->input->post('mahasiswa_nama'):'';
		$data['mahasiswa_tempat_lahir'] = ($this->input->post('mahasiswa_tempat_lahir'))?$this->input->post('mahasiswa_tempat_lahir'):'';
		$data['mahasiswa_tanggal_lahir'] = ($this->input->post('mahasiswa_tanggal_lahir'))?$this->input->post('mahasiswa_tanggal_lahir'):'';
		$data['mahasiswa_jenis_kelamin'] = ($this->input->post('mahasiswa_jenis_kelamin'))?$this->input->post('mahasiswa_jenis_kelamin'):'';
		$data['mahasiswa_tahun_masuk'] = ($this->input->post('mahasiswa_tahun_masuk'))?$this->input->post('mahasiswa_tahun_masuk'):'';
		$data['mahasiswa_semester_awal'] = ($this->input->post('mahasiswa_semester_awal'))?$this->input->post('mahasiswa_semester_awal'):'';
		$data['mahasiswa_batas_studi'] = ($this->input->post('mahasiswa_batas_studi'))?$this->input->post('mahasiswa_batas_studi'):'';
		$data['mahasiswa_tanggal_masuk'] = ($this->input->post('mahasiswa_tanggal_masuk'))?$this->input->post('mahasiswa_tanggal_masuk'):'';
		$data['mahasiswa_tanggal_lulus'] = ($this->input->post('mahasiswa_tanggal_lulus'))?$this->input->post('mahasiswa_tanggal_lulus'):'';
		$data['mahasiswa_sks_diakui'] = ($this->input->post('mahasiswa_sks_diakui'))?$this->input->post('mahasiswa_sks_diakui'):'';
		$data['mahasiswa_pindahan_nim'] = ($this->input->post('mahasiswa_pindahan_nim'))?$this->input->post('mahasiswa_pindahan_nim'):'';
		$data['mahasiswa_pindahan_pt'] = ($this->input->post('mahasiswa_pindahan_pt'))?$this->input->post('mahasiswa_pindahan_pt'):'';
		$data['mahasiswa_pindahan_prodi'] = ($this->input->post('mahasiswa_pindahan_prodi'))?$this->input->post('mahasiswa_pindahan_prodi'):'';
		$data['mahasiswa_pindahan_jenjang'] = ($this->input->post('mahasiswa_pindahan_jenjang'))?$this->input->post('mahasiswa_pindahan_jenjang'):'';
		$data['mahasiswa_user'] = ($this->input->post('mahasiswa_user'))?$this->input->post('mahasiswa_user'):'';
		$data['mahasiswa_pin'] = ($this->input->post('mahasiswa_pin'))?$this->input->post('mahasiswa_pin'):'';
		$data['mahasiswa_dosen_wali'] = ($this->input->post('mahasiswa_dosen_wali'))?$this->input->post('mahasiswa_dosen_wali'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('mahasiswa_user'))."'") < 1 && 
				$this->Mahasiswa_model->count_all_mahasiswa("mahasiswa_user = '".validasi_sql($this->input->post('mahasiswa_user'))."'") < 1){
					
				$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");

				$insert['mahasiswa_id'] = $this->uuid->v4();
				$insert['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
				$insert['program_studi_id'] = validasi_input($this->input->post('program_studi_id'));
				$insert['jenjang_kode'] = $get_program_studi->jenjang_kode;
				$insert['mahasiswa_nim'] = validasi_input($this->input->post('mahasiswa_nim'));
				$insert['mahasiswa_nama'] = validasi_input($this->input->post('mahasiswa_nama'));
				$insert['mahasiswa_tempat_lahir'] = validasi_input($this->input->post('mahasiswa_tempat_lahir'));
				$insert['mahasiswa_tanggal_lahir'] = validasi_input($this->input->post('mahasiswa_tanggal_lahir'));
				$insert['mahasiswa_jenis_kelamin'] = validasi_input($this->input->post('mahasiswa_jenis_kelamin'));
				$insert['mahasiswa_tahun_masuk'] = validasi_input($this->input->post('mahasiswa_tahun_masuk'));
				$insert['mahasiswa_semester_awal'] = validasi_input($this->input->post('mahasiswa_semester_awal'));
				$insert['mahasiswa_batas_studi'] = validasi_input($this->input->post('mahasiswa_batas_studi'));
				$insert['mahasiswa_tanggal_masuk'] = validasi_input($this->input->post('mahasiswa_tanggal_masuk'));
				$insert['mahasiswa_tanggal_lulus'] = validasi_input($this->input->post('mahasiswa_tanggal_lulus'));
				$insert['mahasiswa_sks_diakui'] = validasi_input($this->input->post('mahasiswa_sks_diakui'));
				$insert['mahasiswa_pindahan_nim'] = validasi_input($this->input->post('mahasiswa_pindahan_nim'));
				$insert['mahasiswa_pindahan_pt'] = validasi_input($this->input->post('mahasiswa_pindahan_pt'));
				$insert['mahasiswa_pindahan_prodi'] = validasi_input($this->input->post('mahasiswa_pindahan_prodi'));
				$insert['mahasiswa_pindahan_jenjang'] = validasi_input($this->input->post('mahasiswa_pindahan_jenjang'));
				$insert['mahasiswa_user'] = validasi_input($this->input->post('mahasiswa_user'));
				$insert['mahasiswa_pin'] = validasi_input($this->input->post('mahasiswa_pin'));
				$insert['mahasiswa_dosen_wali'] = validasi_input($this->input->post('mahasiswa_dosen_wali'));
				$insert['created_by'] = userdata('pengguna_id');
						
				$this->Mahasiswa_model->insert_mahasiswa($insert);

				$this->session->set_flashdata('success','Mahasiswa telah berhasil ditambah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User Mahasiswa telah digunakan.');
				redirect(module_url($this->uri->segment(2)));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['mahasiswa_id']		= validasi_sql($this->uri->segment(4)); 
		$mahasiswa 					= $this->Mahasiswa_model->get_mahasiswa('*', $where);

		$data['mahasiswa_id'] = ($this->input->post('mahasiswa_id'))?$this->input->post('mahasiswa_id'):$mahasiswa->mahasiswa_id;
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$mahasiswa->perguruan_tinggi_id;
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$mahasiswa->program_studi_id;
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):$mahasiswa->jenjang_kode;
		$data['mahasiswa_nim'] = ($this->input->post('mahasiswa_nim'))?$this->input->post('mahasiswa_nim'):$mahasiswa->mahasiswa_nim;
		$data['mahasiswa_nama'] = ($this->input->post('mahasiswa_nama'))?$this->input->post('mahasiswa_nama'):$mahasiswa->mahasiswa_nama;
		$data['mahasiswa_tempat_lahir'] = ($this->input->post('mahasiswa_tempat_lahir'))?$this->input->post('mahasiswa_tempat_lahir'):$mahasiswa->mahasiswa_tempat_lahir;
		$data['mahasiswa_tanggal_lahir'] = ($this->input->post('mahasiswa_tanggal_lahir'))?$this->input->post('mahasiswa_tanggal_lahir'):$mahasiswa->mahasiswa_tanggal_lahir;
		$data['mahasiswa_jenis_kelamin'] = ($this->input->post('mahasiswa_jenis_kelamin'))?$this->input->post('mahasiswa_jenis_kelamin'):$mahasiswa->mahasiswa_jenis_kelamin;
		$data['mahasiswa_tahun_masuk'] = ($this->input->post('mahasiswa_tahun_masuk'))?$this->input->post('mahasiswa_tahun_masuk'):$mahasiswa->mahasiswa_tahun_masuk;
		$data['mahasiswa_semester_awal'] = ($this->input->post('mahasiswa_semester_awal'))?$this->input->post('mahasiswa_semester_awal'):$mahasiswa->mahasiswa_semester_awal;
		$data['mahasiswa_batas_studi'] = ($this->input->post('mahasiswa_batas_studi'))?$this->input->post('mahasiswa_batas_studi'):$mahasiswa->mahasiswa_batas_studi;
		$data['mahasiswa_tanggal_masuk'] = ($this->input->post('mahasiswa_tanggal_masuk'))?$this->input->post('mahasiswa_tanggal_masuk'):$mahasiswa->mahasiswa_tanggal_masuk;
		$data['mahasiswa_tanggal_lulus'] = ($this->input->post('mahasiswa_tanggal_lulus'))?$this->input->post('mahasiswa_tanggal_lulus'):$mahasiswa->mahasiswa_tanggal_lulus;
		$data['mahasiswa_sks_diakui'] = ($this->input->post('mahasiswa_sks_diakui'))?$this->input->post('mahasiswa_sks_diakui'):$mahasiswa->mahasiswa_sks_diakui;
		$data['mahasiswa_pindahan_nim'] = ($this->input->post('mahasiswa_pindahan_nim'))?$this->input->post('mahasiswa_pindahan_nim'):$mahasiswa->mahasiswa_pindahan_nim;
		$data['mahasiswa_pindahan_pt'] = ($this->input->post('mahasiswa_pindahan_pt'))?$this->input->post('mahasiswa_pindahan_pt'):$mahasiswa->mahasiswa_pindahan_pt;
		$data['mahasiswa_pindahan_prodi'] = ($this->input->post('mahasiswa_pindahan_prodi'))?$this->input->post('mahasiswa_pindahan_prodi'):$mahasiswa->mahasiswa_pindahan_prodi;
		$data['mahasiswa_pindahan_jenjang'] = ($this->input->post('mahasiswa_pindahan_jenjang'))?$this->input->post('mahasiswa_pindahan_jenjang'):$mahasiswa->mahasiswa_pindahan_jenjang;
		$data['mahasiswa_user'] = ($this->input->post('mahasiswa_user'))?$this->input->post('mahasiswa_user'):$mahasiswa->mahasiswa_user;
		$data['mahasiswa_pin'] = ($this->input->post('mahasiswa_pin'))?$this->input->post('mahasiswa_pin'):$mahasiswa->mahasiswa_pin;
		$data['mahasiswa_dosen_wali'] = ($this->input->post('mahasiswa_dosen_wali'))?$this->input->post('mahasiswa_dosen_wali'):$mahasiswa->mahasiswa_dosen_wali;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('mahasiswa_user'))."' && pengguna_nama != '".$mahasiswa->mahasiswa_user."'") < 1 && 
				$this->Mahasiswa_model->count_all_mahasiswa("mahasiswa_user = '".validasi_sql($this->input->post('mahasiswa_user'))."' && mahasiswa_user != '".$mahasiswa->mahasiswa_user."'") < 1){
				$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");

				$where_update['mahasiswa_id']	= validasi_sql($this->input->post('mahasiswa_id'));
				$update['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
				$update['program_studi_id'] = validasi_input($this->input->post('program_studi_id'));
				$update['jenjang_kode'] = $get_program_studi->jenjang_kode;
				$update['mahasiswa_nim'] = validasi_input($this->input->post('mahasiswa_nim'));
				$update['mahasiswa_nama'] = validasi_input($this->input->post('mahasiswa_nama'));
				$update['mahasiswa_tempat_lahir'] = validasi_input($this->input->post('mahasiswa_tempat_lahir'));
				$update['mahasiswa_tanggal_lahir'] = validasi_input($this->input->post('mahasiswa_tanggal_lahir'));
				$update['mahasiswa_jenis_kelamin'] = validasi_input($this->input->post('mahasiswa_jenis_kelamin'));
				$update['mahasiswa_tahun_masuk'] = validasi_input($this->input->post('mahasiswa_tahun_masuk'));
				$update['mahasiswa_semester_awal'] = validasi_input($this->input->post('mahasiswa_semester_awal'));
				$update['mahasiswa_batas_studi'] = validasi_input($this->input->post('mahasiswa_batas_studi'));
				$update['mahasiswa_tanggal_masuk'] = validasi_input($this->input->post('mahasiswa_tanggal_masuk'));
				$update['mahasiswa_tanggal_lulus'] = validasi_input($this->input->post('mahasiswa_tanggal_lulus'));
				$update['mahasiswa_sks_diakui'] = validasi_input($this->input->post('mahasiswa_sks_diakui'));
				$update['mahasiswa_pindahan_nim'] = validasi_input($this->input->post('mahasiswa_pindahan_nim'));
				$update['mahasiswa_pindahan_pt'] = validasi_input($this->input->post('mahasiswa_pindahan_pt'));
				$update['mahasiswa_pindahan_prodi'] = validasi_input($this->input->post('mahasiswa_pindahan_prodi'));
				$update['mahasiswa_pindahan_jenjang'] = validasi_input($this->input->post('mahasiswa_pindahan_jenjang'));
				$update['mahasiswa_user'] = validasi_input($this->input->post('mahasiswa_user'));
				$update['mahasiswa_pin'] = validasi_input($this->input->post('mahasiswa_pin'));
				$update['mahasiswa_dosen_wali'] = validasi_input($this->input->post('mahasiswa_dosen_wali'));
				$update['updated_by'] = userdata('pengguna_id');

				$this->Mahasiswa_model->update_mahasiswa($where_update, $update);

				// USER SISWA
				$mahasiswa_id		= validasi_sql($this->input->post('mahasiswa_id'));
				$mahasiswa_nama		= validasi_sql($this->input->post('mahasiswa_nama'));
				$mahasiswa_user		= validasi_sql($this->input->post('mahasiswa_user'));
				$mahasiswa_pin		= validasi_sql($this->input->post('mahasiswa_pin'));

				$where_pengguna = array();
				$where_pengguna['pengguna.pengguna_nama']		= $mahasiswa->mahasiswa_user;
				$where_pengguna['pengguna.pengguna_level_id']	= 11;
				if ($this->Pengguna_model->count_all_pengguna($where_pengguna) > 0){
					$update_pengguna = array();
					$update_pengguna['pengguna_nama_depan']	= $mahasiswa_nama;
					if ($mahasiswa_pin != 'CHANGED' && $mahasiswa_pin != $mahasiswa->mahasiswa_pin && $mahasiswa_pin && strlen($mahasiswa_pin) >= 6){
						$update_pengguna['pengguna_kunci']	= hash_password($mahasiswa_pin);

						$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_id'=>$mahasiswa_id), array('mahasiswa_pin'=>$mahasiswa_pin));
					}
					if ($mahasiswa->mahasiswa_user != $mahasiswa_user){
						$update_pengguna['pengguna_nama']	= $mahasiswa_user;
					}
					$this->Pengguna_model->update_pengguna(array('pengguna_nama'=>$mahasiswa->mahasiswa_user, 'pengguna_level_id'=>11), $update_pengguna);
				}
				
				$this->session->set_flashdata('success','Mahasiswa telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User telah digunakan. Silahkan gunakan user yang lain.');
				redirect(module_url($this->uri->segment(2)));
			}
			
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$mahasiswa_id		= validasi_sql($this->uri->segment(4));
		$mahasiswa_user		= validasi_sql($this->uri->segment(5));
		if ($this->Mahasiswa_model->count_all_mahasiswa_kuliah("mahasiswa_id = '$mahasiswa_id'") < 1){

			$where_delete_mahasiswa['mahasiswa_id']	= $mahasiswa_id;
			$this->Mahasiswa_model->delete_mahasiswa($where_delete_mahasiswa);
			
			$where_delete_pengguna = array();
			$where_delete_pengguna['mahasiswa_id']	= $mahasiswa_id;
			$where_delete_pengguna['pengguna_level_id']	= 11;
			$this->Pengguna_model->delete_pengguna($where_delete_pengguna);
				
			$this->session->set_flashdata('success','Mahasiswa telah berhasil dihapus.');
		} else {
			$this->session->set_flashdata('error','Mahasiswa gagal dihapus.');
		}
		
		redirect(module_url($this->uri->segment(2)));
	}

	public function delete_user()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$mahasiswa_id			= validasi_sql($this->uri->segment(4));
		$mahasiswa_user			= validasi_sql($this->uri->segment(5));

		$where_delete_pengguna = array();
		$where_delete_pengguna['mahasiswa_id']	= $mahasiswa_id;
		$where_delete_pengguna['pengguna_level_id']	= 11;
		$this->Pengguna_model->delete_pengguna($where_delete_pengguna);
			
		$this->session->set_flashdata('success','Mahasiswa telah berhasil dihapus.');
		
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['mahasiswa_id']	= validasi_sql($this->uri->segment(4)); 
		$mahasiswa		 			= $this->Mahasiswa_model->get_mahasiswa('*', $where);
		if (!$mahasiswa){
			redirect(module_url($this->uri->segment(2)));
		}
		
		$data['mahasiswa_id'] = $mahasiswa->mahasiswa_id;
		$data['perguruan_tinggi_id'] = $mahasiswa->perguruan_tinggi_id;
		$data['program_studi_id'] = $mahasiswa->program_studi_id;
		$data['jenjang_kode'] = $mahasiswa->jenjang_kode;
		$data['mahasiswa_nim'] = $mahasiswa->mahasiswa_nim;
		$data['mahasiswa_nama'] = $mahasiswa->mahasiswa_nama;
		$data['mahasiswa_tempat_lahir'] = $mahasiswa->mahasiswa_tempat_lahir;
		$data['mahasiswa_tanggal_lahir'] = $mahasiswa->mahasiswa_tanggal_lahir;
		$data['mahasiswa_jenis_kelamin'] = $mahasiswa->mahasiswa_jenis_kelamin;
		$data['mahasiswa_tahun_masuk'] = $mahasiswa->mahasiswa_tahun_masuk;
		$data['mahasiswa_semester_awal'] = $mahasiswa->mahasiswa_semester_awal;
		$data['mahasiswa_batas_studi'] = $mahasiswa->mahasiswa_batas_studi;
		$data['mahasiswa_tanggal_masuk'] = $mahasiswa->mahasiswa_tanggal_masuk;
		$data['mahasiswa_tanggal_lulus'] = $mahasiswa->mahasiswa_tanggal_lulus;
		$data['mahasiswa_sks_diakui'] = $mahasiswa->mahasiswa_sks_diakui;
		$data['mahasiswa_pindahan_nim'] = $mahasiswa->mahasiswa_pindahan_nim;
		$data['mahasiswa_pindahan_pt'] = $mahasiswa->mahasiswa_pindahan_pt;
		$data['mahasiswa_pindahan_prodi'] = $mahasiswa->mahasiswa_pindahan_prodi;
		$data['mahasiswa_pindahan_jenjang'] = $mahasiswa->mahasiswa_pindahan_jenjang;
		$data['mahasiswa_user'] = $mahasiswa->mahasiswa_user;
		$data['mahasiswa_pin'] = $mahasiswa->mahasiswa_pin;
		$data['mahasiswa_dosen_wali'] = $mahasiswa->mahasiswa_dosen_wali;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function import()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'import';
		
		$data['dataExcel']	= array();		
		$data['filename']	= '';
		
		if ($this->input->post('importMahasiswa')){
			if ($_FILES['userfile']['tmp_name']){
				$ext = end(explode(".", basename($_FILES['userfile']['name'])));
				if ($ext == 'xls'){
					
					$timestamp = explode(" ",microtime());
					$filename = time().str_shuffle('123456ABCDEF');
					
					$uploaddir = './asset/temp_upload/';
					$uploadfname = 'mahasiswa_'.$filename . '.' . end(explode(".", basename($_FILES['userfile']['name'])));
					$uploadfile = $uploaddir . $uploadfname;
					$data['filename'] = $uploadfname;
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					
						$this->load->library('excel_reader');
						$this->excel_reader->setOutputEncoding('CP1251');
						$this->excel_reader->read($uploadfile);
						$dataFile = $this->excel_reader->sheets[0];
						
						$paramsReal = array("NO", "PROGRAM STUDI", "JENJANG", "USERNAME", "PASSWORD", "NAMA", "NIM", "JK", "TEMPAT LAHIR", "TANGGAL LAHIR", "TAHUN MASUK", "SEMESTER MASUK", "BATAS STUDI", "TANGGAL MASUK", "TANGGAL LULUS", "NIM ASAL", "SKS DIAKUI", "PERGURUAN TINGGI", "PROGRAM STUDI", "JENJANG");
						$paramsReal = $iOne = array_combine(range(1, count($paramsReal)), array_values($paramsReal));
						$paramsFile = $dataFile['cells'][3];
						$result 	= array_diff($paramsReal,$paramsFile);
						
						if (count($result) == 0){
							for ($i = 5; $i <= $dataFile['numRows']; $i++) {
								if($dataFile['cells'][$i][1]){
									$data['dataExcel'][$i]['mahasiswa_id'] 					= (isset($dataFile['cells'][$i][1]))?trim($dataFile['cells'][$i][1]):'';
									$data['dataExcel'][$i]['mahasiswa_prodi'] 				= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
									$data['dataExcel'][$i]['mahasiswa_user'] 				= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
									$data['dataExcel'][$i]['mahasiswa_pin'] 				= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
									$data['dataExcel'][$i]['mahasiswa_nama'] 				= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
									$data['dataExcel'][$i]['mahasiswa_nim'] 				= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';
									$data['dataExcel'][$i]['mahasiswa_jenis_kelamin'] 		= (isset($dataFile['cells'][$i][7]))?trim($dataFile['cells'][$i][7]):'';
									$data['dataExcel'][$i]['mahasiswa_tempat_lahir'] 		= (isset($dataFile['cells'][$i][8]))?trim($dataFile['cells'][$i][8]):'';
									$data['dataExcel'][$i]['mahasiswa_tanggal_lahir'] 		= (isset($dataFile['cells'][$i][9]))?trim($dataFile['cells'][$i][9]):'';
									$data['dataExcel'][$i]['mahasiswa_tahun_masuk'] 		= (isset($dataFile['cells'][$i][10]))?trim($dataFile['cells'][$i][10]):'';
									$data['dataExcel'][$i]['mahasiswa_semester_awal'] 		= (isset($dataFile['cells'][$i][11]))?trim($dataFile['cells'][$i][11]):'';
									$data['dataExcel'][$i]['mahasiswa_batas_studi'] 		= (isset($dataFile['cells'][$i][12]))?trim($dataFile['cells'][$i][12]):'';
									$data['dataExcel'][$i]['mahasiswa_tanggal_masuk'] 		= (isset($dataFile['cells'][$i][13]))?trim($dataFile['cells'][$i][13]):'';
									$data['dataExcel'][$i]['mahasiswa_tanggal_lulus'] 		= (isset($dataFile['cells'][$i][14]))?trim($dataFile['cells'][$i][14]):'';
									$data['dataExcel'][$i]['mahasiswa_pindahan_nim'] 		= (isset($dataFile['cells'][$i][15]))?trim($dataFile['cells'][$i][15]):'';
									$data['dataExcel'][$i]['mahasiswa_sks_diakui'] 			= (isset($dataFile['cells'][$i][16]))?trim($dataFile['cells'][$i][16]):'';
									$data['dataExcel'][$i]['mahasiswa_pindahan_pt'] 		= (isset($dataFile['cells'][$i][17]))?trim($dataFile['cells'][$i][17]):'';
									$data['dataExcel'][$i]['mahasiswa_pindahan_prodi'] 		= (isset($dataFile['cells'][$i][18]))?trim($dataFile['cells'][$i][18]):'';
									$data['dataExcel'][$i]['mahasiswa_pindahan_jenjang']	= (isset($dataFile['cells'][$i][19]))?trim($dataFile['cells'][$i][19]):'';
									
									$data['dataExcel'][$i]['data_prodi'] 	= 'PRODI INVALID';
									if ($this->Program_studi_model->count_all_program_studi("(program_studi_kode = '".$data['dataExcel'][$i]['mahasiswa_prodi']."' OR program_studi_nama = '".$data['dataExcel'][$i]['mahasiswa_prodi']."')") > 0){
										$data['dataExcel'][$i]['data_prodi'] 	= 'PRODI VALID';
									}
								}
							}
						} else {
							$this->session->set_flashdata('error','Format file salah.');
							redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
						}
					} else {
						$this->session->set_flashdata('error','File gagal di-upload.');
						redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
					}
				} else {
					$this->session->set_flashdata('error','Format file yang Anda gunakan salah. Silahkan gunakan format file Excel 97-2003 (.xls).');
					redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Silahkan masukkan file yang akan di-import.');
				redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
			}
		}
		
		if ($this->input->post('save')){
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$this->excel_reader->read('./asset/temp_upload/'.$this->input->post('filename'));
			$dataFile = $this->excel_reader->sheets[0];
			
			for ($i = 5; $i <= $dataFile['numRows']; $i++) {
				if($dataFile['cells'][$i][1]){
					$mahasiswa_id 				= (isset($dataFile['cells'][$i][1]))?trim($dataFile['cells'][$i][1]):'';
					$mahasiswa_prodi 			= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
					$mahasiswa_user 			= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
					$mahasiswa_pin 				= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
					$mahasiswa_nama 			= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
					$mahasiswa_nim 				= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';
					$mahasiswa_jenis_kelamin 	= (isset($dataFile['cells'][$i][7]))?trim($dataFile['cells'][$i][7]):'';
					$mahasiswa_tempat_lahir 	= (isset($dataFile['cells'][$i][8]))?trim($dataFile['cells'][$i][8]):'';
					$mahasiswa_tanggal_lahir 	= (isset($dataFile['cells'][$i][9]))?trim($dataFile['cells'][$i][9]):'';
					$mahasiswa_tahun_masuk 		= (isset($dataFile['cells'][$i][10]))?trim($dataFile['cells'][$i][10]):'';
					$mahasiswa_semester_awal 	= (isset($dataFile['cells'][$i][11]))?trim($dataFile['cells'][$i][11]):'';
					$mahasiswa_batas_studi 		= (isset($dataFile['cells'][$i][12]))?trim($dataFile['cells'][$i][12]):'';
					$mahasiswa_tanggal_masuk 	= (isset($dataFile['cells'][$i][13]))?trim($dataFile['cells'][$i][13]):'';
					$mahasiswa_tanggal_lulus 	= (isset($dataFile['cells'][$i][14]))?trim($dataFile['cells'][$i][14]):'';
					$mahasiswa_pindahan_nim 	= (isset($dataFile['cells'][$i][15]))?trim($dataFile['cells'][$i][15]):'';
					$mahasiswa_sks_diakui 		= (isset($dataFile['cells'][$i][16]))?trim($dataFile['cells'][$i][16]):'';
					$mahasiswa_pindahan_pt 		= (isset($dataFile['cells'][$i][17]))?trim($dataFile['cells'][$i][17]):'';
					$mahasiswa_pindahan_prodi 	= (isset($dataFile['cells'][$i][18]))?trim($dataFile['cells'][$i][18]):'';
					$mahasiswa_pindahan_jenjang	= (isset($dataFile['cells'][$i][19]))?trim($dataFile['cells'][$i][19]):'';
					
					$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "(program_studi_kode = '$mahasiswa_prodi' OR program_studi_nama = '$mahasiswa_prodi')");
					
					if ($get_program_studi && $mahasiswa_user){
						if ($this->Mahasiswa_model->count_all_mahasiswa(array("mahasiswa.mahasiswa_user"=>$mahasiswa_user)) < 1){

							$insert['mahasiswa_id'] = $this->uuid->v4();
							$insert['perguruan_tinggi_id'] = validasi_input($get_program_studi->perguruan_tinggi_id);
							$insert['program_studi_id'] = validasi_input($get_program_studi->program_studi_id);
							$insert['jenjang_kode'] = validasi_input($get_program_studi->jenjang_kode);
							$insert['mahasiswa_nim'] = validasi_input($mahasiswa_nim);
							$insert['mahasiswa_nama'] = validasi_input($mahasiswa_nama);
							$insert['mahasiswa_tempat_lahir'] = validasi_input($mahasiswa_tempat_lahir);
							$insert['mahasiswa_tanggal_lahir'] = validasi_input($mahasiswa_tanggal_lahir);
							$insert['mahasiswa_jenis_kelamin'] = validasi_input($mahasiswa_jenis_kelamin);
							$insert['mahasiswa_tahun_masuk'] = validasi_input($mahasiswa_tahun_masuk);
							$insert['mahasiswa_semester_awal'] = validasi_input($mahasiswa_semester_awal);
							$insert['mahasiswa_batas_studi'] = validasi_input($mahasiswa_batas_studi);
							$insert['mahasiswa_tanggal_masuk'] = validasi_input($mahasiswa_tanggal_masuk);
							$insert['mahasiswa_tanggal_lulus'] = validasi_input($mahasiswa_tanggal_lulus);
							$insert['mahasiswa_sks_diakui'] = validasi_input($mahasiswa_sks_diakui);
							$insert['mahasiswa_pindahan_nim'] = validasi_input($mahasiswa_pindahan_nim);
							$insert['mahasiswa_pindahan_pt'] = validasi_input($mahasiswa_pindahan_pt);
							$insert['mahasiswa_pindahan_prodi'] = validasi_input($mahasiswa_pindahan_prodi);
							$insert['mahasiswa_pindahan_jenjang'] = validasi_input($mahasiswa_pindahan_jenjang);
							$insert['mahasiswa_user'] = validasi_input($mahasiswa_user);
							$insert['mahasiswa_pin'] = validasi_input($mahasiswa_pin);
							$insert['created_by'] = userdata('pengguna_id');
									
							$this->Mahasiswa_model->insert_mahasiswa($insert);
						} else if ($this->Mahasiswa_model->count_all_mahasiswa(array("mahasiswa.mahasiswa_user"=>$mahasiswa_user)) > 0){

							$get_mahasiswa = $this->Mahasiswa_model->get_mahasiswa("", array("mahasiswa.mahasiswa_user"=>$mahasiswa_user));

							$update = array();
							
							$where_pengguna = array();
							$where_pengguna['mahasiswa.mahasiswa_id'] = $get_mahasiswa->mahasiswa_id;
							$where_pengguna['pengguna.pengguna_level_id'] = 11;
							if ($this->Pengguna_model->count_all_pengguna($where_pengguna) > 0){
								$update_pengguna = array();
								$update_pengguna['pengguna_nama_depan']	= $mahasiswa_nama;
								if ($get_mahasiswa->mahasiswa_pin != 'CHANGED' && $get_mahasiswa->mahasiswa_pin != $mahasiswa_pin && $mahasiswa_pin && strlen($mahasiswa_pin) >= 6){
									$update_pengguna['pengguna_kunci'] = hash_password($mahasiswa_pin);

									$update['mahasiswa_pin'] = $mahasiswa_pin;
								}
								$this->Pengguna_model->update_pengguna(array('mahasiswa_id'=>$get_mahasiswa->mahasiswa_id, 'pengguna_level_id'=>11), $update_pengguna);
							} else {
								$update['mahasiswa_pin'] = $mahasiswa_pin;
							}

							$update['perguruan_tinggi_id'] = validasi_input($get_program_studi->perguruan_tinggi_id);
							$update['program_studi_id'] = validasi_input($get_program_studi->program_studi_id);
							$update['jenjang_kode'] = validasi_input($get_program_studi->jenjang_kode);
							$update['mahasiswa_nim'] = validasi_input($mahasiswa_nim);
							$update['mahasiswa_nama'] = validasi_input($mahasiswa_nama);
							$update['mahasiswa_tempat_lahir'] = validasi_input($mahasiswa_tempat_lahir);
							$update['mahasiswa_tanggal_lahir'] = validasi_input($mahasiswa_tanggal_lahir);
							$update['mahasiswa_jenis_kelamin'] = validasi_input($mahasiswa_jenis_kelamin);
							$update['mahasiswa_tahun_masuk'] = validasi_input($mahasiswa_tahun_masuk);
							$update['mahasiswa_semester_awal'] = validasi_input($mahasiswa_semester_awal);
							$update['mahasiswa_batas_studi'] = validasi_input($mahasiswa_batas_studi);
							$update['mahasiswa_tanggal_masuk'] = validasi_input($mahasiswa_tanggal_masuk);
							$update['mahasiswa_tanggal_lulus'] = validasi_input($mahasiswa_tanggal_lulus);
							$update['mahasiswa_sks_diakui'] = validasi_input($mahasiswa_sks_diakui);
							$update['mahasiswa_pindahan_nim'] = validasi_input($mahasiswa_pindahan_nim);
							$update['mahasiswa_pindahan_pt'] = validasi_input($mahasiswa_pindahan_pt);
							$update['mahasiswa_pindahan_prodi'] = validasi_input($mahasiswa_pindahan_prodi);
							$update['mahasiswa_pindahan_jenjang'] = validasi_input($mahasiswa_pindahan_jenjang);
							$update['updated_by'] = userdata('pengguna_id');
							$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_user'=>$mahasiswa_user), $update);
						}
					}
				}
			}
			
			$this->session->set_flashdata('success','Mahasiswa telah berhasil di-import.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function generate_account()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		foreach($this->Mahasiswa_model->grid_all_mahasiswa('', 'mahasiswa_id', 'ASC') as $row){
			if ($row->mahasiswa_user){
				if ($this->Pengguna_model->count_all_pengguna(array('pengguna_nama'=>$row->mahasiswa_user, 'pengguna.pengguna_level_id'=>11)) < 1){
					if ($row->mahasiswa_pin && strlen($row->mahasiswa_pin) >= 6){
						$acak 	= $row->mahasiswa_pin;
					} else {
						$alpha 	= "0123456789";
						$acak	= str_shuffle($alpha);
						$acak 	= substr($acak,0,6);
						
						$update_pin = array();
						$update_pin['mahasiswa_pin']					= $acak;
						$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_id'=>$row->mahasiswa_id), $update_pin);
					}
					
					$insert_pengguna 						= array();
					$insert_pengguna['program_studi_id']	= $row->program_studi_id;
					$insert_pengguna['mahasiswa_id']		= $row->mahasiswa_id;
					$insert_pengguna['pengguna_id']			= $this->uuid->v4();
					$insert_pengguna['pengguna_nama']		= $row->mahasiswa_user;
					$insert_pengguna['pengguna_nama_depan']	= $row->mahasiswa_nama;
					$insert_pengguna['pengguna_kunci']		= hash_password($acak);
					$insert_pengguna['pengguna_level_id']	= 11;
					$insert_pengguna['pengguna_terdaftar']	= date("Y-m-d H:i:s");
					$insert_pengguna['pengguna_status']		= 'A';
					$this->Pengguna_model->insert_pengguna($insert_pengguna);
				}
			}
		}
		
		$this->session->set_flashdata('success','User mahasiswa telah berhasil di-generate.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function genpinmahasiswa(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$mahasiswa_id 			= validasi_sql($this->uri->segment(4));
		$mahasiswa_user 		= validasi_sql($this->uri->segment(5));
		if ($mahasiswa_user && $this->Pengguna_model->count_all_pengguna(array('pengguna_nama'=>$mahasiswa_user, 'pengguna.pengguna_level_id'=>11)) > 0){
			$alpha 	= "0123456789";
			$acak	= str_shuffle($alpha);
			$acak 	= substr($acak,0,6);
			
			$update_pengguna['pengguna_kunci']		= hash_password($acak);
			$this->Pengguna_model->update_pengguna(array('pengguna_nama'=>$mahasiswa_user, 'pengguna_level_id'=>11), $update_pengguna);
			
			$update_pin = array();
			$update_pin['mahasiswa_pin']				= $acak;
			$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_id'=>$mahasiswa_id), $update_pin);

			$this->session->set_flashdata('success','Pin mahasiswa telah berhasil di-reset.<br /><strong>Pin Baru : '.$acak.'</strong>');
			redirect(module_url($this->uri->segment(2)));
		} else if ($mahasiswa_user) {
			$where['mahasiswa_id']		= $mahasiswa_id; 
			$row 					= $this->Mahasiswa_model->get_mahasiswa('*', $where);
			if ($row->mahasiswa_pin && strlen($row->mahasiswa_pin) >= 6){
				$acak 	= $row->mahasiswa_pin;
			} else {
				$alpha 	= "0123456789";
				$acak	= str_shuffle($alpha);
				$acak 	= substr($acak,0,6);
				
				$update_pin = array();
				$update_pin['mahasiswa_pin']					= $acak;
				$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_id'=>$row->mahasiswa_id), $update_pin);
			}
			
			$insert_pengguna 						= array();
			$insert_pengguna['program_studi_id']	= $row->program_studi_id;
			$insert_pengguna['mahasiswa_id']		= $row->mahasiswa_id;
			$insert_pengguna['pengguna_id']			= $this->uuid->v4();
			$insert_pengguna['pengguna_nama']		= $row->mahasiswa_user;
			$insert_pengguna['pengguna_nama_depan']	= $row->mahasiswa_nama;
			$insert_pengguna['pengguna_kunci']		= hash_password($acak);
			$insert_pengguna['pengguna_level_id']	= 11;
			$insert_pengguna['pengguna_terdaftar']	= date("Y-m-d H:i:s");
			$insert_pengguna['pengguna_status']		= 'A';
			$this->Pengguna_model->insert_pengguna($insert_pengguna);

			$this->session->set_flashdata('success','Pin mahasiswa telah berhasil dibuat.<br /><strong>Pin Anda : '.$acak.'</strong>');
			redirect(module_url($this->uri->segment(2)));
		} else {
			$this->session->set_flashdata('error','Pin mahasiswa gagal dibuat.');
			redirect(module_url($this->uri->segment(2)));
		}
		
	}
	
	public function pin_mahasiswa()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['mahasiswa']	= $this->Mahasiswa_model->grid_all_mahasiswa('*', 'mahasiswa_user', 'ASC');
		$this->load->view(module_dir().'/export/list_user_mahasiswa', $data);
	}

	public function set_mahasiswa()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['mahasiswa']	= $this->Mahasiswa_model->grid_all_mahasiswa('mahasiswa_nama, mahasiswa_user, mahasiswa_id', 'mahasiswa_user', 'ASC');
		foreach ($data['mahasiswa'] as $row) {
			$update_mahasiswa = array();
			$update_mahasiswa['mahasiswa_nama']	= strtoupper($row->mahasiswa_nama);
			$this->Mahasiswa_model->update_mahasiswa(array('mahasiswa_user'=>$row->mahasiswa_user), $update_mahasiswa);
		}
	}
	
	public function check_user(){
		$data = array();
		$action = $this->input->post('action');
		$user = $this->input->post('user');
		$id = $this->input->post('id');
		if ($action == 'add' && $user){
			$mahasiswa = $this->Mahasiswa_model->get_mahasiswa("mahasiswa_id", array('mahasiswa_user'=>$user));
			if (!$mahasiswa){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $mahasiswa;
			} else {
				$data['response']	= false;
				$data['message']	= "User telah digunakan ada.";
			}
		} else if ($action == 'edit' && $user && $id){
			$mahasiswa = $this->Mahasiswa_model->get_mahasiswa("mahasiswa_id", array('mahasiswa_user'=>$user, 'mahasiswa_id !='=>$id));
			if (!$mahasiswa){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $mahasiswa;
			} else {
				$data['response']	= false;
				$data['message']	= "User telah digunakan ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function get_semester(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun("*", array('tahun_kode'=>$tahun_kode));
			if ($tahun){
				$semester = $this->Semester_model->grid_all_semester("semester.semester_kode, semester.semester_nama", "semester_nama", "ASC", 0, 0, array('semester.tahun_kode'=>$tahun_kode));
				if ($semester){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $semester;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}
