<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tingkat extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Tingkat | ' . profile('profil_website');
		$this->active_menu		= 256;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Tingkat_model');
    }
	
	function datatable()
	{
		$departemen_id 	= validasi_sql($this->uri->segment(4));
		$where 			= "departemen.departemen_id = '{$departemen_id}'";

		$this->load->library('Datatables');
		$this->datatables->select('tingkat_id, tingkat_kode, tingkat_nama, tingkat_romawi, tingkat_akhir')
		->add_column('Actions', $this->get_buttons('$1'),'tingkat_id')
		->search_column('tingkat_nama')
		->from('tingkat')
		->join('departemen', 'tingkat.departemen_id=departemen.departemen_id', 'left')
		->where($where);
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		if (check_permission("W")){
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		}
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= $this->uri->segment(4);
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tingkat', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$departemen_id 				= $this->uri->segment(4);
		$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		$data['tingkat_kode']		= ($this->input->post('tingkat_kode'))?$this->input->post('tingkat_kode'):'';
		$data['tingkat_nama']		= ($this->input->post('tingkat_nama'))?$this->input->post('tingkat_nama'):'';
		$data['tingkat_romawi']		= ($this->input->post('tingkat_romawi'))?$this->input->post('tingkat_romawi'):'';
		$data['tingkat_nama']		= ($this->input->post('tingkat_nama'))?$this->input->post('tingkat_nama'):'';
		$data['tingkat_akhir']		= ($this->input->post('tingkat_akhir'))?$this->input->post('tingkat_akhir'):'N';
		$save						= $this->input->post('save');
		if ($save == 'save'){			
			$insert['departemen_id']	= validasi_sql($this->input->post('departemen_id'));
			$insert['tingkat_kode']		= validasi_sql($this->input->post('tingkat_kode'));
			$insert['tingkat_nama']		= validasi_sql($this->input->post('tingkat_nama'));
			$insert['tingkat_romawi']	= validasi_sql($this->input->post('tingkat_romawi'));
			$insert['tingkat_nama']		= validasi_sql($this->input->post('tingkat_nama'));
			$insert['tingkat_akhir']	= validasi_sql($this->input->post('tingkat_akhir'));
			$this->Tingkat_model->insert_tingkat($insert);
			
			$this->session->set_flashdata('success','Tingkat telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id']));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tingkat', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['tingkat_id']		= validasi_sql($this->uri->segment(4)); 
		$tingkat 					= $this->Tingkat_model->get_tingkat('*', $where);

		$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$tingkat->departemen_id;
		$data['tingkat_id']			= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$tingkat->tingkat_id;
		$data['tingkat_kode']		= ($this->input->post('tingkat_kode'))?$this->input->post('tingkat_kode'):$tingkat->tingkat_kode;
		$data['tingkat_nama']		= ($this->input->post('tingkat_nama'))?$this->input->post('tingkat_nama'):$tingkat->tingkat_nama;
		$data['tingkat_romawi']		= ($this->input->post('tingkat_romawi'))?$this->input->post('tingkat_romawi'):$tingkat->tingkat_romawi;
		$data['tingkat_akhir']		= ($this->input->post('tingkat_akhir'))?$this->input->post('tingkat_akhir'):$tingkat->tingkat_akhir;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['tingkat_id']	= validasi_sql($this->input->post('tingkat_id'));
			$edit['departemen_id']		= validasi_sql($this->input->post('departemen_id'));
			$edit['tingkat_kode']		= validasi_sql($this->input->post('tingkat_kode'));
			$edit['tingkat_nama']		= validasi_sql($this->input->post('tingkat_nama'));
			$edit['tingkat_romawi']		= validasi_sql($this->input->post('tingkat_romawi'));
			$edit['tingkat_akhir']		= validasi_sql($this->input->post('tingkat_akhir'));
			$this->Tingkat_model->update_tingkat($where_edit, $edit);
			
			$this->session->set_flashdata('success','Tingkat telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id']));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tingkat', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['tingkat_id']	= validasi_sql($this->uri->segment(4));
		$this->Tingkat_model->delete_tingkat($where_delete);
		
		$this->session->set_flashdata('success','Tingkat telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['tingkat_id']	= validasi_sql($this->uri->segment(4)); 
		$tingkat		 			= $this->Tingkat_model->get_tingkat('*', $where);
		
		$data['tingkat_id']			= $tingkat->tingkat_id;
		$data['tingkat_kode']		= $tingkat->tingkat_kode;
		$data['tingkat_nama']		= $tingkat->tingkat_nama;
		$data['tingkat_romawi']		= $tingkat->tingkat_romawi;
		$data['tingkat_akhir']		= $tingkat->tingkat_akhir;
		$data['departemen_id']		= $tingkat->departemen_id;
		$data['departemen_nama']	= $tingkat->departemen_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/tingkat', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
