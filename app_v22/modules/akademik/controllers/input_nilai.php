<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Input_nilai extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Input Nilai | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Referensi_model');
		$this->load->model('Matakuliah_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Dosen_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Bobot_nilai_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
	}
	
	public function datatable()
    {
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$where = "matakuliah.program_studi_id = '$program_studi_id'";

		$semester_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$where .= " AND matakuliah.semester_kode = '$semester_kode'";

		$this->Datatable_model->set_table("(SELECT matakuliah.*, dosen_nama, (SELECT COUNT(akd_mahasiswa_nilai.mahasiswa_nilai_id) FROM akd_mahasiswa_nilai WHERE akd_mahasiswa_nilai.matakuliah_id=matakuliah.matakuliah_id) AS matakuliah_peserta FROM akd_matakuliah matakuliah LEFT JOIN akd_dosen dosen ON matakuliah.dosen_id=dosen.dosen_id WHERE $where) matakuliah");
		$this->Datatable_model->set_column_order(array('matakuliah_kode', 'matakuliah_nama', 'matakuliah_sks', 'matakuliah_semester_no', 'dosen_nama', null, null));
		$this->Datatable_model->set_column_search(array('matakuliah_kode', 'matakuliah_nama', 'matakuliah_sks', 'matakuliah_semester_no', 'dosen_nama'));
		$this->Datatable_model->set_order(array('matakuliah_kode', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
			$row = array();
            $row['nomor'] = $no;
            $row['matakuliah_id'] = $record->matakuliah_id;
            $row['matakuliah_nama'] = $record->matakuliah_nama;
            $row['matakuliah_kode'] = $record->matakuliah_kode;
            $row['matakuliah_sks'] = $record->matakuliah_sks;
            $row['matakuliah_semester_no'] = $record->matakuliah_semester_no;
			$row['dosen_nama'] = $record->dosen_nama;
			$row['matakuliah_peserta'] = $record->matakuliah_peserta;
            $row['Actions'] = $this->get_buttons($record->matakuliah_id, $record->semester_kode);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id, $kode)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id.'/'.$kode) .'" class="btn btn-success btn-sm" title="Detail"><i class="fa fa-plus"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$semester = $this->Semester_model->get_semester_aktif();
		$semester_kode					= ($this->uri->segment(5))?$this->uri->segment(5):($semester)?$semester->semester_kode:'';
		$data['semester_kode']			= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/input_nilai', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['matakuliah_id']	= validasi_sql($this->uri->segment(4)); 
		$data['matakuliah']		= $this->Matakuliah_model->get_matakuliah2('*', $where);
		if (!$data['matakuliah']){
			redirect(module_url($this->uri->segment(2)));
		}

		$save = $this->input->post('save');
		if ($save){
			$nilai = $this->input->post('nilai');
			if ($nilai){
				foreach ($nilai as $key => $value) {
					$get_bobot_nilai = $this->Bobot_nilai_model->get_bobot_nilai('', array('bobot_nilai_huruf'=>$value, 'bobot_nilai.semester_kode'=>$data['matakuliah']->semester_kode, 'bobot_nilai.program_studi_id'=>$data['matakuliah']->program_studi_id));
					if ($get_bobot_nilai){
						$update_nilai = array();
						$update_nilai['mahasiswa_nilai_huruf'] = $get_bobot_nilai->bobot_nilai_huruf;
						$update_nilai['mahasiswa_nilai_value'] = $get_bobot_nilai->bobot_nilai_value;
					} else {
						$update_nilai = array();
						$update_nilai['mahasiswa_nilai_huruf'] = null;
						$update_nilai['mahasiswa_nilai_value'] = null;
					}
					$this->Mahasiswa_model->update_mahasiswa_nilai(array('mahasiswa_nilai_id'=>$key), $update_nilai);
				}
			
				$this->session->set_flashdata('success','Nilai telah berhasil di-input.');
				redirect(module_url($this->uri->segment(2).'/detail/'.$data['matakuliah']->matakuliah_id));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/input_nilai', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
