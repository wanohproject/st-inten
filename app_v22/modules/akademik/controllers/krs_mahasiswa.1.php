<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Krs_mahasiswa extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'KRS Mahasiswa | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Datatable_model');
		$this->load->model('Referensi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Matakuliah_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$where = "(mahasiswa.mahasiswa_tanggal_lulus IS NULL OR mahasiswa.mahasiswa_tanggal_lulus = '0000-00-00') AND mahasiswa.program_studi_id = '$program_studi_id'";

		$tahun_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		if ($tahun_kode != '-'){
			$where .= " AND mahasiswa_tahun_masuk = '$tahun_kode'";
		}

		$this->load->library('Datatables');
		$this->datatables->select('mahasiswa_id, mahasiswa_nama, mahasiswa_nim, mahasiswa_user, mahasiswa_tahun_masuk')
		->add_column('Actions', $this->get_buttons('$1', '$2'),'mahasiswa_id, mahasiswa_user')
		->search_column('mahasiswa_nama, mahasiswa_nim, mahasiswa_user, mahasiswa_tahun_masuk')
		->from('akd_mahasiswa mahasiswa')
		->join("akd_tahun tahun", "mahasiswa.mahasiswa_tahun_masuk=tahun.tahun_kode", "left")
		->where($where);
        echo $this->datatables->generate();
	}
	
	function get_buttons($id, $nis)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-success btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-plus"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/krs_mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['mahasiswa_id']	= validasi_sql($this->uri->segment(4)); 
		$mahasiswa		 			= $this->Mahasiswa_model->get_mahasiswa('*', $where);
		if (!$mahasiswa){
			redirect(module_url($this->uri->segment(2)));
		}

		$tahun = $this->Tahun_model->get_tahun_aktif();
		$semester = $this->Semester_model->get_semester_aktif();
		
		$data['semester_kode'] = $semester->semester_kode;
		$data['semester_nama'] = $semester->semester_nama;

		$data['mahasiswa_id'] = $mahasiswa->mahasiswa_id;
		$data['perguruan_tinggi_id'] = $mahasiswa->perguruan_tinggi_id;
		$data['program_studi_id'] = $mahasiswa->program_studi_id;
		$data['program_studi_nama'] = $mahasiswa->program_studi_nama;
		$data['jenjang_kode'] = $mahasiswa->jenjang_kode;
		$data['mahasiswa_nim'] = $mahasiswa->mahasiswa_nim;
		$data['mahasiswa_nama'] = $mahasiswa->mahasiswa_nama;
		$data['mahasiswa_tempat_lahir'] = $mahasiswa->mahasiswa_tempat_lahir;
		$data['mahasiswa_tanggal_lahir'] = $mahasiswa->mahasiswa_tanggal_lahir;
		$data['mahasiswa_jenis_kelamin'] = $mahasiswa->mahasiswa_jenis_kelamin;
		$data['mahasiswa_tahun_masuk'] = $mahasiswa->mahasiswa_tahun_masuk;
		$data['mahasiswa_semester_awal'] = $mahasiswa->mahasiswa_semester_awal;
		$data['mahasiswa_batas_studi'] = $mahasiswa->mahasiswa_batas_studi;
		$data['mahasiswa_tanggal_masuk'] = $mahasiswa->mahasiswa_tanggal_masuk;
		$data['mahasiswa_tanggal_lulus'] = $mahasiswa->mahasiswa_tanggal_lulus;
		$data['mahasiswa_sks_diakui'] = $mahasiswa->mahasiswa_sks_diakui;
		$data['mahasiswa_pindahan_nim'] = $mahasiswa->mahasiswa_pindahan_nim;
		$data['mahasiswa_pindahan_pt'] = $mahasiswa->mahasiswa_pindahan_pt;
		$data['mahasiswa_pindahan_prodi'] = $mahasiswa->mahasiswa_pindahan_prodi;
		$data['mahasiswa_pindahan_jenjang'] = $mahasiswa->mahasiswa_pindahan_jenjang;
		$data['mahasiswa_user'] = $mahasiswa->mahasiswa_user;
		$data['mahasiswa_pin'] = $mahasiswa->mahasiswa_pin;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/krs_mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	// aktivitas
	public function datatable_krs()
    {
		$mahasiswa_id = $this->uri->segment(4);
		$mahasiswa = $this->Mahasiswa_model->get_mahasiswa('*', array('mahasiswa_id'=>$mahasiswa_id));
		$tahun = $this->Tahun_model->get_tahun_aktif();
		$semester = $this->Semester_model->get_semester_aktif();

		$program_studi_id = $mahasiswa->program_studi_id;
		$perguruan_tinggi_id = $mahasiswa->perguruan_tinggi_id;
		$jenjang_kode = $mahasiswa->jenjang_kode;
		$semester_kode = $semester->semester_kode;

		$this->Datatable_model->set_table("(SELECT akd_matakuliah.*, akd_mahasiswa_nilai.mahasiswa_nilai_id FROM akd_mahasiswa_nilai LEFT JOIN akd_matakuliah ON akd_mahasiswa_nilai.matakuliah_id=akd_matakuliah.matakuliah_id WHERE akd_mahasiswa_nilai.program_studi_id = '$program_studi_id' AND akd_mahasiswa_nilai.semester_kode = '$semester_kode' AND akd_mahasiswa_nilai.mahasiswa_id = '$mahasiswa_id') mahasiswa_nilai");
		$this->Datatable_model->set_column_order(array('matakuliah_kode', 'matakuliah_nama', 'matakuliah_sks', null));
		$this->Datatable_model->set_column_search(array('matakuliah_kode', 'matakuliah_nama', 'matakuliah_sks'));
		$this->Datatable_model->set_order(array('matakuliah_kode', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['matakuliah_id'] = $record->matakuliah_id;
            $row['matakuliah_kode'] = $record->matakuliah_kode;
            $row['matakuliah_nama'] = $record->matakuliah_nama;
            $row['matakuliah_sks'] = $record->matakuliah_sks;
			
			$html  = '<div class="text-center">';
			if (check_permission("W")){
				$html .= '<a href="'. site_url($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/delete_krs/'.$record->mahasiswa_nilai_id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
			}
			$html .= '</div>';
			
            $row['Actions'] = $html;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function add_matakuliah()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add_matakuliah';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['mahasiswa_id']	= validasi_sql($this->uri->segment(4)); 
		$mahasiswa		 			= $this->Mahasiswa_model->get_mahasiswa('*', $where);
		if (!$mahasiswa){
			redirect(module_url($this->uri->segment(2)));
		}

		$tahun = $this->Tahun_model->get_tahun_aktif();
		$semester = $this->Semester_model->get_semester_aktif();
		
		$data['semester_nama'] = $semester->semester_nama;

		$data['mahasiswa_id'] = $mahasiswa->mahasiswa_id;
		$data['perguruan_tinggi_id'] = $mahasiswa->perguruan_tinggi_id;
		$data['program_studi_id'] = $mahasiswa->program_studi_id;
		$data['program_studi_nama'] = $mahasiswa->program_studi_nama;
		$data['jenjang_kode'] = $mahasiswa->jenjang_kode;
		$data['mahasiswa_nim'] = $mahasiswa->mahasiswa_nim;
		$data['mahasiswa_nama'] = $mahasiswa->mahasiswa_nama;
		$data['mahasiswa_tempat_lahir'] = $mahasiswa->mahasiswa_tempat_lahir;
		$data['mahasiswa_tanggal_lahir'] = $mahasiswa->mahasiswa_tanggal_lahir;
		$data['mahasiswa_jenis_kelamin'] = $mahasiswa->mahasiswa_jenis_kelamin;
		$data['mahasiswa_tahun_masuk'] = $mahasiswa->mahasiswa_tahun_masuk;
		$data['mahasiswa_semester_awal'] = $mahasiswa->mahasiswa_semester_awal;
		$data['mahasiswa_batas_studi'] = $mahasiswa->mahasiswa_batas_studi;
		$data['mahasiswa_tanggal_masuk'] = $mahasiswa->mahasiswa_tanggal_masuk;
		$data['mahasiswa_tanggal_lulus'] = $mahasiswa->mahasiswa_tanggal_lulus;
		$data['mahasiswa_sks_diakui'] = $mahasiswa->mahasiswa_sks_diakui;
		$data['mahasiswa_pindahan_nim'] = $mahasiswa->mahasiswa_pindahan_nim;
		$data['mahasiswa_pindahan_pt'] = $mahasiswa->mahasiswa_pindahan_pt;
		$data['mahasiswa_pindahan_prodi'] = $mahasiswa->mahasiswa_pindahan_prodi;
		$data['mahasiswa_pindahan_jenjang'] = $mahasiswa->mahasiswa_pindahan_jenjang;
		$data['mahasiswa_user'] = $mahasiswa->mahasiswa_user;
		$data['mahasiswa_pin'] = $mahasiswa->mahasiswa_pin;

		$data['semester_kode'] = $semester->semester_kode;

		$program_studi_id = $mahasiswa->program_studi_id;
		$perguruan_tinggi_id = $mahasiswa->perguruan_tinggi_id;
		$jenjang_kode = $mahasiswa->jenjang_kode;
		$semester_kode = $semester->semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/krs_mahasiswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function save_krs(){
		$data = array();
		$action = $this->input->post('action');
		$matakuliah = $this->input->post('matakuliah');
		$mahasiswa = $this->input->post('mahasiswa');
		$matakuliah = $this->Matakuliah_model->get_matakuliah("*", array('matakuliah_id'=>$matakuliah));

		if ($action == 'add' && $matakuliah){
			$insert['mahasiswa_nilai_id'] = $this->uuid->v4();
			$insert['perguruan_tinggi_id'] = $matakuliah->perguruan_tinggi_id;
			$insert['program_studi_id'] = $matakuliah->program_studi_id;
			$insert['jenjang_kode'] = $matakuliah->jenjang_kode;
			$insert['semester_kode'] = $matakuliah->semester_kode;
			$insert['matakuliah_id'] = $matakuliah->matakuliah_id;
			$insert['mahasiswa_id'] = $mahasiswa;
			$insert['created_by'] = userdata('pengguna_id');
			$mahasiswa = $this->Mahasiswa_model->insert_mahasiswa_nilai($insert);

			if ($mahasiswa){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['status']		= "add";
				$data['data']		= $mahasiswa;
			} else {
				$data['response']	= false;
				$data['status']		= "add";
				$data['message']	= "User telah digunakan ada.";
			}
		} else if ($action == 'delete' && $matakuliah){
			$where['perguruan_tinggi_id'] = $matakuliah->perguruan_tinggi_id;
			$where['program_studi_id'] = $matakuliah->program_studi_id;
			$where['jenjang_kode'] = $matakuliah->jenjang_kode;
			$where['semester_kode'] = $matakuliah->semester_kode;
			$where['matakuliah_id'] = $matakuliah->matakuliah_id;
			$where['mahasiswa_id'] = $mahasiswa;
			$mahasiswa = $this->Mahasiswa_model->delete_mahasiswa_nilai($where);

			if ($mahasiswa){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['status']		= "delete";
				$data['data']		= $mahasiswa;
			} else {
				$data['response']	= false;
				$data['status']		= "delete";
				$data['message']	= "User telah digunakan ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function delete_krs()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$mahasiswa_nilai_id = $this->uri->segment(4);
		$mahasiswa_nilai = $this->Mahasiswa_model->get_mahasiswa_nilai('mahasiswa_nilai.*', array('mahasiswa_nilai.mahasiswa_nilai_id'=>$mahasiswa_nilai_id));

		$this->Mahasiswa_model->delete_mahasiswa_nilai(array('mahasiswa_nilai_id'=>validasi_sql($mahasiswa_nilai_id)));
		
		$this->session->set_flashdata('success','Data telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2).'/detail/'.$mahasiswa_nilai->mahasiswa_id.'?tabs=1'));
	}
}
