<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Dosen | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Datatable_model');
		$this->load->model('Referensi_model');
		$this->load->model('Dosen_model');
		$this->load->model('Dosen_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Matakuliah_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$program_studi_id 	= ($this->uri->segment(4))?$this->uri->segment(4):'';
		$where = "dosen_id IS NOT NULL";
		if ($program_studi_id != '-'){
			$where .= " AND dosen.program_studi_id = '$program_studi_id'";
		}

		$status_aktivitas_kode = ($this->uri->segment(5))?$this->uri->segment(5):'-';
		if ($status_aktivitas_kode != '-'){
			$where .= " AND dosen.status_aktivitas_kode = '$status_aktivitas_kode'";
		}

		$this->load->library('Datatables');
		$this->datatables->select('dosen_id, dosen_nama, dosen_nidn, dosen_user, dosen_pin, status_aktivitas_nama, program_studi_nama')
		->add_column('Actions', $this->get_buttons('$1', '$2'),'dosen_id, dosen_user')
		->add_column('dosen_pin', $this->get_genpindosen('$1', '$2', '$3'),'dosen_id, dosen_pin, dosen_user')
		->search_column('program_studi_nama, dosen_nama, dosen_nidn, dosen_user, dosen_pin, dosen_tahun_masuk')
		->from('(SELECT akd_dosen.*, program_studi_nama FROM akd_dosen LEFT JOIN akd_program_studi ON akd_dosen.program_studi_id=akd_program_studi.program_studi_id) dosen')
        ->join("(SELECT ref_kode.kode_value as status_aktivitas_kode, ref_kode.kode_nama as status_aktivitas_nama FROM ref_kode LEFT JOIN ref_jenis ON ref_kode.jenis_id=ref_jenis.jenis_id WHERE ref_jenis.jenis_value = '15') status_aktivitas", "dosen.status_aktivitas_kode=status_aktivitas.status_aktivitas_kode", "left")
		->where($where);
        echo $this->datatables->generate();
    }
	
	function get_genpindosen($id, $pin, $nis){
		$ci= & get_instance();
		$ci->load->helper('url');
		$html = $pin . ' <a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/genpindosen/'.$id.'/'.$nis) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan mereset pin dosen ini.\');" title="Reload"><i class="fa fa-refresh"></i></a>';
		return $html;
	}
	
	function get_genpinortu($id, $pin, $nis){
		$ci= & get_instance();
		$ci->load->helper('url');
		$html = $pin . ' <a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/genpinortu/'.$id.'/'.$nis) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan mereset pin orang tua dosen ini.\');" title="Reload"><i class="fa fa-refresh"></i></a>';
		return $html;
	}
	
	function get_buttons($id, $nis)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete-user/'.$id.'/'.$nis) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus user dosen ini.\');" title="Hapus User"><i class="fa fa-remove"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id.'/'.$nis) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$status_aktivitas_kode				= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['status_aktivitas_kode']		= ($this->input->post('status_aktivitas_kode'))?$this->input->post('status_aktivitas_kode'):$status_aktivitas_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/dosen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['dosen_id'] = ($this->input->post('dosen_id'))?$this->input->post('dosen_id'):'';
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):'';
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):'';
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):'';
		$data['dosen_ktp'] = ($this->input->post('dosen_ktp'))?$this->input->post('dosen_ktp'):'';
		$data['dosen_nidn'] = ($this->input->post('dosen_nidn'))?$this->input->post('dosen_nidn'):'';
		$data['dosen_nama'] = ($this->input->post('dosen_nama'))?$this->input->post('dosen_nama'):'';
		$data['dosen_gelar'] = ($this->input->post('dosen_gelar'))?$this->input->post('dosen_gelar'):'';
		$data['dosen_tempat_lahir'] = ($this->input->post('dosen_tempat_lahir'))?$this->input->post('dosen_tempat_lahir'):'';
		$data['dosen_tanggal_lahir'] = ($this->input->post('dosen_tanggal_lahir'))?$this->input->post('dosen_tanggal_lahir'):'';
		$data['dosen_jenis_kelamin'] = ($this->input->post('dosen_jenis_kelamin'))?$this->input->post('dosen_jenis_kelamin'):'';
		$data['dosen_semester_mulai'] = ($this->input->post('dosen_semester_mulai'))?$this->input->post('dosen_semester_mulai'):'';
		$data['dosen_nip_pns'] = ($this->input->post('dosen_nip_pns'))?$this->input->post('dosen_nip_pns'):'';
		$data['dosen_homebase'] = ($this->input->post('dosen_homebase'))?$this->input->post('dosen_homebase'):'';
		$data['jabatan_akademik_kode'] = ($this->input->post('jabatan_akademik_kode'))?$this->input->post('jabatan_akademik_kode'):'';
		$data['pendidikan_kode'] = ($this->input->post('pendidikan_kode'))?$this->input->post('pendidikan_kode'):'';
		$data['status_ikatan_kerja_kode'] = ($this->input->post('status_ikatan_kerja_kode'))?$this->input->post('status_ikatan_kerja_kode'):'';
		$data['status_aktivitas_kode'] = ($this->input->post('status_aktivitas_kode'))?$this->input->post('status_aktivitas_kode'):'';
		$data['dosen_user'] = ($this->input->post('dosen_user'))?$this->input->post('dosen_user'):'';
		$data['dosen_pin'] = ($this->input->post('dosen_pin'))?$this->input->post('dosen_pin'):'';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('dosen_user'))."'") < 1 && 
				$this->Dosen_model->count_all_dosen("dosen_user = '".validasi_sql($this->input->post('dosen_user'))."'") < 1){

				$insert = array();
				if ($this->input->post('program_studi_id')){
					$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");
					$insert['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
					$insert['program_studi_id'] = $get_program_studi->program_studi_id;
					$insert['jenjang_kode'] = $get_program_studi->jenjang_kode;
				} else {
					$get_perguruan_tinggi	= $this->Perguruan_tinggi_model->get_perguruan_tinggi_aktif();
					$insert['perguruan_tinggi_id'] = ($get_perguruan_tinggi)?$get_perguruan_tinggi->perguruan_tinggi_id:null;
					$insert['program_studi_id'] = null;
					$insert['jenjang_kode'] = null;
				}
				
				$insert['dosen_id'] = $this->uuid->v4();
				$insert['dosen_ktp'] = validasi_input($this->input->post('dosen_ktp'));
				$insert['dosen_nidn'] = validasi_input($this->input->post('dosen_nidn'));
				$insert['dosen_nama'] = validasi_input($this->input->post('dosen_nama'));
				$insert['dosen_gelar'] = validasi_input($this->input->post('dosen_gelar'));
				$insert['dosen_tempat_lahir'] = validasi_input($this->input->post('dosen_tempat_lahir'));
				$insert['dosen_tanggal_lahir'] = validasi_input($this->input->post('dosen_tanggal_lahir'));
				$insert['dosen_jenis_kelamin'] = validasi_input($this->input->post('dosen_jenis_kelamin'));
				$insert['dosen_semester_mulai'] = validasi_input($this->input->post('dosen_semester_mulai'));
				$insert['dosen_nip_pns'] = validasi_input($this->input->post('dosen_nip_pns'));
				$insert['dosen_homebase'] = validasi_input($this->input->post('dosen_homebase'));
				$insert['jabatan_akademik_kode'] = validasi_input($this->input->post('jabatan_akademik_kode'));
				$insert['pendidikan_kode'] = validasi_input($this->input->post('pendidikan_kode'));
				$insert['status_ikatan_kerja_kode'] = validasi_input($this->input->post('status_ikatan_kerja_kode'));
				$insert['status_aktivitas_kode'] = validasi_input($this->input->post('status_aktivitas_kode'));
				$insert['dosen_user'] = validasi_input($this->input->post('dosen_user'));
				$insert['dosen_pin'] = validasi_input($this->input->post('dosen_pin'));
				$insert['created_by'] = userdata('pengguna_id');
						
				$this->Dosen_model->insert_dosen($insert);

				$this->session->set_flashdata('success','Dosen telah berhasil ditambah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User Dosen telah digunakan.');
				redirect(module_url($this->uri->segment(2)));
			}
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/dosen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['dosen_id']		= validasi_sql($this->uri->segment(4)); 
		$dosen 					= $this->Dosen_model->get_dosen('*', $where);

		$data['dosen_id'] = ($this->input->post('dosen_id'))?$this->input->post('dosen_id'):$dosen->dosen_id;
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$dosen->perguruan_tinggi_id;
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$dosen->program_studi_id;
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):$dosen->jenjang_kode;
		$data['dosen_ktp'] = ($this->input->post('dosen_ktp'))?$this->input->post('dosen_ktp'):$dosen->dosen_ktp;
		$data['dosen_nidn'] = ($this->input->post('dosen_nidn'))?$this->input->post('dosen_nidn'):$dosen->dosen_nidn;
		$data['dosen_nama'] = ($this->input->post('dosen_nama'))?$this->input->post('dosen_nama'):$dosen->dosen_nama;
		$data['dosen_gelar'] = ($this->input->post('dosen_gelar'))?$this->input->post('dosen_gelar'):$dosen->dosen_gelar;
		$data['dosen_tempat_lahir'] = ($this->input->post('dosen_tempat_lahir'))?$this->input->post('dosen_tempat_lahir'):$dosen->dosen_tempat_lahir;
		$data['dosen_tanggal_lahir'] = ($this->input->post('dosen_tanggal_lahir'))?$this->input->post('dosen_tanggal_lahir'):$dosen->dosen_tanggal_lahir;
		$data['dosen_jenis_kelamin'] = ($this->input->post('dosen_jenis_kelamin'))?$this->input->post('dosen_jenis_kelamin'):$dosen->dosen_jenis_kelamin;
		$data['dosen_semester_mulai'] = ($this->input->post('dosen_semester_mulai'))?$this->input->post('dosen_semester_mulai'):$dosen->dosen_semester_mulai;
		$data['dosen_nip_pns'] = ($this->input->post('dosen_nip_pns'))?$this->input->post('dosen_nip_pns'):$dosen->dosen_nip_pns;
		$data['dosen_homebase'] = ($this->input->post('dosen_homebase'))?$this->input->post('dosen_homebase'):$dosen->dosen_homebase;
		$data['jabatan_akademik_kode'] = ($this->input->post('jabatan_akademik_kode'))?$this->input->post('jabatan_akademik_kode'):$dosen->jabatan_akademik_kode;
		$data['pendidikan_kode'] = ($this->input->post('pendidikan_kode'))?$this->input->post('pendidikan_kode'):$dosen->pendidikan_kode;
		$data['status_ikatan_kerja_kode'] = ($this->input->post('status_ikatan_kerja_kode'))?$this->input->post('status_ikatan_kerja_kode'):$dosen->status_ikatan_kerja_kode;
		$data['status_aktivitas_kode'] = ($this->input->post('status_aktivitas_kode'))?$this->input->post('status_aktivitas_kode'):$dosen->status_aktivitas_kode;
		$data['dosen_user'] = ($this->input->post('dosen_user'))?$this->input->post('dosen_user'):$dosen->dosen_user;
		$data['dosen_pin'] = ($this->input->post('dosen_pin'))?$this->input->post('dosen_pin'):$dosen->dosen_pin;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('dosen_user'))."' && pengguna_nama != '".$dosen->dosen_user."'") < 1 && 
				$this->Dosen_model->count_all_dosen("dosen_user = '".validasi_sql($this->input->post('dosen_user'))."' && dosen_user != '".$dosen->dosen_user."'") < 1){
				
				$where_update['dosen_id']	= validasi_sql($this->input->post('dosen_id'));
				$update = array();
				if ($this->input->post('program_studi_id')){
					$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "program_studi_id = '".$this->input->post('program_studi_id')."'");
					$update['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
					$update['program_studi_id'] = $get_program_studi->program_studi_id;
					$update['jenjang_kode'] = $get_program_studi->jenjang_kode;
				} else {
					$get_perguruan_tinggi	= $this->Perguruan_tinggi_model->get_perguruan_tinggi_aktif();
					$update['perguruan_tinggi_id'] = ($get_perguruan_tinggi)?$get_perguruan_tinggi->perguruan_tinggi_id:null;
					$update['program_studi_id'] = null;
					$update['jenjang_kode'] = null;
				}
				$update['dosen_ktp'] = validasi_input($this->input->post('dosen_ktp'));
				$update['dosen_nidn'] = validasi_input($this->input->post('dosen_nidn'));
				$update['dosen_nama'] = validasi_input($this->input->post('dosen_nama'));
				$update['dosen_gelar'] = validasi_input($this->input->post('dosen_gelar'));
				$update['dosen_tempat_lahir'] = validasi_input($this->input->post('dosen_tempat_lahir'));
				$update['dosen_tanggal_lahir'] = validasi_input($this->input->post('dosen_tanggal_lahir'));
				$update['dosen_jenis_kelamin'] = validasi_input($this->input->post('dosen_jenis_kelamin'));
				$update['dosen_semester_mulai'] = validasi_input($this->input->post('dosen_semester_mulai'));
				$update['dosen_nip_pns'] = validasi_input($this->input->post('dosen_nip_pns'));
				$update['dosen_homebase'] = validasi_input($this->input->post('dosen_homebase'));
				$update['jabatan_akademik_kode'] = validasi_input($this->input->post('jabatan_akademik_kode'));
				$update['pendidikan_kode'] = validasi_input($this->input->post('pendidikan_kode'));
				$update['status_ikatan_kerja_kode'] = validasi_input($this->input->post('status_ikatan_kerja_kode'));
				$update['status_aktivitas_kode'] = validasi_input($this->input->post('status_aktivitas_kode'));
				$update['dosen_user'] = validasi_input($this->input->post('dosen_user'));
				$update['dosen_pin'] = validasi_input($this->input->post('dosen_pin'));
				$update['updated_by'] = userdata('pengguna_id');

				$this->Dosen_model->update_dosen($where_update, $update);

				// USER SISWA
				$dosen_id		= validasi_sql($this->input->post('dosen_id'));
				$dosen_nama		= validasi_sql($this->input->post('dosen_nama'));
				$dosen_user		= validasi_sql($this->input->post('dosen_user'));
				$dosen_pin		= validasi_sql($this->input->post('dosen_pin'));

				$where_pengguna = array();
				$where_pengguna['pengguna.pengguna_nama']		= $dosen->dosen_user;
				$where_pengguna['pengguna.pengguna_level_id']	= 10;
				if ($this->Pengguna_model->count_all_pengguna($where_pengguna) > 0){
					$update_pengguna = array();
					$update_pengguna['pengguna_nama_depan']	= $dosen_nama;
					if ($dosen_pin != 'CHANGED' && $dosen_pin != $dosen->dosen_pin && $dosen_pin && strlen($dosen_pin) >= 6){
						$update_pengguna['pengguna_kunci']	= hash_password($dosen_pin);

						$this->Dosen_model->update_dosen(array('dosen_id'=>$dosen_id), array('dosen_pin'=>$dosen_pin));
					}
					if ($dosen->dosen_user != $dosen_user){
						$update_pengguna['pengguna_nama']	= $dosen_user;
					}
					$this->Pengguna_model->update_pengguna(array('pengguna_nama'=>$dosen->dosen_user, 'pengguna_level_id'=>10), $update_pengguna);
				}
				
				$this->session->set_flashdata('success','Dosen telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User telah digunakan. Silahkan gunakan user yang lain.');
				redirect(module_url($this->uri->segment(2)));
			}
			
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/dosen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$dosen_id		= validasi_sql($this->uri->segment(4));
		$dosen_user		= validasi_sql($this->uri->segment(5));
		if ($this->Dosen_model->count_all_dosen_mengajar("dosen_id = '$dosen_id' OR dosen_pengampu = '$dosen_id'") < 1){

			$where_delete_dosen['dosen_id']	= $dosen_id;
			$this->Dosen_model->delete_dosen($where_delete_dosen);
			
			$where_delete_pengguna = array();
			$where_delete_pengguna['dosen_id']	= $dosen_id;
			$where_delete_pengguna['pengguna_level_id']	= 10;
			$this->Pengguna_model->delete_pengguna($where_delete_pengguna);
				
			$this->session->set_flashdata('success','Dosen telah berhasil dihapus.');
		} else {
			$this->session->set_flashdata('error','Dosen gagal dihapus.');
		}
		
		redirect(module_url($this->uri->segment(2)));
	}

	public function delete_user()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$dosen_id			= validasi_sql($this->uri->segment(4));
		$dosen_user			= validasi_sql($this->uri->segment(5));

		$where_delete_pengguna = array();
		$where_delete_pengguna['dosen_id']	= $dosen_id;
		$where_delete_pengguna['pengguna_level_id']	= 10;
		$this->Pengguna_model->delete_pengguna($where_delete_pengguna);
			
		$this->session->set_flashdata('success','Dosen telah berhasil dihapus.');
		
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['dosen_id']	= validasi_sql($this->uri->segment(4)); 
		$dosen		 			= $this->Dosen_model->get_dosen('*', $where);
		if (!$dosen){
			redirect(module_url($this->uri->segment(2)));
		}
		
		$data['dosen_id'] = $dosen->dosen_id;
		$data['perguruan_tinggi_id'] = $dosen->perguruan_tinggi_id;
		$data['program_studi_id'] = $dosen->program_studi_id;
		$data['jenjang_kode'] = $dosen->jenjang_kode;
		$data['dosen_ktp'] = $dosen->dosen_ktp;
		$data['dosen_nidn'] = $dosen->dosen_nidn;
		$data['dosen_nama'] = $dosen->dosen_nama;
		$data['dosen_gelar'] = $dosen->dosen_gelar;
		$data['dosen_tempat_lahir'] = $dosen->dosen_tempat_lahir;
		$data['dosen_tanggal_lahir'] = $dosen->dosen_tanggal_lahir;
		$data['dosen_jenis_kelamin'] = $dosen->dosen_jenis_kelamin;
		$data['dosen_semester_mulai'] = $dosen->dosen_semester_mulai;
		$data['dosen_nip_pns'] = $dosen->dosen_nip_pns;
		$data['dosen_homebase'] = $dosen->dosen_homebase;
		$data['jabatan_akademik_kode'] = $dosen->jabatan_akademik_kode;
		$data['pendidikan_kode'] = $dosen->pendidikan_kode;
		$data['status_ikatan_kerja_kode'] = $dosen->status_ikatan_kerja_kode;
		$data['status_aktivitas_kode'] = $dosen->status_aktivitas_kode;
		$data['dosen_user'] = $dosen->dosen_user;
		$data['dosen_pin'] = $dosen->dosen_pin;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/dosen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function import()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'import';
		
		$data['dataExcel']	= array();		
		$data['filename']	= '';
		
		if ($this->input->post('importDosen')){
			if ($_FILES['userfile']['tmp_name']){
				$ext = end(explode(".", basename($_FILES['userfile']['name'])));
				if ($ext == 'xls'){
					
					$timestamp = explode(" ",microtime());
					$filename = time().str_shuffle('123456ABCDEF');
					
					$uploaddir = './asset/temp_upload/';
					$uploadfname = 'dosen_'.$filename . '.' . end(explode(".", basename($_FILES['userfile']['name'])));
					$uploadfile = $uploaddir . $uploadfname;
					$data['filename'] = $uploadfname;
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					
						$this->load->library('excel_reader');
						$this->excel_reader->setOutputEncoding('CP1251');
						$this->excel_reader->read($uploadfile);
						$dataFile = $this->excel_reader->sheets[0];
						
						$paramsReal = array("NO", "PROGRAM STUDI", "USER LOGIN", "PASSWORD LOGIN", "NO KTP", "NIDN", "NAMA LENGKAP", "GELAR AKADEMIK", "TEMPAT LAHIR", "TANGGAL LAHIR", "JENIS KELAMIN", "JABATAN AKADEMIK", "PENDIDIKAN TERTINGGI", "STATUS IKATAN KERJA", "STATUS AKTIVITAS", "SEMESTER AKHIR", "NIP PNS", "HOMEBASE");
						$paramsReal = $iOne = array_combine(range(1, count($paramsReal)), array_values($paramsReal));
						$paramsFile = $dataFile['cells'][2];
						$result 	= array_diff($paramsReal,$paramsFile);
						
						if (count($result) == 0){
							for ($i = 4; $i <= $dataFile['numRows']; $i++) {
								if($dataFile['cells'][$i][1]){
									$data['dataExcel'][$i]['dosen_id'] 					= (isset($dataFile['cells'][$i][1]))?trim($dataFile['cells'][$i][1]):'';
									$data['dataExcel'][$i]['dosen_prodi'] 				= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
									$data['dataExcel'][$i]['dosen_user'] 				= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
									$data['dataExcel'][$i]['dosen_pin'] 				= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
									$data['dataExcel'][$i]['dosen_ktp'] 				= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
									$data['dataExcel'][$i]['dosen_nidn'] 				= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';
									$data['dataExcel'][$i]['dosen_nama'] 				= (isset($dataFile['cells'][$i][7]))?trim($dataFile['cells'][$i][7]):'';
									$data['dataExcel'][$i]['dosen_gelar'] 				= (isset($dataFile['cells'][$i][8]))?trim($dataFile['cells'][$i][8]):'';
									$data['dataExcel'][$i]['dosen_tempat_lahir'] 		= (isset($dataFile['cells'][$i][9]))?trim($dataFile['cells'][$i][9]):'';
									$data['dataExcel'][$i]['dosen_tanggal_lahir'] 		= (isset($dataFile['cells'][$i][10]))?trim($dataFile['cells'][$i][10]):'';
									$data['dataExcel'][$i]['dosen_jenis_kelamin'] 		= (isset($dataFile['cells'][$i][11]))?trim($dataFile['cells'][$i][11]):'';
									$data['dataExcel'][$i]['jabatan_akademik_kode'] 	= (isset($dataFile['cells'][$i][12]))?trim($dataFile['cells'][$i][12]):'';
									$data['dataExcel'][$i]['pendidikan_kode'] 			= (isset($dataFile['cells'][$i][13]))?trim($dataFile['cells'][$i][13]):'';
									$data['dataExcel'][$i]['status_ikatan_kerja_kode'] 	= (isset($dataFile['cells'][$i][14]))?trim($dataFile['cells'][$i][14]):'';
									$data['dataExcel'][$i]['status_aktivitas_kode'] 	= (isset($dataFile['cells'][$i][15]))?trim($dataFile['cells'][$i][15]):'';
									$data['dataExcel'][$i]['dosen_semester_mulai'] 		= (isset($dataFile['cells'][$i][16]))?trim($dataFile['cells'][$i][16]):'';
									$data['dataExcel'][$i]['dosen_nip_pns'] 			= (isset($dataFile['cells'][$i][17]))?trim($dataFile['cells'][$i][17]):'';
									$data['dataExcel'][$i]['dosen_homebase'] 			= (isset($dataFile['cells'][$i][18]))?trim($dataFile['cells'][$i][18]):'';
									
									$data['dataExcel'][$i]['data_prodi'] 	= 'PRODI INVALID';
									if ($this->Program_studi_model->count_all_program_studi("(program_studi_kode = '".$data['dataExcel'][$i]['dosen_prodi']."' OR program_studi_nama = '".$data['dataExcel'][$i]['dosen_prodi']."')") > 0){
										$data['dataExcel'][$i]['data_prodi'] 	= 'PRODI VALID';
									}
								}
							}
						} else {
							$this->session->set_flashdata('error','Format file salah.');
							redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
						}
					} else {
						$this->session->set_flashdata('error','File gagal di-upload.');
						redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
					}
				} else {
					$this->session->set_flashdata('error','Format file yang Anda gunakan salah. Silahkan gunakan format file Excel 97-2003 (.xls).');
					redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Silahkan masukkan file yang akan di-import.');
				redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
			}
		}
		
		if ($this->input->post('save')){
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$this->excel_reader->read('./asset/temp_upload/'.$this->input->post('filename'));
			$dataFile = $this->excel_reader->sheets[0];
			
			for ($i = 4; $i <= $dataFile['numRows']; $i++) {
				if($dataFile['cells'][$i][1]){
					$dosen_id 					= (isset($dataFile['cells'][$i][1]))?trim($dataFile['cells'][$i][1]):'';
					$dosen_prodi 				= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
					$dosen_user 				= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
					$dosen_pin 					= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
					$dosen_ktp 					= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
					$dosen_nidn 				= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';
					$dosen_nama 				= (isset($dataFile['cells'][$i][7]))?trim($dataFile['cells'][$i][7]):'';
					$dosen_gelar 				= (isset($dataFile['cells'][$i][8]))?trim($dataFile['cells'][$i][8]):'';
					$dosen_tempat_lahir 		= (isset($dataFile['cells'][$i][9]))?trim($dataFile['cells'][$i][9]):'';
					$dosen_tanggal_lahir 		= (isset($dataFile['cells'][$i][10]))?trim($dataFile['cells'][$i][10]):'';
					$dosen_jenis_kelamin 		= (isset($dataFile['cells'][$i][11]))?trim($dataFile['cells'][$i][11]):'';
					$jabatan_akademik_kode 		= (isset($dataFile['cells'][$i][12]))?trim($dataFile['cells'][$i][12]):'';
					$pendidikan_kode 			= (isset($dataFile['cells'][$i][13]))?trim($dataFile['cells'][$i][13]):'';
					$status_ikatan_kerja_kode 	= (isset($dataFile['cells'][$i][14]))?trim($dataFile['cells'][$i][14]):'';
					$status_aktivitas_kode 		= (isset($dataFile['cells'][$i][15]))?trim($dataFile['cells'][$i][15]):'';
					$dosen_semester_mulai 		= (isset($dataFile['cells'][$i][16]))?trim($dataFile['cells'][$i][16]):'';
					$dosen_nip_pns 				= (isset($dataFile['cells'][$i][17]))?trim($dataFile['cells'][$i][17]):'';
					$dosen_homebase 			= (isset($dataFile['cells'][$i][18]))?trim($dataFile['cells'][$i][18]):'';
					
					if ($dosen_user){
						if ($this->Dosen_model->count_all_dosen(array("dosen.dosen_user"=>$dosen_user)) < 1){

							$insert = array();
							if ($this->input->post('program_studi_id')){
								$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "(program_studi_kode = '$dosen_prodi' OR program_studi_nama = '$dosen_prodi')");
								$insert['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
								$insert['program_studi_id'] = $get_program_studi->program_studi_id;
								$insert['jenjang_kode'] = $get_program_studi->jenjang_kode;
							} else {
								$get_perguruan_tinggi	= $this->Perguruan_tinggi_model->get_perguruan_tinggi_aktif();
								$insert['perguruan_tinggi_id'] = ($get_perguruan_tinggi)?$get_perguruan_tinggi->perguruan_tinggi_id:null;
								$insert['program_studi_id'] = null;
								$insert['jenjang_kode'] = null;
							}

							$insert['dosen_id'] = $this->uuid->v4();
							$insert['dosen_ktp'] = validasi_input($dosen_ktp);
							$insert['dosen_nidn'] = validasi_input($dosen_nidn);
							$insert['dosen_nama'] = validasi_input($dosen_nama);
							$insert['dosen_gelar'] = validasi_input($dosen_gelar);
							$insert['dosen_tempat_lahir'] = validasi_input($dosen_tempat_lahir);
							$insert['dosen_tanggal_lahir'] = validasi_input($dosen_tanggal_lahir);
							$insert['dosen_jenis_kelamin'] = validasi_input($dosen_jenis_kelamin);
							$insert['dosen_semester_mulai'] = validasi_input($dosen_semester_mulai);
							$insert['dosen_nip_pns'] = validasi_input($dosen_nip_pns);
							$insert['dosen_homebase'] = validasi_input($dosen_homebase);
							$insert['jabatan_akademik_kode'] = validasi_input($jabatan_akademik_kode);
							$insert['pendidikan_kode'] = validasi_input($pendidikan_kode);
							$insert['status_ikatan_kerja_kode'] = validasi_input($status_ikatan_kerja_kode);
							if ($status_aktivitas_kode){
								$insert['status_aktivitas_kode'] = validasi_input($status_aktivitas_kode);
							} else {
								$insert['status_aktivitas_kode'] = "A";
							}
							$insert['dosen_user'] = validasi_input($dosen_user);
							$insert['dosen_pin'] = validasi_input($dosen_pin);
							$insert['created_by'] = userdata('pengguna_id');
									
							$this->Dosen_model->insert_dosen($insert);
						} else if ($this->Dosen_model->count_all_dosen(array("dosen.dosen_user"=>$dosen_user)) > 0){

							$get_dosen = $this->Dosen_model->get_dosen("", array("dosen.dosen_user"=>$dosen_user));

							$update = array();
							if ($this->input->post('program_studi_id')){
								$get_program_studi	= $this->Program_studi_model->get_program_studi("*", "(program_studi_kode = '$dosen_prodi' OR program_studi_nama = '$dosen_prodi')");
								$update['perguruan_tinggi_id'] = $get_program_studi->perguruan_tinggi_id;
								$update['program_studi_id'] = $get_program_studi->program_studi_id;
								$update['jenjang_kode'] = $get_program_studi->jenjang_kode;
							} else {
								$get_perguruan_tinggi	= $this->Perguruan_tinggi_model->get_perguruan_tinggi_aktif();
								$update['perguruan_tinggi_id'] = ($get_perguruan_tinggi)?$get_perguruan_tinggi->perguruan_tinggi_id:null;
								$update['program_studi_id'] = null;
								$update['jenjang_kode'] = null;
							}

							$where_pengguna = array();
							$where_pengguna['dosen.dosen_id'] = $get_dosen->dosen_id;
							$where_pengguna['pengguna.pengguna_level_id'] = 10;
							if ($this->Pengguna_model->count_all_pengguna($where_pengguna) > 0){
								$update_pengguna = array();
								$update_pengguna['pengguna_nama_depan']	= $dosen_nama;
								if ($get_dosen->dosen_pin != 'CHANGED' && $get_dosen->dosen_pin != $dosen_pin && $dosen_pin && strlen($dosen_pin) >= 6){
									$update_pengguna['pengguna_kunci'] = hash_password($dosen_pin);

									$update['dosen_pin'] = $dosen_pin;
								}
								$this->Pengguna_model->update_pengguna(array('dosen_id'=>$get_dosen->dosen_id, 'pengguna_level_id'=>10), $update_pengguna);
							} else {
								$update['dosen_pin'] = $dosen_pin;
							}

							$update['dosen_ktp'] = validasi_input($dosen_ktp);
							$update['dosen_nidn'] = validasi_input($dosen_nidn);
							$update['dosen_nama'] = validasi_input($dosen_nama);
							$update['dosen_gelar'] = validasi_input($dosen_gelar);
							$update['dosen_tempat_lahir'] = validasi_input($dosen_tempat_lahir);
							$update['dosen_tanggal_lahir'] = validasi_input($dosen_tanggal_lahir);
							$update['dosen_jenis_kelamin'] = validasi_input($dosen_jenis_kelamin);
							$update['dosen_semester_mulai'] = validasi_input($dosen_semester_mulai);
							$update['dosen_nip_pns'] = validasi_input($dosen_nip_pns);
							$update['dosen_homebase'] = validasi_input($dosen_homebase);
							$update['jabatan_akademik_kode'] = validasi_input($jabatan_akademik_kode);
							$update['pendidikan_kode'] = validasi_input($pendidikan_kode);
							$update['status_ikatan_kerja_kode'] = validasi_input($status_ikatan_kerja_kode);if ($status_aktivitas_kode){
								$update['status_aktivitas_kode'] = validasi_input($status_aktivitas_kode);
							} else {
								$update['status_aktivitas_kode'] = "A";
							}
							$update['updated_by'] = userdata('pengguna_id');
							$this->Dosen_model->update_dosen(array('dosen_user'=>$dosen_user), $update);
						}
					}
				}
			}

			$this->session->set_flashdata('success','Dosen telah berhasil di-import.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/dosen', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function generate_account()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		foreach($this->Dosen_model->grid_all_dosen('', 'dosen_id', 'ASC') as $row){
			if ($row->dosen_user){
				if ($this->Pengguna_model->count_all_pengguna(array('pengguna_nama'=>$row->dosen_user, 'pengguna.pengguna_level_id'=>10)) < 1){
					if ($row->dosen_pin && strlen($row->dosen_pin) >= 6){
						$acak 	= $row->dosen_pin;
					} else {
						$alpha 	= "0123456789";
						$acak	= str_shuffle($alpha);
						$acak 	= substr($acak,0,6);
						
						$update_pin = array();
						$update_pin['dosen_pin']					= $acak;
						$this->Dosen_model->update_dosen(array('dosen_id'=>$row->dosen_id), $update_pin);
					}
					
					$insert_pengguna 						= array();
					$insert_pengguna['program_studi_id']	= $row->program_studi_id;
					$insert_pengguna['dosen_id']			= $row->dosen_id;
					$insert_pengguna['pengguna_id']			= $this->uuid->v4();
					$insert_pengguna['pengguna_nama']		= $row->dosen_user;
					$insert_pengguna['pengguna_nama_depan']	= $row->dosen_nama;
					$insert_pengguna['pengguna_kunci']		= hash_password($acak);
					$insert_pengguna['pengguna_level_id']	= 10;
					$insert_pengguna['pengguna_terdaftar']	= date("Y-m-d H:i:s");
					$insert_pengguna['pengguna_status']		= 'A';
					$this->Pengguna_model->insert_pengguna($insert_pengguna);
				}
			}
		}
		
		$this->session->set_flashdata('success','User dosen telah berhasil di-generate.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function genpindosen(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$dosen_id 			= validasi_sql($this->uri->segment(4));
		$dosen_user 		= validasi_sql($this->uri->segment(5));
		if ($this->Pengguna_model->count_all_pengguna(array('pengguna_nama'=>$dosen_user, 'pengguna.pengguna_level_id'=>10)) > 0){
			$alpha 	= "0123456789";
			$acak	= str_shuffle($alpha);
			$acak 	= substr($acak,0,6);
			
			$update_pengguna['pengguna_kunci']		= hash_password($acak);
			$this->Pengguna_model->update_pengguna(array('pengguna_nama'=>$dosen_user, 'pengguna_level_id'=>10), $update_pengguna);
			
			$update_pin = array();
			$update_pin['dosen_pin']	= $acak;
			$this->Dosen_model->update_dosen(array('dosen_id'=>$dosen_id), $update_pin);

			$this->session->set_flashdata('success','Pin dosen telah berhasil di-reset.<br /><strong>Pin Baru : '.$acak.'</strong>');
			redirect(module_url($this->uri->segment(2)));
		} else {
			$where['dosen_id']		= $dosen_id; 
			$row 					= $this->Dosen_model->get_dosen('*', $where);
			if ($row->dosen_pin && strlen($row->dosen_pin) >= 6){
				$acak 	= $row->dosen_pin;
			} else {
				$alpha 	= "0123456789";
				$acak	= str_shuffle($alpha);
				$acak 	= substr($acak,0,6);
				
				$update_pin = array();
				$update_pin['dosen_pin']	= $acak;
				$this->Dosen_model->update_dosen(array('dosen_id'=>$row->dosen_id), $update_pin);
			}
			
			$insert_pengguna 						= array();
			$insert_pengguna['program_studi_id']	= $row->program_studi_id;
			$insert_pengguna['dosen_id']			= $row->dosen_id;
			$insert_pengguna['pengguna_id']			= $this->uuid->v4();
			$insert_pengguna['pengguna_nama']		= $row->dosen_user;
			$insert_pengguna['pengguna_nama_depan']	= $row->dosen_nama;
			$insert_pengguna['pengguna_kunci']		= hash_password($acak);
			$insert_pengguna['pengguna_level_id']	= 10;
			$insert_pengguna['pengguna_terdaftar']	= date("Y-m-d H:i:s");
			$insert_pengguna['pengguna_status']		= 'A';
			$this->Pengguna_model->insert_pengguna($insert_pengguna);

			$this->session->set_flashdata('success','Pin dosen telah berhasil dibuat.<br /><strong>Pin Anda : '.$acak.'</strong>');
			redirect(module_url($this->uri->segment(2)));
		}
		
	}
	
	public function pin_dosen()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['dosen']	= $this->Dosen_model->grid_all_dosen('*', 'dosen_user', 'ASC');
		$this->load->view(module_dir().'/export/list_user_dosen', $data);
	}

	public function set_dosen()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['dosen']	= $this->Dosen_model->grid_all_dosen('dosen_nama, dosen_user, dosen_id', 'dosen_user', 'ASC');
		foreach ($data['dosen'] as $row) {
			$update_dosen = array();
			$update_dosen['dosen_nama']	= strtoupper($row->dosen_nama);
			$this->Dosen_model->update_dosen(array('dosen_user'=>$row->dosen_user), $update_dosen);
		}
	}
	
	public function check_user(){
		$data = array();
		$action = $this->input->post('action');
		$user = $this->input->post('user');
		$id = $this->input->post('id');
		if ($action == 'add' && $user){
			$dosen = $this->Dosen_model->get_dosen("dosen_id", array('dosen_user'=>$user));
			if (!$dosen){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $dosen;
			} else {
				$data['response']	= false;
				$data['message']	= "User telah digunakan ada.";
			}
		} else if ($action == 'edit' && $user && $id){
			$dosen = $this->Dosen_model->get_dosen("dosen_id", array('dosen_user'=>$user, 'dosen_id !='=>$id));
			if (!$dosen){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['data']		= $dosen;
			} else {
				$data['response']	= false;
				$data['message']	= "User telah digunakan ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function get_semester(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun("*", array('tahun_kode'=>$tahun_kode));
			if ($tahun){
				$semester = $this->Semester_model->grid_all_semester("semester.semester_kode, semester.semester_nama", "semester_nama", "ASC", 0, 0, array('semester.tahun_kode'=>$tahun_kode));
				if ($semester){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $semester;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	// aktivitas
	public function datatable_aktivitas()
    {
		$dosen_id = $this->uri->segment(4);
		$this->Datatable_model->set_table("(SELECT dosen_mengajar.*, matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, semester.semester_nama FROM akd_dosen_mengajar dosen_mengajar LEFT JOIN akd_matakuliah matakuliah ON dosen_mengajar.matakuliah_id=matakuliah.matakuliah_id LEFT JOIN akd_semester semester ON dosen_mengajar.semester_kode=semester.semester_kode WHERE dosen_mengajar.dosen_id = '$dosen_id') dosen_mengajar");
		$this->Datatable_model->set_column_order(array('semester_nama', 'matakuliah_kode', 'matakuliah_nama', 'dosen_mengajar_rencana_pertemuan', 'dosen_mengajar_realisasi_pertemuan', 'dosen_mengajar_kelas', null));
		$this->Datatable_model->set_column_search(array('semester_nama', 'matakuliah_kode', 'matakuliah_nama', 'dosen_mengajar_rencana_pertemuan', 'dosen_mengajar_realisasi_pertemuan', 'dosen_mengajar_kelas'));
		$this->Datatable_model->set_order(array('semester_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['matakuliah_id'] = $record->matakuliah_id;
            $row['matakuliah_kode'] = $record->matakuliah_kode;
            $row['matakuliah_nama'] = $record->matakuliah_nama;
            $row['semester_nama'] = $record->semester_nama;
            $row['dosen_mengajar_rencana_pertemuan'] = $record->dosen_mengajar_rencana_pertemuan;
            $row['dosen_mengajar_realisasi_pertemuan'] = $record->dosen_mengajar_realisasi_pertemuan;
            $row['dosen_mengajar_kelas'] = $record->dosen_mengajar_kelas;
            $row['dosen_mengajar_id'] = $record->dosen_mengajar_id;
			
			$html  = '<div class="text-center">';
			if (check_permission("W")){
				$html .= '<a href="'. site_url($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/delete_aktivitas/'.$record->dosen_mengajar_id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
			}
			$html .= '</div>';
			
            $row['Actions'] = $html;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function delete_aktivitas()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$dosen_mengajar_id = $this->uri->segment(4);
		$dosen_mengajar = $this->Dosen_model->get_dosen_mengajar('dosen_mengajar.*', array('dosen_mengajar.dosen_mengajar_id'=>$dosen_mengajar_id));

		if ($this->Matakuliah_model->count_all_matakuliah(array('matakuliah.semester_kode'=>$dosen_mengajar->semester_kode, 'matakuliah.dosen_id'=>$dosen_mengajar->dosen_id)) < 1){
			$this->Dosen_model->delete_dosen_mengajar(array('dosen_mengajar_id'=>validasi_sql($dosen_mengajar_id)));
			
			$this->session->set_flashdata('success','Data telah berhasil dihapus.');
			redirect(module_url($this->uri->segment(2).'/detail/'.$dosen_mengajar->dosen_id.'?tabs=1'));
		} else {
			$this->session->set_flashdata('success','Data gagal dihapus.');
			redirect(module_url($this->uri->segment(2).'/detail/'.$dosen_mengajar->dosen_id.'?tabs=1'));
		}

	}
}
