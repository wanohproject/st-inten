<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Kelas | ' . profile('profil_website');
		$this->active_menu		= 257;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Kelas_model');
		$this->load->model('Tahun_model');
		$this->load->model('Peminatan_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Staf_model');
		$this->load->model('Siswa_model');
		$this->load->model('Departemen_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$departemen_id 		= $this->uri->segment(4);
		$tahun_kode 			= $this->uri->segment(5);
		$tingkat_id 		= $this->uri->segment(6);
		$where 				= "kelas.departemen_id = '{$departemen_id}' AND kelas.tahun_kode = '{$tahun_kode}'";
		if ($tingkat_id){
			$where			.= " AND kelas.tingkat_id = '{$tingkat_id}'";
		}
		
		$this->load->library('Datatables');
		$this->datatables->select('kelas_id, kelas_nama, tingkat_nama, staf_nama')
		->add_column('Actions', $this->get_buttons('$1'),'kelas_id')
		->search_column('kelas_nama')
		->from('kelas')
		->join('tingkat', 'kelas.tingkat_id=tingkat.tingkat_id', 'left')
		->join('staf', 'kelas.staf_id=staf.staf_id', 'left')
		->join('tahun_ajaran', 'kelas.tahun_kode=tahun_ajaran.tahun_kode', 'left')
		->join('departemen', 'kelas.departemen_id=departemen.departemen_id', 'left')
		->where($where);
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= $this->uri->segment(4);
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$tahun_kode = ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$tingkat_id = ($this->uri->segment(6))?$this->uri->segment(6):0;

		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$tingkat_id;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$departemen_id = ($this->uri->segment(4))?$this->uri->segment(4):0;
		$tahun_kode = ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
		$tingkat_id = ($this->uri->segment(6))?$this->uri->segment(6):0;

		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$tingkat_id;
		$data['departemen_id']	= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		$data['jenis_kelas_id']	= ($this->input->post('jenis_kelas_id'))?$this->input->post('jenis_kelas_id'):'';
		$data['kelas_nama']		= ($this->input->post('kelas_nama'))?$this->input->post('kelas_nama'):'';
		$data['kelas_peminatan']= ($this->input->post('kelas_peminatan'))?$this->input->post('kelas_peminatan'):'';
		$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):'';
		$data['bk_id']			= ($this->input->post('bk_id'))?$this->input->post('bk_id'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){			
			$insert['tahun_kode']			= validasi_sql($this->input->post('tahun_kode'));
			$insert['tingkat_id']		= validasi_sql($this->input->post('tingkat_id'));
			$insert['departemen_id']	= validasi_sql($this->input->post('departemen_id'));
			$insert['jenis_kelas_id']	= validasi_sql($this->input->post('jenis_kelas_id'));
			$insert['kelas_nama']		= validasi_sql($this->input->post('kelas_nama'));
			$insert['kelas_peminatan']	= validasi_sql($this->input->post('kelas_peminatan'));
			$insert['staf_id']			= validasi_sql($this->input->post('staf_id'));
			$insert['bk_id']			= validasi_sql($this->input->post('bk_id'));
			$this->Kelas_model->insert_kelas($insert);
			
			$this->session->set_flashdata('success','Kelas telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id'].'/'.$data['tahun_kode'].'/'.$data['tingkat_id']));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['kelas_id']		= validasi_sql($this->uri->segment(4)); 
		$kelas 					= $this->Kelas_model->get_kelas('*', $where);

		$data['kelas_id']			= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):$kelas->kelas_id;
		$data['kelas_nama']			= ($this->input->post('kelas_nama'))?$this->input->post('kelas_nama'):$kelas->kelas_nama;
		$data['tahun_kode']			= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$kelas->tahun_kode;
		$data['tingkat_id']			= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$kelas->tingkat_id;
		$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$kelas->departemen_id;
		$data['jenis_kelas_id']		= ($this->input->post('jenis_kelas_id'))?$this->input->post('jenis_kelas_id'):$kelas->jenis_kelas_id;
		$data['kelas_peminatan']	= ($this->input->post('kelas_peminatan'))?$this->input->post('kelas_peminatan'):$kelas->kelas_peminatan;
		$data['kelas_ketua']		= ($this->input->post('kelas_ketua'))?$this->input->post('kelas_ketua'):$kelas->kelas_ketua;
		$data['kelas_sekretaris']	= ($this->input->post('kelas_sekretaris'))?$this->input->post('kelas_sekretaris'):$kelas->kelas_sekretaris;
		$data['staf_id']			= ($this->input->post('staf_id'))?$this->input->post('staf_id'):$kelas->staf_id;
		$data['bk_id']				= ($this->input->post('bk_id'))?$this->input->post('bk_id'):$kelas->bk_id;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['kelas_id']		= validasi_sql($this->input->post('kelas_id'));
			$edit['kelas_nama']			= validasi_sql($this->input->post('kelas_nama'));
			$edit['tahun_kode']			= validasi_sql($this->input->post('tahun_kode'));
			$edit['tingkat_id']			= validasi_sql($this->input->post('tingkat_id'));
			$edit['departemen_id']		= validasi_sql($this->input->post('departemen_id'));
			$edit['jenis_kelas_id']		= validasi_sql($this->input->post('jenis_kelas_id'));
			$edit['kelas_peminatan']	= validasi_sql($this->input->post('kelas_peminatan'));
			$edit['kelas_ketua']		= validasi_sql($this->input->post('kelas_ketua'));
			$edit['kelas_sekretaris']	= validasi_sql($this->input->post('kelas_sekretaris'));
			$edit['staf_id']			= validasi_sql($this->input->post('staf_id'));
			$edit['bk_id']				= validasi_sql($this->input->post('bk_id'));
			$this->Kelas_model->update_kelas($where_edit, $edit);

			if (validasi_sql($this->input->post('kelas_nama')) != $kelas->kelas_nama){
				$update_siswa['siswa_kelas_sekarang'] = validasi_sql($this->input->post('kelas_nama'));
				$this->Siswa_model->update_siswa(array('siswa_kelas_sekarang_id'=>validasi_sql($this->input->post('kelas_id'))), $update_siswa);
			}
			
			$this->session->set_flashdata('success','Kelas telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id'].'/'.$data['tahun_kode'].'/'.$data['tingkat_id']));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$where['kelas_id']		= validasi_sql($this->uri->segment(4)); 
		$kelas 					= $this->Kelas_model->get_kelas('*', $where);

		if ($kelas){
			$data['tahun_kode']			= $kelas->tahun_kode;
			$data['tingkat_id']			= $kelas->tingkat_id;
			$data['departemen_id']		= $kelas->departemen_id;
			
			$where_delete['kelas_id']	= validasi_sql($this->uri->segment(4));
			$this->Kelas_model->delete_kelas($where_delete);
			
			$this->session->set_flashdata('success','Kelas telah berhasil dihapus.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['departemen_id'].'/'.$data['tahun_kode'].'/'.$data['tingkat_id']));
		} else {
			$this->session->set_flashdata('error','Kelas gagal dihapus.');
			redirect(module_url($this->uri->segment(2)));
		}
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['kelas_id']	= validasi_sql($this->uri->segment(4)); 
		$kelas		 		= $this->Kelas_model->get_kelas('*', $where);
		
		$data['kelas_id']			= $kelas->kelas_id;
		$data['kelas_nama']			= $kelas->kelas_nama;
		$data['tingkat_nama']		= $kelas->tingkat_nama;
		$data['jenis_kelas_nama']	= $kelas->jenis_kelas_nama;
		$data['kelas_peminatan']	= $kelas->kelas_peminatan;
		$data['kelas_ketua']	= $kelas->kelas_ketua;
		$data['kelas_sekretaris']	= $kelas->kelas_sekretaris;
		$data['staf_nama']			= $kelas->staf_nama;
		$data['departemen_nama']			= $kelas->departemen_nama;

		$tahun		 		= $this->Tahun_model->get_tahun_ajaran('*', array('tahun_kode'=>$kelas->tahun_kode));
		$data['tahun_nama']			= $tahun->tahun_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function import()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'import';
		
		$data['dataExcel']				= array();		
		$data['filename']				= '';
		
		if ($this->input->post('importKD')){
			if ($_FILES['userfile']['tmp_name']){
				$ext = end(explode(".", basename($_FILES['userfile']['name'])));
				if ($ext == 'xls'){
					
					$timestamp = explode(" ",microtime());
					$filename = time().str_shuffle('123456ABCDEF');
					
					$uploaddir = './asset/temp_upload/';
					$uploadfname = 'siswa_'.$filename . '.' . end(explode(".", basename($_FILES['userfile']['name'])));
					$uploadfile = $uploaddir . $uploadfname;
					$data['filename'] = $uploadfname;
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					
						$this->load->library('excel_reader');
						$this->excel_reader->setOutputEncoding('CP1251');
						$this->excel_reader->read($uploadfile);
						$dataFile = $this->excel_reader->sheets[0];
						
						$paramsReal = array("NO", "DEPARTEMEN", "TAHUN AJARAN", "TINGKAT", "KELAS", "GURU");
						$paramsReal = $iOne = array_combine(range(1, count($paramsReal)), array_values($paramsReal));
						$paramsFile = $dataFile['cells'][2];
						$result 	= array_diff($paramsReal,$paramsFile);
						
						if (count($result) == 0){
							for ($i = 4; $i <= $dataFile['numRows']; $i++) {
								if($dataFile['cells'][$i][1]){
									$data['dataExcel'][$i]['id'] 			= (isset($dataFile['cells'][$i][1]))?$dataFile['cells'][$i][1]:'';
									$data['dataExcel'][$i]['departemen'] 	= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
									$data['dataExcel'][$i]['tahun_ajaran'] 	= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
									$data['dataExcel'][$i]['tingkat'] 		= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
									$data['dataExcel'][$i]['kelas']			= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
									$data['dataExcel'][$i]['guru']			= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';

									$valid = 0;
									$staf_exists = 0;
									if ($this->Tahun_model->count_all_tahun_ajaran(array('tahun_nama'=>$data['dataExcel'][$i]['tahun_ajaran'])) > 0){
										$valid++;
									}

									if ($this->Departemen_model->count_all_departemen("departemen_tipe IN ('Sekolah', 'Akademi') AND (departemen_kode = '".$data['dataExcel'][$i]['departemen']."' OR departemen_nama = '".$data['dataExcel'][$i]['departemen']."')") > 0){
										$valid++;

										$get_departemen	= $this->Departemen_model->get_departemen("*", "departemen_tipe IN ('Sekolah', 'Akademi') AND (departemen_kode = '".$data['dataExcel'][$i]['departemen']."' OR departemen_nama = '".$data['dataExcel'][$i]['departemen']."')");
										$departemen_id 	= ($get_departemen)?$get_departemen->departemen_id:'';
										
										if ($this->Tingkat_model->count_all_tingkat("departemen.departemen_id = '$departemen_id' AND tingkat_nama = '".$data['dataExcel'][$i]['tingkat']."'") > 0){
											$valid++;
										}

										$departemen_list = "";
										if ($departemen_id){
											$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
											$departemen_list = implode(", ", $departemen_list);
										}
										if ($this->Staf_model->count_all_staf("departemen.departemen_id IN ($departemen_list) AND (staf_nama = '".$data['dataExcel'][$i]['guru']."' OR staf_user = '".$data['dataExcel'][$i]['guru']."')") > 0){
											$staf_exists++;
										}
									}

									if ($valid == 3){
										$data['dataExcel'][$i]['nilai_status'] 		= 'Valid';
									} else {
										$data['dataExcel'][$i]['nilai_status'] 		= 'Not Valid';
									}

									if ($staf_exists == 1){
										$data['dataExcel'][$i]['staf_exists'] 		= 'Exists';
									} else {
										$data['dataExcel'][$i]['staf_exists'] 		= 'Not Exists';
									}
								}
							}
						} else {
							$this->session->set_flashdata('error','Format file salah.');
							redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
						}
					} else {
						$this->session->set_flashdata('error','File gagal di-upload.');
						redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
					}
				} else {
					$this->session->set_flashdata('error','Format file yang Anda gunakan salah. Silahkan gunakan format file Excel 97-2003 (.xls).');
					redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Silahkan masukkan file yang akan di-import.');
				redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
			}
		}
		
		if ($this->input->post('save')){
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$this->excel_reader->read('./asset/temp_upload/'.$this->input->post('filename'));
			$dataFile = $this->excel_reader->sheets[0];
			
			for ($i = 4; $i <= $dataFile['numRows']; $i++) {
				if($dataFile['cells'][$i][1]){
					$id 			= (isset($dataFile['cells'][$i][1]))?$dataFile['cells'][$i][1]:'';
					$departemen		= (isset($dataFile['cells'][$i][2]))?trim($dataFile['cells'][$i][2]):'';
					$tahun 			= (isset($dataFile['cells'][$i][3]))?trim($dataFile['cells'][$i][3]):'';
					$tingkat 		= (isset($dataFile['cells'][$i][4]))?trim($dataFile['cells'][$i][4]):'';
					$kelas 			= (isset($dataFile['cells'][$i][5]))?trim($dataFile['cells'][$i][5]):'';
					$guru 			= (isset($dataFile['cells'][$i][6]))?trim($dataFile['cells'][$i][6]):'';

					$get_tahun		= $this->Tahun_model->get_tahun_ajaran("*", array("tahun_nama"=>$tahun));
					$tahun_kode 		= ($get_tahun)?$get_tahun->tahun_kode:'';

					$get_departemen	= $this->Departemen_model->get_departemen("*", "departemen_tipe IN ('Sekolah', 'Akademi') AND (departemen_kode = '$departemen' OR departemen_nama = '$departemen')");
					$departemen_id 	= ($get_departemen)?$get_departemen->departemen_id:'';
					
					$get_tingkat	= $this->Tingkat_model->get_tingkat("*", "departemen.departemen_id = '$departemen_id' AND tingkat_nama = '$tingkat'");
					$tingkat_id 	= ($get_tingkat)?$get_tingkat->tingkat_id:'';
					
					$departemen_list = "";
					if ($departemen_id){
						$departemen_list = $this->Departemen_model->recursive_departemen_child($departemen_id);
						$departemen_list = implode(", ", $departemen_list);
					}
					$get_staf		= $this->Staf_model->get_staf("*", "departemen.departemen_id IN ($departemen_list) AND (staf_user = '$guru' OR staf_nama = '$guru')");
					$staf_id 		= ($get_staf)?$get_staf->staf_id:'';
					
					if ($tahun_kode && $tingkat_id && $departemen_id){
						$where = array();
						$where['kelas.tahun_kode']		= $tahun_kode;
						$where['kelas.tingkat_id']		= $tingkat_id;
						$where['kelas.departemen_id']	= $departemen_id;
						$where['kelas.kelas_nama']		= $kelas;
						
						if ($this->Kelas_model->count_all_kelas($where) < 1){
							$insert = array();
							$insert['tahun_kode'] 		= $tahun_kode;
							$insert['tingkat_id'] 		= $tingkat_id;
							$insert['departemen_id'] 	= $departemen_id;
							$insert['kelas_nama'] 		= $kelas;
							$insert['staf_id'] 			= $staf_id;
							$this->Kelas_model->insert_kelas($insert);
						} else {
							$where_update = array();
							$where_update['tahun_kode']		= $tahun_kode;
							$where_update['tingkat_id']		= $tingkat_id;
							$where_update['departemen_id']	= $departemen_id;
							$where_update['kelas_nama']		= $kelas;
							
							$update = array();
							$update['kelas_nama'] 			= $kelas;
							$update['staf_id'] 				= $staf_id;
							$update['kelas_status'] 		= 'A';
	
							$this->Kelas_model->update_kelas($where_update, $update);
						}
					}
				}
			}
			
			$this->session->set_flashdata('success','Data kelas telah berhasil di-import.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function copy()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'copy';
		
		$data['tahun_kode']				= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
		$data['tahun_tujuan']			= ($this->input->post('tahun_tujuan'))?$this->input->post('tahun_tujuan'):$this->tahun_kode;

		$save					= $this->input->post('save');
		if ($save == 'save'){
			$kelas = $this->Kelas_model->grid_all_kelas("", "kelas_id", "ASC", 0, 0, array("kelas.tahun_kode"=>$data['tahun_kode']));
			if ($kelas && $data['tahun_kode'] && $data['tahun_tujuan']){
				foreach ($kelas as $row) {
					if ($this->Kelas_model->count_all_kelas(array('kelas.tahun_kode'=>$data['tahun_tujuan'], 'kelas_nama'=>$row->kelas_nama)) < 1){
						$insert = array();
						$insert['tingkat_id']		= $row->tingkat_id;
						$insert['tahun_kode']			= $data['tahun_tujuan'];
						$insert['jenis_kelas_id']	= $row->jenis_kelas_id;
						$insert['staf_id']			= $row->staf_id;
						$insert['bk_id']			= $row->bk_id;
						$insert['departemen_id']	= $row->departemen_id;
						$insert['kelas_peminatan']	= $row->kelas_peminatan;
						$insert['kelas_nama']		= $row->kelas_nama;
						$this->Kelas_model->insert_kelas($insert);
					} else if ($this->Kelas_model->count_all_kelas(array('kelas.tahun_kode'=>$data['tahun_tujuan'], 'kelas_nama'=>$row->kelas_nama)) > 0){
						$update = array();
						$update['tingkat_id']		= $row->tingkat_id;
						$update['jenis_kelas_id']	= $row->jenis_kelas_id;
						$update['staf_id']			= $row->staf_id;
						$update['bk_id']			= $row->bk_id;
						$update['departemen_id']	= $row->departemen_id;
						$update['kelas_peminatan']	= $row->kelas_peminatan;
						$this->Kelas_model->update_kelas(array('kelas.tahun_kode'=>$data['tahun_tujuan'], 'kelas_nama'=>$row->kelas_nama), $update);
					}
				}
				$this->session->set_flashdata('success','Kelas telah berhasil di-copy.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','Kelas gagal di-copy.');
				redirect(module_url($this->uri->segment(2)));
			}
			
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelas', $data);
		$this->load->view(module_dir().'/separate/foot');
    }
}