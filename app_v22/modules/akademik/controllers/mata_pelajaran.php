<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mata_pelajaran extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Mata Pelajaran | ' . profile('profil_website');
		$this->active_menu		= 259;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Mata_pelajaran_model');
		$this->load->model('Aspek_pelajaran_model');
		$this->load->model('Tahun_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }
	
	function datatable()
	{
		$this->load->library('Datatables');
		$this->datatables->select('pelajaran_id, pelajaran_kode, pelajaran_nama, pelajaran_sifat, pelajaran_kelompok, pelajaran_peminatan, pelajaran_urutan, peminatan_nama')
		->add_column('pengujian', $this->get_pengujian('$1'),'pelajaran_id')
		->add_column('Actions', $this->get_buttons('$1'),'pelajaran_id')
		->search_column('pelajaran_kode, pelajaran_nama, pelajaran_sifat, pelajaran_kelompok, pelajaran_peminatan, pelajaran_urutan, peminatan_nama')
		->from('mata_pelajaran')
		->join("peminatan", "mata_pelajaran.peminatan_id=peminatan.peminatan_id", "left");
        echo $this->datatables->generate();
    }
	
	function get_pengujian($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/pengujian/index/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Pengujian"><i class="fa fa-eye"></i> Tampilkan</a>';
		$html .= '</div>';
		return $html;
	}

	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mata_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['pelajaran_id']			= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):'';
		$data['pelajaran_kode']			= ($this->input->post('pelajaran_kode'))?$this->input->post('pelajaran_kode'):'';
		$data['pelajaran_nama']			= ($this->input->post('pelajaran_nama'))?$this->input->post('pelajaran_nama'):'';
		$data['pelajaran_sifat']		= ($this->input->post('pelajaran_sifat'))?$this->input->post('pelajaran_sifat'):'Wajib';
		$data['pelajaran_kelompok']		= ($this->input->post('pelajaran_kelompok'))?$this->input->post('pelajaran_kelompok'):'';
		$data['pelajaran_peminatan']	= ($this->input->post('pelajaran_peminatan'))?$this->input->post('pelajaran_peminatan'):'';
		$data['pelajaran_urutan']		= ($this->input->post('pelajaran_urutan'))?$this->input->post('pelajaran_urutan'):'';
		$data['pelajaran_deskripsi']		= ($this->input->post('pelajaran_deskripsi'))?$this->input->post('pelajaran_deskripsi'):'';
		$data['pelajaran_status']		= ($this->input->post('pelajaran_status'))?$this->input->post('pelajaran_status'):'A';
		$save							= $this->input->post('save');
		if ($save == 'save'){
			$insert['peminatan_id']			= validasi_sql($this->input->post('pelajaran_peminatan'));
			$peminatan_nama = $this->db->query("SELECT peminatan_nama FROM peminatan WHERE peminatan_id = '{$insert['peminatan_id']}'")->row()->peminatan_nama;
			$insert['pelajaran_kode']		= validasi_sql($this->input->post('pelajaran_kode'));
			$insert['pelajaran_nama']		= validasi_sql($this->input->post('pelajaran_nama'));
			$insert['pelajaran_sifat']		= validasi_sql($this->input->post('pelajaran_sifat'));
			$insert['pelajaran_kelompok']	= validasi_sql($this->input->post('pelajaran_kelompok'));
			$insert['pelajaran_peminatan']	= $peminatan_nama;
			$insert['pelajaran_urutan']		= validasi_sql($this->input->post('pelajaran_urutan'));
			$insert['pelajaran_deskripsi']	= validasi_sql($this->input->post('pelajaran_deskripsi'));
			$insert['pelajaran_status']		= validasi_sql($this->input->post('pelajaran_status'));
			$this->Mata_pelajaran_model->insert_mata_pelajaran($insert);
						
			// $pelajaran 			= $this->Mata_pelajaran_model->get_mata_pelajaran("pelajaran_id", array('pelajaran_kode'=>$this->input->post('pelajaran_kode')));
			// $pelajaran_id 		= $pelajaran->pelajaran_id;
			// $aspek_pelajaran	= $this->input->post('aspek_pelajaran');
			
			// if ($aspek_pelajaran){
				// $this->Aspek_pelajaran_model->delete_aspek_pelajaran(array('aspek_pelajaran.pelajaran_id'=>$pelajaran_id));
				// foreach($aspek_pelajaran as $row){
					// $insert['aspek_id'] 				= $row;
					// $insert['pelajaran_id'] 			= $pelajaran_id;
					// $insert['tahun_kode'] 				= $this->tahun_kode;
					// $insert['aspek_pelajaran_tanggal'] 	= date('Y-m-d H:i:s');
					// $this->Aspek_pelajaran_model->insert_aspek_pelajaran($insert);
				// }
			// }
			
			$this->session->set_flashdata('success','Mata Pelajaran telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mata_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['pelajaran_id']		= validasi_sql($this->uri->segment(4)); 
		$mata_pelajaran 			= $this->Mata_pelajaran_model->get_mata_pelajaran('*', $where);

		$data['pelajaran_id']			= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):$mata_pelajaran->pelajaran_id;
		$data['pelajaran_kode']			= ($this->input->post('pelajaran_kode'))?$this->input->post('pelajaran_kode'):$mata_pelajaran->pelajaran_kode;
		$data['pelajaran_nama']			= ($this->input->post('pelajaran_nama'))?$this->input->post('pelajaran_nama'):$mata_pelajaran->pelajaran_nama;
		$data['pelajaran_sifat']		= ($this->input->post('pelajaran_sifat'))?$this->input->post('pelajaran_sifat'):$mata_pelajaran->pelajaran_sifat;
		$data['pelajaran_kelompok']		= ($this->input->post('pelajaran_kelompok'))?$this->input->post('pelajaran_kelompok'):$mata_pelajaran->pelajaran_kelompok;
		$data['pelajaran_peminatan']	= ($this->input->post('pelajaran_peminatan'))?$this->input->post('pelajaran_peminatan'):$mata_pelajaran->peminatan_id;
		$data['pelajaran_urutan']		= ($this->input->post('pelajaran_urutan'))?$this->input->post('pelajaran_urutan'):$mata_pelajaran->pelajaran_urutan;
		$data['pelajaran_deskripsi']		= ($this->input->post('pelajaran_deskripsi'))?$this->input->post('pelajaran_deskripsi'):$mata_pelajaran->pelajaran_deskripsi;
		$data['pelajaran_status']		= ($this->input->post('pelajaran_status'))?$this->input->post('pelajaran_status'):$mata_pelajaran->pelajaran_status;
		$save							= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['pelajaran_id']		= validasi_sql($this->input->post('pelajaran_id'));
			$edit['peminatan_id']			= validasi_sql($this->input->post('pelajaran_peminatan'));
			$peminatan_nama = $this->db->query("SELECT peminatan_nama FROM peminatan WHERE peminatan_id = '{$edit['peminatan_id']}'")->row()->peminatan_nama;
			$edit['pelajaran_kode']			= validasi_sql($this->input->post('pelajaran_kode'));
			$edit['pelajaran_nama']			= validasi_sql($this->input->post('pelajaran_nama'));
			$edit['pelajaran_sifat']		= validasi_sql($this->input->post('pelajaran_sifat'));
			$edit['pelajaran_kelompok']		= validasi_sql($this->input->post('pelajaran_kelompok'));
			$edit['pelajaran_peminatan']	= $peminatan_nama;
			$edit['pelajaran_urutan']		= validasi_sql($this->input->post('pelajaran_urutan'));
			$edit['pelajaran_deskripsi']		= validasi_sql($this->input->post('pelajaran_deskripsi'));
			$edit['pelajaran_status']		= validasi_sql($this->input->post('pelajaran_status'));
			$this->Mata_pelajaran_model->update_mata_pelajaran($where_edit, $edit);
			
			// $pelajaran_id 		= $this->input->post('pelajaran_id');
			// $aspek_pelajaran	= $this->input->post('aspek_pelajaran');
			
			// if ($aspek_pelajaran){
				// $this->Aspek_pelajaran_model->delete_aspek_pelajaran(array('aspek_pelajaran.pelajaran_id'=>$pelajaran_id));
				// foreach($aspek_pelajaran as $row){
					// $insert['aspek_id'] 				= $row;
					// $insert['pelajaran_id'] 			= $pelajaran_id;
					// $insert['aspek_pelajaran_tanggal'] 	= date('Y-m-d H:i:s');
					// $this->Aspek_pelajaran_model->insert_aspek_pelajaran($insert);
				// }
			// }
			
			$this->session->set_flashdata('success','Mata Pelajaran telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mata_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['pelajaran_id']	= validasi_sql($this->uri->segment(4));
		$this->Mata_pelajaran_model->delete_mata_pelajaran($where_delete);
		
		$this->session->set_flashdata('success','Mata Pelajaran telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['pelajaran_id']	= validasi_sql($this->uri->segment(4)); 
		$mata_pelajaran		= $this->Mata_pelajaran_model->get_mata_pelajaran('*', $where);
		
		$data['pelajaran_id']		= $mata_pelajaran->pelajaran_id;
		$data['pelajaran_kode']		= $mata_pelajaran->pelajaran_kode;
		$data['pelajaran_nama']		= $mata_pelajaran->pelajaran_nama;
		$data['pelajaran_sifat']	= $mata_pelajaran->pelajaran_sifat;
		$data['pelajaran_kelompok']	= $mata_pelajaran->pelajaran_kelompok;
		$data['pelajaran_peminatan']= $mata_pelajaran->pelajaran_peminatan;
		$data['pelajaran_urutan']	= $mata_pelajaran->pelajaran_urutan;
		$data['pelajaran_deskripsi']	= $mata_pelajaran->pelajaran_deskripsi;
		$data['pelajaran_status']	= $mata_pelajaran->pelajaran_status;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/mata_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function list_mata_pelajaran()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$data['mata_pelajaran']	= $this->Mata_pelajaran_model->grid_all_mata_pelajaran('*', 'pelajaran_nama', 'ASC');
		$this->load->view(module_dir().'/export/list_mata_pelajaran', $data);
	}
}
