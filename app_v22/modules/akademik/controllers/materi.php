<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Materi | ' . profile('profil_website');
		$this->active_menu		= 257;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Materi_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Mata_pelajaran_model');
    }
	
	function datatable()
	{
		$tingkat_id 	= validasi_sql($this->uri->segment(4));
		$pelajaran_id 	= validasi_sql($this->uri->segment(5));
		$where 			= "tingkat.tingkat_id = '{$tingkat_id}' AND mata_pelajaran.pelajaran_id = '{$pelajaran_id}'";
		
		$this->load->library('Datatables');
		$this->datatables->select('materi_id, materi_nama, tingkat_nama, pelajaran_nama')
		->add_column('Actions', $this->get_buttons('$1'),'materi_id')
		->search_column('materi_nama')
		->from('bahanajar_materi materi')
		->join('tingkat', 'materi.tingkat_id=tingkat.tingkat_id', 'left')
		->join('staf', 'materi.staf_id=staf.staf_id', 'left')
		->join('mata_pelajaran', 'materi.pelajaran_id=mata_pelajaran.pelajaran_id', 'left')
		->where($where);
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):'-';
		$data['pelajaran_id']	= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):'-';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/materi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$data['materi_nama']		= ($this->input->post('materi_nama'))?$this->input->post('materi_nama'):'';
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):'';
		$data['pelajaran_id']	= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):'';
		$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){			
			$insert['materi_nama']		= validasi_sql($this->input->post('materi_nama'));
			$insert['pelajaran_id']			= validasi_sql($this->input->post('pelajaran_id'));
			$insert['tingkat_id']		= validasi_sql($this->input->post('tingkat_id'));
			$insert['pelajaran_id']	= validasi_sql($this->input->post('pelajaran_id'));
			$insert['staf_id']			= validasi_sql($this->input->post('staf_id'));
			$this->Materi_model->insert_materi($insert);
			
			$this->session->set_flashdata('success','Materi telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/materi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['materi_id']		= validasi_sql($this->uri->segment(4)); 
		$materi 					= $this->Materi_model->get_materi('*', $where);

		$data['materi_id']		= ($this->input->post('materi_id'))?$this->input->post('materi_id'):$materi->materi_id;
		$data['materi_nama']		= ($this->input->post('materi_nama'))?$this->input->post('materi_nama'):$materi->materi_nama;
		$data['pelajaran_id']		= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):$materi->pelajaran_id;
		$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$materi->tingkat_id;
		$data['pelajaran_id']	= ($this->input->post('pelajaran_id'))?$this->input->post('pelajaran_id'):$materi->pelajaran_id;
		$data['staf_id']		= ($this->input->post('staf_id'))?$this->input->post('staf_id'):$materi->staf_id;
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$where_edit['materi_id']	= validasi_sql($this->input->post('materi_id'));
			$edit['materi_nama']		= validasi_sql($this->input->post('materi_nama'));
			$edit['pelajaran_id']		= validasi_sql($this->input->post('pelajaran_id'));
			$edit['tingkat_id']		= validasi_sql($this->input->post('tingkat_id'));
			$edit['pelajaran_id']	= validasi_sql($this->input->post('pelajaran_id'));
			$edit['staf_id']		= validasi_sql($this->input->post('staf_id'));
			$this->Materi_model->update_materi($where_edit, $edit);
			
			$this->session->set_flashdata('success','Materi telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2)));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/materi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$where_delete['materi_id']	= validasi_sql($this->uri->segment(4));
		$this->Materi_model->delete_materi($where_delete);
		
		$this->session->set_flashdata('success','Materi telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2)));
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['materi_id']	= validasi_sql($this->uri->segment(4)); 
		$materi		 		= $this->Materi_model->get_materi('*', $where);
		
		$data['materi_id']			= $materi->materi_id;
		$data['materi_nama']			= $materi->materi_nama;
		$data['tahun_nama']			= $materi->tahun_nama;
		$data['tingkat_nama']		= $materi->tingkat_nama;
		$data['pelajaran_nama']	= $materi->pelajaran_nama;
		$data['staf_nama']			= $materi->staf_nama;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/materi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}
