<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kelulusan_siswa extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	private $tahun_angkatan;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Kelulusan Siswa | ' . profile('profil_website');
		$this->active_menu		= 304;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Tahun_model');
		$this->load->model('Tingkat_model');
		$this->load->model('Kelas_model');
		$this->load->model('Siswa_model');
		$this->load->model('Siswa_kelas_model');
		$this->load->model('Alumni_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
		$this->tahun_angkatan	= $this->Tahun_model->get_tahun_aktif()->tahun_angkatan;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$tahun_ajaran_asal = $this->db->query("SELECT * FROM tahun_ajaran WHERE tahun_angkatan = '".($this->tahun_angkatan - 1)."'")->row();
		$data['tahun_asal_id']			= $tahun_ajaran_asal->tahun_kode;
		$data['tahun_asal_angkatan']	= $tahun_ajaran_asal->tahun_angkatan;
		
		$tahun_ajaran_tujuan = $this->db->query("SELECT * FROM tahun_ajaran WHERE tahun_angkatan = '".($tahun_ajaran_asal->tahun_angkatan + 1)."'")->row();
		$data['tahun_tujuan_id']		= ($tahun_ajaran_tujuan)?$tahun_ajaran_tujuan->tahun_kode:'';
		$data['tahun_tujuan_angkatan']	= ($tahun_ajaran_tujuan)?$tahun_ajaran_tujuan->tahun_angkatan:'';
		
		$data['tingkat_asal_id']		= ($this->input->post('tingkat_asal_id'))?$this->input->post('tingkat_asal_id'):'';
		$data['tingkat_tujuan_id']		= ($this->input->post('tingkat_tujuan_id'))?$this->input->post('tingkat_tujuan_id'):'';
		$data['kelas_id']			= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):'';
		
		if (userdata('departemen_id')){
			$data['departemen_id']		= userdata('departemen_id');
		} else {
			$departemen_id 				= $this->uri->segment(4);
			$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
		}
		
		$save	= $this->input->post('save');
		if ($save){
			
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/kelulusan_siswa', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function get_siswa_asal(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$tingkat_id = $this->input->post('tingkat');
		$kelas_id = $this->input->post('kelas');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun_ajaran("tahun_angkatan", array('tahun_kode'=>$tahun_kode));
			$tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_id));
			$kelas = $this->Kelas_model->get_kelas("*", array('kelas_id'=>$kelas_id, 'tahun_kode'=>$tahun_kode));
			if ($tahun){
				$siswa = $this->db->query("SELECT siswa.siswa_id, siswa_nis, siswa_nama 
										   FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id 
										   WHERE siswa_status = 'Siswa' 
												AND siswa_kelas.tahun_kode='".$tahun_kode."' 
												AND siswa_kelas.kelas_id='".$kelas_id."' 
												AND (SELECT COUNT(a.alumni_id) FROM alumni a 
													WHERE a.siswa_id=siswa_kelas.siswa_id) < 1
										   ORDER BY siswa_nama ASC")->result();
				if ($siswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $siswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data siswa tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tahun tersebut tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_alumni(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			if ($tahun){
				$alumni = $this->db->query("SELECT siswa.siswa_id, siswa.siswa_nis, siswa.siswa_nama FROM alumni LEFT JOIN siswa ON alumni.siswa_id=siswa.siswa_id WHERE alumni.alumni_tahun='".$tahun->tahun_angkatan."' ORDER BY siswa_nama ASC")->result();
				if ($alumni){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $alumni;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_alumni_filter(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$filter = $this->input->post('filter');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			if ($tahun){
				$alumni = $this->db->query("SELECT siswa.siswa_id, siswa.siswa_nis, siswa.siswa_nama FROM alumni LEFT JOIN siswa ON alumni.siswa_id=siswa.siswa_id WHERE alumni.alumni_tahun='".$tahun->tahun_angkatan."' AND (siswa_nama LIKE '%$filter%' OR siswa_nis LIKE '%$filter%') ORDER BY siswa_nama ASC")->result();
				if ($alumni){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $alumni;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_kelas(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		$tingkat_id = $this->input->post('tingkat');
		if ($tahun_kode && $tingkat_id){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			$tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_id));
			if ($tahun && $tingkat){
				$kelas = $this->Kelas_model->grid_all_kelas("kelas.kelas_id, kelas.kelas_nama", "kelas_nama", "ASC", 0, 0,array('kelas.tahun_kode'=>$tahun_kode, 'kelas.tingkat_id'=>$tingkat_id));
				if ($kelas){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $kelas;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function set_alumni(){
		$data = array();
		$siswa_id = $this->input->post('siswa');
		$tahun_asal_id = $this->input->post('tahun_asal');
		$tingkat_asal_id = $this->input->post('tingkat_asal');
		$kelas_asal_id = $this->input->post('kelas_asal');
		$tahun_tujuan_id = $this->input->post('tahun_tujuan');
		
		if ($siswa_id && $tahun_asal_id && $tingkat_asal_id && $kelas_asal_id && $tahun_tujuan_id){
			$get_tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_asal_id));
			$get_tahun = $this->Tahun_model->get_tahun_ajaran("*", array("tahun_kode"=>$tahun_tujuan_id));
			$get_kelas = $this->Kelas_model->get_kelas("*", array("kelas_id"=>$kelas_asal_id, "kelas.tahun_kode"=>$tahun_asal_id));
			$get_siswa = $this->Siswa_model->get_siswa("", array("siswa.siswa_id"=>$siswa_id));
			if ($get_tahun && $get_tingkat && $get_kelas && $get_siswa){
				$insert_alumni['siswa_id']			= $get_siswa->siswa_id;
				$insert_alumni['kelas_id']			= $get_kelas->kelas_id;
				$insert_alumni['alumni_tahun']		= $get_tahun->tahun_angkatan;
				$insert_alumni['alumni_tanggal']	= date('Y-m-d');
				$insert_alumni['created_at']		= date('Y-m-d H:i:s');
				$this->Alumni_model->insert_alumni($insert_alumni);
				
				$data['response']	= true;
				$data['message']	= "Data berhasil disimpan.";
				$data['params']		= array("siswa_id"=>$siswa_id,
											"tahun_asal_id"=>$tahun_asal_id,
											"tingkat_asal_id"=>$tingkat_asal_id,
											"kelas_asal_id"=>$kelas_asal_id,
											"tahun_tujuan_id"=>$tahun_tujuan_id);
			} else {
				$data['response']	= false;
				$data['message']	= "Data tahun, tingkat, kelas atau siswa tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function set_kelas(){
		$data = array();
		$tahun_asal_id = $this->input->post('tahun_asal');
		$tingkat_asal_id = $this->input->post('tingkat_asal');
		$kelas_asal_id = $this->input->post('kelas_asal');
		$tahun_tujuan_id = $this->input->post('tahun_tujuan');
		
		if ($tahun_asal_id && $tingkat_asal_id && $kelas_asal_id && $tahun_tujuan_id){
			$get_tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_asal_id));
			$get_tahun = $this->Tahun_model->get_tahun_ajaran("*", array("tahun_kode"=>$tahun_tujuan_id));
			$get_kelas = $this->Kelas_model->get_kelas("*", array("kelas_id"=>$kelas_asal_id, "kelas.tahun_kode"=>$tahun_asal_id));
			
			$siswa = $this->db->query("SELECT siswa.siswa_id, siswa_nis, siswa_nama 
										FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id 
										WHERE siswa_status = 'Siswa' 
											AND siswa_kelas.tahun_kode='".$tahun_asal_id."' 
											AND siswa_kelas.kelas_id='".$kelas_asal_id."' 
											AND (SELECT COUNT(sk.siswa_kelas_id) FROM siswa_kelas sk 
												LEFT JOIN tahun_ajaran t ON sk.tahun_kode=t.tahun_kode
												WHERE t.tahun_angkatan > '".($get_tahun->tahun_angkatan)."' AND sk.siswa_id=siswa_kelas.siswa_id) < 1
										ORDER BY siswa_nama ASC")->result();

			if ($get_tahun && $get_tingkat && $get_kelas && $siswa){
				foreach ($siswa as $row_siswa) {
					$insert_alumni['siswa_id']			= $row_siswa->siswa_id;
					$insert_alumni['kelas_id']			= $get_kelas->kelas_id;
					$insert_alumni['alumni_tahun']		= $get_tahun->tahun_angkatan;
					$insert_alumni['alumni_tanggal']	= date('Y-m-d');
					$insert_alumni['created_at']		= date('Y-m-d H:i:s');
					$this->Alumni_model->insert_alumni($insert_alumni);
				}
				$data['response']	= true;
				$data['message']	= "Data berhasil disimpan.";
				$data['params']		= array("tahun_asal_id"=>$tahun_asal_id,
											"tingkat_asal_id"=>$tingkat_asal_id,
											"kelas_asal_id"=>$kelas_asal_id,
											"tahun_tujuan_id"=>$tahun_tujuan_id);
			} else {
				$data['response']	= false;
				$data['message']	= "Data tahun, tingkat, kelas atau siswa tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function remove_siswa(){
		$data = array();
		$siswa_id = $this->input->post('siswa');
		$tahun_asal_id = $this->input->post('tahun_asal');
		$tingkat_asal_id = $this->input->post('tingkat_asal');
		$kelas_asal_id = $this->input->post('kelas_asal');
		$tahun_tujuan_id = $this->input->post('tahun_tujuan');
		
		if ($siswa_id && $tahun_asal_id && $tingkat_asal_id && $kelas_asal_id && $tahun_tujuan_id){
			$get_tingkat = $this->Tingkat_model->get_tingkat("*", array('tingkat_id'=>$tingkat_asal_id));
			$get_tahun = $this->Tahun_model->get_tahun_ajaran("*", array("tahun_kode"=>$tahun_tujuan_id));
			$get_kelas = $this->Kelas_model->get_kelas("*", array("kelas_id"=>$kelas_asal_id, "kelas.tahun_kode"=>$tahun_asal_id));
			$get_siswa = $this->Siswa_model->get_siswa("", array("siswa.siswa_id"=>$siswa_id));
			if ($get_tahun && $get_tingkat && $get_kelas && $get_siswa){
				$query_delete_alumni = $this->Alumni_model->delete_alumni(array('siswa_id'=>$siswa_id));
				
				$data['response']	= true;
				$data['message']	= "Data berhasil disimpan.";
				$data['params']		= array("siswa_id"=>$siswa_id,
											"tahun_asal_id"=>$tahun_asal_id,
											"tingkat_asal_id"=>$tingkat_asal_id,
											"kelas_asal_id"=>$kelas_asal_id,
											"tahun_tujuan_id"=>$tahun_tujuan_id);
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function get_tahun_tujuan(){
		$data = array();
		$tahun_kode = $this->input->post('tahun');
		if ($tahun_kode){
			$tahun = $this->Tahun_model->get_tahun_ajaran("*", array('tahun_kode'=>$tahun_kode));
			if ($tahun){
				$tahun_tujuan = $this->Tahun_model->grid_all_tahun_ajaran("*", "tahun_angkatan", "DESC", 0, 0,array('tahun_ajaran.tahun_angkatan > '=>$tahun->tahun_angkatan));
				if ($tahun_tujuan){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $tahun_tujuan;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}
