<html>
	<head>
		<title>DAFTAR MATA PELAJARAN</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
		<style>
		.table tr td,
		.table tr th {
			font-size: 13px;
			padding: 2px 5px !important;
			border: 1px solid black !important;
		}
		</style>
	</head>
	<body onload="window.print();">
		<p class="text-center" style="font-weight:bold;">DAFTAR MATA PELAJARAN</p>
		<table class="table table-bordered">
			<tr>
				<th width="30">NO</th>
				<th width="60">KODE</th>
				<th>NAMA</th>
				<th width="200">KELOMPOK</th>
				<th>PEMINATAN</th>
			</tr>
			<?php
			$i = 1;
			foreach($mata_pelajaran as $row){
			?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td><?php echo $row->pelajaran_kode?></td>
				<td><?php echo $row->pelajaran_nama?></td>
				<td><?php echo $row->pelajaran_kelompok?></td>
				<td><?php echo $row->pelajaran_peminatan?></td>
			</tr>
			<?php 
			$i++;
			}
			?>
		</table> 
	</body>
</html>	