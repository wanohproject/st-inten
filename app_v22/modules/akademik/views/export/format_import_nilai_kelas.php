<?php
if ($excel == TRUE){
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msexcel'); // Other browsers  
header("Content-Disposition: attachment;Filename=Nilai_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$pelajaran_nama))."_".str_replace(' ', '_', $kelas_nama)."_".date("YmdHis").".xls");
}
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=ProgId content=Excel.Sheet>
		<title>Format Nilai Siswa Kelas</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
	</head>
	<body>		
		<table border="1" style="border-collapse:collapse">
			<tr>
				<th colspan="4">DATA NILAI</th>
			</tr>
			<tr>
				<td colspan="2">Kelas</td>
				<td colspan="2" style="font-weight:bold;"><?php echo strtoupper($kelas_nama); ?></td>
			</tr>
			<tr>
				<td colspan="2">Semester</td>
				<td colspan="2" style="font-weight:bold;"><?php echo strtoupper($semester_nama); ?></td>
			</tr>
			<tr>
				<td colspan="2">Tahun Pelajaran</td>
				<td colspan="2" style="font-weight:bold;"><?php echo strtoupper($tahun_nama); ?></td>
			</tr>
			<tr>
				<td colspan="2">Mata Pelajaran</td>
				<td colspan="2" style="font-weight:bold;"><?php echo strtoupper($pelajaran_nama); ?></td>
			</tr>
			<tr>
				<td colspan="2">Guru Pengajar</td>
				<td colspan="2" style="font-weight:bold;"><?php echo strtoupper($guru_nama); ?></td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<th style="text-align:center" width="50">NO</th>
				<th style="text-align:center" width="100">NIS</th>
				<th style="text-align:center" width="350">NAMA</th>
				<th style="text-align:center" width="100">NILAI</th>
			</tr>
			<?php
			$i = 1;
			if ($grid_siswa_kelas){
				foreach($grid_siswa_kelas as $row){
				?>
				<tr>
					<td align="center"><?php echo $i; ?></td>
					<td align="center"><?php echo $row->siswa_nis?></td>
					<td><?php echo $row->siswa_nama?></td>
					<td>&nbsp;</td>
				</tr>
				<?php 
				$i++;
				}
			}
			?>
		</table> 
	</body>
</html>	