<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="Doc1_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2008-06-16T08:31:00Z</o:Created>
  <o:LastSaved>2008-06-16T08:31:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
 <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:Arial;}
@page Section1
	{
	size:595.0pt 842.0pt;
	margin:36.0pt 15.0pt 36.0pt 15.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
.style1 {
	color: #000000;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
}
.style2 {
	font-weight: bold;
	font-size: 13px;
	font-family: Arial, Helvetica, sans-serif;
	color: #000000;
}
.style3 {
	font-weight: bold;
	font-size: 18px;
	font-family: Arial, Helvetica, sans-serif;
	color: #000000;
}
.style5, .style5 p {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight:normal;}
.style13 {color: #000000}
.style14 {color: #FFFFFF}
.style17 {color: #FFFFFF; font-weight: bold; font-family: Arial, Helvetica, sans-serif; font-size: 13px; }
.style20 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; }
.style21 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style22 {font-family: Arial, Helvetica, sans-serif}
.style24 {
	font-size: 13px;
	color: #FFFFFF;
}
.style27 {color: #FFFFFF; font-weight: bold; font-family: Arial, Helvetica, sans-serif; font-size: 13px; }
.style54 {font-size: 12px;}
.tandatangan {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
.style-table01 { padding: 0px 5px 0px 5px;}
.text-center { text-align: center;}
table {border-collapse:collapse;font-family:Arial, Helvetica, sans-serif; font-size:12px;}
table td {font-family:Arial, Helvetica, sans-serif; font-size:12px;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:Arial;
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>
<!-- <body lang=EN-US style='tab-interval:36.0pt'> -->
<body lang=EN-US style='tab-interval:36.0pt' onload="window.print();">
<?php
$query_kkm = $this->db->query("SELECT nilai_kkm 
								FROM nilai 
								WHERE siswa_id = '{$siswa_id}'
									AND tahun_kode = '{$tahun_kode}'
									AND semester_id = '{$semester_id}'")->row();
$global_kkm	 = ($query_kkm)?$query_kkm->nilai_kkm:'';

$tingkat_nama	= $this->db->query("SELECT * FROM tingkat WHERE tingkat_id = '{$tingkat_id}'")->row()->tingkat_romawi;
$tingkat_akhir	= $this->db->query("SELECT * FROM tingkat WHERE tingkat_id = '{$tingkat_id}'")->row()->tingkat_akhir;
$trim_kelas = explode(" ", $siswa->kelas_nama);
$new_semester = "";
if ($semester_id == 1){
	$new_semester = "1 (Satu)";
} else {
	$new_semester = "2 (Dua)";
}

?>
<div class=Section1>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
<tr>
	<td colspan="2"><div align="center" class="style3"><strong>LAPORAN HASIL CAPAIAN KOMPETENSI<br />TAHSIN, TAHFIDZ, TADABBUR AL-QURAN (T3Q)<br /><?php echo strtoupper($nama_sekolah); ?></strong></div></td>
</tr>
<tr>
	<td colspan="2" valign="top" style="padding: 0px;padding-bottom:20px;">
	<br />
	<br />
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="65%" valign="top">
				<table width="100%" border="0" cellpadding="1" cellspacing="0">
					
					<tr>
						<td><span class="style5">Nama Sekolah</span></td>
						<td><span class="style5">:</span></td>
						<td><span class="style5"><?=$nama_sekolah?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Alamat Sekolah</span></td>
						<td><span class="style5">:</span></td>
						<td><span class="style5"><?=$alamat_sekolah?></span></td>
					</tr>
					<tr>
						<td width="130"><span class="style5">Nama Peserta Didik</span></td>
						<td width="10"><span class="style5">:</span></td>
						<td><span class="style5"><?=$siswa->siswa_nama?></span></td>
					</tr>
					<tr>
						<td><span class="style5">No. Induk / NISN</span></td>
						<td><span class="style5">:</span></td>
						<td><span class="style5"><?=$siswa->siswa_nis?> / <?=$siswa->siswa_nisn?></span></td>
					</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="1" cellspacing="0">
					<tr>
						<td width="50%"><span class="style5">Kelas</span></td>
						<td width="10"><span class="style5">:</span></td>
						<td width="50%"><span class="style5"><?=$siswa->kelas_nama?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Semester</span></td>
						<td><span class="style5">:</span></td>
						<td><span class="style5"><?=$new_semester?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Tahun Pelajaran</span></td>
						<td><span class="style5">:</span></td>
						<td><span class="style5"><?=$siswa->tahun_nama?></span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td colspan="2"><div align="left" class="style2"><strong>A. CAPAIAN KOMPETENSI</strong></div></td>
</tr>
<tr>
<td colspan="2" style="padding:5px 0px 20px 0px;">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table width="100%" border="1" class="tab" bordercolor="#000000">
				<tr height="30">
					<th class="headerlong" width="30"><div align="center">No.</div></th>
					<th class="headerlong"><div align="center">Program</div></th>
					<th class="headerlong" width="100"><div align="center">Angka</div></th>
					<th class="headerlong" width="100"><div align="center">Predikat</div></th>
				</tr>
				<?php
				$rata = 0;
				$grid_mapel = $this->db->query("SELECT mata_pelajaran.pelajaran_id, pelajaran_nama
												FROM mata_pelajaran 
												WHERE pelajaran_status = 'A' 
													AND pelajaran_kelompok = 'Kelompok D (Unggulan)' 
													AND pelajaran_sifat = 'Wajib' 
												GROUP BY mata_pelajaran.pelajaran_id 
												ORDER BY pelajaran_urutan ASC")->result();
														
				$i_mapel = 1;
				$i_rata = 0;
				foreach($grid_mapel as $row_mapel){
					$get_nilai_ki3 = $this->db->query("SELECT *
														FROM nilai
														WHERE nilai.siswa_id='{$siswa_id}'
															AND nilai.aspek_id=1
															AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
															AND nilai.tahun_kode='{$tahun_kode}'
															AND nilai.komponen_id IS NULL
															AND nilai.semester_id='{$semester_id}'")->row();
					
					$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
					$nilai_angka_ki3 = ($get_nilai_ki3 && $get_nilai_ki3->nilai_angka > 0)?$get_nilai_ki3->nilai_angka:'-';
					$nilai_huruf_ki3 = ($get_nilai_ki3 && $get_nilai_ki3->nilai_angka > 0)?konversi_grade($tahun_kode, $get_nilai_ki3->nilai_angka):'-';
					if ($nilai_angka_ki3 != '-'){
						$i_rata++;
						$rata = $rata + $nilai_angka_ki3;
					}
					$get_deskripsi_ki3 = $this->db->query("SELECT aspek_penilaian.*, deskripsi_nilai.nilai_deskripsi
															FROM deskripsi_nilai 
																LEFT JOIN aspek_penilaian ON deskripsi_nilai.aspek_id=aspek_penilaian.aspek_id
															WHERE deskripsi_nilai.tahun_kode = '{$tahun_kode}' 
																AND deskripsi_nilai.siswa_id = '{$siswa_id}' 
																AND deskripsi_nilai.semester_id = '{$semester_id}' 
																AND deskripsi_nilai.pelajaran_id = '{$row_mapel->pelajaran_id}'
																AND deskripsi_nilai.aspek_id = 3
																AND deskripsi_nilai.komponen_id IS NULL
															GROUP BY deskripsi_nilai.aspek_id
															ORDER BY aspek_penilaian.aspek_kode ASC")->row();

					$deskripsi_ki3 = ($get_deskripsi_ki3)?$get_deskripsi_ki3->nilai_deskripsi:'-';
					
					echo "<tr height=\"25\">
						<td style=\"padding:3px 5px;\" class=\"text-center\">".$i_mapel."</td>
						<td style=\"padding:3px 5px;\">".$row_mapel->pelajaran_nama."</td>
						<td style=\"padding:3px 5px;\" class=\"text-center\">".$nilai_angka_ki3."</td>
						<td style=\"padding:3px 5px;\" class=\"text-center\">".$nilai_huruf_ki3."</td>
					</tr>";
					$i_mapel++;
				}
				$rata = ($i_rata > 0)?$rata / $i_rata:0;
				?>
				<tr height="30">
					<th colspan="2" class="text-center">RATA- RATA NILAI</th>
					<th colspan="2" class="text-center"><?php echo ($rata); ?></th>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>

<tr>
<td colspan="2" style="padding:5px 0px 20px 0px;">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<?php
		$rata = 0;
		$grid_mapel = $this->db->query("SELECT mata_pelajaran.pelajaran_id, pelajaran_nama
										FROM mata_pelajaran 
										WHERE pelajaran_status = 'A' 
											AND pelajaran_kelompok = 'Kelompok D (Unggulan)' 
											AND pelajaran_sifat = 'Wajib' 
										GROUP BY mata_pelajaran.pelajaran_id 
										ORDER BY pelajaran_urutan ASC")->result();
												
		$i_mapel = 1;
		$i_rata = 0;
		$x = "B";
		foreach($grid_mapel as $row_mapel){
			$get_deskripsi_ki3 = $this->db->query("SELECT aspek_penilaian.*, deskripsi_nilai.nilai_deskripsi
													FROM deskripsi_nilai 
														LEFT JOIN aspek_penilaian ON deskripsi_nilai.aspek_id=aspek_penilaian.aspek_id
													WHERE deskripsi_nilai.tahun_kode = '{$tahun_kode}' 
														AND deskripsi_nilai.siswa_id = '{$siswa_id}' 
														AND deskripsi_nilai.semester_id = '{$semester_id}' 
														AND deskripsi_nilai.pelajaran_id = '{$row_mapel->pelajaran_id}'
														AND deskripsi_nilai.aspek_id = 3
														AND deskripsi_nilai.komponen_id IS NULL
													GROUP BY deskripsi_nilai.aspek_id
													ORDER BY aspek_penilaian.aspek_kode ASC")->row();

			$deskripsi_ki3 = ($get_deskripsi_ki3)?$get_deskripsi_ki3->nilai_deskripsi:'-';
			
			echo "
			<strong style=\"padding-left:0px;\">$x. CATATAN ".strtoupper($row_mapel->pelajaran_nama)."</strong>
			<table width=\"100%\" border=\"1\" class=\"tab\" bordercolor=\"#000000\">
			<tr height=\"25\">
				<td style=\"padding:3px 5px;\">".$deskripsi_ki3."</td>
			</tr>
			</table><br />";
			$i_mapel++;
			$x++;
		}
		$rata = ($i_rata > 0)?$rata / $i_rata:0;
		?>
		</td>
	</tr>
</table>
</td>
</tr>
<tr>
<td class="style2" colspan="2">
<table width="90%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top">
		<br />
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="tandatangan">
			<tr>
				<td style="padding-top:0px;" width="40%"><div align="left"><br />Mengetahui:<br />KEPALA SEKOLAH,</div></td>
				<td style="padding-top:0px;">&nbsp;</td>
				<td style="padding-top:0px;" width="35%">Bandung, <?php echo dateIndo_(($tanggal_raport)?$tanggal_raport:date('Y-m-d')); ?><br />GURU TAHFIDZ</td>
			</tr>
			<tr>
				<td colspan="3"><br /><br />&nbsp;</td>
			</tr>
			<tr>
				<td><strong>( <?=($kepala_sekolah)?$kepala_sekolah->staf_nama:'';?> )</strong><br />NIK. <?=($kepala_sekolah)?$kepala_sekolah->staf_nip:'';?></td>
				<td>&nbsp;</td>
				<td><strong>( <?=$kelas->staf_nama?> )</strong></td>
			</tr>
		</table>
		<br />
		</td>
	</tr>
</table>
</td>
</tr>
</table>

</div>
<!-- /////////////////////////////////////////////////////////////////////////////// -->
</body>
</html>