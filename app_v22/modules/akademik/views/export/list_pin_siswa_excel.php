<?php
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msexcel'); // Other browsers
header("Content-Disposition: attachment;Filename=Data_PIN_Siswa.xls");
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<html>
	<head>
		<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
		<meta name=ProgId content=Excel.Sheet>
		<title>DAFTAR PIN SISWA</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
		<style>
		.table tr td,
		.table tr th {
			font-size: 13px;
			padding: 2px 5px !important;
			border: 1px solid black !important;
		}
		.num {
		mso-number-format:General;
		}
		.text{
		mso-number-format:"\@";/*force text*/
		}
		</style>
	</head>
	<body>
		<h2 class="text-center">DAFTAR USER SISWA</h2>
		<hr />
		<table class="table table-bordered" border="1">
			<tr>
				<th style="text-align:center !important" rowspan="2" width="50">NO</th>
				<th style="text-align:center !important" rowspan="2" width="80">NIS</th>
				<th style="text-align:center !important" rowspan="2">NAMA</th>
				<th style="text-align:center !important" rowspan="2" width="80">USER</th>
				<th style="text-align:center !important" colspan="2">PIN</th>
				<th style="text-align:center !important" rowspan="2" width="80">KELAS</th>
			</tr>
			<tr>
				<th style="text-align:center !important" width="80">SISWA</th>
				<th style="text-align:center !important" width="80">ORANGTUA</th>
			</tr>
			<?php
			$i = 1;
			if ($siswa){
				foreach($siswa as $row){
				?>
				<tr>
					<td align="center"><?php echo $i; ?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_nis?></td>
					<td><?php echo $row->siswa_nama?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_user?></td>
					<td style="text-align:center !important" class="text"><?php echo $row->siswa_pin?></td>
					<td style="text-align:center !important" class="text"><?php echo $row->siswa_pin_orangtua?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_kelas_sekarang?></td>
				</tr>
				<?php 
				$i++;
				}
			}
			?>
		</table> 
	</body>
</html>	