<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="Doc1_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2008-06-16T08:31:00Z</o:Created>
  <o:LastSaved>2008-06-16T08:31:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
 <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
table { page-break-inside:auto }
tr    { page-break-inside:avoid; page-break-after:auto }
thead { display:table-header-group }
tfoot { display:table-footer-group }
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:609.0pt 935.0pt;
	margin:36.0pt 15.0pt 36.0pt 15.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
.style1 {
	color: #000000;
	font-weight: bold;
	font-family: "Times New Roman", Helvetica, sans-serif;
	font-size: 13px;
}
.style2 {
	font-weight: bold;
	font-size: 13px;
	font-family: "Times New Roman", Helvetica, sans-serif;
	color: #000000;
}
.style5, .style5 p {font-family: "Times New Roman", Helvetica, sans-serif; font-size: 12px; font-weight:bold;}
.style13 {color: #000000}
.style14 {color: #FFFFFF}
.style17 {color: #FFFFFF; font-weight: bold; font-family: "Times New Roman", Helvetica, sans-serif; font-size: 13px; }
.style20 {font-family: "Times New Roman", Helvetica, sans-serif; font-weight: bold; font-size: 13px; }
.style21 {
	font-family: "Times New Roman", Helvetica, sans-serif;
	font-weight: bold;
}
.style22 {font-family: "Times New Roman", Helvetica, sans-serif}
.style24 {
	font-size: 13px;
	color: #FFFFFF;
}
.style27 {color: #FFFFFF; font-weight: bold; font-family: "Times New Roman", Helvetica, sans-serif; font-size: 13px; }
.style54 {font-size: 12px;}
.tandatangan {font-family: "Times New Roman", Helvetica, sans-serif; font-size: 12px; }
.style-table01 { padding: 0px 5px 0px 5px;}
.text-center { text-align: center;}
table {border-collapse:collapse;font-family:"Times New Roman", Helvetica, sans-serif; font-size:12px;}
table td {font-family:"Times New Roman", Helvetica, sans-serif; font-size:12px;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>
<body lang=EN-US style='tab-interval:36.0pt' onload="window.print();">
<!-- <body lang=EN-US style='tab-interval:36.0pt'> -->
<?php
$where_antar['antar_mata_pelajaran.siswa_id']		= $siswa_id;
$where_antar['mata_pelajaran.pelajaran_sifat']		= "Tambahan";
$where_antar['antar_mata_pelajaran.tahun_kode']		= $tahun_kode;
$where_antar['antar_mata_pelajaran.semester_id']	= $semester_id;
$dataAMP = $this->Antar_mata_pelajaran_model->get_antar_mata_pelajaran("", $where_antar);

$dsEkskul 			= ($dataAMP)?json_decode($dataAMP->ekstrakurikuler):'';
$dsAbsensi 			= ($dataAMP)?json_decode($dataAMP->presensi):'';
$dsPrestasi 		= ($dataAMP)?json_decode($dataAMP->prestasi):'';
$CatatanWaliKelas 	= ($dataAMP)?$dataAMP->catatan_walikelas:'';
$CatatanOrangTua 	= ($dataAMP)?$dataAMP->catatan_orangtua:'';
$query_kkm = $this->db->query("SELECT nilai_kkm 
								FROM nilai 
								WHERE siswa_id = '{$siswa_id}'
									AND tahun_kode = '{$tahun_kode}'
									AND semester_id = '{$semester_id}'")->row();
$global_kkm	 = ($query_kkm)?$query_kkm->nilai_kkm:'';

$tingkat_nama	= $this->db->query("SELECT * FROM tingkat WHERE tingkat_id = '{$tingkat_id}'")->row()->tingkat_romawi;
$trim_kelas = explode(" ", $siswa->kelas_nama);
$new_semester = "";
if ($semester_id == 1){
	$new_semester = "1 (Satu)";
} else {
	$new_semester = "2 (Dua)";
}
// if ($tingkat_nama == "VII" && $semester_id == 1){
// 	$new_semester = "1 (Satu)";
// } else if ($tingkat_nama == "VII" && $semester_id == 2){
// 	$new_semester = "2 (Dua)";
// } else if ($tingkat_nama == "VIII" && $semester_id == 1){
// 	$new_semester = "3 (Tiga)";
// } else if ($tingkat_nama == "VIII" && $semester_id == 2){
// 	$new_semester = "4 Empat)";
// } else if ($tingkat_nama == "IX" && $semester_id == 1){
// 	$new_semester = "5 (Lima)";
// } else if ($tingkat_nama == "IX" && $semester_id == 2){
// 	$new_semester = "6 (Enam)";
// } else if ($tingkat_nama == "VII" && $semester_id == 1){
// 	$new_semester = "1 (Satu)";
// } else if ($tingkat_nama == "VII" && $semester_id == 2){
// 	$new_semester = "2 (Dua)";
// } else if ($tingkat_nama == "VIII" && $semester_id == 1){
// 	$new_semester = "3 (Tiga)";
// } else if ($tingkat_nama == "VIII" && $semester_id == 2){
// 	$new_semester = "4 (Empat)";
// } else if ($tingkat_nama == "IX" && $semester_id == 1){
// 	$new_semester = "5 (Lima)";
// } else if ($tingkat_nama == "IX" && $semester_id == 2){
// 	$new_semester = "6 (Enam)";
// }

?>
<div class=Section1>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
<tr>
	<td colspan="2"><div align="center" class="style2"><strong>LAPORAN KEBERLANJUTAN HASIL BELAJAR SISWA</strong></div><br /></td>
</tr>
<tr>
	<td colspan="2" valign="top" style="padding: 0px;padding-bottom:20px;">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:1px solid;">
		<tr>
			<td width="70%" valign="top">
				<table width="100%" border="0" cellpadding="1" cellspacing="0">
					<tr>
						<td width="30%"><span class="style5">Nama Sekolah</span></td>
						<td width="70%"><span class="style5">:&nbsp;<?=$nama_sekolah?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Alamat</span></td>
						<td><span class="style5">:&nbsp;<?=$alamat_sekolah?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Nama</span></td>
						<td><span class="style5">:&nbsp;<?=$siswa->siswa_nama?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Nomor Induk / NISN</span></td>
						<td><span class="style5">:&nbsp;<?=$siswa->siswa_nis?> / <?=$siswa->siswa_nisn?></span></td>
					</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="1" cellspacing="0">
					<tr>
						<td width="50%"><span class="style5">Kelas</span></td>
						<td width="50%"><span class="style5">:&nbsp;<?=$siswa->kelas_nama?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Semester</span></td>
						<td><span class="style5">:&nbsp;<?=$new_semester?></span></td>
					</tr>
					<tr>
						<td><span class="style5">Tahun Pelajaran</span></td>
						<td><span class="style5">:&nbsp;<?=$siswa->tahun_nama?></span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td class="style2">A. </td>
	<td class="style2">PENGETAHUAN DAN KETERAMPILAN</td>
</tr>
<tr>
<td class="style2">&nbsp;</td>
<td style="padding:5px 0px 20px 0px;">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<?php
		$q_aspek = $this->db->query("SELECT * 
										FROM aspek_pelajaran 
											LEFT JOIN aspek_penilaian ON aspek_pelajaran.aspek_id=aspek_penilaian.aspek_id
										WHERE aspek_pelajaran.tahun_kode = '{$tahun_kode}'
											AND aspek_pelajaran.aspek_id IN (1, 2)
											AND aspek_pelajaran_status = 'A'
											-- AND (SELECT COUNT(nilai_id) FROM nilai WHERE nilai.tahun_kode=aspek_pelajaran.tahun_kode AND nilai.aspek_id=aspek_pelajaran.aspek_id) > 0
										GROUP BY aspek_penilaian.aspek_id
										ORDER BY aspek_penilaian.aspek_kode ASC");
		$grid_aspek = $q_aspek->result();
		$count_aspek = $q_aspek->num_rows();

		$listAspek = array();
		$listKomponen = array();
		$listNilai = array();
		$iAspek = 0;
		$iKomponen = 0;
		$iNilai = 0;
		foreach ($grid_aspek as $row_aspek) {
			$listAspek[$iAspek]['aspek_id']		= $row_aspek->aspek_id;
			$listAspek[$iAspek]['aspek_kode']	= $row_aspek->aspek_kode;
			$listAspek[$iAspek]['aspek_nama']	= $row_aspek->aspek_nama;

			$nAspekColspan = 0;
			$q_komponen = $this->db->query("SELECT * 
												FROM komponen_pelajaran 
													LEFT JOIN komponen_penilaian ON komponen_pelajaran.komponen_id=komponen_penilaian.komponen_id
												WHERE komponen_pelajaran.tahun_kode = '{$tahun_kode}'
													AND komponen_pelajaran.semester_id = '{$semester_id}'
													AND komponen_pelajaran.tingkat_id = '{$tingkat_id}'
													AND komponen_pelajaran.aspek_id = '{$row_aspek->aspek_id}'
													AND komponen_pelajaran_status = 'A'
													-- AND (SELECT COUNT(nilai_id) 
													-- 		FROM nilai 
													-- 			LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id 
													-- 		WHERE nilai.tahun_kode = siswa_kelas.tahun_kode 
													-- 			AND nilai.siswa_id = siswa_kelas.siswa_id 
													-- 			AND nilai.tahun_kode=komponen_pelajaran.tahun_kode 
													-- 			AND nilai.semester_id=komponen_pelajaran.semester_id 
													-- 			AND siswa_kelas.kelas_id='{$kelas_id}' 
													-- 			AND nilai.aspek_id=komponen_pelajaran.aspek_id 
													-- 			AND nilai.komponen_id=komponen_pelajaran.komponen_id) > 0
												GROUP BY komponen_penilaian.komponen_id
												ORDER BY komponen_penilaian.komponen_urutan ASC");

			$grid_komponen = $q_komponen->result();
			$count_komponen = $q_komponen->num_rows();

			if ($count_komponen > 0){
				foreach ($grid_komponen as $row_komponen) {
					$listKomponen[$iKomponen]['komponen_id']			= $row_komponen->komponen_id;
					$listKomponen[$iKomponen]['komponen_kode']			= $row_komponen->komponen_kode;
					$listKomponen[$iKomponen]['komponen_nama']			= $row_komponen->komponen_nama;
					$listKomponen[$iKomponen]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
					$listKomponen[$iKomponen]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
					
					$nKomponenColspan = 0;
					$q_nilai = $this->db->query("SELECT * 
														FROM nilai 
															LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id
														WHERE nilai.tahun_kode = siswa_kelas.tahun_kode
															AND nilai.siswa_id = siswa_kelas.siswa_id
															and nilai.tahun_kode = '{$tahun_kode}'
															AND nilai.semester_id = '{$semester_id}'
															AND siswa_kelas.kelas_id = '{$kelas_id}'
															AND nilai.aspek_id = '{$row_aspek->aspek_id}'
															AND nilai.komponen_id = '{$row_komponen->komponen_id}'
															AND nilai_akhir = 'N'
														GROUP BY nilai.nilai_urutan
														ORDER BY nilai.nilai_urutan ASC");

					$grid_nilai = $q_nilai->result();
					$count_nilai = $q_nilai->num_rows();

					if ($count_nilai){
						foreach ($grid_nilai as $row_nilai) {
							$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
							$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
							$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
							$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
							$listNilai[$iNilai]['nilai_urutan']			= $row_nilai->nilai_urutan;
							$listNilai[$iNilai]['kompetensi_id']		= $row_nilai->kompetensi_id;
							$listNilai[$iNilai]['nilai_kode']			= "VALUE";
							$iNilai++;
							$nAspekColspan++;
							$nKomponenColspan++;
						}
						if ($row_komponen->komponen_kompetensi == 'Y'){
							$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
							$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
							$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
							$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
							$listNilai[$iNilai]['nilai_urutan']			= "RATA<u>2</u>";
							$listNilai[$iNilai]['kompetensi_id']		= "0";
							$listNilai[$iNilai]['nilai_kode']			= "NA-K";
							$iNilai++;
							$nAspekColspan++;
							$nKomponenColspan++;
						}
					} else {
						$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
						$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
						$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
						$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
						$listNilai[$iNilai]['nilai_urutan']			= "NULL";
						$listNilai[$iNilai]['kompetensi_id']		= "0";
						$listNilai[$iNilai]['nilai_kode']			= "NULL";
						$iNilai++;
						$nAspekColspan++;
						$nKomponenColspan++;
					}
					$listKomponen[$iKomponen]['komponen_colspan']	= $nKomponenColspan;
					$iKomponen++;
				}
			} else {
				$listKomponen[$iKomponen]['komponen_id']			= "NULL";
				$listKomponen[$iKomponen]['komponen_kode']			= "NULL";
				$listKomponen[$iKomponen]['komponen_nama']			= "NULL";
				$listKomponen[$iKomponen]['komponen_persentase']	= "NULL";
				$listKomponen[$iKomponen]['komponen_colspan']		= 1;
				$iKomponen++;
			}

			$listAspek[$iAspek]['aspek_colspan']	= $nAspekColspan;
			$iAspek++;
		}
		?>
			<table width="100%" border="1" class="tab" bordercolor="#000000">
				<tr>
					<th rowspan="3" width="10" style="vertical-align:middle">No</th>
					<th rowspan="3" style="vertical-align:middle">Mata Pelajaran</th>
					<th rowspan="3" width="40" style="vertical-align:middle;text-align:center">KKM</th>
					<?php if ($listAspek){ 
						foreach ($listAspek as $row_aspek){
							echo "<th style=\"text-align:center\" colspan=\"".$row_aspek['aspek_colspan']."\">".$row_aspek['aspek_nama']."</th>";
						}
					}?>
				</tr>
				<tr>
					<?php if ($listKomponen){ 
						foreach ($listKomponen as $row_komponen){
							if ($row_komponen['komponen_id'] == 'NA' || $row_komponen['komponen_colspan'] == '1' || $row_komponen['komponen_kompetensi'] == 'N'){
								echo "<th style=\"text-align:center;vertical-align:middle;\" width=\"40\" rowspan=\"2\" colspan=\"".$row_komponen['komponen_colspan']."\">".$row_komponen['komponen_kode']."</th>";
							} else {
								echo "<th style=\"text-align:center;\" colspan=\"".$row_komponen['komponen_colspan']."\">".$row_komponen['komponen_kode']."</th>";
							}
						}
					}?>
				</tr>
				<tr>
					<?php if ($listNilai){ 
						foreach ($listNilai as $row_nilai){
							if (($row_nilai['nilai_kode'] == 'VALUE' && $row_nilai['komponen_kompetensi'] == 'Y') || ($row_nilai['nilai_kode'] == 'NA-K' && $row_nilai['komponen_kompetensi'] == 'Y')){
								echo "<th style=\"text-align:center\" width=\"40\">".$row_nilai['nilai_urutan']."</th>";
							}
						}
					}?>
				</tr>
				<?php				
				$grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
													FROM guru_pelajaran 
														LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
													WHERE guru_pelajaran_status = 'A' 
														AND tahun_kode = '{$tahun_kode}' 
														AND pelajaran_status = 'A' 
														AND pelajaran_sifat = 'Wajib' 
														AND pelajaran_kelompok != 'Kelompok D (Unggulan)'
													GROUP BY pelajaran_kelompok 
													ORDER BY pelajaran_kelompok ASC")->result();
				
				foreach($grid_kelompok as $row_kelompok){
					echo "<tr><td colspan=\"11\" height=\"25\"><div style=\"font-weight:bold;\">".$row_kelompok->pelajaran_kelompok."</div></td></tr>";
					if ($row_kelompok->pelajaran_peminatan){
						$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
															FROM guru_pelajaran 
																LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
															WHERE guru_pelajaran_status = 'A' 
																AND tahun_kode = '{$tahun_kode}' 
																AND pelajaran_status = 'A' 
																AND pelajaran_sifat = 'Wajib' 
																AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																GROUP BY pelajaran_peminatan
																ORDER BY peminatan.peminatan_urutan ASC")->result();
						$j = 1;
						foreach($grid_peminatan as $row_peminatan){
							$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
															FROM guru_pelajaran 
																LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
															WHERE guru_pelajaran_status = 'A'  
																AND tahun_kode = '{$tahun_kode}'
																AND guru_pelajaran.kelas_id = '{$kelas_id}'
																AND pelajaran_status = 'A' 
																AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																AND pelajaran_sifat = 'Wajib' 
															GROUP BY guru_pelajaran.pelajaran_id 
															ORDER BY pelajaran_urutan ASC")->result();
							if ($grid_mapel){
								echo "<tr height=\"20\"><td align=\"left\" colspan=\"11\" style=\"font-weight:bold;\">".konversi_romawi($j).'. '.$row_peminatan->pelajaran_peminatan."</td></tr>";
																					
								$i_mapel = 1;
								foreach($grid_mapel as $row_mapel){
									$where_kkm['aspek_pelajaran.tahun_kode']		= $tahun_kode;
									$where_kkm['aspek_pelajaran.semester_id']	= $semester_id;
									$where_kkm['aspek_pelajaran.tingkat_id']	= $tingkat_id;
									$where_kkm['aspek_pelajaran.pelajaran_id']	= $row_mapel->pelajaran_id;
									$where_kkm['aspek_penilaian.aspek_kode']	= "KI 3";
									$kkm = $this->Aspek_pelajaran_model->get_aspek_pelajaran("*", $where_kkm);
									$nilai_kkm = ($kkm)?$kkm->aspek_pelajaran_kkm:0;

									echo "<tr>
										<td class=\"text-center\">".$i_mapel."</td>
										<td><strong>".$row_mapel->pelajaran_nama."</strong></td>
										<td class=\"text-center\">".$nilai_kkm."</td>";
										
										$NA_A = 0;
										$NA_K = 0;
										$n_k = 0;
										$n_a = 0;
										
										$deskripsi_tuntas = array();
										$deskripsi_lampaui = array();
										$deskripsi_belum = array();

										if ($listNilai){ 
											foreach ($listNilai as $row2){
												$where_persentase['komponen_pelajaran.tahun_kode']	= $tahun_kode;
												$where_persentase['komponen_pelajaran.semester_id']	= $semester_id;
												$where_persentase['komponen_pelajaran.pelajaran_id']= $row_mapel->pelajaran_id;
												$where_persentase['komponen_pelajaran.tingkat_id']	= $tingkat_id;
												$where_persentase['komponen_pelajaran.aspek_id']	= $row2['aspek_id'];
												$where_persentase['komponen_pelajaran.komponen_pelajaran_persentase >']	= 0;
												$persentase = $this->Komponen_pelajaran_model->get_komponen_pelajaran("SUM(komponen_pelajaran_persentase) AS total_persentase, COUNT(komponen_pelajaran_persentase) AS jumlah_persentase", $where_persentase);
												$total_persentase = ($persentase)?$persentase->total_persentase:0;
												$jumlah_persentase = ($persentase)?$persentase->jumlah_persentase:0;
												
												$where = array();
												$where['nilai.siswa_id']		= $siswa_id;
												$where['nilai.pelajaran_id']	= $row_mapel->pelajaran_id;
												$where['nilai.tahun_kode']		= $tahun_kode;
												$where['nilai.semester_id']		= $semester_id;
												$where['nilai.aspek_id']		= $row2['aspek_id'];
												$where['nilai.komponen_id']		= $row2['komponen_id'];
												$where['nilai.nilai_urutan']	= $row2['nilai_urutan'];
												$nilai = $this->Nilai_model->get_nilai("*", $where);
												$nilai_angka = ($nilai)?$nilai->nilai_angka:'-';

												if ($nilai && $row2['komponen_kompetensi'] == "Y" && $row2['nilai_kode'] == "VALUE"){
													$NA_K = $NA_K + $nilai_angka;
													$n_k++;
													echo "<td width=\"40\" style=\"text-align:center;\">".$nilai_angka."</td>";
												} else if ($row2['komponen_kompetensi'] == "N" && $row2['nilai_kode'] == "VALUE"){
													$NA_A = $NA_A + ($nilai_angka * ($row2['komponen_persentase'] / $total_persentase));
													$n_a++;
													echo "<td width=\"40\" style=\"text-align:center;\">".$nilai_angka."</td>";
												} else if ($row2['nilai_kode'] == "NA-K"){
													$NA_K = ($n_k > 0 && $NA_K > 0)?$NA_K / $n_k:'-';
													$NA_A = $NA_A + ($NA_K * ($row2['komponen_persentase'] / $total_persentase));
													echo "<td width=\"40\" style=\"text-align:center;\">".$NA_K."</td>";
													$n_a++;
													$n_k = 0;
													$NA_K = 0;
												} else {
													echo "<td width=\"40\" style=\"text-align:center;\">-</td>";
												}
											}
										}
									echo "</tr>";
									$i_mapel++;
								}
								$j++;
							}
						}
					} else {
						$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
														FROM guru_pelajaran 
															LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
															LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
														WHERE guru_pelajaran_status = 'A'  
															AND tahun_kode = '{$tahun_kode}'
															AND guru_pelajaran.kelas_id = '{$kelas_id}'
															AND pelajaran_status = 'A' 
															AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
															AND pelajaran_sifat = 'Wajib' 
														GROUP BY guru_pelajaran.pelajaran_id 
														ORDER BY pelajaran_urutan ASC")->result();
														
						$i_mapel = 1;
						foreach($grid_mapel as $row_mapel){
							$where_kkm['aspek_pelajaran.tahun_kode']		= $tahun_kode;
							$where_kkm['aspek_pelajaran.semester_id']	= $semester_id;
							$where_kkm['aspek_pelajaran.tingkat_id']	= $tingkat_id;
							$where_kkm['aspek_pelajaran.pelajaran_id']	= $row_mapel->pelajaran_id;
							$where_kkm['aspek_penilaian.aspek_kode']	= "KI 3";
							$kkm = $this->Aspek_pelajaran_model->get_aspek_pelajaran("*", $where_kkm);
							$nilai_kkm = ($kkm)?$kkm->aspek_pelajaran_kkm:0;

							echo "<tr>
								<td class=\"text-center\">".$i_mapel."</td>
								<td><strong>".$row_mapel->pelajaran_nama."</strong></td>
								<td class=\"text-center\">".$nilai_kkm."</td>";
								
								$NA_A = 0;
								$NA_K = 0;
								$n_k = 0;
								$n_a = 0;
								
								$deskripsi_tuntas = array();
								$deskripsi_lampaui = array();
								$deskripsi_belum = array();

								if ($listNilai){ 
									foreach ($listNilai as $row2){
										$where_persentase['komponen_pelajaran.tahun_kode']	= $tahun_kode;
										$where_persentase['komponen_pelajaran.semester_id']	= $semester_id;
										$where_persentase['komponen_pelajaran.pelajaran_id']= $row_mapel->pelajaran_id;
										$where_persentase['komponen_pelajaran.tingkat_id']	= $tingkat_id;
										$where_persentase['komponen_pelajaran.aspek_id']	= $row2['aspek_id'];
										$where_persentase['komponen_pelajaran.komponen_pelajaran_persentase >']	= 0;
										$persentase = $this->Komponen_pelajaran_model->get_komponen_pelajaran("SUM(komponen_pelajaran_persentase) AS total_persentase, COUNT(komponen_pelajaran_persentase) AS jumlah_persentase", $where_persentase);
										$total_persentase = ($persentase)?$persentase->total_persentase:0;
										$jumlah_persentase = ($persentase)?$persentase->jumlah_persentase:0;
										
										$where = array();
										$where['nilai.siswa_id']		= $siswa_id;
										$where['nilai.pelajaran_id']	= $row_mapel->pelajaran_id;
										$where['nilai.tahun_kode']		= $tahun_kode;
										$where['nilai.semester_id']		= $semester_id;
										$where['nilai.aspek_id']		= $row2['aspek_id'];
										$where['nilai.komponen_id']		= $row2['komponen_id'];
										$where['nilai.nilai_urutan']	= $row2['nilai_urutan'];
										$nilai = $this->Nilai_model->get_nilai("*", $where);
										$nilai_angka = ($nilai)?$nilai->nilai_angka:'-';

										if ($nilai && $row2['komponen_kompetensi'] == "Y" && $row2['nilai_kode'] == "VALUE"){
											$NA_K = $NA_K + $nilai_angka;
											$n_k++;
											echo "<td width=\"40\" style=\"text-align:center;\">".$nilai_angka."</td>";
										} else if ($row2['komponen_kompetensi'] == "N" && $row2['nilai_kode'] == "VALUE"){
											$NA_A = $NA_A + ($nilai_angka * ($row2['komponen_persentase'] / $total_persentase));
											$n_a++;
											echo "<td width=\"40\" style=\"text-align:center;\">".$nilai_angka."</td>";
										} else if ($row2['nilai_kode'] == "NA-K"){
											$NA_K = ($n_k > 0 && $NA_K > 0)?$NA_K / $n_k:'-';
											$NA_A = $NA_A + ($NA_K * ($row2['komponen_persentase'] / $total_persentase));
											echo "<td width=\"40\" style=\"text-align:center;\">".$NA_K."</td>";
											$n_a++;
											$n_k = 0;
											$NA_K = 0;
										} else {
											echo "<td width=\"40\" style=\"text-align:center;\">-</td>";
										}
									}
								}
							echo "</tr>";
							$i_mapel++;
						}
					}
				}
				?>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
<tr>
	<td class="style2">B. </td>
	<td class="style2">KETIDAKHADIRAN</td>
</tr>
<tr>
<td class="style2">&nbsp;</td>
<td style="padding:5px 0px 20px 0px;">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<table width="300" border="1" style="border-collapse:collapse">
			<?php
			$i = 1;
			?>
			<tr>
				<td align="left" style="padding:3px 5px;">Sakit</td>
				<td align="left" style="padding:3px 5px;"><?php echo ($dsAbsensi)?$dsAbsensi->sakit:''; ?> hari</td>
			</tr>
			<tr>
				<td align="left" style="padding:3px 5px;">Izin</td>
				<td align="left" style="padding:3px 5px;"><?php echo ($dsAbsensi)?$dsAbsensi->izin:''; ?> hari</td>
			</tr>
			<tr>
				<td align="left" style="padding:3px 5px;">Tanpa Keterangan</td>
				<td align="left" style="padding:3px 5px;"><?php echo ($dsAbsensi)?$dsAbsensi->tanpa_keterangan:''; ?> hari</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</td>
</tr>
<tr>
<td class="style2">&nbsp;</td>
<td style="padding:5px 0px 20px 0px;">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top">
		<br />
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="tandatangan">
			<tr>
				<td style="padding-top:0px;" width="25%"><div align="left">Mengetahui:<br />Orang Tua/Wali,</div></td>
				<td style="padding-top:0px;">&nbsp;</td>
				<td style="padding-top:0px;" width="30%">Bandung, <?php echo dateIndo_(($tanggal_raport)?$tanggal_raport:date('Y-m-d')); ?><br />Wali Kelas</td>
			</tr>
			<tr>
				<td colspan="3"><br /><br />&nbsp;</td>
			</tr>
			<tr>
				<td><div align="left"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></div></td>
				<td>&nbsp;</td>
				<td><u><?=$kelas->staf_nama?></u></td>
			</tr>
			<tr>
				<td style="padding-top:0px;" width="25%">&nbsp;</td>
				<td style="padding-top:0px;"><div align="center"><br />Mengetahui:<br />Kepala Sekolah,</div></td>
				<td style="padding-top:0px;" width="30%">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3"><br /><br />&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<div align="center"><u><?=($kepala_sekolah)?$kepala_sekolah->staf_nama:'';?></u></div>
					<div align="center"><?=($kepala_sekolah)?'NIP. '.$kepala_sekolah->staf_nip:'';?></div>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<br />
		</td>
	</tr>
</table>
</td>
</tr>
</table>

</div>
<!-- /////////////////////////////////////////////////////////////////////////////// -->
</body>
</html>