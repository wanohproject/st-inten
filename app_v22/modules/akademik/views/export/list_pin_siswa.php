<html>
	<head>
		<title>DAFTAR USER SISWA</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
		<style>
		.table tr td,
		.table tr th {
			font-size: 13px;
			padding: 2px 5px !important;
			border: 1px solid black !important;
		}
		</style>
	</head>
	<body onload="window.print();" style="padding:0.5cm;">
		<p class="text-center" style="font-weight:bold;">DAFTAR USER SISWA</p>
		<table class="table table-bordered">
			<tr>
				<th style="text-align:center !important" rowspan="2" width="50">NO</th>
				<th style="text-align:center !important" rowspan="2" width="80">NIS</th>
				<th style="text-align:center !important" rowspan="2">NAMA</th>
				<th style="text-align:center !important" rowspan="2" width="80">USER</th>
				<th style="text-align:center !important" colspan="2">PIN</th>
				<th style="text-align:center !important" rowspan="2" width="80">KELAS</th>
			</tr>
			<tr>
				<th style="text-align:center !important" width="80">SISWA</th>
				<th style="text-align:center !important" width="80">ORANGTUA</th>
			</tr>
			<?php
			$i = 1;
			if ($siswa) {
				foreach($siswa as $row){
				?>
				<tr>
					<td align="center"><?php echo $i; ?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_nis?></td>
					<td><?php echo $row->siswa_nama?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_user?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_pin?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_pin_orangtua?></td>
					<td style="text-align:center !important"><?php echo $row->siswa_kelas_sekarang?></td>
				</tr>
				<?php 
				$i++;
				}
			}
			?>
		</table> 
	</body>
</html>	