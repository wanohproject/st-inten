<?php
if ($excel == TRUE){
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msexcel'); // Other browsers  
header("Content-Disposition: attachment;Filename=Leger_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$pelajaran_nama))."_".str_replace(' ', '_', $kelas_nama)."_".date("YmdHis").".xls");
}
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=ProgId content=Excel.Sheet>
		<title>Leger Mata Pelajaran</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
		<style>
			td, th {
				font-size:12px;
			}
		</style>
	</head>
	<body>
		<table border="1" style="border-collapse:collapse" width="100%">
			<thead>
				<tr>
					<th colspan="6" style="text-align:center;">LEGGER MATA PELAJARAN</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th colspan="6">&nbsp;</th>
				</tr>
				<tr>
					<td colspan="6">Tahun Ajaran : <?php echo $tahun_nama; ?></td>
				</tr>
				<tr>
					<td colspan="6">Mata Pelajaran : <?php echo $pelajaran_nama; ?></td>
				</tr>
				<tr>
					<td colspan="6">Guru : <?php echo $guru_nama; ?></td>
				</tr>
				<tr>
					<td colspan="6">Kelas : <?php echo $kelas_nama; ?></td>
				</tr>
				<tr>
					<th colspan="6">&nbsp;</th>
				</tr>
			</tbody>
			<?php
			$q_aspek = $this->db->query("SELECT * 
										FROM aspek_pelajaran 
											LEFT JOIN aspek_penilaian ON aspek_pelajaran.aspek_id=aspek_penilaian.aspek_id
										WHERE aspek_pelajaran.tahun_kode = '{$tahun_kode}'
											AND aspek_pelajaran.aspek_id IN (1, 2)
											AND aspek_pelajaran_status = 'A'
											-- AND (SELECT COUNT(nilai_id) FROM nilai WHERE nilai.tahun_kode=aspek_pelajaran.tahun_kode AND nilai.aspek_id=aspek_pelajaran.aspek_id) > 0
										GROUP BY aspek_penilaian.aspek_id
										ORDER BY aspek_penilaian.aspek_kode ASC");
			$grid_aspek = $q_aspek->result();
			$count_aspek = $q_aspek->num_rows();

			$listAspek = array();
			$listKomponen = array();
			$listNilai = array();
			$iAspek = 0;
			$iKomponen = 0;
			$iNilai = 0;
			foreach ($grid_aspek as $row_aspek) {
			$listAspek[$iAspek]['aspek_id']		= $row_aspek->aspek_id;
			$listAspek[$iAspek]['aspek_kode']	= $row_aspek->aspek_kode;
			$listAspek[$iAspek]['aspek_nama']	= $row_aspek->aspek_nama;

			$nAspekColspan = 0;
			$q_komponen = $this->db->query("SELECT * 
											FROM komponen_pelajaran 
												LEFT JOIN komponen_penilaian ON komponen_pelajaran.komponen_id=komponen_penilaian.komponen_id
											WHERE komponen_pelajaran.tahun_kode = '{$tahun_kode}'
												AND komponen_pelajaran.semester_id = '{$semester_id}'
												AND komponen_pelajaran.tingkat_id = '{$tingkat_id}'
												AND komponen_pelajaran.aspek_id = '{$row_aspek->aspek_id}'
												AND komponen_pelajaran_status = 'A'
												-- AND (SELECT COUNT(nilai_id) 
												-- 		FROM nilai 
												-- 			LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id 
												-- 		WHERE nilai.tahun_kode = siswa_kelas.tahun_kode 
												-- 			AND nilai.siswa_id = siswa_kelas.siswa_id 
												-- 			AND nilai.tahun_kode=komponen_pelajaran.tahun_kode 
												-- 			AND nilai.semester_id=komponen_pelajaran.semester_id 
												-- 			AND siswa_kelas.kelas_id='{$kelas_id}' 
												-- 			AND nilai.aspek_id=komponen_pelajaran.aspek_id 
												-- 			AND nilai.komponen_id=komponen_pelajaran.komponen_id) > 0
											GROUP BY komponen_penilaian.komponen_id
											ORDER BY komponen_penilaian.komponen_urutan ASC");

			$grid_komponen = $q_komponen->result();
			$count_komponen = $q_komponen->num_rows();

			if ($count_komponen > 0){
			foreach ($grid_komponen as $row_komponen) {
				$listKomponen[$iKomponen]['komponen_id']			= $row_komponen->komponen_id;
				$listKomponen[$iKomponen]['komponen_kode']			= $row_komponen->komponen_kode;
				$listKomponen[$iKomponen]['komponen_nama']			= $row_komponen->komponen_nama;
				$listKomponen[$iKomponen]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
				$listKomponen[$iKomponen]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
				
				$nKomponenColspan = 0;
				$q_nilai = $this->db->query("SELECT * 
													FROM nilai 
														LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id
													WHERE nilai.tahun_kode = siswa_kelas.tahun_kode
														AND nilai.siswa_id = siswa_kelas.siswa_id
														and nilai.tahun_kode = '{$tahun_kode}'
														AND nilai.semester_id = '{$semester_id}'
														AND siswa_kelas.kelas_id = '{$kelas_id}'
														AND nilai.aspek_id = '{$row_aspek->aspek_id}'
														AND nilai.komponen_id = '{$row_komponen->komponen_id}'
														AND nilai_akhir = 'N'
													GROUP BY nilai.nilai_urutan
													ORDER BY nilai.nilai_urutan ASC");

				$grid_nilai = $q_nilai->result();
				$count_nilai = $q_nilai->num_rows();

				if ($count_nilai){
					foreach ($grid_nilai as $row_nilai) {
						$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
						$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
						$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
						$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
						$listNilai[$iNilai]['nilai_urutan']			= $row_nilai->nilai_urutan;
						$listNilai[$iNilai]['kompetensi_id']		= $row_nilai->kompetensi_id;
						$listNilai[$iNilai]['nilai_kode']			= "VALUE";
						$iNilai++;
						$nAspekColspan++;
						$nKomponenColspan++;
					}
					if ($row_komponen->komponen_kompetensi == 'Y'){
						$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
						$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
						$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
						$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
						$listNilai[$iNilai]['nilai_urutan']			= "RATA<u>2</u>";
						$listNilai[$iNilai]['kompetensi_id']		= "0";
						$listNilai[$iNilai]['nilai_kode']			= "NA-K";
						$iNilai++;
						$nAspekColspan++;
						$nKomponenColspan++;
					}
				} else {
					$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
					$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
					$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
					$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
					$listNilai[$iNilai]['nilai_urutan']			= "NULL";
					$listNilai[$iNilai]['kompetensi_id']		= "0";
					$listNilai[$iNilai]['nilai_kode']			= "NULL";
					$iNilai++;
					$nAspekColspan++;
					$nKomponenColspan++;
				}
				$listKomponen[$iKomponen]['komponen_colspan']	= $nKomponenColspan;
				$iKomponen++;
			}
			if ($iKomponen > 0){
				$listKomponen[$iKomponen]['komponen_id']			= "NA";
				$listKomponen[$iKomponen]['komponen_kode']			= "NA";
				$listKomponen[$iKomponen]['komponen_nama']			= "NA";
				$listKomponen[$iKomponen]['komponen_persentase']	= "NA";
				$listKomponen[$iKomponen]['komponen_colspan']		= 1;
				$iKomponen++;

				$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
				$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
				$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
				$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
				$listNilai[$iNilai]['nilai_urutan']			= "NA";
				$listNilai[$iNilai]['kompetensi_id']		= "0";
				$listNilai[$iNilai]['nilai_kode']			= "NA-A";
				$iNilai++;
			}
			} else {
			$listKomponen[$iKomponen]['komponen_id']			= "NULL";
			$listKomponen[$iKomponen]['komponen_kode']			= "NULL";
			$listKomponen[$iKomponen]['komponen_nama']			= "NULL";
			$listKomponen[$iKomponen]['komponen_persentase']	= "NULL";
			$listKomponen[$iKomponen]['komponen_colspan']		= 1;
			$iKomponen++;
			}

			$listAspek[$iAspek]['aspek_colspan']	= $nAspekColspan + 1;
			$iAspek++;
			}
			?>
			<thead>
				<tr>
					<th rowspan="3" width="35" style="vertical-align:middle;text-align:center;">No</th>
					<th rowspan="3" width="100" style="vertical-align:middle;text-align:center;">NIS</th>
					<th rowspan="3" width="300" style="vertical-align:middle;text-align:center;">Siswa</th>
					<th rowspan="3" width="55" style="vertical-align:middle;text-align:center">KKM</th>
					<?php if ($listAspek){ 
						foreach ($listAspek as $row_aspek){
							echo "<th style=\"text-align:center\" colspan=\"".$row_aspek['aspek_colspan']."\">".$row_aspek['aspek_nama']."</th>";
						}
					}?>
				</tr>
				<tr>
					<?php if ($listKomponen){ 
						foreach ($listKomponen as $row_komponen){
							if ($row_komponen['komponen_id'] == 'NA' || $row_komponen['komponen_colspan'] == '1' || $row_komponen['komponen_kompetensi'] == 'N'){
								echo "<th style=\"text-align:center;vertical-align:middle;\" width=\"60\" rowspan=\"2\" colspan=\"".$row_komponen['komponen_colspan']."\">".$row_komponen['komponen_kode']."</th>";
							} else {
								echo "<th style=\"text-align:center;\" colspan=\"".$row_komponen['komponen_colspan']."\">".$row_komponen['komponen_kode']."</th>";
							}
						}
					}?>
				</tr>
				<tr>
					<?php if ($listNilai){ 
						foreach ($listNilai as $row_nilai){
							if (($row_nilai['nilai_kode'] == 'VALUE' && $row_nilai['komponen_kompetensi'] == 'Y') || ($row_nilai['nilai_kode'] == 'NA-K' && $row_nilai['komponen_kompetensi'] == 'Y')){
								echo "<th style=\"text-align:center\" width=\"60\">".$row_nilai['nilai_urutan']."</th>";
							}
						}
					}?>
				</tr>
			</thead>
			<tbody>
			<?php
			if ($tahun_kode != '-' && $semester_id != '-' && $pelajaran_id != '-' && $tingkat_id != '-' && $kelas_id != '-'){
				$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa_kelas.siswa_id ASC");
				$i = 1;
				if ($query->num_rows() > 0) {
					foreach($query->result() as $row) {
						$where_kkm['aspek_pelajaran.tahun_kode']		= $tahun_kode;
						$where_kkm['aspek_pelajaran.semester_id']	= $semester_id;
						$where_kkm['aspek_pelajaran.tingkat_id']	= $tingkat_id;
						$where_kkm['aspek_pelajaran.pelajaran_id']	= $pelajaran_id;
						$where_kkm['aspek_penilaian.aspek_kode']	= "KI 3";
						$kkm = $this->Aspek_pelajaran_model->get_aspek_pelajaran("*", $where_kkm);
						$nilai_kkm = ($kkm)?$kkm->aspek_pelajaran_kkm:0;

						?>
						<tr>
							<td style="text-align:center;"><?php echo $i; ?></td>
							<td style="text-align:center;"><?php echo $row->siswa_nis; ?></td>
							<td style="text-align:left;"><?php echo $row->siswa_nama; ?></td>
							<td style="text-align:center;"><?php echo $nilai_kkm; ?></td>
							<?php 
							$NA_A = 0;
							$NA_K = 0;
							$n_k = 0;
							$n_a = 0;
							
							$deskripsi_tuntas = array();
							$deskripsi_lampaui = array();
							$deskripsi_belum = array();

							if ($listNilai){ 
								foreach ($listNilai as $row2){
									$where_persentase['komponen_pelajaran.tahun_kode']	= $tahun_kode;
									$where_persentase['komponen_pelajaran.semester_id']	= $semester_id;
									$where_persentase['komponen_pelajaran.pelajaran_id']= $pelajaran_id;
									$where_persentase['komponen_pelajaran.tingkat_id']	= $tingkat_id;
									$where_persentase['komponen_pelajaran.aspek_id']	= $row2['aspek_id'];
									$where_persentase['komponen_pelajaran.komponen_pelajaran_persentase >']	= 0;
									$persentase = $this->Komponen_pelajaran_model->get_komponen_pelajaran("SUM(komponen_pelajaran_persentase) AS total_persentase, COUNT(komponen_pelajaran_persentase) AS jumlah_persentase", $where_persentase);
									$total_persentase = ($persentase)?$persentase->total_persentase:0;
									$jumlah_persentase = ($persentase)?$persentase->jumlah_persentase:0;
									
									$where = array();
									$where['nilai.siswa_id']		= $row->siswa_id;
									$where['nilai.pelajaran_id']	= $pelajaran_id;
									$where['nilai.tahun_kode']		= $tahun_kode;
									$where['nilai.semester_id']		= $semester_id;
									$where['nilai.aspek_id']		= $row2['aspek_id'];
									$where['nilai.komponen_id']		= $row2['komponen_id'];
									$where['nilai.nilai_urutan']	= $row2['nilai_urutan'];
									$nilai = $this->Nilai_model->get_nilai("*", $where);
									$nilai_angka = ($nilai)?$nilai->nilai_angka:'-';

									if ($nilai && $row2['komponen_kompetensi'] == "Y" && $row2['nilai_kode'] == "VALUE"){
										$NA_K = $NA_K + $nilai_angka;
										$n_k++;
										echo "<td width=\"55\" style=\"text-align:center;\">".$nilai_angka."</td>";
									} else if ($row2['komponen_kompetensi'] == "N" && $row2['nilai_kode'] == "VALUE"){
										$NA_A = $NA_A + ($nilai_angka * ($row2['komponen_persentase'] / $total_persentase));
										$n_a++;
										echo "<td width=\"60\" style=\"text-align:center;\">".$nilai_angka."</td>";
									} else if ($row2['nilai_kode'] == "NA-K"){
										$NA_K = ($n_k > 0 && $NA_K > 0)?$NA_K / $n_k:'-';
										$NA_A = $NA_A + ($NA_K * ($row2['komponen_persentase'] / $total_persentase));
										echo "<td width=\"60\" style=\"text-align:center;\">".$NA_K."</td>";
										$n_a++;
										$n_k = 0;
										$NA_K = 0;
									} else if ($row2['nilai_kode'] == "NA-A"){
										//NILAI
										$whereDB = array();
										$whereDB['nilai.siswa_id']		= $row->siswa_id;
										$whereDB['nilai.pelajaran_id']	= $pelajaran_id;
										$whereDB['nilai.tahun_kode']		= $tahun_kode;
										$whereDB['nilai.semester_id']	= $semester_id;
										$whereDB['nilai.aspek_id']		= $row2['aspek_id'];
										$whereDB['nilai.nilai_akhir']	= 'Y';
										$whereDB['nilai.komponen_id']	= null;
										$whereDB['nilai.nilai_urutan']	= null;
										$nilaiDB = $this->Nilai_model->get_nilai("nilai_angka", $whereDB);

										if ($nilaiDB){
											$NA_A = $nilaiDB->nilai_angka;
										} else {
											$NA_A = ($n_a > 0 && $NA_A > 0)?round($NA_A, 2):'-';
										}
										
										echo "<td width=\"60\" style=\"text-align:center;\">".$NA_A."</td>";
										$NA_A = 0;
									} else {
										echo "<td width=\"60\" style=\"text-align:center;\">-</td>";
									}
								}
							}
							?>
						</tr>
						<?php
						$i++;
					}
				}
			}
			?>
			</tbody>
		</table>
	</body>
</html>	