<html>
	<head>
		<title>DAFTAR USER GURU</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
		<style>
		.table tr td,
		.table tr th {
			font-size: 13px;
			padding: 2px 5px !important;
			border: 1px solid black !important;
		}
		</style>
	</head>
	<body onload="window.print();" style="padding:20px;">
		<p class="text-center" style="font-weight:bold;">DAFTAR USER GURU</p>
		<table class="table table-bordered">
			<tr>
				<th width="30">NO</th>
				<th width="80">NIP</th>
				<th width="40">KODE</th>
				<th>NAMA</th>
				<th width="80">USER</th>
				<th width="80">PIN</th>
			</tr>
			<?php
			$i = 1;
			if ($staf) {
				foreach($staf as $row){
				?>
				<tr>
					<td align="center"><?php echo $i; ?></td>
					<td><?php echo $row->staf_nip?></td>
					<td><?php echo $row->staf_kode?></td>
					<td><?php echo $row->staf_nama?></td>
					<td><?php echo $row->staf_user?></td>
					<td><?php echo $row->staf_pin?></td>
				</tr>
				<?php 
				$i++;
				}
			}
			?>
		</table> 
	</body>
</html>	