<?php
if ($excel == TRUE){
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msexcel'); // Other browsers  
header("Content-Disposition: attachment;Filename=Leger_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$pelajaran_nama))."_".str_replace(' ', '_', $kelas_nama)."_".date("YmdHis").".xls");
}

// MENGHITUNG SEMUA ASPEK PELAJARAN YANG AKTIF
$query_pelajaran = $this->db->query("SELECT * FROM guru_pelajaran 
									LEFT JOIN aspek_pelajaran ON guru_pelajaran.pelajaran_id=aspek_pelajaran.pelajaran_id 
									WHERE guru_pelajaran.tahun_kode = '$tahun_kode'
										AND guru_pelajaran.kelas_id = '$kelas_id'
									GROUP BY aspek_pelajaran.pelajaran_id, aspek_pelajaran.aspek_id"); 
$count_pelajaran = $query_pelajaran->num_rows();

$query = $this->db->query("SELECT * FROM pengujian WHERE pengujian.tahun_kode = '$tahun_kode' AND pengujian.tingkat_id = '$tingkat_id' AND pengujian.semester_id = '$semester_id' AND pengujian.pelajaran_id='$pelajaran_id'");
$jml_pengujian = $query->num_rows();
$data_pengujian = $query->result();
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=ProgId content=Excel.Sheet>
		<title>Leger Wali Kelas</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
	</head>
	<body>
		<table border="1" style="border-collapse:collapse">
			<thead>
				<tr>
					<th colspan="<?php echo ($count_pelajaran + 3); ?>" style="text-align:center;">LEGGER KELAS </th>
				</tr>
				<tr>
					<th colspan="6">&nbsp;</th>
				</tr>
				<tr>
					<td colspan="6">Tahun Ajaran : <?php echo $tahun_nama; ?></td>
				</tr>
				<tr>
					<td colspan="6">Jenis Pengujian : <?php echo $komponen_nama; ?></td>
				</tr>
				<tr>
					<td colspan="6">Guru : <?php echo $guru_nama; ?></td>
				</tr>
				<tr>
					<td colspan="6">Kelas : <?php echo $kelas_nama; ?></td>
				</tr>
				<tr>
					<th colspan="6">&nbsp;</th>
				</tr>
			</thead>
			<?php 
			if ($kelas_id && $pelajaran_id && $komponen_id && $kelas_id != '-' && $pelajaran_id != '-' && $komponen_id != '-'){
			?>
			<tbody>
				<tr>
					<th rowspan="3" width="30" style="vertical-align:middle;text-align:center;">No</th>
					<th rowspan="3" width="100" style="vertical-align:middle;text-align:center;">NIS</th>
					<th rowspan="3" width="300" style="vertical-align:middle;text-align:center;">Nama</th>
					<th colspan="<?php echo $count_pelajaran; ?>" style="text-align:center;">MATA PELAJARAN</th>
				</tr>
				<tr>
				<?php 
				$grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
														FROM guru_pelajaran 
															LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
														WHERE guru_pelajaran_status = 'A' 
															AND tahun_kode = '{$tahun_kode}' 
															AND pelajaran_status = 'A' 
															AND pelajaran_sifat = 'Wajib' 
														GROUP BY pelajaran_kelompok 
														ORDER BY pelajaran_kelompok ASC")->result();
														
					foreach($grid_kelompok as $row_kelompok){
						if ($row_kelompok->pelajaran_peminatan){
							$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																FROM guru_pelajaran 
																	LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																	LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																WHERE guru_pelajaran_status = 'A' 
																	AND tahun_kode = '{$tahun_kode}' 
																	AND pelajaran_status = 'A' 
																	AND pelajaran_sifat = 'Wajib' 
																	AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																	GROUP BY pelajaran_peminatan
																	ORDER BY peminatan.peminatan_urutan ASC")->result();
							$i_peminatan = 1;
							foreach($grid_peminatan as $row_peminatan){
								$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																FROM guru_pelajaran 
																	LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																	LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																WHERE guru_pelajaran_status = 'A'  
																	AND tahun_kode = '{$tahun_kode}'
																	AND guru_pelajaran.kelas_id = '{$kelas_id}'
																	AND pelajaran_status = 'A' 
																	AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																	AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																	AND pelajaran_sifat = 'Wajib' 
																GROUP BY guru_pelajaran.pelajaran_id 
																ORDER BY pelajaran_urutan ASC")->result();
								if ($grid_mapel){
									$i_mapel = 1;
									foreach($grid_mapel as $row_mapel){
										$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
										echo "<td colspan=\"$count_aspek\" style=\"vertical-align:middle;text-align:center\">{$row_mapel->pelajaran_nama}</td>";
										$i_mapel++;
									}
									$i_peminatan++;
								}
							}
						} else {
							$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
															FROM guru_pelajaran 
																LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
															WHERE guru_pelajaran_status = 'A' 
																AND tahun_kode = '{$tahun_kode}'
																AND guru_pelajaran.kelas_id = '{$kelas_id}'
																AND pelajaran_status = 'A' 
																AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																AND pelajaran_sifat = 'Wajib' 
															GROUP BY guru_pelajaran.pelajaran_id 
															ORDER BY pelajaran_urutan ASC")->result();
							$i_mapel = 1;
							foreach($grid_mapel as $row_mapel){
								$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
								echo "<td colspan=\"$count_aspek\" style=\"vertical-align:middle;text-align:center\">{$row_mapel->pelajaran_nama}</td>";
								$i_mapel++;
							}
						}
					}?>
				</tr>
				
				<tr>
				<?php $grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
														FROM guru_pelajaran 
															LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
														WHERE guru_pelajaran_status = 'A' 
															AND tahun_kode = '{$tahun_kode}' 
															AND pelajaran_status = 'A' 
															AND pelajaran_sifat = 'Wajib' 
														GROUP BY pelajaran_kelompok 
														ORDER BY pelajaran_kelompok ASC")->result();
														
					foreach($grid_kelompok as $row_kelompok){
						if ($row_kelompok->pelajaran_peminatan){
							$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																FROM guru_pelajaran 
																	LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																	LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																WHERE guru_pelajaran_status = 'A' 
																	AND tahun_kode = '{$tahun_kode}' 
																	AND pelajaran_status = 'A' 
																	AND pelajaran_sifat = 'Wajib' 
																	AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																	GROUP BY pelajaran_peminatan
																	ORDER BY peminatan.peminatan_urutan ASC")->result();
							$i_peminatan = 1;
							foreach($grid_peminatan as $row_peminatan){
								$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																FROM guru_pelajaran 
																	LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																	LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																WHERE guru_pelajaran_status = 'A'  
																	AND tahun_kode = '{$tahun_kode}'
																	AND guru_pelajaran.kelas_id = '{$kelas_id}'
																	AND pelajaran_status = 'A' 
																	AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																	AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																	AND pelajaran_sifat = 'Wajib' 
																GROUP BY guru_pelajaran.pelajaran_id 
																ORDER BY pelajaran_urutan ASC")->result();
								if ($grid_mapel){
									$i_mapel = 1;
									foreach($grid_mapel as $row_mapel){
										$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
										if ($grid_aspek){
											foreach ($grid_aspek as $row2){
												echo "<td style=\"text-align:center;white-space: nowrap;\" width=\"80\">{$row2->aspek_kode}</td>";
											}
										}
										$i_mapel++;
									}
									$i_peminatan++;
								}
							}
						} else {
							$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
															FROM guru_pelajaran 
																LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
															WHERE guru_pelajaran_status = 'A' 
																AND tahun_kode = '{$tahun_kode}'
																AND guru_pelajaran.kelas_id = '{$kelas_id}'
																AND pelajaran_status = 'A' 
																AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																AND pelajaran_sifat = 'Wajib' 
															GROUP BY guru_pelajaran.pelajaran_id 
															ORDER BY pelajaran_urutan ASC")->result();
							$i_mapel = 1;
							foreach($grid_mapel as $row_mapel){
								$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
								if ($grid_aspek){
									foreach ($grid_aspek as $row2){
										echo "<td style=\"text-align:center;white-space: nowrap;\" width=\"80\">{$row2->aspek_kode}</td>";
									}
								}
								$i_mapel++;
							}
						}
					}?>
				</tr>
				<?php
				$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa.siswa_nama ASC");
				$i = 1;
				if ($query->num_rows() > 0) {
					foreach($query->result() as $row) {
					?>
					<tr>
						<td style="text-align:center;"><?php echo $i; ?></td>
						<td style="text-align:center;"><?php echo $row->siswa_nis; ?></td>
						<td style="text-align:left;"><?php echo $row->siswa_nama; ?></td>
						<?php 
						$grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
														FROM guru_pelajaran 
															LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
														WHERE guru_pelajaran_status = 'A' 
															AND tahun_kode = '{$tahun_kode}' 
															AND pelajaran_status = 'A' 
															AND pelajaran_sifat = 'Wajib' 
														GROUP BY pelajaran_kelompok 
														ORDER BY pelajaran_kelompok ASC")->result();
														
						foreach($grid_kelompok as $row_kelompok){
							if ($row_kelompok->pelajaran_peminatan){
								$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																	FROM guru_pelajaran 
																		LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																		LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																	WHERE guru_pelajaran_status = 'A' 
																		AND tahun_kode = '{$tahun_kode}' 
																		AND pelajaran_status = 'A' 
																		AND pelajaran_sifat = 'Wajib' 
																		AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																		GROUP BY pelajaran_peminatan
																		ORDER BY peminatan.peminatan_urutan ASC")->result();
								$i_peminatan = 1;
								foreach($grid_peminatan as $row_peminatan){
									$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																	FROM guru_pelajaran 
																		LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																		LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																	WHERE guru_pelajaran_status = 'A'  
																		AND tahun_kode = '{$tahun_kode}'
																		AND guru_pelajaran.kelas_id = '{$kelas_id}'
																		AND pelajaran_status = 'A' 
																		AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																		AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																		AND pelajaran_sifat = 'Wajib' 
																	GROUP BY guru_pelajaran.pelajaran_id 
																	ORDER BY pelajaran_urutan ASC")->result();
									if ($grid_mapel){
										$i_mapel = 1;
										foreach($grid_mapel as $row_mapel){
											$get_nilai_ki3 = $this->db->query("SELECT *
																		FROM nilai
																		WHERE nilai.siswa_id='{$row->siswa_id}'
																			AND nilai.aspek_id='1'
																			AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																			AND nilai.tahun_kode='{$tahun_kode}'
																			AND nilai.semester_id='{$semester_id}'
																			AND nilai.komponen_id='{$komponen_id}'
																			AND nilai.staf_id='{$row_mapel->staf_id}'
																			AND nilai.nilai_angka > 0")->row();
										
										$get_nilai_ki4 = $this->db->query("SELECT *
																		FROM nilai
																		WHERE nilai.siswa_id='{$row->siswa_id}'
																			AND nilai.aspek_id='2'
																			AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																			AND nilai.tahun_kode='{$tahun_kode}'
																			AND nilai.semester_id='{$semester_id}'
																			AND nilai.komponen_id='{$komponen_id}'
																			AND nilai.staf_id='{$row_mapel->staf_id}'
																			AND nilai.nilai_angka > 0")->row();
																			
										$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
										$nilai_angka_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_angka:'-';
										$nilai_huruf_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_huruf:'-';
										$nilai_angka_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_angka:'-';
										$nilai_huruf_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_huruf:'-';
										echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki3 ."</td>";
										echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki4 ."</td>";
											$i_mapel++;
										}
										$i_peminatan++;
									}
								}
							} else {
								$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																FROM guru_pelajaran 
																	LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																	LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																WHERE guru_pelajaran_status = 'A' 
																	AND tahun_kode = '{$tahun_kode}'
																	AND guru_pelajaran.kelas_id = '{$kelas_id}'
																	AND pelajaran_status = 'A' 
																	AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																	AND pelajaran_sifat = 'Wajib' 
																GROUP BY guru_pelajaran.pelajaran_id 
																ORDER BY pelajaran_urutan ASC")->result();
								$i_mapel = 1;
								foreach($grid_mapel as $row_mapel){
									$get_nilai_ki3 = $this->db->query("SELECT *
																	FROM nilai
																	WHERE nilai.siswa_id='{$row->siswa_id}'
																		AND nilai.aspek_id='1'
																		AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																		AND nilai.tahun_kode='{$tahun_kode}'
																		AND nilai.semester_id='{$semester_id}'
																		AND nilai.komponen_id='{$komponen_id}'
																		AND nilai.staf_id='{$row_mapel->staf_id}'
																		AND nilai.nilai_angka > 0")->row();
									
									$get_nilai_ki4 = $this->db->query("SELECT *
																	FROM nilai
																	WHERE nilai.siswa_id='{$row->siswa_id}'
																		AND nilai.aspek_id='2'
																		AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																		AND nilai.tahun_kode='{$tahun_kode}'
																		AND nilai.semester_id='{$semester_id}'
																		AND nilai.komponen_id='{$komponen_id}'
																		AND nilai.staf_id='{$row_mapel->staf_id}'
																		AND nilai.nilai_angka > 0")->row();
																		
									$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
									$nilai_angka_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_angka:'-';
									$nilai_huruf_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_huruf:'-';
									$nilai_angka_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_angka:'-';
									$nilai_huruf_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_huruf:'-';
									echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki3 ."</td>";
									echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki4 ."</td>";
									$i_mapel++;
								}
							}
						}?>
					</tr>
					<?php
					$i++;
					}
				}			
			?>
			</tbody>
		</table>
		<?php } ?>
	</body>
</html>	