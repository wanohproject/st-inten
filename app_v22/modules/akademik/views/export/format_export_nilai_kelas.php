<?php
if ($excel == TRUE){
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msexcel'); // Other browsers 
	if ($komponen_kode == 'PH' && $kompetensi_kode){
		header("Content-Disposition: attachment;Filename=Nilai_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$komponen_nama))."_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$aspek_nama))."_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$kompetensi_kode))."_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$pelajaran_nama))."_".str_replace(' ', '_', $kelas_nama)."_".date("YmdHis").".xls");
	} else {
		header("Content-Disposition: attachment;Filename=Nilai_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$komponen_nama))."_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$aspek_nama))."_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$pelajaran_nama))."_".str_replace(' ', '_', $kelas_nama)."_".date("YmdHis").".xls");
	}
}
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=ProgId content=Excel.Sheet>
		<title>Format Nilai Siswa Kelas</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
	</head>
	<body>		
		<table border="1" style="border-collapse:collapse">
			<?php if ($komponen_kompetensi == 'Y'){ ?>
			<tr>
				<th colspan="5">DATA NILAI <?php echo strtoupper($komponen_kode); ?>-<?php echo $nilai_urutan?> - <?php echo strtoupper($aspek_nama); ?> -  <?php echo strtoupper($pelajaran_nama); ?> - KELAS <?php echo strtoupper($kelas_nama); ?> - <?php echo strtoupper($guru_nama); ?></th>
			</tr>
			<?php } else { ?>
				<tr>
				<th colspan="5">DATA NILAI <?php echo strtoupper($komponen_kode); ?> - <?php echo strtoupper($aspek_nama); ?> -  <?php echo strtoupper($pelajaran_nama); ?> - KELAS <?php echo strtoupper($kelas_nama); ?> - <?php echo strtoupper($guru_nama); ?></th>
			</tr>
			<?php } ?>
			<tr>
				<th style="text-align:center" width="50">NO</th>
				<th style="text-align:center" width="100">NIS</th>
				<th style="text-align:center" width="350">NAMA</th>
				<th style="text-align:center" width="80">KKM</th>
				<th style="text-align:center" width="80">NILAI</th>
			</tr>
			<?php
			$i = 1;
			foreach($grid_siswa_kelas as $row){
				if ($komponen_kompetensi == 'Y'){
					$query_nilai = $this->db->query("SELECT nilai_kkm, nilai_angka FROM nilai
													WHERE nilai.siswa_id='{$row->siswa_id}'
														AND nilai.nilai_urutan='{$nilai_urutan}'
														AND nilai.aspek_id='{$aspek_id}'
														AND nilai.pelajaran_id='{$pelajaran_id}'
														AND nilai.tahun_kode='{$tahun_kode}'
														AND nilai.semester_id='{$semester_id}'
														AND nilai.komponen_id='{$komponen_id}'
														AND nilai.staf_id='{$staf_id}'")->row();

					$nilai_kkm = ($query_nilai)?$query_nilai->nilai_kkm:'-';
					$nilai_angka = ($query_nilai && $query_nilai->nilai_angka > 0)?$query_nilai->nilai_angka:'-';
				} else {
					$query_nilai = $this->db->query("SELECT nilai_kkm, nilai_angka FROM nilai
					WHERE nilai.siswa_id='{$row->siswa_id}'
						AND nilai.aspek_id='{$aspek_id}'
						AND nilai.pelajaran_id='{$pelajaran_id}'
						AND nilai.tahun_kode='{$tahun_kode}'
						AND nilai.semester_id='{$semester_id}'
						AND nilai.komponen_id='{$komponen_id}'
						AND nilai.staf_id='{$staf_id}'")->row();

						$nilai_kkm = ($query_nilai)?$query_nilai->nilai_kkm:'-';
						$nilai_angka = ($query_nilai && $query_nilai->nilai_angka > 0)?$query_nilai->nilai_angka:'-';
				}
			?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td align="center"><?php echo $row->siswa_nis?></td>
				<td><?php echo $row->siswa_nama?></td>
				<td><?php echo $nilai_kkm?></td>
				<td><?php echo $nilai_angka?></td>
			</tr>
			<?php 
			$i++;
			}
			?>
		</table> 
	</body>
</html>