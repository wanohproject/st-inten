<?php
if ($excel == TRUE){
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msexcel'); // Other browsers  
header("Content-Disposition: attachment;Filename=Data_".str_replace(' ', '_', preg_replace("/[^A-Za-z0-9_ ]/",'',$pelajaran_nama))."_".str_replace(' ', '_', $kelas_nama)."_".date("YmdHis").".xls");
}
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=ProgId content=Excel.Sheet>
		<title>Format Nilai Siswa Kelas</title>
		<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/bootstrap/css/bootstrap.min.css');?>">
	</head>
	<body>		
		<table border="1" style="border-collapse:collapse">
			<tr>
				<th colspan="6">DATA INFORMASI RAPORT</th>
			</tr>
			<tr>
				<td colspan="2">Kelas</td>
				<td colspan="4" style="font-weight:bold;"><?php echo strtoupper($kelas_nama); ?></td>
			</tr>
			<tr>
				<td colspan="2">Semester</td>
				<td colspan="4" style="font-weight:bold;"><?php echo strtoupper($semester_nama); ?></td>
			</tr>
			<tr>
				<td colspan="2">Tahun Pelajaran</td>
				<td colspan="4" style="font-weight:bold;"><?php echo strtoupper($tahun_nama); ?></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<th style="text-align:center">&nbsp;</th>
				<th style="text-align:center">&nbsp;</th>
				<th style="text-align:center">&nbsp;</th>
				<th style="text-align:center" colspan="2">SIKAP SPIRITUAL</th>
				<th style="text-align:center" colspan="2">SIKAP SOSIAL</th>
				<th style="text-align:center">&nbsp;</th>
				<th style="text-align:center" colspan="5">PRESTASI</th>
				<th style="text-align:center" colspan="5">EKSTRAKURIKULER</th>
				<th style="text-align:center" colspan="3">PRESENSI</th>
			</tr>
			<tr>
				<th style="text-align:center" width="50">NO</th>
				<th style="text-align:center" width="100">NIS</th>
				<th style="text-align:center" width="300">NAMA</th>
				<th style="text-align:center" width="90">PREDIKAT</th>
				<th style="text-align:center" width="300">DESKRIPSI</th>
				<th style="text-align:center" width="90">PREDIKAT</th>
				<th style="text-align:center" width="300">DESKRIPSI</th>
				<th style="text-align:center" width="300">CATATAN WALI KELAS</th>
				<th style="text-align:center" width="300">1</th>
				<th style="text-align:center" width="300">2</th>
				<th style="text-align:center" width="300">3</th>
				<th style="text-align:center" width="300">4</th>
				<th style="text-align:center" width="300">5</th>
				<th style="text-align:center" width="300">1</th>
				<th style="text-align:center" width="300">2</th>
				<th style="text-align:center" width="300">3</th>
				<th style="text-align:center" width="300">4</th>
				<th style="text-align:center" width="300">5</th>
				<th style="text-align:center" width="80">SAKIT</th>
				<th style="text-align:center" width="80">IZIN</th>
				<th style="text-align:center" width="80">ALFA</th>
			</tr>
			<?php
			$i = 1;
			if ($grid_siswa_kelas) {
				foreach($grid_siswa_kelas as $row){
				?>
				<tr>
					<td align="center"><?php echo $i; ?></td>
					<td align="center"><?php echo $row->siswa_nis?></td>
					<td><?php echo $row->siswa_nama?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Jenis Kegiatan;Keterangan</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Kegiatan Ekstrakurikuler;Keterangan;Nilai</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<?php 
				$i++;
				}
			}
			?>
		</table> 
	</body>
</html>	