<html>
<head>
<title>Cetak Kartu</title>

</head>
<body onload="window.print();">
<style>@media print {
    footer {page-break-after: always;}
}
</style>
<?php
$i = 1;
// $query = $this->db->query("SELECT * 
// FROM siswa_kelas 
// LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id 
// LEFT JOIN kelas ON siswa_kelas.kelas_id=kelas.kelas_id 
// WHERE siswa_kelas.kelas_id = '$kelas_id' AND kelas.tahun_kode='$tahun_kode'");
// $grid = $query->result();
// $count = $query->num_rows();
// print_r($grid);
if ($siswa){
	foreach($siswa as $row){
		if ($i % 10 == 1 && $i > 0){
			echo '<div style="page-break-before:always;"></div>
				<table width="100%" border="0" style="margin-top:8px;">';	
		} else if ($i % 10 == 1){
			echo '<table width="100%" border="0" style="margin-top:8px;">';	
		}
		
		if ($i % 2 == 1){
			echo "<tr>";
		}
		?>
		<td>
		<table style="width:10.2cm;border:1px solid black; padding-top:6px; font-family:Arial, Helvetica, sans-serif; font-size:12px" class="kartu" border="0">
			<tbody>
				<tr>
					<td colspan="3" style="padding:1px" align="center">
						<table width="98%" class="kartu" cellpadding="0px">
						<tbody><tr>
							<td><img src="<?php echo base_url('asset/profil/'.profile('profil_logo')); ?>" height="48"></td>
							<td align="center" style="font-weight:bold">
								<?php echo profile('profil_institusi'); ?> <br>SISTEM INFORMASI PRESENSI<BR /> 
						</td>
						</tr>
						</tbody></table>		
					</td>
				</tr>
				<tr height="10px"><td colspan="2" valign="top" align="center"><br></td></tr>
				<tr height="10px"><td width="90">&nbsp;NIS</td><td width="8">:</td><td width="226" style="font-size:12px;font-weight:bold;"><?php echo $row->siswa_nis; ?></td></tr>
				<tr height="10px"><td width="90">&nbsp;Nama</td><td width="8">:</td><td width="226" style="font-size:12px;font-weight:bold;"><?php echo $row->siswa_nama; ?></td></tr>
				<tr height="10px"><td>&nbsp;Kelas</td><td>:</td><td style="font-size:12px;font-weight:bold;"><?php echo $row->kelas_nama; ?></td></tr>    
				<tr height="10px"><td >&nbsp;Username</td><td>:</td><td style="font-size:12px;font-weight:bold;"><?php echo $row->siswa_user; ?></td></tr>
				<tr height="10px"><td>&nbsp;PIN Siswa</td><td>:</td><td style="font-size:12px;font-weight:bold;"><?php echo $row->siswa_pin; ?></td></tr>
				<tr height="10px"><td>&nbsp;PIN Orang Tua</td><td>:</td><td style="font-size:12px;font-weight:bold;"><?php echo $row->siswa_pin_orangtua; ?></td></tr>
				<tr height="10px"><td colspan="2" valign="top" align="center"><br></td></tr>
			</tbody>
		</table>
		</td>
		<?php 
		if ($i % 2 == 0){
			echo "</tr>";
		}
		if ($i % 10 == 0){
			echo '<table width="100%" border="0" style="margin-top:8px;">';	
		}
		$i++;
	}
}
?>
</table>
</body>
</html>