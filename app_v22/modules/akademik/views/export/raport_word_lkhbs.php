<?php

header('Content-Type: application/vnd.ms-word'); //IE and Opera  
header('Content-Type: application/w-msword'); // Other browsers  
header('Content-Disposition: attachment; filename=Raport_LKHBS_Siswa_'.$siswa->siswa_nis.'_'.date('d_m_Y_H_i_s').'.doc'); 
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="Doc1_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2008-06-16T08:31:00Z</o:Created>
  <o:LastSaved>2008-06-16T08:31:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
 <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
table { page-break-inside:auto }
tr    { page-break-inside:avoid; page-break-after:auto }
thead { display:table-header-group }
tfoot { display:table-footer-group }
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Arial";
	mso-fareast-font-family:"Arial";}
@page Section1
	{size:609.0pt 935.0pt;
	margin:36.0pt 20.0pt 36.0pt 20.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
@page Section2
	{size:609.0pt 935.0pt;
	margin:36.0pt 20.0pt 36.0pt 20.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section2
	{page:Section2;}
@page Section3
	{size:609.0pt 935.0pt;
	margin:36.0pt 20.0pt 36.0pt 20.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section3
	{page:Section3;}
@page Section4
	{size:609.0pt 935.0pt;
	margin:36.0pt 20.0pt 36.0pt 20.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section4
	{page:Section4;}
@page Section5
	{size:609.0pt 935.0pt;
	margin:36.0pt 20.0pt 36.0pt 20.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section5
	{page:Section5;}
.style1 {
	color: #000000;
	font-weight: bold;
	font-family: Arial, Arial, Helvetica, sans-serif;
	font-size: 13px;
}
.style2 {
	font-size: 13px;
	font-family: Arial, Arial, Helvetica, sans-serif;
	color: #000000;
}
.style5, .style5 p {font-family: Arial, Arial, Helvetica, sans-serif; font-size: 12px; }
.style13 {color: #000000}
.style14 {color: #FFFFFF}
.style17 {color: #FFFFFF; font-weight: bold; font-family: Arial, Arial, Helvetica, sans-serif; font-size: 13px; }
.style20 {font-family: Arial, Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; }
.style21 {
	font-family: Arial, Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style22 {font-family: Arial, Arial, Helvetica, sans-serif}
.style24 {
	font-size: 13px;
	color: #FFFFFF;
}
.style27 {color: #FFFFFF; font-weight: bold; font-family: Arial, Arial, Helvetica, sans-serif; font-size: 13px; }
.style54 {font-size: 12px;}
.tandatangan {font-family: Arial, Arial, Helvetica, sans-serif; font-size: 12px; }
.style-table01 { padding: 0px 5px 0px 5px;}
table {border-collapse:collapse;font-family:Arial, Geneva, sans-serif; font-size:12px;}
table td {font-family:Arial, Geneva, sans-serif; font-size:12px; font-weight:bold;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Arial";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>
<body lang=EN-US style='tab-interval:36.0pt'>
<?php
$where_antar['antar_mata_pelajaran.siswa_id']		= $siswa_id;
$where_antar['antar_mata_pelajaran.komponen_id']	= $komponen_id;
$where_antar['mata_pelajaran.pelajaran_sifat']		= "Tambahan";
$where_antar['antar_mata_pelajaran.tahun_kode']		= $tahun_kode;
$where_antar['antar_mata_pelajaran.semester_id']	= $semester_id;
$dataAMP = $this->Antar_mata_pelajaran_model->get_antar_mata_pelajaran("", $where_antar);

$dsEkskul 			= ($dataAMP)?json_decode($dataAMP->ekstrakurikuler):'';
$dsAbsensi 			= ($dataAMP)?json_decode($dataAMP->presensi):'';
$dsPrestasi 		= ($dataAMP)?json_decode($dataAMP->prestasi):'';
$CatatanWaliKelas 	= ($dataAMP)?$dataAMP->catatan_walikelas:'';
$CatatanOrangTua 	= ($dataAMP)?$dataAMP->catatan_orangtua:'';
$query_kkm = $this->db->query("SELECT nilai_kkm 
								FROM nilai 
								WHERE siswa_id = '{$siswa_id}'
									AND tahun_kode = '{$tahun_kode}'
									AND semester_id = '{$semester_id}'
									AND komponen_id = '{$komponen_id}'")->row();
$global_kkm	 = ($query_kkm)?$query_kkm->nilai_kkm:'';

$tingkat_nama	= $this->db->query("SELECT * FROM tingkat WHERE tingkat_id = '{$tingkat_id}'")->row()->tingkat_romawi;
$trim_kelas = explode(" ", $siswa->kelas_nama);
$new_semester = "";
if ($tingkat_nama == "VII" && $semester_id == 1){
	$new_semester = "1 (Satu)";
} else if ($tingkat_nama == "VII" && $semester_id == 2){
	$new_semester = "2 (Dua)";
} else if ($tingkat_nama == "VIII" && $semester_id == 1){
	$new_semester = "3 (Tiga)";
} else if ($tingkat_nama == "VII" && $semester_id == 2){
	$new_semester = "4 Empat)";
} else if ($tingkat_nama == "IX" && $semester_id == 1){
	$new_semester = "5 (Lima)";
} else if ($tingkat_nama == "IX" && $semester_id == 2){
	$new_semester = "6 (Enam)";
} else if ($tingkat_nama == "VII" && $semester_id == 1){
	$new_semester = "1 (Satu)";
} else if ($tingkat_nama == "VII" && $semester_id == 2){
	$new_semester = "2 (Dua)";
} else if ($tingkat_nama == "VIII" && $semester_id == 1){
	$new_semester = "3 (Tiga)";
} else if ($tingkat_nama == "VIII" && $semester_id == 2){
	$new_semester = "4 (Empat)";
} else if ($tingkat_nama == "IX" && $semester_id == 1){
	$new_semester = "5 (Lima)";
} else if ($tingkat_nama == "IX" && $semester_id == 2){
	$new_semester = "6 (Enam)";
}

?>

<div class=Section1>
<?=getHeader()?>
<table width="100%" border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
<tr>
<td style="padding:5px 0px 20px 0px;">
<table width="94%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top" style="padding: 0px;padding-bottom:20px;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:1px solid;">
			<tr>
				<td width="70%" valign="top">
					<table width="100%" border="0" cellpadding="1" cellspacing="0">
						<tr>
							<td width="30%"><span class="style5">Nama Sekolah</span></td>
							<td width="70%"><span class="style5">:&nbsp;<?=$nama_sekolah?></span></td>
						</tr>
						<tr>
							<td><span class="style5">Alamat</span></td>
							<td><span class="style5">:&nbsp;<?=$alamat_sekolah?></span></td>
						</tr>
						<tr>
							<td><span class="style5">Nama</span></td>
							<td><span class="style5">:&nbsp;<?=$siswa->siswa_nama?></span></td>
						</tr>
						<tr>
							<td><span class="style5">Nomor Induk / NISN</span></td>
							<td><span class="style5">:&nbsp;<?=$siswa->siswa_nis?> / <?=$siswa->siswa_nisn?></span></td>
						</tr>
					</table>
				</td>
				<td valign="top">
					<table width="100%" border="0" cellpadding="1" cellspacing="0">
						<tr>
							<td width="50%"><span class="style5">Kelas</span></td>
							<td width="50%"><span class="style5">:&nbsp;<?=$siswa->kelas_nama?></span></td>
						</tr>
						<tr>
							<td><span class="style5">Semester</span></td>
							<td><span class="style5">:&nbsp;<?=$new_semester?></span></td>
						</tr>
						<tr>
							<td><span class="style5">Tahun Pelajaran</span></td>
							<td><span class="style5">:&nbsp;<?=$siswa->tahun_nama?></span></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
		</td>
	</tr>
	<tr>
		<td class="style2">B. Pengetahuan dan Keterampilan</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="1" class="tab" bordercolor="#000000">
				<tr>
					<td rowspan="2" class="headerlong"><div align="center">NO</div></td>
					<td rowspan="2" class="headerlong"><div align="center">PELAJARAN</div></td>
					<td rowspan="2" class="headerlong"><div align="center">KKM</div></td>
					<td colspan="2" class="headerlong"><div align="center">PENGETAHUAN</div></td>
					<td colspan="2" class="headerlong"><div align="center">KETERAMPILAN</div></td>
				</tr>
				<tr>
					<td class="headerlong" width="80px"><div align="center">ANGKA</div></td>
					<td class="headerlong" width="80px"><div align="center">PREDIKAT</div></td>
					<td class="headerlong" width="80px"><div align="center">ANGKA</div></td>
					<td class="headerlong" width="80px"><div align="center">PREDIKAT</div></td>
				</tr>
				<?php				
				$grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
													FROM guru_pelajaran 
														LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
													WHERE guru_pelajaran_status = 'A' 
														AND tahun_kode = '{$tahun_kode}' 
														AND pelajaran_status = 'A' 
														AND pelajaran_sifat = 'Wajib' 
														AND pelajaran_kelompok != 'Kelompok D (Unggulan)'
													GROUP BY pelajaran_kelompok 
													ORDER BY pelajaran_kelompok ASC")->result();
				
				foreach($grid_kelompok as $row_kelompok){
					echo "<tr><td colspan=\"7\" height=\"30\"><div style=\"font-weight:bold;\">".strtoupper($row_kelompok->pelajaran_kelompok)."</div></td></tr>";
					if ($row_kelompok->pelajaran_peminatan){
						$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
															FROM guru_pelajaran 
																LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
															WHERE guru_pelajaran_status = 'A' 
																AND tahun_kode = '{$tahun_kode}' 
																AND pelajaran_status = 'A' 
																AND pelajaran_sifat = 'Wajib' 
																AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																GROUP BY pelajaran_peminatan
																ORDER BY peminatan.peminatan_urutan ASC")->result();
						$j = 1;
						foreach($grid_peminatan as $row_peminatan){
							$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
															FROM guru_pelajaran 
																LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
															WHERE guru_pelajaran_status = 'A'  
																AND tahun_kode = '{$tahun_kode}'
																AND guru_pelajaran.kelas_id = '{$kelas_id}'
																AND pelajaran_status = 'A' 
																AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																AND pelajaran_sifat = 'Wajib' 
															GROUP BY guru_pelajaran.pelajaran_id 
															ORDER BY pelajaran_urutan ASC")->result();
							if ($grid_mapel){
								echo "<tr height=\"20\"><td align=\"left\" colspan=\"7\" style=\"font-weight:bold;\">".konversi_romawi($j).'. '.$row_peminatan->pelajaran_peminatan."</td></tr>";
																					
								$i_mapel = 1;
								foreach($grid_mapel as $row_mapel){
									$get_nilai_ki3 = $this->db->query("SELECT *
																	FROM nilai
																	WHERE nilai.siswa_id='{$siswa_id}'
																		AND nilai.aspek_id='1'
																		AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																		AND nilai.tahun_kode='{$tahun_kode}'
																		AND nilai.semester_id='{$semester_id}'
																		AND nilai.komponen_id='{$komponen_id}'
																		AND nilai.nilai_angka > 0")->row();
									
									$get_nilai_ki4 = $this->db->query("SELECT *
																	FROM nilai
																	WHERE nilai.siswa_id='{$siswa_id}'
																		AND nilai.aspek_id='2'
																		AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																		AND nilai.tahun_kode='{$tahun_kode}'
																		AND nilai.semester_id='{$semester_id}'
																		AND nilai.komponen_id='{$komponen_id}'
																		AND nilai.nilai_angka > 0")->row();
																		
									$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
									$nilai_angka_ki3 = ($get_nilai_ki3 && $get_nilai_ki3->nilai_angka > 0)?$get_nilai_ki3->nilai_angka:'-';
									$nilai_huruf_ki3 = ($get_nilai_ki3 && $get_nilai_ki3->nilai_angka > 0)?konversi_grade($tahun_kode, $get_nilai_ki3->nilai_angka):'-';
									$nilai_angka_ki4 = ($get_nilai_ki4 && $get_nilai_ki4->nilai_angka > 0)?$get_nilai_ki4->nilai_angka:'-';
									$nilai_huruf_ki4 = ($get_nilai_ki4 && $get_nilai_ki4->nilai_angka > 0)?konversi_grade($tahun_kode, $get_nilai_ki4->nilai_angka):'-';
									
																		
									echo "<tr height=\"20\">
										<td align=\"center\" valign=\"top\" width=\"30\">".$i_mapel."</td>
										<td align=\"left\"><strong>".$row_mapel->pelajaran_nama."</strong><br />".$row_mapel->staf_nama."</td>
										<td align=\"center\">".$nilai_kkm."</td>
										<td align=\"center\">".$nilai_angka_ki3."</td>
										<td align=\"center\">".$nilai_huruf_ki3."</td>
										<td align=\"center\">".$nilai_angka_ki4."</td>
										<td align=\"center\">".$nilai_huruf_ki4."</td>
									</tr>";
									$i_mapel++;
								}
								$j++;
							}
						}
					} else {
						$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
														FROM guru_pelajaran 
															LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
															LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
														WHERE guru_pelajaran_status = 'A'  
															AND tahun_kode = '{$tahun_kode}'
															AND guru_pelajaran.kelas_id = '{$kelas_id}'
															AND pelajaran_status = 'A' 
															AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
															AND pelajaran_sifat = 'Wajib' 
														GROUP BY guru_pelajaran.pelajaran_id 
														ORDER BY pelajaran_urutan ASC")->result();
														
						$i_mapel = 1;
						foreach($grid_mapel as $row_mapel){
							$get_nilai_ki3 = $this->db->query("SELECT *
																FROM nilai
																WHERE nilai.siswa_id='{$siswa_id}'
																	AND nilai.aspek_id='1'
																	AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																	AND nilai.tahun_kode='{$tahun_kode}'
																	AND nilai.semester_id='{$semester_id}'
																	AND nilai.komponen_id='{$komponen_id}'")->row();
								
								$get_nilai_ki4 = $this->db->query("SELECT *
																FROM nilai
																WHERE nilai.siswa_id='{$siswa_id}'
																	AND nilai.aspek_id='2'
																	AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																	AND nilai.tahun_kode='{$tahun_kode}'
																	AND nilai.semester_id='{$semester_id}'
																	AND nilai.komponen_id='{$komponen_id}'")->row();
								$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
									$nilai_angka_ki3 = ($get_nilai_ki3 && $get_nilai_ki3->nilai_angka > 0)?$get_nilai_ki3->nilai_angka:'-';
									$nilai_huruf_ki3 = ($get_nilai_ki3 && $get_nilai_ki3->nilai_angka > 0)?konversi_grade($tahun_kode, $get_nilai_ki3->nilai_angka):'-';
									$nilai_angka_ki4 = ($get_nilai_ki4 && $get_nilai_ki4->nilai_angka > 0)?$get_nilai_ki4->nilai_angka:'-';
									$nilai_huruf_ki4 = ($get_nilai_ki4 && $get_nilai_ki4->nilai_angka > 0)?konversi_grade($tahun_kode, $get_nilai_ki4->nilai_angka):'-';
								
								echo "<tr height=\"20\">
									<td align=\"center\" valign=\"top\" width=\"30\">".$i_mapel."</td>
									<td align=\"left\"><strong>".$row_mapel->pelajaran_nama."</strong><br />".$row_mapel->staf_nama."</td>
									<td align=\"center\">".$nilai_kkm."</td>
									<td align=\"center\">".$nilai_angka_ki3."</td>
									<td align=\"center\">".$nilai_huruf_ki3."</td>
									<td align=\"center\">".$nilai_angka_ki4."</td>
									<td align=\"center\">".$nilai_huruf_ki4."</td>
								</tr>";
								$i_mapel++;
						}
					}
				}
				?>
			</table>
			<br />
			<p style="margin-bottom:5px;padding-bottom:5px;">Tabel interval predikat berdasarkan KKM</p>
			<table width="100%" border="1" style="border-collapse:collapse">
				<tr>
					<th align="center" width="20%" style="padding:1px 5px;">KKM</th>
					<?php
					$grid_grade = $this->db->query("SELECT * FROM grade WHERE tahun_kode = '{$tahun_kode}' ORDER BY grade_huruf DESC")->result();
					foreach($grid_grade as $row_grade){
						echo "<th align=\"center\" style=\"padding:1px 5px;\" width=\"20%\">".$row_grade->grade_huruf.'='.$row_grade->grade_nama."</th>";
					}
					?>
				</tr>
				<tr>
					<td align="center" style="padding:1px 5px;"><?php echo $global_kkm; ?></td>
					<?php
					$grid_grade = $this->db->query("SELECT * FROM grade WHERE tahun_kode = '{$tahun_kode}' ORDER BY grade_huruf DESC")->result();
					foreach($grid_grade as $row_grade){
						echo "<td align=\"center\" style=\"padding:1px 5px;\">".$row_grade->grade_batas_bawah.'<=Nilai<='.$row_grade->grade_batas_atas."</td>";
					}
					?>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	if (($tingkat_nama == "IX" && $semester_id == 1) || $tingkat_nama == "VII" || $tingkat_nama == "VIII"){
	if ($semester_id == 1){
	?>
	<tr>
		<td valign="top">
		<br />
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="tandatangan">
			<tr>
				<td style="padding-top:0px;" width="25%"><div align="left">Mengetahui:<br />Orang Tua/Wali,</div></td>
				<td style="padding-top:0px;">&nbsp;</td>
				<td style="padding-top:0px;" width="30%">Bandung, <?php echo dateIndo_(($tanggal_raport)?$tanggal_raport:date('Y-m-d')); ?><br />Wali Kelas <?=$kelas->kelas_nama?></td>
			</tr>
			<tr>
				<td colspan="3"><br /><br />&nbsp;</td>
			</tr>
			<tr>
				<td><div align="left"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></div></td>
				<td>&nbsp;</td>
				<td><u><?=$kelas->staf_nama?></u></td>
			</tr>
		</table>
		<br />
		</td>
	</tr>
	<?php } else if ($semester_id == 2){
	$kelasSplit = explode(" ", $kelas->kelas_nama);
	$namaTingkatBaru = "";
	if ($tingkat_nama == "VII"){
		$namaTingkatBaru = "VIII";
	} else if ($tingkat_nama == "VIII"){
		$namaTingkatBaru = "IX";
	}
	$namaPeminatan = "";
	if ($kelasSplit[1] == "IIS" || $kelasSplit[1] == "IPS"){
		$namaPeminatan = "IPS";
	} else if ($kelasSplit[1] == "MIA" || $kelasSplit[1] == "MIPA"){
		$namaPeminatan = "MIPA";
	}
	?>
	<tr>
		<td valign="top">
		<br />
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="tandatangan">
			<tr>
				<td style="padding-top:0px;vertical-align:bottom;" width="25%"><div align="left">Mengetahui:<br />Orang Tua/Wali,</div></td>
				<td style="padding-top:0px;vertical-align:bottom;text-align:center">Wali Kelas <?=$kelas->kelas_nama?>,</td>
				<td style="padding-top:0px;vertical-align:bottom;" width="40%">Keputusan:<br /><span style="font-weight:normal !important">Berdasarkan hasil yang dicapai pada semester ganjil dan genap, peserta didik ditetapkan</span> <br />Naik / Tidak Naik ke kelas <span style="font-weight:normal">.......</span> <?php echo $namaPeminatan; ?>.<br /><br />Bandung, <?php echo dateIndo_(($tanggal_raport)?$tanggal_raport:date('Y-m-d')); ?><br />Kepala Sekolah</td>
			</tr>
			<tr>
				<td colspan="3"><br /><br />&nbsp;</td>
			</tr>
			<tr>
				<td><div align="left"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></div></td>
				<td><div align="center"><u><?=$kelas->staf_nama?></u></div></td>
				<td><u><?=($kepala_sekolah)?$kepala_sekolah->staf_nama:'';?></u></td>
			</tr>
		</table>
		<br />
		</td>
	</tr>
	<?php } ?>
	<?php } else { ?>	
	<tr>
		<td valign="top">
		<br />
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="tandatangan">
			<tr>
				<td style="padding-top:0px;vertical-align:bottom;" width="25%"><div align="left">Mengetahui:<br />Orang Tua/Wali,</div></td>
				<td style="padding-top:0px;vertical-align:bottom;text-align:center">Wali Kelas <?=$kelas->kelas_nama?>,</td>
				<td style="padding-top:0px;vertical-align:bottom;" width="40%">Keputusan:<br /><span style="font-weight:normal !important">Berdasarkan hasil yang dicapai pada semester ganjil dan genap, peserta didik ditetapkan</span> <br />Lulus / Tidak Lulus.<br /><br />Bandung, <?php echo dateIndo_(($tanggal_raport)?$tanggal_raport:date('Y-m-d')); ?><br />Kepala Sekolah</td>
			</tr>
			<tr>
				<td colspan="3"><br /><br />&nbsp;</td>
			</tr>
			<tr>
				<td><div align="left"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></div></td>
				<td><div align="center"><u><?=$kelas->staf_nama?></u></div></td>
				<td><u><?=($kepala_sekolah)?$kepala_sekolah->staf_nama:'';?></u></td>
			</tr>
		</table>
		<br />
		</td>
	</tr>
	<?php } ?>
</table>
</td>
</tr>
</table>

</div>
<!-- /////////////////////////////////////////////////////////////////////////////// -->
</body>
</html>