<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('peminatan/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "peminatan_singkat", "width": "25%" },
			{ "data": "peminatan_nama"},
			{ "data": "peminatan_urutan"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Peminatan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('peminatan'); ?>">Peminatan</a></li>
            <li class="active">Daftar Peminatan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Peminatan</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Singkat</th>
                        <th>Nama</th>
                        <th>Urutan</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Peminatan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('peminatan'); ?>">Peminatan</a></li>
            <li class="active">Tambah Peminatan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Peminatan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<label for="peminatan_singkat" class="col-sm-2 control-label">Singkat</label>
							<div class="col-md-2 col-sm-4">
								<input type="text" class="form-control" name="peminatan_singkat" id="peminatan_singkat" value="<?php echo $peminatan_singkat; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="peminatan_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="peminatan_nama" id="peminatan_nama" value="<?php echo $peminatan_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="peminatan_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-md-2 col-sm-3">
								<input type="number" class="form-control" name="peminatan_urutan" id="peminatan_urutan" value="<?php echo $peminatan_urutan; ?>" placeholder="" required>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Peminatan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('peminatan'); ?>">Peminatan</a></li>
            <li class="active">Ubah Peminatan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Peminatan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$peminatan_id);?>" method="post">
					<input type="hidden" class="form-control" name="peminatan_id" id="peminatan_id" value="<?php echo $peminatan_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="peminatan_singkat" class="col-sm-2 control-label">Singkat</label>
							<div class="col-md-2 col-sm-4">
								<input type="text" class="form-control" name="peminatan_singkat" id="peminatan_singkat" value="<?php echo $peminatan_singkat; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="peminatan_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="peminatan_nama" id="peminatan_nama" value="<?php echo $peminatan_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="peminatan_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-md-2 col-sm-3">
								<input type="number" class="form-control" name="peminatan_urutan" id="peminatan_urutan" value="<?php echo $peminatan_urutan; ?>" placeholder="" required>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Peminatan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('peminatan'); ?>">Peminatan</a></li>
            <li class="active">Detail Peminatan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Peminatan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="peminatan_id" id="peminatan_id" value="<?php echo $peminatan_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group row">
							<label for="peminatan_singkat" class="col-sm-2 control-label">Singkat</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $peminatan_singkat; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="peminatan_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $peminatan_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="peminatan_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $peminatan_urutan; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$peminatan_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>