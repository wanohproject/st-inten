<?php
if ($action == '' || $action == 'grid'){
?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Penentuan Kelas
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('penentuan-kelas'); ?>">Penentuan Kelas</a></li>
            <li class="active">Daftar Penentuan Kelas</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
				<form action="<?php echo module_url($this->uri->segment(2));?>" method="post">
                <div class="box-header">
					<h3 class="box-title">Pilih Penentuan Kelas</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-body">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label for="tahun_kode" class="control-label">TA Sumber</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<label for="tahun_depan_id" class="control-label">TA Tujuan</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_depan_id', 'tahun_kode', 'tahun_nama', $tahun_depan_id, 'submit();', 'none', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group">
									<label for="kenaikan_jenis" class="control-label">Sumber Siswa</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tingkat ORDER BY tingkat_nama ASC")->result(), 'kenaikan_jenis', 'tingkat_id', 'tingkat_nama',  $kenaikan_jenis, 'submit();', 'Siswa Baru', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
							
							<?php if ($kenaikan_jenis){ ?>
							<div class="col-md-3">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tingkat_id = '$kenaikan_jenis' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama',  $kelas_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>
								</div>
							</div>
							<?php }?>
						</div>
					<div class="box-header">
						<h3 class="box-title">Data Siswa</h3>
					</div><!-- /.box-header -->
					<?php if ($kenaikan_jenis == '' || !isset($kenaikan_jenis)){ ?>
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th width="50">No</th>
									<th width="150">NIS</th>
									<th>NAMA</th>
									<th width="150">KELAS</th>
								</tr>
							</thead>                        
							<tbody>
							<?php
							$query = $this->db->query("SELECT * FROM siswa WHERE siswa_status = 'Siswa' AND (SELECT COUNT(siswa_id) FROM siswa_kelas WHERE siswa_kelas.siswa_id=siswa.siswa_id) < 1 ORDER BY siswa_nama ASC");
							$i = 1;
							if ($query->num_rows() > 0) {
								foreach($query->result() as $row) {
									$query_kelas = $this->db->query("SELECT * FROM siswa_kelas WHERE siswa_id='$row->siswa_id' AND tahun_kode = '$tahun_depan_id' AND kelas_id = '$kelas_id'")->row();
									?>
									<tr>
										<td style="text-align:center"><?php echo $i; ?></td>
										<td style="text-align:center"><?php echo $row->siswa_nis; ?></td>
										<td><?php echo $row->siswa_nama; ?></td>
										<td style="text-align:center">
											<?php $this->Kelas_model->combobox_kelas('data_asal['.$row->siswa_id.']', $kelas_id, ($query_kelas)?$query_kelas->kelas_id:'', '', 'class="form-control select2"', $tahun_kode); ?>
											<?php //combobox('db', $this->db->query("SELECT * FROM kelas")->result(), 'data_asal['.$row->siswa_id.']', 'kelas_id', 'kelas_nama', '', '', '', 'class="form-control" required'); ?>
										</td>
									</tr>
									<?php
									$i++;
								}
							}
							?>
							</tbody>
						</table>
					</div><!-- /.box-body -->
					<?php } else if ($kenaikan_jenis){ ?>
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th width="50">No</th>
									<th width="150">NIS</th>
									<th>NAMA</th>
									<th width="150">KELAS</th>
								</tr>
							</thead>                        
							<tbody>
							<?php
							$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_status = 'Siswa' AND siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa_nama ASC");
							$i = 1;
							if ($query->num_rows() > 0) {
								foreach($query->result() as $row) {
									$query_kelas = $this->db->query("SELECT * FROM siswa_kelas WHERE siswa_id='$row->siswa_id' AND tahun_kode = '$tahun_depan_id' AND kelas_id = '$kelas_id'")->row();
									?>
									<tr>
										<td style="text-align:center"><?php echo $i; ?></td>
										<td style="text-align:center"><?php echo $row->siswa_nis; ?></td>
										<td><?php echo $row->siswa_nama; ?></td>
										<td style="text-align:center">
											<?php $this->Kelas_model->combobox_kelas('data_asal['.$row->siswa_id.']', $kelas_id, ($query_kelas)?$query_kelas->kelas_id:'', '', 'class="form-control select2"', $tahun_kode); ?>
											<?php //combobox('db', $this->db->query("SELECT * FROM kelas")->result(), 'data_asal['.$row->siswa_id.']', 'kelas_id', 'kelas_nama', '', '', '', 'class="form-control" required'); ?>
										</td>
									</tr>
									<?php
								$i++;
								}
							}
							?>
							</tbody>
						</table>
					</div><!-- /.box-body -->
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>