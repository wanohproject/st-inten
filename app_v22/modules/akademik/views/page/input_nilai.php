<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url($this->uri->segment(2).'/datatable/'.$program_studi_id.'/'.$semester_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "matakuliah_kode"},
			{ "data": "matakuliah_nama"},
			{ "data": "matakuliah_sks"},
			{ "data": "matakuliah_semester_no"},
			{ "data": "dosen_nama"},
			{ "data": "matakuliah_peserta"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Nilai Matakuliah
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Nilai Matakuliah</a></li>
            <li class="active">Daftar Nilai Matakuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Nilai Matakuliah</h3>
                </div><!-- /.box-header -->
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$semester_kode);?>" method="post">
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="program_studi_id" class="control-label">Program Studi</label>
												<div class="">
													<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="semester_kode" class="control-label">Semester Pelaporan</label>
												<div class="">	
													<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', '-- PILIH SEMESTER PELAPORAN --', 'class="form-control" required');?>
												</div>		
											</div>
										</div>
									</div>
								</div>
								</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="60">Kode</th>
                        <th>Nama</th>
                        <th width="60">SKS</th>
                        <th width="60">Semester</th>
                        <th>Pengampu</th>
                        <th width="60">Peserta</th>
                        <th style="width:60px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<script>
  $(function () {
	
		<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
			<?php if ($this->session->flashdata('success')) { ?>
				$('#successModal').modal('show');
			<?php } else { ?>
				$('#errorModal').modal('show');
			<?php } ?>
		<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mata Kuliah
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Mata Kuliah</a></li>
            <li class="active">Detail Mata Kuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
				<form action="<?php echo module_url($this->uri->segment(2).'/detail/'.$matakuliah->matakuliah_id);?>" method="post">
					<input type="hidden" name="matakuliah_id" value="<?php echo $matakuliah->matakuliah_id; ?>">
					<div class="row">
            <div class="col-xs-12">
							<div class="box box-primary with-border">
								<div class="box-header with-border">
                  <h3 class="box-title">Detail Mata Kuliah</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div class="row">
										<label for="matakuliah_kode" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Kode</label>
										<div class="col-xs-8 col-md-4">
											<label for="matakuliah_kode" style="text-align:left;">: <?php echo $matakuliah->matakuliah_kode; ?></label>
										</div>
										<label for="matakuliah_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Nama</label>
										<div class="col-xs-8 col-md-4">
											<label for="matakuliah_nama" style="text-align:left;">: <?php echo $matakuliah->matakuliah_nama; ?></label>
										</div>
									</div>
									<div class="row">
										<label for="program_studi_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Program Studi</label>
										<div class="col-xs-8 col-md-4">
											<label for="program_studi_nama" style="text-align:left;">: <?php echo $matakuliah->program_studi_nama; ?></label>
										</div>
										<label for="semester_nama" class="col-xs-4 col-md-2 control-label" style="text-align:left;">Periode</label>
										<div class="col-xs-8 col-md-4">
											<label for="semester_nama" style="text-align:left;">: <?php echo $matakuliah->semester_nama; ?></label>										
										</div>
									</div>
								</div>
								<div class="box-header with-border">
                  <h3 class="box-title">Input Nilai Mata Kuliah</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div style="overflow-x:auto;">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th width="30">#</th>
													<th width="150">NIM</th>
													<th>NAMA</th>
													<th width="150">NILAI</th>
												</tr>
											</thead>
											<tbody>
											<?php
											$grid_mahasiswa = $this->Mahasiswa_model->grid_all_mahasiswa_nilai("", "mahasiswa_nim", "ASC", 0, 0, array('mahasiswa_nilai.semester_kode'=>$matakuliah->semester_kode, 'mahasiswa_nilai.matakuliah_id'=>$matakuliah->matakuliah_id));
											if ($grid_mahasiswa){
												$i = 1;
												foreach ($grid_mahasiswa as $row_mahasiswa) {
													?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $row_mahasiswa->mahasiswa_nim; ?></td>
														<td><?php echo $row_mahasiswa->mahasiswa_nama; ?></td>
														<td><?php combobox('db', $this->Bobot_nilai_model->grid_all_bobot_nilai("", "bobot_nilai_value", "DESC"), "nilai[$row_mahasiswa->mahasiswa_nilai_id]", "bobot_nilai_huruf", "bobot_nilai_huruf", $row_mahasiswa->mahasiswa_nilai_huruf, '', '', 'class="form-control"'); ?></td>
													</tr>
													<?php
													$i++;
												}
											}
											?>
											</tbody>
										</table>
									</div>
								</div>
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
					<div class="row">
            <div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<?php if (check_permission("W")){?>
									<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
									<?php } ?>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$matakuliah->program_studi_id.'/'.$matakuliah->semester_kode); ?>'" class="btn btn-default">Kembali</button>
								</div><!-- /.box-footer -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
				</form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	
			<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>