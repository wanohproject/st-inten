<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('matakuliah/datatable/'.$program_studi_id.'/'.$semester_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "matakuliah_kode"},
			{ "data": "matakuliah_nama"},
			{ "data": "matakuliah_sks"},
			{ "data": "matakuliah_semester_no"},
			{ "data": "semester_kode"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Matakuliah
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Matakuliah</a></li>
            <li class="active">Daftar Matakuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Matakuliah</h3>
									<div class="pull-right">
									<a href="<?php echo base_url('asset/file/format_import_matakuliah.xls'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-download"></span> Download Format Import</a>
									<a href="<?php echo module_url($this->uri->segment(2).'/import'); ?>" class="btn btn-primary"><span class="fa fa-download"></span> Import Matakuliah</a>
									<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
									</div>
                </div><!-- /.box-header -->
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$semester_kode);?>" method="post">
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="program_studi_id" class="control-label">Program Studi</label>
												<div class="">
													<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="semester_kode" class="control-label">Semester Pelaporan</label>
												<div class="">	
													<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', '-- PILIH SEMESTER PELAPORAN --', 'class="form-control" required');?>
												</div>		
											</div>
										</div>
									</div>
								</div>
								</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>SKS</th>
                        <th>Semester</th>
                        <th>Semester Pelaporan</th>
                        <th style="width:130px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<script>
$(function () {
	$("#matakuliah_kode, #semester_kode").change(function(){
		if ($("#program_studi_id").val() != "" && $("#semester_kode").val() != "" && $("#matakuliah_kode").val() != ""){
			var params = {
				action: 'add',
				semester: $("#semester_kode").val(),
				program_studi: $("#program_studi_id").val(),
				kode: $("#matakuliah_kode").val()
			};
			// console.log(params);
			
			$.ajax({
				url: "<?php echo module_url($this->uri->segment(2).'/check-kode'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				beforeSend: function() {},
				success: function(data){
					if (data.response == false){
						$("#matakuliah_kode").css("border-color", "#a94442");
						$("#form-add").attr('onsubmit', 'return false;');
					} else {
						$("#matakuliah_kode").css("border-color", "#5cb85c");
						$("#form-add").attr('onsubmit', 'return true;');
					}
				},
				complete: function() {},
			})
		}
	});
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Matakuliah
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Matakuliah</a></li>
            <li class="active">Tambah Matakuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Matakuliah</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="semester_kode" class="col-md-2 control-label" style="text-align:left">Semester Pelaporan</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="matakuliah_kode" class="col-md-2 control-label" style="text-align:left">Kode</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="matakuliah_kode" id="matakuliah_kode" value="<?php echo $matakuliah_kode; ?>" placeholder="" required>
						</div>
						<label for="matakuliah_semester_no" class="col-md-2 control-label" style="text-align:left">Semester</label>
						<div class="col-md-4">
							<?php echo combobox('1d', range(1, 14), 'matakuliah_semester_no', 'matakuliah_semester_no', 'semester_nama', $matakuliah_semester_no, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="matakuliah_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="matakuliah_nama" id="matakuliah_nama" value="<?php echo $matakuliah_nama; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="kelompok_matakuliah_kode" class="col-md-2 control-label" style="text-align:left">Kelompok Mata Kuliah</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(10, 'kelompok_matakuliah_kode', 'kode_value', 'kode_nama', $kelompok_matakuliah_kode, '', '', 'class="form-control"');?>
						</div>
						<label for="kurikulum_jenis_kode" class="col-md-2 control-label" style="text-align:left">Jenis Kurikulum</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(11, 'kurikulum_jenis_kode', 'kode_value', 'kode_nama', $kurikulum_jenis_kode, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="matakuliah_jenis_kode" class="col-md-2 control-label" style="text-align:left">Jenis Mata Kuliah</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(28, 'matakuliah_jenis_kode', 'kode_value', 'kode_nama', $matakuliah_jenis_kode, '', '', 'class="form-control"');?>
						</div>
						<label for="matakuliah_status_kode" class="col-md-2 control-label" style="text-align:left">Status Mata Kuliah</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(14, 'matakuliah_status_kode', 'kode_value', 'kode_nama', $matakuliah_status_kode, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="matakuliah_sks" class="col-md-2 control-label" style="text-align:left">SKS Mata Kuliah</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="matakuliah_sks" id="matakuliah_sks" value="<?php echo $matakuliah_sks; ?>" placeholder="">
						</div>
						<label for="matakuliah_sks_tatap_muka" class="col-md-2 control-label" style="text-align:left">SKS Tatap Muka</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="matakuliah_sks_tatap_muka" id="matakuliah_sks_tatap_muka" value="<?php echo $matakuliah_sks_tatap_muka; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="matakuliah_sks_praktikum" class="col-md-2 control-label" style="text-align:left">SKS Praktikum</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="matakuliah_sks_praktikum" id="matakuliah_sks_praktikum" value="<?php echo $matakuliah_sks_praktikum; ?>" placeholder="">
						</div>
						<label for="matakuliah_sks_lapangan" class="col-md-2 control-label" style="text-align:left">SKS Praktik Lapangan</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="matakuliah_sks_lapangan" id="matakuliah_sks_lapangan" value="<?php echo $matakuliah_sks_lapangan; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="matakuliah_silabus" class="col-md-2 control-label" style="text-align:left">Ada Silabus?</label>
						<div class="col-md-4">
							<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_silabus', 'kode_value', 'kode_nama', $matakuliah_silabus, '', '', 'class="form-control"');?>
						</div>
						<label for="matakuliah_sap" class="col-md-2 control-label" style="text-align:left">Ada SAP?</label>
						<div class="col-md-4">
							<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_sap', 'kode_value', 'kode_nama', $matakuliah_sap, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="matakuliah_bahan" class="col-md-2 control-label" style="text-align:left">Ada Bahan?</label>
						<div class="col-md-4">
							<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_bahan', 'kode_value', 'kode_nama', $matakuliah_bahan, '', '', 'class="form-control"');?>
						</div>
						<label for="matakuliah_diktat" class="col-md-2 control-label" style="text-align:left">Ada Diktat?</label>
						<div class="col-md-4">
							<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_diktat', 'kode_value', 'kode_nama', $matakuliah_diktat, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_id" class="col-md-2 control-label" style="text-align:left">Dosen Pengampu</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Dosen_model->grid_all_dosen('', 'dosen_nama', 'ASC', '', '', "dosen.program_studi_id = '$program_studi_id' AND dosen.status_aktivitas_kode = 'A'"), 'dosen_id', 'dosen_id', 'dosen_nama', $dosen_id, '', '', 'class="form-control"');?>
						</div>
						<label for="dosen_mengajar_kelas" class="col-md-2 control-label" style="text-align:left">Mengajar Kelas</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_mengajar_kelas" id="dosen_mengajar_kelas" value="<?php echo $dosen_mengajar_kelas; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_mengajar_rencana_pertemuan" class="col-md-2 control-label" style="text-align:left">Rencana Pertemuan</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="dosen_mengajar_rencana_pertemuan" id="dosen_mengajar_rencana_pertemuan" value="<?php echo $dosen_mengajar_rencana_pertemuan; ?>" placeholder="">
						</div>
						<label for="dosen_mengajar_realisasi_pertemuan" class="col-md-2 control-label" style="text-align:left">Realisasi Pertemuan</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="dosen_mengajar_realisasi_pertemuan" id="dosen_mengajar_realisasi_pertemuan" value="<?php echo $dosen_mengajar_realisasi_pertemuan; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<script>
$(function () {
	$("#matakuliah_kode, #semester_kode").change(function(){
		if ($("#program_studi_id").val() != "" && $("#semester_kode").val() != "" && $("#matakuliah_kode").val() != ""){
			var params = {
				action: 'edit',
				id: '<?php echo $matakuliah_id; ?>',
				semester: $("#semester_kode").val(),
				program_studi: $("#program_studi_id").val(),
				kode: $("#matakuliah_kode").val()
			};
			// console.log(params);
			
			$.ajax({
				url: "<?php echo module_url($this->uri->segment(2).'/check-kode'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				beforeSend: function() {},
				success: function(data){
					if (data.response == false){
						$("#matakuliah_kode").css("border-color", "#a94442");
						$("#form-add").attr('onsubmit', 'return false;');
					} else {
						$("#matakuliah_kode").css("border-color", "#5cb85c");
						$("#form-add").attr('onsubmit', 'return true;');
					}
				},
				complete: function() {},
			});
		}
	});
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Matakuliah
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Matakuliah</a></li>
            <li class="active">Ubah Matakuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Matakuliah</h3>
                </div><!-- /.box-header -->
								<form id="form-add" class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$matakuliah_id);?>" method="post">
								<input type="hidden" class="form-control" name="matakuliah_id" id="matakuliah_id" value="<?php echo $matakuliah_id; ?>" placeholder="">
								<div class="box-body">
									<div class="form-group">
										<label for="semester_kode" class="col-md-2 control-label" style="text-align:left">Semester Pelaporan</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="matakuliah_kode" class="col-md-2 control-label" style="text-align:left">Kode</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="matakuliah_kode" id="matakuliah_kode" value="<?php echo $matakuliah_kode; ?>" placeholder="" required>
										</div>
										<label for="matakuliah_semester_no" class="col-md-2 control-label" style="text-align:left">Semester</label>
										<div class="col-md-4">
											<?php echo combobox('1d', range(1, 14), 'matakuliah_semester_no', 'semester_kode', 'semester_nama', $matakuliah_semester_no, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="matakuliah_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
										<div class="col-md-10">
											<input type="text" class="form-control" name="matakuliah_nama" id="matakuliah_nama" value="<?php echo $matakuliah_nama; ?>" placeholder="" required>
										</div>
									</div>
									<div class="form-group">
										<label for="kelompok_matakuliah_kode" class="col-md-2 control-label" style="text-align:left">Kelompok Mata Kuliah</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(10, 'kelompok_matakuliah_kode', 'kode_value', 'kode_nama', $kelompok_matakuliah_kode, '', '', 'class="form-control"');?>
										</div>
										<label for="kurikulum_jenis_kode" class="col-md-2 control-label" style="text-align:left">Jenis Kurikulum</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(11, 'kurikulum_jenis_kode', 'kode_value', 'kode_nama', $kurikulum_jenis_kode, '', '', 'class="form-control"');?>
										</div>
									</div>
									<div class="form-group">
										<label for="matakuliah_jenis_kode" class="col-md-2 control-label" style="text-align:left">Jenis Mata Kuliah</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(28, 'matakuliah_jenis_kode', 'kode_value', 'kode_nama', $matakuliah_jenis_kode, '', '', 'class="form-control"');?>
										</div>
										<label for="matakuliah_status_kode" class="col-md-2 control-label" style="text-align:left">Status Mata Kuliah</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(14, 'matakuliah_status_kode', 'kode_value', 'kode_nama', $matakuliah_status_kode, '', '', 'class="form-control"');?>
										</div>
									</div>
									<div class="form-group">
										<label for="matakuliah_sks" class="col-md-2 control-label" style="text-align:left">SKS Mata Kuliah</label>
										<div class="col-md-4">
											<input type="number" class="form-control" name="matakuliah_sks" id="matakuliah_sks" value="<?php echo $matakuliah_sks; ?>" placeholder="">
										</div>
										<label for="matakuliah_sks_tatap_muka" class="col-md-2 control-label" style="text-align:left">SKS Tatap Muka</label>
										<div class="col-md-4">
											<input type="number" class="form-control" name="matakuliah_sks_tatap_muka" id="matakuliah_sks_tatap_muka" value="<?php echo $matakuliah_sks_tatap_muka; ?>" placeholder="">
										</div>
									</div>
									<div class="form-group">
										<label for="matakuliah_sks_praktikum" class="col-md-2 control-label" style="text-align:left">SKS Praktikum</label>
										<div class="col-md-4">
											<input type="number" class="form-control" name="matakuliah_sks_praktikum" id="matakuliah_sks_praktikum" value="<?php echo $matakuliah_sks_praktikum; ?>" placeholder="">
										</div>
										<label for="matakuliah_sks_lapangan" class="col-md-2 control-label" style="text-align:left">SKS Praktik Lapangan</label>
										<div class="col-md-4">
											<input type="number" class="form-control" name="matakuliah_sks_lapangan" id="matakuliah_sks_lapangan" value="<?php echo $matakuliah_sks_lapangan; ?>" placeholder="">
										</div>
									</div>
									<div class="form-group">
										<label for="matakuliah_silabus" class="col-md-2 control-label" style="text-align:left">Ada Silabus?</label>
										<div class="col-md-4">
											<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_silabus', 'kode_value', 'kode_nama', $matakuliah_silabus, '', '', 'class="form-control"');?>
										</div>
										<label for="matakuliah_sap" class="col-md-2 control-label" style="text-align:left">Ada SAP?</label>
										<div class="col-md-4">
											<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_sap', 'kode_value', 'kode_nama', $matakuliah_sap, '', '', 'class="form-control"');?>
										</div>
									</div>
									<div class="form-group">
										<label for="matakuliah_bahan" class="col-md-2 control-label" style="text-align:left">Ada Bahan?</label>
										<div class="col-md-4">
											<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_bahan', 'kode_value', 'kode_nama', $matakuliah_bahan, '', '', 'class="form-control"');?>
										</div>
										<label for="matakuliah_diktat" class="col-md-2 control-label" style="text-align:left">Ada Diktat?</label>
										<div class="col-md-4">
											<?php radiobutton('db', $this->Referensi_model->grid_all_kode('', 'kode_value', 'DESC', 0, 0, "jenis.jenis_value = '72'"), 'matakuliah_diktat', 'kode_value', 'kode_nama', $matakuliah_diktat, '', '', 'class="form-control"');?>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_id" class="col-md-2 control-label" style="text-align:left">Dosen Pengampu</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Dosen_model->grid_all_dosen('', 'dosen_nama', 'ASC', '', '', "dosen.program_studi_id = '$program_studi_id' AND dosen.status_aktivitas_kode = 'A'"), 'dosen_id', 'dosen_id', 'dosen_nama', $dosen_id, '', '', 'class="form-control"');?>
										</div>
										<label for="dosen_mengajar_kelas" class="col-md-2 control-label" style="text-align:left">Mengajar Kelas</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_mengajar_kelas" id="dosen_mengajar_kelas" value="<?php echo $dosen_mengajar_kelas; ?>" placeholder="">
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_mengajar_rencana_pertemuan" class="col-md-2 control-label" style="text-align:left">Rencana Pertemuan</label>
										<div class="col-md-4">
											<input type="number" class="form-control" name="dosen_mengajar_rencana_pertemuan" id="dosen_mengajar_rencana_pertemuan" value="<?php echo $dosen_mengajar_rencana_pertemuan; ?>" placeholder="">
										</div>
										<label for="dosen_mengajar_realisasi_pertemuan" class="col-md-2 control-label" style="text-align:left">Realisasi Pertemuan</label>
										<div class="col-md-4">
											<input type="number" class="form-control" name="dosen_mengajar_realisasi_pertemuan" id="dosen_mengajar_realisasi_pertemuan" value="<?php echo $dosen_mengajar_realisasi_pertemuan; ?>" placeholder="">
										</div>
									</div>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
								</div><!-- /.box-footer -->
								</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Matakuliah
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Matakuliah</a></li>
            <li class="active">Import Matakuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Matakuliah</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
								<span class="input-group-btn">
									<input type="submit" name="importMatakuliah" value="Import" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetMatakuliah" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
									<th nowrap>#</th>
									<th nowrap>PROGRAM STUDI</th>
									<th nowrap>SEMESTER PELAPORAN</th>
									<th nowrap>KODE MATA KULIAH</th>
									<th nowrap>NAMA MATA KULIAH</th>
									<th nowrap>SEMESTER</th>
									<th nowrap>KELOMPOK MATA KULIAH</th>
									<th nowrap>JENIS KURIKULUM</th>
									<th nowrap>JENIS MATA KULIAH</th>
									<th nowrap>STATUS MATA KULIAH</th>
									<th nowrap>SKS MATA KULIAH</th>
									<th nowrap>SKS TATAP MUKA</th>
									<th nowrap>SKS PRAKTIKUM</th>
									<th nowrap>SKS PRAKTIK LAPANGAN</th>
									<th nowrap>ADA SILABUS</th>
									<th nowrap>ADA SAP</th>
									<th nowrap>ADA BAHAN</th>
									<th nowrap>ADA DIKTAT</th>
									<th nowrap>DOSEN PELAPORAN</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
									$dataprodi = "";
									$datastatus = "";
									if ($row['data_prodi'] == 'PRODI VALID'){
										$dataprodi = 'style="background:#B1E3B1"';
									} else {
										$dataprodi = 'style="background:#EEB9B9"';
									}
									?>
									<tr <?php echo $dataprodi; ?>>
									  <th scope="row"><?php echo $i; ?></th>
									  <td nowrap><?php echo $row['matakuliah_prodi']; ?></td>
									  <td nowrap><?php echo $row['semester_kode']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_kode']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_nama']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_semester_no']; ?></td>
									  <td nowrap><?php echo $row['kelompok_matakuliah_kode']; ?></td>
									  <td nowrap><?php echo $row['kurikulum_jenis_kode']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_jenis_kode']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_status_kode']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_sks']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_sks_tatap_muka']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_sks_praktikum']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_sks_lapangan']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_silabus']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_sap']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_bahan']; ?></td>
									  <td nowrap><?php echo $row['matakuliah_diktat']; ?></td>
									  <td nowrap><?php echo $row['dosen_id']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
		
		var table_pengajar = $("#datagrid_pengajar").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url" : "<?php echo module_url($this->uri->segment(2).'/datatable_pengajar/'.$matakuliah_id); ?>",
				"type" : "POST",
			},
			"columns": [
				{ "data": "dosen_nama"},
				{ "data": "dosen_mengajar_rencana_pertemuan"},
				{ "data": "dosen_mengajar_realisasi_pertemuan"},
				{ "data": "dosen_mengajar_kelas"},
				{ "data": "Actions"}
			],
			"language": {
				"emptyTable": "Tidak ada data pada tabel ini",
				"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
				"infoEmpty": "Tidak ada data yang sesuai",
				"infoFiltered": "(hasil pencarian dari _MAX_ data)",
				"lengthMenu": "Tampil _MENU_  baris",
				"search": "Cari: ",
				"zeroRecords": "Tidak ada baris yang sesuai"
			},
			"sScrollX": "100%",
				"sScrollXInner": "100%",
				"bScrollCollapse": true,
			"lengthMenu": [
				[10, 20, 30, -1],
				[10, 20, 30, "All"] // change per page values here
			],
			"order": [
				[0, 'asc']
			],
			"pageLength": 10,
				"columnDefs": [{
				'orderable': false,
				'targets': [-1]
			}, {
				"searchable": false,
				"targets": [-1]
			}]
		});

		<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
			<?php if ($this->session->flashdata('success')) { ?>
				$('#successModal').modal('show');
			<?php } else { ?>
				$('#errorModal').modal('show');
			<?php } ?>
		<?php } ?>

		$( "#addBtn" ).click(function() {
			var tabs = $(this).attr("data-tabs");
			if (tabs == 1){
				$('#addDosenModal').modal('show');
			}
		});
		
		table_pengajar.on('click', 'a.btn-edit', function () {
				var data_id = $(this).attr("data-id");
				$("#ek_id").val(data_id);
				var data_id = $("#ek_id").val();
				$.ajax({ 
						type: 'GET', 
						url: '<?php echo module_url($this->uri->segment(2).'/get-pengajar'); ?>/' + data_id,
						dataType: 'json',
						success: function (data) { 
								if (data.response == true){
									$("#ek_dosen_id").val(data.data.dosen_id);
									$("#ek_rencana_pertemuan").val(data.data.dosen_mengajar_rencana_pertemuan);
									$("#ek_realisasi_pertemuan").val(data.data.dosen_mengajar_realisasi_pertemuan);
									$("#ek_mengajar_kelas").val(data.data.dosen_mengajar_kelas);
								}
						}
				});
        $('#editDosenModal').modal('show');
    });
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Matakuliah
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Matakuliah</a></li>
            <li class="active">Detail Matakuliah</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
					<div class="row">
            <div class="col-xs-12">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs" role="tablist" id="myTab">
									<li <?php echo ($tabs == 0)?' class="active"':''; ?>><a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$matakuliah_id); ?>?tabs=0">Detail Matakuliah</a></li>
									<?php if ($tabs != 0 && check_permission("W")){ ?>
									<li class="pull-right"><a href="#" class="text-default" id="addBtn" data-tabs="<?php echo $tabs; ?>"><i class="fa fa-plus"></i> Tambah</a></li>
									<?php } ?>
								</ul>
								<div class="tab-content">
									<div class="tab-pane <?php echo ($tabs == 0)?'active':''; ?>" id="tab_1">
										<div class="box-body form-horizontal">
											<div class="form-group">
												<label for="semester_kode" class="col-md-2 control-label" style="text-align:left">Semester Pelaporan</label>
												<div class="col-md-4">
													<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, '', '', 'class="form-control" disabled');?>
												</div>
											</div>
											<div class="form-group">
												<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
												<div class="col-md-4">
													<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '', 'class="form-control" disabled');?>
												</div>
											</div>
											<div class="form-group">
												<label for="matakuliah_kode" class="col-md-2 control-label" style="text-align:left">Kode</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="matakuliah_kode" id="matakuliah_kode" value="<?php echo $matakuliah_kode; ?>" placeholder="" readonly>
												</div>
												<label for="matakuliah_semester_no" class="col-md-2 control-label" style="text-align:left">Semester</label>
												<div class="col-md-4">
													<?php echo combobox('1d', range(1, 14), 'matakuliah_semester_no', 'semester_kode', 'semester_nama', $matakuliah_semester_no, '', '', 'class="form-control" disabled');?>
												</div>
											</div>
											<div class="form-group">
												<label for="matakuliah_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
												<div class="col-md-10">
													<input type="text" class="form-control" name="matakuliah_nama" id="matakuliah_nama" value="<?php echo $matakuliah_nama; ?>" placeholder="" readonly>
												</div>
											</div>
											<div class="form-group">
												<label for="kelompok_matakuliah_kode" class="col-md-2 control-label" style="text-align:left">Kelompok Mata Kuliah</label>
												<div class="col-md-4">
													<?php echo $this->Referensi_model->combobox(10, 'kelompok_matakuliah_kode', 'kode_value', 'kode_nama', $kelompok_matakuliah_kode, '', '', 'class="form-control" disabled');?>
												</div>
												<label for="kurikulum_jenis_kode" class="col-md-2 control-label" style="text-align:left">Jenis Kurikulum</label>
												<div class="col-md-4">
													<?php echo $this->Referensi_model->combobox(11, 'kurikulum_jenis_kode', 'kode_value', 'kode_nama', $kurikulum_jenis_kode, '', '', 'class="form-control" disabled');?>
												</div>
											</div>
											<div class="form-group">
												<label for="matakuliah_jenis_kode" class="col-md-2 control-label" style="text-align:left">Jenis Mata Kuliah</label>
												<div class="col-md-4">
													<?php echo $this->Referensi_model->combobox(28, 'matakuliah_jenis_kode', 'kode_value', 'kode_nama', $matakuliah_jenis_kode, '', '', 'class="form-control" disabled');?>
												</div>
												<label for="matakuliah_status_kode" class="col-md-2 control-label" style="text-align:left">Status Mata Kuliah</label>
												<div class="col-md-4">
													<?php echo $this->Referensi_model->combobox(14, 'matakuliah_status_kode', 'kode_value', 'kode_nama', $matakuliah_status_kode, '', '', 'class="form-control" disabled');?>
												</div>
											</div>
											<div class="form-group">
												<label for="matakuliah_sks" class="col-md-2 control-label" style="text-align:left">SKS Mata Kuliah</label>
												<div class="col-md-4">
													<input type="number" class="form-control" name="matakuliah_sks" id="matakuliah_sks" value="<?php echo $matakuliah_sks; ?>" placeholder="" readonly>
												</div>
												<label for="matakuliah_sks_tatap_muka" class="col-md-2 control-label" style="text-align:left">SKS Tatap Muka</label>
												<div class="col-md-4">
													<input type="number" class="form-control" name="matakuliah_sks_tatap_muka" id="matakuliah_sks_tatap_muka" value="<?php echo $matakuliah_sks_tatap_muka; ?>" placeholder="" readonly>
												</div>
											</div>
											<div class="form-group">
												<label for="matakuliah_sks_praktikum" class="col-md-2 control-label" style="text-align:left">SKS Praktikum</label>
												<div class="col-md-4">
													<input type="number" class="form-control" name="matakuliah_sks_praktikum" id="matakuliah_sks_praktikum" value="<?php echo $matakuliah_sks_praktikum; ?>" placeholder="" readonly>
												</div>
												<label for="matakuliah_sks_lapangan" class="col-md-2 control-label" style="text-align:left">SKS Praktik Lapangan</label>
												<div class="col-md-4">
													<input type="number" class="form-control" name="matakuliah_sks_lapangan" id="matakuliah_sks_lapangan" value="<?php echo $matakuliah_sks_lapangan; ?>" placeholder="" readonly>
												</div>
											</div>
											<div class="form-group">
												<label for="matakuliah_silabus" class="col-md-2 control-label" style="text-align:left">Ada Silabus?</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="matakuliah_silabus" id="matakuliah_silabus" value="<?php echo $matakuliah_silabus; ?>" placeholder="" readonly>
												</div>
												<label for="matakuliah_sap" class="col-md-2 control-label" style="text-align:left">Ada SAP?</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="matakuliah_sap" id="matakuliah_sap" value="<?php echo $matakuliah_sap; ?>" placeholder="" readonly>
												</div>
											</div>
											<div class="form-group">
												<label for="matakuliah_bahan" class="col-md-2 control-label" style="text-align:left">Ada Bahan?</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="matakuliah_bahan" id="matakuliah_bahan" value="<?php echo $matakuliah_bahan; ?>" placeholder="" readonly>
												</div>
												<label for="matakuliah_diktat" class="col-md-2 control-label" style="text-align:left">Ada Diktat?</label>
												<div class="col-md-4">
													<input type="text" class="form-control" name="matakuliah_diktat" id="matakuliah_diktat" value="<?php echo $matakuliah_diktat; ?>" placeholder="" readonly>
												</div>
											</div>
											<div class="form-group">
												<label for="dosen_id" class="col-md-2 control-label" style="text-align:left">Dosen Pengampu</label>
												<div class="col-md-4">
													<?php echo combobox('db', $this->Dosen_model->grid_all_dosen('', 'dosen_nama', 'ASC', '', '', "dosen.program_studi_id = '$program_studi_id' AND dosen.status_aktivitas_kode = 'A'"), 'dosen_id', 'dosen_id', 'dosen_nama', $dosen_id, '', '', 'class="form-control" disabled');?>
												</div>
											</div>
										</div><!-- /.box-body -->
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane <?php echo ($tabs == 1)?'active':''; ?>" id="tab_2">
										<table id="datagrid_pengajar" class="table table-bordered table-striped" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>DOSEN PENGAMPU</th>
													<th>RENCANA PERTEMUAN</th>
													<th>REALISASI PERTEMUAN</th>
													<th>MENGEJAR KEALS</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
										</table>
									</div>
									<!-- /.tab-pane -->
								</div>
								<!-- /.tab-content -->
							</div>
							<!-- nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
					<div class="row">
            <div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
									<?php if (check_permission("W")){?>
									<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$matakuliah_id); ?>'" class="btn btn-primary" name="save" value="save">Ubah Matakuliah</button>
									<?php } ?>
								</div><!-- /.box-footer -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	
	<div class="modal fade modal-green" id="addDosenModal" tabindex="-1" role="dialog" aria-labelledby="addPengajarLabel">
	  <div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="addPengajarLabel">Tambah Pengajar</h4>
				</div>
				<form action="<?php echo module_url($this->uri->segment(2).'/add_pengajar/'.$matakuliah_id);?>?tabs=<?php echo $tabs; ?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="matakuliah_id" value="<?php echo $matakuliah_id; ?>">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="ak_dosen_id" class="control-label">Dosen Pelapoaran <span style="color:#F00">*</span></label>
								<div class="">
									<?php echo combobox('db', $this->Dosen_model->grid_all_dosen('', 'dosen_nama', 'ASC', '', '', "dosen.program_studi_id = '$program_studi_id' AND dosen.status_aktivitas_kode = 'A'"), 'ak_dosen_id', 'dosen_id', 'dosen_nama', '', '', '', 'class="form-control"');?>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="ak_rencana_pertemuan" class="control-label">Rencana Pertemuan</label>
								<div class="">
									<input type="text" class="form-control" name="ak_rencana_pertemuan" id="ak_rencana_pertemuan" placeholder="" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="ak_realisasi_pertemuan" class="control-label">Realisasi Pertemuan</label>
								<div class="">
									<input type="text" class="form-control" name="ak_realisasi_pertemuan" id="ak_realisasi_pertemuan" placeholder="" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="ak_mengajar_kelas" class="control-label">Mengajar Kelas</label>
								<div class="">
									<input type="text" class="form-control" name="ak_mengajar_kelas" id="ak_mengajar_kelas" placeholder="" value="">
								</div>
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
	  </div>
	</div>
	
	<div class="modal fade modal-green" id="editDosenModal" tabindex="-1" role="dialog" aria-labelledby="editPengajarLabel">
	  <div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="editPengajarLabel">Edit Pengajar</h4>
				</div>
				<form action="<?php echo module_url($this->uri->segment(2).'/edit_pengajar/'.$matakuliah_id);?>?tabs=<?php echo $tabs; ?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="matakuliah_id" value="<?php echo $matakuliah_id; ?>">
				<input type="hidden" name="ek_id" id="ek_id" value="">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="ek_dosen_id" class="control-label">Dosen Pelapoaran <span style="color:#F00">*</span></label>
								<div class="">
									<?php echo combobox('db', $this->Dosen_model->grid_all_dosen('', 'dosen_nama', 'ASC', '', '', "dosen.program_studi_id = '$program_studi_id' AND dosen.status_aktivitas_kode = 'A'"), 'ek_dosen_id', 'dosen_id', 'dosen_nama', '', '', '', 'class="form-control"');?>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="ek_rencana_pertemuan" class="control-label">Rencana Pertemuan</label>
								<div class="">
									<input type="text" class="form-control" name="ek_rencana_pertemuan" id="ek_rencana_pertemuan" placeholder="" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="ek_realisasi_pertemuan" class="control-label">Realisasi Pertemuan</label>
								<div class="">
									<input type="text" class="form-control" name="ek_realisasi_pertemuan" id="ek_realisasi_pertemuan" placeholder="" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="ek_mengajar_kelas" class="control-label">Mengajar Kelas</label>
								<div class="">
									<input type="text" class="form-control" name="ek_mengajar_kelas" id="ek_mengajar_kelas" placeholder="" value="">
								</div>
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
	  </div>
	</div>
<?php } ?>