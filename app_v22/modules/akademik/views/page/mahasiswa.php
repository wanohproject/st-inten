<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('mahasiswa/datatable/'.$program_studi_id.'/'.$tahun_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "mahasiswa_nama"},
			{ "data": "mahasiswa_nim"},
			{ "data": "mahasiswa_user"},
			{ "data": "mahasiswa_pin"},
			{ "data": "mahasiswa_tahun_masuk"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Mahasiswa</a></li>
            <li class="active">Daftar Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Mahasiswa</h3>
				  <div class="pull-right">
					<a href="<?php echo base_url('asset/file/format_import_mahasiswa.xls'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-download"></span> Download Format Import</a>
					<a href="<?php echo module_url($this->uri->segment(2).'/import'); ?>" class="btn btn-primary"><span class="fa fa-download"></span> Import Mahasiswa</a>
					<a href="<?php echo module_url($this->uri->segment(2).'/generate-account'); ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin? \nAkan men-generate user mahasiswa.');"><span class="fa fa-cog"></span> Generate User Mahasiswa</a>
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
				<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$tahun_kode);?>" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="program_studi_id" class="control-label">Program Studi</label>
								<div class="">
									<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="tingkat_id" class="control-label">Tahun Masuk</label>
								<div class="">	
									<?php combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_kode', 'DESC'), 'tahun_kode', 'tahun_kode', 'tahun_kode', $tahun_kode, 'submit();', 'PILIH TAHUN', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
					</div>
				</div>
				</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>NIM</th>
                        <th>User</th>
                        <th>Password</th>
                        <th>Tahun Masuk</th>
                        <th style="width:130px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#mahasiswa_tanggal_lahir, #mahasiswa_tanggal_masuk, #mahasiswa_tanggal_lulus').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<script>
$(function () {
	$("#mahasiswa_user").change(function(){
		var params = {
			action: 'add',
			user: $("#mahasiswa_user").val()
		};
		// console.log(params);
		
		$.ajax({
			url: "<?php echo module_url($this->uri->segment(2).'/check-user'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			beforeSend: function() {},
			success: function(data){
				if (data.response == false){
					$("#mahasiswa_user").css("border-color", "#a94442");
					$("#form-add").attr('onsubmit', 'return false;');
				} else {
					$("#mahasiswa_user").css("border-color", "#5cb85c");
					$("#form-add").attr('onsubmit', 'return true;');
				}
			},
			complete: function() {},
		});
	});
});

function getSemester(){
	var params = {
		tahun: $("#mahasiswa_tahun_masuk").val()
	};
	
   
	$.ajax({
		url: "<?php echo module_url($this->uri->segment(2).'/get-semester'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		beforeSend: function(data){
			$("#mahasiswa_semester_awal").select2("val", "");
		},
		success: function(data){
			var innerHTML = "";
			if(data.response == true){
				innerHTML += "<option value=\"\"></option>";
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<option value=\"" + data.data[i].semester_kode + "\">" + data.data[i].semester_nama + "</option>";
				}
			} else {
				innerHTML = "<option value=\"\"></option>";
			}
			$("#mahasiswa_semester_awal").html(innerHTML);
		},
	});
}
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Mahasiswa</a></li>
            <li class="active">Tambah Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Mahasiswa</h3>
                </div><!-- /.box-header -->
				<form id="form-add" class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
				<div class="box-header">
					<h3 class="box-title">Informasi Akun</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="mahasiswa_user" class="col-md-2 control-label" style="text-align:left">User</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_user" id="mahasiswa_user" value="<?php echo $mahasiswa_user; ?>" placeholder="">
						</div>
						<label for="mahasiswa_pin" class="col-md-2 control-label" style="text-align:left">Password</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_pin" id="mahasiswa_pin" value="<?php echo $mahasiswa_pin; ?>" placeholder="">
							<small>* Hapus 6 Karakter.</small>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							* Apabila <strong>User</strong> tidak diisi maka user mahasiswa tidak akan dibuat. <br />
							* Apabila <strong>Password</strong> tidak diisi maka pin mahasiswa akan dibuat otomatis 6 Karakter Acak. <br />
						</div>
					</div>
				</div>
				<div class="box-header">
					<h3 class="box-title">Informasi Identitas</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit()', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_nim" class="col-md-2 control-label" style="text-align:left">NIM</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_nim" id="mahasiswa_nim" value="<?php echo $mahasiswa_nim; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="mahasiswa_nama" id="mahasiswa_nama" value="<?php echo $mahasiswa_nama; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(8, 'mahasiswa_jenis_kelamin', 'kode_value', 'kode_nama', $mahasiswa_jenis_kelamin, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_tempat_lahir" class="col-md-2 control-label" style="text-align:left">Tempat, Tanggal Lahir</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="mahasiswa_tempat_lahir" id="mahasiswa_tempat_lahir" value="<?php echo $mahasiswa_tempat_lahir; ?>" placeholder="Tempat Lahir">
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_tanggal_lahir" id="mahasiswa_tanggal_lahir" value="<?php echo $mahasiswa_tanggal_lahir; ?>" placeholder="Tanggal Lahir" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_tahun_masuk" class="col-md-2 control-label" style="text-align:left">Tahun Masuk</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_nama', 'ASC'), 'mahasiswa_tahun_masuk', 'tahun_kode', 'tahun_nama', $mahasiswa_tahun_masuk, 'getSemester();', '', 'class="form-control" required');?>
						</div>
						<label for="mahasiswa_semester_awal" class="col-md-2 control-label" style="text-align:left">Semester Awal Masuk</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_semester_awal', 'semester_kode', 'semester_nama', $mahasiswa_semester_awal, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_batas_studi" class="col-md-2 control-label" style="text-align:left">Batas Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_batas_studi', 'semester_kode', 'semester_nama', $mahasiswa_batas_studi, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_tanggal_masuk" class="col-md-2 control-label" style="text-align:left">Tanggal Masuk</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_tanggal_masuk" id="mahasiswa_tanggal_masuk" value="<?php echo $mahasiswa_tanggal_masuk; ?>" placeholder="" autocomplete="off">
						</div>
						<label for="mahasiswa_tanggal_lulus" class="col-md-2 control-label" style="text-align:left">Tanggal Lulus</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_tanggal_lulus" id="mahasiswa_tanggal_lulus" value="<?php echo $mahasiswa_tanggal_lulus; ?>" placeholder="" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_dosen_wali" class="col-md-2 control-label" style="text-align:left">Dosen Wali</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Dosen_model->grid_all_dosen('', 'dosen_nama', 'ASC', '', '', "dosen.program_studi_id = '$program_studi_id'"), 'mahasiswa_dosen_wali', 'dosen_id', 'dosen_nama', $mahasiswa_dosen_wali, '', '', 'class="form-control"');?>
						</div>
					</div>
					<h4>Mahasiswa Pindahan</h4>
					<div class="form-group">
						<label for="mahasiswa_pindahan_pt" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_pindahan_pt" id="mahasiswa_pindahan_pt" value="<?php echo $mahasiswa_pindahan_pt; ?>" placeholder="">
						</div>
						<label for="mahasiswa_pindahan_prodi" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_pindahan_prodi" id="mahasiswa_pindahan_prodi" value="<?php echo $mahasiswa_pindahan_prodi; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_pindahan_jenjang" class="col-md-2 control-label" style="text-align:left">Jenjang</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(4, 'mahasiswa_pindahan_jenjang', 'kode_value', 'kode_nama', $mahasiswa_pindahan_jenjang, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_pindahan_nim" class="col-md-2 control-label" style="text-align:left">NIM Asal</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="mahasiswa_pindahan_nim" id="mahasiswa_pindahan_nim" value="<?php echo $mahasiswa_pindahan_nim; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="mahasiswa_sks_diakui" class="col-md-2 control-label" style="text-align:left">SKS yang Diakui</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="mahasiswa_sks_diakui" id="mahasiswa_sks_diakui" value="<?php echo $mahasiswa_sks_diakui; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
	<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#mahasiswa_tanggal_lahir, #mahasiswa_tanggal_masuk, #mahasiswa_tanggal_lulus').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<script>
$(function () {
	$("#mahasiswa_user").change(function(){
		var params = {
			action: 'edit',
			id: '<?php echo $mahasiswa_id; ?>',
			user: $("#mahasiswa_user").val()
		};
		// console.log(params);
		
		$.ajax({
			url: "<?php echo module_url($this->uri->segment(2).'/check-user'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			beforeSend: function() {},
			success: function(data){
				if (data.response == false){
					$("#mahasiswa_user").css("border-color", "#a94442");
					$("#form-add").attr('onsubmit', 'return false;');
				} else {
					$("#mahasiswa_user").css("border-color", "#5cb85c");
					$("#form-add").attr('onsubmit', 'return true;');
				}
			},
			complete: function() {},
		});
	});
});

function getSemester(){
	var params = {
		tahun: $("#mahasiswa_tahun_masuk").val()
	};
	
   
	$.ajax({
		url: "<?php echo module_url($this->uri->segment(2).'/get-semester'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		beforeSend: function(data){
			$("#mahasiswa_semester_awal").select2("val", "");
		},
		success: function(data){
			var innerHTML = "";
			if(data.response == true){
				innerHTML += "<option value=\"\"></option>";
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<option value=\"" + data.data[i].semester_kode + "\">" + data.data[i].semester_nama + "</option>";
				}
			} else {
				innerHTML = "<option value=\"\"></option>";
			}
			$("#mahasiswa_semester_awal").html(innerHTML);
		},
	});
}
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Mahasiswa</a></li>
            <li class="active">Ubah Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Mahasiswa</h3>
                </div><!-- /.box-header -->
								<form id="form-add" class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$mahasiswa_id);?>" method="post">
								<input type="hidden" class="form-control" name="mahasiswa_id" id="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>" placeholder="">
								<div class="box-header">
									<h3 class="box-title">Informasi Akun</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="mahasiswa_user" class="col-md-2 control-label" style="text-align:left">User</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_user" id="mahasiswa_user" value="<?php echo $mahasiswa_user; ?>" placeholder="">
										</div>
										<label for="mahasiswa_pin" class="col-md-2 control-label" style="text-align:left">Password</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_pin" id="mahasiswa_pin" value="<?php echo $mahasiswa_pin; ?>" placeholder="">
											<small>* Hapus 6 Karakter.</small>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											* Apabila <strong>User</strong> tidak diisi maka user mahasiswa tidak akan dibuat. <br />
											* Apabila <strong>Password</strong> tidak diisi maka pin mahasiswa akan dibuat otomatis 6 Karakter Acak. <br />
										</div>
									</div>
								</div>
								<div class="box-header">
									<h3 class="box-title">Informasi Identitas</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_nim" class="col-md-2 control-label" style="text-align:left">NIM</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_nim" id="mahasiswa_nim" value="<?php echo $mahasiswa_nim; ?>" placeholder="">
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
										<div class="col-md-10">
											<input type="text" class="form-control" name="mahasiswa_nama" id="mahasiswa_nama" value="<?php echo $mahasiswa_nama; ?>" placeholder="" required>
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(8, 'mahasiswa_jenis_kelamin', 'kode_value', 'kode_nama', $mahasiswa_jenis_kelamin, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_tempat_lahir" class="col-md-2 control-label" style="text-align:left">Tempat, Tanggal Lahir</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="mahasiswa_tempat_lahir" id="mahasiswa_tempat_lahir" value="<?php echo $mahasiswa_tempat_lahir; ?>" placeholder="Tempat Lahir">
										</div>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_tanggal_lahir" id="mahasiswa_tanggal_lahir" value="<?php echo $mahasiswa_tanggal_lahir; ?>" placeholder="Tanggal Lahir" autocomplete="off">
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_tahun_masuk" class="col-md-2 control-label" style="text-align:left">Tahun Masuk</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_nama', 'ASC'), 'mahasiswa_tahun_masuk', 'tahun_kode', 'tahun_nama', $mahasiswa_tahun_masuk, 'getSemester();', '', 'class="form-control" required');?>
										</div>
										<label for="mahasiswa_semester_awal" class="col-md-2 control-label" style="text-align:left">Semester Awal Masuk</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_semester_awal', 'semester_kode', 'semester_nama', $mahasiswa_semester_awal, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_batas_studi" class="col-md-2 control-label" style="text-align:left">Batas Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_batas_studi', 'semester_kode', 'semester_nama', $mahasiswa_batas_studi, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_tanggal_masuk" class="col-md-2 control-label" style="text-align:left">Tanggal Masuk</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_tanggal_masuk" id="mahasiswa_tanggal_masuk" value="<?php echo $mahasiswa_tanggal_masuk; ?>" placeholder="" autocomplete="off">
										</div>
										<label for="mahasiswa_tanggal_lulus" class="col-md-2 control-label" style="text-align:left">Tanggal Lulus</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_tanggal_lulus" id="mahasiswa_tanggal_lulus" value="<?php echo $mahasiswa_tanggal_lulus; ?>" placeholder="" autocomplete="off">
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_dosen_wali" class="col-md-2 control-label" style="text-align:left">Dosen Wali</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Dosen_model->grid_all_dosen('', 'dosen_nama', 'ASC', '', '', "dosen.program_studi_id = '$program_studi_id'"), 'mahasiswa_dosen_wali', 'dosen_id', 'dosen_nama', $mahasiswa_dosen_wali, '', '', 'class="form-control"');?>
										</div>
									</div>
									<h4>Mahasiswa Pindahan</h4>
									<div class="form-group">
										<label for="mahasiswa_pindahan_pt" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_pindahan_pt" id="mahasiswa_pindahan_pt" value="<?php echo $mahasiswa_pindahan_pt; ?>" placeholder="">
										</div>
										<label for="mahasiswa_pindahan_prodi" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_pindahan_prodi" id="mahasiswa_pindahan_prodi" value="<?php echo $mahasiswa_pindahan_prodi; ?>" placeholder="">
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_pindahan_jenjang" class="col-md-2 control-label" style="text-align:left">Jenjang</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(4, 'mahasiswa_pindahan_jenjang', 'kode_value', 'kode_nama', $mahasiswa_pindahan_jenjang, '', '', 'class="form-control"');?>
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_pindahan_nim" class="col-md-2 control-label" style="text-align:left">NIM Asal</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="mahasiswa_pindahan_nim" id="mahasiswa_pindahan_nim" value="<?php echo $mahasiswa_pindahan_nim; ?>" placeholder="">
										</div>
									</div>
									<div class="form-group">
										<label for="mahasiswa_sks_diakui" class="col-md-2 control-label" style="text-align:left">SKS yang Diakui</label>
										<div class="col-md-4">
											<input type="number" class="form-control" name="mahasiswa_sks_diakui" id="mahasiswa_sks_diakui" value="<?php echo $mahasiswa_sks_diakui; ?>" placeholder="">
										</div>
									</div>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
								</div><!-- /.box-footer -->
								</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Mahasiswa</a></li>
            <li class="active">Import Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Mahasiswa</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
								<span class="input-group-btn">
									<input type="submit" name="importMahasiswa" value="Import" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetMahasiswa" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
									<th nowrap>#</th>
									<th nowrap>PROGRAM STUDI</th>
									<th nowrap>USERNAME</th>
									<th nowrap>PASSWORD</th>
									<th nowrap>NAMA</th>
									<th nowrap>NIM</th>
									<th nowrap>JK</th>
									<th nowrap>TEMPAT LAHIR</th>
									<th nowrap>TANGGAL LAHIR</th>
									<th nowrap>TAHUN MASUK</th>
									<th nowrap>SEMESTER MASUK</th>
									<th nowrap>BATAS STUDI</th>
									<th nowrap>TANGGAL MASUK</th>
									<th nowrap>TANGGAL LULUS</th>
									<th nowrap>NIM ASAL</th>
									<th nowrap>SKS DIAKUI</th>
									<th nowrap>PERGURUAN TINGGI</th>
									<th nowrap>PROGRAM STUDI</th>
									<th nowrap>JENJANG</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
									$dataprodi = "";
									$datastatus = "";
									if ($row['data_prodi'] == 'PRODI VALID'){
										$dataprodi = 'style="background:#B1E3B1"';
									} else {
										$dataprodi = 'style="background:#EEB9B9"';
									}
									?>
									<tr <?php echo $dataprodi; ?>>
									  <th scope="row"><?php echo $i; ?></th>
									  <td nowrap><?php echo $row['mahasiswa_prodi']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_user']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_pin']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_nama']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_nim']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_jenis_kelamin']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_tempat_lahir']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_tanggal_lahir']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_tahun_masuk']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_semester_awal']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_batas_studi']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_tanggal_masuk']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_tanggal_lulus']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_pindahan_nim']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_sks_diakui']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_pindahan_pt']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_pindahan_prodi']; ?></td>
									  <td nowrap><?php echo $row['mahasiswa_pindahan_jenjang']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mahasiswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Mahasiswa</a></li>
            <li class="active">Detail Mahasiswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs" role="tablist" id="myTab">
									<li <?php echo ($tabs == 0)?' class="active"':''; ?>><a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$mahasiswa_id); ?>?tabs=0">Detail Mahasiswa</a></li>
									<li <?php echo ($tabs == 1)?' class="active"':''; ?>><a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$mahasiswa_id); ?>?tabs=1">Transkrip Historis</a></li>
								</ul>
								<div class="tab-content">
									<?php if ($tabs == 0){ ?>
										<div class="tab-pane active form-horizontal" id="tab_1">
											<div class="box-header with-border">
												<h3 class="box-title">Informasi Akun</h3>
											</div><!-- /.box-header -->
											<div class="box-body">
												<div class="form-group">
													<label for="mahasiswa_user" class="col-md-2 control-label" style="text-align:left">User</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_user" id="mahasiswa_user" value="<?php echo $mahasiswa_user; ?>" placeholder="" readonly="readonly">
													</div>
													<label for="mahasiswa_pin" class="col-md-2 control-label" style="text-align:left">Password</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_pin" id="mahasiswa_pin" value="<?php echo $mahasiswa_pin; ?>" placeholder="" readonly="readonly">
													</div>
												</div>
											</div>
											<div class="box-header with-border">
												<h3 class="box-title">Informasi Identitas</h3>
											</div><!-- /.box-header -->
											<div class="box-body">
												<div class="form-group">
													<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
													<div class="col-md-4">
														<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_nim" class="col-md-2 control-label" style="text-align:left">NIM</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_nim" id="mahasiswa_nim" value="<?php echo $mahasiswa_nim; ?>" placeholder="" readonly="readonly">
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
													<div class="col-md-10">
														<input type="text" class="form-control" name="mahasiswa_nama" id="mahasiswa_nama" value="<?php echo $mahasiswa_nama; ?>" placeholder="" readonly="readonly">
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
													<div class="col-md-4">
														<?php echo $this->Referensi_model->combobox(8, 'mahasiswa_jenis_kelamin', 'kode_value', 'kode_nama', $mahasiswa_jenis_kelamin, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_tempat_lahir" class="col-md-2 control-label" style="text-align:left">Tempat, Tanggal Lahir</label>
													<div class="col-md-6">
														<input type="text" class="form-control" name="mahasiswa_tempat_lahir" id="mahasiswa_tempat_lahir" value="<?php echo $mahasiswa_tempat_lahir; ?>" placeholder="Tempat Lahir" readonly="readonly">
													</div>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_tanggal_lahir" id="mahasiswa_tanggal_lahir" value="<?php echo $mahasiswa_tanggal_lahir; ?>" placeholder="Tanggal Lahir" autocomplete="off" readonly="readonly">
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_tahun_masuk" class="col-md-2 control-label" style="text-align:left">Tahun Masuk</label>
													<div class="col-md-4">
														<?php echo combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_nama', 'ASC'), 'mahasiswa_tahun_masuk', 'tahun_kode', 'tahun_nama', $mahasiswa_tahun_masuk, 'getSemester();', '', 'class="form-control" disabled');?>
													</div>
													<label for="mahasiswa_semester_awal" class="col-md-2 control-label" style="text-align:left">Semester Awal Masuk</label>
													<div class="col-md-4">
														<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_semester_awal', 'semester_kode', 'semester_nama', $mahasiswa_semester_awal, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_batas_studi" class="col-md-2 control-label" style="text-align:left">Batas Studi</label>
													<div class="col-md-4">
														<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'mahasiswa_batas_studi', 'semester_kode', 'semester_nama', $mahasiswa_batas_studi, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_tanggal_masuk" class="col-md-2 control-label" style="text-align:left">Tanggal Masuk</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_tanggal_masuk" id="mahasiswa_tanggal_masuk" value="<?php echo $mahasiswa_tanggal_masuk; ?>" placeholder="" autocomplete="off" readonly="readonly">
													</div>
													<label for="mahasiswa_tanggal_lulus" class="col-md-2 control-label" style="text-align:left">Tanggal Lulus</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_tanggal_lulus" id="mahasiswa_tanggal_lulus" value="<?php echo $mahasiswa_tanggal_lulus; ?>" placeholder="" autocomplete="off" readonly="readonly">
													</div>
												</div>
												<h4>Mahasiswa Pindahan</h4>
												<div class="form-group">
													<label for="mahasiswa_pindahan_pt" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_pindahan_pt" id="mahasiswa_pindahan_pt" value="<?php echo $mahasiswa_pindahan_pt; ?>" placeholder="" readonly="readonly">
													</div>
													<label for="mahasiswa_pindahan_prodi" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_pindahan_prodi" id="mahasiswa_pindahan_prodi" value="<?php echo $mahasiswa_pindahan_prodi; ?>" placeholder="" readonly="readonly">
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_pindahan_jenjang" class="col-md-2 control-label" style="text-align:left">Jenjang</label>
													<div class="col-md-4">
														<?php echo $this->Referensi_model->combobox(4, 'mahasiswa_pindahan_jenjang', 'kode_value', 'kode_nama', $mahasiswa_pindahan_jenjang, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_pindahan_nim" class="col-md-2 control-label" style="text-align:left">NIM Asal</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="mahasiswa_pindahan_nim" id="mahasiswa_pindahan_nim" value="<?php echo $mahasiswa_pindahan_nim; ?>" placeholder="" readonly="readonly">
													</div>
												</div>
												<div class="form-group">
													<label for="mahasiswa_sks_diakui" class="col-md-2 control-label" style="text-align:left">SKS yang Diakui</label>
													<div class="col-md-4">
														<input type="number" class="form-control" name="mahasiswa_sks_diakui" id="mahasiswa_sks_diakui" value="<?php echo $mahasiswa_sks_diakui; ?>" placeholder="" readonly="readonly">
													</div>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.tab-pane -->
									<?php } else if ($tabs == 1){ ?>
									<div class="tab-pane active form-horizontal" id="tab_1">
										<div class="box-body">
											<table id="datagrid_krs" class="table table-bordered table-striped" cellspacing="0" width="100%">
												<tbody>
												<?php
												$grid_semester = $this->db->query("SELECT akd_mahasiswa_nilai.*, akd_semester.semester_nama FROM akd_mahasiswa_nilai LEFT JOIN akd_matakuliah ON akd_mahasiswa_nilai.matakuliah_id=akd_matakuliah.matakuliah_id LEFT JOIN akd_semester ON akd_mahasiswa_nilai.semester_kode=akd_semester.semester_kode WHERE akd_mahasiswa_nilai.mahasiswa_id = '$mahasiswa_id' GROUP BY akd_mahasiswa_nilai.semester_kode ORDER BY akd_mahasiswa_nilai.semester_kode ASC")->result();
												if ($grid_semester){
													foreach ($grid_semester as $row_semester) {
														?>
														<thead>
															<tr>
																<th colspan="4">Tahun Ajaran <?php echo $row_semester->semester_nama; ?></th>
															</tr>
															<tr>
																<th width="150">KODE</th>
																<th>NAMA MATA KUIAH</th>
																<th width="100">JUMLAH SKS</th>
																<th width="100">NILAI</th>
															</tr>
														</thead>
														<?php
														$matakuliah = $this->db->query("SELECT akd_matakuliah.*, mahasiswa_nilai_huruf FROM akd_mahasiswa_nilai LEFT JOIN akd_matakuliah ON akd_mahasiswa_nilai.matakuliah_id=akd_matakuliah.matakuliah_id LEFT JOIN akd_semester ON akd_mahasiswa_nilai.semester_kode=akd_semester.semester_kode WHERE akd_mahasiswa_nilai.mahasiswa_id = '$mahasiswa_id' AND akd_mahasiswa_nilai.semester_kode='$row_semester->semester_kode' ORDER BY matakuliah_kode ASC")->result();
														if ($matakuliah){
															foreach ($matakuliah as $row) {
																?>
																<tr>
																	<td><?php echo $row->matakuliah_kode; ?></td>
																	<td><?php echo $row->matakuliah_nama; ?></td>
																	<td class="text-center"><?php echo $row->matakuliah_sks; ?></td>
																	<td class="text-center"><?php echo $row->mahasiswa_nilai_huruf; ?></td>
																</tr>
																<?php
															}
														}
													}
												}
												?>
												</tbody>
											</table>
										</div>
									</div>
									<?php } else if ($tabs == 2){ ?>
									<?php } ?>
								</div><!-- /.tab-content -->
							</div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
					<div class="row">
            <div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
									<?php if (check_permission("W")){?>
									<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$mahasiswa_id); ?>'" class="btn btn-primary" name="save" value="save">Ubah Mahasiswa</button>
									<?php } ?>
								</div><!-- /.box-footer -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>