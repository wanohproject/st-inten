<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('staf/datatable/'.$departemen_id); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "departemen_kode"},
			{ "data": "staf_nama"},
			{ "data": "staf_user"},
			{ "data": "staf_pin"},
			{ "data": "staf_user_aktif"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc'],
			[1, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Daftar Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Staf</h3>
									<div class="pull-right">
										<a href="<?php echo module_url($this->uri->segment(2).'/upload-foto'); ?>" class="btn btn-primary"><span class="fa fa-upload"></span> Upload Foto</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/pin_staf'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-print"></span> Cetak Pin</a>
										<a href="<?php echo base_url('asset/file/format_import_staf.xls'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-download"></span> Download Format Import</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/import'); ?>" class="btn btn-primary"><span class="fa fa-download"></span> Import</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/generate-account'); ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin? \nAkan men-generate user guru.');"><span class="fa fa-cog"></span> Generate User Staf</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
									</div>
                </div><!-- /.box-header -->
								<?php if (userdata('departemen_id') == ""){?>
								<div class="box-body">
									<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id);?>" method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="departemen_id" class="control-label">Departemen</label>
													<div class="">
														<?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_utama IS NULL ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
													</div>		
												</div>
											</div>
										</div>
									</form>
								</div>
								<?php } ?>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Departemen</th>
                        <th>Nama</th>
                        <th>User</th>
                        <th>Pin</th>
                        <th>User Aktif</th>
                        <th style="width:150px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Tambah Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
					<div class="box-header">
					  <h3 class="box-title">Informasi Akun</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_user" class="col-md-2 control-label" style="text-align:left;">User</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_user" id="staf_user" value="<?php echo $staf_user; ?>" placeholder="">
							</div>
							<label for="staf_pin" class="col-md-2 control-label" style="text-align:left;">PIN</label>
							<div class="col-md-4">
								<input type="number" maxlength="6" class="form-control" name="staf_pin" id="staf_pin" value="<?php echo $staf_pin; ?>" placeholder="">
								<small>* Hapus 6 Karakter.</small>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								* Apabila <strong> User</strong> tidak diisi maka user guru tidak akan dibuat. <br />
								* Apabila <strong>PIN</strong> tidak diisi maka ping guru akan dibuat otomatis 6 Karakter Acak. <br />
							</div>
						</div>
					</div>
					<div class="box-header">
					  <h3 class="box-title">Informasi Identitas</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_nip" class="col-md-2 control-label" style="text-align:left;">Nomor Induk Pegawai</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nip" id="staf_nip" value="<?php echo $staf_nip; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_gelar_depan" class="col-md-2 control-label" style="text-align:left;">Gelar Depan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_gelar_depan" id="staf_gelar_depan" value="<?php echo $staf_gelar_depan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nama" class="col-md-2 control-label" style="text-align:left;">Nama Lengkap<br /><small style="font-weight:normal">(Tanpa Gelar)</small></label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama" id="staf_nama" value="<?php echo $staf_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_gelar_belakang" class="col-md-2 control-label" style="text-align:left;">Gelar Belakang</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_gelar_belakang" id="staf_gelar_belakang" value="<?php echo $staf_gelar_belakang; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_tempat_lahir" class="col-md-2 control-label" style="text-align:left;">Tempat, Tanggal Lahir</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="staf_tempat_lahir" id="staf_tempat_lahir" value="<?php echo $staf_tempat_lahir; ?>" placeholder="">
							</div>
							<div class="col-md-5">
								<input type="date" class="form-control" name="staf_tanggal_lahir" id="staf_tanggal_lahir" value="<?php echo $staf_tanggal_lahir; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_pendidikan_terakhir" class="col-md-2 control-label" style="text-align:left;">Pendidikan Terkahir</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_pendidikan_terakhir" id="staf_pendidikan_terakhir" value="<?php echo $staf_pendidikan_terakhir; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nama_ibu_kandung" class="col-md-2 control-label" style="text-align:left;">Nama Ibu Kandung</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama_ibu_kandung" id="staf_nama_ibu_kandung" value="<?php echo $staf_nama_ibu_kandung; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_jenis_kelamin" class="col-md-2 control-label" style="text-align:left;">Jenis Kelamin</label>
							<div class="col-md-10">
								<div class="radio">
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="L" <?php echo $jenis_kelamin_a = ($staf_jenis_kelamin == 'L')?'checked':''; ?>> L
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="P" <?php echo $jenis_kelamin_h = ($staf_jenis_kelamin == 'P')?'checked':''; ?>> P
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_agama" class="col-md-2 control-label" style="text-align:left;">Agama & Kepercayaan</label>
							<div class="col-md-10">
								<?php combobox('1d', array("Islam", "Kristen", "Katholik", "Hindu", "Budha", "Kong Hu Chu", "Kepercayaan kpd Tuhan YME", "Lainnya"), 'staf_agama', '', '', $staf_agama, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status_perkawinan" class="col-md-2 control-label" style="text-align:left;">Status Perkawinan</label>
							<div class="col-md-4">
								<?php combobox('1d', array("Menikah", "Belum Menikah", "Janda/Duda"), 'staf_status_perkawinan', '', '', $staf_status_perkawinan, '', '', 'class="form-control"');?>
							</div>
							<label for="staf_nama_suami_istri" class="col-md-2 control-label" style="text-align:left;">Tanggal Menikah</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_nama_suami_istri" id="staf_nama_suami_istri" value="<?php echo $staf_nama_suami_istri; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_golongan_darah" class="col-md-2 control-label" style="text-align:left;">Golongan Darah</label>
							<div class="col-md-10">
								<?php combobox('1d', array("A", "B", "AB", "O"), 'staf_golongan_darah', '', '', $staf_golongan_darah, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_alamat" class="col-md-2 control-label" style="text-align:left;">Alamat Rumah</label>
							<div class="col-md-10">
								<textarea type="text" class="form-control" name="staf_alamat" id="staf_alamat" placeholder=""><?php echo $staf_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_dusun" class="col-md-2 control-label" style="text-align:left;">RT/RW</label>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rt" id="staf_rt" value="<?php echo $staf_rt; ?>" placeholder="RT">
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rw" id="staf_rw" value="<?php echo $staf_rw; ?>" placeholder="RW">
							</div>
							<label for="staf_dusun" class="col-md-2 control-label" style="text-align:left;">Dusun</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_dusun" id="staf_dusun" value="<?php echo $staf_dusun; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_deskel" class="col-md-2 control-label" style="text-align:left;">Desa/Kelurahan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_deskel" id="staf_deskel" value="<?php echo $staf_deskel; ?>" placeholder="">
							</div>
							<label for="staf_kecamatan" class="col-md-2 control-label" style="text-align:left;">Kecamatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kecamatan" id="staf_kecamatan" value="<?php echo $staf_kecamatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_kabkota" class="col-md-2 control-label" style="text-align:left;">Kota/Kabupaten</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kabkota" id="staf_kabkota" value="<?php echo $staf_kabkota; ?>" placeholder="">
							</div>
							<label for="staf_provinsi" class="col-md-2 control-label" style="text-align:left;">Provinsi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_provinsi" id="staf_provinsi" value="<?php echo $staf_provinsi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_kodepos" class="col-md-2 control-label" style="text-align:left;">Kode Pos</label>
							<div class="col-md-4">
								<input type="number" class="form-control" name="staf_kodepos" id="staf_kodepos" value="<?php echo $staf_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_telepon" class="col-md-2 control-label" style="text-align:left;">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_telepon" id="staf_telepon" value="<?php echo $staf_telepon; ?>" placeholder="">
							</div>
							<label for="staf_hp" class="col-md-2 control-label" style="text-align:left;">HP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_hp" id="staf_hp" value="<?php echo $staf_hp; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_email" class="col-md-2 control-label" style="text-align:left;">Email</label>
							<div class="col-md-10">
								<input type="email" class="form-control" name="staf_email" id="staf_email" value="<?php echo $staf_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nik" class="col-md-2 control-label" style="text-align:left;">No. KTP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nik" id="staf_nik" value="<?php echo $staf_nik; ?>" placeholder="">
							</div>
							<label for="staf_no_kk" class="col-md-2 control-label" style="text-align:left;">No. KK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_no_kk" id="staf_no_kk" value="<?php echo $staf_no_kk; ?>" placeholder="">
							</div>
						</div>
						
						<div class="form-group">
							<label for="staf_npwp" class="col-md-2 control-label" style="text-align:left;">NPWP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_npwp" id="staf_npwp" value="<?php echo $staf_npwp; ?>" placeholder="">
							</div>
							<label for="staf_status_pajak" class="col-md-2 control-label" style="text-align:left;">Status Pajak</label>
							<div class="col-md-4">
								<?php combobox('1d', array("TK - Tidak Kawin", "K/0 - Kawin Anak 0", "K/1 - Kawin Anak 1", "K/2 - Kawin Anak 2", "K/3 - Kawin Anak 3"), 'staf_status_pajak', '', '', $staf_status_pajak, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status_sdm" class="col-md-2 control-label" style="text-align:left;">Status SDM</label>
							<div class="col-md-4">
								<?php combobox('1d', array("1. Pegawai Tetap", "2. Pegawai Tidak Tetap"), 'staf_status_sdm', '', '', $staf_status_sdm, '', '', 'class="form-control"');?>
							</div>
							<label for="staf_tmt" class="col-md-2 control-label" style="text-align:left;">Mulai Bekerja di YTB</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt" id="staf_tmt" value="<?php echo $staf_tmt; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_sk_pengangkatan" class="col-md-2 control-label" style="text-align:left;">No. SK Pengangkatan Tetap</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_sk_pengangkatan" id="staf_sk_pengangkatan" value="<?php echo $staf_sk_pengangkatan; ?>" placeholder="">
							</div>
							<label for="staf_tmt_pengangkatan" class="col-md-2 control-label" style="text-align:left;">Tanggal SK Pengangkatan Tetap</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt_pengangkatan" id="staf_tmt_pengangkatan" value="<?php echo $staf_tmt_pengangkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_posisi_sekarang" class="col-md-2 control-label" style="text-align:left;">Posisi Sekarang</label>
							<div class="col-md-10">
								<?php combobox('1d', array("Guru", "Dosen", "Karyawan"), 'staf_posisi_sekarang', '', '', $staf_posisi_sekarang, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_bidang_studi" class="col-md-2 control-label" style="text-align:left;">Bidang Studi</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_bidang_studi" id="staf_bidang_studi" value="<?php echo $staf_bidang_studi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_jabatan" class="col-md-2 control-label" style="text-align:left;">Jabatan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_jabatan" id="staf_jabatan" value="<?php echo $staf_jabatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_pangkat_golongan" class="col-md-2 control-label" style="text-align:left;">Pangkat / Golongan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_pangkat_golongan" id="staf_pangkat_golongan" value="<?php echo $staf_pangkat_golongan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_lokasi_kerja" class="col-md-2 control-label" style="text-align:left;">Lokasi Kerja</label>
							<div class="col-md-10">
								<?php combobox('1d', array("JL. MARTADINATA 52", "JL. MARTADINATA 91-93", "GOR JL.SURAPATI"), 'staf_lokasi_kerja', '', '', $staf_lokasi_kerja, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status" class="col-md-2 control-label" style="text-align:left;">Departemen</label>
							<div class="col-md-10">
								<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_id", "departemen_id", "departemen_nama", $departemen_id, '', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_tst" class="col-md-2 control-label" style="text-align:left;">Tanggal Akhir Kerja</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tst" id="staf_tst" value="<?php echo $staf_tst; ?>" placeholder="">
								<small>Digunaan untuk menghilangkan pegawai yang sudah tidak bekerja di YTB</small>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Ubah Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$staf_id);?>" method="post">
				<input type="hidden" class="form-control" name="staf_id" id="staf_id" value="<?php echo $staf_id; ?>" placeholder="">
				<div class="box-header">
					  <h3 class="box-title">Informasi Akun</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_user" class="col-md-2 control-label" style="text-align:left;">User</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_user" id="staf_user" value="<?php echo $staf_user; ?>" placeholder="">
							</div>
							<label for="staf_pin" class="col-md-2 control-label" style="text-align:left;">PIN</label>
							<div class="col-md-4">
								<input type="number" maxlength="6" class="form-control" name="staf_pin" id="staf_pin" value="<?php echo $staf_pin; ?>" placeholder="">
								<small>* Hapus 6 Karakter.</small>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								* Apabila <strong> User</strong> tidak diisi maka user guru tidak akan dibuat. <br />
								* Apabila <strong>PIN</strong> tidak diisi maka ping guru akan dibuat otomatis 6 Karakter Acak. <br />
							</div>
						</div>
					</div>
					<div class="box-header">
					  <h3 class="box-title">Informasi Identitas</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_nip" class="col-md-2 control-label" style="text-align:left;">Nomor Induk Pegawai</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nip" id="staf_nip" value="<?php echo $staf_nip; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_gelar_depan" class="col-md-2 control-label" style="text-align:left;">Gelar Depan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_gelar_depan" id="staf_gelar_depan" value="<?php echo $staf_gelar_depan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nama" class="col-md-2 control-label" style="text-align:left;">Nama Lengkap<br /><small style="font-weight:normal">(Tanpa Gelar)</small></label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama" id="staf_nama" value="<?php echo $staf_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_gelar_belakang" class="col-md-2 control-label" style="text-align:left;">Gelar Belakang</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_gelar_belakang" id="staf_gelar_belakang" value="<?php echo $staf_gelar_belakang; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_tempat_lahir" class="col-md-2 control-label" style="text-align:left;">Tempat, Tanggal Lahir</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="staf_tempat_lahir" id="staf_tempat_lahir" value="<?php echo $staf_tempat_lahir; ?>" placeholder="">
							</div>
							<div class="col-md-5">
								<input type="date" class="form-control" name="staf_tanggal_lahir" id="staf_tanggal_lahir" value="<?php echo $staf_tanggal_lahir; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_pendidikan_terakhir" class="col-md-2 control-label" style="text-align:left;">Pendidikan Terkahir</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_pendidikan_terakhir" id="staf_pendidikan_terakhir" value="<?php echo $staf_pendidikan_terakhir; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nama_ibu_kandung" class="col-md-2 control-label" style="text-align:left;">Nama Ibu Kandung</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama_ibu_kandung" id="staf_nama_ibu_kandung" value="<?php echo $staf_nama_ibu_kandung; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_jenis_kelamin" class="col-md-2 control-label" style="text-align:left;">Jenis Kelamin</label>
							<div class="col-md-10">
								<div class="radio">
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="L" <?php echo $jenis_kelamin_a = ($staf_jenis_kelamin == 'L')?'checked':''; ?>> L
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="P" <?php echo $jenis_kelamin_h = ($staf_jenis_kelamin == 'P')?'checked':''; ?>> P
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_agama" class="col-md-2 control-label" style="text-align:left;">Agama & Kepercayaan</label>
							<div class="col-md-10">
								<?php combobox('1d', array("Islam", "Kristen", "Katholik", "Hindu", "Budha", "Kong Hu Chu", "Kepercayaan kpd Tuhan YME", "Lainnya"), 'staf_agama', '', '', $staf_agama, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status_perkawinan" class="col-md-2 control-label" style="text-align:left;">Status Perkawinan</label>
							<div class="col-md-4">
								<?php combobox('1d', array("Menikah", "Belum Menikah", "Janda/Duda"), 'staf_status_perkawinan', '', '', $staf_status_perkawinan, '', '', 'class="form-control"');?>
							</div>
							<label for="staf_nama_suami_istri" class="col-md-2 control-label" style="text-align:left;">Tanggal Menikah</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_nama_suami_istri" id="staf_nama_suami_istri" value="<?php echo $staf_nama_suami_istri; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_golongan_darah" class="col-md-2 control-label" style="text-align:left;">Golongan Darah</label>
							<div class="col-md-10">
								<?php combobox('1d', array("A", "B", "AB", "O"), 'staf_golongan_darah', '', '', $staf_golongan_darah, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_alamat" class="col-md-2 control-label" style="text-align:left;">Alamat Rumah</label>
							<div class="col-md-10">
								<textarea type="text" class="form-control" name="staf_alamat" id="staf_alamat" placeholder=""><?php echo $staf_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_dusun" class="col-md-2 control-label" style="text-align:left;">RT/RW</label>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rt" id="staf_rt" value="<?php echo $staf_rt; ?>" placeholder="RT">
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rw" id="staf_rw" value="<?php echo $staf_rw; ?>" placeholder="RW">
							</div>
							<label for="staf_dusun" class="col-md-2 control-label" style="text-align:left;">Dusun</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_dusun" id="staf_dusun" value="<?php echo $staf_dusun; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_deskel" class="col-md-2 control-label" style="text-align:left;">Desa/Kelurahan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_deskel" id="staf_deskel" value="<?php echo $staf_deskel; ?>" placeholder="">
							</div>
							<label for="staf_kecamatan" class="col-md-2 control-label" style="text-align:left;">Kecamatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kecamatan" id="staf_kecamatan" value="<?php echo $staf_kecamatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_kabkota" class="col-md-2 control-label" style="text-align:left;">Kota/Kabupaten</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kabkota" id="staf_kabkota" value="<?php echo $staf_kabkota; ?>" placeholder="">
							</div>
							<label for="staf_provinsi" class="col-md-2 control-label" style="text-align:left;">Provinsi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_provinsi" id="staf_provinsi" value="<?php echo $staf_provinsi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_kodepos" class="col-md-2 control-label" style="text-align:left;">Kode Pos</label>
							<div class="col-md-4">
								<input type="number" class="form-control" name="staf_kodepos" id="staf_kodepos" value="<?php echo $staf_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_telepon" class="col-md-2 control-label" style="text-align:left;">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_telepon" id="staf_telepon" value="<?php echo $staf_telepon; ?>" placeholder="">
							</div>
							<label for="staf_hp" class="col-md-2 control-label" style="text-align:left;">HP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_hp" id="staf_hp" value="<?php echo $staf_hp; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_email" class="col-md-2 control-label" style="text-align:left;">Email</label>
							<div class="col-md-10">
								<input type="email" class="form-control" name="staf_email" id="staf_email" value="<?php echo $staf_email; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nik" class="col-md-2 control-label" style="text-align:left;">No. KTP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nik" id="staf_nik" value="<?php echo $staf_nik; ?>" placeholder="">
							</div>
							<label for="staf_no_kk" class="col-md-2 control-label" style="text-align:left;">No. KK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_no_kk" id="staf_no_kk" value="<?php echo $staf_no_kk; ?>" placeholder="">
							</div>
						</div>
						
						<div class="form-group">
							<label for="staf_npwp" class="col-md-2 control-label" style="text-align:left;">NPWP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_npwp" id="staf_npwp" value="<?php echo $staf_npwp; ?>" placeholder="">
							</div>
							<label for="staf_status_pajak" class="col-md-2 control-label" style="text-align:left;">Status Pajak</label>
							<div class="col-md-4">
								<?php combobox('1d', array("TK - Tidak Kawin", "K/0 - Kawin Anak 0", "K/1 - Kawin Anak 1", "K/2 - Kawin Anak 2", "K/3 - Kawin Anak 3"), 'staf_status_pajak', '', '', $staf_status_pajak, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status_sdm" class="col-md-2 control-label" style="text-align:left;">Status SDM</label>
							<div class="col-md-4">
								<?php combobox('1d', array("1. Pegawai Tetap", "2. Pegawai Tidak Tetap"), 'staf_status_sdm', '', '', $staf_status_sdm, '', '', 'class="form-control"');?>
							</div>
							<label for="staf_tmt" class="col-md-2 control-label" style="text-align:left;">Mulai Bekerja di YTB</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt" id="staf_tmt" value="<?php echo $staf_tmt; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_sk_pengangkatan" class="col-md-2 control-label" style="text-align:left;">No. SK Pengangkatan Tetap</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_sk_pengangkatan" id="staf_sk_pengangkatan" value="<?php echo $staf_sk_pengangkatan; ?>" placeholder="">
							</div>
							<label for="staf_tmt_pengangkatan" class="col-md-2 control-label" style="text-align:left;">Tanggal SK Pengangkatan Tetap</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt_pengangkatan" id="staf_tmt_pengangkatan" value="<?php echo $staf_tmt_pengangkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_posisi_sekarang" class="col-md-2 control-label" style="text-align:left;">Posisi Sekarang</label>
							<div class="col-md-10">
								<?php combobox('1d', array("Guru", "Dosen", "Karyawan"), 'staf_posisi_sekarang', '', '', $staf_posisi_sekarang, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_bidang_studi" class="col-md-2 control-label" style="text-align:left;">Bidang Studi</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_bidang_studi" id="staf_bidang_studi" value="<?php echo $staf_bidang_studi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_jabatan" class="col-md-2 control-label" style="text-align:left;">Jabatan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_jabatan" id="staf_jabatan" value="<?php echo $staf_jabatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_pangkat_golongan" class="col-md-2 control-label" style="text-align:left;">Pangkat / Golongan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_pangkat_golongan" id="staf_pangkat_golongan" value="<?php echo $staf_pangkat_golongan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_lokasi_kerja" class="col-md-2 control-label" style="text-align:left;">Lokasi Kerja</label>
							<div class="col-md-10">
								<?php combobox('1d', array("JL. MARTADINATA 52", "JL. MARTADINATA 91-93", "GOR JL.SURAPATI"), 'staf_lokasi_kerja', '', '', $staf_lokasi_kerja, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status" class="col-md-2 control-label" style="text-align:left;">Departemen</label>
							<div class="col-md-10">
								<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_id", "departemen_id", "departemen_nama", $departemen_id, '', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_tst" class="col-md-2 control-label" style="text-align:left;">Tanggal Akhir Kerja</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tst" id="staf_tst" value="<?php echo $staf_tst; ?>" placeholder="">
								<small>Digunaan untuk menghilangkan pegawai yang sudah tidak bekerja di YTB</small>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Import Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
								<div class="input-group">
									<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
									<span class="input-group-btn">
										<input type="submit" name="importStaf" value="Import" class="btn btn-primary" />
									</span>
								</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetStaf" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
								  <th nowrap>NO</th>
								  <th nowrap>USERID</th>
								  <th nowrap>PIN</th>
								  <th nowrap>NOMOR INDUK PEGAWAI</th>
								  <th nowrap>GELAR DEPAN</th>
								  <th nowrap>NAMA LENGKAP</th>
								  <th nowrap>GELAR BELAKANG</th>
								  <th nowrap>TEMPAT LAHIR</th>
								  <th nowrap>TANGGAL LAHIR</th>
								  <th nowrap>PENDIDIKAN TERAKHIR</th>
								  <th nowrap>NAMA IBU KANDUNG</th>
								  <th nowrap>JENIS KELAMIN</th>
								  <th nowrap>AGAMA</th>
								  <th nowrap>STATUS PERKAWINAN</th>
								  <th nowrap>TANGGAL MENIKAH</th>
								  <th nowrap>GOLONGAN DARAH</th>
								  <th nowrap>ALAMAT RUMAH</th>
								  <th nowrap>RT</th>
								  <th nowrap>RW</th>
								  <th nowrap>NAMA DUSUN</th>
								  <th nowrap>DESA/KELURAHAN</th>
								  <th nowrap>KECAMATAN</th>
								  <th nowrap>KABUPATEN/KOTA</th>
								  <th nowrap>PROVINSI</th>
								  <th nowrap>KODE POS</th>
								  <th nowrap>TELEPON</th>
								  <th nowrap>HP</th>
								  <th nowrap>EMAIL</th>
								  <th nowrap>NO. KTP</th>
								  <th nowrap>NO. KK</th>
								  <th nowrap>NPWP</th>
								  <th nowrap>STATUS PAJAK</th>
								  <th nowrap>STATUS SDM</th>
								  <th nowrap>MULAI BEKERJA DI YTB</th>
								  <th nowrap>NO. SK PENGANGKATAN</th>
								  <th nowrap>TANGGAL SK PENGANGKATAN</th>
								  <th nowrap>POSISI SEKARANG</th>
								  <th nowrap>BIDANG STUDI</th>
								  <th nowrap>JABATAN</th>
								  <th nowrap>PANGKAT/GOLONGAN</th>
								  <th nowrap>LOKASI KERJA</th>
								  <th nowrap>KODE UNIT</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
									$dept_color = 'style="background:#EEB9B9"';
									if ($row['departemen_id']){
										$dept_color = 'style="background:#B1E3B1"';
									}
									?>
									<tr <?php echo $dept_color; ?>>
									  <th nowrap scope="row"><?php echo $i; ?></th>
									  <td nowrap><?php echo $row['staf_user']; ?></td>
									  <td nowrap><?php echo $row['staf_pin']; ?></td>
									  <td nowrap><?php echo $row['staf_nip']; ?></td>
									  <td nowrap><?php echo $row['staf_gelar_depan']; ?></td>
									  <td nowrap><?php echo $row['staf_nama']; ?></td>
									  <td nowrap><?php echo $row['staf_gelar_belakang']; ?></td>
									  <td nowrap><?php echo $row['staf_tempat_lahir']; ?></td>
									  <td nowrap><?php echo $row['staf_tanggal_lahir']; ?></td>
									  <td nowrap><?php echo $row['staf_pendidikan_terakhir']; ?></td>
									  <td nowrap><?php echo $row['staf_nama_ibu_kandung']; ?></td>
									  <td nowrap><?php echo $row['staf_jenis_kelamin']; ?></td>
									  <td nowrap><?php echo $row['staf_agama']; ?></td>
									  <td nowrap><?php echo $row['staf_status_perkawinan']; ?></td>
									  <td nowrap><?php echo $row['staf_tanggal_menikah']; ?></td>
									  <td nowrap><?php echo $row['staf_golongan_darah']; ?></td>
									  <td nowrap><?php echo $row['staf_alamat']; ?></td>
									  <td nowrap><?php echo $row['staf_rt']; ?></td>
									  <td nowrap><?php echo $row['staf_rw']; ?></td>
									  <td nowrap><?php echo $row['staf_dusun']; ?></td>
									  <td nowrap><?php echo $row['staf_deskel']; ?></td>
									  <td nowrap><?php echo $row['staf_kecamatan']; ?></td>
									  <td nowrap><?php echo $row['staf_kabkota']; ?></td>
									  <td nowrap><?php echo $row['staf_provinsi']; ?></td>
									  <td nowrap><?php echo $row['staf_kodepos']; ?></td>
									  <td nowrap><?php echo $row['staf_telepon']; ?></td>
									  <td nowrap><?php echo $row['staf_hp']; ?></td>
									  <td nowrap><?php echo $row['staf_email']; ?></td>
									  <td nowrap><?php echo $row['staf_nik']; ?></td>
									  <td nowrap><?php echo $row['staf_no_kk']; ?></td>
									  <td nowrap><?php echo $row['staf_npwp']; ?></td>
									  <td nowrap><?php echo $row['staf_status_pajak']; ?></td>
									  <td nowrap><?php echo $row['staf_status_sdm']; ?></td>
									  <td nowrap><?php echo $row['staf_tmt']; ?></td>
									  <td nowrap><?php echo $row['staf_sk_pengangkatan']; ?></td>
									  <td nowrap><?php echo $row['staf_tmt_pengangkatan']; ?></td>
									  <td nowrap><?php echo $row['staf_posisi_sekarang']; ?></td>
									  <td nowrap><?php echo $row['staf_bidang_studi']; ?></td>
									  <td nowrap><?php echo $row['staf_jabatan']; ?></td>
									  <td nowrap><?php echo $row['staf_pangkat_golongan']; ?></td>
									  <td nowrap><?php echo $row['staf_lokasi_kerja']; ?></td>
									  <td nowrap><?php echo $row['staf_departemen']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'upload_foto') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Upload Foto Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Upload Foto Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Upload File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="fileFoto[]" class="form-control" placeholder="" multiple="true" data-required="true" accept="image/x-png, image/gif, image/jpeg" />
								<span class="input-group-btn">
									<input type="submit" name="uploadFoto" value="Upload" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Detail Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Staf</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
						<div class="form-group row">
							<label for="staf_user" class="col-md-2 control-label" style="text-align:left;">User</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_user" id="staf_user" value="<?php echo $staf_user; ?>" placeholder="" readonly="readonly">
							</div>
							<label for="staf_pin" class="col-md-2 control-label" style="text-align:left;">PIN</label>
							<div class="col-md-4">
								<input type="number" maxlength="6" class="form-control" name="staf_pin" id="staf_pin" value="<?php echo $staf_pin; ?>" placeholder="" readonly="readonly">
								<small>* Hapus 6 Karakter.</small>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								* Apabila <strong> User</strong> tidak diisi maka user guru tidak akan dibuat. <br />
								* Apabila <strong>PIN</strong> tidak diisi maka ping guru akan dibuat otomatis 6 Karakter Acak. <br />
							</div>
						</div>
					</div>
					<div class="box-header">
					  <h3 class="box-title">Informasi Identitas</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group row">
							<label for="staf_nip" class="col-md-2 control-label" style="text-align:left;">Nomor Induk Pegawai</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nip" id="staf_nip" value="<?php echo $staf_nip; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_gelar_depan" class="col-md-2 control-label" style="text-align:left;">Gelar Depan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_gelar_depan" id="staf_gelar_depan" value="<?php echo $staf_gelar_depan; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_nama" class="col-md-2 control-label" style="text-align:left;">Nama Lengkap<br /><small style="font-weight:normal">(Tanpa Gelar)</small></label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama" id="staf_nama" value="<?php echo $staf_nama; ?>" placeholder="" readonly="readonly" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_gelar_belakang" class="col-md-2 control-label" style="text-align:left;">Gelar Belakang</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_gelar_belakang" id="staf_gelar_belakang" value="<?php echo $staf_gelar_belakang; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_tempat_lahir" class="col-md-2 control-label" style="text-align:left;">Tempat, Tanggal Lahir</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="staf_tempat_lahir" id="staf_tempat_lahir" value="<?php echo $staf_tempat_lahir; ?>" placeholder="" readonly="readonly">
							</div>
							<div class="col-md-5">
								<input type="date" class="form-control" name="staf_tanggal_lahir" id="staf_tanggal_lahir" value="<?php echo $staf_tanggal_lahir; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_pendidikan_terakhir" class="col-md-2 control-label" style="text-align:left;">Pendidikan Terkahir</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_pendidikan_terakhir" id="staf_pendidikan_terakhir" value="<?php echo $staf_pendidikan_terakhir; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_nama_ibu_kandung" class="col-md-2 control-label" style="text-align:left;">Nama Ibu Kandung</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama_ibu_kandung" id="staf_nama_ibu_kandung" value="<?php echo $staf_nama_ibu_kandung; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_jenis_kelamin" class="col-md-2 control-label" style="text-align:left;">Jenis Kelamin</label>
							<div class="col-md-10">
								<div class="radio">
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="L" <?php echo $jenis_kelamin_a = ($staf_jenis_kelamin == 'L')?'checked':''; ?> disabled> L
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="P" <?php echo $jenis_kelamin_h = ($staf_jenis_kelamin == 'P')?'checked':''; ?> disabled> P
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_agama" class="col-md-2 control-label" style="text-align:left;">Agama & Kepercayaan</label>
							<div class="col-md-10">
								<?php combobox('1d', array("Islam", "Kristen", "Katholik", "Hindu", "Budha", "Kong Hu Chu", "Kepercayaan kpd Tuhan YME", "Lainnya"), 'staf_agama', '', '', $staf_agama, '', '', 'class="form-control" disabled');?>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_status_perkawinan" class="col-md-2 control-label" style="text-align:left;">Status Perkawinan</label>
							<div class="col-md-4">
								<?php combobox('1d', array("Menikah", "Belum Menikah", "Janda/Duda"), 'staf_status_perkawinan', '', '', $staf_status_perkawinan, '', '', 'class="form-control" disabled');?>
							</div>
							<label for="staf_nama_suami_istri" class="col-md-2 control-label" style="text-align:left;">Tanggal Menikah</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_nama_suami_istri" id="staf_nama_suami_istri" value="<?php echo $staf_nama_suami_istri; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_golongan_darah" class="col-md-2 control-label" style="text-align:left;">Golongan Darah</label>
							<div class="col-md-10">
								<?php combobox('1d', array("A", "B", "AB", "O"), 'staf_golongan_darah', '', '', $staf_golongan_darah, '', '', 'class="form-control" disabled');?>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_alamat" class="col-md-2 control-label" style="text-align:left;">Alamat Rumah</label>
							<div class="col-md-10">
								<textarea type="text" class="form-control" name="staf_alamat" id="staf_alamat" placeholder="" readonly="readonly"><?php echo $staf_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_dusun" class="col-md-2 control-label" style="text-align:left;">RT/RW</label>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rt" id="staf_rt" value="<?php echo $staf_rt; ?>" placeholder="RT" disabled>
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rw" id="staf_rw" value="<?php echo $staf_rw; ?>" placeholder="RW" disabled>
							</div>
							<label for="staf_dusun" class="col-md-2 control-label" style="text-align:left;">Dusun</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_dusun" id="staf_dusun" value="<?php echo $staf_dusun; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_deskel" class="col-md-2 control-label" style="text-align:left;">Desa/Kelurahan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_deskel" id="staf_deskel" value="<?php echo $staf_deskel; ?>" placeholder="" readonly="readonly">
							</div>
							<label for="staf_kecamatan" class="col-md-2 control-label" style="text-align:left;">Kecamatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kecamatan" id="staf_kecamatan" value="<?php echo $staf_kecamatan; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_kabkota" class="col-md-2 control-label" style="text-align:left;">Kota/Kabupaten</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kabkota" id="staf_kabkota" value="<?php echo $staf_kabkota; ?>" placeholder="" readonly="readonly">
							</div>
							<label for="staf_provinsi" class="col-md-2 control-label" style="text-align:left;">Provinsi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_provinsi" id="staf_provinsi" value="<?php echo $staf_provinsi; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_kodepos" class="col-md-2 control-label" style="text-align:left;">Kode Pos</label>
							<div class="col-md-4">
								<input type="number" class="form-control" name="staf_kodepos" id="staf_kodepos" value="<?php echo $staf_kodepos; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_telepon" class="col-md-2 control-label" style="text-align:left;">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_telepon" id="staf_telepon" value="<?php echo $staf_telepon; ?>" placeholder="" readonly="readonly">
							</div>
							<label for="staf_hp" class="col-md-2 control-label" style="text-align:left;">HP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_hp" id="staf_hp" value="<?php echo $staf_hp; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_email" class="col-md-2 control-label" style="text-align:left;">Email</label>
							<div class="col-md-10">
								<input type="email" class="form-control" name="staf_email" id="staf_email" value="<?php echo $staf_email; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_nik" class="col-md-2 control-label" style="text-align:left;">No. KTP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nik" id="staf_nik" value="<?php echo $staf_nik; ?>" placeholder="" readonly="readonly">
							</div>
							<label for="staf_no_kk" class="col-md-2 control-label" style="text-align:left;">No. KK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_no_kk" id="staf_no_kk" value="<?php echo $staf_no_kk; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="staf_npwp" class="col-md-2 control-label" style="text-align:left;">NPWP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_npwp" id="staf_npwp" value="<?php echo $staf_npwp; ?>" placeholder="" readonly="readonly">
							</div>
							<label for="staf_status_pajak" class="col-md-2 control-label" style="text-align:left;">Status Pajak</label>
							<div class="col-md-4">
								<?php combobox('1d', array("TK - Tidak Kawin", "K/0 - Kawin Anak 0", "K/1 - Kawin Anak 1", "K/2 - Kawin Anak 2", "K/3 - Kawin Anak 3"), 'staf_status_pajak', '', '', $staf_status_pajak, '', '', 'class="form-control" disabled');?>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_status_sdm" class="col-md-2 control-label" style="text-align:left;">Status SDM</label>
							<div class="col-md-4">
								<?php combobox('1d', array("1. Pegawai Tetap", "2. Pegawai Tidak Tetap"), 'staf_status_sdm', '', '', $staf_status_sdm, '', '', 'class="form-control" disabled');?>
							</div>
							<label for="staf_tmt" class="col-md-2 control-label" style="text-align:left;">Mulai Bekerja di YTB</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt" id="staf_tmt" value="<?php echo $staf_tmt; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_sk_pengangkatan" class="col-md-2 control-label" style="text-align:left;">No. SK Pengangkatan Tetap</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_sk_pengangkatan" id="staf_sk_pengangkatan" value="<?php echo $staf_sk_pengangkatan; ?>" placeholder="" readonly="readonly">
							</div>
							<label for="staf_tmt_pengangkatan" class="col-md-2 control-label" style="text-align:left;">Tanggal SK Pengangkatan Tetap</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt_pengangkatan" id="staf_tmt_pengangkatan" value="<?php echo $staf_tmt_pengangkatan; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_posisi_sekarang" class="col-md-2 control-label" style="text-align:left;">Posisi Sekarang</label>
							<div class="col-md-10">
								<?php combobox('1d', array("Guru", "Dosen", "Karyawan"), 'staf_posisi_sekarang', '', '', $staf_posisi_sekarang, '', '', 'class="form-control" disabled');?>
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_bidang_studi" class="col-md-2 control-label" style="text-align:left;">Bidang Studi</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_bidang_studi" id="staf_bidang_studi" value="<?php echo $staf_bidang_studi; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_jabatan" class="col-md-2 control-label" style="text-align:left;">Jabatan</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_jabatan" id="staf_jabatan" value="<?php echo $staf_jabatan; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_pangkat_golongan" class="col-md-2 control-label" style="text-align:left;">Pangkat / Golongan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_pangkat_golongan" id="staf_pangkat_golongan" value="<?php echo $staf_pangkat_golongan; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="form-group row">
							<label for="staf_lokasi_kerja" class="col-md-2 control-label" style="text-align:left;">Lokasi Kerja</label>
							<div class="col-md-10">
								<?php combobox('1d', array("JL. MARTADINATA 52", "JL. MARTADINATA 91-93", "GOR JL.SURAPATI"), 'staf_lokasi_kerja', '', '', $staf_lokasi_kerja, '', '', 'class="form-control" disabled');?>
							</div>
						</div>
						<div class="form-group row">
							<label for="departemen_id" class="col-md-2 control-label" style="text-align:left;">Departemen</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="departemen_kode" id="departemen_kode" value="<?php echo $departemen_kode; ?>" placeholder="" readonly="readonly">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group row">
							<label for="staf_tst" class="col-md-2 control-label" style="text-align:left;">Tanggal Akhir Kerja</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tst" id="staf_tst" value="<?php echo $staf_tst; ?>" placeholder="" readonly="readonly">
								<small>Digunaan untuk menghilangkan pegawai yang sudah tidak bekerja di YTB</small>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$staf_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>