<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('departemen/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "departemen_kode"},
			{ "data": "departemen_nama"},
			{ "data": "departemen_tipe"},
			{ "data": "departemen_parent"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthDepartemen": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Departemen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('departemen'); ?>">Departemen</a></li>
            <li class="active">Daftar Departemen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Departemen</h3>
								<div class="pull-right">
								<?php if (check_permission("W")){?>
								<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
								<?php } ?>
								</div>
                </div><!-- /.box-header --><div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Departemen</th>
                        <th>Tipe</th>
                        <th>Parent</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Departemen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('departemen'); ?>">Departemen</a></li>
            <li class="active">Tambah Departemen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Departemen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<label for="departemen_utama" class="col-sm-2 control-label">Parent</label>
							<div class="col-sm-4 col-sm-6">
								<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_utama", "departemen_id", "departemen_nama", $departemen_utama, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="departemen_kode" id="departemen_kode" value="<?php echo $departemen_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="departemen_nama" id="departemen_nama" value="<?php echo $departemen_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-md-4 col-sm-6">
								<input type="number" min="0" class="form-control" name="departemen_urutan" id="departemen_urutan" value="<?php echo $departemen_urutan; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_tipe" class="col-sm-2 control-label">Tipe</label>
							<div class="col-md-4 col-sm-6">
								<?php combobox('1d', array("Unit", "Bagian", "Sekolah", "Akademi"), 'departemen_tipe', '', '', $departemen_tipe, '', 'none', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Departemen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('departemen'); ?>">Departemen</a></li>
            <li class="active">Ubah Departemen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Departemen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$departemen_id);?>" method="post">
					<input type="hidden" class="form-control" name="departemen_id" id="departemen_id" value="<?php echo $departemen_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group">
							<label for="departemen_utama" class="col-sm-2 control-label">Parent</label>
							<div class="col-sm-4 col-sm-6">
								<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_utama", "departemen_id", "departemen_nama", $departemen_utama, '', '', 'class="form-control select2"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="departemen_kode" id="departemen_kode" value="<?php echo $departemen_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="departemen_nama" id="departemen_nama" value="<?php echo $departemen_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-md-4 col-sm-6">
								<input type="number" min="0" class="form-control" name="departemen_urutan" id="departemen_urutan" value="<?php echo $departemen_urutan; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="departemen_tipe" class="col-sm-2 control-label">Tipe</label>
							<div class="col-md-4 col-sm-6">
								<?php combobox('1d', array("Unit", "Bagian", "Sekolah", "Akademi"), 'departemen_tipe', '', '', $departemen_tipe, '', 'none', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Departemen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('departemen'); ?>">Departemen</a></li>
            <li class="active">Detail Departemen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Departemen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="departemen_id" id="departemen_id" value="<?php echo $departemen_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group row">
							<label for="departemen_kode" class="col-sm-2 control-label">Kode</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $departemen_kode; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="departemen_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $departemen_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="departemen_urutan" class="col-sm-2 control-label">Urutan</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $departemen_urutan; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$departemen_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Departemen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('guru-pelajaran'); ?>">Departemen</a></li>
            <li class="active">Import Departemen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Departemen</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
								<span class="input-group-btn">
									<input type="submit" name="importKD" value="Import" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetKD" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
								  <th>#</th>
								  <th>STATUS</th>
								  <th>TAHUN AJARAN</th>
								  <th>TINGKAT</th>
								  <th>JURUSAN/PEMINATAN</th>
								  <th>KELAS</th>
								  <th>GURU</th>
								  <th>BK</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
                  $nilaistatus = "";
									if ($row['nilai_status'] == 'Not Valid'){
										$nilaistatus = 'style="background:#EEB9B9"';
									} else {
										$nilaistatus = 'style="background:#B1E3B1"';
									}
									?>
									<tr <?php echo $nilaistatus; ?>>
										<th scope="row"><?php echo $i; ?></th>
										<td><?php echo $row['nilai_status']; ?></td>
									  <td><?php echo $row['tahun_ajaran']; ?></td>
									  <td><?php echo $row['tingkat']; ?></td>
									  <td><?php echo $row['peminatan']; ?></td>
									  <td><?php echo $row['departemen']; ?></td>
									  <td><?php echo $row['guru']; ?></td>
									  <td><?php echo $row['bk']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'copy') {?>
  <script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<style>
  .table-raport>thead>tr>th, .table-raport>tbody>tr>th, .table-raport>tfoot>tr>th, .table-raport>thead>tr>td, .table-raport>tbody>tr>td, .table-raport>tfoot>tr>td {
    border: 1px solid #000;
    padding: 3px 8px;
  }
  .table-raport .btn-group-sm>.btn, .table-raport .btn-sm {
    padding: 0px 7px;
  }
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Departemen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('format_raport'); ?>">Departemen</a></li>
            <li class="active">Copy Departemen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Copy Departemen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				        <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                  <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tahun_kode" class="control-label">Tahun Asal</label>
                                <div class="">
                                    <?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
                                </div>		
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="control-label">&nbsp;</label>
                                <div class="text-center">
                                    <button type="button" class="btn btn-default">&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;</button>
                                </div>		
                            </div>
                        </div>
												<div class="col-md-4">
                            <div class="form-group">
                                <label for="tahun_tujuan" class="control-label">Tahun Tujuan</label>
                                <div class="">
                                    <?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_tujuan', 'tahun_kode', 'tahun_nama', $tahun_tujuan, 'submit();', 'none', 'class="form-control select2" required');?>
                                </div>		
                            </div>
                        </div>
                  </div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Copy</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index'); ?>'" class="btn btn-default">Kembali</button>
					</div><!-- /.box-footer -->
				</form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>