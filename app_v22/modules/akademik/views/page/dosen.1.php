<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('dosen/datatable/'.$program_studi_id.'/'.$status_aktivitas_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "dosen_nama"},
			{ "data": "dosen_nidn"},
			{ "data": "dosen_user"},
			{ "data": "dosen_pin"},
			{ "data": "status_aktivitas_nama"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dosen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Dosen</a></li>
            <li class="active">Daftar Dosen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Dosen</h3>
									<div class="pull-right">
									<a href="<?php echo base_url('asset/file/format_import_dosen.xls'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-download"></span> Download Format Import</a>
									<a href="<?php echo module_url($this->uri->segment(2).'/import'); ?>" class="btn btn-primary"><span class="fa fa-download"></span> Import Dosen</a>
									<a href="<?php echo module_url($this->uri->segment(2).'/generate-account'); ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin? \nAkan men-generate user dosen.');"><span class="fa fa-cog"></span> Generate User Dosen</a>
									<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
									</div>
                </div><!-- /.box-header -->
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$status_aktivitas_kode);?>" method="post">
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="program_studi_id" class="control-label">Program Studi</label>
												<div class="">
													<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Status Aktivitas</label>
												<div class="">	
													<?php echo $this->Referensi_model->combobox(15, 'status_aktivitas_kode', 'kode_value', 'kode_nama', $status_aktivitas_kode, '', '', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
									</div>
								</div>
								</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>NIDN</th>
                        <th>User</th>
                        <th>Password</th>
                        <th>Status Aktivitas</th>
                        <th style="width:130px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#dosen_tanggal_lahir').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<script>
$(function () {
	$("#dosen_user").change(function(){
		var params = {
			action: 'add',
			user: $("#dosen_user").val()
		};
		// console.log(params);
		
		$.ajax({
			url: "<?php echo module_url($this->uri->segment(2).'/check-user'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			beforeSend: function() {},
			success: function(data){
				if (data.response == false){
					$("#dosen_user").css("border-color", "#a94442");
					$("#form-add").attr('onsubmit', 'return false;');
				} else {
					$("#dosen_user").css("border-color", "#5cb85c");
					$("#form-add").attr('onsubmit', 'return true;');
				}
			},
			complete: function() {},
		});
	});
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dosen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Dosen</a></li>
            <li class="active">Tambah Dosen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Dosen</h3>
                </div><!-- /.box-header -->
				<form id="form-add" class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
				<div class="box-header">
					<h3 class="box-title">Informasi Akun</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="dosen_user" class="col-md-2 control-label" style="text-align:left">User</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_user" id="dosen_user" value="<?php echo $dosen_user; ?>" placeholder="" required>
						</div>
						<label for="dosen_pin" class="col-md-2 control-label" style="text-align:left">Password</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_pin" id="dosen_pin" value="<?php echo $dosen_pin; ?>" placeholder="">
							<small>* Hapus 6 Karakter.</small>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							* Apabila <strong>User</strong> tidak diisi maka user dosen tidak akan dibuat. <br />
							* Apabila <strong>Password</strong> tidak diisi maka pin dosen akan dibuat otomatis 6 Karakter Acak. <br />
						</div>
					</div>
				</div>
				<div class="box-header">
					<h3 class="box-title">Informasi Identitas</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_nidn" class="col-md-2 control-label" style="text-align:left">NIDN</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_nidn" id="dosen_nidn" value="<?php echo $dosen_nidn; ?>" placeholder="">
						</div>
						<label for="dosen_ktp" class="col-md-2 control-label" style="text-align:left">No. KTP</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_ktp" id="dosen_ktp" value="<?php echo $dosen_ktp; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_nama" id="dosen_nama" value="<?php echo $dosen_nama; ?>" placeholder="" required>
						</div>
						<label for="dosen_gelar" class="col-md-2 control-label" style="text-align:left">Gelar Akademik/Profesional Tertinggi</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_gelar" id="dosen_gelar" value="<?php echo $dosen_gelar; ?>" placeholder="" required>
							<small>Singkatan</small>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(8, 'dosen_jenis_kelamin', 'kode_value', 'kode_nama', $dosen_jenis_kelamin, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="jabatan_akademik_kode" class="col-md-2 control-label" style="text-align:left">Jabatan Akademik</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(2, 'jabatan_akademik_kode', 'kode_value', 'kode_nama', $jabatan_akademik_kode, '', '', 'class="form-control" required');?>
						</div>
						<label for="pendidikan_kode" class="col-md-2 control-label" style="text-align:left">Pendidikan Tertinggi</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(1, 'pendidikan_kode', 'kode_value', 'kode_nama', $pendidikan_kode, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="status_ikatan_kerja_kode" class="col-md-2 control-label" style="text-align:left">Status Ikatan Kerja Dosen</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(3, 'status_ikatan_kerja_kode', 'kode_value', 'kode_nama', $status_ikatan_kerja_kode, '', '', 'class="form-control" required');?>
						</div>
						<label for="status_aktivitas_kode" class="col-md-2 control-label" style="text-align:left">Status Aktivitas Dosen</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(15, 'status_aktivitas_kode', 'kode_value', 'kode_nama', $status_aktivitas_kode, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_semester_mulai" class="col-md-2 control-label" style="text-align:left">Semester Mulai Keluar/Pensiun/Alm.</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'dosen_semester_mulai', 'semester_kode', 'semester_nama', $dosen_semester_mulai, '', '', 'class="form-control" required');?>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_nip_pns" class="col-md-2 control-label" style="text-align:left">NIP PNS</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_nip_pns" id="dosen_nip_pns" value="<?php echo $dosen_nip_pns; ?>" placeholder="">
						</div>
						<label for="dosen_homebase" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi Induk (Homebase)</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_homebase" id="dosen_homebase" value="<?php echo $dosen_homebase; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#dosen_tanggal_lahir').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<script>
$(function () {
	$("#dosen_user").change(function(){
		var params = {
			action: 'edit',
			id: '<?php echo $dosen_id; ?>',
			user: $("#dosen_user").val()
		};
		// console.log(params);
		
		$.ajax({
			url: "<?php echo module_url($this->uri->segment(2).'/check-user'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			beforeSend: function() {},
			success: function(data){
				if (data.response == false){
					$("#dosen_user").css("border-color", "#a94442");
					$("#form-add").attr('onsubmit', 'return false;');
				} else {
					$("#dosen_user").css("border-color", "#5cb85c");
					$("#form-add").attr('onsubmit', 'return true;');
				}
			},
			complete: function() {},
		});
	});
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dosen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Dosen</a></li>
            <li class="active">Ubah Dosen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Dosen</h3>
                </div><!-- /.box-header -->
								<form id="form-add" class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$dosen_id);?>" method="post">
								<input type="hidden" class="form-control" name="dosen_id" id="dosen_id" value="<?php echo $dosen_id; ?>" placeholder="">
								<div class="box-header">
									<h3 class="box-title">Informasi Akun</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="dosen_user" class="col-md-2 control-label" style="text-align:left">User</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_user" id="dosen_user" value="<?php echo $dosen_user; ?>" placeholder="" required>
										</div>
										<label for="dosen_pin" class="col-md-2 control-label" style="text-align:left">Password</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_pin" id="dosen_pin" value="<?php echo $dosen_pin; ?>" placeholder="">
											<small>* Hapus 6 Karakter.</small>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											* Apabila <strong>User</strong> tidak diisi maka user dosen tidak akan dibuat. <br />
											* Apabila <strong>Password</strong> tidak diisi maka pin dosen akan dibuat otomatis 6 Karakter Acak. <br />
										</div>
									</div>
								</div>
								<div class="box-header">
									<h3 class="box-title">Informasi Identitas</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_nidn" class="col-md-2 control-label" style="text-align:left">NIDN</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_nidn" id="dosen_nidn" value="<?php echo $dosen_nidn; ?>" placeholder="">
										</div>
										<label for="dosen_ktp" class="col-md-2 control-label" style="text-align:left">No. KTP</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_ktp" id="dosen_ktp" value="<?php echo $dosen_ktp; ?>" placeholder="">
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_nama" id="dosen_nama" value="<?php echo $dosen_nama; ?>" placeholder="" required>
										</div>
										<label for="dosen_gelar" class="col-md-2 control-label" style="text-align:left">Gelar</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_gelar" id="dosen_gelar" value="<?php echo $dosen_gelar; ?>" placeholder="" required>
											<small>Singkatan</small>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(8, 'dosen_jenis_kelamin', 'kode_value', 'kode_nama', $dosen_jenis_kelamin, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="jabatan_akademik_kode" class="col-md-2 control-label" style="text-align:left">Jabatan Akademik</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(2, 'jabatan_akademik_kode', 'kode_value', 'kode_nama', $jabatan_akademik_kode, '', '', 'class="form-control" required');?>
										</div>
										<label for="pendidikan_kode" class="col-md-2 control-label" style="text-align:left">Pendidikan Tertinggi</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(1, 'pendidikan_kode', 'kode_value', 'kode_nama', $pendidikan_kode, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="status_ikatan_kerja_kode" class="col-md-2 control-label" style="text-align:left">Status Ikatan Kerja Dosen</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(3, 'status_ikatan_kerja_kode', 'kode_value', 'kode_nama', $status_ikatan_kerja_kode, '', '', 'class="form-control" required');?>
										</div>
										<label for="status_aktivitas_kode" class="col-md-2 control-label" style="text-align:left">Status Aktivitas Dosen</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(15, 'status_aktivitas_kode', 'kode_value', 'kode_nama', $status_aktivitas_kode, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_semester_mulai" class="col-md-2 control-label" style="text-align:left">Semester Mulai Keluar/Pensiun/Alm.</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'dosen_semester_mulai', 'semester_kode', 'semester_nama', $dosen_semester_mulai, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_nip_pns" class="col-md-2 control-label" style="text-align:left">NIP PNS</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_nip_pns" id="dosen_nip_pns" value="<?php echo $dosen_nip_pns; ?>" placeholder="">
										</div>
										<label for="dosen_homebase" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi Induk (Homebase)</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_homebase" id="dosen_homebase" value="<?php echo $dosen_homebase; ?>" placeholder="">
										</div>
									</div>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
								</div><!-- /.box-footer -->
								</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dosen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Dosen</a></li>
            <li class="active">Import Dosen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Dosen</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
								<span class="input-group-btn">
									<input type="submit" name="importDosen" value="Import" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetDosen" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
									<th nowrap>#</th>
									<th nowrap>PROGRAM STUDI</th>
									<th nowrap>USER LOGIN</th>
									<th nowrap>PASSWORD LOGIN</th>
									<th nowrap>NO KTP</th>
									<th nowrap>NIDN</th>
									<th nowrap>NAMA LENGKAP</th>
									<th nowrap>GELAR AKADEMIK</th>
									<th nowrap>TEMPAT LAHIR</th>
									<th nowrap>TANGGAL LAHIR</th>
									<th nowrap>JENIS KELAMIN</th>
									<th nowrap>JABATAN AKADEMIK</th>
									<th nowrap>PENDIDIKAN TERTINGGI</th>
									<th nowrap>STATUS IKATAN KERJA</th>
									<th nowrap>STATUS AKTIVITAS</th>
									<th nowrap>SEMESTER AKHIR</th>
									<th nowrap>NIP PNS</th>
									<th nowrap>HOMEBASE</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
									$dataprodi = "";
									$datastatus = "";
									if ($row['data_prodi'] == 'PRODI VALID'){
										$dataprodi = 'style="background:#B1E3B1"';
									} else {
										$dataprodi = 'style="background:#EEB9B9"';
									}
									?>
									<tr <?php echo $dataprodi; ?>>
									  <th scope="row"><?php echo $i; ?></th>
									  <td nowrap><?php echo $row['dosen_prodi']; ?></td>
									  <td nowrap><?php echo $row['dosen_user']; ?></td>
									  <td nowrap><?php echo $row['dosen_pin']; ?></td>
									  <td nowrap><?php echo $row['dosen_ktp']; ?></td>
									  <td nowrap><?php echo $row['dosen_nidn']; ?></td>
									  <td nowrap><?php echo $row['dosen_nama']; ?></td>
									  <td nowrap><?php echo $row['dosen_gelar']; ?></td>
									  <td nowrap><?php echo $row['dosen_tempat_lahir']; ?></td>
									  <td nowrap><?php echo $row['dosen_tanggal_lahir']; ?></td>
									  <td nowrap><?php echo $row['dosen_jenis_kelamin']; ?></td>
									  <td nowrap><?php echo $row['jabatan_akademik_kode']; ?></td>
									  <td nowrap><?php echo $row['pendidikan_kode']; ?></td>
									  <td nowrap><?php echo $row['status_ikatan_kerja_kode']; ?></td>
									  <td nowrap><?php echo $row['status_aktivitas_kode']; ?></td>
									  <td nowrap><?php echo $row['dosen_semester_mulai']; ?></td>
									  <td nowrap><?php echo $row['dosen_nip_pns']; ?></td>
									  <td nowrap><?php echo $row['dosen_homebase']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dosen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Dosen</a></li>
            <li class="active">Detail Dosen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning form-horizontal">
                <div class="box-header with-border">
                  <h3 class="box-title">Detail Dosen</h3>
                </div><!-- /.box-header -->
								<div class="box-header">
									<h3 class="box-title">Informasi Akun</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="dosen_user" class="col-md-2 control-label" style="text-align:left">User</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_user" id="dosen_user" value="<?php echo $dosen_user; ?>" placeholder="" readonly>
										</div>
										<label for="dosen_pin" class="col-md-2 control-label" style="text-align:left">Password</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_pin" id="dosen_pin" value="<?php echo $dosen_pin; ?>" placeholder="" readonly>
											<small>* Hapus 6 Karakter.</small>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											* Apabila <strong>User</strong> tidak diisi maka user dosen tidak akan dibuat. <br />
											* Apabila <strong>Password</strong> tidak diisi maka pin dosen akan dibuat otomatis 6 Karakter Acak. <br />
										</div>
									</div>
								</div>
								<div class="box-header">
									<h3 class="box-title">Informasi Identitas</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" disabled');?>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_nidn" class="col-md-2 control-label" style="text-align:left">NIDN</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_nidn" id="dosen_nidn" value="<?php echo $dosen_nidn; ?>" placeholder="" readonly>
										</div>
										<label for="dosen_ktp" class="col-md-2 control-label" style="text-align:left">No. KTP</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_ktp" id="dosen_ktp" value="<?php echo $dosen_ktp; ?>" placeholder="" readonly>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_nama" id="dosen_nama" value="<?php echo $dosen_nama; ?>" placeholder="" readonly>
										</div>
										<label for="dosen_gelar" class="col-md-2 control-label" style="text-align:left">Gelar</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_gelar" id="dosen_gelar" value="<?php echo $dosen_gelar; ?>" placeholder="" readonly>
											<small>Singkatan</small>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(8, 'dosen_jenis_kelamin', 'kode_value', 'kode_nama', $dosen_jenis_kelamin, '', '', 'class="form-control" disabled');?>
										</div>
									</div>
									<div class="form-group">
										<label for="jabatan_akademik_kode" class="col-md-2 control-label" style="text-align:left">Jabatan Akademik</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(2, 'jabatan_akademik_kode', 'kode_value', 'kode_nama', $jabatan_akademik_kode, '', '', 'class="form-control" disabled');?>
										</div>
										<label for="pendidikan_kode" class="col-md-2 control-label" style="text-align:left">Pendidikan Tertinggi</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(1, 'pendidikan_kode', 'kode_value', 'kode_nama', $pendidikan_kode, '', '', 'class="form-control" disabled');?>
										</div>
									</div>
									<div class="form-group">
										<label for="status_ikatan_kerja_kode" class="col-md-2 control-label" style="text-align:left">Status Ikatan Kerja Dosen</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(3, 'status_ikatan_kerja_kode', 'kode_value', 'kode_nama', $status_ikatan_kerja_kode, '', '', 'class="form-control" disabled');?>
										</div>
										<label for="status_aktivitas_kode" class="col-md-2 control-label" style="text-align:left">Status Aktivitas Dosen</label>
										<div class="col-md-4">
											<?php echo $this->Referensi_model->combobox(15, 'status_aktivitas_kode', 'kode_value', 'kode_nama', $status_aktivitas_kode, '', '', 'class="form-control" disabled');?>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_semester_mulai" class="col-md-2 control-label" style="text-align:left">Semester Mulai Keluar/Pensiun/Alm.</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'dosen_semester_mulai', 'semester_kode', 'semester_nama', $dosen_semester_mulai, '', '', 'class="form-control" disabled');?>
										</div>
									</div>
									<div class="form-group">
										<label for="dosen_nip_pns" class="col-md-2 control-label" style="text-align:left">NIP PNS</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_nip_pns" id="dosen_nip_pns" value="<?php echo $dosen_nip_pns; ?>" placeholder="" readonly>
										</div>
										<label for="dosen_homebase" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi Induk (Homebase)</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="dosen_homebase" id="dosen_homebase" value="<?php echo $dosen_homebase; ?>" placeholder="" readonly>
										</div>
									</div>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
									<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$dosen_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
								</div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>