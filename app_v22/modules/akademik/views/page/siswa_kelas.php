<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('siswa-kelas/datatable/'.$departemen_id.'/'.$tahun_kode.'/'.$tingkat_id.'/'.$kelas_id); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "siswa_nama"},
			{ "data": "siswa_nis"},
			{ "data": "siswa_pin"},
			{ "data": "siswa_pin_orangtua"},
			{ "data": "siswa_kelas_sekarang"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('siswa'); ?>">Siswa</a></li>
            <li class="active">Daftar Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Siswa</h3>
				  <div class="pull-right">
					
				  </div>
                </div><!-- /.box-header -->
				<div class="box-body">
					<form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4));?>" method="post">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="departemen_id" class="control-label">Departemen</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Alumni') ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- DEPARTEMEN --', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							<?php if ($this->session->userdata('level') == 10){?>
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tingkat</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tingkat.departemen_id = '$departemen_id' AND kelas.tahun_kode = '$tahun_kode' AND kelas.siswa_id = '$siswa_id' GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE kelas.tahun_kode = '$tahun_kode' AND kelas.tingkat_id = '$tingkat_id' AND kelas.siswa_id = '$siswa_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							<?php } else { ?>
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tingkat</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tingkat WHERE tingkat.departemen_id = '$departemen_id' ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '$tahun_kode' AND tingkat_id = '$tingkat_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							<?php } ?>
							<?php if ($tahun_kode && $tingkat_id && $kelas_id){?>
							<div class="col-md-4">
								<div class="form-group">
									<label for="kelas_id" class="control-label">&nbsp;</label>
									<div class="">
										<a href="<?php echo module_url($this->uri->segment(2).'/cetak_pin/'.$departemen_id.'/'.$tahun_kode.'/'.$tingkat_id.'/'.$kelas_id); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-print"></span> Pin</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/cetak_kartu/'.$departemen_id.'/'.$tahun_kode.'/'.$tingkat_id.'/'.$kelas_id); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-print"></span> Kartu</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/cetak_pin_excel/'.$departemen_id.'/'.$tahun_kode.'/'.$tingkat_id.'/'.$kelas_id); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-file-excel-o"></span> Excel</a>
									</div>		
								</div>
							</div>
							<?php } else { ?>
							<div class="col-md-3">
								<div class="form-group">
									<label for="kelas_id" class="control-label">&nbsp;</label>
									<div class="">
										<a href="javascript:void(0)" onclick="alert('Silahkan pilih terlebih dahulu tahun ajaran, tingkat dan kelas');" class="btn btn-primary" target="_blank"><span class="fa fa-print"></span> Pin</a>
										<a href="javascript:void(0)" onclick="alert('Silahkan pilih terlebih dahulu tahun ajaran, tingkat dan kelas');" class="btn btn-primary" target="_blank"><span class="fa fa-print"></span> Kartu</a>
										<a href="javascript:void(0)" onclick="alert('Silahkan pilih terlebih dahulu tahun ajaran, tingkat dan kelas');" class="btn btn-primary" target="_blank"><span class="fa fa-file-excel-o"></span> Excel</a>
									</div>		
								</div>
							</div>
							<?php } ?>
						</div>
					</form>
				</div>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>NIS</th>
                        <th>PIN Siswa</th>
                        <th>PIN Orang Tua</th>
                        <th>Kelas</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('siswa'); ?>">Siswa</a></li>
            <li class="active">Detail Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Siswa</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div class="box-header">
										<h3 class="box-title">Informasi Akun</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_user" class="col-md-2 control-label">User</label>
											<div class="col-md-4">
												<?php echo $siswa_user; ?>
											</div>
											<label for="siswa_pin" class="col-md-2 control-label">PIN Siswa</label>
											<div class="col-md-4">
												<?php echo $siswa_pin; ?>
											</div>
											<label for="siswa_pin_orangtua" class="col-md-offset-6 col-md-2 control-label">PIN Orang Tua</label>
											<div class="col-md-4">
												<?php echo $siswa_pin_orangtua; ?>
											</div>
										</div>
									</div>
									<div class="box-header">
										<h3 class="box-title">Informasi Identitas</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_nama" class="col-md-2 control-label">Nama</label>
											<div class="col-md-10">
												<?php echo $siswa_nama; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_jenis_kelamin" class="col-md-2 control-label">Jenis Kelamin</label>
											<div class="col-md-10">
												<?php echo $siswa_jenis_kelamin; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nisn" class="col-md-2 control-label">NISN</label>
											<div class="col-md-4">
												<?php echo $siswa_nisn; ?>
											</div>
											<label for="siswa_nis" class="col-md-2 control-label">NIS</label>
											<div class="col-md-4">
												<?php echo $siswa_nis; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nik" class="col-md-2 control-label">NIK</label>
											<div class="col-md-10">
												<?php echo $siswa_nik; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_tempat_lahir" class="col-md-2 control-label">Tempat, Tanggal Lahir</label>
											<div class="col-md-5">
												<?php echo $siswa_tempat_lahir; ?>
											</div>
											<div class="col-md-5">
												<?php echo $siswa_tanggal_lahir; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_no_akta_lahir" class="col-md-2 control-label">No Registrasi Akta Lahir</label>
											<div class="col-md-10">
												<?php echo $siswa_no_akta_lahir; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_agama" class="col-md-2 control-label">Agama & Kepercayaan</label>
											<div class="col-md-4">
											<?php echo $siswa_agama; ?>
											</div>
											<label for="siswa_kewarganegaraan" class="col-md-2 control-label">Kewarganegaraan</label>
											<div class="col-md-4">
												<?php echo $siswa_kewarganegaraan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_kebutuhan_khusus" class="col-md-2 control-label">Kebutuhan Khusus</label>
											<div class="col-md-10">
												<?php echo $siswa_kebutuhan_khusus; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_alamat" class="col-md-2 control-label">Alamat</label>
											<div class="col-md-10">
											<?php echo $siswa_alamat; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_dusun" class="col-md-2 control-label">RT/RW</label>
											<div class="col-md-2">
												<?php echo $siswa_rt; ?> / <?php echo $siswa_rw; ?>
											</div>
											<label for="siswa_dusun" class="col-md-2 control-label">Dusun</label>
											<div class="col-md-4">
												<?php echo $siswa_dusun; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_deskel" class="col-md-2 control-label">Desa/Kelurahan</label>
											<div class="col-md-4">
												<?php echo $siswa_deskel; ?>
											</div>
											<label for="siswa_kecamatan" class="col-md-2 control-label">Kecamatan</label>
											<div class="col-md-4">
												<?php echo $siswa_kecamatan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_kabkota" class="col-md-2 control-label">Kota/Kabupaten</label>
											<div class="col-md-4">
												<?php echo $siswa_kabkota; ?>
											</div>
											<label for="siswa_provinsi" class="col-md-2 control-label">Provinsi</label>
											<div class="col-md-4">
												<?php echo $siswa_provinsi; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_kodepos" class="col-md-2 control-label">Kode Pos</label>
											<div class="col-md-3">
												<?php echo $siswa_kodepos; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_jenis_tinggal" class="col-md-2 control-label">Tempat Tinggal</label>
											<div class="col-md-4">
												<?php echo $siswa_jenis_tinggal; ?>
											</div>
											<label for="siswa_alat_transportasi" class="col-md-2 control-label">Moda Transportasi</label>
											<div class="col-md-4">
												<?php echo $siswa_alat_transportasi; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nomor_kks" class="col-md-2 control-label">No. KKS</label>
											<div class="col-md-4">
												<?php echo $siswa_nomor_kks; ?>
											</div>
											<label for="siswa_anak_ke" class="col-md-2 control-label">Anak ke-</label>
											<div class="col-md-4">
												<?php echo $siswa_anak_ke; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_penerima_kps" class="col-md-2 control-label">Penerima KPS/PKH</label>
											<div class="col-md-4">
												<?php echo $siswa_penerima_kps; ?>
											</div>
											<label for="siswa_no_kps" class="col-md-2 control-label">No. KPS/PKH</label>
											<div class="col-md-4">
												<?php echo $siswa_no_kps; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_layak_pip" class="col-md-2 control-label">Layak PIP</label>
											<div class="col-md-4">
												<?php echo $siswa_layak_pip; ?>
											</div>
											<label for="siswa_no_kps" class="col-md-2 control-label">Alasan Layak PIP</label>
											<div class="col-md-4">
											<?php echo $siswa_no_kps; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_penerima_kip" class="col-md-2 control-label">Penerima KIP</label>
											<div class="col-md-4">
												<?php echo $siswa_penerima_kip; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nomor_kip" class="col-md-2 control-label">No. KIP</label>
											<div class="col-md-4">
											<?php echo $siswa_nomor_kip; ?>
											</div>
											<label for="siswa_nama_kip" class="col-md-2 control-label">Nama Tertera di KIP</label>
											<div class="col-md-4">
											<?php echo $siswa_nama_kip; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Bank diperuntukan PIP</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_bank" class="col-md-2 control-label">Bank</label>
											<div class="col-md-10">
												<?php echo $siswa_bank; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nomor_rekening_bank" class="col-md-2 control-label">Nomor Rekening</label>
											<div class="col-md-4">
												<?php echo $siswa_nomor_rekening_bank; ?>
											</div>
											<label for="siswa_rekening_atas_nama" class="col-md-2 control-label">Rekening Atas Nama</label>
											<div class="col-md-4">
												<?php echo $siswa_rekening_atas_nama; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Informasi Kontak Siswa</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_telepon" class="col-md-2 control-label">Telepon</label>
											<div class="col-md-4">
												<?php echo $siswa_telepon; ?>
											</div>
											<label for="siswa_hp" class="col-md-2 control-label">HP</label>
											<div class="col-md-4">
												<?php echo $siswa_hp; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_email" class="col-md-2 control-label">Email</label>
											<div class="col-md-10">
												<?php echo $siswa_email; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Data Ayah Kandung</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_nama_ayah" class="col-md-2 control-label">Nama</label>
											<div class="col-md-4">
												<?php echo $siswa_nama_ayah; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nik_ayah" class="col-md-2 control-label">NIK</label>
											<div class="col-md-4">
												<?php echo $siswa_nik_ayah; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_tahun_ayah" class="col-md-2 control-label">Tahun Lahir</label>
											<div class="col-md-4">
												<?php echo $siswa_tahun_ayah; ?>
											</div>
											<label for="siswa_pendidikan_ayah" class="col-md-2 control-label">Pendidikan</label>
											<div class="col-md-4">
												<?php echo $siswa_pendidikan_ayah; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_pekerjaan_ayah" class="col-md-2 control-label">Pekerjaan</label>
											<div class="col-md-4">
												<?php echo $siswa_pekerjaan_ayah; ?>
											</div>
											<label for="siswa_penghasilan_ayah" class="col-md-2 control-label">Penghasilan</label>
											<div class="col-md-4">
												<?php echo $siswa_penghasilan_ayah; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Data Ibu Kandung</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_nama_ibu" class="col-md-2 control-label">Nama</label>
											<div class="col-md-4">
												<?php echo $siswa_nama_ibu; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nik_ibu" class="col-md-2 control-label">NIK</label>
											<div class="col-md-4">
												<?php echo $siswa_nik_ibu; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_tahun_ibu" class="col-md-2 control-label">Tahun Lahir</label>
											<div class="col-md-4">
												<?php echo $siswa_tahun_ibu; ?>
											</div>
											<label for="siswa_pendidikan_ibu" class="col-md-2 control-label">Pendidikan</label>
											<div class="col-md-4">
												<?php echo $siswa_pendidikan_ibu; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_pekerjaan_ibu" class="col-md-2 control-label">Pekerjaan</label>
											<div class="col-md-4">
												<?php echo $siswa_pekerjaan_ibu; ?>
											</div>
											<label for="siswa_penghasilan_ibu" class="col-md-2 control-label">Penghasilan</label>
											<div class="col-md-4">
												<?php echo $siswa_penghasilan_ibu; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Data Wali</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_nama_wali" class="col-md-2 control-label">Nama</label>
											<div class="col-md-4">
												<?php echo $siswa_nama_wali; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_nik_wali" class="col-md-2 control-label">NIK</label>
											<div class="col-md-4">
												<?php echo $siswa_nik_wali; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_tahun_wali" class="col-md-2 control-label">Tahun Lahir</label>
											<div class="col-md-4">
												<?php echo $siswa_tahun_wali; ?>
											</div>
											<label for="siswa_pendidikan_wali" class="col-md-2 control-label">Pendidikan</label>
											<div class="col-md-4">
												<?php echo $siswa_pendidikan_wali; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="siswa_pekerjaan_wali" class="col-md-2 control-label">Pekerjaan</label>
											<div class="col-md-4">
												<?php echo $siswa_pekerjaan_wali; ?>
											</div>
											<label for="siswa_penghasilan_wali" class="col-md-2 control-label">Penghasilan</label>
											<div class="col-md-4">
												<?php echo $siswa_penghasilan_wali; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Informasi Kontak Orang Tua/Wali</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_telepon_orangtua" class="col-md-2 control-label">Telepon</label>
											<div class="col-md-4">
												<?php echo $siswa_telepon_orangtua; ?>
											</div>
											<label for="siswa_hp_orangtua" class="col-md-2 control-label">HP</label>
											<div class="col-md-4">
												<?php echo $siswa_hp_orangtua; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Informasi Lainnya</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="siswa_tahun" class="col-md-2 control-label">Tahun Masuk</label>
											<div class="col-md-4">
												<?php echo $siswa_tahun; ?>
											</div>
											<label for="siswa_status" class="col-md-2 control-label">Status</label>
											<div class="col-md-4">
												<?php echo $siswa_status; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-footer">
										<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
										<!-- <button type="button" onclick="location.href='<?php echo module_url('siswa/edit/'.$siswa_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button> -->
									</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>