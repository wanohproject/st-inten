<?php
if ($action == '' || $action == 'grid'){
?>
<script>
$(function () {
<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
	<?php if ($this->session->flashdata('success')) { ?>
		$('#successModal').modal('show');
	<?php } else { ?>
		$('#errorModal').modal('show');
	<?php } ?>
<?php } ?>
});

$( document ).ready(function() {
	$("#tahun_asal_id").change(function(){
		var tahun = $(this).val();
		
		var params = {
			tahun: tahun
		};
		
	   
		$.ajax({
			url: "<?php echo module_url('kenaikan-kelas/get-tahun-tujuan'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			success:
			function(data){
				var innerHTML = "";
				if(data.response == true){
					innerHTML += "<option value=\"\"></option>";
					for(var i = 0;i < data.data.length;i++){
						innerHTML += "<option value=\"" + data.data[i].tahun_kode + "\">" + data.data[i].tahun_nama + "</option>";
					}
				} else {
					innerHTML = "<option value=\"\"></option>";
				}
				$("#kelas_tujuan_id").select2("val", "");
				$("#tingkat_tujuan_id").select2("val", "");
				$("#tahun_tujuan_id").select2("val", "");
				$("#tahun_tujuan_id").html(innerHTML);
			},
		});
	});
	
	$("#tingkat_asal_id").change(function(){
		var tingkat = $(this).val();
		
		var params = {
			tingkat: tingkat
		};
		
	   
		$.ajax({
			url: "<?php echo module_url('kenaikan-kelas/get-tingkat-tujuan'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			success:
			function(data){
				var innerHTML = "";
				if(data.response == true){
					innerHTML += "<option value=\"\"></option>";
					for(var i = 0;i < data.data.length;i++){
						innerHTML += "<option value=\"" + data.data[i].tingkat_id + "\">" + data.data[i].tingkat_nama + "</option>";
					}
				} else {
					innerHTML = "<option value=\"\"></option>";
				}
				$("#kelas_tujuan_id").select2("val", "");
				$("#tingkat_tujuan_id").select2("val", "");
				$("#tingkat_tujuan_id").html(innerHTML);
			},
		});
	});
	
	$("#kelas_asal_id").change(function(){
		var kelas_asal = $("#kelas_asal_id").val();
		var tahun_asal = $("#tahun_asal_id").val();
		var tingkat_asal = $("#tingkat_asal_id").val();
		loadSiswaAsal(tahun_asal, tingkat_asal, kelas_asal);
	});
	
	$("#kelas_tujuan_id").change(function(){
		var kelas_tujuan = $("#kelas_tujuan_id").val();
		var tahun_tujuan = $("#tahun_tujuan_id").val();
		var tingkat_tujuan = $("#tingkat_tujuan_id").val();
		loadSiswaTujuan(tahun_tujuan, tingkat_tujuan, kelas_tujuan);
	});
	
});
function loadSiswaAsal(ta, ti, ke){
	var params = {
		tahun: ta,
		tingkat: ti,
		kelas: ke
	};
   
	$.ajax({
		url: "<?php echo module_url('kenaikan-kelas/get-siswa-asal'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].siswa_nis + "</td><td>" + data.data[i].siswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_siswa\" value=\"" + data.data[i].siswa_id + "\" onclick=\"setSiswa(this.value)\" class=\"btn btn-sm btn-primary calon_siswa\"><i class=\"fa fa-angle-right\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-siswa-asal tbody").html(innerHTML);
		},
	});
}

function loadSiswaTujuan(ta, ti, ke){
	var params = {
		tahun: ta,
		tingkat: ti,
		kelas: ke
	};
   
	$.ajax({
		url: "<?php echo module_url('kenaikan-kelas/get-siswa-tujuan'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].siswa_nis + "</td><td>" + data.data[i].siswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_siswa\" value=\"" + data.data[i].siswa_id + "\" onclick=\"removeSiswa(this.value)\" class=\"btn btn-sm btn-danger calon_siswa\"><i class=\"fa fa-times\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-siswa-tujuan tbody").html(innerHTML);
		},
	});
}

function setSiswa(id){
	var tahun_asal = $("#tahun_asal_id").val();
	var tingkat_asal = $("#tingkat_asal_id").val();
	var kelas_asal = $("#kelas_asal_id").val();
	
	var tahun_tujuan = $("#tahun_tujuan_id").val();
	var tingkat_tujuan = $("#tingkat_tujuan_id").val();
	var kelas_tujuan = $("#kelas_tujuan_id").val();
	
	if (tahun_asal != "" && tingkat_asal != "" && kelas_asal != "" && tahun_tujuan != "" && tingkat_tujuan != "" && kelas_tujuan != ""){
		var params = {
			tahun_asal: tahun_asal,
			tingkat_asal: tingkat_asal,
			kelas_asal: kelas_asal,
			tahun_tujuan: tahun_tujuan,
			tingkat_tujuan: tingkat_tujuan,
			kelas_tujuan: kelas_tujuan,
			siswa: id
		};
	   
		$.ajax({
			url: "<?php echo module_url('kenaikan-kelas/set-siswa'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			success:
			function(data){
				if(data.response == true){
					loadSiswaAsal(data.params.tahun_asal_id, data.params.tingkat_asal_id, data.params.kelas_asal_id);
					loadSiswaTujuan(data.params.tahun_tujuan_id, data.params.tingkat_tujuan_id, data.params.kelas_tujuan_id);
				} else {
					alert(data.message);
				}
			},
		});
	} else {
		alert("Tahun Ajaran, Tingkat dan Kelas tidak boleh kosong.");
	}
}

function setKelas(){
	if (confirm("Apakah Anda yakin akan menaik kelaskan semua siswa?") == true) {
		var tahun_asal = $("#tahun_asal_id").val();
		var tingkat_asal = $("#tingkat_asal_id").val();
		var kelas_asal = $("#kelas_asal_id").val();
		
		var tahun_tujuan = $("#tahun_tujuan_id").val();
		var tingkat_tujuan = $("#tingkat_tujuan_id").val();
		var kelas_tujuan = $("#kelas_tujuan_id").val();
		
		if (tahun_asal != "" && tingkat_asal != "" && kelas_asal != "" && tahun_tujuan != "" && tingkat_tujuan != "" && kelas_tujuan != ""){
			var params = {
				tahun_asal: tahun_asal,
				tingkat_asal: tingkat_asal,
				kelas_asal: kelas_asal,
				tahun_tujuan: tahun_tujuan,
				tingkat_tujuan: tingkat_tujuan,
				kelas_tujuan: kelas_tujuan
			};
		
			$.ajax({
				url: "<?php echo module_url('kenaikan-kelas/set-kelas'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				success:
				function(data){
					if(data.response == true){
						loadSiswaAsal(data.params.tahun_asal_id, data.params.tingkat_asal_id, data.params.kelas_asal_id);
						loadSiswaTujuan(data.params.tahun_tujuan_id, data.params.tingkat_tujuan_id, data.params.kelas_tujuan_id);
					} else {
						alert(data.message);
					}
				},
			});
		} else {
			alert("Tahun Ajaran, Tingkat dan Kelas tidak boleh kosong.");
		}

		return true;
	} else {
		return false;
	}
}

function removeSiswa(id){
	if (confirm("Apakah yakin akan menghapus siswa dari kelas tersebut?") == true) {
		var tahun_asal = $("#tahun_asal_id").val();
		var tingkat_asal = $("#tingkat_asal_id").val();
		var kelas_asal = $("#kelas_asal_id").val();
		
		var tahun_tujuan = $("#tahun_tujuan_id").val();
		var tingkat_tujuan = $("#tingkat_tujuan_id").val();
		var kelas_tujuan = $("#kelas_tujuan_id").val();
		if (tahun_asal != "" && tingkat_asal != "" && kelas_asal != "" && tahun_tujuan != "" && tingkat_tujuan != "" && kelas_tujuan != ""){
			var params = {
				tahun_asal: tahun_asal,
				tingkat_asal: tingkat_asal,
				kelas_asal: kelas_asal,
				tahun_tujuan: tahun_tujuan,
				tingkat_tujuan: tingkat_tujuan,
				kelas_tujuan: kelas_tujuan,
				siswa: id
			};
		   
			$.ajax({
				url: "<?php echo module_url('kenaikan-kelas/remove-siswa'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				success:
				function(data){
					if(data.response == true){
						loadSiswaAsal(data.params.tahun_asal_id, data.params.tingkat_asal_id, data.params.kelas_asal_id);
						loadSiswaTujuan(data.params.tahun_tujuan_id, data.params.tingkat_tujuan_id, data.params.kelas_tujuan_id);
					} else {
						alert(data.message);
					}
				},
			});
		} else {
			alert("Tahun Ajaran, Tingkat dan Kelas tidak boleh kosong.");
		}
	}
}

function getKelasAsal(){
	var params = {
		tahun: $("#tahun_asal_id").val(),
		tingkat: $("#tingkat_asal_id").val()
	};
	
   
	$.ajax({
		url: "<?php echo module_url('kenaikan-kelas/get-kelas'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		beforeSend: function(data){
			$("#kelas_asal_id").select2("val", "");
		},
		success: function(data){
			var innerHTML = "";
			if(data.response == true){
				innerHTML += "<option value=\"\"></option>";
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<option value=\"" + data.data[i].kelas_id + "\">" + data.data[i].kelas_nama + "</option>";
				}
			} else {
				innerHTML = "<option value=\"\"></option>";
			}
			$("#kelas_asal_id").select2("val", "");
			$("#kelas_asal_id").html(innerHTML);
		},
	});
}

function getKelasTujuan(){
	var params = {
		tahun: $("#tahun_tujuan_id").val(),
		tingkat: $("#tingkat_tujuan_id").val()
	};
	
   
	$.ajax({
		url: "<?php echo module_url('kenaikan-kelas/get-kelas'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		beforeSend: function(data){
			$("#kelas_tujuan_id").select2("val", "");
		},
		success: function(data){
			var innerHTML = "";
			if(data.response == true){
				innerHTML += "<option value=\"\"></option>";
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<option value=\"" + data.data[i].kelas_id + "\">" + data.data[i].kelas_nama + "</option>";
				}
			} else {
				innerHTML = "<option value=\"\"></option>";
			}
			$("#kelas_tujuan_id").select2("val", "");
			$("#kelas_tujuan_id").html(innerHTML);
		},
	});
}
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Kenaikan Kelas
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('kenaikan-kelas'); ?>">Kenaikan Kelas</a></li>
            <li class="active">Daftar Kenaikan Kelas</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
		  	<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
				<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id);?>" method="post">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="departemen_id" class="control-label">Departemen</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_tipe IN ('Sekolah', 'Akademi') AND departemen_utama IS NULL ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
					</div>
				</form>
				</div>
              </div><!-- /.box -->
			</div><!-- /.col -->
            <div class="col-xs-6">
              <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Kelas Asal</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="tahun_asal_id" class="control-label">Tahun Ajaran</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_asal_id', 'tahun_kode', 'tahun_nama', $tahun_asal_id, 'getKelasAsal();', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="tingkat_asal_id" class="control-label">Tingkat</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM (SELECT * FROM tingkat t ORDER BY t.tingkat_kode DESC LIMIT 1, 20) tingkat WHERE departemen_id = '$departemen_id' ORDER BY tingkat_nama ASC")->result(), 'tingkat_asal_id', 'tingkat_id', 'tingkat_nama', '', 'getKelasAsal();', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="kelas_asal_id" class="control-label">Kelas</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode='$tahun_asal_id' AND tingkat_id='$tingkat_asal_id' ORDER BY kelas_nama ASC")->result(), 'kelas_asal_id', 'kelas_id', 'kelas_nama', '', '', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<div class="">
									<button type="button" name="naikkelas" onclick="setKelas()" class="btn btn-sm btn-primary calon_siswa"><i class="fa fa-angle-right"></i> Naik Kelaskan Semua Siswa</button>
								</div>		
							</div>
						</div>
					</div>
				</div>
				<div class="box-body" style="overflow-x:auto;overflow-y:auto;max-height:500px;">
					<table class="table table-bordered table-report table-siswa-asal">
						<thead>
							<tr>
								<th width="10" style="vertical-align:middle">No</th>
								<th width="100" style="vertical-align:middle">NIS</th>
								<th style="vertical-align:middle">Siswa</th>
								<th width="60"style="vertical-align:middle">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$siswa = $this->db->query("SELECT siswa_id, siswa_nis, siswa_nama FROM siswa WHERE siswa_status = 'Siswa' AND siswa_tahun='".$tahun_asal_angkatan."' AND departemen_id = '$departemen_id' AND (SELECT COUNT(siswa_id) FROM siswa_kelas WHERE siswa_kelas.siswa_id=siswa.siswa_id) < 1 ORDER BY siswa_nama ASC")->result();
						if ($siswa){
							$i = 1;
							foreach($siswa as $row){
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php echo $row->siswa_nis; ?></td>
								<td><?php echo $row->siswa_nama; ?></td>
								<td style="text-align:center;"><button type="button" name="calon_siswa" value="<?php echo $row->siswa_id; ?>" onclick="setSiswa(this.value)" class="btn btn-sm btn-primary calon_siswa"><i class="fa fa-angle-right"></i></button></td></tr>
							</tr>
							<?php
							$i++;
							}
						} else {
						?>
							<tr>
								<td colspan="4">Data tidak ada.</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
				</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
			<div class="col-xs-6">
              <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Kelas Tujuan</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="tahun_tujuan_id" class="control-label">Tahun Ajaran</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran WHERE tahun_angkatan > '".$tahun_asal_angkatan."' ORDER BY tahun_nama DESC")->result(), 'tahun_tujuan_id', 'tahun_kode', 'tahun_nama', $tahun_tujuan_id, 'getKelasTujuan();', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="tingkat_tujuan_id" class="control-label">Tingkat</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM tingkat WHERE departemen_id = '$departemen_id' ORDER BY tingkat_nama ASC LIMIT 1, 20")->result(), 'tingkat_tujuan_id', 'tingkat_id', 'tingkat_nama', '', 'getKelasTujuan();', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="kelas_tujuan_id" class="control-label">Kelas</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode='$tahun_tujuan_id' AND tingkat_id='$tingkat_tujuan_id' ORDER BY kelas_nama ASC")->result(), 'kelas_tujuan_id', 'kelas_id', 'kelas_nama', '', '', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
					</div>
				</div>
				<div class="box-body" style="overflow-x:auto;overflow-y:auto;max-height:500px;">
					<table class="table table-bordered table-report table-siswa-tujuan">
						<thead>
							<tr>
								<th width="10" style="vertical-align:middle">No</th>
								<th width="100" style="vertical-align:middle">NIS</th>
								<th style="vertical-align:middle">Siswa</th>
								<th width="60"style="vertical-align:middle">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4">Data tidak ada.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
				</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>