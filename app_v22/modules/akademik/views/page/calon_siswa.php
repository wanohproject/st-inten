<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('calon-siswa/datatable/'.$tahun_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "siswa_reg"},
			{ "data": "siswa_nama"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Calon Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('calon-siswa'); ?>">Calon Siswa</a></li>
            <li class="active">Daftar Calon Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Calon Siswa</h3>
				  <div class="pull-right">
					<a href="<?php echo base_url('asset/file/format_import_calon_siswa.xls'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-download"></span> Download Format Import</a>
					<a href="<?php echo module_url($this->uri->segment(2).'/import'); ?>" class="btn btn-primary"><span class="fa fa-download"></span> Import Calon Siswa</a>
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/index/'.$tahun_kode);?>" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="tingkat_id" class="col-md-2 control-label">Tahun Ajaran</label>
						<div class="col-md-3">
							<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
						</div>		
					</div>
				</div>
				</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width:110px;">No. Registrasi</th>
                        <th>Nama</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Calon Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('calon-siswa'); ?>">Calon Siswa</a></li>
            <li class="active">Tambah Calon Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Calon Siswa</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-body">
					<div class="box-body">
						<div class="form-group">
							<label for="siswa_reg" class="col-sm-2 control-label">No. Registrasi</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_reg" id="siswa_reg" value="<?php echo $siswa_reg; ?>" placeholder="">
							</div>
							<label for="siswa_nisn" class="col-sm-2 control-label">NISN</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_nisn" id="siswa_nisn" value="<?php echo $siswa_nisn; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="siswa_nama" id="siswa_nama" value="<?php echo $siswa_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_tempat_lahir" class="col-sm-2 control-label">Tempat, Tanggal Lahir</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="siswa_tempat_lahir" id="siswa_tempat_lahir" value="<?php echo $siswa_tempat_lahir; ?>" placeholder="">
							</div>
							<div class="col-md-3 col-sm-4">
								<input type="date" class="form-control" name="siswa_tanggal_lahir" id="siswa_tanggal_lahir" value="<?php echo $siswa_tanggal_lahir; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
							<div class="col-md-4 col-sm-6">
								<div class="radio">
									<label>
										<input type="radio" name="siswa_jenis_kelamin" value="L" <?php echo $jenis_kelamin_a = ($siswa_jenis_kelamin == 'L')?'checked':''; ?>> Laki-laki
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="siswa_jenis_kelamin" value="P" <?php echo $jenis_kelamin_h = ($siswa_jenis_kelamin == 'P')?'checked':''; ?>> Perempuan
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_agama" class="col-sm-2 control-label">Agama</label>
							<div class="col-md-3 col-sm-4">
								<?php combobox('1d', array("Islam", "Kristen Katolik", "Kristen Protestan", "Budha", "Hindu", "Konghuchu", "Lainnya"), 'siswa_agama', '', '', $siswa_agama, '', '', 'class="form-control"');?>
							</div>
							<label for="siswa_warganegara" class="col-sm-2 control-label">Warga Negara</label>
							<div class="col-md-3 col-sm-6">
								<?php combobox('2d', array("WNI"=>"Warga Negara Indonesia", "WNA"=>"Warga Negara Asing"), 'siswa_warganegara', '', '', $siswa_warganegara, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_golongan_darah" class="col-sm-2 control-label">Golongan Darah</label>
							<div class="col-md-2 col-sm-4">
								<?php combobox('1d', array("A", "B", "AB", "O"), 'siswa_golongan_darah', '', '', $siswa_golongan_darah, '', '', 'class="form-control"');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Kontak Calon Siswa</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="siswa_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_telepon" id="siswa_telepon" value="<?php echo $siswa_telepon; ?>" placeholder="">
							</div>
							<label for="siswa_hp" class="col-sm-2 control-label">HP</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_hp" id="siswa_hp" value="<?php echo $siswa_hp; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_email" id="siswa_email" value="<?php echo $siswa_email; ?>" placeholder="">
							</div>
						</div>						
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Orang Tua</h3>
					</div><!-- /.box-header -->
					<div class="box-body">						
 						<div class="form-group">
							<label for="siswa_nama_ayah" class="col-sm-2 control-label">Nama Ayah</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_nama_ayah" id="siswa_nama_ayah" value="<?php echo $siswa_nama_ayah; ?>" placeholder="">
							</div>
							<label for="siswa_pekerjaan_ayah" class="col-sm-2 control-label">Pekerjaan Ayah</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_pekerjaan_ayah" id="siswa_pekerjaan_ayah" value="<?php echo $siswa_pekerjaan_ayah; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_nama_ibu" class="col-sm-2 control-label">Nama Ibu</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_nama_ibu" id="siswa_nama_ibu" value="<?php echo $siswa_nama_ibu; ?>" placeholder="">
							</div>
							<label for="siswa_pekerjaan_ibu" class="col-sm-2 control-label">Pekerjaan Ibu</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_pekerjaan_ibu" id="siswa_pekerjaan_ibu" value="<?php echo $siswa_pekerjaan_ibu; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-5 col-sm-8">
								<textarea type="text" class="form-control" name="siswa_alamat" id="siswa_alamat" placeholder=""><?php echo $siswa_alamat; ?></textarea>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-6">
								<input type="text" class="form-control" name="siswa_rt" id="siswa_rt" value="<?php echo $siswa_rt; ?>" placeholder="RT">
							</div>
							<div class="col-md-2 col-sm-2 col-xs-6">
								<input type="text" class="form-control" name="siswa_rw" id="siswa_rw" value="<?php echo $siswa_rw; ?>" placeholder="RW">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_deskel" class="col-sm-2 control-label">Desa/Kelurahan</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_deskel" id="siswa_deskel" value="<?php echo $siswa_deskel; ?>" placeholder="">
							</div>
							<label for="siswa_kecamatan" class="col-sm-2 control-label">Kecamatan</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_kecamatan" id="siswa_kecamatan" value="<?php echo $siswa_kecamatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">			
							<label for="siswa_kabkota" class="col-sm-2 control-label">Kota/Kabupaten</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_kabkota" id="siswa_kabkota" value="<?php echo $siswa_kabkota; ?>" placeholder="">
							</div>
							<label for="siswa_provinsi" class="col-sm-2 control-label">Provinsi</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_provinsi" id="siswa_provinsi" value="<?php echo $siswa_provinsi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">			
							<label for="siswa_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-2 col-sm-3">
								<input type="text" class="form-control" name="siswa_kodepos" id="siswa_kodepos" value="<?php echo $siswa_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_telepon_orangtua" class="col-sm-2 control-label">Telepon Orang Tua</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_telepon_orangtua" id="siswa_telepon_orangtua" value="<?php echo $siswa_telepon_orangtua; ?>" placeholder="">
							</div>
							<label for="siswa_hp_orangtua" class="col-sm-2 control-label">HP Orang Tua</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_hp_orangtua" id="siswa_hp_orangtua" value="<?php echo $siswa_hp_orangtua; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="siswa_no_raport" class="col-sm-2 control-label">No. Raport</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_no_raport" id="siswa_no_raport" value="<?php echo $siswa_no_raport; ?>" placeholder="">
							</div>
							<label for="siswa_no_skhun" class="col-sm-2 control-label">No. SKHUN</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_no_skhun" id="siswa_no_skhun" value="<?php echo $siswa_no_skhun; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_tahun" class="col-sm-2 control-label">Tahun Masuk</label>
							<div class="col-md-3 col-sm-4">
								<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama")->result(), 'siswa_tahun', 'tahun_angkatan', 'tahun_nama', $siswa_tahun, '', '', 'class="form-control" required');?>
							</div>
							<label for="siswa_status" class="col-sm-2 control-label">Status</label>
							<div class="col-md-3 col-sm-4">
								<?php combobox('1d', array("Calon Siswa", "Siswa"), 'siswa_status', '', '', $siswa_status, '', '', 'class="form-control" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Calon Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('calon-siswa'); ?>">Calon Siswa</a></li>
            <li class="active">Ubah Calon Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Calon Siswa</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$siswa_id);?>" method="post">
				<input type="hidden" class="form-control" name="siswa_id" id="siswa_id" value="<?php echo $siswa_id; ?>" placeholder="">
                <div class="box-body">
					<div class="box-body">
						<div class="form-group">
							<label for="siswa_reg" class="col-sm-2 control-label">No. Registrasi</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_reg" id="siswa_reg" value="<?php echo $siswa_reg; ?>" placeholder="">
							</div>
							<label for="siswa_nisn" class="col-sm-2 control-label">NISN</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_nisn" id="siswa_nisn" value="<?php echo $siswa_nisn; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="siswa_nama" id="siswa_nama" value="<?php echo $siswa_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_tempat_lahir" class="col-sm-2 control-label">Tempat, Tanggal Lahir</label>
							<div class="col-md-4 col-sm-6">
								<input type="text" class="form-control" name="siswa_tempat_lahir" id="siswa_tempat_lahir" value="<?php echo $siswa_tempat_lahir; ?>" placeholder="">
							</div>
							<div class="col-md-3 col-sm-4">
								<input type="date" class="form-control" name="siswa_tanggal_lahir" id="siswa_tanggal_lahir" value="<?php echo $siswa_tanggal_lahir; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
							<div class="col-md-4 col-sm-6">
								<div class="radio">
									<label>
										<input type="radio" name="siswa_jenis_kelamin" value="L" <?php echo $jenis_kelamin_a = ($siswa_jenis_kelamin == 'L')?'checked':''; ?>> Laki-laki
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="siswa_jenis_kelamin" value="P" <?php echo $jenis_kelamin_h = ($siswa_jenis_kelamin == 'P')?'checked':''; ?>> Perempuan
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_agama" class="col-sm-2 control-label">Agama</label>
							<div class="col-md-3 col-sm-4">
								<?php combobox('1d', array("Islam", "Kristen Katolik", "Kristen Protestan", "Budha", "Hindu", "Konghuchu", "Lainnya"), 'siswa_agama', '', '', $siswa_agama, '', '', 'class="form-control"');?>
							</div>
							<label for="siswa_warganegara" class="col-sm-2 control-label">Warga Negara</label>
							<div class="col-md-3 col-sm-6">
								<?php combobox('2d', array("WNI"=>"Warga Negara Indonesia", "WNA"=>"Warga Negara Asing"), 'siswa_warganegara', '', '', $siswa_warganegara, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_golongan_darah" class="col-sm-2 control-label">Golongan Darah</label>
							<div class="col-md-2 col-sm-4">
								<?php combobox('1d', array("A", "B", "AB", "O"), 'siswa_golongan_darah', '', '', $siswa_golongan_darah, '', '', 'class="form-control"');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Kontak Calon Siswa</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="siswa_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_telepon" id="siswa_telepon" value="<?php echo $siswa_telepon; ?>" placeholder="">
							</div>
							<label for="siswa_hp" class="col-sm-2 control-label">HP</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_hp" id="siswa_hp" value="<?php echo $siswa_hp; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_email" id="siswa_email" value="<?php echo $siswa_email; ?>" placeholder="">
							</div>
						</div>						
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Orang Tua</h3>
					</div><!-- /.box-header -->
					<div class="box-body">						
 						<div class="form-group">
							<label for="siswa_nama_ayah" class="col-sm-2 control-label">Nama Ayah</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_nama_ayah" id="siswa_nama_ayah" value="<?php echo $siswa_nama_ayah; ?>" placeholder="">
							</div>
							<label for="siswa_pekerjaan_ayah" class="col-sm-2 control-label">Pekerjaan Ayah</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_pekerjaan_ayah" id="siswa_pekerjaan_ayah" value="<?php echo $siswa_pekerjaan_ayah; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_nama_ibu" class="col-sm-2 control-label">Nama Ibu</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_nama_ibu" id="siswa_nama_ibu" value="<?php echo $siswa_nama_ibu; ?>" placeholder="">
							</div>
							<label for="siswa_pekerjaan_ibu" class="col-sm-2 control-label">Pekerjaan Ibu</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_pekerjaan_ibu" id="siswa_pekerjaan_ibu" value="<?php echo $siswa_pekerjaan_ibu; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-5 col-sm-8">
								<textarea type="text" class="form-control" name="siswa_alamat" id="siswa_alamat" placeholder=""><?php echo $siswa_alamat; ?></textarea>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-6">
								<input type="text" class="form-control" name="siswa_rt" id="siswa_rt" value="<?php echo $siswa_rt; ?>" placeholder="RT">
							</div>
							<div class="col-md-2 col-sm-2 col-xs-6">
								<input type="text" class="form-control" name="siswa_rw" id="siswa_rw" value="<?php echo $siswa_rw; ?>" placeholder="RW">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_deskel" class="col-sm-2 control-label">Desa/Kelurahan</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_deskel" id="siswa_deskel" value="<?php echo $siswa_deskel; ?>" placeholder="">
							</div>
							<label for="siswa_kecamatan" class="col-sm-2 control-label">Kecamatan</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_kecamatan" id="siswa_kecamatan" value="<?php echo $siswa_kecamatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">			
							<label for="siswa_kabkota" class="col-sm-2 control-label">Kota/Kabupaten</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_kabkota" id="siswa_kabkota" value="<?php echo $siswa_kabkota; ?>" placeholder="">
							</div>
							<label for="siswa_provinsi" class="col-sm-2 control-label">Provinsi</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_provinsi" id="siswa_provinsi" value="<?php echo $siswa_provinsi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">			
							<label for="siswa_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-2 col-sm-3">
								<input type="text" class="form-control" name="siswa_kodepos" id="siswa_kodepos" value="<?php echo $siswa_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_telepon_orangtua" class="col-sm-2 control-label">Telepon Orang Tua</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_telepon_orangtua" id="siswa_telepon_orangtua" value="<?php echo $siswa_telepon_orangtua; ?>" placeholder="">
							</div>
							<label for="siswa_hp_orangtua" class="col-sm-2 control-label">HP Orang Tua</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_hp_orangtua" id="siswa_hp_orangtua" value="<?php echo $siswa_hp_orangtua; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="siswa_no_raport" class="col-sm-2 control-label">No. Raport</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_no_raport" id="siswa_no_raport" value="<?php echo $siswa_no_raport; ?>" placeholder="">
							</div>
							<label for="siswa_no_skhun" class="col-sm-2 control-label">No. SKHUN</label>
							<div class="col-md-3 col-sm-4">
								<input type="text" class="form-control" name="siswa_no_skhun" id="siswa_no_skhun" value="<?php echo $siswa_no_skhun; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="siswa_tahun" class="col-sm-2 control-label">Tahun Masuk</label>
							<div class="col-md-3 col-sm-4">
								<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama")->result(), 'siswa_tahun', 'tahun_angkatan', 'tahun_nama', $siswa_tahun, '', '', 'class="form-control" required');?>
							</div>
							<label for="siswa_status" class="col-sm-2 control-label">Status</label>
							<div class="col-md-3 col-sm-4">
								<?php combobox('1d', array("Calon Siswa", "Siswa"), 'siswa_status', '', '', $siswa_status, '', '', 'class="form-control" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Calon Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('calon-siswa'); ?>">Calon Siswa</a></li>
            <li class="active">Import Calon Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Calon Siswa</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
								<span class="input-group-btn">
									<input type="submit" name="importSiswa" value="Import" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetSiswa" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
								  <th>#</th>
								  <th>Tahun Ajaran</th>
								  <th>Tahun Masuk</th>
								  <th>No. Registrasi</th>
								  <th>NISN</th>
								  <th>Nama</th>
								  <th>Tempat Lahir</th>
								  <th>Tanggal Lahir</th>
								  <th>Kelamin</th>
								  <th>Agama</th>
								  <th>Warga Negara</th>
								  <th>Goldar</th>
								  <th>Telp. Calon Siswa</th>
								  <th>HP Calon Siswa</th>
								  <th>Email Calon Siswa</th>
								  <th>Nama Ayah</th>
								  <th>Pekerjaan Ayah</th>
								  <th>Nama Ibu</th>
								  <th>Pekerjaan Ibu</th>
								  <th>Alamat Orang Tua</th>
								  <th>RT</th>
								  <th>RW</th>
								  <th>Desa/Kelurahan</th>
								  <th>Kecamatan</th>
								  <th>Kab./Kota</th>
								  <th>Provinsi</th>
								  <th>Kode Pos</th>
								  <th>Telp. Ortu</th>
								  <th>HP. Ortu</th>
								  <th>No. Raport</th>
								  <th>No. Skhun</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
									?>
									<tr>
									  <th scope="row"><?php echo $i; ?></th>
									  <td><?php echo $row['siswa_tahun_ajaran']; ?></td>
									  <td><?php echo $row['siswa_tahun_masuk']; ?></td>
									  <td><?php echo $row['siswa_reg']; ?></td>
									  <td><?php echo $row['siswa_nisn']; ?></td>
									  <td><?php echo $row['siswa_nama']; ?></td>
									  <td><?php echo $row['siswa_tempat_lahir']; ?></td>
									  <td><?php echo $row['siswa_tanggal_lahir']; ?></td>
									  <td><?php echo $row['siswa_jenis_kelamin']; ?></td>
									  <td><?php echo $row['siswa_agama']; ?></td>
									  <td><?php echo $row['siswa_warganegara']; ?></td>
									  <td><?php echo $row['siswa_golongan_darah']; ?></td>
									  <td><?php echo $row['siswa_telepon']; ?></td>
									  <td><?php echo $row['siswa_hp']; ?></td>
									  <td><?php echo $row['siswa_email']; ?></td>
									  <td><?php echo $row['siswa_nama_ayah']; ?></td>
									  <td><?php echo $row['siswa_pekerjaan_ayah']; ?></td>
									  <td><?php echo $row['siswa_nama_ibu']; ?></td>
									  <td><?php echo $row['siswa_pekerjaan_ibu']; ?></td>
									  <td><?php echo $row['siswa_alamat']; ?></td>
									  <td><?php echo $row['siswa_rt']; ?></td>
									  <td><?php echo $row['siswa_rw']; ?></td>
									  <td><?php echo $row['siswa_deskel']; ?></td>
									  <td><?php echo $row['siswa_kecamatan']; ?></td>
									  <td><?php echo $row['siswa_kabkota']; ?></td>
									  <td><?php echo $row['siswa_provinsi']; ?></td>
									  <td><?php echo $row['siswa_kodepos']; ?></td>
									  <td><?php echo $row['siswa_telepon_orangtua']; ?></td>
									  <td><?php echo $row['siswa_hp_orangtua']; ?></td>
									  <td><?php echo $row['siswa_no_raport']; ?></td>
									  <td><?php echo $row['siswa_no_skhun']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Calon Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('calon-siswa'); ?>">Calon Siswa</a></li>
            <li class="active">Detail Calon Siswa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Calon Siswa</h3>
                </div><!-- /.box-header -->
				<div class="box-body">
					<div class="box-body">
						<div class="form-group row">
							<label for="siswa_reg" class="col-sm-2 control-label">No. Registrasi</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_reg; ?>
							</div>
							<label for="siswa_nisn" class="col-sm-2 control-label">NISN</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_nisn; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $siswa_nama; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_tempat_lahir" class="col-sm-2 control-label">Tempat, Tanggal Lahir</label>
							<div class="col-sm-8">
								<?php echo $siswa_tempat_lahir.', '.dateIndo($siswa_tanggal_lahir); ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
							<div class="col-md-4 col-sm-6">
								<?php echo $siswa_jenis_kelamin; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_agama" class="col-sm-2 control-label">Agama</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_agama; ?>
							</div>
							<label for="siswa_warganegara" class="col-sm-2 control-label">Warga Negara</label>
							<div class="col-md-3 col-sm-6">
								<?php echo $siswa_warganegara; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_golongan_darah" class="col-sm-2 control-label">Golongan Darah</label>
							<div class="col-md-2 col-sm-4">
								<?php echo $siswa_golongan_darah; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Kontak Siswa</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group row">
							<label for="siswa_telepon" class="col-sm-2 control-label">Telepon</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_telepon; ?>
							</div>
							<label for="siswa_hp" class="col-sm-2 control-label">HP</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_hp; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_email" class="col-sm-2 control-label">Email</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_email; ?>
							</div>
						</div>						
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Orang Tua</h3>
					</div><!-- /.box-header -->
					<div class="box-body">						
						<div class="form-group row">
							<label for="siswa_nama_ayah" class="col-sm-2 control-label">Nama Ayah</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_nama_ayah; ?>
							</div>
							<label for="siswa_pekerjaan_ayah" class="col-sm-2 control-label">Pekerjaan Ayah</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_pekerjaan_ayah; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_nama_ibu" class="col-sm-2 control-label">Nama Ibu</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_nama_ibu; ?>
							</div>
							<label for="siswa_pekerjaan_ibu" class="col-sm-2 control-label">Pekerjaan Ibu</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_pekerjaan_ibu; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-md-5 col-sm-8">
								<?php echo $siswa_alamat . ', '.$siswa_rt . '/' . $siswa_rw; ?>, 
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_deskel" class="col-sm-2 control-label">Desa/Kelurahan</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_deskel; ?>
							</div>
							<label for="siswa_kecamatan" class="col-sm-2 control-label">Kecamatan</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_kecamatan; ?>
							</div>
						</div>
						<div class="form-group row">			
							<label for="siswa_kabkota" class="col-sm-2 control-label">Kota/Kabupaten</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_kabkota; ?>
							</div>
							<label for="siswa_provinsi" class="col-sm-2 control-label">Provinsi</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_provinsi; ?>
							</div>
						</div>
						<div class="form-group row">			
							<label for="siswa_kodepos" class="col-sm-2 control-label">Kode Pos</label>
							<div class="col-md-2 col-sm-3">
								<?php echo $siswa_kodepos; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_telepon_orangtua" class="col-sm-2 control-label">Telepon Orang Tua</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_telepon_orangtua; ?>
							</div>
							<label for="siswa_hp_orangtua" class="col-sm-2 control-label">HP Orang Tua</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_hp_orangtua; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					
					<div class="box-header">
					  <h3 class="box-title">Informasi Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group row">
							<label for="siswa_no_raport" class="col-sm-2 control-label">No. Raport</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_no_raport; ?>
							</div>
							<label for="siswa_no_skhun" class="col-sm-2 control-label">No. SKHUN</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_no_skhun; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="siswa_tahun" class="col-sm-2 control-label">Tahun Masuk</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_tahun; ?>
							</div>
							<label for="siswa_status" class="col-sm-2 control-label">Status</label>
							<div class="col-md-3 col-sm-4">
								<?php echo $siswa_status; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$siswa_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>