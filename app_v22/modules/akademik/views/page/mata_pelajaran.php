<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('mata-pelajaran/datatable'); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "pelajaran_kode"},
			{ "data": "pelajaran_nama"},
			{ "data": "pelajaran_sifat"},
			{ "data": "pelajaran_kelompok"},
			{ "data": "peminatan_nama"},
			{ "data": "pelajaran_urutan"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mata Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mata-pelajaran'); ?>">Mata Pelajaran</a></li>
            <li class="active">Daftar Mata Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Mata Pelajaran</h3>
				  <div class="pull-right">
					<a href="<?php echo module_url($this->uri->segment(2).'/list_mata_pelajaran'); ?>" class="btn btn-primary"><span class="fa fa-print"></span> Cetak Daftar Pelajaran</a>
					<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
				  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Sifat</th>
                        <th>Kelompok</th>
                        <th>Peminatan</th>
                        <th>Urutan</th>
                        <th style="width:110px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mata Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mata-pelajaran'); ?>">Mata Pelajaran</a></li>
            <li class="active">Tambah Mata Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
		<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Mata Pelajaran</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="pelajaran_kode" class="col-sm-3 control-label">Kode</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="pelajaran_kode" id="pelajaran_kode" value="<?php echo $pelajaran_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_nama" class="col-sm-3 control-label">Nama</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="pelajaran_nama" id="pelajaran_nama" value="<?php echo $pelajaran_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_deskripsi" class="col-sm-3 control-label">Deskripsi</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="pelajaran_deskripsi" id="pelajaran_deskripsi" placeholder=""><?php echo $pelajaran_deskripsi; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_urutan" class="col-sm-3 control-label">Urutan</label>
							<div class="col-sm-6">
								<input type="number" class="form-control" name="pelajaran_urutan" id="pelajaran_urutan" value="<?php echo $pelajaran_urutan; ?>" placeholder="" min="0" max="100" maxlength="3" required>									
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_sifat" class="col-sm-3 control-label">Sifat</label>
							<div class="col-sm-8">
								<?php combobox('1d', array('Wajib', 'Tambahan'), 'pelajaran_sifat', '', '', $pelajaran_sifat, '', '', 'class="form-control" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_kelompok" class="col-sm-3 control-label">Kelompok</label>
							<div class="col-sm-8">
								<?php combobox('1d', array('Kelompok A (Wajib)', 'Kelompok B (Wajib)', 'Kelompok C (Peminatan)', 'Kelompok D (Unggulan)'), 'pelajaran_kelompok', '', '', $pelajaran_kelompok, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_peminatan" class="col-sm-3 control-label">Peminatan</label>
							<div class="col-sm-8">
								<?php combobox('db', $this->db->query("SELECT * FROM peminatan ORDER BY peminatan_urutan ASC")->result(), 'pelajaran_peminatan', 'peminatan_id', 'peminatan_nama', $pelajaran_peminatan, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_status" class="col-sm-3 control-label">Status</label>
							<div class="col-sm-8">
								<div class="radio">
									<label>
										<input type="radio" name="pelajaran_status" value="A" <?php echo $pelajaran_status_a = ($pelajaran_status == 'A')?'checked':''; ?> required> Aktif
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="pelajaran_status" value="H" <?php echo $pelajaran_status_h = ($pelajaran_status == 'H')?'checked':''; ?> required> Tidak Aktif
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						* Sifat Tambahan hanya digunakan untuk Informasi Raport. Untuk Mata Pelajaran silahkan gunakan Sifat Wajib.
					</div>
				  </div>
                </div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		</form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mata Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mata-pelajaran'); ?>">Mata Pelajaran</a></li>
            <li class="active">Ubah Mata Pelajaran</li>
          </ol>
        </section>

        <section class="content">
		<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$pelajaran_id);?>" method="post">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Mata Pelajaran</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				  <div class="row">
					<div class="col-md-6">
						<input type="hidden" class="form-control" name="pelajaran_id" id="pelajaran_id" value="<?php echo $pelajaran_id; ?>" placeholder="">
						<div class="form-group">
							<label for="pelajaran_kode" class="col-sm-3 control-label">Kode</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="pelajaran_kode" id="pelajaran_kode" value="<?php echo $pelajaran_kode; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_nama" class="col-sm-3 control-label">Nama</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="pelajaran_nama" id="pelajaran_nama" value="<?php echo $pelajaran_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_deskripsi" class="col-sm-3 control-label">Deskripsi</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="pelajaran_deskripsi" id="pelajaran_deskripsi" placeholder=""><?php echo $pelajaran_deskripsi; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_urutan" class="col-sm-3 control-label">Urutan</label>
							<div class="col-sm-6">
								<input type="number" class="form-control" name="pelajaran_urutan" id="pelajaran_urutan" value="<?php echo $pelajaran_urutan; ?>" placeholder="" min="0" max="100" maxlength="3" required>									
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_sifat" class="col-sm-3 control-label">Sifat</label>
							<div class="col-sm-8">
								<?php combobox('1d', array('Wajib', 'Tambahan'), 'pelajaran_sifat', '', '', $pelajaran_sifat, '', '', 'class="form-control" required');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_kelompok" class="col-sm-3 control-label">Kelompok</label>
							<div class="col-sm-8">
								<?php combobox('1d', array('Kelompok A (Wajib)', 'Kelompok B (Wajib)', 'Kelompok C (Peminatan)', 'Kelompok D (Unggulan)'), 'pelajaran_kelompok', '', '', $pelajaran_kelompok, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_peminatan" class="col-sm-3 control-label">Peminatan</label>
							<div class="col-sm-8">
								<?php combobox('db', $this->db->query("SELECT * FROM peminatan ORDER BY peminatan_urutan ASC")->result(), 'pelajaran_peminatan', 'peminatan_id', 'peminatan_nama', $pelajaran_peminatan, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="pelajaran_status" class="col-sm-3 control-label">Status</label>
							<div class="col-sm-8">
								<div class="radio">
									<label>
										<input type="radio" name="pelajaran_status" value="A" <?php echo $pelajaran_status_a = ($pelajaran_status == 'A')?'checked':''; ?> required> Aktif
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="pelajaran_status" value="H" <?php echo $pelajaran_status_h = ($pelajaran_status == 'H')?'checked':''; ?> required> Tidak Aktif
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						* Sifat Tambahan hanya digunakan untuk Informasi Raport. Untuk Mata Pelajaran silahkan gunakan Sifat Wajib.
					</div>
				  </div>
                </div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		</form>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mata Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('mata-pelajaran'); ?>">Mata Pelajaran</a></li>
            <li class="active">Detail Mata Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Mata Pelajaran</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<input type="hidden" class="form-control" name="pelajaran_id" id="pelajaran_id" value="<?php echo $pelajaran_id; ?>" placeholder="">
					<div class="box-body">
						<div class="form-group row">
							<label for="pelajaran_kode" class="col-sm-3 control-label">Kode</label>
							<div class="col-sm-8">
								<?php echo $pelajaran_kode; ?>
							</div>
						</div>
						<div class="form-group row">
							<label for="pelajaran_nama" class="col-sm-3 control-label">Nama</label>
							<div class="col-sm-8">
								<?php echo $pelajaran_nama; ?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$pelajaran_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>