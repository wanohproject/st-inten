<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url($this->uri->segment(2).'/datatable/'.$program_studi_id.'/'.$semester_kode); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "bobot_nilai_huruf"},
			{ "data": "bobot_nilai_value"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bobot Nilai
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Bobot Nilai</a></li>
            <li class="active">Daftar Bobot Nilai</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Bobot Nilai</h3>
									<div class="pull-right">
									<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
									</div>
                </div><!-- /.box-header -->
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$semester_kode);?>" method="post">
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="program_studi_id" class="control-label">Program Studi</label>
												<div class="">
													<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, 'submit();', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="semester_kode" class="control-label">Semester Pelaporan</label>
												<div class="">	
													<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', '-- PILIH SEMESTER PELAPORAN --', 'class="form-control" required');?>
												</div>		
											</div>
										</div>
									</div>
								</div>
								</form>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Huruf</th>
                        <th>Angka</th>
                        <th style="width:130px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<script>
$(function () {
	$("#bobot_nilai_huruf, #semester_kode").change(function(){
		if ($("#program_studi_id").val() != "" && $("#semester_kode").val() != "" && $("#bobot_nilai_huruf").val() != ""){
			var params = {
				action: 'add',
				semester: $("#semester_kode").val(),
				program_studi: $("#program_studi_id").val(),
				kode: $("#bobot_nilai_huruf").val()
			};
			// console.log(params);
			
			$.ajax({
				url: "<?php echo module_url($this->uri->segment(2).'/check-kode'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				beforeSend: function() {},
				success: function(data){
					if (data.response == false){
						$("#bobot_nilai_huruf").css("border-color", "#a94442");
						$("#form-add").attr('onsubmit', 'return false;');
					} else {
						$("#bobot_nilai_huruf").css("border-color", "#5cb85c");
						$("#form-add").attr('onsubmit', 'return true;');
					}
				},
				complete: function() {},
			})
		}
	});
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bobot Nilai
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Bobot Nilai</a></li>
            <li class="active">Tambah Bobot Nilai</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Bobot Nilai</h3>
                </div><!-- /.box-header -->
								<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
								<div class="box-body">
									<div class="form-group">
										<label for="semester_kode" class="col-md-2 control-label" style="text-align:left">Semester Pelaporan</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="bobot_nilai_huruf" class="col-md-2 control-label" style="text-align:left">Huruf</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="bobot_nilai_huruf" id="bobot_nilai_huruf" value="<?php echo $bobot_nilai_huruf; ?>" placeholder="" required>
										</div>
									</div>
									<div class="form-group">
										<label for="bobot_nilai_value" class="col-md-2 control-label" style="text-align:left">Angka</label>
										<div class="col-md-4">
											<input type="number" step="any" class="form-control" name="bobot_nilai_value" id="bobot_nilai_value" value="<?php echo $bobot_nilai_value; ?>" placeholder="" required>
										</div>
									</div>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
								</div><!-- /.box-footer -->
								</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<script>
$(function () {
	$("#bobot_nilai_huruf, #semester_kode").change(function(){
		if ($("#program_studi_id").val() != "" && $("#semester_kode").val() != "" && $("#bobot_nilai_huruf").val() != ""){
			var params = {
				action: 'edit',
				id: '<?php echo $bobot_nilai_id; ?>',
				semester: $("#semester_kode").val(),
				program_studi: $("#program_studi_id").val(),
				kode: $("#bobot_nilai_huruf").val()
			};
			// console.log(params);
			
			$.ajax({
				url: "<?php echo module_url($this->uri->segment(2).'/check-kode'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				beforeSend: function() {},
				success: function(data){
					if (data.response == false){
						$("#bobot_nilai_huruf").css("border-color", "#a94442");
						$("#form-add").attr('onsubmit', 'return false;');
					} else {
						$("#bobot_nilai_huruf").css("border-color", "#5cb85c");
						$("#form-add").attr('onsubmit', 'return true;');
					}
				},
				complete: function() {},
			});
		}
	});
});
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bobot Nilai
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Bobot Nilai</a></li>
            <li class="active">Ubah Bobot Nilai</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Bobot Nilai</h3>
                </div><!-- /.box-header -->
								<form id="form-add" class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$bobot_nilai_id);?>" method="post">
								<input type="hidden" class="form-control" name="bobot_nilai_id" id="bobot_nilai_id" value="<?php echo $bobot_nilai_id; ?>" placeholder="">
								<div class="box-body">
									<div class="form-group">
										<label for="semester_kode" class="col-md-2 control-label" style="text-align:left">Semester Pelaporan</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" required');?>
										</div>
									</div>
									<div class="form-group">
										<label for="bobot_nilai_huruf" class="col-md-2 control-label" style="text-align:left">Huruf</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="bobot_nilai_huruf" id="bobot_nilai_huruf" value="<?php echo $bobot_nilai_huruf; ?>" placeholder="" required>
										</div>
									</div>
									<div class="form-group">
										<label for="bobot_nilai_value" class="col-md-2 control-label" style="text-align:left">Angka</label>
										<div class="col-md-4">
											<input type="number" step="any" class="form-control" name="bobot_nilai_value" id="bobot_nilai_value" value="<?php echo $bobot_nilai_value; ?>" placeholder="" required>
										</div>
									</div>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$semester_kode); ?>'" class="btn btn-default">Batalkan</button>
								</div><!-- /.box-footer -->
								</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bobot Nilai
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Bobot Nilai</a></li>
            <li class="active">Detail Bobot Nilai</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
					<div class="row">
            <div class="col-xs-12">
						<div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Bobot Nilai</h3>
                </div><!-- /.box-header -->
								<div class="box-body form-horizontal">
									<div class="form-group">
										<label for="semester_kode" class="col-md-2 control-label" style="text-align:left">Semester Pelaporan</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, '', '', 'class="form-control" disabled');?>
										</div>
									</div>
									<div class="form-group">
										<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
										<div class="col-md-4">
											<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" disabled');?>
										</div>
									</div>
									<div class="form-group">
										<label for="bobot_nilai_huruf" class="col-md-2 control-label" style="text-align:left">Huruf</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="bobot_nilai_huruf" id="bobot_nilai_huruf" value="<?php echo $bobot_nilai_huruf; ?>" placeholder="" readonly>
										</div>
									</div>
									<div class="form-group">
										<label for="bobot_nilai_value" class="col-md-2 control-label" style="text-align:left">Angka</label>
										<div class="col-md-4">
											<input type="number" step="any" class="form-control" name="bobot_nilai_value" id="bobot_nilai_value" value="<?php echo $bobot_nilai_value; ?>" placeholder="" readonly>
										</div>
									</div>
								</div><!-- /.box-body -->
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$program_studi_id.'/'.$semester_kode); ?>'" class="btn btn-default">Kembali</button>
									<?php if (check_permission("W")){?>
									<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$bobot_nilai_id); ?>'" class="btn btn-primary" name="save" value="save">Ubah Bobot Nilai</button>
									<?php } ?>
								</div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>