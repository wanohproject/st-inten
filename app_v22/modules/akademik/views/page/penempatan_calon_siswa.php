<?php
if ($action == '' || $action == 'grid'){
?>
<script>
$(function () {
<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
	<?php if ($this->session->flashdata('success')) { ?>
		$('#successModal').modal('show');
	<?php } else { ?>
		$('#errorModal').modal('show');
	<?php } ?>
<?php } ?>
});

$( document ).ready(function() {
	$("#tahun_kode").change(function(){
		var tahun = $(this).val();
		loadCalonSiswa(tahun);
	});
	
	$("#kelas_id").change(function(){
		var kelas = $(this).val();
		var tahun = $("#tahun_tujuan_id").val();
		var tingkat = $("#tingkat_id").val();
		loadSiswaBaru(tahun, tingkat, kelas);
	});
	
	$("#cari_siswa_nama").keyup(function(){
		var tahun = $("#tahun_tujuan_id").val();
		var filter = $("#cari_siswa_nama").val();
		loadCalonSiswaFilter(tahun, filter);
	});
});
function loadCalonSiswa(t){
	var params = {
		tahun: t
	};
   
	$.ajax({
		url: "<?php echo module_url('penempatan-calon-siswa/get-calon-siswa'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].siswa_reg + "</td><td>" + data.data[i].siswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_siswa\" value=\"" + data.data[i].siswa_id + "\" onclick=\"setSiswa(this.value)\" class=\"btn btn-sm btn-primary calon_siswa\"><i class=\"fa fa-angle-right\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-calon-siswa tbody").html(innerHTML);
		},
	});
}

function loadCalonSiswaFilter(t, f){
	var params = {
		tahun: t,
		filter: f
	};
   
	$.ajax({
		url: "<?php echo module_url('penempatan-calon-siswa/get-calon-siswa-filter'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].siswa_reg + "</td><td>" + data.data[i].siswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_siswa\" value=\"" + data.data[i].siswa_id + "\" onclick=\"setSiswa(this.value)\" class=\"btn btn-sm btn-primary calon_siswa\"><i class=\"fa fa-angle-right\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-calon-siswa tbody").html(innerHTML);
		},
	});
}

function loadSiswaBaru(ta, ti, ke){
	var params = {
		tahun: ta,
		tingkat: ti,
		kelas: ke
	};
   
	$.ajax({
		url: "<?php echo module_url('penempatan-calon-siswa/get-siswa-baru'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].siswa_reg + "</td><td>" + data.data[i].siswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_siswa\" value=\"" + data.data[i].siswa_id + "\" onclick=\"removeSiswa(this.value)\" class=\"btn btn-sm btn-danger calon_siswa\"><i class=\"fa fa-times\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-siswa-baru tbody").html(innerHTML);
		},
	});
}

function setSiswa(id){
	var tingkat = $("#tingkat_id").val();
	var kelas = $("#kelas_id").val();
	var tahun_tujuan = $("#tahun_tujuan_id").val();
	if (kelas != "" && tingkat != "" && tahun_tujuan != ""){
		var params = {
			tahun: tahun_tujuan,
			tingkat: tingkat,
			kelas: kelas,
			siswa: id,
		};
	   
		$.ajax({
			url: "<?php echo module_url('penempatan-calon-siswa/set-siswa'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			success:
			function(data){
				if(data.response == true){
					loadCalonSiswa(data.params.tahun_kode);
					loadSiswaBaru(data.params.tahun_kode, data.params.tingkat_id, data.params.kelas_id);
				} else {
					alert(data.message);
				}
			},
		});
	} else {
		alert("Tahun Ajaran, Tingkat dan Kelas tidak boleh kosong.");
	}
}

function removeSiswa(id){
	if (confirm("Apakah yakin akan menghapus siswa dari kelas tersebut?") == true) {
		var tingkat = $("#tingkat_id").val();
		var kelas = $("#kelas_id").val();
		var tahun_tujuan = $("#tahun_tujuan_id").val();
		if (kelas != "" && tingkat != "" && tahun_tujuan != ""){
			var params = {
				tahun: tahun_tujuan,
				tingkat: tingkat,
				kelas: kelas,
				siswa: id,
			};
		   
			$.ajax({
				url: "<?php echo module_url('penempatan-calon-siswa/remove-siswa'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				success:
				function(data){
					if(data.response == true){
						loadCalonSiswa(data.params.tahun_kode);
						loadSiswaBaru(data.params.tahun_kode, data.params.tingkat_id, data.params.kelas_id);
					} else {
						alert(data.message);
					}
				},
			});
		} else {
			alert("Tahun Ajaran, Tingkat dan Kelas tidak boleh kosong.");
		}
	}
}

function getKelas(){
	var params = {
		tahun: $("#tahun_tujuan_id").val(),
		tingkat: $("#tingkat_id").val()
	};
	
   
	$.ajax({
		url: "<?php echo module_url('penempatan-calon-siswa/get-kelas'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				innerHTML += "<option value=\"\"></option>";
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<option value=\"" + data.data[i].kelas_id + "\">" + data.data[i].kelas_nama + "</option>";
				}
			} else {
				innerHTML = "<option value=\"\"></option>";
			}
			$("#kelas_id").html(innerHTML);
		},
	});
}
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Penempatan Siswa Baru
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('penempatan-calon-siswa'); ?>">Penempatan Siswa Baru</a></li>
            <li class="active">Daftar Penempatan Siswa Baru</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-6">
              <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Siswa Baru</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="tahun_kode" class="control-label">Tahun Angkatan</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_angkatan', $tahun_kode, '', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="cari_siswa_nama" class="control-label">Cari Nama/No. Registrasi</label>
								<div class="">
									<input type="text" id="cari_siswa_nama" name="cari_siswa_nama" class="form-control" placeholder="Nama atau No. Registrasi" />
								</div>		
							</div>
						</div>
					</div>
				</div>
				<div class="box-body" style="overflow-x:auto;">
					<table class="table table-bordered table-report table-calon-siswa">
						<thead>
							<tr>
								<th width="10" style="vertical-align:middle">No</th>
								<th width="100" style="vertical-align:middle">No. Registrasi</th>
								<th style="vertical-align:middle">Siswa</th>
								<th width="60"style="vertical-align:middle">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$siswa = $this->db->query("SELECT siswa_id, siswa_reg, siswa_nama FROM calon_siswa WHERE siswa_status = 'Calon Siswa' AND siswa_tahun='".$tahun_angkatan."' ORDER BY siswa_nama ASC")->result();
						if ($siswa){
							$i = 1;
							foreach($siswa as $row){
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php echo $row->siswa_reg; ?></td>
								<td><?php echo $row->siswa_nama; ?></td>
								<td style="text-align:center;"><button type="button" name="calon_siswa" value="<?php echo $row->siswa_id; ?>" onclick="setSiswa(this.value)" class="btn btn-sm btn-primary calon_siswa"><i class="fa fa-angle-right"></i></button></td></tr>
							</tr>
							<?php
							$i++;
							}
						} else {
						?>
							<tr>
								<td colspan="4">Data tidak ada.</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
				</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
			<div class="col-xs-6">
              <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Kelas Tujuan</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="tahun_tujuan_id" class="control-label">Tahun Ajaran</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_tujuan_id', 'tahun_kode', 'tahun_nama', $tahun_kode, 'getKelas();', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="tingkat_id" class="control-label">Tingkat</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM tingkat ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', '', 'getKelas();', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="kelas_id" class="control-label">Kelas</label>
								<div class="">
									<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode='$tahun_kode' AND tingkat_id='$tingkat_id' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', '', '', '', 'class="form-control select2" required');?>
								</div>		
							</div>
						</div>
					</div>
				</div>
				<div class="box-body" style="overflow-x:auto;">
					<table class="table table-bordered table-report table-siswa-baru">
						<thead>
							<tr>
								<th width="10" style="vertical-align:middle">No</th>
								<th width="100" style="vertical-align:middle">No. Registrasi</th>
								<th style="vertical-align:middle">Siswa</th>
								<th width="60"style="vertical-align:middle">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4">Data tidak ada.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
				</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>