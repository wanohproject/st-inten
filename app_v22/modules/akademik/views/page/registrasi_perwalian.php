<?php
if ($action == '' || $action == 'grid'){
?>
<script>
$(function () {
<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
	<?php if ($this->session->flashdata('success')) { ?>
		$('#successModal').modal('show');
	<?php } else { ?>
		$('#errorModal').modal('show');
	<?php } ?>
<?php } ?>
});

$( document ).ready(function() {
	$("#program_studi_id").change(function(){
		var program_studi_id = $("#program_studi_id").val();
		var tahun_kode = $("#tahun_kode").val();
		var semester_kode = $("#semester_kode").val();
		loadMahasiswa(program_studi_id, tahun_kode, semester_kode);
		loadRegister(program_studi_id, tahun_kode, semester_kode);
	});
	
	$("#tahun_kode").change(function(){
		var program_studi_id = $("#program_studi_id").val();
		var tahun_kode = $("#tahun_kode").val();
		var semester_kode = $("#semester_kode").val();
		loadMahasiswa(program_studi_id, tahun_kode, semester_kode);
		loadRegister(program_studi_id, tahun_kode, semester_kode);
	});
	
	$("#semester_kode").change(function(){
		var program_studi_id = $("#program_studi_id").val();
		var tahun_kode = $("#tahun_kode").val();
		var semester_kode = $("#semester_kode").val();
		loadMahasiswa(program_studi_id, tahun_kode, semester_kode);
		loadRegister(program_studi_id, tahun_kode, semester_kode);
	});
	
	$("#cari_mahasiswa_nama").keyup(function(){
		var program_studi_id = $("#program_studi_id").val();
		var tahun_kode = $("#tahun_kode").val();
		var semester_kode = $("#semester_kode").val();
		var filter = $("#cari_mahasiswa_nama").val();
		loadMahasiswaFilter(program_studi_id, tahun_kode, semester_kode, filter);
	});
});

function loadMahasiswa(program_studi_id, tahun_kode, semester_kode){
	var params = {
		tahun: tahun_kode,
		semester: semester_kode,
		program_studi: program_studi_id
	};
   
	$.ajax({
		url: "<?php echo module_url($this->uri->segment(2).'/get-mahasiswa'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].mahasiswa_nim + "</td><td>" + data.data[i].mahasiswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_mahasiswa\" value=\"" + data.data[i].mahasiswa_id + "\" onclick=\"setMahasiswa(this.value)\" class=\"btn btn-sm btn-primary calon_mahasiswa\"><i class=\"fa fa-angle-right\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-mahasiswa tbody").html(innerHTML);
		},
	});
}

function loadMahasiswaFilter(program_studi_id, tahun_kode, semester_kode, filter){
	var params = {
		tahun: tahun_kode,
		semester: semester_kode,
		program_studi: program_studi_id,
		filter: filter
	};
   
	$.ajax({
		url: "<?php echo module_url($this->uri->segment(2).'/get-mahasiswa-filter'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].mahasiswa_nim + "</td><td>" + data.data[i].mahasiswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_mahasiswa\" value=\"" + data.data[i].mahasiswa_id + "\" onclick=\"setMahasiswa(this.value)\" class=\"btn btn-sm btn-primary calon_mahasiswa\"><i class=\"fa fa-angle-right\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-mahasiswa tbody").html(innerHTML);
		},
	});
}

function loadRegister(program_studi_id, tahun_kode, semester_kode){
	var params = {
		tahun: tahun_kode,
		semester: semester_kode,
		program_studi: program_studi_id
	};
	
	$.ajax({
		url: "<?php echo module_url($this->uri->segment(2).'/get-registrasi'); ?>",
		dataType: 'json',
		type: 'POST',
		data: params,
		success:
		function(data){
			var innerHTML = "";
			if(data.response == true){
				for(var i = 0;i < data.data.length;i++){
					innerHTML += "<tr><td>" + (i + 1) + "</td><td>" + data.data[i].mahasiswa_nim + "</td><td>" + data.data[i].mahasiswa_nama + "</td><td style=\"text-align:center;\"><button type=\"button\" name=\"calon_mahasiswa\" value=\"" + data.data[i].mahasiswa_id + "\" onclick=\"removeMahasiswa(this.value)\" class=\"btn btn-sm btn-danger calon_mahasiswa\"><i class=\"fa fa-times\"></i></button></td></tr>";
				}
			} else {
				innerHTML = "<tr><td colspan=\"4\">Data tidak ada.</td></tr>";
			}
			$(".table-registrasi tbody").html(innerHTML);
		},
	});
}

function setMahasiswa(id){
	var program_studi_id = $("#program_studi_id").val();
	var tahun_kode = $("#tahun_kode").val();
	var semester_kode = $("#semester_kode").val();
	if (program_studi_id != "" && semester_kode != ""){
		var params = {
			tahun: tahun_kode,
			program_studi: program_studi_id,
			semester: semester_kode,
			mahasiswa: id
		};
	   
		$.ajax({
			url: "<?php echo module_url($this->uri->segment(2).'/set-mahasiswa'); ?>",
			dataType: 'json',
			type: 'POST',
			data: params,
			success:
			function(data){
				if(data.response == true){
					loadMahasiswa(data.params.program_studi_id, data.params.tahun_kode, data.params.semester_kode);
					loadRegister(data.params.program_studi_id, data.params.tahun_kode, data.params.semester_kode);
				} else {
					alert(data.message);
				}
			},
		});
	} else {
		alert("Program Studi dan Semester tidak boleh kosong.");
	}
}

function removeMahasiswa(id){
	if (confirm("Apakah yakin akan menghapus mahasiswa dari kelas tersebut?") == true) {
		var program_studi_id = $("#program_studi_id").val();
		var tahun_kode = $("#tahun_kode").val();
		var semester_kode = $("#semester_kode").val();
		if (program_studi_id != "" && semester_kode != ""){
			var params = {
				tahun: tahun_kode,
				program_studi: program_studi_id,
				semester: semester_kode,
				mahasiswa: id
			};
		   
			$.ajax({
				url: "<?php echo module_url($this->uri->segment(2).'/remove-mahasiswa'); ?>",
				dataType: 'json',
				type: 'POST',
				data: params,
				success:
				function(data){
					if(data.response == true){
						loadMahasiswa(data.params.program_studi_id, data.params.tahun_kode, data.params.semester_kode);
						loadRegister(data.params.program_studi_id, data.params.tahun_kode, data.params.semester_kode);
					} else {
						alert(data.message);
					}
				},
			});
		} else {
			alert("Program Studi dan Semester tidak boleh kosong.");
		}
	}
}
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Registrasi Perwalian
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url($this->uri->segment(2)); ?>">Registrasi Perwalian</a></li>
            <li class="active">Daftar Registrasi Perwalian</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-body">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="program_studi_id" class="control-label">Program Studi</label>
												<div class="">
													<?php combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '-- PILIH PROGRAM STUDI --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tahun Masuk</label>
												<div class="">	
													<?php combobox('db', $this->Tahun_model->grid_all_tahun('', 'tahun_kode', 'DESC'), 'tahun_kode', 'tahun_kode', 'tahun_kode', $tahun_kode, '', '-- PILIH TAHUN --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="semester_kode" class="control-label">Semester</label>
												<div class="">
													<?php combobox('db', $this->Semester_model->grid_all_semester('', 'semester_kode', 'DESC'), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, '', '-- PILIH SEMESTER --', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
									</div>
								</div>
              </div><!-- /.box -->
						</div><!-- /.col -->
						<div class="col-xs-6">
              <div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Daftar Mahasiswa</h3>
									<div class="pull-right"><input type="text" class="form-control" name="cari_mahasiswa_nama" id="cari_mahasiswa_nama" placeholder="Nama atau NIS"></div>
								</div><!-- /.box-header -->
								<div class="box-body" style="overflow-x:auto;overflow-y:auto;max-height:500px;">
									<table class="table table-bordered table-report table-mahasiswa">
										<thead>
											<tr>
												<th width="10" style="vertical-align:middle">No</th>
												<th width="100" style="vertical-align:middle">NIM</th>
												<th style="vertical-align:middle">Mahasiswa</th>
												<th width="60"style="vertical-align:middle">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="4">Data tidak ada.</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
								</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
						<div class="col-xs-6">
              <div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Mahasiswa Teregistrasi</h3>
								</div><!-- /.box-header -->
								<div class="box-body" style="overflow-x:auto;overflow-y:auto;max-height:500px;">
									<table class="table table-bordered table-report table-registrasi">
										<thead>
											<tr>
												<th width="10" style="vertical-align:middle">No</th>
												<th width="100" style="vertical-align:middle">NIM</th>
												<th style="vertical-align:middle">Mahasiswa</th>
												<th width="60"style="vertical-align:middle">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="4">Data tidak ada.</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
								</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>