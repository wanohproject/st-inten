<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
	$("#datagrid").DataTable({
		"processing": true,
        "serverSide": true,
		"ajax": {
			"url" : "<?php echo module_url('staf/datatable/'.$departemen_id); ?>",
			"type" : "POST",
		},
		"columns": [
			{ "data": "departemen_kode"},
			{ "data": "staf_nama"},
			{ "data": "staf_user"},
			{ "data": "staf_pin"},
			{ "data": "staf_user_aktif"},
			{ "data": "Actions"},
		],
		"language": {
			"emptyTable": "Tidak ada data pada tabel ini",
			"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
			"infoEmpty": "Tidak ada data yang sesuai",
			"infoFiltered": "(hasil pencarian dari _MAX_ data)",
			"lengthMenu": "Tampil _MENU_  baris",
			"search": "Cari: ",
			"zeroRecords": "Tidak ada baris yang sesuai"
		},
		"sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
		"lengthMenu": [
			[10, 20, 30, -1],
			[10, 20, 30, "All"] // change per page values here
		],
		"order": [
			[0, 'asc'],
			[1, 'asc']
		],
		"pageLength": 10,
		"columnDefs": [{
			'orderable': false,
			'targets': [-1]
		}, {
			"searchable": false,
			"targets": [-1]
		}]
	});
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Daftar Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Staf</h3>
									<div class="pull-right">
										<a href="<?php echo module_url($this->uri->segment(2).'/upload-foto'); ?>" class="btn btn-primary"><span class="fa fa-upload"></span> Upload Foto</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/pin_staf'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-print"></span> Cetak Pin</a>
										<a href="<?php echo base_url('asset/file/format_import_staf.xls'); ?>" class="btn btn-primary" target="_blank"><span class="fa fa-download"></span> Download Format Import</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/import'); ?>" class="btn btn-primary"><span class="fa fa-download"></span> Import</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/generate-account'); ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin? \nAkan men-generate user guru.');"><span class="fa fa-cog"></span> Generate User Staf</a>
										<a href="<?php echo module_url($this->uri->segment(2).'/add'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
									</div>
                </div><!-- /.box-header -->
								<?php if (userdata('departemen_id') == ""){?>
								<div class="box-body">
									<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$departemen_id);?>" method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="departemen_id" class="control-label">Departemen</label>
													<div class="">
														<?php combobox('db', $this->db->query("SELECT * FROM departemen WHERE departemen_utama IS NULL ORDER BY departemen_urutan ASC")->result(), 'departemen_id', 'departemen_id', 'departemen_nama', $departemen_id, 'submit();', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
													</div>		
												</div>
											</div>
										</div>
									</form>
								</div>
								<?php } ?>
                <div class="box-body">
                  <table id="datagrid" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Departemen</th>
                        <th>Nama</th>
                        <th>User</th>
                        <th>Pin</th>
                        <th>User Aktif</th>
                        <th style="width:150px;">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Tambah Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Tambah Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post">
                <div class="box-body">
								
					<div class="box-header">
					  <h3 class="box-title">Informasi Akun</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_user" class="col-md-2 control-label">User</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_user" id="staf_user" value="<?php echo $staf_user; ?>" placeholder="">
							</div>
							<label for="staf_pin" class="col-md-2 control-label">PIN</label>
							<div class="col-md-4">
								<input type="number" maxlength="6" class="form-control" name="staf_pin" id="staf_pin" value="<?php echo $staf_pin; ?>" placeholder="">
								<small>* Hapus 6 Karakter.</small>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								* Apabila <strong> User</strong> tidak diisi maka user guru tidak akan dibuat. <br />
								* Apabila <strong>PIN</strong> tidak diisi maka ping guru akan dibuat otomatis 6 Karakter Acak. <br />
							</div>
						</div>
					</div>
					<div class="box-header">
					  <h3 class="box-title">Informasi Identitas</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_nama" class="col-md-2 control-label">Nama</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama" id="staf_nama" value="<?php echo $staf_nama; ?>" placeholder="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nik" class="col-md-2 control-label">NIK</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nik" id="staf_nik" value="<?php echo $staf_nik; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_jenis_kelamin" class="col-md-2 control-label">Jenis Kelamin</label>
							<div class="col-md-10">
								<div class="radio">
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="L" <?php echo $jenis_kelamin_a = ($staf_jenis_kelamin == 'L')?'checked':''; ?>> L
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_jenis_kelamin" value="P" <?php echo $jenis_kelamin_h = ($staf_jenis_kelamin == 'P')?'checked':''; ?>> P
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_tempat_lahir" class="col-md-2 control-label">Tempat, Tanggal Lahir</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="staf_tempat_lahir" id="staf_tempat_lahir" value="<?php echo $staf_tempat_lahir; ?>" placeholder="">
							</div>
							<div class="col-md-5">
								<input type="date" class="form-control" name="staf_tanggal_lahir" id="staf_tanggal_lahir" value="<?php echo $staf_tanggal_lahir; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nama_ibu_kandung" class="col-md-2 control-label">Nama Ibu Kandung</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_nama_ibu_kandung" id="staf_nama_ibu_kandung" value="<?php echo $staf_nama_ibu_kandung; ?>" placeholder="">
							</div>
						</div>
					</div>
					<div class="box-header">
					  <h3 class="box-title">Informasi Data Pribadi</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_alamat" class="col-md-2 control-label">Alamat</label>
							<div class="col-md-10">
								<textarea type="text" class="form-control" name="staf_alamat" id="staf_alamat" placeholder=""><?php echo $staf_alamat; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_dusun" class="col-md-2 control-label">RT/RW</label>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rt" id="staf_rt" value="<?php echo $staf_rt; ?>" placeholder="RT">
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control" name="staf_rw" id="staf_rw" value="<?php echo $staf_rw; ?>" placeholder="RW">
							</div>
							<label for="staf_dusun" class="col-md-2 control-label">Dusun</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_dusun" id="staf_dusun" value="<?php echo $staf_dusun; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_deskel" class="col-md-2 control-label">Desa/Kelurahan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_deskel" id="staf_deskel" value="<?php echo $staf_deskel; ?>" placeholder="">
							</div>
							<label for="staf_kecamatan" class="col-md-2 control-label">Kecamatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kecamatan" id="staf_kecamatan" value="<?php echo $staf_kecamatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_kabkota" class="col-md-2 control-label">Kota/Kabupaten</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kabkota" id="staf_kabkota" value="<?php echo $staf_kabkota; ?>" placeholder="">
							</div>
							<label for="staf_provinsi" class="col-md-2 control-label">Provinsi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_provinsi" id="staf_provinsi" value="<?php echo $staf_provinsi; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_kodepos" class="col-md-2 control-label">Kode Pos</label>
							<div class="col-md-3">
								<input type="number" class="form-control" name="staf_kodepos" id="staf_kodepos" value="<?php echo $staf_kodepos; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_agama" class="col-md-2 control-label">Agama & Kepercayaan</label>
							<div class="col-md-4">
								<?php combobox('1d', array("Islam", "Kristen", "Katholik", "Hindu", "Budha", "Kong Hu Chu", "Kepercayaan kpd Tuhan YME", "Lainnya"), 'staf_agama', '', '', $staf_agama, '', '', 'class="form-control"');?>
							</div>
							<label for="staf_kewarganegaraan" class="col-md-2 control-label">Kewarganegaraan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kewarganegaraan" id="staf_kewarganegaraan" value="<?php echo $staf_kewarganegaraan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_npwp" class="col-md-2 control-label">NPWP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_npwp" id="staf_npwp" value="<?php echo $staf_npwp; ?>" placeholder="">
							</div>
							<label for="staf_nama_wajib_pajak" class="col-md-2 control-label">Nama Wajib Pajak</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nama_wajib_pajak" id="staf_nama_wajib_pajak" value="<?php echo $staf_nama_wajib_pajak; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status_perkawinan" class="col-md-2 control-label">Status Perkawinan</label>
							<div class="col-md-4">
								<?php combobox('1d', array("Kawin", "Belum Kawin", "Janda/Duda"), 'staf_status_perkawinan', '', '', $staf_status_perkawinan, '', '', 'class="form-control"');?>
							</div>
							<label for="staf_nama_suami_istri" class="col-md-2 control-label">Nama Suami / Istri</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nama_suami_istri" id="staf_nama_suami_istri" value="<?php echo $staf_nama_suami_istri; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nip_suami_istri" class="col-md-2 control-label">NIP Suami / Istri</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nip_suami_istri" id="staf_nip_suami_istri" value="<?php echo $staf_nip_suami_istri; ?>" placeholder="">
							</div>
							<label for="staf_pekerjaan_suami_istri" class="col-md-2 control-label">Pekerjaan Suami / Istri</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_pekerjaan_suami_istri" id="staf_pekerjaan_suami_istri" value="<?php echo $staf_pekerjaan_suami_istri; ?>" placeholder="">
							</div>
						</div>			
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Kepegawaian</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_jenis_ptk" class="col-md-2 control-label">Jenis PTK</label>
							<div class="col-md-4">
								<?php combobox('1d', array("Guru", "Guru Mapel", "Guru Inkubasi", "Guru TIK", "Kepala Sekolah", "Laboran", "Pengawas BK", "Penjaga Sekolah", "Pesuruh/Office Boy", "Petugas Keamanan", "Tenaga Administrasi Sekolah", "Tenaga Perpustakaan", "Tukang Kebun"), 'staf_jenis_ptk', '', '', $staf_jenis_ptk, '', '', 'class="form-control"');?>
							</div>
							<label for="staf_status_kepegawaian" class="col-md-2 control-label">Status Kepegawaian</label>
							<div class="col-md-4">
								<?php combobox('1d', array("PNS", "PNS Diperbantukan", "PNS Depag", "GTY/PTY", "Honor Daerah TK.I Provinsi", "Honor Daerah TK.II Kab/Kota", "Guru Bantu Pusat", "Guru Honor Sekolah", "Tenaga Honor Sekolah", "CPNS", "Kontrak Kerja WNA"), 'staf_status_kepegawaian', '', '', $staf_status_kepegawaian, '', '', 'class="form-control"');?>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nip" class="col-md-2 control-label">NIP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nip" id="staf_nip" value="<?php echo $staf_nip; ?>" placeholder="">
							</div>
							<label for="staf_niy" class="col-md-2 control-label">NIY/NIGK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_niy" id="staf_niy" value="<?php echo $staf_niy; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nuptk" class="col-md-2 control-label">NUPTK</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_nuptk" id="staf_nuptk" value="<?php echo $staf_nuptk; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_sk_pengangkatan" class="col-md-2 control-label">SK Pengangkatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_sk_pengangkatan" id="staf_sk_pengangkatan" value="<?php echo $staf_sk_pengangkatan; ?>" placeholder="">
							</div>
							<label for="staf_tmt_pengangkatan" class="col-md-2 control-label">TMT Pengangkatan</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt_pengangkatan" id="staf_tmt_pengangkatan" value="<?php echo $staf_tmt_pengangkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_lembaga_pengangkatan" class="col-md-2 control-label">Lembaga Pengangkatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_lembaga_pengangkatan" id="staf_lembaga_pengangkatan" value="<?php echo $staf_lembaga_pengangkatan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_sk_cpns" class="col-md-2 control-label">SK CPNS</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_sk_cpns" id="staf_sk_cpns" value="<?php echo $staf_sk_cpns; ?>" placeholder="">
							</div>
							<label for="staf_tanggal_cpns" class="col-md-2 control-label">TMT CPNS</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tanggal_cpns" id="staf_tanggal_cpns" value="<?php echo $staf_tanggal_cpns; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_tmt_pns" class="col-md-2 control-label">TMT PNS</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt_pns" id="staf_tmt_pns" value="<?php echo $staf_tmt_pns; ?>" placeholder="">
							</div>
							<label for="staf_pangkat_golongan" class="col-md-2 control-label">Pangkat / Golongan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_pangkat_golongan" id="staf_pangkat_golongan" value="<?php echo $staf_pangkat_golongan; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_sumber_gaji" class="col-md-2 control-label">Sumber Gaji</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_sumber_gaji" id="staf_sumber_gaji" value="<?php echo $staf_sumber_gaji; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Kompetensi Khusus</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_sudah_lisensi_kepala_sekolah" class="col-md-2 control-label">Punya Lisensi Kepala Sekolah</label>
							<div class="col-md-4">
								<div class="radio">
									<label>
										<input type="radio" name="staf_sudah_lisensi_kepala_sekolah" value="Ya" <?php echo ($staf_sudah_lisensi_kepala_sekolah == 'Ya')?'checked':''; ?>> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_sudah_lisensi_kepala_sekolah" value="Tidak" <?php echo ($staf_sudah_lisensi_kepala_sekolah == 'Tidak')?'checked':''; ?>> Tidak
									</label>
								</div>
							</div>
							<label for="staf_pernah_diklat_kepengawasan" class="col-md-2 control-label">Pernah Diklat Kepegawaian</label>
							<div class="col-md-4">
								<div class="radio">
									<label>
										<input type="radio" name="staf_pernah_diklat_kepengawasan" value="Ya" <?php echo ($staf_pernah_diklat_kepengawasan == 'Ya')?'checked':''; ?>> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_pernah_diklat_kepengawasan" value="Tidak" <?php echo ($staf_pernah_diklat_kepengawasan == 'Tidak')?'checked':''; ?>> Tidak
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="staf_keahlian_braille" class="col-md-2 control-label">Keahlian Braille</label>
							<div class="col-md-4">
								<div class="radio">
									<label>
										<input type="radio" name="staf_keahlian_braille" value="Ya" <?php echo ($staf_keahlian_braille == 'Ya')?'checked':''; ?>> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_keahlian_braille" value="Tidak" <?php echo ($staf_keahlian_braille == 'Tidak')?'checked':''; ?>> Tidak
									</label>
								</div>
							</div>
							<label for="staf_keahlian_bahasa_isyarat" class="col-md-2 control-label">Keahlian Bahasa Isyarat</label>
							<div class="col-md-4">
								<div class="radio">
									<label>
										<input type="radio" name="staf_keahlian_bahasa_isyarat" value="Ya" <?php echo ($staf_keahlian_bahasa_isyarat == 'Ya')?'checked':''; ?>> Ya
									</label>
									&nbsp;&nbsp;&nbsp;
									<label>
										<input type="radio" name="staf_keahlian_bahasa_isyarat" value="Tidak" <?php echo ($staf_keahlian_bahasa_isyarat == 'Tidak')?'checked':''; ?>> Tidak
									</label>
								</div>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Kontak</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_telepon" class="col-md-2 control-label">Telepon</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_telepon" id="staf_telepon" value="<?php echo $staf_telepon; ?>" placeholder="">
							</div>
							<label for="staf_hp" class="col-md-2 control-label">HP</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_hp" id="staf_hp" value="<?php echo $staf_hp; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_email" class="col-md-2 control-label">Email</label>
							<div class="col-md-10">
								<input type="email" class="form-control" name="staf_email" id="staf_email" value="<?php echo $staf_email; ?>" placeholder="">
							</div>
						</div>
						</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Bank</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_bank" class="col-md-2 control-label">Bank</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="staf_bank" id="staf_bank" value="<?php echo $staf_bank; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_nomor_rekening_bank" class="col-md-2 control-label">Nomor Rekening</label>
							<div class="col-md-4">
								<input type="number" class="form-control" name="staf_nomor_rekening_bank" id="staf_nomor_rekening_bank" value="<?php echo $staf_nomor_rekening_bank; ?>" placeholder="">
							</div>
							<label for="staf_rekening_atas_nama" class="col-md-2 control-label">Rekening Atas Nama</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_rekening_atas_nama" id="staf_rekening_atas_nama" value="<?php echo $staf_rekening_atas_nama; ?>" placeholder="">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-header">
					  <h3 class="box-title">Informasi Lainnya</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="staf_pelajaran" class="col-md-2 control-label">Mata Pelajaran</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_pelajaran" id="staf_pelajaran" value="<?php echo $staf_pelajaran; ?>" placeholder="">
							</div>
							<label for="staf_kode" class="col-md-2 control-label">Staf Kode</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kode" id="staf_kode" value="<?php echo $staf_kode; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_tmt" class="col-md-2 control-label">Tanggal Mulai Kerja</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tmt" id="staf_tmt" value="<?php echo $staf_tmt; ?>" placeholder="">
							</div>
							<label for="staf_tst" class="col-md-2 control-label">Tanggal Akhir Kerja</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="staf_tst" id="staf_tst" value="<?php echo $staf_tst; ?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="staf_status" class="col-md-2 control-label">Departemen</label>
							<div class="col-md-4">
								<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_id", "departemen_id", "departemen_nama", $departemen_id, '', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Ubah Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Ubah Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$staf_id);?>" method="post">
				<input type="hidden" class="form-control" name="staf_id" id="staf_id" value="<?php echo $staf_id; ?>" placeholder="">
				<div class="box-body">
								
				<div class="box-header">
					<h3 class="box-title">Informasi Akun</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_user" class="col-md-2 control-label">User</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_user" id="staf_user" value="<?php echo $staf_user; ?>" placeholder="">
						</div>
						<label for="staf_pin" class="col-md-2 control-label">PIN</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_pin" id="staf_pin" value="<?php echo $staf_pin; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							* Apabila User dan PIN tidak diisi maka user guru tidak akan dibuat.
						</div>
					</div>
				</div>
				<div class="box-header">
					<h3 class="box-title">Informasi Identitas</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_nama" class="col-md-2 control-label">Nama</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="staf_nama" id="staf_nama" value="<?php echo $staf_nama; ?>" placeholder="" required>
						</div>
					</div>
					<div class="form-group">
						<label for="staf_nik" class="col-md-2 control-label">NIK</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="staf_nik" id="staf_nik" value="<?php echo $staf_nik; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_jenis_kelamin" class="col-md-2 control-label">Jenis Kelamin</label>
						<div class="col-md-10">
							<div class="radio">
								<label>
									<input type="radio" name="staf_jenis_kelamin" value="L" <?php echo $jenis_kelamin_a = ($staf_jenis_kelamin == 'L')?'checked':''; ?>> L
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="staf_jenis_kelamin" value="P" <?php echo $jenis_kelamin_h = ($staf_jenis_kelamin == 'P')?'checked':''; ?>> P
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="staf_tempat_lahir" class="col-md-2 control-label">Tempat, Tanggal Lahir</label>
						<div class="col-md-5">
							<input type="text" class="form-control" name="staf_tempat_lahir" id="staf_tempat_lahir" value="<?php echo $staf_tempat_lahir; ?>" placeholder="">
						</div>
						<div class="col-md-5">
							<input type="date" class="form-control" name="staf_tanggal_lahir" id="staf_tanggal_lahir" value="<?php echo $staf_tanggal_lahir; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_nama_ibu_kandung" class="col-md-2 control-label">Nama Ibu Kandung</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="staf_nama_ibu_kandung" id="staf_nama_ibu_kandung" value="<?php echo $staf_nama_ibu_kandung; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-header">
					<h3 class="box-title">Informasi Data Pribadi</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_alamat" class="col-md-2 control-label">Alamat</label>
						<div class="col-md-10">
							<textarea type="text" class="form-control" name="staf_alamat" id="staf_alamat" placeholder=""><?php echo $staf_alamat; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="staf_dusun" class="col-md-2 control-label">RT/RW</label>
						<div class="col-md-2">
							<input type="text" class="form-control" name="staf_rt" id="staf_rt" value="<?php echo $staf_rt; ?>" placeholder="RT">
						</div>
						<div class="col-md-2">
							<input type="text" class="form-control" name="staf_rw" id="staf_rw" value="<?php echo $staf_rw; ?>" placeholder="RW">
						</div>
						<label for="staf_dusun" class="col-md-2 control-label">Dusun</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_dusun" id="staf_dusun" value="<?php echo $staf_dusun; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_deskel" class="col-md-2 control-label">Desa/Kelurahan</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_deskel" id="staf_deskel" value="<?php echo $staf_deskel; ?>" placeholder="">
						</div>
						<label for="staf_kecamatan" class="col-md-2 control-label">Kecamatan</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_kecamatan" id="staf_kecamatan" value="<?php echo $staf_kecamatan; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_kabkota" class="col-md-2 control-label">Kota/Kabupaten</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_kabkota" id="staf_kabkota" value="<?php echo $staf_kabkota; ?>" placeholder="">
						</div>
						<label for="staf_provinsi" class="col-md-2 control-label">Provinsi</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_provinsi" id="staf_provinsi" value="<?php echo $staf_provinsi; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_kodepos" class="col-md-2 control-label">Kode Pos</label>
						<div class="col-md-3">
							<input type="number" class="form-control" name="staf_kodepos" id="staf_kodepos" value="<?php echo $staf_kodepos; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_agama" class="col-md-2 control-label">Agama & Kepercayaan</label>
						<div class="col-md-4">
							<?php combobox('1d', array("Islam", "Kristen", "Katholik", "Hindu", "Budha", "Kong Hu Chu", "Kepercayaan kpd Tuhan YME", "Lainnya"), 'staf_agama', '', '', $staf_agama, '', '', 'class="form-control"');?>
						</div>
						<label for="staf_kewarganegaraan" class="col-md-2 control-label">Kewarganegaraan</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_kewarganegaraan" id="staf_kewarganegaraan" value="<?php echo $staf_kewarganegaraan; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_npwp" class="col-md-2 control-label">NPWP</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_npwp" id="staf_npwp" value="<?php echo $staf_npwp; ?>" placeholder="">
						</div>
						<label for="staf_nama_wajib_pajak" class="col-md-2 control-label">Nama Wajib Pajak</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_nama_wajib_pajak" id="staf_nama_wajib_pajak" value="<?php echo $staf_nama_wajib_pajak; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_status_perkawinan" class="col-md-2 control-label">Status Perkawinan</label>
						<div class="col-md-4">
							<?php combobox('1d', array("Kawin", "Belum Kawin", "Janda/Duda"), 'staf_status_perkawinan', '', '', $staf_status_perkawinan, '', '', 'class="form-control"');?>
						</div>
						<label for="staf_nama_suami_istri" class="col-md-2 control-label">Nama Suami / Istri</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_nama_suami_istri" id="staf_nama_suami_istri" value="<?php echo $staf_nama_suami_istri; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_nip_suami_istri" class="col-md-2 control-label">NIP Suami / Istri</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_nip_suami_istri" id="staf_nip_suami_istri" value="<?php echo $staf_nip_suami_istri; ?>" placeholder="">
						</div>
						<label for="staf_pekerjaan_suami_istri" class="col-md-2 control-label">Pekerjaan Suami / Istri</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_pekerjaan_suami_istri" id="staf_pekerjaan_suami_istri" value="<?php echo $staf_pekerjaan_suami_istri; ?>" placeholder="">
						</div>
					</div>			
				</div><!-- /.box-body -->
				<div class="box-header">
					<h3 class="box-title">Informasi Kepegawaian</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_jenis_ptk" class="col-md-2 control-label">Jenis PTK</label>
						<div class="col-md-4">
							<?php combobox('1d', array("Guru", "Guru Mapel", "Guru Inkubasi", "Guru TIK", "Kepala Sekolah", "Laboran", "Pengawas BK", "Penjaga Sekolah", "Pesuruh/Office Boy", "Petugas Keamanan", "Tenaga Administrasi Sekolah", "Tenaga Perpustakaan", "Tukang Kebun"), 'staf_jenis_ptk', '', '', $staf_jenis_ptk, '', '', 'class="form-control"');?>
						</div>
						<label for="staf_status_kepegawaian" class="col-md-2 control-label">Status Kepegawaian</label>
						<div class="col-md-4">
							<?php combobox('1d', array("PNS", "PNS Diperbantukan", "PNS Depag", "GTY/PTY", "Honor Daerah TK.I Provinsi", "Honor Daerah TK.II Kab/Kota", "Guru Bantu Pusat", "Guru Honor Sekolah", "Tenaga Honor Sekolah", "CPNS", "Kontrak Kerja WNA"), 'staf_status_kepegawaian', '', '', $staf_status_kepegawaian, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="staf_nip" class="col-md-2 control-label">NIP</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_nip" id="staf_nip" value="<?php echo $staf_nip; ?>" placeholder="">
						</div>
						<label for="staf_niy" class="col-md-2 control-label">NIY/NIGK</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_niy" id="staf_niy" value="<?php echo $staf_niy; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_nuptk" class="col-md-2 control-label">NUPTK</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_nuptk" id="staf_nuptk" value="<?php echo $staf_nuptk; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_sk_pengangkatan" class="col-md-2 control-label">SK Pengangkatan</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_sk_pengangkatan" id="staf_sk_pengangkatan" value="<?php echo $staf_sk_pengangkatan; ?>" placeholder="">
						</div>
						<label for="staf_tmt_pengangkatan" class="col-md-2 control-label">TMT Pengangkatan</label>
						<div class="col-md-4">
							<input type="date" class="form-control" name="staf_tmt_pengangkatan" id="staf_tmt_pengangkatan" value="<?php echo $staf_tmt_pengangkatan; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_lembaga_pengangkatan" class="col-md-2 control-label">Lembaga Pengangkatan</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_lembaga_pengangkatan" id="staf_lembaga_pengangkatan" value="<?php echo $staf_lembaga_pengangkatan; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_sk_cpns" class="col-md-2 control-label">SK CPNS</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_sk_cpns" id="staf_sk_cpns" value="<?php echo $staf_sk_cpns; ?>" placeholder="">
						</div>
						<label for="staf_tanggal_cpns" class="col-md-2 control-label">TMT CPNS</label>
						<div class="col-md-4">
							<input type="date" class="form-control" name="staf_tanggal_cpns" id="staf_tanggal_cpns" value="<?php echo $staf_tanggal_cpns; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_tmt_pns" class="col-md-2 control-label">TMT PNS</label>
						<div class="col-md-4">
							<input type="date" class="form-control" name="staf_tmt_pns" id="staf_tmt_pns" value="<?php echo $staf_tmt_pns; ?>" placeholder="">
						</div>
						<label for="staf_pangkat_golongan" class="col-md-2 control-label">Pangkat / Golongan</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_pangkat_golongan" id="staf_pangkat_golongan" value="<?php echo $staf_pangkat_golongan; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_sumber_gaji" class="col-md-2 control-label">Sumber Gaji</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_sumber_gaji" id="staf_sumber_gaji" value="<?php echo $staf_sumber_gaji; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-header">
					<h3 class="box-title">Kompetensi Khusus</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_sudah_lisensi_kepala_sekolah" class="col-md-2 control-label">Punya Lisensi Kepala Sekolah</label>
						<div class="col-md-4">
							<div class="radio">
								<label>
									<input type="radio" name="staf_sudah_lisensi_kepala_sekolah" value="Ya" <?php echo ($staf_sudah_lisensi_kepala_sekolah == 'Ya')?'checked':''; ?>> Ya
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="staf_sudah_lisensi_kepala_sekolah" value="Tidak" <?php echo ($staf_sudah_lisensi_kepala_sekolah == 'Tidak')?'checked':''; ?>> Tidak
								</label>
							</div>
						</div>
						<label for="staf_pernah_diklat_kepengawasan" class="col-md-2 control-label">Pernah Diklat Kepegawaian</label>
						<div class="col-md-4">
							<div class="radio">
								<label>
									<input type="radio" name="staf_pernah_diklat_kepengawasan" value="Ya" <?php echo ($staf_pernah_diklat_kepengawasan == 'Ya')?'checked':''; ?>> Ya
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="staf_pernah_diklat_kepengawasan" value="Tidak" <?php echo ($staf_pernah_diklat_kepengawasan == 'Tidak')?'checked':''; ?>> Tidak
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="staf_keahlian_braille" class="col-md-2 control-label">Keahlian Braille</label>
						<div class="col-md-4">
							<div class="radio">
								<label>
									<input type="radio" name="staf_keahlian_braille" value="Ya" <?php echo ($staf_keahlian_braille == 'Ya')?'checked':''; ?>> Ya
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="staf_keahlian_braille" value="Tidak" <?php echo ($staf_keahlian_braille == 'Tidak')?'checked':''; ?>> Tidak
								</label>
							</div>
						</div>
						<label for="staf_keahlian_bahasa_isyarat" class="col-md-2 control-label">Keahlian Bahasa Isyarat</label>
						<div class="col-md-4">
							<div class="radio">
								<label>
									<input type="radio" name="staf_keahlian_bahasa_isyarat" value="Ya" <?php echo ($staf_keahlian_bahasa_isyarat == 'Ya')?'checked':''; ?>> Ya
								</label>
								&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" name="staf_keahlian_bahasa_isyarat" value="Tidak" <?php echo ($staf_keahlian_bahasa_isyarat == 'Tidak')?'checked':''; ?>> Tidak
								</label>
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-header">
					<h3 class="box-title">Informasi Kontak</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_telepon" class="col-md-2 control-label">Telepon</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_telepon" id="staf_telepon" value="<?php echo $staf_telepon; ?>" placeholder="">
						</div>
						<label for="staf_hp" class="col-md-2 control-label">HP</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_hp" id="staf_hp" value="<?php echo $staf_hp; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_email" class="col-md-2 control-label">Email</label>
						<div class="col-md-10">
							<input type="email" class="form-control" name="staf_email" id="staf_email" value="<?php echo $staf_email; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-header">
					<h3 class="box-title">Informasi Bank</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="staf_bank" class="col-md-2 control-label">Bank</label>
						<div class="col-md-10">
							<input type="text" class="form-control" name="staf_bank" id="staf_bank" value="<?php echo $staf_bank; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_nomor_rekening_bank" class="col-md-2 control-label">Nomor Rekening</label>
						<div class="col-md-4">
							<input type="number" class="form-control" name="staf_nomor_rekening_bank" id="staf_nomor_rekening_bank" value="<?php echo $staf_nomor_rekening_bank; ?>" placeholder="">
						</div>
						<label for="staf_rekening_atas_nama" class="col-md-2 control-label">Rekening Atas Nama</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="staf_rekening_atas_nama" id="staf_rekening_atas_nama" value="<?php echo $staf_rekening_atas_nama; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-header">
					<h3 class="box-title">Informasi Lainnya</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
							<label for="staf_pelajaran" class="col-md-2 control-label">Mata Pelajaran</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_pelajaran" id="staf_pelajaran" value="<?php echo $staf_pelajaran; ?>" placeholder="">
							</div>
							<label for="staf_kode" class="col-md-2 control-label">Staf Kode</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="staf_kode" id="staf_kode" value="<?php echo $staf_kode; ?>" placeholder="">
							</div>
						</div>
					<div class="form-group">
						<label for="staf_tmt" class="col-md-2 control-label">Tanggal Mulai Kerja</label>
						<div class="col-md-4">
							<input type="date" class="form-control" name="staf_tmt" id="staf_tmt" value="<?php echo $staf_tmt; ?>" placeholder="">
						</div>
						<label for="staf_tst" class="col-md-2 control-label">Tanggal Akhir Kerja</label>
						<div class="col-md-4">
							<input type="date" class="form-control" name="staf_tst" id="staf_tst" value="<?php echo $staf_tst; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="staf_status" class="col-md-2 control-label">Departemen</label>
						<div class="col-md-4">
							<?php echo $this->Departemen_model->combobox_departemen(0, "", 0, "departemen_id", "departemen_id", "departemen_nama", $departemen_id, '', '-- PILIH DEPARTEMEN --', 'class="form-control select2" required');?>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Siswa
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Import Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
								<div class="input-group">
									<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
									<span class="input-group-btn">
										<input type="submit" name="importStaf" value="Import" class="btn btn-primary" />
									</span>
								</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetStaf" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
								  <th>#</th>
								  <th>DEPARTEMEN</th>
								  <th>USERID</th>
								  <th>PIN</th>
								  <th>NAMA</th>
								  <th>NUPTK</th>
								  <th>JENIS KELAMIN</th>
								  <th>TEMPAT LAHIR</th>
								  <th>TANGGAL LAHIR</th>
								  <th>NIP</th>
								  <th>STATUS KEPEGAWAIAN</th>
								  <th>JENIS PTK</th>
								  <th>AGAMA</th>
								  <th>ALAMAT JALAN</th>
								  <th>RT</th>
								  <th>RW</th>
								  <th>NAMA DUSUN</th>
								  <th>DESA/KELURAHAN</th>
								  <th>KECAMATAN</th>
								  <th>KODE POS</th>
								  <th>TELEPON</th>
								  <th>HP</th>
								  <th>EMAIL</th>
								  <th>TUGAS TAMBAHAN</th>
								  <th>SK CPNS</th>
								  <th>TANGGAL CPNS</th>
								  <th>SK PENGANGKATAN</th>
								  <th>TMT PENGANGKATAN</th>
								  <th>LEMBAGA PENGANGKATAN</th>
								  <th>PANGKAT GOLONGAN</th>
								  <th>SUMBER GAJI</th>
								  <th>NAMA IBU KANDUNG</th>
								  <th>STATUS PERKAWINAN</th>
								  <th>NAMA SUAMI/ISTRI</th>
								  <th>NIP SUAMI/ISTRI</th>
								  <th>PEKERJAAN SUAMI/ISTRI</th>
								  <th>TMT PNS</th>
								  <th>SUDAH LISENSI KEPSEK</th>
								  <th>DIKLAT KEPENGAWASAN</th>
								  <th>KEAHLIAN BRAILLE</th>
								  <th>KEAHLIAN BAHASA ISYARAT</th>
								  <th>NPWP</th>
								  <th>NAMA WAJIB PAJAK</th>
								  <th>KEWARGANEGARAAN</th>
								  <th>BANK</th>
								  <th>NOMOR REKENING</th>
								  <th>REKENING ATAS NAMA</th>
								  <th>NIK</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
									$dept_color = 'style="background:#EEB9B9"';
									if ($row['departemen_id']){
										$dept_color = 'style="background:#B1E3B1"';
									}
									?>
									<tr <?php echo $dept_color; ?>>
									  <th scope="row"><?php echo $i; ?></th>
									  <td><?php echo $row['staf_departemen']; ?></td>
									  <td><?php echo $row['staf_user']; ?></td>
									  <td><?php echo $row['staf_pin']; ?></td>
									  <td nowrap><?php echo $row['staf_nama']; ?></td>
									  <td><?php echo $row['staf_nuptk']; ?></td>
									  <td><?php echo $row['staf_jenis_kelamin']; ?></td>
									  <td nowrap><?php echo $row['staf_tempat_lahir']; ?></td>
									  <td nowrap><?php echo $row['staf_tanggal_lahir']; ?></td>
									  <td><?php echo $row['staf_nip']; ?></td>
									  <td><?php echo $row['staf_status_kepegawaian']; ?></td>
									  <td><?php echo $row['staf_jenis_ptk']; ?></td>
									  <td><?php echo $row['staf_agama']; ?></td>
									  <td><?php echo $row['staf_alamat']; ?></td>
									  <td><?php echo $row['staf_rt']; ?></td>
									  <td><?php echo $row['staf_rw']; ?></td>
									  <td><?php echo $row['staf_dusun']; ?></td>
									  <td><?php echo $row['staf_deskel']; ?></td>
									  <td><?php echo $row['staf_kecamatan']; ?></td>
									  <td><?php echo $row['staf_kodepos']; ?></td>
									  <td><?php echo $row['staf_telepon']; ?></td>
									  <td><?php echo $row['staf_hp']; ?></td>
									  <td><?php echo $row['staf_email']; ?></td>
									  <td><?php echo $row['staf_tugas_tambahan']; ?></td>
									  <td><?php echo $row['staf_sk_cpns']; ?></td>
									  <td><?php echo $row['staf_tanggal_cpns']; ?></td>
									  <td><?php echo $row['staf_sk_pengangkatan']; ?></td>
									  <td><?php echo $row['staf_tmt_pengangkatan']; ?></td>
									  <td><?php echo $row['staf_lembaga_pengangkatan']; ?></td>
									  <td><?php echo $row['staf_pangkat_golongan']; ?></td>
									  <td><?php echo $row['staf_sumber_gaji']; ?></td>
									  <td><?php echo $row['staf_nama_ibu_kandung']; ?></td>
									  <td><?php echo $row['staf_status_perkawinan']; ?></td>
									  <td><?php echo $row['staf_nama_suami_istri']; ?></td>
									  <td><?php echo $row['staf_nip_suami_istri']; ?></td>
									  <td><?php echo $row['staf_pekerjaan_suami_istri']; ?></td>
									  <td><?php echo $row['staf_tmt_pns']; ?></td>
									  <td><?php echo $row['staf_sudah_lisensi_kepala_sekolah']; ?></td>
									  <td><?php echo $row['staf_pernah_diklat_kepengawasan']; ?></td>
									  <td><?php echo $row['staf_keahlian_braille']; ?></td>
									  <td><?php echo $row['staf_keahlian_bahasa_isyarat']; ?></td>
									  <td><?php echo $row['staf_npwp']; ?></td>
									  <td><?php echo $row['staf_nama_wajib_pajak']; ?></td>
									  <td><?php echo $row['staf_kewarganegaraan']; ?></td>
									  <td><?php echo $row['staf_bank']; ?></td>
									  <td><?php echo $row['staf_nomor_rekening_bank']; ?></td>
									  <td><?php echo $row['staf_rekening_atas_nama']; ?></td>
									  <td><?php echo $row['staf_nik']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'upload_foto') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Upload Foto Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Upload Foto Staf</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Upload File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="fileFoto[]" class="form-control" placeholder="" multiple="true" data-required="true" accept="image/x-png, image/gif, image/jpeg" />
								<span class="input-group-btn">
									<input type="submit" name="uploadFoto" value="Upload" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'detail') {?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Staf
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Staf</a></li>
            <li class="active">Detail Staf</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Detail Staf</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
									<div class="box-header">
										<h3 class="box-title">Informasi Akun</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="staf_user" class="col-md-2 control-label">User</label>
											<div class="col-md-4">
												<?php echo $staf_user; ?>
											</div>
											<label for="staf_pin" class="col-md-2 control-label">PIN</label>
											<div class="col-md-4">
												<?php echo $staf_pin; ?>
											</div>
										</div>
									</div>
									<div class="box-header">
										<h3 class="box-title">Informasi Identitas</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="staf_nama" class="col-md-2 control-label">Nama</label>
											<div class="col-md-10">
												<?php echo $staf_nama; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_nik" class="col-md-2 control-label">NIK</label>
											<div class="col-md-10">
												<?php echo $staf_nik; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_jenis_kelamin" class="col-md-2 control-label">Jenis Kelamin</label>
											<div class="col-md-10">
												<?php echo $staf_jenis_kelamin; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_tempat_lahir" class="col-md-2 control-label">Tempat, Tanggal Lahir</label>
											<div class="col-md-5">
												<?php echo $staf_tempat_lahir; ?>
											</div>
											<div class="col-md-5">
												<?php echo $staf_tanggal_lahir; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_nama_ibu_kandung" class="col-md-2 control-label">Nama Ibu Kandung</label>
											<div class="col-md-10">
												<?php echo $staf_nama_ibu_kandung; ?>
											</div>
										</div>
									</div>
									<div class="box-header">
										<h3 class="box-title">Informasi Data Pribadi</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="staf_alamat" class="col-md-2 control-label">Alamat</label>
											<div class="col-md-10">
												<?php echo $staf_alamat; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_dusun" class="col-md-2 control-label">RT/RW</label>
											<div class="col-md-2">
												<?php echo $staf_rt; ?> / <?php echo $staf_rw; ?>
											</div>
											<label for="staf_dusun" class="col-md-2 control-label">Dusun</label>
											<div class="col-md-4">
												<?php echo $staf_dusun; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_deskel" class="col-md-2 control-label">Desa/Kelurahan</label>
											<div class="col-md-4">
												<?php echo $staf_deskel; ?>
											</div>
											<label for="staf_kecamatan" class="col-md-2 control-label">Kecamatan</label>
											<div class="col-md-4">
												<?php echo $staf_kecamatan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_kabkota" class="col-md-2 control-label">Kota/Kabupaten</label>
											<div class="col-md-4">
												<?php echo $staf_kabkota; ?>
											</div>
											<label for="staf_provinsi" class="col-md-2 control-label">Provinsi</label>
											<div class="col-md-4">
												<?php echo $staf_provinsi; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_kodepos" class="col-md-2 control-label">Kode Pos</label>
											<div class="col-md-3">
												<?php echo $staf_kodepos; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_agama" class="col-md-2 control-label">Agama & Kepercayaan</label>
											<div class="col-md-4">
												<?php echo $staf_agama; ?>
											</div>
											<label for="staf_kewarganegaraan" class="col-md-2 control-label">Kewarganegaraan</label>
											<div class="col-md-4">
												<?php echo $staf_kewarganegaraan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_npwp" class="col-md-2 control-label">NPWP</label>
											<div class="col-md-4">
												<?php echo $staf_npwp; ?>
											</div>
											<label for="staf_nama_wajib_pajak" class="col-md-2 control-label">Nama Wajib Pajak</label>
											<div class="col-md-4">
												<?php echo $staf_nama_wajib_pajak; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_status_perkawinan" class="col-md-2 control-label">Status Perkawinan</label>
											<div class="col-md-4">
												<?php echo $staf_status_perkawinan; ?>
											</div>
											<label for="staf_nama_suami_istri" class="col-md-2 control-label">Nama Suami / Istri</label>
											<div class="col-md-4">
												<?php echo $staf_nama_suami_istri; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_nip_suami_istri" class="col-md-2 control-label">NIP Suami / Istri</label>
											<div class="col-md-4">
												<?php echo $staf_nip_suami_istri; ?>
											</div>
											<label for="staf_pekerjaan_suami_istri" class="col-md-2 control-label">Pekerjaan Suami / Istri</label>
											<div class="col-md-4">
												<?php echo $staf_pekerjaan_suami_istri; ?>
											</div>
										</div>			
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Informasi Kepegawaian</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="staf_jenis_ptk" class="col-md-2 control-label">Jenis PTK</label>
											<div class="col-md-4">
												<?php echo $staf_jenis_ptk; ?>
											</div>
											<label for="staf_status_kepegawaian" class="col-md-2 control-label">Status Kepegawaian</label>
											<div class="col-md-4">
												<?php echo $staf_status_kepegawaian; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_nip" class="col-md-2 control-label">NIP</label>
											<div class="col-md-4">
												<?php echo $staf_nip; ?>
											</div>
											<label for="staf_niy" class="col-md-2 control-label">NIY/NIGK</label>
											<div class="col-md-4">
												<?php echo $staf_niy; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_nuptk" class="col-md-2 control-label">NUPTK</label>
											<div class="col-md-4">
												<?php echo $staf_nuptk; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_sk_pengangkatan" class="col-md-2 control-label">SK Pengangkatan</label>
											<div class="col-md-4">
												<?php echo $staf_sk_pengangkatan; ?>
											</div>
											<label for="staf_tmt_pengangkatan" class="col-md-2 control-label">TMT Pengangkatan</label>
											<div class="col-md-4">
												<?php echo $staf_tmt_pengangkatan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_lembaga_pengangkatan" class="col-md-2 control-label">Lembaga Pengangkatan</label>
											<div class="col-md-4">
												<?php echo $staf_lembaga_pengangkatan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_sk_cpns" class="col-md-2 control-label">SK CPNS</label>
											<div class="col-md-4">
												<?php echo $staf_sk_cpns; ?>
											</div>
											<label for="staf_tanggal_cpns" class="col-md-2 control-label">TMT CPNS</label>
											<div class="col-md-4">
												<?php echo $staf_tanggal_cpns; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_tmt_pns" class="col-md-2 control-label">TMT PNS</label>
											<div class="col-md-4">
												<?php echo $staf_tmt_pns; ?>
											</div>
											<label for="staf_pangkat_golongan" class="col-md-2 control-label">Pangkat / Golongan</label>
											<div class="col-md-4">
												<?php echo $staf_pangkat_golongan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_sumber_gaji" class="col-md-2 control-label">Sumber Gaji</label>
											<div class="col-md-4">
												<?php echo $staf_sumber_gaji; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Kompetensi Khusus</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="staf_sudah_lisensi_kepala_sekolah" class="col-md-2 control-label">Punya Lisensi Kepala Sekolah</label>
											<div class="col-md-4">
												<?php echo $staf_sudah_lisensi_kepala_sekolah; ?>
											</div>
											<label for="staf_pernah_diklat_kepengawasan" class="col-md-2 control-label">Pernah Diklat Kepegawaian</label>
											<div class="col-md-4">
												<?php echo $staf_pernah_diklat_kepengawasan; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_keahlian_braille" class="col-md-2 control-label">Keahlian Braille</label>
											<div class="col-md-4">
												<?php echo $staf_keahlian_braille; ?>
											</div>
											<label for="staf_keahlian_bahasa_isyarat" class="col-md-2 control-label">Keahlian Bahasa Isyarat</label>
											<div class="col-md-4">
												<?php echo $staf_keahlian_bahasa_isyarat; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Informasi Kontak</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="staf_telepon" class="col-md-2 control-label">Telepon</label>
											<div class="col-md-4">
												<?php echo $staf_telepon; ?>
											</div>
											<label for="staf_hp" class="col-md-2 control-label">HP</label>
											<div class="col-md-4">
												<?php echo $staf_hp; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_email" class="col-md-2 control-label">Email</label>
											<div class="col-md-10">
												<?php echo $staf_email; ?>
											</div>
										</div>
										</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Informasi Bank</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group row">
											<label for="staf_bank" class="col-md-2 control-label">Bank</label>
											<div class="col-md-10">
												<?php echo $staf_bank; ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="staf_nomor_rekening_bank" class="col-md-2 control-label">Nomor Rekening</label>
											<div class="col-md-4">
												<?php echo $staf_nomor_rekening_bank; ?>
											</div>
											<label for="staf_rekening_atas_nama" class="col-md-2 control-label">Rekening Atas Nama</label>
											<div class="col-md-4">
												<?php echo $staf_rekening_atas_nama; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
									<div class="box-header">
										<h3 class="box-title">Informasi Lainnya</h3>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="form-group">
											<label for="staf_pelajaran" class="col-md-2 control-label">Mata Pelajaran</label>
											<div class="col-md-10">
												<?php echo $staf_pelajaran; ?>
											</div>
										</div>
									</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
						<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$staf_id); ?>'" class="btn btn-primary" name="save" value="save">Update</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php } ?>