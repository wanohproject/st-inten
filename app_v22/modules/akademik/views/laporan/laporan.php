<?php if ($action == '' || $action == 'index'){ ?>
<style>
.icon-btn {
  height: 60px;
  min-width: 80px;
  margin: 5px 5px 0 0;
  border: 1px solid #ddd;
  padding: 12px 5px 0px 5px;
  background-color: #fafafa;
  background-image: none;
  filter: none;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
  display: inline-block;
  color: #646464;
  text-shadow: none;
  text-align: center;
  cursor: pointer;
  position: relative;
  -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -ms-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
}
.icon-btn:hover {
  text-decoration: none;
  border-color: #999;
  color: #444;
  text-shadow: 0 1px 0px white;
  -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -ms-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
}
.icon-btn:hover > .badge {
  -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -ms-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
}
.icon-btn > div {
  margin-top: 5px;
  margin-bottom: 20px;
  color: #000;
  font-size: 12px;
  font-weight: 300;
}
.icon-btn > .badge {
  position: absolute;
  font-size: 11px;
  font-weight: 300;
  top: -5px;
  right: -5px;
  padding: 3px 6px 3px 6px;
  color: white;
  text-shadow: none;
  border-width: 0;
  border-style: solid;
  -webkit-border-radius: 12px;
  -moz-border-radius: 12px;
  border-radius: 12px;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
}
.icon-btn > i {
  font-size: 18px;
}
.ie8 .icon-btn:hover {
  filter: none;
}
</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Laporan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Laporan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Laporan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<h4>Legger Nilai</h4>
					<a href="<?php echo module_url('laporan/legger'); ?>" class="icon-btn">
						<i class="fa fa-book"></i>
						<div>Legger Mata Pelajaran</div>
					</a>
					<!-- <a href="<?php echo module_url('laporan/legger-semester'); ?>" class="icon-btn">
						<i class="fa fa-book"></i>
						<div>Legger Semua Semester</div>
					</a> -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'add') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Legger
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('laporan'); ?>">Laporan</a></li>
            <li class="active">Legger Walikelas</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <form action="<?php echo module_url($this->uri->segment(2).'/index/'.$tahun_kode.'/'.$semester_id.'/'.$pelajaran_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$komponen_id);?>" method="post">
                <div class="box-header">
					<h3 class="box-title">Legger Walikelas</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-body">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="semester_id" class="control-label">Semester</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="staf_id" class="control-label">Guru</label>
									<div class="">
										<?php 
										if ($this->session->userdata('level') == 10){
											$this->Staf_model->combobox_guru('staf_id', $staf_id, 'submit();', 'none', 'class="form-control select2" required', true, $tahun_kode, userdata('staf_id'));
										} else {
											$this->Staf_model->combobox_guru('staf_id', $staf_id, 'submit();', '', 'class="form-control select2" required', true, $tahun_kode);
										}
										?>
									</div>		
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="pelajaran_id" class="control-label">Mata Pelajaran</label>
									<div class="">
										<?php $this->Mata_pelajaran_model->combobox_mata_pelajaran_guru2('pelajaran_id', $pelajaran_id, 'submit();', '', 'class="form-control select2" required', true, $staf_id); ?>
									</div>		
								</div>
							</div>
						</div>
						<div class="row">
							<?php if ($pelajaran_sifat == 'Wajib'){?>
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tingkat</label>
									<div class="">
										<?php $this->Tingkat_model->combobox_tingkat_guru_pelajaran('tingkat_id', $tingkat_id, 'submit();', '', 'class="form-control select2" required', true, $staf_id, $tahun_kode, $pelajaran_id); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php $this->Kelas_model->combobox_kelas_guru_pelajaran('kelas_id', $kelas_id, 'submit();', '', 'class="form-control select2" required', true, $staf_id, $tahun_kode, $tingkat_id, $pelajaran_id); ?>
									</div>		
								</div>
							</div>
							<?php } else { ?>
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tingkat</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tahun_kode = '{$tahun_kode}' AND staf_id = '{$staf_id}' GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '{$tahun_kode}' AND tingkat_id = '{$tingkat_id}' AND staf_id = '{$staf_id}' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
							<?php } ?>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="komponen_id" class="control-label">Nilai</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM komponen_pelajaran LEFT JOIN komponen_penilaian ON komponen_pelajaran.komponen_id=komponen_penilaian.komponen_id WHERE tahun_kode = '{$tahun_kode}' ORDER BY komponen_nama ASC")->result(), 'komponen_id', 'komponen_id', 'komponen_nama', $komponen_id, 'submit();', '', 'class="form-control select2" required'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="komponen_id" class="control-label">Aspek</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM aspek_pelajaran LEFT JOIN aspek_penilaian ON aspek_pelajaran.aspek_id=aspek_penilaian.aspek_id WHERE tahun_kode = '{$tahun_kode}' AND pelajaran_id = '{$pelajaran_id}' ORDER BY aspek_nama ASC")->result(), 'aspek_id', 'aspek_id', 'aspek_nama', $aspek_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<div class="box-header" id="boxnilai">
						<h3 class="box-title">Daftar Siswa</h3>
						<hr />
					</div><!-- /.box-header -->
					<?php 
					$pelajaran_ 		= $this->db->query("SELECT pelajaran_sifat FROM mata_pelajaran WHERE pelajaran_id = '{$pelajaran_id}'")->row();
					$pelajaran_sifat	= "Wajib";
					if ($pelajaran_){
						$pelajaran_sifat= $pelajaran_->pelajaran_sifat;
					}
					if ($pelajaran_sifat == 'Wajib'){
					?>
					<div class="box-body">
						<?php 
						if ($aspek_id){
							$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id, 'aspek_pelajaran.aspek_id'=>$aspek_id));
							$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id, 'aspek_pelajaran.aspek_id'=>$aspek_id));
						} else {
							$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id));
							$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id));
						}
						$query = $this->db->query("SELECT * FROM pengujian WHERE pengujian.tahun_kode = '$tahun_kode' AND pengujian.tingkat_id = '$tingkat_id' AND pengujian.semester_id = '$semester_id' AND pengujian.pelajaran_id='$pelajaran_id'");
						$jml_pengujian = $query->num_rows();
						$data_pengujian = $query->result();
						?>
						<table class="table table-bordered table-report">
							<thead>
								<tr>
									<th rowspan="2" width="10" style="vertical-align:middle">No</th>
									<th rowspan="2" width="200" style="vertical-align:middle">Siswa</th>
									<th rowspan="2" width="60" style="vertical-align:middle;text-align:center">KKM</th>
									<th colspan="<?php echo $count_aspek; ?>" style="text-align:center">Nilai</th>
									<th colspan="<?php echo $count_aspek; ?>" style="vertical-align:middle;text-align:center">Deskripsi</th>
								</tr>
								<tr>
								<?php if ($grid_aspek){ 
									foreach ($grid_aspek as $row){
										echo "<td style=\"text-align:center\" width=\"80\">{$row->aspek_kode}</td>";
									}
								}?>
								<?php if ($grid_aspek){ 
									foreach ($grid_aspek as $row){
										echo "<td style=\"text-align:center\" width=\"80\">{$row->aspek_kode}</td>";
									}
								}?>
								</tr>
							</thead>
							<tbody>
							<?php
							if ($kelas_id && $pelajaran_id && $komponen_id){
								$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa_kelas.siswa_id ASC");
								$i = 1;
								if ($query->num_rows() > 0) {
									foreach($query->result() as $row) {
										$where['nilai.siswa_id']		= $row->siswa_id;
										$where['nilai.komponen_id']		= $komponen_id;
										$where['nilai.pelajaran_id']	= $pelajaran_id;
										$where['nilai.tahun_kode']		= $tahun_kode;
										$where['nilai.semester_id']		= $semester_id;
										$where['siswa_kelas.kelas_id']	= $kelas_id;
										$nilai = $this->Nilai_model->get_nilai("*", $where);
										if ($nilai){
											$nilai_kkm		= $nilai->nilai_kkm;
										} else {
											$nilai_kkm		= '';
										}
									?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><strong><?php echo $row->siswa_nis; ?></strong><br /><?php echo $row->siswa_nama; ?></td>
										<td style="text-align:center;"><?php echo $nilai_kkm; ?></td>
										<?php 
										if ($grid_aspek){ 
											foreach ($grid_aspek as $row2){
												$where['nilai.siswa_id']		= $row->siswa_id;
												$where['nilai.aspek_id']		= $row2->aspek_id;
												$where['nilai.pelajaran_id']	= $pelajaran_id;
												$where['nilai.tahun_kode']		= $tahun_kode;
												$where['nilai.semester_id']		= $semester_id;
												$where['nilai.komponen_id']		= $komponen_id;
												$where['nilai.staf_id']			= $staf_id;
												$nilai = $this->Nilai_model->get_nilai("*", $where);
												if ($nilai){
												?>
												<td width="80" style="text-align:center;"><?php echo $nilai->nilai_angka . ' / ' . $nilai->nilai_huruf; ?></td>
												<?php
												} else {
												?>
												<td width="80" style="text-align:center;">0</td>
												<?php
												}
											}
										}
										if ($grid_aspek){ 
											foreach ($grid_aspek as $row2){
												$where_des['deskripsi_nilai.siswa_id']			= $row->siswa_id;
												$where_des['deskripsi_nilai.aspek_id']			= $row2->aspek_id;
												$where_des['deskripsi_nilai.pelajaran_id']		= $pelajaran_id;
												$where_des['deskripsi_nilai.tahun_kode']			= $tahun_kode;
												$where_des['deskripsi_nilai.semester_id']		= $semester_id;
												$where_des['deskripsi_nilai.komponen_id']		= $komponen_id;
												$where_des['deskripsi_nilai.staf_id']			= $staf_id;
												$deskripsi_nilai = $this->Deskripsi_nilai_model->get_deskripsi_nilai("*", $where_des);
												if ($deskripsi_nilai){
												?>
												<td><?php echo $deskripsi_nilai->nilai_deskripsi; ?></td>
												<?php } else { ?>
												<td>&nbsp;</td>
												<?php }
											}
										}
										?>
									</tr>
									<?php
									$i++;
									}
								}
							}
							?>
							</tbody>
						</table>
					</div><!-- /.box-body -->
					<?php } else { 
					$where_antar['antar_mata_pelajaran.siswa_id']		= $this->uri->segment(11);
					$where_antar['antar_mata_pelajaran.komponen_id']	= $komponen_id;
					$where_antar['antar_mata_pelajaran.pelajaran_id']	= $pelajaran_id;
					$where_antar['antar_mata_pelajaran.tahun_kode']		= $tahun_kode;
					$where_antar['antar_mata_pelajaran.semester_id']	= $semester_id;
					$antar = $this->Antar_mata_pelajaran_model->get_antar_mata_pelajaran("", $where_antar);					
					?>
					<input type="hidden" name="siswa_id" value="<?php echo $this->uri->segment(11); ?>" />
					<div class="box-body">
						<div class="row">
							<div class="col-md-3">
								<?php 
								$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_nama", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id));
								$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id));
								$query = $this->db->query("SELECT * FROM pengujian WHERE pengujian.tahun_kode = '$tahun_kode' AND pengujian.tingkat_id = '$tingkat_id' AND pengujian.semester_id = '$semester_id' AND pengujian.pelajaran_id='$pelajaran_id'");
								$jml_pengujian = $query->num_rows();
								$data_pengujian = $query->result();
								?>
								<table class="table table-bordered table-report">
									<thead>
										<tr>
											<th width="10" style="vertical-align:middle">No</th>
											<th style="vertical-align:middle">Siswa</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ($kelas_id && $pelajaran_id && $komponen_id){
											$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa_kelas.siswa_id ASC");
											$i = 1;
											if ($query->num_rows() > 0) {
												foreach($query->result() as $row) {
													if ($row->siswa_id == $this->uri->segment(11)){
														?>
														<tr style="background-color:#ddd">
															<td><?php echo $i; ?></td>
															<td><a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$tahun_kode.'/'.$semester_id.'/'.$pelajaran_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$komponen_id.'/'.$row->siswa_id.'#boxnilai');?>"><strong><?php echo $row->siswa_nis; ?></strong><br /><?php echo $row->siswa_nama; ?></a></td>
														</tr>
														<?php
													} else {
														?>
														<tr>
															<td><?php echo $i; ?></td>
															<td><a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$tahun_kode.'/'.$semester_id.'/'.$pelajaran_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$komponen_id.'/'.$row->siswa_id.'#boxnilai');?>"><strong><?php echo $row->siswa_nis; ?></strong><br /><?php echo $row->siswa_nama; ?></a></td>
														</tr>
														<?php
													}
												$i++;
												}
											}
										}
										?>
									</tbody>
								</table>
							</div>
							<?php
							if ($kelas_id && $pelajaran_id && $komponen_id && $this->uri->segment(11)){
							?>
							<div class="col-md-9 well">
								<div class="row">
									<div class="col-md-12">
										<label for="data_sikap_spiritual_huruf" class="control-label">Sikap Spiritual</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
										<div class="form-group">
											<label for="data_sikap_spiritual_huruf" class="control-label">Predikat</label>
											<div class="">
											<?php combobox('1d', array('A', 'B', 'C', 'D', 'E'), "data_sikap_spiritual_huruf", '', '', ($antar)?$antar->sikap_spiritual_huruf:'', '', '', 'class="form-control"'); ?>
											</div>		
										</div>
									</div>
									
									<div class="col-md-10">
										<div class="form-group">
											<label for="data_sikap_spiritual_deskripsi" class="control-label">Deskripsi</label>
											<div class="">
												<textarea class="form-control" rows="3" name="data_sikap_spiritual_deskripsi" id="data_sikap_spiritual_deskripsi" placeholder=""><?php echo ($antar)?$antar->sikap_spiritual_deskripsi:''; ?></textarea>
											</div>		
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<label for="data_sikap_sosial_huruf" class="control-label">Sikap Sosial</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
										<div class="form-group">
											<label for="data_sikap_sosial_huruf" class="control-label">Predikat</label>
											<div class="">
											<?php combobox('1d', array('A', 'B', 'C', 'D', 'E'), "data_sikap_sosial_huruf", '', '', ($antar)?$antar->sikap_sosial_huruf:'', '', '', 'class="form-control"'); ?>
											</div>		
										</div>
									</div>
									
									<div class="col-md-10">
										<div class="form-group">
											<label for="data_sikap_sosial_deskripsi" class="control-label">Deskripsi</label>
											<div class="">
												<textarea class="form-control" rows="3" name="data_sikap_sosial_deskripsi" id="data_sikap_sosial_deskripsi" placeholder=""><?php echo ($antar)?$antar->sikap_sosial_deskripsi:''; ?></textarea>
											</div>		
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<label for="" class="control-label">Prestasi</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<table class="table table-bordered table-report">
											<thead>
												<tr>
													<th width="10" style="vertical-align:middle">No</th>
													<th style="vertical-align:middle">Jenis Kegiatan</th>
													<th style="vertical-align:middle">Keterangan</th>
												</tr>
											</thead>
											<tbody>
											<?php for($i=1;$i<=4;$i++){ 
											$data_prestasi = ($antar)?json_decode($antar->prestasi, true):'';
											?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><input type="text" class="form-control" name="data_prestasi[<?php echo $i; ?>][kegiatan]" value="<?php echo ($data_prestasi)?$data_prestasi[$i]['kegiatan']:''; ?>"/></td>
													<td><input type="text" class="form-control" name="data_prestasi[<?php echo $i; ?>][keterangan]" value="<?php echo ($data_prestasi)?$data_prestasi[$i]['keterangan']:''; ?>"/></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<label for="" class="control-label">Ekstrakurikuler</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<table class="table table-bordered table-report">
											<thead>
												<tr>
													<th width="10" style="vertical-align:middle">No</th>
													<th style="vertical-align:middle">Kegiatan Ekstrakurikuler</th>
													<th style="vertical-align:middle">Keterangan</th>
													<th width="80" style="vertical-align:middle">Nilai</th>
												</tr>
											</thead>
											<tbody>
											<?php for($i=1;$i<=4;$i++){ 
											$data_ekstrakurikuler = ($antar)?json_decode($antar->ekstrakurikuler, true):'';
											?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><input type="text" class="form-control" name="data_ekstrakurikuler[<?php echo $i; ?>][kegiatan]" value="<?php echo ($data_ekstrakurikuler)?$data_ekstrakurikuler[$i]['kegiatan']:''; ?>"/></td>
													<td><input type="text" class="form-control" name="data_ekstrakurikuler[<?php echo $i; ?>][keterangan]" value="<?php echo ($data_ekstrakurikuler)?$data_ekstrakurikuler[$i]['keterangan']:''; ?>"/></td>
													<td><?php combobox('1d', array('A', 'B', 'C', 'D', 'E'), "data_ekstrakurikuler[$i][nilai]", '', '', ($data_ekstrakurikuler)?$data_ekstrakurikuler[$i]['nilai']:'', '', '', 'class="form-control"'); ?></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<label for="" class="control-label">Presensi</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<table class="table table-bordered table-report">
											<thead>
												<tr>
													<th style="vertical-align:middle">Ketidakhadiran</th>
													<th style="vertical-align:middle">Jumlah</th>
												</tr>
											</thead>
											<tbody>
											<?php
											$data_presensi = ($antar)?json_decode($antar->presensi, true):'';
											?>
												<tr>
													<td>Sakit</td>
													<td><div class="input-group"><input type="text" class="form-control" name="data_presensi[sakit]" value="<?php echo ($data_presensi)?$data_presensi['sakit']:''; ?>"/> <span class="input-group-addon">Hari</span></div></td>
												</tr>
												<tr>
													<td>Izin</td>
													<td><div class="input-group"><input type="text" class="form-control" name="data_presensi[izin]" value="<?php echo ($data_presensi)?$data_presensi['izin']:''; ?>"/> <span class="input-group-addon">Hari</span></div></td>
												</tr>
												<tr>
													<td>Tanpa Keterangan</td>
													<td><div class="input-group"><input type="text" class="form-control" name="data_presensi[tanpa_keterangan]" value="<?php echo ($data_presensi)?$data_presensi['tanpa_keterangan']:''; ?>"/> <span class="input-group-addon">Hari</span></div></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="data_catatan_walikelas" class="control-label">Catatan Wali Kelas</label>
											<div class="">
												<textarea class="form-control" rows="3" name="data_catatan_walikelas" id="data_catatan_walikelas" placeholder=""><?php echo ($antar)?$antar->catatan_walikelas:''; ?></textarea>
											</div>		
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="data_catatan_orangtua" class="control-label">Catatan Orang Tua</label>
											<div class="">
												<textarea class="form-control" rows="3" name="data_catatan_orangtua" id="data_catatan_orangtua" placeholder=""><?php echo ($antar)?$antar->catatan_orangtua:''; ?></textarea>
											</div>		
										</div>
									</div>
								</div>
								
							</div>
							<?php } ?>
						</div>
						
					</div><!-- /.box-body -->
					<?php } ?>
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Refresh</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>