<?php
if ($action == 'legger'){
$pelajaran_ 		= $this->db->query("SELECT pelajaran_sifat FROM mata_pelajaran WHERE pelajaran_id = '{$pelajaran_id}'")->row();
$pelajaran_sifat	= "Wajib";
if ($pelajaran_){
	$pelajaran_sifat = $pelajaran_->pelajaran_sifat;
} else if ($pelajaran_id == 'semua-pelajaran'){
	$pelajaran_sifat = 'semua-pelajaran';
}
?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Legger Semua Semester
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('laporan'); ?>">Laporan</a></li>
            <li class="active">Legger Semua Semester</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$tahun_kode.'/'.$semester_id.'/'.$staf_id.'/'.$pelajaran_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$komponen_id.'/'.$tampilkan);?>" method="post">
                <div class="box-header">
					<h3 class="box-title">Pilih Legger Nilai</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-body">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="semester_id" class="control-label">Semester</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tingkat</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tahun_kode = '{$tahun_kode}' GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '{$tahun_kode}' AND tingkat_id = '{$tingkat_id}' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="komponen_id" class="control-label">Nilai</label>
									<div class="">
										<?php $this->Komponen_pelajaran_model->combobox_komponen_pelajaran2('komponen_id', $komponen_id, 'submit();', '', 'class="form-control select2" required', $tahun_kode, $kelas_id); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="tampilkan" class="control-label">&nbsp;</label>
									<div class="">
										<button type="submit" name="tampilkan" class="btn btn-primary" value="true">Tampilkan</button>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<?php 
					$pelajaran_ 		= $this->db->query("SELECT pelajaran_sifat FROM mata_pelajaran WHERE pelajaran_id = '{$pelajaran_id}'")->row();
					$pelajaran_sifat	= "Wajib";
					if ($pelajaran_){
						$pelajaran_sifat= $pelajaran_->pelajaran_sifat;
					} else if ($pelajaran_id == 'semua-pelajaran'){
						$pelajaran_sifat = 'semua-pelajaran';
					}
					
					if ($pelajaran_sifat = 'semua-pelajaran') { ?>
						<?php 
						if ($kelas_id && $pelajaran_id && $komponen_id && $tampilkan && $kelas_id != '-' && $pelajaran_id != '-' && $komponen_id != '-' && $tampilkan != '-'){
						// MENGHITUNG SEMUA ASPEK PELAJARAN YANG AKTIF
						$query_pelajaran = $this->db->query("SELECT * FROM guru_pelajaran 
															LEFT JOIN aspek_pelajaran ON guru_pelajaran.pelajaran_id=aspek_pelajaran.pelajaran_id 
															WHERE guru_pelajaran.tahun_kode = '$tahun_kode'
																AND guru_pelajaran.kelas_id = '$kelas_id'
															GROUP BY aspek_pelajaran.pelajaran_id, aspek_pelajaran.aspek_id"); 
						$count_pelajaran = $query_pelajaran->num_rows();
						
						$query = $this->db->query("SELECT * FROM pengujian WHERE pengujian.tahun_kode = '$tahun_kode' AND pengujian.tingkat_id = '$tingkat_id' AND pengujian.semester_id = '$semester_id' AND pengujian.pelajaran_id='$pelajaran_id'");
						$jml_pengujian = $query->num_rows();
						$data_pengujian = $query->result();
						?>
						<div class="box-header" id="boxnilai">
							<h3 class="box-title">Daftar Siswa</h3>
							<hr />
						</div><!-- /.box-header -->
						<div class="box-body" style="overflow-x:auto">
							<table class="table table-bordered table-report">
								<thead>
									<tr>
										<th rowspan="3" width="10" style="vertical-align:middle">No</th>
										<th rowspan="3" width="200" style="vertical-align:middle">Siswa</th>
										<th colspan="<?php echo $count_pelajaran; ?>" style="text-align:center;width:2000">MATA PELAJARAN</th>
									</tr>
									<tr>
									<?php $grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
																			FROM guru_pelajaran 
																				LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																			WHERE guru_pelajaran_status = 'A' 
																				AND tahun_kode = '{$tahun_kode}' 
																				AND pelajaran_status = 'A' 
																				AND pelajaran_sifat = 'Wajib' 
																			GROUP BY pelajaran_kelompok 
																			ORDER BY pelajaran_kelompok ASC")->result();
																			
										foreach($grid_kelompok as $row_kelompok){
											if ($row_kelompok->pelajaran_peminatan){
												$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																					WHERE guru_pelajaran_status = 'A' 
																						AND tahun_kode = '{$tahun_kode}' 
																						AND pelajaran_status = 'A' 
																						AND pelajaran_sifat = 'Wajib' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						GROUP BY pelajaran_peminatan
																						ORDER BY peminatan.peminatan_urutan ASC")->result();
												$i_peminatan = 1;
												foreach($grid_peminatan as $row_peminatan){
													$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																					WHERE guru_pelajaran_status = 'A'  
																						AND tahun_kode = '{$tahun_kode}'
																						AND guru_pelajaran.kelas_id = '{$kelas_id}'
																						AND pelajaran_status = 'A' 
																						AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						AND pelajaran_sifat = 'Wajib' 
																					GROUP BY guru_pelajaran.pelajaran_id 
																					ORDER BY pelajaran_urutan ASC")->result();
													if ($grid_mapel){
														$i_mapel = 1;
														foreach($grid_mapel as $row_mapel){
															$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
															echo "<td colspan=\"$count_aspek\" style=\"text-align:center\" width=\"".(80 * $count_aspek)."\">{$row_mapel->pelajaran_nama}</td>";
															$i_mapel++;
														}
														$i_peminatan++;
													}
												}
											} else {
												$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																				FROM guru_pelajaran 
																					LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																					LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																				WHERE guru_pelajaran_status = 'A' 
																					AND tahun_kode = '{$tahun_kode}'
																					AND guru_pelajaran.kelas_id = '{$kelas_id}'
																					AND pelajaran_status = 'A' 
																					AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																					AND pelajaran_sifat = 'Wajib' 
																				GROUP BY guru_pelajaran.pelajaran_id 
																				ORDER BY pelajaran_urutan ASC")->result();
												$i_mapel = 1;
												foreach($grid_mapel as $row_mapel){
													$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
													echo "<td colspan=\"$count_aspek\" style=\"text-align:center\" width=\"".(80 * $count_aspek)."\">{$row_mapel->pelajaran_nama}</td>";
													$i_mapel++;
												}
											}
										}?>
									</tr>
									
									<tr>
									<?php $grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
																			FROM guru_pelajaran 
																				LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																			WHERE guru_pelajaran_status = 'A' 
																				AND tahun_kode = '{$tahun_kode}' 
																				AND pelajaran_status = 'A' 
																				AND pelajaran_sifat = 'Wajib' 
																			GROUP BY pelajaran_kelompok 
																			ORDER BY pelajaran_kelompok ASC")->result();
																			
										foreach($grid_kelompok as $row_kelompok){
											if ($row_kelompok->pelajaran_peminatan){
												$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																					WHERE guru_pelajaran_status = 'A' 
																						AND tahun_kode = '{$tahun_kode}' 
																						AND pelajaran_status = 'A' 
																						AND pelajaran_sifat = 'Wajib' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						GROUP BY pelajaran_peminatan
																						ORDER BY peminatan.peminatan_urutan ASC")->result();
												$i_peminatan = 1;
												foreach($grid_peminatan as $row_peminatan){
													$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																					WHERE guru_pelajaran_status = 'A'  
																						AND tahun_kode = '{$tahun_kode}'
																						AND guru_pelajaran.kelas_id = '{$kelas_id}'
																						AND pelajaran_status = 'A' 
																						AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						AND pelajaran_sifat = 'Wajib' 
																					GROUP BY guru_pelajaran.pelajaran_id 
																					ORDER BY pelajaran_urutan ASC")->result();
													if ($grid_mapel){
														$i_mapel = 1;
														foreach($grid_mapel as $row_mapel){
															$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
															if ($grid_aspek){
																foreach ($grid_aspek as $row2){
																	echo "<td style=\"text-align:center;white-space: nowrap;\" width=\"80\">{$row2->aspek_kode}</td>";
																}
															}
															$i_mapel++;
														}
														$i_peminatan++;
													}
												}
											} else {
												$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																				FROM guru_pelajaran 
																					LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																					LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																				WHERE guru_pelajaran_status = 'A' 
																					AND tahun_kode = '{$tahun_kode}'
																					AND guru_pelajaran.kelas_id = '{$kelas_id}'
																					AND pelajaran_status = 'A' 
																					AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																					AND pelajaran_sifat = 'Wajib' 
																				GROUP BY guru_pelajaran.pelajaran_id 
																				ORDER BY pelajaran_urutan ASC")->result();
												$i_mapel = 1;
												foreach($grid_mapel as $row_mapel){
													$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
													if ($grid_aspek){
														foreach ($grid_aspek as $row2){
															echo "<td style=\"text-align:center;white-space: nowrap;\" width=\"80\">{$row2->aspek_kode}</td>";
														}
													}
													$i_mapel++;
												}
											}
										}?>
									</tr>
								</thead>
								<tbody>
								<?php
								$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa_kelas.siswa_id ASC");
								$i = 1;
								if ($query->num_rows() > 0) {
									foreach($query->result() as $row) {
									?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><strong><?php echo $row->siswa_nis; ?></strong><br /><?php echo $row->siswa_nama; ?></td>
										<?php 
										$grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
																		FROM guru_pelajaran 
																			LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																		WHERE guru_pelajaran_status = 'A' 
																			AND tahun_kode = '{$tahun_kode}' 
																			AND pelajaran_status = 'A' 
																			AND pelajaran_sifat = 'Wajib' 
																		GROUP BY pelajaran_kelompok 
																		ORDER BY pelajaran_kelompok ASC")->result();
																		
										foreach($grid_kelompok as $row_kelompok){
											if ($row_kelompok->pelajaran_peminatan){
												$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																					WHERE guru_pelajaran_status = 'A' 
																						AND tahun_kode = '{$tahun_kode}' 
																						AND pelajaran_status = 'A' 
																						AND pelajaran_sifat = 'Wajib' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						GROUP BY pelajaran_peminatan
																						ORDER BY peminatan.peminatan_urutan ASC")->result();
												$i_peminatan = 1;
												foreach($grid_peminatan as $row_peminatan){
													$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																					WHERE guru_pelajaran_status = 'A'  
																						AND tahun_kode = '{$tahun_kode}'
																						AND guru_pelajaran.kelas_id = '{$kelas_id}'
																						AND pelajaran_status = 'A' 
																						AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						AND pelajaran_sifat = 'Wajib' 
																					GROUP BY guru_pelajaran.pelajaran_id 
																					ORDER BY pelajaran_urutan ASC")->result();
													if ($grid_mapel){
														$i_mapel = 1;
														foreach($grid_mapel as $row_mapel){
															$get_nilai_ki3 = $this->db->query("SELECT *
																						FROM nilai
																						WHERE nilai.siswa_id='{$row->siswa_id}'
																							AND nilai.aspek_id='1'
																							AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																							AND nilai.tahun_kode='{$tahun_kode}'
																							AND nilai.semester_id='{$semester_id}'
																							AND nilai.komponen_id='{$komponen_id}'
																							AND nilai.staf_id='{$row_mapel->staf_id}'
																							AND nilai.nilai_angka > 0")->row();
														
														$get_nilai_ki4 = $this->db->query("SELECT *
																						FROM nilai
																						WHERE nilai.siswa_id='{$row->siswa_id}'
																							AND nilai.aspek_id='2'
																							AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																							AND nilai.tahun_kode='{$tahun_kode}'
																							AND nilai.semester_id='{$semester_id}'
																							AND nilai.komponen_id='{$komponen_id}'
																							AND nilai.staf_id='{$row_mapel->staf_id}'
																							AND nilai.nilai_angka > 0")->row();
																							
														$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
														$nilai_angka_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_angka:'-';
														$nilai_huruf_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_huruf:'-';
														$nilai_angka_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_angka:'-';
														$nilai_huruf_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_huruf:'-';
														echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki3 . '/' . $nilai_huruf_ki3 ."</td>";
														echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki4 . '/' . $nilai_huruf_ki4 ."</td>";
															$i_mapel++;
														}
														$i_peminatan++;
													}
												}
											} else {
												$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																				FROM guru_pelajaran 
																					LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																					LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																				WHERE guru_pelajaran_status = 'A' 
																					AND tahun_kode = '{$tahun_kode}'
																					AND guru_pelajaran.kelas_id = '{$kelas_id}'
																					AND pelajaran_status = 'A' 
																					AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																					AND pelajaran_sifat = 'Wajib' 
																				GROUP BY guru_pelajaran.pelajaran_id 
																				ORDER BY pelajaran_urutan ASC")->result();
												$i_mapel = 1;
												foreach($grid_mapel as $row_mapel){
													$get_nilai_ki3 = $this->db->query("SELECT *
																					FROM nilai
																					WHERE nilai.siswa_id='{$row->siswa_id}'
																						AND nilai.aspek_id='1'
																						AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																						AND nilai.tahun_kode='{$tahun_kode}'
																						AND nilai.semester_id='{$semester_id}'
																						AND nilai.komponen_id='{$komponen_id}'
																						AND nilai.staf_id='{$row_mapel->staf_id}'
																						AND nilai.nilai_angka > 0")->row();
													
													$get_nilai_ki4 = $this->db->query("SELECT *
																					FROM nilai
																					WHERE nilai.siswa_id='{$row->siswa_id}'
																						AND nilai.aspek_id='2'
																						AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																						AND nilai.tahun_kode='{$tahun_kode}'
																						AND nilai.semester_id='{$semester_id}'
																						AND nilai.komponen_id='{$komponen_id}'
																						AND nilai.staf_id='{$row_mapel->staf_id}'
																						AND nilai.nilai_angka > 0")->row();
																						
													$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
													$nilai_angka_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_angka:'-';
													$nilai_huruf_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_huruf:'-';
													$nilai_angka_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_angka:'-';
													$nilai_huruf_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_huruf:'-';
													echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki3 . '/' . $nilai_huruf_ki3 ."</td>";
													echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki4 . '/' . $nilai_huruf_ki4 ."</td>";
													$i_mapel++;
												}
											}
										}?>
									</tr>
									<?php
									$i++;
									}
								}
								?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
						<?php } ?>
					<?php } ?>
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3)); ?>'" class="btn btn-default">Refresh</button>
						<?php if ($komponen_id && $komponen_id != '-' && $pelajaran_id == 'semua-pelajaran'){?>
						<a href="<?php echo module_url($this->uri->segment(2).'/export-legger-seluruh/'.$tahun_kode.'/'.$semester_id.'/'.$staf_id.'/'.$pelajaran_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$komponen_id); ?>" target="_blank" class="btn btn-primary"><span class="fa fa-download"></span> Export Legger Semua Semester</a>
						<?php } ?>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>