<?php
if ($action == 'legger'){
$pelajaran_ 		= $this->db->query("SELECT pelajaran_sifat FROM mata_pelajaran WHERE pelajaran_id = '{$pelajaran_id}'")->row();
$pelajaran_sifat	= "Wajib";
if ($pelajaran_){
	$pelajaran_sifat = $pelajaran_->pelajaran_sifat;
} else if ($pelajaran_id == 'semua-pelajaran'){
	$pelajaran_sifat = 'semua-pelajaran';
}
?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Legger Nilai
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('laporan'); ?>">Laporan</a></li>
            <li class="active">Legger Nilai</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$tahun_kode.'/'.$semester_id.'/'.$staf_id.'/'.$pelajaran_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$komponen_id.'/'.$tampilkan);?>" method="post">
                <div class="box-header">
					<h3 class="box-title">Pilih Legger Nilai</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<div class="box-body">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM tahun_ajaran ORDER BY tahun_nama DESC")->result(), 'tahun_kode', 'tahun_kode', 'tahun_nama', $tahun_kode, 'submit();', 'none', 'class="form-control select2" required');?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="semester_id" class="control-label">Semester</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM semester ORDER BY semester_nama ASC")->result(), 'semester_id', 'semester_id', 'semester_nama', $semester_id, 'submit();', 'none', 'class="form-control select2" required'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="staf_id" class="control-label">Guru</label>
									<div class="">
										<?php 
										if ($this->session->userdata('level') == 10){
											$this->Staf_model->combobox_guru('staf_id', $staf_id, 'submit();', 'none', 'class="form-control select2" required', true, $tahun_kode, userdata('staf_id'));
										} else {
											$this->Staf_model->combobox_guru('staf_id', $staf_id, 'submit();', '', 'class="form-control select2" required', true, $tahun_kode);
										}
										?>
									</div>		
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="pelajaran_id" class="control-label">Mata Pelajaran</label>
									<div class="">
										<?php $this->Mata_pelajaran_model->combobox_mata_pelajaran_guru3('pelajaran_id', $pelajaran_id, 'submit();', '', 'class="form-control select2" required', true, $staf_id); ?>
									</div>		
								</div>
							</div>
						</div>
						<div class="row">
							<?php if ($pelajaran_sifat == 'Wajib'){?>
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tingkat</label>
									<div class="">
										<?php $this->Tingkat_model->combobox_tingkat_guru_pelajaran('tingkat_id', $tingkat_id, 'submit();', '', 'class="form-control select2" required', true, $staf_id, $tahun_kode, $pelajaran_id); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php $this->Kelas_model->combobox_kelas_guru_pelajaran('kelas_id', $kelas_id, 'submit();', '', 'class="form-control select2" required', true, $staf_id, $tahun_kode, $tingkat_id, $pelajaran_id); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="komponen_id" class="control-label">Nilai</label>
									<div class="">
										<?php $this->Komponen_pelajaran_model->combobox_komponen_pelajaran('komponen_id', $komponen_id, 'submit();', '', 'class="form-control select2"', $staf_id, $tahun_kode, $pelajaran_id, $kelas_id); ?>
									</div>		
								</div>
							</div>
							<?php } else { ?>
							<div class="col-md-2">
								<div class="form-group">
									<label for="tingkat_id" class="control-label">Tingkat</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas LEFT JOIN tingkat ON kelas.tingkat_id=tingkat.tingkat_id WHERE tahun_kode = '{$tahun_kode}' AND staf_id = '{$staf_id}' GROUP BY tingkat.tingkat_id ORDER BY tingkat_nama ASC")->result(), 'tingkat_id', 'tingkat_id', 'tingkat_nama', $tingkat_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-2">
								<div class="form-group">
									<label for="kelas_id" class="control-label">Kelas</label>
									<div class="">
										<?php combobox('db', $this->db->query("SELECT * FROM kelas WHERE tahun_kode = '{$tahun_kode}' AND tingkat_id = '{$tingkat_id}' AND staf_id = '{$staf_id}' ORDER BY kelas_nama ASC")->result(), 'kelas_id', 'kelas_id', 'kelas_nama', $kelas_id, 'submit();', '', 'class="form-control select2"'); ?>
									</div>		
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="komponen_id" class="control-label">Nilai</label>
									<div class="">
										<?php $this->Komponen_pelajaran_model->combobox_komponen_pelajaran2('komponen_id', $komponen_id, 'submit();', '', 'class="form-control select2" required', $tahun_kode, $kelas_id); ?>
									</div>		
								</div>
							</div>
							<?php } ?>
							
							<div class="col-md-4">
								<div class="form-group">
									<label for="tampilkan" class="control-label">&nbsp;</label>
									<div class="">
										<button type="submit" name="tampilkan" class="btn btn-primary" value="true">Tampilkan</button>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<?php 
					$pelajaran_ 		= $this->db->query("SELECT pelajaran_sifat FROM mata_pelajaran WHERE pelajaran_id = '{$pelajaran_id}'")->row();
					$pelajaran_sifat	= "Wajib";
					if ($pelajaran_){
						$pelajaran_sifat= $pelajaran_->pelajaran_sifat;
					} else if ($pelajaran_id == 'semua-pelajaran'){
						$pelajaran_sifat = 'semua-pelajaran';
					}
					if ($pelajaran_sifat == 'Wajib'){
						
						if ($komponen_id && $komponen_id != '-'){
							$query_komponen = $this->db->query("SELECT * FROM nilai 
																LEFT JOIN komponen_penilaian ON nilai.komponen_id=komponen_penilaian.komponen_id 
																LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id 
																WHERE nilai.tahun_kode = '$tahun_kode'
																	AND nilai.pelajaran_id = '$pelajaran_id'
																	AND siswa_kelas.kelas_id = '$kelas_id'
																	AND nilai.komponen_id = '$komponen_id'
																GROUP BY nilai.komponen_id
																ORDER BY komponen_nama ASC"); 
							$grid_komponen = $query_komponen->result();
							$count_komponen = $query_komponen->num_rows();
						} else {
							$query_komponen = $this->db->query("SELECT * FROM nilai 
																LEFT JOIN komponen_penilaian ON nilai.komponen_id=komponen_penilaian.komponen_id 
																LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id 
																WHERE nilai.tahun_kode = '$tahun_kode'
																	AND nilai.pelajaran_id = '$pelajaran_id'
																	AND siswa_kelas.kelas_id = '$kelas_id'
																GROUP BY nilai.komponen_id
																ORDER BY komponen_nama ASC"); 
							$grid_komponen = $query_komponen->result();
							$count_komponen = $query_komponen->num_rows();
						}
						
						$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id));
						$count_aspek = ($this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$pelajaran_id)) * 2) + 1;
						
						$query = $this->db->query("SELECT * FROM pengujian WHERE pengujian.tahun_kode = '$tahun_kode' AND pengujian.tingkat_id = '$tingkat_id' AND pengujian.semester_id = '$semester_id' AND pengujian.pelajaran_id='$pelajaran_id'");
						$jml_pengujian = $query->num_rows();
						$data_pengujian = $query->result();
						
						if ($kelas_id && $pelajaran_id && $tampilkan && $kelas_id != '-' && $pelajaran_id != '-' && $tampilkan != '-'){
						?>
						<div class="box-header" id="boxnilai">
							<h3 class="box-title">Daftar Siswa</h3>
							<hr />
						</div><!-- /.box-header -->
						<div class="box-body" style="overflow-x:auto">
						<?php
							$q_aspek = $this->db->query("SELECT * 
														FROM aspek_pelajaran 
															LEFT JOIN aspek_penilaian ON aspek_pelajaran.aspek_id=aspek_penilaian.aspek_id
														WHERE aspek_pelajaran.tahun_kode = '{$tahun_kode}'
															AND aspek_pelajaran.aspek_id IN (1, 2)
															AND aspek_pelajaran_status = 'A'
															-- AND (SELECT COUNT(nilai_id) FROM nilai WHERE nilai.tahun_kode=aspek_pelajaran.tahun_kode AND nilai.aspek_id=aspek_pelajaran.aspek_id) > 0
														GROUP BY aspek_penilaian.aspek_id
														ORDER BY aspek_penilaian.aspek_kode ASC");
							$grid_aspek = $q_aspek->result();
							$count_aspek = $q_aspek->num_rows();

							$listAspek = array();
							$listKomponen = array();
							$listNilai = array();
							$iAspek = 0;
							$iKomponen = 0;
							$iNilai = 0;
							foreach ($grid_aspek as $row_aspek) {
							$listAspek[$iAspek]['aspek_id']		= $row_aspek->aspek_id;
							$listAspek[$iAspek]['aspek_kode']	= $row_aspek->aspek_kode;
							$listAspek[$iAspek]['aspek_nama']	= $row_aspek->aspek_nama;

							$nAspekColspan = 0;
							$q_komponen = $this->db->query("SELECT * 
															FROM komponen_pelajaran 
																LEFT JOIN komponen_penilaian ON komponen_pelajaran.komponen_id=komponen_penilaian.komponen_id
															WHERE komponen_pelajaran.tahun_kode = '{$tahun_kode}'
																AND komponen_pelajaran.semester_id = '{$semester_id}'
																AND komponen_pelajaran.tingkat_id = '{$tingkat_id}'
																AND komponen_pelajaran.aspek_id = '{$row_aspek->aspek_id}'
																AND komponen_pelajaran_status = 'A'
																-- AND (SELECT COUNT(nilai_id) 
																-- 		FROM nilai 
																-- 			LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id 
																-- 		WHERE nilai.tahun_kode = siswa_kelas.tahun_kode 
																-- 			AND nilai.siswa_id = siswa_kelas.siswa_id 
																-- 			AND nilai.tahun_kode=komponen_pelajaran.tahun_kode 
																-- 			AND nilai.semester_id=komponen_pelajaran.semester_id 
																-- 			AND siswa_kelas.kelas_id='{$kelas_id}' 
																-- 			AND nilai.aspek_id=komponen_pelajaran.aspek_id 
																-- 			AND nilai.komponen_id=komponen_pelajaran.komponen_id) > 0
															GROUP BY komponen_penilaian.komponen_id
															ORDER BY komponen_penilaian.komponen_urutan ASC");

							$grid_komponen = $q_komponen->result();
							$count_komponen = $q_komponen->num_rows();

							if ($count_komponen > 0){
							foreach ($grid_komponen as $row_komponen) {
								$listKomponen[$iKomponen]['komponen_id']			= $row_komponen->komponen_id;
								$listKomponen[$iKomponen]['komponen_kode']			= $row_komponen->komponen_kode;
								$listKomponen[$iKomponen]['komponen_nama']			= $row_komponen->komponen_nama;
								$listKomponen[$iKomponen]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
								$listKomponen[$iKomponen]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
								
								$nKomponenColspan = 0;
								$q_nilai = $this->db->query("SELECT * 
																	FROM nilai 
																		LEFT JOIN siswa_kelas ON nilai.siswa_id=siswa_kelas.siswa_id
																	WHERE nilai.tahun_kode = siswa_kelas.tahun_kode
																		AND nilai.siswa_id = siswa_kelas.siswa_id
																		and nilai.tahun_kode = '{$tahun_kode}'
																		AND nilai.semester_id = '{$semester_id}'
																		AND siswa_kelas.kelas_id = '{$kelas_id}'
																		AND nilai.aspek_id = '{$row_aspek->aspek_id}'
																		AND nilai.komponen_id = '{$row_komponen->komponen_id}'
																		AND nilai_akhir = 'N'
																	GROUP BY nilai.nilai_urutan
																	ORDER BY nilai.nilai_urutan ASC");

								$grid_nilai = $q_nilai->result();
								$count_nilai = $q_nilai->num_rows();

								if ($count_nilai){
									foreach ($grid_nilai as $row_nilai) {
										$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
										$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
										$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
										$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
										$listNilai[$iNilai]['nilai_urutan']			= $row_nilai->nilai_urutan;
										$listNilai[$iNilai]['kompetensi_id']		= $row_nilai->kompetensi_id;
										$listNilai[$iNilai]['nilai_kode']			= "VALUE";
										$iNilai++;
										$nAspekColspan++;
										$nKomponenColspan++;
									}
									if ($row_komponen->komponen_kompetensi == 'Y'){
										$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
										$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
										$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
										$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
										$listNilai[$iNilai]['nilai_urutan']			= "RATA<u>2</u>";
										$listNilai[$iNilai]['kompetensi_id']		= "0";
										$listNilai[$iNilai]['nilai_kode']			= "NA-K";
										$iNilai++;
										$nAspekColspan++;
										$nKomponenColspan++;
									}
								} else {
									$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
									$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
									$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
									$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
									$listNilai[$iNilai]['nilai_urutan']			= "NULL";
									$listNilai[$iNilai]['kompetensi_id']		= "0";
									$listNilai[$iNilai]['nilai_kode']			= "NULL";
									$iNilai++;
									$nAspekColspan++;
									$nKomponenColspan++;
								}
								$listKomponen[$iKomponen]['komponen_colspan']	= $nKomponenColspan;
								$iKomponen++;
							}
							if ($iKomponen > 0){
								$listKomponen[$iKomponen]['komponen_id']			= "NA";
								$listKomponen[$iKomponen]['komponen_kode']			= "NA";
								$listKomponen[$iKomponen]['komponen_nama']			= "NA";
								$listKomponen[$iKomponen]['komponen_persentase']	= "NA";
								$listKomponen[$iKomponen]['komponen_colspan']		= 1;
								$iKomponen++;

								$listNilai[$iNilai]['komponen_persentase']	= $row_komponen->komponen_pelajaran_persentase;
								$listNilai[$iNilai]['komponen_kompetensi']	= $row_komponen->komponen_kompetensi;
								$listNilai[$iNilai]['komponen_id']			= $row_komponen->komponen_id;
								$listNilai[$iNilai]['aspek_id']				= $row_aspek->aspek_id;
								$listNilai[$iNilai]['nilai_urutan']			= "NA";
								$listNilai[$iNilai]['kompetensi_id']		= "0";
								$listNilai[$iNilai]['nilai_kode']			= "NA-A";
								$iNilai++;
							}
							} else {
							$listKomponen[$iKomponen]['komponen_id']			= "NULL";
							$listKomponen[$iKomponen]['komponen_kode']			= "NULL";
							$listKomponen[$iKomponen]['komponen_nama']			= "NULL";
							$listKomponen[$iKomponen]['komponen_persentase']	= "NULL";
							$listKomponen[$iKomponen]['komponen_colspan']		= 1;
							$iKomponen++;
							}

							$listAspek[$iAspek]['aspek_colspan']	= $nAspekColspan + 1;
							$iAspek++;
							}
							?>
							<table class="table table-bordered table-report">
								<thead>
									<tr>
										<th rowspan="3" width="10" style="vertical-align:middle">No</th>
										<th rowspan="3" width="150" style="vertical-align:middle">Siswa</th>
										<th rowspan="3" width="55" style="vertical-align:middle;text-align:center">KKM</th>
										<?php if ($listAspek){ 
											foreach ($listAspek as $row_aspek){
												echo "<th style=\"text-align:center\" colspan=\"".$row_aspek['aspek_colspan']."\">".$row_aspek['aspek_nama']."</th>";
											}
										}?>
									</tr>
									<tr>
										<?php if ($listKomponen){ 
											foreach ($listKomponen as $row_komponen){
												if ($row_komponen['komponen_id'] == 'NA' || $row_komponen['komponen_colspan'] == '1' || $row_komponen['komponen_kompetensi'] == 'N'){
													echo "<th style=\"text-align:center;vertical-align:middle;\" width=\"60\" rowspan=\"2\" colspan=\"".$row_komponen['komponen_colspan']."\">".$row_komponen['komponen_kode']."</th>";
												} else {
													echo "<th style=\"text-align:center;\" colspan=\"".$row_komponen['komponen_colspan']."\">".$row_komponen['komponen_kode']."</th>";
												}
											}
										}?>
									</tr>
									<tr>
										<?php if ($listNilai){ 
											foreach ($listNilai as $row_nilai){
												if (($row_nilai['nilai_kode'] == 'VALUE' && $row_nilai['komponen_kompetensi'] == 'Y') || ($row_nilai['nilai_kode'] == 'NA-K' && $row_nilai['komponen_kompetensi'] == 'Y')){
													echo "<th style=\"text-align:center\" width=\"60\">".$row_nilai['nilai_urutan']."</th>";
												}
											}
										}?>
									</tr>
								</thead>
								<tbody>
								<?php								
									$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa_kelas.siswa_id ASC");
									$i = 1;
									if ($query->num_rows() > 0) {
										foreach($query->result() as $row) {
											$where = array();
											$where['nilai.siswa_id']		= $row->siswa_id;
											$where['nilai.pelajaran_id']	= $pelajaran_id;
											$where['nilai.tahun_kode']		= $tahun_kode;
											$where['nilai.semester_id']		= $semester_id;
											$where['siswa_kelas.kelas_id']	= $kelas_id;
											$nilai = $this->Nilai_model->get_nilai("*", $where);
											if ($nilai){
												$nilai_kkm		= $nilai->nilai_kkm;
											} else {
												$nilai_kkm		= '';
											}
										?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><strong><?php echo $row->siswa_nis; ?></strong><br /><?php echo $row->siswa_nama; ?></td>
											<td style="text-align:center;"><?php echo $nilai_kkm; ?></td>
											<?php 
											$NA_A = 0;
											$NA_K = 0;
											$n_k = 0;
											$n_a = 0;
											
											$deskripsi_tuntas = array();
											$deskripsi_lampaui = array();
											$deskripsi_belum = array();

											if ($listNilai){
												foreach ($listNilai as $row2){
													$where = array();
													$where['nilai.siswa_id']		= $row->siswa_id;
													$where['nilai.pelajaran_id']	= $pelajaran_id;
													$where['nilai.tahun_kode']		= $tahun_kode;
													$where['nilai.semester_id']		= $semester_id;
													$where['nilai.staf_id']			= $staf_id;
													$where['nilai.aspek_id']		= $row2['aspek_id'];
													$where['nilai.komponen_id']		= $row2['komponen_id'];
													$where['nilai.nilai_urutan']	= $row2['nilai_urutan'];
													$nilai = $this->Nilai_model->get_nilai("*", $where);

													if ($nilai && $row2['komponen_kompetensi'] == "Y" && $row2['nilai_kode'] == "VALUE"){
														$NA_K = $NA_K + $nilai->nilai_angka;
														$n_k++;
														echo "<td width=\"55\" style=\"text-align:center;\">".$nilai->nilai_angka."</td>";
													} else if ($row2['komponen_kompetensi'] == "N" && $row2['nilai_kode'] == "VALUE"){
														$NA_A = $NA_A + ($nilai->nilai_angka * ($row2['komponen_persentase'] / 100));
														$n_a++;
														echo "<td width=\"60\" style=\"text-align:center;\">".$nilai->nilai_angka."</td>";
													} else if ($row2['nilai_kode'] == "NA-K"){
														$NA_K = ($n_k > 0 && $NA_K > 0)?$NA_K / $n_k:'-';
														$NA_A = $NA_A + ($NA_K * ($row2['komponen_persentase'] / 100));
														echo "<td width=\"60\" style=\"text-align:center;\">".$NA_K."</td>";
														$n_a++;
														$n_k = 0;
														$NA_K = 0;
													} else if ($row2['nilai_kode'] == "NA-A"){
														//NILAI
														$whereDB = array();
														$whereDB['nilai.siswa_id']		= $row->siswa_id;
														$whereDB['nilai.pelajaran_id']	= $pelajaran_id;
														$whereDB['nilai.tahun_kode']		= $tahun_kode;
														$whereDB['nilai.semester_id']	= $semester_id;
														$whereDB['nilai.staf_id']		= $staf_id;
														$whereDB['nilai.aspek_id']		= $row2['aspek_id'];
														$whereDB['nilai.nilai_akhir']	= 'Y';
														$whereDB['nilai.komponen_id']	= null;
														$whereDB['nilai.nilai_urutan']	= null;
														$nilaiDB = $this->Nilai_model->get_nilai("nilai_angka", $whereDB);

														if ($nilaiDB && $gen == 0){
															$NA_A = $nilaiDB->nilai_angka;
														} else {
															$NA_A = ($n_a > 0 && $NA_A > 0)?round($NA_A, 2):'-';
														}
														
														echo "<td width=\"60\">".$NA_A."</td>";
														$NA_A = 0;
													} else {
														echo "<td width=\"60\" style=\"text-align:center;\">-</td>";
													}
												}
											}
											?>
										</tr>
										<?php
										$i++;
										}
									}
								?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
						<?php } ?>
					<?php } else if ($pelajaran_sifat = 'semua-pelajaran') { ?>
						<?php 
						if ($kelas_id && $pelajaran_id && $komponen_id && $tampilkan && $kelas_id != '-' && $pelajaran_id != '-' && $komponen_id != '-' && $tampilkan != '-'){
						// MENGHITUNG SEMUA ASPEK PELAJARAN YANG AKTIF
						$query_pelajaran = $this->db->query("SELECT * FROM guru_pelajaran 
															LEFT JOIN aspek_pelajaran ON guru_pelajaran.pelajaran_id=aspek_pelajaran.pelajaran_id 
															WHERE guru_pelajaran.tahun_kode = '$tahun_kode'
																AND guru_pelajaran.kelas_id = '$kelas_id'
															GROUP BY aspek_pelajaran.pelajaran_id, aspek_pelajaran.aspek_id"); 
						$count_pelajaran = $query_pelajaran->num_rows();
						
						$query = $this->db->query("SELECT * FROM pengujian WHERE pengujian.tahun_kode = '$tahun_kode' AND pengujian.tingkat_id = '$tingkat_id' AND pengujian.semester_id = '$semester_id' AND pengujian.pelajaran_id='$pelajaran_id'");
						$jml_pengujian = $query->num_rows();
						$data_pengujian = $query->result();
						?>
						<div class="box-header" id="boxnilai">
							<h3 class="box-title">Daftar Siswa</h3>
							<hr />
						</div><!-- /.box-header -->
						<div class="box-body" style="overflow-x:auto">
							<table class="table table-bordered table-report">
								<thead>
									<tr>
										<th rowspan="3" width="10" style="vertical-align:middle">No</th>
										<th rowspan="3" width="200" style="vertical-align:middle">Siswa</th>
										<th colspan="<?php echo $count_pelajaran; ?>" style="text-align:center;width:2000">MATA PELAJARAN</th>
									</tr>
									<tr>
									<?php $grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
																			FROM guru_pelajaran 
																				LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																			WHERE guru_pelajaran_status = 'A' 
																				AND tahun_kode = '{$tahun_kode}' 
																				AND pelajaran_status = 'A' 
																				AND pelajaran_sifat = 'Wajib' 
																			GROUP BY pelajaran_kelompok 
																			ORDER BY pelajaran_kelompok ASC")->result();
																			
										foreach($grid_kelompok as $row_kelompok){
											if ($row_kelompok->pelajaran_peminatan){
												$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																					WHERE guru_pelajaran_status = 'A' 
																						AND tahun_kode = '{$tahun_kode}' 
																						AND pelajaran_status = 'A' 
																						AND pelajaran_sifat = 'Wajib' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						GROUP BY pelajaran_peminatan
																						ORDER BY peminatan.peminatan_urutan ASC")->result();
												$i_peminatan = 1;
												foreach($grid_peminatan as $row_peminatan){
													$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																					WHERE guru_pelajaran_status = 'A'  
																						AND tahun_kode = '{$tahun_kode}'
																						AND guru_pelajaran.kelas_id = '{$kelas_id}'
																						AND pelajaran_status = 'A' 
																						AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						AND pelajaran_sifat = 'Wajib' 
																					GROUP BY guru_pelajaran.pelajaran_id 
																					ORDER BY pelajaran_urutan ASC")->result();
													if ($grid_mapel){
														$i_mapel = 1;
														foreach($grid_mapel as $row_mapel){
															$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
															echo "<td colspan=\"$count_aspek\" style=\"text-align:center\" width=\"".(80 * $count_aspek)."\">{$row_mapel->pelajaran_nama}</td>";
															$i_mapel++;
														}
														$i_peminatan++;
													}
												}
											} else {
												$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																				FROM guru_pelajaran 
																					LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																					LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																				WHERE guru_pelajaran_status = 'A' 
																					AND tahun_kode = '{$tahun_kode}'
																					AND guru_pelajaran.kelas_id = '{$kelas_id}'
																					AND pelajaran_status = 'A' 
																					AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																					AND pelajaran_sifat = 'Wajib' 
																				GROUP BY guru_pelajaran.pelajaran_id 
																				ORDER BY pelajaran_urutan ASC")->result();
												$i_mapel = 1;
												foreach($grid_mapel as $row_mapel){
													$count_aspek = $this->Aspek_pelajaran_model->count_all_aspek_pelajaran(array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
													echo "<td colspan=\"$count_aspek\" style=\"text-align:center\" width=\"".(80 * $count_aspek)."\">{$row_mapel->pelajaran_nama}</td>";
													$i_mapel++;
												}
											}
										}?>
									</tr>
									
									<tr>
									<?php $grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
																			FROM guru_pelajaran 
																				LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																			WHERE guru_pelajaran_status = 'A' 
																				AND tahun_kode = '{$tahun_kode}' 
																				AND pelajaran_status = 'A' 
																				AND pelajaran_sifat = 'Wajib' 
																			GROUP BY pelajaran_kelompok 
																			ORDER BY pelajaran_kelompok ASC")->result();
																			
										foreach($grid_kelompok as $row_kelompok){
											if ($row_kelompok->pelajaran_peminatan){
												$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																					WHERE guru_pelajaran_status = 'A' 
																						AND tahun_kode = '{$tahun_kode}' 
																						AND pelajaran_status = 'A' 
																						AND pelajaran_sifat = 'Wajib' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						GROUP BY pelajaran_peminatan
																						ORDER BY peminatan.peminatan_urutan ASC")->result();
												$i_peminatan = 1;
												foreach($grid_peminatan as $row_peminatan){
													$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																					WHERE guru_pelajaran_status = 'A'  
																						AND tahun_kode = '{$tahun_kode}'
																						AND guru_pelajaran.kelas_id = '{$kelas_id}'
																						AND pelajaran_status = 'A' 
																						AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						AND pelajaran_sifat = 'Wajib' 
																					GROUP BY guru_pelajaran.pelajaran_id 
																					ORDER BY pelajaran_urutan ASC")->result();
													if ($grid_mapel){
														$i_mapel = 1;
														foreach($grid_mapel as $row_mapel){
															$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
															if ($grid_aspek){
																foreach ($grid_aspek as $row2){
																	echo "<td style=\"text-align:center;white-space: nowrap;\" width=\"80\">{$row2->aspek_kode}</td>";
																}
															}
															$i_mapel++;
														}
														$i_peminatan++;
													}
												}
											} else {
												$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																				FROM guru_pelajaran 
																					LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																					LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																				WHERE guru_pelajaran_status = 'A' 
																					AND tahun_kode = '{$tahun_kode}'
																					AND guru_pelajaran.kelas_id = '{$kelas_id}'
																					AND pelajaran_status = 'A' 
																					AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																					AND pelajaran_sifat = 'Wajib' 
																				GROUP BY guru_pelajaran.pelajaran_id 
																				ORDER BY pelajaran_urutan ASC")->result();
												$i_mapel = 1;
												foreach($grid_mapel as $row_mapel){
													$grid_aspek = $this->Aspek_pelajaran_model->grid_all_aspek_pelajaran("", "aspek_kode", "ASC", 0, 0, array('aspek_status'=>'A', 'aspek_pelajaran.tahun_kode'=>$tahun_kode, 'aspek_pelajaran.pelajaran_id'=>$row_mapel->pelajaran_id));
													if ($grid_aspek){
														foreach ($grid_aspek as $row2){
															echo "<td style=\"text-align:center;white-space: nowrap;\" width=\"80\">{$row2->aspek_kode}</td>";
														}
													}
													$i_mapel++;
												}
											}
										}?>
									</tr>
								</thead>
								<tbody>
								<?php
								$query = $this->db->query("SELECT * FROM siswa_kelas LEFT JOIN siswa ON siswa_kelas.siswa_id=siswa.siswa_id WHERE siswa_kelas.kelas_id = '$kelas_id' AND siswa_kelas.tahun_kode = '$tahun_kode' ORDER BY siswa_kelas.siswa_id ASC");
								$i = 1;
								if ($query->num_rows() > 0) {
									foreach($query->result() as $row) {
									?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><strong><?php echo $row->siswa_nis; ?></strong><br /><?php echo $row->siswa_nama; ?></td>
										<?php 
										$grid_kelompok = $this->db->query("SELECT pelajaran_kelompok, pelajaran_peminatan 
																		FROM guru_pelajaran 
																			LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																		WHERE guru_pelajaran_status = 'A' 
																			AND tahun_kode = '{$tahun_kode}' 
																			AND pelajaran_status = 'A' 
																			AND pelajaran_sifat = 'Wajib' 
																		GROUP BY pelajaran_kelompok 
																		ORDER BY pelajaran_kelompok ASC")->result();
																		
										foreach($grid_kelompok as $row_kelompok){
											if ($row_kelompok->pelajaran_peminatan){
												$grid_peminatan = $this->db->query("SELECT pelajaran_peminatan 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN peminatan ON mata_pelajaran.peminatan_id=peminatan.peminatan_id 
																					WHERE guru_pelajaran_status = 'A' 
																						AND tahun_kode = '{$tahun_kode}' 
																						AND pelajaran_status = 'A' 
																						AND pelajaran_sifat = 'Wajib' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						GROUP BY pelajaran_peminatan
																						ORDER BY peminatan.peminatan_urutan ASC")->result();
												$i_peminatan = 1;
												foreach($grid_peminatan as $row_peminatan){
													$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																					FROM guru_pelajaran 
																						LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																						LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																					WHERE guru_pelajaran_status = 'A'  
																						AND tahun_kode = '{$tahun_kode}'
																						AND guru_pelajaran.kelas_id = '{$kelas_id}'
																						AND pelajaran_status = 'A' 
																						AND pelajaran_peminatan = '{$row_peminatan->pelajaran_peminatan}' 
																						AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																						AND pelajaran_sifat = 'Wajib' 
																					GROUP BY guru_pelajaran.pelajaran_id 
																					ORDER BY pelajaran_urutan ASC")->result();
													if ($grid_mapel){
														$i_mapel = 1;
														foreach($grid_mapel as $row_mapel){
															$get_nilai_ki3 = $this->db->query("SELECT *
																						FROM nilai
																						WHERE nilai.siswa_id='{$row->siswa_id}'
																							AND nilai.aspek_id='1'
																							AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																							AND nilai.tahun_kode='{$tahun_kode}'
																							AND nilai.semester_id='{$semester_id}'
																							AND nilai.komponen_id='{$komponen_id}'
																							AND nilai.staf_id='{$row_mapel->staf_id}'
																							AND nilai.nilai_angka > 0")->row();
														
														$get_nilai_ki4 = $this->db->query("SELECT *
																						FROM nilai
																						WHERE nilai.siswa_id='{$row->siswa_id}'
																							AND nilai.aspek_id='2'
																							AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																							AND nilai.tahun_kode='{$tahun_kode}'
																							AND nilai.semester_id='{$semester_id}'
																							AND nilai.komponen_id='{$komponen_id}'
																							AND nilai.staf_id='{$row_mapel->staf_id}'
																							AND nilai.nilai_angka > 0")->row();
																							
														$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
														$nilai_angka_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_angka:'-';
														$nilai_huruf_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_huruf:'-';
														$nilai_angka_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_angka:'-';
														$nilai_huruf_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_huruf:'-';
														echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki3 . '/' . $nilai_huruf_ki3 ."</td>";
														echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki4 . '/' . $nilai_huruf_ki4 ."</td>";
															$i_mapel++;
														}
														$i_peminatan++;
													}
												}
											} else {
												$grid_mapel = $this->db->query("SELECT guru_pelajaran.pelajaran_id, pelajaran_nama, staf.staf_id, staf.staf_nama 
																				FROM guru_pelajaran 
																					LEFT JOIN mata_pelajaran ON guru_pelajaran.pelajaran_id=mata_pelajaran.pelajaran_id 
																					LEFT JOIN staf ON guru_pelajaran.staf_id=staf.staf_id 
																				WHERE guru_pelajaran_status = 'A' 
																					AND tahun_kode = '{$tahun_kode}'
																					AND guru_pelajaran.kelas_id = '{$kelas_id}'
																					AND pelajaran_status = 'A' 
																					AND pelajaran_kelompok = '{$row_kelompok->pelajaran_kelompok}' 
																					AND pelajaran_sifat = 'Wajib' 
																				GROUP BY guru_pelajaran.pelajaran_id 
																				ORDER BY pelajaran_urutan ASC")->result();
												$i_mapel = 1;
												foreach($grid_mapel as $row_mapel){
													$get_nilai_ki3 = $this->db->query("SELECT *
																					FROM nilai
																					WHERE nilai.siswa_id='{$row->siswa_id}'
																						AND nilai.aspek_id='1'
																						AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																						AND nilai.tahun_kode='{$tahun_kode}'
																						AND nilai.semester_id='{$semester_id}'
																						AND nilai.komponen_id='{$komponen_id}'
																						AND nilai.staf_id='{$row_mapel->staf_id}'
																						AND nilai.nilai_angka > 0")->row();
													
													$get_nilai_ki4 = $this->db->query("SELECT *
																					FROM nilai
																					WHERE nilai.siswa_id='{$row->siswa_id}'
																						AND nilai.aspek_id='2'
																						AND nilai.pelajaran_id='{$row_mapel->pelajaran_id}'
																						AND nilai.tahun_kode='{$tahun_kode}'
																						AND nilai.semester_id='{$semester_id}'
																						AND nilai.komponen_id='{$komponen_id}'
																						AND nilai.staf_id='{$row_mapel->staf_id}'
																						AND nilai.nilai_angka > 0")->row();
																						
													$nilai_kkm  	 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_kkm:'-';
													$nilai_angka_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_angka:'-';
													$nilai_huruf_ki3 = ($get_nilai_ki3)?$get_nilai_ki3->nilai_huruf:'-';
													$nilai_angka_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_angka:'-';
													$nilai_huruf_ki4 = ($get_nilai_ki4)?$get_nilai_ki4->nilai_huruf:'-';
													echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki3 . '/' . $nilai_huruf_ki3 ."</td>";
													echo "<td style=\"text-align:center\" width=\"80\">".$nilai_angka_ki4 . '/' . $nilai_huruf_ki4 ."</td>";
													$i_mapel++;
												}
											}
										}?>
									</tr>
									<?php
									$i++;
									}
								}
								?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
						<?php } ?>
					<?php } ?>
					<div class="box-footer">
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3)); ?>'" class="btn btn-default">Refresh</button>
						<?php if ($komponen_id && $komponen_id != '-'){?>
						<a href="<?php echo module_url($this->uri->segment(2).'/export-legger-pelajaran/'.$tahun_kode.'/'.$semester_id.'/'.$staf_id.'/'.$pelajaran_id.'/'.$tingkat_id.'/'.$kelas_id.'/'.$komponen_id); ?>" target="_blank" class="btn btn-primary"><span class="fa fa-download"></span> Export Legger ke Excel</a>
						<?php } ?>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>