<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Biodata Dosen | ' . profile('profil_website');
		$this->active_menu		= '380';
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Datatable_model');
		$this->load->model('Referensi_model');
		$this->load->model('Dosen_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Dosen_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
	}
	
	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$data['tabs']		= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['dosen_id']	= userdata('dosen_id'); 
		$dosen		 		= $this->Dosen_model->get_dosen('*', $where);
		if (!$dosen){
			redirect(site_url());
			exit();
		}
		if ($this->session->userdata('level') != 10){
			redirect(site_url());
			exit();
		}
		
		$data['dosen_id'] = $dosen->dosen_id;
		$data['perguruan_tinggi_id'] = $dosen->perguruan_tinggi_id;
		$data['program_studi_id'] = $dosen->program_studi_id;
		$data['jenjang_kode'] = $dosen->jenjang_kode;
		$data['dosen_ktp'] = $dosen->dosen_ktp;
		$data['dosen_nidn'] = $dosen->dosen_nidn;
		$data['dosen_nama'] = $dosen->dosen_nama;
		$data['dosen_gelar'] = $dosen->dosen_gelar;
		$data['dosen_tempat_lahir'] = $dosen->dosen_tempat_lahir;
		$data['dosen_tanggal_lahir'] = $dosen->dosen_tanggal_lahir;
		$data['dosen_jenis_kelamin'] = $dosen->dosen_jenis_kelamin;
		$data['dosen_semester_mulai'] = $dosen->dosen_semester_mulai;
		$data['dosen_nip_pns'] = $dosen->dosen_nip_pns;
		$data['dosen_homebase'] = $dosen->dosen_homebase;
		$data['jabatan_akademik_kode'] = $dosen->jabatan_akademik_kode;
		$data['pendidikan_kode'] = $dosen->pendidikan_kode;
		$data['status_ikatan_kerja_kode'] = $dosen->status_ikatan_kerja_kode;
		$data['status_aktivitas_kode'] = $dosen->status_aktivitas_kode;
		$data['dosen_user'] = $dosen->dosen_user;
		$data['dosen_pin'] = $dosen->dosen_pin;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/biodata', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['dosen_id']		= userdata('dosen_id'); 
		$dosen 					= $this->Dosen_model->get_dosen('*', $where);

		$data['dosen_id'] = ($this->input->post('dosen_id'))?$this->input->post('dosen_id'):$dosen->dosen_id;
		$data['perguruan_tinggi_id'] = ($this->input->post('perguruan_tinggi_id'))?$this->input->post('perguruan_tinggi_id'):$dosen->perguruan_tinggi_id;
		$data['program_studi_id'] = ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$dosen->program_studi_id;
		$data['jenjang_kode'] = ($this->input->post('jenjang_kode'))?$this->input->post('jenjang_kode'):$dosen->jenjang_kode;
		$data['dosen_ktp'] = ($this->input->post('dosen_ktp'))?$this->input->post('dosen_ktp'):$dosen->dosen_ktp;
		$data['dosen_nidn'] = ($this->input->post('dosen_nidn'))?$this->input->post('dosen_nidn'):$dosen->dosen_nidn;
		$data['dosen_nama'] = ($this->input->post('dosen_nama'))?$this->input->post('dosen_nama'):$dosen->dosen_nama;
		$data['dosen_gelar'] = ($this->input->post('dosen_gelar'))?$this->input->post('dosen_gelar'):$dosen->dosen_gelar;
		$data['dosen_tempat_lahir'] = ($this->input->post('dosen_tempat_lahir'))?$this->input->post('dosen_tempat_lahir'):$dosen->dosen_tempat_lahir;
		$data['dosen_tanggal_lahir'] = ($this->input->post('dosen_tanggal_lahir'))?$this->input->post('dosen_tanggal_lahir'):$dosen->dosen_tanggal_lahir;
		$data['dosen_jenis_kelamin'] = ($this->input->post('dosen_jenis_kelamin'))?$this->input->post('dosen_jenis_kelamin'):$dosen->dosen_jenis_kelamin;
		$data['dosen_semester_mulai'] = ($this->input->post('dosen_semester_mulai'))?$this->input->post('dosen_semester_mulai'):$dosen->dosen_semester_mulai;
		$data['dosen_nip_pns'] = ($this->input->post('dosen_nip_pns'))?$this->input->post('dosen_nip_pns'):$dosen->dosen_nip_pns;
		$data['dosen_homebase'] = ($this->input->post('dosen_homebase'))?$this->input->post('dosen_homebase'):$dosen->dosen_homebase;
		$data['jabatan_akademik_kode'] = ($this->input->post('jabatan_akademik_kode'))?$this->input->post('jabatan_akademik_kode'):$dosen->jabatan_akademik_kode;
		$data['pendidikan_kode'] = ($this->input->post('pendidikan_kode'))?$this->input->post('pendidikan_kode'):$dosen->pendidikan_kode;
		$data['status_ikatan_kerja_kode'] = ($this->input->post('status_ikatan_kerja_kode'))?$this->input->post('status_ikatan_kerja_kode'):$dosen->status_ikatan_kerja_kode;
		$data['status_aktivitas_kode'] = ($this->input->post('status_aktivitas_kode'))?$this->input->post('status_aktivitas_kode'):$dosen->status_aktivitas_kode;
		$data['dosen_user'] = ($this->input->post('dosen_user'))?$this->input->post('dosen_user'):$dosen->dosen_user;
		$data['dosen_pin'] = ($this->input->post('dosen_pin'))?$this->input->post('dosen_pin'):$dosen->dosen_pin;
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			if ($this->Pengguna_model->count_all_pengguna("pengguna_nama = '".validasi_sql($this->input->post('dosen_user'))."' && pengguna_nama != '".$dosen->dosen_user."'") < 1 && 
				$this->Dosen_model->count_all_dosen("dosen_user = '".validasi_sql($this->input->post('dosen_user'))."' && dosen_user != '".$dosen->dosen_user."'") < 1){
				
				$where_update['dosen_id']	= validasi_sql($this->input->post('dosen_id'));
				$update['dosen_ktp'] = validasi_input($this->input->post('dosen_ktp'));
				$update['dosen_nidn'] = validasi_input($this->input->post('dosen_nidn'));
				$update['dosen_nama'] = validasi_input($this->input->post('dosen_nama'));
				$update['dosen_gelar'] = validasi_input($this->input->post('dosen_gelar'));
				$update['dosen_tempat_lahir'] = validasi_input($this->input->post('dosen_tempat_lahir'));
				$update['dosen_tanggal_lahir'] = validasi_input($this->input->post('dosen_tanggal_lahir'));
				$update['dosen_jenis_kelamin'] = validasi_input($this->input->post('dosen_jenis_kelamin'));
				$update['dosen_nip_pns'] = validasi_input($this->input->post('dosen_nip_pns'));
				$update['dosen_homebase'] = validasi_input($this->input->post('dosen_homebase'));
				$update['jabatan_akademik_kode'] = validasi_input($this->input->post('jabatan_akademik_kode'));
				$update['pendidikan_kode'] = validasi_input($this->input->post('pendidikan_kode'));
				$update['status_ikatan_kerja_kode'] = validasi_input($this->input->post('status_ikatan_kerja_kode'));
				$update['status_aktivitas_kode'] = validasi_input($this->input->post('status_aktivitas_kode'));
				$this->Dosen_model->update_dosen($where_update, $update);
				
				$dosen_id		= validasi_sql($this->input->post('dosen_id'));
				$dosen_nama		= validasi_sql($this->input->post('dosen_nama'));
				$dosen_user		= validasi_sql($this->input->post('dosen_user'));
				$dosen_pin		= validasi_sql($this->input->post('dosen_pin'));

				$where_pengguna = array();
				$where_pengguna['pengguna.pengguna_nama']		= $dosen->dosen_user;
				$where_pengguna['pengguna.pengguna_level_id']	= 10;
				if ($this->Pengguna_model->count_all_pengguna($where_pengguna) > 0){
					$update_pengguna = array();
					$update_pengguna['pengguna_nama_depan']	= $dosen_nama;
					if ($dosen_pin != 'CHANGED' && $dosen_pin != $dosen->dosen_pin && $dosen_pin && strlen($dosen_pin) >= 6){
						$update_pengguna['pengguna_kunci']	= hash_password($dosen_pin);

						$this->Dosen_model->update_dosen(array('dosen_id'=>$dosen_id), array('dosen_pin'=>$dosen_pin));
					}
					if ($dosen->dosen_user != $dosen_user){
						$update_pengguna['pengguna_nama']	= $dosen_user;
					}
					$this->Pengguna_model->update_pengguna(array('pengguna_nama'=>$dosen->dosen_user, 'pengguna_level_id'=>10), $update_pengguna);
				}
				
				$this->session->set_flashdata('success','Dosen telah berhasil diubah.');
				redirect(module_url($this->uri->segment(2)));
			} else {
				$this->session->set_flashdata('error','User telah digunakan. Silahkan gunakan user yang lain.');
				redirect(module_url($this->uri->segment(2)));
			}
			
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/biodata', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function upload_foto()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'upload_foto';
		
		$data['dataExcel']				= array();		
		$data['filename']				= '';
		
		if ($this->input->post('uploadFoto')){
			$total = count($_FILES['fileFoto']['name']);
			$path		= "./asset/uploads/foto-dosen/";
			$mkdir		= true;
			if(file_exists($path) && is_dir($path)){
			} else {
				if (mkdir($path, 0777, true)){
				} else {
					$mkdir = false;
				}
			}
			for($i=0; $i<$total; $i++) {
				$name		= $_FILES['fileFoto']['name'][$i];
				$size		= $_FILES['fileFoto']['size'][$i];
				$basename 	= pathinfo($name);

				$where_update['dosen_user']	= $basename['filename'];
				if ($this->Dosen_model->count_all_dosen($where_update) > 0){
					if ($mkdir){
						$newname	= filename_seo($name);
						$fullname	= $path . '/' . filename_seo($name);
						$tmp = $_FILES['fileFoto']['tmp_name'][$i];
						if ($tmp){
							if (move_uploaded_file($tmp, $fullname)){
								$this->Dosen_model->update_dosen($where_update, array('dosen_foto'=>$basename['basename']));
							}
						}
					}
				}
			}
			$this->session->set_flashdata('success','Materi telah berhasil ditambah.');
			redirect(current_url());
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/biodata', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	// aktivitas
	public function datatable_aktivitas()
    {
		$dosen_id = userdata('dosen_id');
		$this->Datatable_model->set_table("(SELECT dosen_mengajar.*, matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, semester.semester_nama FROM akd_dosen_mengajar dosen_mengajar LEFT JOIN akd_matakuliah matakuliah ON dosen_mengajar.matakuliah_id=matakuliah.matakuliah_id LEFT JOIN akd_semester semester ON dosen_mengajar.semester_kode=semester.semester_kode WHERE dosen_mengajar.dosen_id = '$dosen_id') dosen_mengajar");
		$this->Datatable_model->set_column_order(array('semester_nama', 'matakuliah_kode', 'matakuliah_nama', 'dosen_mengajar_rencana_pertemuan', 'dosen_mengajar_realisasi_pertemuan', 'dosen_mengajar_kelas', null));
		$this->Datatable_model->set_column_search(array('semester_nama', 'matakuliah_kode', 'matakuliah_nama', 'dosen_mengajar_rencana_pertemuan', 'dosen_mengajar_realisasi_pertemuan', 'dosen_mengajar_kelas'));
		$this->Datatable_model->set_order(array('semester_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
            $no++;
            $row = array();
            $row['nomor'] = $no;
            $row['matakuliah_id'] = $record->matakuliah_id;
            $row['matakuliah_kode'] = $record->matakuliah_kode;
            $row['matakuliah_nama'] = $record->matakuliah_nama;
            $row['semester_nama'] = $record->semester_nama;
            $row['dosen_mengajar_rencana_pertemuan'] = $record->dosen_mengajar_rencana_pertemuan;
            $row['dosen_mengajar_realisasi_pertemuan'] = $record->dosen_mengajar_realisasi_pertemuan;
            $row['dosen_mengajar_kelas'] = $record->dosen_mengajar_kelas;
            $row['dosen_mengajar_id'] = $record->dosen_mengajar_id;
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}

	public function delete_aktivitas()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$dosen_mengajar_id = $this->uri->segment(4);
		$dosen_mengajar = $this->Dosen_model->get_dosen_mengajar('dosen_mengajar.*', array('dosen_mengajar.dosen_mengajar_id'=>$dosen_mengajar_id));

		if ($this->Matakuliah_model->count_all_matakuliah(array('matakuliah.semester_kode'=>$dosen_mengajar->semester_kode, 'matakuliah.dosen_id'=>$dosen_mengajar->dosen_id)) < 1){
			$this->Dosen_model->delete_dosen_mengajar(array('dosen_mengajar_id'=>validasi_sql($dosen_mengajar_id)));
			
			$this->session->set_flashdata('success','Data telah berhasil dihapus.');
			redirect(module_url($this->uri->segment(2).'/detail/'.$dosen_mengajar->dosen_id.'?tabs=1'));
		} else {
			$this->session->set_flashdata('success','Data gagal dihapus.');
			redirect(module_url($this->uri->segment(2).'/detail/'.$dosen_mengajar->dosen_id.'?tabs=1'));
		}

	}
}
