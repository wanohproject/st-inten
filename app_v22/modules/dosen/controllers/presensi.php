<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	public $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Presensi Dosen | ' . profile('profil_website');
		$this->active_menu		= 381;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Datatable_model');
		$this->load->model('Staf_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Kelas_model');
		$this->load->model('Siswa_kelas_model');
		$this->load->model('Siswa_model');
		$this->load->model('Departemen_model');
		
		$this->load->model('presensi/Presensi_model');
		$this->load->model('presensi/Absensi_model');
		$this->load->model('presensi/Mesin_model');
		$this->load->model('presensi/Jadwal_model');
		$this->load->model('presensi/Libur_model');
		
		$this->tahun_kode = $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
        $data['action']		= 'grid';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		if (in_array(userdata('pengguna_level_id'), array(11, 15))){
			$where_siswa_kelas['siswa_kelas.siswa_id']	= userdata('siswa_id');
			$where_siswa_kelas['siswa_kelas.tahun_kode']	= $this->tahun_kode;
			$get_siswa 									= $this->Siswa_kelas_model->get_siswa_kelas("", $where_siswa_kelas);
			$departemen_id			= ($this->uri->segment(4))?$this->uri->segment(4):userdata('departemen_id');
			$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
			$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
			$tingkat_id				= ($this->uri->segment(7))?$this->uri->segment(7):$get_siswa->tingkat_id;
			$kelas_id				= ($this->uri->segment(8))?$this->uri->segment(8):$get_siswa->kelas_id;
			$siswa_id				= ($this->uri->segment(9))?$this->uri->segment(9):userdata('siswa_id');

			$data['departemen_id']	= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):userdata('departemen_id');
			$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
			$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
			$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$get_siswa->tingkat_id;
			$data['kelas_id']		= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):$get_siswa->kelas_id;
			$data['siswa_id']		= ($this->input->post('siswa_id'))?$this->input->post('siswa_id'):userdata('siswa_id');
			$data['show']			= ($this->input->post('show'))?$this->input->post('show'):'';
		} else {
			if (userdata('departemen_id')){
				$data['departemen_id']		= userdata('departemen_id');
			} else {
				$departemen_id 				= ($this->uri->segment(4))?$this->uri->segment(4):0;
				$data['departemen_id']		= ($this->input->post('departemen_id'))?$this->input->post('departemen_id'):$departemen_id;
			}
			
			$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):$this->tahun_kode;
			$semester_id			= ($this->uri->segment(6))?$this->uri->segment(6):$semester_id;
			$tingkat_id				= ($this->uri->segment(7))?$this->uri->segment(7):'-';
			$kelas_id				= ($this->uri->segment(8))?$this->uri->segment(8):'-';
			$siswa_id				= ($this->uri->segment(9))?$this->uri->segment(9):'-';

			$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$this->tahun_kode;
			$data['semester_id']	= ($this->input->post('semester_id'))?$this->input->post('semester_id'):$semester_id;
			$data['tingkat_id']		= ($this->input->post('tingkat_id'))?$this->input->post('tingkat_id'):$tingkat_id;
			$data['kelas_id']		= ($this->input->post('kelas_id'))?$this->input->post('kelas_id'):$kelas_id;
			$data['siswa_id']		= ($this->input->post('siswa_id'))?$this->input->post('siswa_id'):$siswa_id;
			$data['show']			= ($this->input->post('show'))?$this->input->post('show'):'';
		}

		$data['presensi_tanggal_awal']		= ($this->input->post('presensi_tanggal_awal'))?$this->input->post('presensi_tanggal_awal'):date('Y-m-d', strtotime(date("Y-m-d")." -1 months "));
		$data['presensi_tanggal_akhir']		= ($this->input->post('presensi_tanggal_akhir'))?$this->input->post('presensi_tanggal_akhir'):date('Y-m-d');

		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;

		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/presensi', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function cetak()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;

		if (in_array(userdata('pengguna_level_id'), array(11, 15))){
			$where_siswa_kelas['siswa_kelas.siswa_id']	= userdata('siswa_id');
			$where_siswa_kelas['siswa_kelas.tahun_kode']	= $this->tahun_kode;
			$get_siswa 									= $this->Siswa_kelas_model->get_siswa_kelas("", $where_siswa_kelas);
			
			$data['departemen_id']			= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):userdata('departemen_id');
			$data['tahun_kode']				= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$this->tahun_kode;
			$data['semester_id']			= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):$semester_id;
			$data['tingkat_id']				= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):$get_siswa->tingkat_id;
			$data['kelas_id']				= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):$get_siswa->kelas_id;
			$data['siswa_id']				= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):userdata('siswa_id');
			$data['presensi_tanggal_awal']	= ($this->uri->segment(10))?validasi_sql($this->uri->segment(10)):date('Y-m-d');
			$data['presensi_tanggal_akhir']	= ($this->uri->segment(11))?validasi_sql($this->uri->segment(11)):date('Y-m-d');
		} else {
			$data['departemen_id']			= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):'-';
			$data['tahun_kode']				= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$this->tahun_kode;
			$data['semester_id']			= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):$semester_id;
			$data['tingkat_id']				= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
			$data['kelas_id']				= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):'-';
			$data['siswa_id']				= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):'-';
			$data['presensi_tanggal_awal']	= ($this->uri->segment(10))?validasi_sql($this->uri->segment(10)):date('Y-m-d');
			$data['presensi_tanggal_akhir']	= ($this->uri->segment(11))?validasi_sql($this->uri->segment(11)):date('Y-m-d');
		}
		
		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;
		
		$data['kepala_sekolah']	= $this->Staf_model->get_staf('*', array('staf_id'=>profile('kepala_sekolah')));
		$this->load->view(module_dir().'/export/siswa_rekap_presensi_print', $data);
	}
	
	public function word()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']				= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']			= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$data['tingkat_id']				= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):'-';
		$data['kelas_id']				= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
		$data['siswa_id']				= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):'-';
		$data['presensi_tanggal_awal']	= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):date('Y-m-d');
		$data['presensi_tanggal_akhir']	= ($this->uri->segment(10))?validasi_sql($this->uri->segment(10)):date('Y-m-d');
		
		$get_kelas 				= $this->Kelas_model->get_kelas("", array("kelas_id"=>$data['kelas_id']));
		$where_siswa_kelas['siswa_kelas.siswa_id']	= $data['siswa_id'];
		$where_siswa_kelas['siswa_kelas.tahun_kode']	= $data['tahun_kode'];
		$where_siswa_kelas['siswa_kelas.kelas_id']	= $data['kelas_id'];
		$get_siswa 									= $this->Siswa_kelas_model->get_siswa_kelas("", $where_siswa_kelas);
		
		$data['siswa']			= $get_siswa;
		$data['kelas']			= $get_kelas;

		$siswa_foto = ($data['siswa'])?$data['siswa']->siswa_foto:'';
		$data['foto'] = ($siswa_foto && file_exists('./asset/foto-siswa/'.$siswa_foto))?base_url('asset/foto-siswa/'.$siswa_foto):base_url('asset/profil/users.jpg');

		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;

		$sidik_jari	= $this->db->query("SELECT * FROM sat_sidik_jari sidik_jari LEFT JOIN siswa ON sidik_jari.sidik_jari_user=siswa.siswa_user WHERE siswa.siswa_id = '".$data['siswa_id']."'")->row();

		$data['sidik_jari_id'] = ($sidik_jari)?$sidik_jari->sidik_jari_id:'';
		$data['finger_id'] = ($sidik_jari)?$sidik_jari->finger_id:'';
		$data['mesin_id'] = ($sidik_jari)?$sidik_jari->mesin_id:'';
		$data['user_id'] = ($sidik_jari)?$sidik_jari->user_id:'';
		$data['sidik_jari_user'] = ($sidik_jari)?$sidik_jari->sidik_jari_user:'';
		$data['sidik_jari_nama'] = ($sidik_jari)?$sidik_jari->sidik_jari_nama:'';
		$data['sidik_jari_level'] = ($sidik_jari)?$sidik_jari->sidik_jari_level:'Dosen';
		$data['sidik_jari_ukuran'] = ($sidik_jari)?$sidik_jari->sidik_jari_ukuran:'';
		$data['sidik_jari_valid'] = ($sidik_jari)?$sidik_jari->sidik_jari_valid:'';
		$data['sidik_jari_file'] = ($sidik_jari)?$sidik_jari->sidik_jari_file:'';
		$data['sidik_jari_sms'] = ($sidik_jari)?$sidik_jari->sidik_jari_sms:'N';
		$data['sidik_jari_tanggal'] = ($sidik_jari)?$sidik_jari->sidik_jari_tanggal:'';
		$data['siswa_id'] = ($sidik_jari)?$sidik_jari->siswa_id:'';
		$data['siswa_user'] = ($sidik_jari)?$sidik_jari->siswa_user:'';
		
		$data['nama_sekolah']	= profile('profil_institusi');
		$data['alamat_sekolah']	= profile('profil_kontak');
		
		$trim_kelas = explode(" ", $get_kelas->kelas_nama);
		$trim_semester = $data['semester_id'];
		$data['new_semester'] = "";
		if (trim($trim_kelas[0]) == "VII" && $trim_semester == 1){
			$data['new_semester'] = "1 (Satu)";
		} else if (trim($trim_kelas[0]) == "VII" && $trim_semester == 2){
			$data['new_semester'] = "2 (Dua)";
		} else if (trim($trim_kelas[0]) == "VIII" && $trim_semester == 1){
			$data['new_semester'] = "3 (Tiga)";
		} else if (trim($trim_kelas[0]) == "VIII" && $trim_semester == 2){
			$data['new_semester'] = "4 (Empat)";
		} else if (trim($trim_kelas[0]) == "IX" && $trim_semester == 1){
			$data['new_semester'] = "5 (Lima)";
		} else if (trim($trim_kelas[0]) == "IX" && $trim_semester == 2){
			$data['new_semester'] = "6 (Enam)";
		}
		
		$data['kepala_sekolah']	= $this->Staf_model->get_staf('*', array('staf_id'=>profile('kepala_sekolah')));
		$this->load->view(module_dir().'/export/siswa_rekap_presensi_word', $data);
	}

	public function word_kelas()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']				= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']			= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$data['tingkat_id']				= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):'-';
		$data['kelas_id']				= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
		$data['presensi_tanggal_awal']	= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):date('Y-m-d');
		$data['presensi_tanggal_akhir']	= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):date('Y-m-d');
		
		$get_kelas 				= $this->Kelas_model->get_kelas("", array("kelas_id"=>$data['kelas_id']));
		$data['kelas']			= $get_kelas;

		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;
		
		$data['nama_sekolah']	= profile('profil_institusi');
		$data['alamat_sekolah']	= profile('profil_kontak');
		
		$trim_kelas = explode(" ", $get_kelas->kelas_nama);
		$trim_semester = $data['semester_id'];
		$data['new_semester'] = "";
		if (trim($trim_kelas[0]) == "VII" && $trim_semester == 1){
			$data['new_semester'] = "1 (Satu)";
		} else if (trim($trim_kelas[0]) == "VII" && $trim_semester == 2){
			$data['new_semester'] = "2 (Dua)";
		} else if (trim($trim_kelas[0]) == "VIII" && $trim_semester == 1){
			$data['new_semester'] = "3 (Tiga)";
		} else if (trim($trim_kelas[0]) == "VIII" && $trim_semester == 2){
			$data['new_semester'] = "4 (Empat)";
		} else if (trim($trim_kelas[0]) == "IX" && $trim_semester == 1){
			$data['new_semester'] = "5 (Lima)";
		} else if (trim($trim_kelas[0]) == "IX" && $trim_semester == 2){
			$data['new_semester'] = "6 (Enam)";
		}
		
		$data['kepala_sekolah']	= $this->Staf_model->get_staf('*', array('staf_id'=>profile('kepala_sekolah')));
		$this->load->view(module_dir().'/export/siswa_rekap_presensi_word_kelas', $data);
	}

	public function excel_kelas()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'print';
		
		$semester 				= $this->Semester_model->get_semester("semester_id, semester_nama", array("semester_status"=>"A"));
		$semester_id			= $semester->semester_id;
		
		$data['tahun_kode']				= ($this->uri->segment(4))?validasi_sql($this->uri->segment(4)):$this->tahun_kode;
		$data['semester_id']			= ($this->uri->segment(5))?validasi_sql($this->uri->segment(5)):$semester_id;
		$data['tingkat_id']				= ($this->uri->segment(6))?validasi_sql($this->uri->segment(6)):'-';
		$data['kelas_id']				= ($this->uri->segment(7))?validasi_sql($this->uri->segment(7)):'-';
		$data['presensi_tanggal_awal']	= ($this->uri->segment(8))?validasi_sql($this->uri->segment(8)):date('Y-m-d');
		$data['presensi_tanggal_akhir']	= ($this->uri->segment(9))?validasi_sql($this->uri->segment(9)):date('Y-m-d');
		
		$get_kelas 				= $this->Kelas_model->get_kelas("", array("kelas_id"=>$data['kelas_id']));
		$data['kelas']			= $get_kelas;

		$presensi_tanggal_awal = strtotime($data['presensi_tanggal_awal']);
		$presensi_tanggal_akhir = strtotime($data['presensi_tanggal_akhir']);
		$datediff = $presensi_tanggal_akhir - $presensi_tanggal_awal;
		$data['days'] = round($datediff / (60 * 60 * 24)) + 1;
		
		$data['nama_sekolah']	= profile('profil_institusi');
		$data['alamat_sekolah']	= profile('profil_kontak');
		
		$trim_kelas = explode(" ", $get_kelas->kelas_nama);
		$trim_semester = $data['semester_id'];
		$data['new_semester'] = "";
		if (trim($trim_kelas[0]) == "VII" && $trim_semester == 1){
			$data['new_semester'] = "1 (Satu)";
		} else if (trim($trim_kelas[0]) == "VII" && $trim_semester == 2){
			$data['new_semester'] = "2 (Dua)";
		} else if (trim($trim_kelas[0]) == "VIII" && $trim_semester == 1){
			$data['new_semester'] = "3 (Tiga)";
		} else if (trim($trim_kelas[0]) == "VIII" && $trim_semester == 2){
			$data['new_semester'] = "4 (Empat)";
		} else if (trim($trim_kelas[0]) == "IX" && $trim_semester == 1){
			$data['new_semester'] = "5 (Lima)";
		} else if (trim($trim_kelas[0]) == "IX" && $trim_semester == 2){
			$data['new_semester'] = "6 (Enam)";
		}
		$data['sidik_jari_level'] = 'Dosen';
		$data['excel']			= true;
		
		$data['kepala_sekolah']	= $this->Staf_model->get_staf('*', array('staf_id'=>profile('kepala_sekolah')));
		$this->load->view(module_dir().'/export/siswa_rekap_presensi_excel_kelas', $data);
	}
}
