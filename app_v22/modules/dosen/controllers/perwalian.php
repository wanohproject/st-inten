<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Perwalian extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $semester_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Perwalian | ' . profile('profil_website');
		$this->active_menu		= 260;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Datatable_model');
		$this->load->model('Referensi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Matakuliah_model');
		$this->load->model('Perwalian_model');
		$this->load->model('Dosen_model');
		
		$this->semester_kode = $this->Semester_model->get_semester_aktif()->semester_kode;
    }

	public function datatable()
    {
		$dosen_id 	= userdata('dosen_id');
		$where = "(mahasiswa.mahasiswa_tanggal_lulus IS NULL OR mahasiswa.mahasiswa_tanggal_lulus = '0000-00-00') AND mahasiswa.mahasiswa_dosen_wali = '$dosen_id'";

		$this->Datatable_model->set_table("(SELECT mahasiswa.* FROM akd_mahasiswa mahasiswa WHERE $where) dosen_mengajar");
		$this->Datatable_model->set_column_order(array('mahasiswa_tahun_masuk', 'mahasiswa_nim', 'mahasiswa_nama', null, null));
		$this->Datatable_model->set_column_search(array('mahasiswa_tahun_masuk', 'mahasiswa_nim', 'mahasiswa_nama'));
		$this->Datatable_model->set_order(array('mahasiswa_nama', 'asc'));
        $list = $this->Datatable_model->get_datatables();		
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $record) {
			$no++;
            $row = array();
            $row['nomor'] = $no;
            $row['mahasiswa_id'] = $record->mahasiswa_id;
            $row['mahasiswa_nama'] = $record->mahasiswa_nama;
            $row['mahasiswa_nim'] = $record->mahasiswa_nim;
			$row['mahasiswa_tahun_masuk'] = $record->mahasiswa_tahun_masuk;
			
			$get_perwalian = $this->Perwalian_model->get_perwalian("", array('perwalian.semester_kode'=>$this->semester_kode, 'perwalian.mahasiswa_id'=>$record->mahasiswa_id, 'perwalian.perwalian_jenis'=>'RS'));
			if (settings_get_value('rencana_studi_active') == 'RS'){
				$row['status_perwalian'] = "<span class=\"label label-danger\">BELUM SUBMIT</span>";
				if ($get_perwalian){
					if ($get_perwalian->perwalian_submit_status == 'Y' && $get_perwalian->approve_wali_status == 'Y'){
						$row['status_perwalian'] = "<span class=\"label label-success\">SUDAH DISETUJUI</span>";
					} else if ($get_perwalian->perwalian_submit_status == 'Y' && $get_perwalian->approve_wali_status == 'N'){
						$row['status_perwalian'] = "<span class=\"label label-warning\">BELUM DISETUJUI</span>";
					}
				} else {
					$row['status_perwalian'] = "<span class=\"label label-danger\">BELUM REGISTRASI</span>";
				}
			} else if (settings_get_value('rencana_studi_active') == 'PRS'){
				$get_prs = $this->Perwalian_model->get_perwalian("", array('perwalian.semester_kode'=>$this->semester_kode, 'perwalian.mahasiswa_id'=>$record->mahasiswa_id, 'perwalian.perwalian_jenis'=>'PRS'));
				$row['status_perwalian'] = "<span class=\"label label-danger\">BELUM SUBMIT</span>";
				if ($get_prs){
					if ($get_prs->perwalian_submit_status == 'Y' && $get_prs->approve_wali_status == 'Y'){
						$row['status_perwalian'] = "<span class=\"label label-success\">SUDAH DISETUJUI</span>";
					} else if ($get_prs->perwalian_submit_status == 'Y' && $get_prs->approve_wali_status == 'N'){
						$row['status_perwalian'] = "<span class=\"label label-warning\">BELUM DISETUJUI</span>";
					}
				} else if ($get_perwalian) {
					if ($get_prs->perwalian_submit_status == 'Y' && $get_prs->approve_wali_status == 'Y'){
						$row['status_perwalian'] = "<span class=\"label label-success\">SUDAH DISETUJUI</span>";
					} else if ($get_prs->perwalian_submit_status == 'Y' && $get_prs->approve_wali_status == 'N'){
						$row['status_perwalian'] = "<span class=\"label label-warning\">BELUM DISETUJUI</span>";
					}
				} else {
					$row['status_perwalian'] = "<span class=\"label label-danger\">BELUM REGISTRASI</span>";
				}
			} else {
				$row['status_perwalian'] = "<span class=\"label label-danger\">BELUM SUBMIT</span>";
				if ($get_perwalian){
					if ($get_perwalian->perwalian_submit_status == 'Y' && $get_perwalian->approve_wali_status == 'Y'){
						$row['status_perwalian'] = "<span class=\"label label-success\">SUDAH DISETUJUI</span>";
					} else if ($get_perwalian->perwalian_submit_status == 'Y' && $get_perwalian->approve_wali_status == 'N'){
						$row['status_perwalian'] = "<span class=\"label label-warning\">BELUM DISETUJUI</span>";
					}
				} else {
					$row['status_perwalian'] = "<span class=\"label label-danger\">BELUM REGISTRASI</span>";
				}
			}
            $row['Actions'] = $this->get_buttons($record->mahasiswa_id);
            $data[] = $row;
        }
 
        $output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->Datatable_model->count_all()),
			"recordsFiltered" => intval($this->Datatable_model->count_filtered()),
			"data" => $data,
        );
		
		header('Content-Type: application/json');
        echo json_encode($output, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
	}
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-success btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-plus"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/perwalian', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';

		
		$data['tabs']				= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['mahasiswa_id']	= validasi_sql($this->uri->segment(4)); 
		$data['mahasiswa'] = $this->Mahasiswa_model->get_mahasiswa('mahasiswa.*, program_studi_nama', $where);
		if (!$data['mahasiswa']){
			redirect(module_url($this->uri->segment(2)));
		}

		$semester = $this->Semester_model->get_semester_aktif();

		$data['semester_kode'] 			= $this->semester_kode; 
		$data['mahasiswa_id'] 			= $data['mahasiswa']->mahasiswa_id; 
		$data['mahasiswa_nim'] 			= $data['mahasiswa']->mahasiswa_nim; 
		$data['mahasiswa_nama'] 		= $data['mahasiswa']->mahasiswa_nama; 
		$data['mahasiswa_tahun_masuk'] 	= $data['mahasiswa']->mahasiswa_tahun_masuk; 
		$data['program_studi_id'] 		= $data['mahasiswa']->program_studi_id; 
		$data['program_studi_nama'] 	= $data['mahasiswa']->program_studi_nama; 
		$data['semester_nama'] 			= $semester->semester_nama; 

		$data['rencanastudi']	= new stdClass();
		$data['kuliah']				= new stdClass();
		if (settings_get_value('rencana_studi_active') == 'RS' || settings_get_value('rencana_studi_active') == 'PRS'){
			$data['rencanastudi'] = $this->Perwalian_model->get_perwalian('perwalian.*, mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, perwalian.semester_kode, program_studi_nama, semester_nama', array('perwalian.mahasiswa_id'=>$data['mahasiswa_id'], 'perwalian.semester_kode'=>$this->semester_kode, 'perwalian.perwalian_jenis'=>'RS', 'perwalian.perwalian_status'=>'A'));
			$perwalian_prs = $this->Perwalian_model->get_perwalian('perwalian.*, mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, perwalian.semester_kode, program_studi_nama, semester_nama', array('perwalian.mahasiswa_id'=>$data['mahasiswa_id'], 'perwalian.semester_kode'=>$this->semester_kode, 'perwalian.perwalian_jenis'=>'PRS', 'perwalian.perwalian_status'=>'A'));
			if (settings_get_value('rencana_studi_active') == 'PRS' && $perwalian_prs){
				$data['rencanastudi'] = $perwalian_prs;
			}
		} else {
			$data['rencanastudi'] = $this->Perwalian_model->get_perwalian('perwalian.*, mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, perwalian.semester_kode, program_studi_nama, semester_nama', array('perwalian.mahasiswa_id'=>$data['mahasiswa_id'], 'perwalian.semester_kode'=>$this->semester_kode, 'perwalian.perwalian_status'=>'A', 'perwalian.approve_akademik_status'=>'Y'));
			$data['kuliah'] 		= $this->Mahasiswa_model->get_mahasiswa_kuliah('mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, mahasiswa_kuliah.semester_kode', array('mahasiswa_kuliah.mahasiswa_id'=>$data['mahasiswa_id'], 'mahasiswa_kuliah.semester_kode'=>$this->semester_kode));
		}

		$data['dosen_id'] = userdata('dosen_id');
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/perwalian', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function add_matakuliah()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add_matakuliah';

		$data['tabs']			= ($this->input->get('tabs'))?$this->input->get('tabs'):0;
		
		$where['perwalian_id']	= validasi_sql($this->uri->segment(4)); 
		$data['perwalian'] = $this->Perwalian_model->get_perwalian('perwalian.*, mahasiswa.mahasiswa_nama, mahasiswa.mahasiswa_nim, mahasiswa.mahasiswa_tahun_masuk, perwalian.semester_kode, program_studi_nama, semester_nama', $where);
		if (!$data['perwalian']){
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/perwalian', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function save_krs(){
		$data = array();
		$action = $this->input->post('action');
		$matakuliah = $this->input->post('matakuliah');
		$mahasiswa = $this->input->post('mahasiswa');
		$perwalian = $this->input->post('perwalian');
		$perwalian = $this->Perwalian_model->get_perwalian("perwalian.*", array('perwalian_id'=>$perwalian));

		if ($action == 'add' && $perwalian && $matakuliah){
			$insert['perwalian_matakuliah_id'] = $this->uuid->v4();
			$insert['perguruan_tinggi_id'] = $perwalian->perguruan_tinggi_id;
			$insert['program_studi_id'] = $perwalian->program_studi_id;
			$insert['jenjang_kode'] = $perwalian->jenjang_kode;
			$insert['semester_kode'] = $perwalian->semester_kode;
			$insert['perwalian_id'] = $perwalian->perwalian_id;
			$insert['matakuliah_id'] = $matakuliah;
			$insert['mahasiswa_id'] = $mahasiswa;
			$insert['created_by'] = userdata('pengguna_id');
			$res_insert = $this->Perwalian_model->insert_perwalian_matakuliah($insert);

			if ($res_insert){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['status']		= "add";
				$data['data']		= $res_insert;
			} else {
				$data['response']	= false;
				$data['status']		= "add";
				$data['message']	= "User telah digunakan ada.";
			}
		} else if ($action == 'delete' && $perwalian && $matakuliah){
			$where['perwalian_id'] = $perwalian->perwalian_id;
			$where['matakuliah_id'] = $matakuliah;
			$res_delete = $this->Perwalian_model->delete_perwalian_matakuliah($where);

			if ($res_delete){
				$data['response']	= true;
				$data['message']	= "Data sukses";
				$data['status']		= "delete";
				$data['data']		= $res_delete;
			} else {
				$data['response']	= false;
				$data['status']		= "delete";
				$data['message']	= "User telah digunakan ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function delete_krs()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$perwalian_matakuliah_id = $this->uri->segment(4);
		$perwalian_matakuliah = $this->Perwalian_model->get_perwalian_matakuliah('perwalian_matakuliah.*', array('perwalian_matakuliah.perwalian_matakuliah_id'=>$perwalian_matakuliah_id));

		$this->Perwalian_model->delete_perwalian_matakuliah(array('perwalian_matakuliah_id'=>validasi_sql($perwalian_matakuliah_id)));
		
		$this->session->set_flashdata('success','Data telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2).'/detail/'.$perwalian_matakuliah->perwalian_id.'?tabs=1'));
	}

	public function save_catatan(){
		$data = array();
		$mahasiswa_id = $this->input->post('mahasiswa');
		$dosen_id = $this->input->post('dosen');
		$program_studi_id = $this->input->post('program_studi');
		$semester_kode = $this->input->post('semester');
		$catatan = $this->input->post('catatan');
		
		if ($mahasiswa_id && $program_studi_id && $semester_kode && $dosen_id){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$semester = $this->Semester_model->get_semester("*", array('semester_kode'=>$semester_kode));
			$mahasiswa = $this->Mahasiswa_model->get_mahasiswa("*", array('mahasiswa_id'=>$mahasiswa_id));
			$dosen = $this->Dosen_model->get_dosen("*", array('dosen_id'=>$dosen_id));

			if ($program_studi && $semester && $mahasiswa && $dosen){
				$insert_perwalian_catatan = array();
				$insert_perwalian_catatan['perguruan_tinggi_id']			= $mahasiswa->perguruan_tinggi_id;
				$insert_perwalian_catatan['program_studi_id']				= $mahasiswa->program_studi_id;
				$insert_perwalian_catatan['jenjang_kode']					= $mahasiswa->jenjang_kode;
				$insert_perwalian_catatan['perguruan_tinggi_id']			= $mahasiswa->perguruan_tinggi_id;
				$insert_perwalian_catatan['dosen_id']						= $dosen->dosen_id;
				$insert_perwalian_catatan['mahasiswa_id']					= $mahasiswa->mahasiswa_id;
				$insert_perwalian_catatan['semester_kode']					= $semester->semester_kode;
				$insert_perwalian_catatan['perwalian_catatan_id']			= $this->uuid->v4();
				$insert_perwalian_catatan['perwalian_catatan_tanggal']		= date('Y-m-d');
				$insert_perwalian_catatan['perwalian_catatan_isi']			= html_encode($catatan);
				$insert_perwalian_catatan['perwalian_catatan_jenis']		= "Dosen";
				$insert_perwalian_catatan['created_by']						= userdata('pengguna_id');
				$this->Perwalian_model->insert_perwalian_catatan($insert_perwalian_catatan);

				$data['response']	= true;
				$data['message']	= "Data berhasil disimpan.";
				$data['params']		= array("mahasiswa_id"=>$mahasiswa_id,
											"program_studi_id"=>$program_studi_id,
											"semester_kode"=>$semester_kode);
			} else {
				$data['response']	= false;
				$data['message']	= "Data semester tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function get_catatan(){
		$data = array();
		$program_studi_id = $this->input->post('program_studi');
		$semester_kode = $this->input->post('semester');
		$mahasiswa_id = $this->input->post('mahasiswa');
		if ($mahasiswa_id && $program_studi_id && $semester_kode){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$semester = $this->Semester_model->get_semester("*", array('semester_kode'=>$semester_kode));
			$mahasiswa = $this->Mahasiswa_model->get_mahasiswa("*", array('mahasiswa_id'=>$mahasiswa_id));

			if ($program_studi && $semester && $mahasiswa){
				$perwalian_catatan = $this->Perwalian_model->grid_all_perwalian_catatan('perwalian_catatan.*, mahasiswa_nama, dosen_nama', 'perwalian_catatan.created_at', 'ASC', '', '', array('perwalian_catatan.mahasiswa_id'=>$mahasiswa_id));
				$res = array();
				$i = 0;
				foreach ($perwalian_catatan as $row_catatan) {
					$res[$i] = new stdClass();
					$res[$i]->mahasiswa_nama = $row_catatan->mahasiswa_nama;
					$res[$i]->dosen_nama = $row_catatan->dosen_nama;
					$res[$i]->perwalian_catatan_jenis = $row_catatan->perwalian_catatan_jenis;
					$res[$i]->perwalian_catatan_isi = html_decode($row_catatan->perwalian_catatan_isi);
					$res[$i]->created_at = dateIndo5($row_catatan->created_at);
					$i++;
				}
				if ($perwalian_catatan){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					// $data['data']		= $perwalian_catatan;
					$data['data']		= $res;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}

	public function submit(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$perwalian_id = $this->uri->segment(4);
		$perwalian = $this->Perwalian_model->get_perwalian("perwalian.*", array('perwalian_id'=>$perwalian_id));

		if ($perwalian->approve_wali_status == 'N'){
			$update = array();
			$update['approve_wali_by']			= userdata('pengguna_id');
			$update['approve_wali_status']		= 'Y';
			$update['approve_akademik_tanggal']	= date('Y-m-d H:i:s');
			$update['updated_by']				= userdata('pengguna_id');

			$this->Perwalian_model->update_perwalian(array('perwalian_id'=>validasi_sql($perwalian_id)), $update);

			$this->session->set_flashdata('success','Perwalian telah berhasil di submit.');
		} else {
			$this->session->set_flashdata('error','Perwalian gagal di submit.');
		}
		redirect(module_url($this->uri->segment(2).'/detail/'.$perwalian->mahasiswa_id.'?tabs=1'));
	}
	
	public function cancel(){
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$perwalian_id = $this->uri->segment(4);
		$perwalian = $this->Perwalian_model->get_perwalian("perwalian.*", array('perwalian_id'=>$perwalian_id));

		if ($perwalian->approve_wali_status == 'Y'){
			$update = array();
			$update['approve_wali_by']			= userdata('pengguna_id');
			$update['approve_wali_status']		= 'N';
			$update['approve_akademik_tanggal']	= date('Y-m-d H:i:s');
			$update['updated_by']				= userdata('pengguna_id');

			$this->Perwalian_model->update_perwalian(array('perwalian_id'=>validasi_sql($perwalian_id)), $update);

			$this->session->set_flashdata('success','Perwalian telah berhasil di batal submit.');
		} else {
			$this->session->set_flashdata('error','Perwalian gagal di batal submit.');
		}
		redirect(module_url($this->uri->segment(2).'/detail/'.$perwalian->mahasiswa_id.'?tabs=1'));
	}
}