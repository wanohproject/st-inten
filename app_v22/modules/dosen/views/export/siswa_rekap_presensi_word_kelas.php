<?php

header('Content-Type: application/vnd.ms-word'); //IE and Opera  
header('Content-Type: application/w-msword'); // Other browsers  
header('Content-Disposition: attachment; filename=Presensi_Kelas_'.str_replace(' ', '_', $kelas->kelas_nama).'_'.date('d_m_Y_H_i_s').'.doc'); 
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="Doc1_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2008-06-16T08:31:00Z</o:Created>
  <o:LastSaved>2008-06-16T08:31:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
 <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1{
	size:595.0pt 842.0pt;
	margin:36.0pt 57.0pt 36.0pt 57.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;
}
div.Section1
	{page:Section1;}
.style1 {
	color: #000000;
	font-weight: bold;
	font-family: Arial, "Times New Roman", Helvetica, sans-serif;
	font-size: 13px;
}
.style2 {
	font-weight: bold;
	font-size: 14px;
	font-family: Arial, "Times New Roman", Helvetica, sans-serif;
	color: #000000;
}
.style5, .style5 p {font-family: Arial, "Times New Roman", Helvetica, sans-serif; font-size: 13px; font-weight:bold;}
.style13 {color: #000000}
.style14 {color: #FFFFFF}
.style17 {color: #FFFFFF; font-weight: bold; font-family: Arial, "Times New Roman", Helvetica, sans-serif; font-size: 13px; }
.style20 {font-family: Arial, "Times New Roman", Helvetica, sans-serif; font-weight: bold; font-size: 13px; }
.style21 {
	font-family: Arial, "Times New Roman", Helvetica, sans-serif;
	font-weight: bold;
}
.style22 {font-family: Arial, "Times New Roman", Helvetica, sans-serif}
.style24 {
	font-size: 13px;
	color: #FFFFFF;
}
.style27 {color: #FFFFFF; font-weight: bold; font-family: Arial, "Times New Roman", Helvetica, sans-serif; font-size: 13px; }
.style54 {font-size: 12px;}
.tandatangan {font-family: Arial, "Times New Roman", Helvetica, sans-serif; font-size: 12px; }
.style-table01 { padding: 0px 5px 0px 5px;}
.text-center { text-align: center;}
table {border-collapse:collapse;font-family:Arial, "Times New Roman", Geneva, sans-serif; font-size:12px;}
table th, table td {font-family:Arial, "Times New Roman", Geneva, sans-serif; font-size:13px; font-weight:bold;}

.table th,
.table td{
	padding: 0px 5px;
}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>
<body lang=EN-US style='tab-interval:36.0pt'>
<?php
$where_siswa_kelas['siswa_kelas.tahun_kode']	= $tahun_kode;
$where_siswa_kelas['siswa_kelas.kelas_id']	= $kelas_id;
$grid_siswa 								= $this->Siswa_kelas_model->grid_all_siswa_kelas("", "siswa_nama", "ASC", 0, 0, $where_siswa_kelas);
foreach($grid_siswa as $row){
	$siswa 		= $row;
	$siswa_id 	= $row->siswa_id;	

	$siswa_foto = ($siswa)?$siswa->siswa_foto:'';
	$foto = ($siswa_foto && file_exists('./asset/foto-siswa/'.$siswa_foto))?base_url('asset/foto-siswa/'.$siswa_foto):base_url('asset/profil/users.jpg');
	
	$sidik_jari	= $this->db->query("SELECT * FROM sat_sidik_jari sidik_jari LEFT JOIN siswa ON sidik_jari.sidik_jari_user=siswa.siswa_user WHERE siswa.siswa_id = '".$siswa_id."'")->row();

	$sidik_jari_id = ($sidik_jari)?$sidik_jari->sidik_jari_id:'';
	$finger_id = ($sidik_jari)?$sidik_jari->finger_id:'';
	$mesin_id = ($sidik_jari)?$sidik_jari->mesin_id:'';
	$user_id = ($sidik_jari)?$sidik_jari->user_id:'';
	$sidik_jari_user = ($sidik_jari)?$sidik_jari->sidik_jari_user:'';
	$sidik_jari_nama = ($sidik_jari)?$sidik_jari->sidik_jari_nama:'';
	$sidik_jari_level = ($sidik_jari)?$sidik_jari->sidik_jari_level:'Dosen';
	$sidik_jari_ukuran = ($sidik_jari)?$sidik_jari->sidik_jari_ukuran:'';
	$sidik_jari_valid = ($sidik_jari)?$sidik_jari->sidik_jari_valid:'';
	$sidik_jari_file = ($sidik_jari)?$sidik_jari->sidik_jari_file:'';
	$sidik_jari_sms = ($sidik_jari)?$sidik_jari->sidik_jari_sms:'N';
	$sidik_jari_tanggal = ($sidik_jari)?$sidik_jari->sidik_jari_tanggal:'';
	$siswa_id = ($sidik_jari)?$sidik_jari->siswa_id:'';
	$siswa_user = ($sidik_jari)?$sidik_jari->siswa_user:'';
		
	$tingkat_nama	= $this->db->query("SELECT * FROM tingkat WHERE tingkat_id = '{$tingkat_id}'")->row()->tingkat_romawi;
	$trim_kelas = explode(" ", $siswa->kelas_nama);
	$new_semester = "";
	if ($semester_id == 1){
		$new_semester = "1 (Satu)";
	} else {
		$new_semester = "2 (Dua)";
	}
	// if ($tingkat_nama == "VII" && $semester_id == 1){
	// 	$new_semester = "1 (Satu)";
	// } else if ($tingkat_nama == "VII" && $semester_id == 2){
	// 	$new_semester = "2 (Dua)";
	// } else if ($tingkat_nama == "VIII" && $semester_id == 1){
	// 	$new_semester = "3 (Tiga)";
	// } else if ($tingkat_nama == "VIII" && $semester_id == 2){
	// 	$new_semester = "4 Empat)";
	// } else if ($tingkat_nama == "IX" && $semester_id == 1){
	// 	$new_semester = "5 (Lima)";
	// } else if ($tingkat_nama == "IX" && $semester_id == 2){
	// 	$new_semester = "6 (Enam)";
	// } else if ($tingkat_nama == "VII" && $semester_id == 1){
	// 	$new_semester = "1 (Satu)";
	// } else if ($tingkat_nama == "VII" && $semester_id == 2){
	// 	$new_semester = "2 (Dua)";
	// } else if ($tingkat_nama == "VIII" && $semester_id == 1){
	// 	$new_semester = "3 (Tiga)";
	// } else if ($tingkat_nama == "VIII" && $semester_id == 2){
	// 	$new_semester = "4 (Empat)";
	// } else if ($tingkat_nama == "IX" && $semester_id == 1){
	// 	$new_semester = "5 (Lima)";
	// } else if ($tingkat_nama == "IX" && $semester_id == 2){
	// 	$new_semester = "6 (Enam)";
	// }	
	?>
	<div class=Section1>
	<div align="center" class="style2" style="padding:8px;font-size:18px;"><strong>LAPORAN PRESENSI SISWA</strong></div><br />
	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
	<tr>
		<td colspan="2" valign="top" style="padding: 0px;padding-bottom:20px;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="150" valign="top" rowspan="">
					<img src="<?php echo $foto; ?>" alt="" width="100" height="100">
				</td>
				<td valign="top">
					<table width="100%" border="0" cellpadding="1" cellspacing="0">
						<tr height="18">
							<td width="150"><span class="style5">Nomor Induk / NISN</span></td>
							<td><span class="style5">:&nbsp;<?=$siswa->siswa_nis?> / <?=$siswa->siswa_nisn?></span></td>
						</tr>
						<tr height="18">
							<td><span class="style5">Nama</span></td>
							<td><span class="style5">:&nbsp;<?=$siswa->siswa_nama?></span></td>
						</tr>
						<tr height="18">
							<td><span class="style5">Jenis Kelamin</span></td>
							<td><span class="style5">:&nbsp;<?=$siswa->siswa_jenis_kelamin?></span></td>
						</tr>
						<tr height="18">
							<td><span class="style5">Kelas</span></td>
							<td><span class="style5">:&nbsp;<?=$siswa->kelas_nama?></span></td>
						</tr>
						<tr height="18">
							<td><span class="style5">Tanggal</span></td>
							<td><span class="style5">:&nbsp;<?php echo dateIndo($presensi_tanggal_awal) . ' s/d ' . dateIndo($presensi_tanggal_akhir); ?></span></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" valign="top" style="padding: 0px;padding-bottom:20px;">
		<table width="100%" class="table" border="1">
			<tr height="25">
				<th width="35">No</th>
				<th width="80">Hari</th>
				<th>Tanggal</th>
				<th width="80">Status</th>
				<th width="90">Jam Masuk</th>
				<th width="90">Jam Pulang</th>
				<th width="120">Waktu di Sekolah</th>
			</tr>
			<?php
			$hadir = 0;
			$terlambat = 0;
			$tidakhadir = 0;

			if ($days && $siswa_user != '-'){
				$j = 1;
				for ($i=0; $i < $days; $i++) { 
					if ($i == 0){
						$date = $presensi_tanggal_awal;
					} else {
						$date = date('Y-m-d', strtotime($presensi_tanggal_awal . " + $i days"));
					}

					$N = date('N', strtotime($date));
					$jadwal = $this->Jadwal_model->get_jadwal('', array('jadwal_hari'=>$N, 'jadwal_level'=>$sidik_jari_level));
					
					if ($jadwal){
						$color = "FBCBCB";
						$color_masuk = "FBCBCB";
						$color_pulang = "FBCBCB";
						$status = "TK";

						if ($this->Libur_model->count_all_libur('', array('libur_tanggal'=>$date)) < 1){
							$presensi	= $this->Presensi_model->get_presensi('*', "presensi.tahun_kode = '$tahun_kode' AND presensi.semester_id = '$semester_id' AND presensi.presensi_user = '$siswa_user' AND DATE(presensi.presensi_tanggal_masuk) = '$date'");                        
							if ($presensi){
							$status = "Hadir";
							}
							
							$presensi_tanggal_masuk = ($presensi)?($presensi->presensi_tanggal_masuk)?$presensi->presensi_tanggal_masuk:'':'';
							$presensi_tanggal_keluar = ($presensi)?($presensi->presensi_tanggal_keluar)?$presensi->presensi_tanggal_keluar:'':'';
							$jam_masuk = ($presensi_tanggal_masuk)?date('H:i:s', strtotime($presensi->presensi_tanggal_masuk)):'&nbsp;';
							$jam_pulang = ($presensi_tanggal_keluar)?date('H:i:s', strtotime($presensi->presensi_tanggal_keluar)):'&nbsp;';
							
							$jam_masuk_ = $jam_masuk;
							$jam_pulang_ = $jam_pulang;
							if ($presensi_tanggal_masuk){
							if (strtotime($presensi_tanggal_masuk) >= strtotime($date.' '.$jadwal->jadwal_masuk_awal) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_tepat)){
								$color = "#B3DE81";
								$color_masuk = "#B3DE81";
								$hadir++;
							} else if (strtotime($presensi_tanggal_masuk) > strtotime($date.' '.$jadwal->jadwal_masuk_tepat) && strtotime($presensi_tanggal_masuk) <= strtotime($date.' '.$jadwal->jadwal_masuk_akhir)){
								$color = "#FBCBCB";
								$color_masuk = "#FBCBCB";
								$terlambat++;
							}
							}
							
							if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_tepat) && strtotime($presensi_tanggal_keluar) <= strtotime($date.' '.$jadwal->jadwal_pulang_akhir)){
							$color_pulang = "#B3DE81";
							} else if (strtotime($presensi_tanggal_keluar) >= strtotime($date.' '.$jadwal->jadwal_pulang_awal) && strtotime($presensi_tanggal_keluar) < strtotime($date.' '.$jadwal->jadwal_pulang_tepat)){
							$color_pulang = "#FBCBCB";
							}

							$absensi	= $this->Absensi_model->get_absensi('*', "absensi.tahun_kode = '$tahun_kode' AND absensi.semester_id = '$semester_id' AND absensi.absensi_user = '$siswa_user' AND DATE(absensi.absensi_tanggal) = '$date'");
							if ($absensi){
							$status = $absensi->absensi_info;
							$color = "FBCBCB";
							}

							if ($status != "Hadir"){
							$tidakhadir++;
							}

							$lama_kerja = ($jam_masuk_ != '&nbsp;' && $jam_pulang_ != '&nbsp;')?selisihDate($jam_masuk_, $jam_pulang_):'&nbsp;';
							?>
							<tr height="20">
								<td class="text-center"><?php echo $j; ?></td>
								<td class="text-center"><?php echo inday($date); ?></td>
								<td class="text-center"><?php echo dateIndo($date); ?></td>
								<td class="text-center" bgcolor="<?php echo $color; ?>"><?php echo $status; ?></td>
								<td class="text-center" bgcolor="<?php echo $color_masuk; ?>"><?php echo $jam_masuk; ?></td>
								<td class="text-center" bgcolor="<?php echo $color_pulang; ?>"><?php echo $jam_pulang; ?></td>
								<td class="text-center"><?php echo $lama_kerja; ?></td>
							</tr>
							<?php
						} else {
							$status = "Libur";
							?>
							<tr height="20">
							  <td class="text-center"><?php echo $j; ?></td>
							  <td class="text-center"><?php echo inday($date); ?></td>
							  <td class="text-center"><?php echo dateIndo($date); ?></td>
							  <td class="text-center" bgcolor="<?php echo $color; ?>"><?php echo $status; ?></td>
							  <td class="text-center" bgcolor="<?php echo $color; ?>">-</td>
							  <td class="text-center" bgcolor="<?php echo $color; ?>">-</td>
							  <td class="text-center">-</td>
							</tr>
							<?php
						}
						$j++;
					}
				}
			}
			?>
				<tr height="20">
				<th style="text-align:left" colspan="7">Hadir: <?php echo $hadir; ?></th>
				</tr>
				<tr height="20">
				<th style="text-align:left" colspan="7">Terlambat: <?php echo $terlambat; ?></th>
				</tr>
				<tr height="20">
				<th style="text-align:left" colspan="7">Tidak Hadir: <?php echo $tidakhadir; ?></th>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="text-left">
			<table width="100%" border="0" cellpadding="2" cellspacing="0" class="tandatangan" align="left">
				<tr>
					<td style="padding-top:0px;vertical-align:bottom;font-weight:bold;" width="40%">Bandung, <?php echo dateIndo_(date('Y-m-d')); ?><br />Kepala Sekolah,</td>
				</tr>
				<tr>
					<td><br /><br />&nbsp;</td>
				</tr>
				<tr>
					<td style="font-weight:bold;"><u><?=($kepala_sekolah)?$kepala_sekolah->staf_nama:'';?></u><br /><?=($kepala_sekolah)?'NIP. '.$kepala_sekolah->staf_nip:'';?></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>

	</div>
	
	<span style='font-size:12.0pt;font-family:"Times New Roman";mso-fareast-font-family:
	"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
	mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always;
	mso-break-type:section-break'>
	</span>
	<div style="page-break-after:always;"></div>
	<!-- /////////////////////////////////////////////////////////////////////////////// -->
<?php } ?>
</body>
</html>