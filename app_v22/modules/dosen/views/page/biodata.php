<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fastclick/fastclick.min.js');?>"></script>
<script>
  $(function () {
		
		var table_aktivitas = $("#datagrid_aktivitas").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url" : "<?php echo module_url($this->uri->segment(2).'/datatable_aktivitas/'.$dosen_id); ?>",
				"type" : "POST",
			},
			"columns": [
				{ "data": "semester_nama"},
				{ "data": "matakuliah_kode"},
				{ "data": "matakuliah_nama"},
				{ "data": "dosen_mengajar_rencana_pertemuan"},
				{ "data": "dosen_mengajar_realisasi_pertemuan"},
				{ "data": "dosen_mengajar_kelas"}
			],
			"language": {
				"emptyTable": "Tidak ada data pada tabel ini",
				"info": "Data ke _START_ sampai _END_ dari _TOTAL_ data",
				"infoEmpty": "Tidak ada data yang sesuai",
				"infoFiltered": "(hasil pencarian dari _MAX_ data)",
				"lengthMenu": "Tampil _MENU_  baris",
				"search": "Cari: ",
				"zeroRecords": "Tidak ada baris yang sesuai"
			},
			"sScrollX": "100%",
				"sScrollXInner": "100%",
				"bScrollCollapse": true,
			"lengthMenu": [
				[10, 20, 30, -1],
				[10, 20, 30, "All"] // change per page values here
			],
			"order": [
				[0, 'asc']
			],
			"pageLength": 10,
				"columnDefs": [{
				'orderable': false,
				'targets': [-1]
			}, {
				"searchable": false,
				"targets": [-1]
			}]
		});

		<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
			<?php if ($this->session->flashdata('success')) { ?>
				$('#successModal').modal('show');
			<?php } else { ?>
				$('#errorModal').modal('show');
			<?php } ?>
		<?php } ?>

		$( "#addBtn" ).click(function() {
			var tabs = $(this).attr("data-tabs");
			if (tabs == 1){
				$('#addDosenModal').modal('show');
			}
		});
		
		table_aktivitas.on('click', 'a.btn-edit', function () {
				var data_id = $(this).attr("data-id");
				$("#ek_id").val(data_id);
				var data_id = $("#ek_id").val();
				$.ajax({ 
						type: 'GET', 
						url: '<?php echo module_url($this->uri->segment(2).'/get-aktivitas'); ?>/' + data_id,
						dataType: 'json',
						success: function (data) { 
								if (data.response == true){
									$("#ek_dosen_id").val(data.data.dosen_id);
									$("#ek_rencana_pertemuan").val(data.data.dosen_mengajar_rencana_pertemuan);
									$("#ek_realisasi_pertemuan").val(data.data.dosen_mengajar_realisasi_pertemuan);
									$("#ek_mengajar_kelas").val(data.data.dosen_mengajar_kelas);
								}
						}
				});
        $('#editDosenModal').modal('show');
    });
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dosen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('staf'); ?>">Dosen</a></li>
            <li class="active">Detail Dosen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
					<div class="row">
            <div class="col-xs-12">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs" role="tablist" id="myTab">
									<li <?php echo ($tabs == 0)?' class="active"':''; ?>><a href="<?php echo module_url($this->uri->segment(2).'/index/'.$dosen_id); ?>?tabs=0">Detail Mata Kuliah</a></li>
									<li <?php echo ($tabs == 1)?' class="active"':''; ?>><a href="<?php echo module_url($this->uri->segment(2).'/index/'.$dosen_id); ?>?tabs=1">Aktivitas Mengajar</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane <?php echo ($tabs == 0)?'active':''; ?>" id="tab_1">
										<div class="form-horizontal">
											<div class="box-header">
												<h3 class="box-title">Informasi Akun</h3>
											</div><!-- /.box-header -->
											<div class="box-body">
												<div class="form-group">
													<label for="dosen_user" class="col-md-2 control-label" style="text-align:left">User</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_user" id="dosen_user" value="<?php echo $dosen_user; ?>" placeholder="" readonly>
													</div>
													<label for="dosen_pin" class="col-md-2 control-label" style="text-align:left">Password</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_pin" id="dosen_pin" value="<?php echo $dosen_pin; ?>" placeholder="" readonly>
														<small>* Hapus 6 Karakter.</small>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														* Apabila <strong>User</strong> tidak diisi maka user dosen tidak akan dibuat. <br />
														* Apabila <strong>Password</strong> tidak diisi maka pin dosen akan dibuat otomatis 6 Karakter Acak. <br />
													</div>
												</div>
											</div>
											<div class="box-header">
												<h3 class="box-title">Informasi Identitas</h3>
											</div><!-- /.box-header -->
											<div class="box-body">
												<div class="form-group">
													<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
													<div class="col-md-4">
														<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="dosen_nidn" class="col-md-2 control-label" style="text-align:left">NIDN</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_nidn" id="dosen_nidn" value="<?php echo $dosen_nidn; ?>" placeholder="" readonly>
													</div>
													<label for="dosen_ktp" class="col-md-2 control-label" style="text-align:left">No. KTP</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_ktp" id="dosen_ktp" value="<?php echo $dosen_ktp; ?>" placeholder="" readonly>
													</div>
												</div>
												<div class="form-group">
													<label for="dosen_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_nama" id="dosen_nama" value="<?php echo $dosen_nama; ?>" placeholder="" readonly>
													</div>
													<label for="dosen_gelar" class="col-md-2 control-label" style="text-align:left">Gelar</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_gelar" id="dosen_gelar" value="<?php echo $dosen_gelar; ?>" placeholder="" readonly>
														<small>Singkatan</small>
													</div>
												</div>
												<div class="form-group">
													<label for="dosen_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
													<div class="col-md-4">
														<?php echo $this->Referensi_model->combobox(8, 'dosen_jenis_kelamin', 'kode_value', 'kode_nama', $dosen_jenis_kelamin, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="jabatan_akademik_kode" class="col-md-2 control-label" style="text-align:left">Jabatan Akademik</label>
													<div class="col-md-4">
														<?php echo $this->Referensi_model->combobox(2, 'jabatan_akademik_kode', 'kode_value', 'kode_nama', $jabatan_akademik_kode, '', '', 'class="form-control" disabled');?>
													</div>
													<label for="pendidikan_kode" class="col-md-2 control-label" style="text-align:left">Pendidikan Tertinggi</label>
													<div class="col-md-4">
														<?php echo $this->Referensi_model->combobox(1, 'pendidikan_kode', 'kode_value', 'kode_nama', $pendidikan_kode, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="status_ikatan_kerja_kode" class="col-md-2 control-label" style="text-align:left">Status Ikatan Kerja Dosen</label>
													<div class="col-md-4">
														<?php echo $this->Referensi_model->combobox(3, 'status_ikatan_kerja_kode', 'kode_value', 'kode_nama', $status_ikatan_kerja_kode, '', '', 'class="form-control" disabled');?>
													</div>
													<label for="status_aktivitas_kode" class="col-md-2 control-label" style="text-align:left">Status Aktivitas Dosen</label>
													<div class="col-md-4">
														<?php echo $this->Referensi_model->combobox(15, 'status_aktivitas_kode', 'kode_value', 'kode_nama', $status_aktivitas_kode, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="dosen_semester_mulai" class="col-md-2 control-label" style="text-align:left">Semester Mulai Keluar/Pensiun/Alm.</label>
													<div class="col-md-4">
														<?php echo combobox('db', $this->Semester_model->grid_all_semester('', 'semester_nama', 'DESC'), 'dosen_semester_mulai', 'semester_kode', 'semester_nama', $dosen_semester_mulai, '', '', 'class="form-control" disabled');?>
													</div>
												</div>
												<div class="form-group">
													<label for="dosen_nip_pns" class="col-md-2 control-label" style="text-align:left">NIP PNS</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_nip_pns" id="dosen_nip_pns" value="<?php echo $dosen_nip_pns; ?>" placeholder="" readonly>
													</div>
													<label for="dosen_homebase" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi Induk (Homebase)</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="dosen_homebase" id="dosen_homebase" value="<?php echo $dosen_homebase; ?>" placeholder="" readonly>
													</div>
												</div>
											</div><!-- /.box-body -->
										</div><!-- /.box-body -->
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane <?php echo ($tabs == 1)?'active':''; ?>" id="tab_2">
										<table id="datagrid_aktivitas" class="table table-bordered table-striped" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>SEMESTER</th>
													<th>KODE MATA KULIAH</th>
													<th>NAMA MATA KULIAH</th>
													<th>RENCANA PERTEMUAN</th>
													<th>REALISASI PERTEMUAN</th>
													<th>MENGEJAR KELAS</th>
												</tr>
											</thead>
										</table>
									</div>
									<!-- /.tab-pane -->
								</div>
								<!-- /.tab-content -->
							</div>
							<!-- nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
					<div class="row">
            <div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Kembali</button>
									<?php if (check_permission("W")){?>
									<button type="button" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/edit/'.$dosen_id); ?>'" class="btn btn-primary" name="save" value="save">Ubah Dosen</button>
									<?php } ?>
								</div><!-- /.box-footer -->
							</div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'edit') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#mahasiswa_tanggal_lahir').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Biodata Dosen
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li class="active">Update Biodata Dosen</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Update Biodata Dosen</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$dosen_id);?>" method="post">
				<input type="hidden" class="form-control" name="dosen_id" id="dosen_id" value="<?php echo $dosen_id; ?>" placeholder="">
				<div class="box-body">
					<div class="form-group">
						<label for="dosen_user" class="col-md-2 control-label" style="text-align:left">User</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_user" id="dosen_user" value="<?php echo $dosen_user; ?>" placeholder="" readonly>
						</div>
					</div>
				</div>
				<div class="box-header">
					<h3 class="box-title">Informasi Identitas</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label for="program_studi_id" class="col-md-2 control-label" style="text-align:left">Program Studi</label>
						<div class="col-md-4">
							<?php echo combobox('db', $this->Program_studi_model->grid_all_program_studi('', 'program_studi_nama', 'ASC'), 'program_studi_id', 'program_studi_id', 'program_studi_nama', $program_studi_id, '', '', 'class="form-control" disabled');?>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_nidn" class="col-md-2 control-label" style="text-align:left">NIDN</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_nidn" id="dosen_nidn" value="<?php echo $dosen_nidn; ?>" placeholder="">
						</div>
						<label for="dosen_ktp" class="col-md-2 control-label" style="text-align:left">No. KTP</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_ktp" id="dosen_ktp" value="<?php echo $dosen_ktp; ?>" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_nama" class="col-md-2 control-label" style="text-align:left">Nama</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_nama" id="dosen_nama" value="<?php echo $dosen_nama; ?>" placeholder="" required>
						</div>
						<label for="dosen_gelar" class="col-md-2 control-label" style="text-align:left">Gelar Akademik/Profesional Tertinggi</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_gelar" id="dosen_gelar" value="<?php echo $dosen_gelar; ?>" placeholder="">
							<small>Singkatan</small>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_jenis_kelamin" class="col-md-2 control-label" style="text-align:left">Jenis Kelamin</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(8, 'dosen_jenis_kelamin', 'kode_value', 'kode_nama', $dosen_jenis_kelamin, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="jabatan_akademik_kode" class="col-md-2 control-label" style="text-align:left">Jabatan Akademik</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(2, 'jabatan_akademik_kode', 'kode_value', 'kode_nama', $jabatan_akademik_kode, '', '', 'class="form-control"');?>
						</div>
						<label for="pendidikan_kode" class="col-md-2 control-label" style="text-align:left">Pendidikan Tertinggi</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(1, 'pendidikan_kode', 'kode_value', 'kode_nama', $pendidikan_kode, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="status_ikatan_kerja_kode" class="col-md-2 control-label" style="text-align:left">Status Ikatan Kerja Dosen</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(3, 'status_ikatan_kerja_kode', 'kode_value', 'kode_nama', $status_ikatan_kerja_kode, '', '', 'class="form-control"');?>
						</div>
						<label for="status_aktivitas_kode" class="col-md-2 control-label" style="text-align:left">Status Aktivitas Dosen</label>
						<div class="col-md-4">
							<?php echo $this->Referensi_model->combobox(15, 'status_aktivitas_kode', 'kode_value', 'kode_nama', $status_aktivitas_kode, '', '', 'class="form-control"');?>
						</div>
					</div>
					<div class="form-group">
						<label for="dosen_nip_pns" class="col-md-2 control-label" style="text-align:left">NIP PNS</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_nip_pns" id="dosen_nip_pns" value="<?php echo $dosen_nip_pns; ?>" placeholder="">
						</div>
						<label for="dosen_homebase" class="col-md-2 control-label" style="text-align:left">Perguruan Tinggi Induk (Homebase)</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="dosen_homebase" id="dosen_homebase" value="<?php echo $dosen_homebase; ?>" placeholder="">
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
					<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
				</div><!-- /.box-footer -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } ?>