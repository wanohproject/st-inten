<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Input_jadwal extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $semester_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Jadwal Mata Kuliah | ' . profile('profil_website');
		$this->active_menu		= 362;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Jadwal_pelajaran_model');
		$this->load->model('Jam_pelajaran_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Dosen_model');
		$this->load->model('Ruangan_model');
		
		$this->semester_kode			= $this->Semester_model->get_semester_aktif()->semester_kode;
    }
	
	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$semester_kode			= ($this->uri->segment(4))?$this->uri->segment(4):$this->semester_kode;
		$ruangan_id				= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['semester_kode']	= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		$data['ruangan_id']		= ($this->input->post('ruangan_id'))?$this->input->post('ruangan_id'):$ruangan_id;

		$data['dayName'] = array('1'=>'Senin', '2'=>'Selasa', '3'=>'Rabu', '4'=>'Kamis', '5'=>'Jumat', '6'=>'Sabtu');

		$where_jamke['semester_kode']				= $data['semester_kode'];
		$where_jamke['jam_pelajaran_hari']	= $this->input->post('jadwal_pelajaran_hari');
		$where_jamke['jam_pelajaran_info']	= "KBM";
		$data['grid_jam'] = $this->db->query("SELECT * FROM akd_jam_pelajaran jam_pelajaran WHERE jam_pelajaran.semester_kode = '".$data['semester_kode']."' AND jam_pelajaran_info = 'KBM' GROUP BY jam_pelajaran_ke ORDER BY CAST(jam_pelajaran_ke AS UNSIGNED) ASC")->result();
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/input_jadwal', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function set()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'set';
		
		$semester_kode			= ($this->uri->segment(4))?$this->uri->segment(4):$this->semester_kode;
		$ruangan_id					= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$dosen_mengajar_id	= ($this->uri->segment(6))?$this->uri->segment(6):'-';
		$viewby							= ($this->uri->segment(7))?$this->uri->segment(7):'-';
		
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		$data['dosen_mengajar_id']	= '-';
		$data['ruangan_id']			= ($this->input->post('ruangan_id'))?$this->input->post('ruangan_id'):$ruangan_id;
		$data['dosen_id']			= '-';
		$data['viewby']				= 'ruangan';

		$data['jadwal_pelajaran_hari'] = ($this->input->post('jadwal_pelajaran_hari'))?$this->input->post('jadwal_pelajaran_hari'):'';
		$data['jadwal_pelajaran_mulai'] = ($this->input->post('jadwal_pelajaran_mulai'))?$this->input->post('jadwal_pelajaran_mulai'):'07:00:00';
		$data['jadwal_pelajaran_selesai'] = ($this->input->post('jadwal_pelajaran_selesai'))?$this->input->post('jadwal_pelajaran_selesai'):'07:00:00';
		
		$save					= $this->input->post('save');
		if ($save == 'save'){
			$dosen_mengajar_id = $this->input->post('dosen_mengajar_id');

			$where_delete['semester_kode'] 			= $this->input->post('semester_kode');
			$where_delete['ruangan_id'] 			= $this->input->post('ruangan_id');
			$where_delete['jadwal_pelajaran_hari'] 	= $this->input->post('jadwal_pelajaran_hari');

			$this->Jadwal_pelajaran_model->delete_jadwal_pelajaran($where_delete);

			$where_jamke['semester_kode']		= $this->input->post('semester_kode');
			$where_jamke['jam_pelajaran_hari']	= $this->input->post('jadwal_pelajaran_hari');
			$where_jamke['jam_pelajaran_info']	= "KBM";
			$grid_jam = $this->Jam_pelajaran_model->grid_all_jam_pelajaran("", "jam_pelajaran_urutan", "ASC", 0, 0, $where_jamke);
			if ($grid_jam){
				
				foreach ($grid_jam as $row_jam) {
					$where['jam_pelajaran_hari'] 		= $this->input->post('jadwal_pelajaran_hari');
					$where['jam_pelajaran_ke'] 			= $row_jam->jam_pelajaran_ke;
					$where['semester_kode'] 					= $this->input->post('semester_kode');
					$jam_pelajaran = $this->Jam_pelajaran_model->get_jam_pelajaran("", $where);
					
					$dosen_mengajar_id_ = $dosen_mengajar_id[$row_jam->jam_pelajaran_ke];
					$get_dosen_mengajar = $this->Dosen_model->get_dosen_mengajar("dosen_mengajar.*", array('dosen_mengajar.dosen_mengajar_id'=>$dosen_mengajar_id_));
					
					// echo "<pre>";
					// print_r($jam_pelajaran);
					// print_r($get_dosen_mengajar);
					// die;
					if ($jam_pelajaran && $dosen_mengajar_id_ && $get_dosen_mengajar){
						$insert = array();
						$insert['perguruan_tinggi_id'] 		= $get_dosen_mengajar->perguruan_tinggi_id;
						$insert['program_studi_id'] 		= $get_dosen_mengajar->program_studi_id;
						$insert['jenjang_kode'] 			= $get_dosen_mengajar->jenjang_kode;
						$insert['semester_kode'] 			= $this->input->post('semester_kode');
						$insert['matakuliah_id'] 			= $get_dosen_mengajar->matakuliah_id;
						$insert['dosen_id'] 				= $get_dosen_mengajar->dosen_id;
						$insert['ruangan_id'] 				= $this->input->post('ruangan_id');
						$insert['jadwal_pelajaran_id'] 		= $this->uuid->v4();
						$insert['jadwal_pelajaran_hari'] 	= $this->input->post('jadwal_pelajaran_hari');
						$insert['jadwal_pelajaran_urutan'] 	= $jam_pelajaran->jam_pelajaran_urutan;
						$insert['jadwal_pelajaran_ke'] 		= $row_jam->jam_pelajaran_ke;
						$insert['jadwal_pelajaran_mulai'] 	= $jam_pelajaran->jam_pelajaran_mulai;
						$insert['jadwal_pelajaran_selesai'] = $jam_pelajaran->jam_pelajaran_selesai;
						$insert['created_by'] 				= userdata('pengguna_');
						$this->Jadwal_pelajaran_model->insert_jadwal_pelajaran($insert);
					}
				}
			}
			$this->session->set_flashdata('success','Jadwal Pelajaran telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$data['semester_kode']));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/input_jadwal', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
		
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$jadwal_pelajaran_id	= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$viewby					= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		
		$where_delete['jadwal_pelajaran_id']	= validasi_sql($jadwal_pelajaran_id);
		$jadwal_pelajaran 						= $this->Jadwal_pelajaran_model->get_jadwal_pelajaran('*', $where_delete);

		$data['semester_kode']	= $jadwal_pelajaran->semester_kode;
		$data['matakuliah_id']	= '-';
		$data['ruangan_id']		= $jadwal_pelajaran->ruangan_id;
		$data['dosen_id']		= '-';
		
		$get_ruangan = $this->Ruangan_model->get_ruangan("", array('ruangan_id'=>$data['ruangan_id']));
		$data['tingkat_id']		= $get_ruangan->tingkat_id;
		$data['viewby']			= 'ruangan';

		$this->Jadwal_pelajaran_model->delete_jadwal_pelajaran($where_delete);
		
		$this->session->set_flashdata('success','Jadwal Pelajaran telah berhasil dihapus.');
		redirect(module_url($this->uri->segment(2).'/index/'.$data['semester_kode']));
	}

	public function import()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'import';
		
		$data['dataExcel']				= array();		
		$data['filename']				= '';
		
		if ($this->input->post('importKD')){
			if ($_FILES['userfile']['tmp_name']){
				$ext = end(explode(".", basename($_FILES['userfile']['name'])));
				if ($ext == 'xls'){
					
					$timestamp = explode(" ",microtime());
					$filename = time().str_shuffle('123456ABCDEF');
					
					$uploaddir = './asset/temp_upload/';
					$uploadfname = 'jadwal_pelajaran_'.$filename . '.' . end(explode(".", basename($_FILES['userfile']['name'])));
					$uploadfile = $uploaddir . $uploadfname;
					$data['filename'] = $uploadfname;
					
					if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					
						$this->load->library('excel_reader');
						$this->excel_reader->setOutputEncoding('CP1251');
						$this->excel_reader->read($uploadfile);
						$dataFile = $this->excel_reader->sheets[0];
						
						$paramsReal = array("NO", "TAHUN AJARAN", "SEMESTER", "TINGKAT", "KELAS", "HARI", "JAM MULAI", "JAM SELESAI", "MATA PELAJARAN", "GURU", "RUANG");
						$paramsReal = $iOne = array_combine(range(1, count($paramsReal)), array_values($paramsReal));
						$paramsFile = $dataFile['cells'][2];
						$result 	= array_diff($paramsReal,$paramsFile);
						
						if (count($result) == 0){
							for ($i = 4; $i <= $dataFile['numRows']; $i++) {
								if($dataFile['cells'][$i][1]){
									$data['dataExcel'][$i]['id'] 			= (isset($dataFile['cells'][$i][1]))?$dataFile['cells'][$i][1]:'';
									$data['dataExcel'][$i]['semester'] 	= (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
									$data['dataExcel'][$i]['semester'] 		= (isset($dataFile['cells'][$i][3]))?$dataFile['cells'][$i][3]:'';
									$data['dataExcel'][$i]['tingkat'] 		= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
									$data['dataExcel'][$i]['ruangan'] 		= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
									$data['dataExcel'][$i]['hari'] 			= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
									$data['dataExcel'][$i]['jam_mulai'] 	= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
									$data['dataExcel'][$i]['jam_selesai'] 	= (isset($dataFile['cells'][$i][8]))?$dataFile['cells'][$i][8]:'';
									$data['dataExcel'][$i]['matakuliah']= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
									$data['dataExcel'][$i]['guru'] 			= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
									$data['dataExcel'][$i]['ruangan'] 		= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';

									$valid = 0;
									if ($this->Semester_model->count_all_semester(array('semester_nama'=>$data['dataExcel'][$i]['semester'])) > 0){
										$valid++;
									}

									if ($this->Semester_model->count_all_semester(array('semester_kode'=>$data['dataExcel'][$i]['semester'])) > 0){
										$valid++;
									}

									if ($this->Tingkat_model->count_all_tingkat(array('tingkat_nama'=>$data['dataExcel'][$i]['tingkat'])) > 0){
										$valid++;
									}

									if ($this->Ruangan_model->count_all_ruangan(array('ruangan_nama'=>$data['dataExcel'][$i]['ruangan'])) > 0){
										$valid++;
									}

									if ($this->Staf_model->count_all_dosen("dosen_nama = '".$data['dataExcel'][$i]['guru']."' OR dosen_user = '".$data['dataExcel'][$i]['guru']."' OR dosen_kode = '".$data['dataExcel'][$i]['guru']."'") > 0){
										$get_dosen		= $this->Staf_model->get_dosen("*", "dosen_nama = '".$data['dataExcel'][$i]['guru']."' OR dosen_kode = '".$data['dataExcel'][$i]['guru']."' OR dosen_user = '".$data['dataExcel'][$i]['guru']."'");
										$data['dataExcel'][$i]['guru_nama'] 			= ($get_dosen)?$get_dosen->dosen_nama:'';
										$valid++;
									}

									if ($this->db->query("SELECT * FROM matakuliah WHERE pelajaran_kode = '".$data['dataExcel'][$i]['matakuliah']."'")->num_rows() > 0){
										$valid++;
									}

									if ($valid == 6){
										$data['dataExcel'][$i]['nilai_status'] 		= 'Valid';
									} else {
										$data['dataExcel'][$i]['nilai_status'] 		= 'Not Valid';
									}
								}
							}
						} else {
							$this->session->set_flashdata('error','Format file salah.');
							redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
						}
					} else {
						$this->session->set_flashdata('error','File gagal di-upload.');
						redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
					}
				} else {
					$this->session->set_flashdata('error','Format file yang Anda gunakan salah. Silahkan gunakan format file Excel 97-2003 (.xls).');
					redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
				}
			} else {
				$this->session->set_flashdata('error','Silahkan masukkan file yang akan di-import.');
				redirect(module_url($this->uri->segment(2) . '/' . $this->uri->segment(3)));
			}
		}
		
		if ($this->input->post('save')){
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$this->excel_reader->read('./asset/temp_upload/'.$this->input->post('filename'));
			$dataFile = $this->excel_reader->sheets[0];
			
			for ($i = 4; $i <= $dataFile['numRows']; $i++) {
				if($dataFile['cells'][$i][1]){
					$id 			= (isset($dataFile['cells'][$i][1]))?$dataFile['cells'][$i][1]:'';
					$semester 			= (isset($dataFile['cells'][$i][2]))?$dataFile['cells'][$i][2]:'';
					$semester 		= (isset($dataFile['cells'][$i][3]))?$dataFile['cells'][$i][3]:'';
					$tingkat 		= (isset($dataFile['cells'][$i][4]))?$dataFile['cells'][$i][4]:'';
					$ruangan 			= (isset($dataFile['cells'][$i][5]))?$dataFile['cells'][$i][5]:'';
					$hari 			= (isset($dataFile['cells'][$i][6]))?$dataFile['cells'][$i][6]:'';
					$jam_mulai 		= (isset($dataFile['cells'][$i][7]))?$dataFile['cells'][$i][7]:'';
					$jam_selesai 	= (isset($dataFile['cells'][$i][8]))?$dataFile['cells'][$i][8]:'';
					$matakuliah	= (isset($dataFile['cells'][$i][9]))?$dataFile['cells'][$i][9]:'';
					$guru 			= (isset($dataFile['cells'][$i][10]))?$dataFile['cells'][$i][10]:'';
					$ruangan 		= (isset($dataFile['cells'][$i][11]))?$dataFile['cells'][$i][11]:'';

					$get_semester		= $this->Semester_model->get_semester("*", array("semester_nama"=>$semester));
					$get_semester	= $this->Semester_model->get_semester("*", array("semester_kode"=>$semester));
					$get_tingkat	= $this->Tingkat_model->get_tingkat("*", array("tingkat_nama"=>$tingkat));
					$get_ruangan		= $this->Ruangan_model->get_ruangan("*", array("ruangan_nama"=>$ruangan));
					$get_dosen		= $this->Staf_model->get_dosen("*", "dosen_nama = '".$guru."' OR dosen_kode = '".$guru."' OR dosen_user = '".$guru."'");
					$get_pelajaran = $this->db->query("SELECT * FROM matakuliah WHERE pelajaran_kode = '".$matakuliah."'")->row();
					
					$semester_kode 		= ($get_semester)?$get_semester->semester_kode:'';
					$semester_kode 	= ($get_semester)?$get_semester->semester_kode:'';
					$tingkat_id 	= ($get_tingkat)?$get_tingkat->tingkat_id:'';
					$ruangan_id 		= ($get_ruangan)?$get_ruangan->ruangan_id:'';
					$dosen_id 		= ($get_dosen)?$get_dosen->dosen_id:'';
					$pelajaran_id 	= ($get_pelajaran)?$get_pelajaran->pelajaran_id:'';

					if ($dosen_id && $pelajaran_id && intval($jam_mulai) && intval($jam_selesai)){
						for ($j=intval($jam_mulai); $j <= intval($jam_selesai); $j++) {
							$where = array();
							$where['jam_pelajaran_hari'] 		= $hari;
							$where['jam_pelajaran_ke'] 			= $j;
							$where['semester_kode'] 					= $semester_kode;
							$jam_pelajaran = $this->Jam_pelajaran_model->get_jam_pelajaran("", $where);
							if ($jam_pelajaran){
								$where_update['jadwal_pelajaran.semester_kode'] 				= $semester_kode;
								$where_update['jadwal_pelajaran.semester_kode'] 			= $semester_kode;
								$where_update['jadwal_pelajaran.pelajaran_id'] 			= $pelajaran_id;
								$where_update['jadwal_pelajaran.ruangan_id'] 				= $ruangan_id;
								$where_update['jadwal_pelajaran.dosen_id'] 				= $dosen_id;
								$where_update['jadwal_pelajaran.jadwal_pelajaran_hari'] = $hari;
								$where_update['jadwal_pelajaran.jadwal_pelajaran_ke'] 	= $j;
								if ($this->Jadwal_pelajaran_model->count_all_jadwal_pelajaran($where_update) < 1){
									$insert = array();
									$insert['semester_kode'] 				= $semester_kode;
									$insert['semester_kode'] 				= $semester_kode;
									$insert['pelajaran_id'] 			= $pelajaran_id;
									$insert['ruangan_id'] 				= $ruangan_id;
									$insert['dosen_id'] 					= $dosen_id;
									$insert['jadwal_pelajaran_hari'] 	= $hari;
									$insert['jadwal_pelajaran_urutan'] 	= $jam_pelajaran->jadwal_pelajaran_urutan;
									$insert['jadwal_pelajaran_ke'] 		= $j;
									$insert['jadwal_pelajaran_mulai'] 	= $jam_pelajaran->jam_pelajaran_mulai;
									$insert['jadwal_pelajaran_selesai'] = $jam_pelajaran->jam_pelajaran_selesai;
									$insert['jadwal_pelajaran_ruang'] 	= $ruangan;
									$insert['jadwal_pelajaran_tanggal'] = date('Y-m-d H:i:s');
									$this->Jadwal_pelajaran_model->insert_jadwal_pelajaran($insert);
								} else {
									$update = array();
									$update['jadwal_pelajaran_urutan'] 	= $jam_pelajaran->jadwal_pelajaran_urutan;
									$update['jadwal_pelajaran_mulai'] 	= $jam_pelajaran->jam_pelajaran_mulai;
									$update['jadwal_pelajaran_selesai'] = $jam_pelajaran->jam_pelajaran_selesai;
									$update['jadwal_pelajaran_ruang'] 	= $ruangan;
									$update['jadwal_pelajaran_tanggal'] = date('Y-m-d H:i:s');
									$this->Jadwal_pelajaran_model->update_jadwal_pelajaran($where_update, $update);
								}
							}
						}

						if ($dosen_id && $pelajaran_id){
							if ($this->Guru_pelajaran_model->count_all_guru_pelajaran(array('guru_pelajaran.ruangan_id'=>$ruangan_id, 'guru_pelajaran.semester_kode'=>$semester_kode, 'guru_pelajaran.pelajaran_id'=>$pelajaran_id, 'guru_pelajaran.dosen_id'=>$dosen_id)) > 0){
								$update = array();
								$update['guru_pelajaran_status']	= 'A';
								$this->Guru_pelajaran_model->update_guru_pelajaran(array('ruangan_id'=>$ruangan_id, 'guru_pelajaran.semester_kode'=>$semester_kode, 'pelajaran_id'=>$pelajaran_id), $update);
							} else if ($this->Guru_pelajaran_model->count_all_guru_pelajaran("guru_pelajaran.ruangan_id = '".$ruangan_id."' AND guru_pelajaran.semester_kode = '".$semester_kode."' AND guru_pelajaran.pelajaran_id = '".$pelajaran_id."' AND ( guru_pelajaran.dosen_id IS NULL OR guru_pelajaran.dosen_id = '' ) ") > 0){
								$update = array();
								$update['guru_pelajaran_status']	= 'A';
								$update['dosen_id'] 					= $dosen_id;
								$this->Guru_pelajaran_model->update_guru_pelajaran(array('ruangan_id'=>$ruangan_id, 'guru_pelajaran.semester_kode'=>$semester_kode, 'pelajaran_id'=>$pelajaran_id), $update);
							} else if ($this->Guru_pelajaran_model->count_all_guru_pelajaran(array('guru_pelajaran.ruangan_id'=>$ruangan_id, 'guru_pelajaran.semester_kode'=>$semester_kode, 'guru_pelajaran.pelajaran_id'=>$pelajaran_id, 'guru_pelajaran.dosen_id'=>$dosen_id)) < 1){
								$insert = array();
								$insert['semester_kode'] 				= $semester_kode;
								$insert['dosen_id'] 					= $dosen_id;
								$insert['pelajaran_id'] 			= $pelajaran_id;
								$insert['ruangan_id'] 				= $ruangan_id;
								$insert['guru_pelajaran_tanggal'] 	= date('Y-m-d H:i:s');
								$this->Guru_pelajaran_model->insert_guru_pelajaran($insert);
							}
						}
					}
				}
			}
			
			$this->session->set_flashdata('success','Data jadwal pelajaran telah berhasil di-import.');
			redirect(module_url($this->uri->segment(2)));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/input_jadwal', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function get_event(){
		$semester_kode			= ($this->uri->segment(4))?$this->uri->segment(4):$this->semester_kode;

		if (!isset($_GET['start']) || !isset($_GET['end'])) {
			die("Please provide a date range.");
		}
		
		$range_start = parseDateTime($_GET['start']);
		$range_end = parseDateTime($_GET['end']);
		
		$timezone = null;
		if (isset($_GET['timezone'])) {
			$timezone = new DateTimeZone($_GET['timezone']);
		}
		
		$query = $this->db->query("SELECT jadwal_pelajaran.*, semester.semester_nama, matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, ruangan.ruangan_kode, ruangan.ruangan_nama, dosen.dosen_nama 
					FROM akd_jadwal_pelajaran jadwal_pelajaran 
					LEFT JOIN akd_semester semester ON jadwal_pelajaran.semester_kode=semester.semester_kode 
					LEFT JOIN akd_dosen dosen ON jadwal_pelajaran.dosen_id=dosen.dosen_id 
					LEFT JOIN akd_matakuliah matakuliah ON jadwal_pelajaran.matakuliah_id=matakuliah.matakuliah_id 
					LEFT JOIN akd_ruangan ruangan ON jadwal_pelajaran.ruangan_id=ruangan.ruangan_id 
				WHERE jadwal_pelajaran_status = 'A' 
					AND jadwal_pelajaran.semester_kode = '$semester_kode'")->result();
		
		$start_date = date("Y-m-d", strtotime($_GET['start']));
		$dayName = array('1'=>'Senin', '2'=>'Selasa', '3'=>'Rabu', '4'=>'Kamis', '5'=>'Jumat', '6'=>'Sabtu', '7'=>'Minggu');
		
		// Accumulate an output array of event data arrays.
		$output_arrays = array();
		foreach ($query as $row) {
			$addDays = $row->jadwal_pelajaran_hari - 1;
			$param = array();
			$param['id']							= $row->jadwal_pelajaran_id;
			$param['semester_kode']		= $row->semester_kode;
			$param['semester_nama']		= $row->semester_nama;

			$param['matakuliah_id']		= $row->matakuliah_id;
			$param['matakuliah_kode']	= $row->matakuliah_kode;
			$param['matakuliah_nama']	= $row->matakuliah_nama;

			$param['ruangan_id']			= $row->ruangan_id;
			$param['ruangan_kode']		= $row->ruangan_kode;
			$param['ruangan_nama']		= $row->ruangan_nama;

			$param['dosen_id']				= $row->dosen_id;
			$param['dosen_nama']			= $row->dosen_nama;

			$param['hari_nama']							= (array_key_exists($row->jadwal_pelajaran_hari, $dayName))?$dayName[$row->jadwal_pelajaran_hari]:0;
			$param['jadwal_mulai']					= substr($row->jadwal_pelajaran_mulai, 0, 5);
			$param['jadwal_selesai']				= substr($row->jadwal_pelajaran_selesai, 0, 5);
			$param['jadwal_pelajaran_hari']	= $row->jadwal_pelajaran_hari;

			$param['title']					= $row->ruangan_kode;
			$param['description']		= "";
			// $param['description']		= $row->matakuliah_kode .' - '. $row->matakuliah_nama .' ('. $row->dosen_nama .')';

			$param['start']				= date("Y-m-d", strtotime($_GET['start'] . " + $addDays days")) . ' ' . $row->jadwal_pelajaran_mulai;
			$param['end']					= date("Y-m-d", strtotime($_GET['start'] . " + $addDays days")) . ' ' . $row->jadwal_pelajaran_selesai;
			$param['tanggal']			= date("Y-m-d", strtotime($_GET['start'] . " + $addDays days"));
			$param['presensi_aktif']	= 'N';

			$output_arrays[] = $param;
		}
		
		// Send JSON to the client.
		header('Content-Type: application/json');
		echo json_encode($output_arrays);
	}
}