<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ruangan extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Ruangan | ' . profile('profil_website');
		$this->active_menu		= 257;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Ruangan_model');
    }
	
	function datatable()
	{	
		$this->load->library('Datatables');
		$this->datatables->select('ruangan_id, ruangan_kode, ruangan_nama, ruangan_lokasi, ruangan_kapasitas')
		->add_column('Actions', $this->get_buttons('$1'),'ruangan_id')
		->search_column('ruangan_nama')
		->from('akd_ruangan');
        echo $this->datatables->generate();
    }
	
	function get_buttons($id)
	{
		$ci= & get_instance();
		$ci->load->helper('url');
		$html  = '<div class="text-center">';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/detail/'.$id) .'" class="btn btn-primary btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Detail"><i class="fa fa-file-text"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/edit/'.$id) .'" class="btn btn-warning btn-sm" style="margin-right:5px;margin-bottom:5px;" title="Ubah"><i class="fa fa-pencil"></i></a>';
		$html .= '<a href="'. site_url($ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/delete/'.$id) .'" class="btn btn-danger btn-sm" style="margin-right:5px;margin-bottom:5px;" onclick="return confirm(\'Apakah Anda yakin? \nAkan menghapus data ini.\');"><i class="fa fa-trash-o"></i></a>';
		$html .= '</div>';
		return $html;
	}

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/ruangan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';

		$data['ruangan_id']			= ($this->input->post('ruangan_id'))?$this->input->post('ruangan_id'):'';
		$data['ruangan_kode']		= ($this->input->post('ruangan_kode'))?$this->input->post('ruangan_kode'):'';
		$data['ruangan_kapasitas']	= ($this->input->post('ruangan_kapasitas'))?$this->input->post('ruangan_kapasitas'):'';
		$data['ruangan_nama']		= ($this->input->post('ruangan_nama'))?$this->input->post('ruangan_nama'):'';
		$data['ruangan_lokasi']		= ($this->input->post('ruangan_lokasi'))?$this->input->post('ruangan_lokasi'):'';
		$save					= $this->input->post('save');
		if ($save == 'save'){			
			$insert['ruangan_id']			= $this->uuid->v4();
			$insert['ruangan_kode']			= validasi_input($this->input->post('ruangan_kode'));
			$insert['ruangan_kapasitas']	= validasi_input($this->input->post('ruangan_kapasitas'));
			$insert['ruangan_nama']			= validasi_input($this->input->post('ruangan_nama'));
			$insert['ruangan_lokasi']		= validasi_input($this->input->post('ruangan_lokasi'));
			$this->Ruangan_model->insert_ruangan($insert);
			
			$this->session->set_flashdata('success','Ruangan telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index'));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/ruangan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function edit()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'edit';
		
		$where['ruangan_id']		= validasi_sql($this->uri->segment(4)); 
		$ruangan 					= $this->Ruangan_model->get_ruangan('*', $where);

		$data['ruangan_id']			= ($this->input->post('ruangan_id'))?$this->input->post('ruangan_id'):$ruangan->ruangan_id;
		$data['ruangan_kode']		= ($this->input->post('ruangan_kode'))?$this->input->post('ruangan_kode'):$ruangan->ruangan_kode;
		$data['ruangan_kapasitas']	= ($this->input->post('ruangan_kapasitas'))?$this->input->post('ruangan_kapasitas'):$ruangan->ruangan_kapasitas;
		$data['ruangan_nama']		= ($this->input->post('ruangan_nama'))?$this->input->post('ruangan_nama'):$ruangan->ruangan_nama;
		$data['ruangan_lokasi']		= ($this->input->post('ruangan_lokasi'))?$this->input->post('ruangan_lokasi'):$ruangan->ruangan_lokasi;
		$save						= $this->input->post('save');
		if ($save == 'save'){
			$where_update['ruangan_id']		= validasi_sql($this->input->post('ruangan_id'));
			$update['ruangan_kode']			= validasi_input($this->input->post('ruangan_kode'));
			$update['ruangan_kapasitas']	= validasi_input($this->input->post('ruangan_kapasitas'));
			$update['ruangan_nama']			= validasi_input($this->input->post('ruangan_nama'));
			$update['ruangan_lokasi']		= validasi_input($this->input->post('ruangan_lokasi'));
			$this->Ruangan_model->update_ruangan($where_update, $update);
			
			$this->session->set_flashdata('success','Ruangan telah berhasil diubah.');
			redirect(module_url($this->uri->segment(2).'/index'));
		}
	
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/ruangan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	public function delete()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$where['ruangan_id']		= validasi_sql($this->uri->segment(4)); 
		$ruangan 					= $this->Ruangan_model->get_ruangan('*', $where);

		if ($ruangan){
			$where_delete['ruangan_id']	= validasi_sql($this->uri->segment(4));
			$this->Ruangan_model->delete_ruangan($where_delete);
			
			$this->session->set_flashdata('success','Ruangan telah berhasil dihapus.');
			redirect(module_url($this->uri->segment(2)));
		} else {
			$this->session->set_flashdata('error','Ruangan gagal dihapus.');
			redirect(module_url($this->uri->segment(2)));
		}
	}
	
	public function detail()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'detail';
		
		$where['ruangan_id']	= validasi_sql($this->uri->segment(4)); 
		$ruangan		 		= $this->Ruangan_model->get_ruangan('*', $where);
		
		$data['ruangan_id']			= $ruangan->ruangan_id;
		$data['ruangan_kode']		= $ruangan->ruangan_kode;
		$data['ruangan_kapasitas']	= $ruangan->ruangan_kapasitas;
		$data['ruangan_nama']		= $ruangan->ruangan_nama;
		$data['ruangan_lokasi']		= $ruangan->ruangan_lokasi;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/ruangan', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
}