<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jam_pelajaran extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $semester_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Jam Pelajaran | ' . profile('profil_website');
		$this->active_menu		= 362;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Jam_pelajaran_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		
		$this->semester_kode = $this->Semester_model->get_semester_aktif()->semester_kode;
    }
	
	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';

		$semester_kode				= ($this->uri->segment(4))?$this->uri->segment(4):$this->semester_kode;
		
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jam_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function add()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'add';
		
		$semester_kode				= ($this->uri->segment(4))?$this->uri->segment(4):$this->semester_kode;
		$jam_pelajaran_hari			= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		
		$where['semester_kode']			= validasi_sql($semester_kode); 
		$where['jam_pelajaran_hari']	= validasi_sql($jam_pelajaran_hari); 
		$jam_pelajaran 					= $this->Jam_pelajaran_model->get_jam_pelajaran('*', $where);

		if ($jam_pelajaran){
			$data['semester_kode']				= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$jam_pelajaran->semester_kode;
			$data['jam_pelajaran_hari'] 	= ($this->input->post('jam_pelajaran_hari'))?$this->input->post('jam_pelajaran_hari'):$jam_pelajaran->jam_pelajaran_hari;
			$data['jam_pelajaran_mulai'] 	= ($this->input->post('jam_pelajaran_mulai'))?$this->input->post('jam_pelajaran_mulai'):'07:00:00';
			$data['jam_pelajaran_selesai']	= ($this->input->post('jam_pelajaran_selesai'))?$this->input->post('jam_pelajaran_selesai'):'07:00:00';
		} else {
			$data['semester_kode']				= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
			$data['jam_pelajaran_hari'] 	= ($this->input->post('jam_pelajaran_hari'))?$this->input->post('jam_pelajaran_hari'):$jam_pelajaran_hari;
			$data['jam_pelajaran_mulai'] 	= ($this->input->post('jam_pelajaran_mulai'))?$this->input->post('jam_pelajaran_mulai'):'07:00:00';
			$data['jam_pelajaran_selesai']	= ($this->input->post('jam_pelajaran_selesai'))?$this->input->post('jam_pelajaran_selesai'):'07:00:00';
		}

		$save					= $this->input->post('save');
		if ($save == 'save'){
			$jam_pelajaran_info 	= $this->input->post('jam_pelajaran_info');
			$jam_pelajaran_mulai 	= $this->input->post('jam_pelajaran_mulai');
			$jam_pelajaran_selesai 	= $this->input->post('jam_pelajaran_selesai');
			$jam_pelajaran_urutan 	= $this->input->post('jam_pelajaran_urutan');
			$jam_pelajaran_ke 		= $this->input->post('jam_pelajaran_ke');
			
			for ($i=1; $i < 20; $i++) {
				if (array_key_exists($i, $jam_pelajaran_urutan)){
					$where = array();
					$where['semester_kode'] 				= $this->input->post('semester_kode');
					$where['jam_pelajaran_hari'] 	= $this->input->post('jam_pelajaran_hari');
					$where['jam_pelajaran_urutan'] 	= $i;
					if ($this->Jam_pelajaran_model->count_all_jam_pelajaran($where) < 1){
						$insert = array();
						$insert['semester_kode'] 			= $this->input->post('semester_kode');
						$insert['jam_pelajaran_id'] 		= $this->uuid->v4();
						$insert['jam_pelajaran_hari'] 		= $this->input->post('jam_pelajaran_hari');
						$insert['jam_pelajaran_info'] 		= $jam_pelajaran_info[$i];
						$insert['jam_pelajaran_mulai'] 		= $jam_pelajaran_mulai[$i];
						$insert['jam_pelajaran_selesai'] 	= $jam_pelajaran_selesai[$i];
						$insert['jam_pelajaran_urutan'] 	= $i;
						$insert['jam_pelajaran_ke'] 		= ($jam_pelajaran_ke[$i] || $jam_pelajaran_ke[$i] == 0)?$jam_pelajaran_ke[$i]:null;
						$insert['created_by'] 				= userdata('pengguna_id');
						$this->Jam_pelajaran_model->insert_jam_pelajaran($insert);
					} else {
						$update = array();
						$update['jam_pelajaran_info'] 		= $jam_pelajaran_info[$i];
						$update['jam_pelajaran_mulai'] 		= $jam_pelajaran_mulai[$i];
						$update['jam_pelajaran_selesai'] 	= $jam_pelajaran_selesai[$i];
						$update['jam_pelajaran_urutan'] 	= $i;
						$update['jam_pelajaran_ke'] 		= ($jam_pelajaran_ke[$i] || $jam_pelajaran_ke[$i] == 0)?$jam_pelajaran_ke[$i]:null;
						$update['created_by'] 				= userdata('pengguna_id');
						$this->Jam_pelajaran_model->update_jam_pelajaran($where, $update);
					}
				} else {
					$where = array();
					$where['semester_kode'] 		= $this->input->post('semester_kode');
					$where['jam_pelajaran_hari'] 	= $this->input->post('jam_pelajaran_hari');
					$where['jam_pelajaran_urutan'] 	= $i;
					$this->Jam_pelajaran_model->delete_jam_pelajaran($where);
				}
			}
			
			$this->session->set_flashdata('success','Jam Pelajaran telah berhasil ditambah.');
			redirect(module_url($this->uri->segment(2).'/index/'.$this->input->post('semester_kode')));
		}
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/jam_pelajaran', $data);
		$this->load->view(module_dir().'/separate/foot');
	}

	public function get_event(){
		$semester_kode = ($this->uri->segment(4))?$this->uri->segment(4):$this->semester_kode;

		if (!isset($_GET['start']) || !isset($_GET['end'])) {
			die("Please provide a date range.");
		}
		
		// Parse the start/end parameters.
		// These are assumed to be ISO8601 strings with no time nor timezone, like "2013-12-29".
		// Since no timezone will be present, they will parsed as UTC.
		$range_start = parseDateTime($_GET['start']);
		$range_end = parseDateTime($_GET['end']);
		
		// Parse the timezone parameter if it is present.
		$timezone = null;
		if (isset($_GET['timezone'])) {
			$timezone = new DateTimeZone($_GET['timezone']);
		}
		
		$query = $this->db->query("SELECT jam_pelajaran.*, akd_semester.semester_nama FROM akd_jam_pelajaran jam_pelajaran 
					LEFT JOIN akd_semester ON jam_pelajaran.semester_kode=akd_semester.semester_kode 
				WHERE jam_pelajaran.semester_kode = '$semester_kode'")->result();
		
		$start_date = date("Y-m-d", strtotime($_GET['start']));
		$dayName = array('1'=>'Senin', '2'=>'Selasa', '3'=>'Rabu', '4'=>'Kamis', '5'=>'Jumat', '6'=>'Sabtu', '7'=>'Minggu');
		
		// Accumulate an output array of event data arrays.
		$output_arrays = array();
		foreach ($query as $row) {
			$addDays = $row->jam_pelajaran_hari - 1;
			$param = array();
			$param['id']				= $row->jam_pelajaran_id;
			$param['semester_kode']			= $row->semester_kode;
			$param['semester_nama']		= $row->semester_nama;
			$param['hari_id']			= $row->jam_pelajaran_hari;
			$param['hari_nama']			= (array_key_exists($row->jam_pelajaran_hari,$dayName))?$dayName[$row->jam_pelajaran_hari]:0;
			$param['jam_mulai']			= substr($row->jam_pelajaran_mulai, 0, 5);
			$param['jam_selesai']		= substr($row->jam_pelajaran_selesai, 0, 5);
			$param['jam_ke']			= $row->jam_pelajaran_ke;
			$param['jam_urutan']		= $row->jam_pelajaran_urutan;
			$param['jam_info']			= $row->jam_pelajaran_info;

			$param['title']				= $row->jam_pelajaran_info;
			$param['description']		= '';

			$param['start']				= date("Y-m-d", strtotime($_GET['start'] . " + $addDays days")) . ' ' . $row->jam_pelajaran_mulai;
			$param['end']				= date("Y-m-d", strtotime($_GET['start'] . " + $addDays days")) . ' ' . $row->jam_pelajaran_selesai;
			$param['tanggal']			= date("Y-m-d", strtotime($_GET['start'] . " + $addDays days"));
			
			// $param['backgroundColor']	= '#6BA5C1';
			// $param['borderColor']		= '#3A87AD';

			if ($row->jam_pelajaran_info == 'ISTIRAHAT'){
				$param['backgroundColor']	= '#f39c12';
				$param['borderColor']		= '#f39c12';
			}

			$output_arrays[] = $param;
		}
		
		// Send JSON to the client.
		header('Content-Type: application/json');
		echo json_encode($output_arrays);
	}
}
