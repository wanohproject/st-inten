<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi_perwalian extends MX_Controller {
	
	public $title;
	public $content;
	public $active_menu;
	private $tahun_kode;
	
	public function __construct()
    {
    	parent::__construct();
		$this->title			= 'Registrasi_perwalian | ' . profile('profil_website');
		$this->active_menu		= 301;
		
		$this->load->helper('mod_constant');
		$this->load->library('authentication');
		$this->authentication->set_menu($this->active_menu);
		$this->authentication->permission();
		
		$this->load->model('Perguruan_tinggi_model');
		$this->load->model('Program_studi_model');
		$this->load->model('Datatable_model');
		$this->load->model('Referensi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Tahun_model');
		$this->load->model('Semester_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Matakuliah_model');
		$this->load->model('Perwalian_model');
		
		$this->tahun_kode			= $this->Tahun_model->get_tahun_aktif()->tahun_kode;
    }

	public function index()
	{
		$head['title']		= $this->title;
		$sidebar['active']	= $this->active_menu;
		$data['action']		= 'grid';
		
		$program_studi_id 				= ($this->uri->segment(4))?$this->uri->segment(4):'-';
		$data['program_studi_id']		= ($this->input->post('program_studi_id'))?$this->input->post('program_studi_id'):$program_studi_id;
		
		$tahun_kode				= ($this->uri->segment(5))?$this->uri->segment(5):'-';
		$data['tahun_kode']		= ($this->input->post('tahun_kode'))?$this->input->post('tahun_kode'):$tahun_kode;
		
		$semester = $this->Semester_model->get_semester_aktif()->semester_kode;
		$semester_kode				= ($this->uri->segment(6))?$this->uri->segment(6):$semester;
		$data['semester_kode']		= ($this->input->post('semester_kode'))?$this->input->post('semester_kode'):$semester_kode;
		
		$this->load->view(module_dir().'/separate/head', $head);
		$this->load->view(module_dir().'/separate/header');
		$this->load->view(module_dir().'/separate/sidebar',$sidebar);
		$this->load->view(module_dir().'/page/registrasi_perwalian', $data);
		$this->load->view(module_dir().'/separate/foot');
	}
	
	public function get_mahasiswa(){
		$data = array();
		$program_studi_id = $this->input->post('program_studi');
		$tahun_kode = $this->input->post('tahun');
		$semester_kode = $this->input->post('semester');
		if ($program_studi_id){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$tahun = $this->Tahun_model->get_tahun("*", array('tahun_kode'=>$tahun_kode));
			if ($program_studi){
				$where = "(mahasiswa.mahasiswa_tanggal_lulus IS NULL OR mahasiswa.mahasiswa_tanggal_lulus = '0000-00-00') AND mahasiswa.program_studi_id = '$program_studi_id' AND (SELECT COUNT(akd_perwalian.perwalian_id) FROM akd_perwalian WHERE akd_perwalian.mahasiswa_id=mahasiswa.mahasiswa_id AND akd_perwalian.semester_kode='$semester_kode') < 1";

				if ($tahun && $tahun_kode){
					$where .= " AND mahasiswa_tahun_masuk = '$tahun_kode'";
				}
				
				$mahasiswa = $this->db->query("SELECT * FROM akd_mahasiswa mahasiswa WHERE $where ORDER BY mahasiswa_nim ASC")->result();
				if ($mahasiswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $mahasiswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_mahasiswa_filter(){
		$data = array();
		$program_studi_id = $this->input->post('program_studi');
		$tahun_kode = $this->input->post('tahun');
		$semester_kode = $this->input->post('semester');
		$filter = $this->input->post('filter');
		if ($program_studi_id){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$tahun = $this->Tahun_model->get_tahun("*", array('tahun_kode'=>$tahun_kode));
			if ($program_studi){
				$where = "(mahasiswa.mahasiswa_tanggal_lulus IS NULL OR mahasiswa.mahasiswa_tanggal_lulus = '0000-00-00') AND mahasiswa.program_studi_id = '$program_studi_id' AND (SELECT COUNT(akd_perwalian.perwalian_id) FROM akd_perwalian WHERE akd_perwalian.mahasiswa_id=mahasiswa.mahasiswa_id AND akd_perwalian.semester_kode='$semester_kode') < 1";
				
				if ($filter){
					$where .= " AND (mahasiswa_nama LIKE '%$filter%' OR mahasiswa_nim LIKE '%$filter%')";
				}
				if ($tahun && $tahun_kode){
					$where .= " AND mahasiswa_tahun_masuk = '$tahun_kode'";
				}
				
				$mahasiswa = $this->db->query("SELECT * FROM akd_mahasiswa mahasiswa WHERE $where ORDER BY mahasiswa_nim ASC")->result();
				if ($mahasiswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $mahasiswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function get_registrasi(){
		$data = array();
		$program_studi_id = $this->input->post('program_studi');
		$tahun_kode = $this->input->post('tahun');
		$semester_kode = $this->input->post('semester');
		if ($program_studi_id && $semester_kode){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$tahun = $this->Tahun_model->get_tahun("*", array('tahun_kode'=>$tahun_kode));
			$semester = $this->Semester_model->get_semester("*", array('semester_kode'=>$semester_kode));
			if ($program_studi && $semester_kode){
				$where = "(mahasiswa.mahasiswa_tanggal_lulus IS NULL OR mahasiswa.mahasiswa_tanggal_lulus = '0000-00-00') AND mahasiswa.program_studi_id = '$program_studi_id' AND perwalian.semester_kode = '$semester_kode'";
				
				if ($tahun && $tahun_kode){
					$where .= " AND mahasiswa_tahun_masuk = '$tahun_kode'";
				}
				
				$mahasiswa = $this->db->query("SELECT * FROM akd_perwalian perwalian LEFT JOIN akd_mahasiswa mahasiswa ON perwalian.mahasiswa_id=mahasiswa.mahasiswa_id WHERE $where ORDER BY mahasiswa_nim ASC")->result();
				if ($mahasiswa){
					$data['response']	= true;
					$data['message']	= "Data sukses";
					$data['data']		= $mahasiswa;
				} else {
					$data['response']	= false;
					$data['message']	= "Data tidak ada.";
				}
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function set_mahasiswa(){
		$data = array();
		$mahasiswa_id 	= $this->input->post('mahasiswa');
		$program_studi_id = $this->input->post('program_studi');
		$tahun_kode = $this->input->post('tahun');
		$semester_kode = $this->input->post('semester');
		
		if ($mahasiswa_id && $program_studi_id && $semester_kode){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$tahun = $this->Tahun_model->get_tahun("*", array('tahun_kode'=>$tahun_kode));
			$semester = $this->Semester_model->get_semester("*", array('semester_kode'=>$semester_kode));
			$mahasiswa = $this->Mahasiswa_model->get_mahasiswa("*", array('mahasiswa_id'=>$mahasiswa_id));

			if ($program_studi && $semester && $mahasiswa){
				$where_perwalian['perwalian.mahasiswa_id'] = $mahasiswa->mahasiswa_id;
				$where_perwalian['perwalian.semester_kode'] = $semester->semester_kode;
				if ($this->Perwalian_model->count_all_perwalian($where_perwalian) < 1){
					$insert_perwalian = array();
					$insert_perwalian['perguruan_tinggi_id']	= $mahasiswa->perguruan_tinggi_id;
					$insert_perwalian['program_studi_id']		= $mahasiswa->program_studi_id;
					$insert_perwalian['jenjang_kode']			= $mahasiswa->jenjang_kode;
					$insert_perwalian['perguruan_tinggi_id']	= $mahasiswa->perguruan_tinggi_id;
					$insert_perwalian['dosen_id']				= $mahasiswa->mahasiswa_dosen_wali;
					$insert_perwalian['mahasiswa_id']			= $mahasiswa->mahasiswa_id;
					$insert_perwalian['semester_kode']			= $semester->semester_kode;
					$insert_perwalian['perwalian_id']			= $this->uuid->v4();
					$insert_perwalian['perwalian_tanggal']		= date('Y-m-d');
					$insert_perwalian['perwalian_jenis']		= "RS";
					$insert_perwalian['created_by']				= userdata('pengguna_id');
					$this->Perwalian_model->insert_perwalian($insert_perwalian);
				}

				$data['response']	= true;
				$data['message']	= "Data berhasil disimpan.";
				$data['params']		= array("mahasiswa_id"=>$mahasiswa_id,
											"program_studi_id"=>$program_studi_id,
											"tahun_kode"=>$tahun_kode,
											"semester_kode"=>$semester_kode);
			} else {
				$data['response']	= false;
				$data['message']	= "Data semester atau mahasiswa tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
	
	public function remove_mahasiswa(){
		$data = array();
		$mahasiswa_id 	= $this->input->post('mahasiswa');
		$program_studi_id = $this->input->post('program_studi');
		$tahun_kode = $this->input->post('tahun');
		$semester_kode = $this->input->post('semester');
		
		if ($mahasiswa_id && $program_studi_id && $semester_kode){
			$program_studi = $this->Program_studi_model->get_program_studi("program_studi_id", array('program_studi_id'=>$program_studi_id));
			$tahun = $this->Tahun_model->get_tahun("*", array('tahun_kode'=>$tahun_kode));
			$semester = $this->Semester_model->get_semester("*", array('semester_kode'=>$semester_kode));
			$mahasiswa = $this->Mahasiswa_model->get_mahasiswa("*", array('mahasiswa_id'=>$mahasiswa_id));
			$registrasi_mahasiswa = $this->Perwalian_model->get_perwalian("*", array('perwalian.mahasiswa_id'=>$mahasiswa_id, 'perwalian.semester_kode'=>$semester_kode, 'approve_akademik_status'=>'N'));

			if ($program_studi && $semester && $mahasiswa && $registrasi_mahasiswa){
				$query_delete_perwalian = $this->Perwalian_model->delete_perwalian(array('mahasiswa_id'=>$mahasiswa_id, 'semester_kode'=>$semester_kode, 'approve_akademik_status'=>'N'));
				
				$data['response']	= true;
				$data['message']	= "Data berhasil disimpan.";
				$data['params']		= array("mahasiswa_id"=>$mahasiswa_id,
											"program_studi_id"=>$program_studi_id,
											"tahun_kode"=>$tahun_kode,
											"semester_kode"=>$semester_kode);
			} else {
				$data['response']	= false;
				$data['message']	= "Data tidak ada.";
			}
		} else {
			$data['response']	= false;
			$data['message']	= "Parameter tidak lengkap.";
		}
		echo json_encode($data);
	}
}
