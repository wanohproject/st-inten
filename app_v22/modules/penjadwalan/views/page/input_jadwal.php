<?php
if ($action == '' || $action == 'grid'){
?>
<script>
  $(document).ready(function() {

    $('#detailModal').on('shown.bs.modal', function (e) {
			console.log("Load Detail Modal");
    });

	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show');
		<?php } else { ?>
			$('#errorModal').modal('show');
		<?php } ?>
	<?php } ?>
  });
</script>
<style>
#calendar {
    max-width: 100%;
    margin: 0 auto;
}
</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Jadwal Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('jadwal-pelajaran'); ?>">Jadwal Pelajaran</a></li>
            <li class="active">Daftar Jadwal Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Jadwal Pelajaran</h3>
									<div class="pull-right">
										<a href="<?php echo module_url($this->uri->segment(2).'/set/'.$semester_kode.'/'.$ruangan_id); ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah</a>
									</div>
                </div><!-- /.box-header -->
								<div class="box-body">
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$semester_kode.'/'.$ruangan_id);?>" method="post">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="semester_kode" class="control-label">Semester</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM akd_semester ORDER BY semester_kode DESC")->result(), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', 'none', 'class="form-control select2" required'); ?>
												</div>		
											</div>
										</div>
											
										<div class="col-md-4">
											<div class="form-group">
												<label for="ruangan_id" class="control-label">Ruangan</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM akd_ruangan ORDER BY ruangan_nama ASC")->result(), 'ruangan_id', 'ruangan_id', 'ruangan_nama', $ruangan_id, 'submit();', '-- PILIH RUANGAN --', 'class="form-control select2" required'); ?>
												</div>		
											</div>
										</div>
									</div>
								</div>
                <div class="box-body">
									<div style="overflow-x:auto;">
										<table class="table table-bordered table-report">
											<thead>
												<tr>
													<th width="80">&nbsp;</th>
													<?php
													if ($dayName) {
														foreach ($dayName as $key_dayName => $value_dayName) {
															echo "<th class=\"text-center\" width=\"15.3%\">$value_dayName</th>";
														}
													}
													?>
												</tr>
											</thead>
											<tbody>
											<?php
											if ($grid_jam) {
												foreach ($grid_jam as $row_jam) {
													?>
													<tr>
														<td class="text-center" style="line-height:1em;"><strong><?php echo $row_jam->jam_pelajaran_ke; ?></strong><br /><small><?php echo substr($row_jam->jam_pelajaran_mulai, 0, 5); ?>-<?php echo substr($row_jam->jam_pelajaran_selesai, 0, 5); ?></small></td>
														<?php
														if ($dayName) {
															foreach ($dayName as $key_dayName => $value_dayName) {
																if ($ruangan_id != '-'){
																	$where_jadwal_pelajaran = array();
																	$where_jadwal_pelajaran['jadwal_pelajaran.ruangan_id']= $ruangan_id;
																	$where_jadwal_pelajaran['jadwal_pelajaran.jadwal_pelajaran_ke']	= $row_jam->jam_pelajaran_ke;
																	$where_jadwal_pelajaran['jadwal_pelajaran.jadwal_pelajaran_hari']	= $key_dayName;
																	$get_jadwal_pelajaran = $this->Jadwal_pelajaran_model->get_jadwal_pelajaran("", $where_jadwal_pelajaran);
																	if ($get_jadwal_pelajaran){
																		echo "<td class=\"text-center\" style=\"line-height:1em;\">$get_jadwal_pelajaran->matakuliah_kode<br /><small><i>$get_jadwal_pelajaran->dosen_nama</i></small></td>";
																	} else {
																		echo "<td class=\"text-center\" style=\"line-height:1em;\">&nbsp;</td>";
																	}
																} else {
																	// $label = "";
																	// $grid_ruangan = $this->db->query("SELECT * FROM akd_ruangan ORDER BY ruangan_kode ASC")->result();
																	// if ($grid_ruangan){
																	// 	foreach ($grid_ruangan as $row_ruangan) {
																	// 		$where_jadwal_pelajaran = array();
																	// 		$where_jadwal_pelajaran['jadwal_pelajaran.ruangan_id']= $row_ruangan->ruangan_id;
																	// 		$where_jadwal_pelajaran['jadwal_pelajaran.jadwal_pelajaran_ke']	= $row_jam->jam_pelajaran_ke;
																	// 		$where_jadwal_pelajaran['jadwal_pelajaran.jadwal_pelajaran_hari']	= $key_dayName;
																	// 		$get_jadwal_pelajaran = $this->Jadwal_pelajaran_model->get_jadwal_pelajaran("", $where_jadwal_pelajaran);
																	// 		if ($get_jadwal_pelajaran){
																	// 			$label .= "<span class=\"label label-success\">$row_ruangan->ruangan_kode</span> ";
																	// 		} else {
																	// 			$label .= "<span class=\"label label-danger\">$row_ruangan->ruangan_kode</span> ";
																	// 		}
																	// 	}
																	// 	echo "<td class=\"text-center\" style=\"line-height:1em;\">$label</td>";
																	// } else {
																		echo "<td class=\"text-center\" style=\"line-height:1em;\">&nbsp;</td>";
																	// }
																}
															}
														}
														?>
													</tr>
													<?php
												}
											}
											?>
											</tbody>
										</table>
									</div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
			
	<div class="modal fade modal-primary" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Detail Jadwal</h4>
		  </div>
		  <div class="modal-body">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group row">
                  <label class="col-sm-3 control-label">Tahun</label>
                  <label class="col-sm-8 control-label" style="text-align: left;" id="tahun_nama"></label>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 control-label">Semester</label>
                  <label class="col-sm-8 control-label" style="text-align: left;" id="semester_nama"></label>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 control-label">Kelas</label>
                  <label class="col-sm-8 control-label" style="text-align: left;" id="ruangan_nama"></label>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 control-label">Mata Pelajaran</label>
                  <label class="col-sm-8 control-label" style="text-align: left;" id="pelajaran_nama"></label>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 control-label">Guru</label>
                  <label class="col-sm-8 control-label" style="text-align: left;" id="guru_nama"></label>
                </div>
                <div class="form-group row">
									<label class="col-sm-3 control-label">Jadwal</label>
                  <label class="col-sm-8 control-label" style="text-align: left;"><span id="hari_nama"></span>, <span id="jadwal_mulai"></span> - <span id="jadwal_selesai"></span></label>
                </div>
                <div class="form-group row">
									<label class="col-sm-3 control-label">Ruang</label>
									<label class="col-sm-8 control-label" style="text-align: left;" id="ruangan_nama"></label>
                </div>
              </div>
          </form>
		  </div>
		  <div class="modal-footer">
			<a href="#" id="action-edit" class="btn btn-warning pull-left">Ubah</a>
			<a href="#" id="action-delete" class="btn btn-danger pull-left" onclick="return confirm('Apakah Anda yakin akan menghapus data?'); ">Hapus</a>
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'set') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('#jadwal_pelajaran_mulai').datetimepicker({
        sideBySide: true,
        format: 'HH:mm:ss'
    });
    $('#jadwal_pelajaran_selesai').datetimepicker({
        sideBySide: true,
        format: 'HH:mm:ss'
    });
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Jadwal Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('jadwal-pelajaran'); ?>">Jadwal Pelajaran</a></li>
            <li class="active">Atur Jadwal Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Atur Jadwal Pelajaran</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
								<form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$semester_kode.'/'.$ruangan_id);?>" method="post">
								<input type="hidden" name="viewby" value="<?php echo $viewby; ?>" />
								<div class="box-body">
									<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label for="semester_kode" class="control-label">Semester</label>
													<div class="">
														<?php combobox('db', $this->db->query("SELECT * FROM akd_semester ORDER BY semester_kode DESC")->result(), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', 'none', 'class="form-control select2" required'); ?>
													</div>		
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label for="ruangan_id" class="control-label">Ruangan</label>
													<div class="">
														<?php combobox('db', $this->db->query("SELECT * FROM akd_ruangan ORDER BY ruangan_nama ASC")->result(), 'ruangan_id', 'ruangan_id', 'ruangan_nama', $ruangan_id, 'submit();', '', 'class="form-control select2" required'); ?>
													</div>		
												</div>
											</div>

											<div class="col-md-4">
												<div class="form-group">
													<label for="tingkat_id" class="control-label">Hari</label>
													<div class="">
														<?php combobox('2d', array('1'=>'Senin', '2'=>'Selasa', '3'=>'Rabu', '4'=>'Kamis', '5'=>'Jumat', '6'=>'Sabtu', '7'=>'Minggu'), 'jadwal_pelajaran_hari', '', '', $jadwal_pelajaran_hari, 'submit();', '', 'class="form-control select2" required');?>
													</div>		
												</div>
											</div>
										</div>

										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="text-align:center;" width="80">Jam Ke-</th>
													<th style="text-align:center;">Jam Mulai</th>
													<th style="text-align:center;">Jam Selesai</th>
													<th style="text-align:center;" width="50%">Mata Kuliah / Dosen</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$where['semester_kode']				= $semester_kode;
												$where['jam_pelajaran_hari']	= $jadwal_pelajaran_hari;
												$grid_jam = $this->Jam_pelajaran_model->grid_all_jam_pelajaran("", "jam_pelajaran_urutan", "ASC", 0, 0, $where);
												if ($grid_jam){
													foreach ($grid_jam as $row_jam) {
														$where_jadwal['jadwal_pelajaran.semester_kode'] 				= $semester_kode;
														$where_jadwal['jadwal_pelajaran.ruangan_id'] 						= $ruangan_id;
														$where_jadwal['jadwal_pelajaran.jadwal_pelajaran_hari'] = $jadwal_pelajaran_hari;
														$where_jadwal['jadwal_pelajaran.jadwal_pelajaran_ke'] 	= $row_jam->jam_pelajaran_ke;

														$jadwalpel = $this->Jadwal_pelajaran_model->get_jadwal_pelajaran("", $where_jadwal);
														if ($jadwalpel){
															$program_studi_id_ = $jadwalpel->program_studi_id;
															$matakuliah_id_ = $jadwalpel->matakuliah_id;
															$dosen_id_ = $jadwalpel->dosen_id;
															$get_dosen_mengajar = $this->Dosen_model->get_dosen_mengajar("dosen_mengajar_id", array('dosen_mengajar.dosen_id'=>$dosen_id_, 'dosen_mengajar.matakuliah_id'=>$matakuliah_id_, 'dosen_mengajar.program_studi_id'=>$program_studi_id_, 'dosen_mengajar.semester_kode'=>$semester_kode));
															$dosen_mengajar_id_ = ($get_dosen_mengajar)?$get_dosen_mengajar->dosen_mengajar_id:"";
														} else {
															$program_studi_id_ = "";
															$matakuliah_id_ = "";
															$dosen_id_ = "";
															$dosen_mengajar_id_ = "";
														}
														if ($row_jam->jam_pelajaran_info == 'KBM'){
															?>
															<tr>
																<td style="text-align:center;"><?php echo $row_jam->jam_pelajaran_ke; ?></td>
																<td style="text-align:center;"><?php echo $row_jam->jam_pelajaran_mulai; ?></td>
																<td style="text-align:center;"><?php echo $row_jam->jam_pelajaran_selesai; ?></td>
																<td><?php echo $this->Dosen_model->combobox_dosen_mengajar("dosen_mengajar_id[$row_jam->jam_pelajaran_ke]", $dosen_mengajar_id_, '', '', 'class="form-control select2"', $semester_kode); ?></td>
															</tr>
															<?php
														} else {
															?>
															<tr bgcolor="#FFFF00">
																<td colspan="6"><?php echo $row_jam->jam_pelajaran_info; ?></td>
															</tr>
															<?php
														}
														
													}
												}
												?>
											</tbody>
										</table>
									</div>
									<div class="box-footer">
										<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
										<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$semester_kode.'/'.$ruangan_id); ?>'" class="btn btn-default">Batalkan</button>
									</div><!-- /.box-footer -->
								</form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php } else if ($action == 'import') {?>
<script>
  $(function () {
	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Jadwal Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('jadwal-pelajaran'); ?>">Jadwal Pelajaran</a></li>
            <li class="active">Import Jadwal Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Import Jadwal Pelajaran</h3>
                </div><!-- /.box-header -->
				<form class="form-horizontal" action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3));?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
				<div class="box-body">
					<div class="box-body">
						<div class="form-group">
						  <label class="col-md-2">Import File</label>
						  <div class="col-md-6">
							<div class="input-group">
								<input type="file" name="userfile" class="form-control" placeholder="" data-required="true" />
								<span class="input-group-btn">
									<input type="submit" name="importKD" value="Import" class="btn btn-primary" />
								</span>
							</div>
						  </div>
						</div>
						
						<?php if (!$dataExcel){?>
						<div class="form-group">
						  <div class="col-md-12 text-center">
								<input type="reset" name="resetKD" value="Kembali" class="btn btn-default" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" />
							</div>
						</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<?php if ($dataExcel){?>
					<div class="box-body">
						<h3>Preview Import</h3>
						<div style="overflow:auto;max-height:500px">
							<table class="table table-bordered">
							  <thead>
								<tr>
								  <th>#</th>
								  <th>STATUS</th>
								  <th>TAHUN AJARAN</th>
								  <th>SEMESTER</th>
								  <th>TINGKAT</th>
								  <th>KELAS</th>
								  <th>MATA PELAJARAN</th>
								  <th>GURU</th>
								  <th>GURU NAMA</th>
								  <th>HARI</th>
								  <th>JAM MULAI</th>
								  <th>JAM SELESAI</th>
								  <th>RUANGAN</th>
								</tr>
							  </thead>
							  <tbody>
							  <?php
							  if ($dataExcel){
								$i = 1;
								foreach($dataExcel as $row){
                  $nilaistatus = "";
									if ($row['nilai_status'] == 'Not Valid'){
										$nilaistatus = 'style="background:#EEB9B9"';
									} else {
										$nilaistatus = 'style="background:#B1E3B1"';
									}
									?>
									<tr <?php echo $nilaistatus; ?>>
										<th scope="row"><?php echo $i; ?></th>
										<td nowrap><?php echo $row['nilai_status']; ?></td>
									  <td nowrap><?php echo $row['tahun_ajaran']; ?></td>
									  <td nowrap><?php echo $row['semester']; ?></td>
									  <td nowrap><?php echo $row['tingkat']; ?></td>
									  <td nowrap><?php echo $row['ruangan']; ?></td>
									  <td nowrap><?php echo $row['mata_pelajaran']; ?></td>
									  <td nowrap><?php echo $row['guru']; ?></td>
									  <td nowrap><?php echo $row['guru_nama']; ?></td>
									  <td nowrap><?php echo $row['hari']; ?></td>
									  <td nowrap><?php echo $row['jam_mulai']; ?></td>
									  <td nowrap><?php echo $row['jam_selesai']; ?></td>
									  <td nowrap><?php echo $row['ruangan']; ?></td>
									</tr>
									<?php
									$i++;
								}
							  }
							  ?>
							  </tbody>
							</table>
						</div>
					</div>
					<?php } ?>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
						<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2)); ?>'" class="btn btn-default">Batalkan</button>
					</div><!-- /.box-footer -->
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  <div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } ?>