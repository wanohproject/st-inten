<?php
if ($action == '' || $action == 'grid'){
?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/fullcalendar/fullcalendar.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fullcalendar/fullcalendar.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/fullcalendar/locale/id.js');?>"></script>
<script>
  $(document).ready(function() {

		$('#calendar').fullCalendar({
			header: false,
			footer: false,
			locale: 'id',
			defaultView: 'agendaWeek',
			defaultDate: new Date(),
			editable: false,
			allDaySlot: false,
			contentHeight: "auto",
			minTime: "07:00:00",
  		maxTime: "22:00:00",
			// agendaEventMinHeight: 100,
			// columnFormat: 'ddd',
			slotDuration: '00:30:00',
			snapDuration: '00:30:00',
			// navLinks: true, // can click day/week names to navigate views
			eventLimit: true, // allow "more" link when too many events
			events: {
				url: '<?php echo module_url($this->uri->segment(2).'/get-event/'.$semester_kode); ?>',
				error: function() {
					$('#script-warning').show();
				}
			},
			loading: function(bool) {
				$('#loading').toggle(bool);
			},
			eventClick: function(calEvent, jsEvent, view) {
				$('#detailModal').modal('show');
				$('#id_jam').val(calEvent.id);
				$('#semester_nama').html(calEvent.semester_nama);
				$('#jam_mulai').html(calEvent.jam_mulai);
				$('#jam_selesai').html(calEvent.jam_selesai);
				$('#jam_info').html(calEvent.jam_info);
				$('#jam_urutan').html(calEvent.jam_urutan);
				$('#jam_ke').html(calEvent.jam_ke);
				$('#hari_nama').html(calEvent.hari_nama);

				$('#action-edit').attr("href", "<?php echo module_url($this->uri->segment(2)); ?>/add/" + calEvent.semester_kode + "/" + calEvent.hari_id);
			},
			eventRender: function(event, element, view) {
				element.find('.fc-title').append("<br/>" + event.description);
			}
		});

    $('#detailModal').on('shown.bs.modal', function (e) {
			console.log("Load Detail Modal");
    })

	<?php if ($this->session->flashdata('success') || $this->session->flashdata('error')) {?>
		<?php if ($this->session->flashdata('success')) { ?>
			$('#successModal').modal('show')
		<?php } else { ?>
			$('#errorModal').modal('show')
		<?php } ?>
	<?php } ?>
  });
</script>
<style>
#calendar {
    max-width: 100%;
    margin: 0 auto;
}
/* .fc-time-grid td { height: 20px !important; } */
</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Jam Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('jam-pelajaran'); ?>">Jam Pelajaran</a></li>
            <li class="active">Daftar Jam Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Daftar Jam Pelajaran</h3>
									<div class="pull-right">
										<a href="<?php echo module_url($this->uri->segment(2).'/add/'.$semester_kode); ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah</a>
										<!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span> Tambah</button> -->
									</div>
                </div><!-- /.box-header -->
								<div class="box-body">
								<form action="<?php echo module_url($this->uri->segment(2).'/index/'.$semester_kode);?>" method="post">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
												<div class="">
													<?php combobox('db', $this->db->query("SELECT * FROM akd_semester ORDER BY semester_kode DESC")->result(), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', 'none', 'class="form-control select2" required');?>
												</div>		
											</div>
										</div>
									</div>
								</div>
                <div class="box-body">
									<div id='calendar'></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
			
	<div class="modal fade modal-primary" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Detail Jadwal</h4>
		  </div>
		  <div class="modal-body">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Tahun</label>
                  <label class="col-sm-8 control-label" style="text-align: left;" id="semester_nama"></label>
                </div>
                <div class="form-group">
									<label class="col-sm-3 control-label">Keterangan</label>
									<label class="col-sm-8 control-label" style="text-align: left;" id="jam_info"></label>
                </div>
                <div class="form-group">
									<label class="col-sm-3 control-label">Jam Ke-</label>
									<label class="col-sm-8 control-label" style="text-align: left;" id="jam_ke"></label>
                </div>
                <div class="form-group">
									<label class="col-sm-3 control-label">Jadwal</label>
                  <label class="col-sm-8 control-label" style="text-align: left;"><span id="hari_nama"></span>, <span id="jam_mulai"></span> - <span id="jam_selesai"></span></label>
                </div>
              </div>
          </form>
		  </div>
		  <div class="modal-footer">
			<a href="#" id="action-edit" class="btn btn-warning pull-left">Ubah</a>
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-success" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Berhasil</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('success')) {
				echo $this->session->flashdata('success');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade modal-warning" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Gagal</h4>
		  </div>
		  <div class="modal-body">
			<?php if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			} ?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
<?php } else if ($action == 'add') {?>
<link rel="stylesheet" href="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>">
<script src="<?php echo theme_dir('admin_v2/plugins/moment/moment.min.js');?>"></script>
<script src="<?php echo theme_dir('admin_v2/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
<script>
  $(function () {
    //Date picker
    $('.jam').datetimepicker({
        sideBySide: true,
        format: 'HH:mm:ss'
    });

		<?php
    for ($i=1; $i < 20; $i++) {
      ?>
      $('#jam_pelajaran_urutan_<?php echo $i; ?>').click(function() {
          if ($(this).is(':checked') == true){
            $('#jam_pelajaran_ke_<?php echo $i; ?>').prop('disabled', false);
            $('#jam_pelajaran_mulai_<?php echo $i; ?>').prop('disabled', false);
            $('#jam_pelajaran_selesai_<?php echo $i; ?>').prop('disabled', false);
            $('#jam_pelajaran_info_<?php echo $i; ?>').prop('disabled', false);
          } else {
            $('#jam_pelajaran_ke_<?php echo $i; ?>').prop('disabled', true);
            $('#jam_pelajaran_mulai_<?php echo $i; ?>').prop('disabled', true);
            $('#jam_pelajaran_selesai_<?php echo $i; ?>').prop('disabled', true);
            $('#jam_pelajaran_info_<?php echo $i; ?>').prop('disabled', true);
          }
      });
      <?php
    }
    ?>
  });
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Jam Pelajaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo module_url(); ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo module_url('jam-pelajaran'); ?>">Jam Pelajaran</a></li>
            <li class="active">Ubah Jam Pelajaran</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Ubah Jam Pelajaran</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
								<form action="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$semester_kode.'/'.$jam_pelajaran_hari);?>" method="post">
									<input type="hidden" name="semester_kode" value="<?php echo $semester_kode; ?>" />
									<input type="hidden" name="jam_pelajaran_hari" value="<?php echo $jam_pelajaran_hari; ?>" />
									<div class="box-body">
									<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label for="tingkat_id" class="control-label">Tahun Ajaran</label>
													<div class="">
														<?php combobox('db', $this->db->query("SELECT * FROM akd_semester ORDER BY semester_kode DESC")->result(), 'semester_kode', 'semester_kode', 'semester_nama', $semester_kode, 'submit();', 'none', 'class="form-control select2" required');?>
													</div>		
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label for="tingkat_id" class="control-label">Hari</label>
													<div class="">
														<?php combobox('2d', array('1'=>'Senin', '2'=>'Selasa', '3'=>'Rabu', '4'=>'Kamis', '5'=>'Jumat', '6'=>'Sabtu', '7'=>'Minggu'), 'jam_pelajaran_hari', '', '', $jam_pelajaran_hari, 'submit();', '-- PILIH HARI --', 'class="form-control select2" required');?>
													</div>		
												</div>
											</div>
											<div class="col-md-offset-3 col-md-3">
                        <div class="form-group">
                          <label for="semester_kode" class="control-label">&nbsp;</label>
                          <div class="text-right">
                            <?php if ($this->input->get('refId')){ ?>
                              <a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$semester_kode.'/'.$jam_pelajaran_hari);?>" class="btn btn-danger">Hapus Referensi</a>
                            <?php } else { ?>
                              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default"><i class="fa fa-copy"></i> Salin Referensi</button>
                            <?php } ?>
                          </div>		
                        </div>
                      </div>
										</div>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th width="50">#</th>
													<th width="100">Jam Ke-</th>
													<th>Jam Mulai</th>
													<th>Jam Selesai</th>
													<th>Info</th>
												</tr>
											</thead>
											<tbody>
												<?php
												for ($i=1; $i < 20; $i++) {
													if ($this->input->get('refDay') && $this->input->get('refYear')){
														$where['jam_pelajaran_hari'] 		= $this->input->get('refDay');
														$where['jam_pelajaran_urutan'] 	= $i;
														$where['semester_kode'] 							= $this->input->get('refYear');
														if ($this->Jam_pelajaran_model->count_all_jam_pelajaran($where) < 1){
															$jam_pelajaran_ke = "";
															$jam_pelajaran_mulai = "07:00:00";
															$jam_pelajaran_selesai = "07:00:00";
															$jam_pelajaran_info = "KBM";
		
															$disabled = "disabled";
															$checked = "";
														} else {
															$jam_pelajaran = $this->Jam_pelajaran_model->get_jam_pelajaran("", $where);
															$jam_pelajaran_ke = $jam_pelajaran->jam_pelajaran_ke;
															$jam_pelajaran_mulai = $jam_pelajaran->jam_pelajaran_mulai;
															$jam_pelajaran_selesai = $jam_pelajaran->jam_pelajaran_selesai;
															$jam_pelajaran_info = $jam_pelajaran->jam_pelajaran_info;
															$disabled = "";
															$checked = "checked";
														}
													} else {
														$where['jam_pelajaran_hari'] 		= $jam_pelajaran_hari;
														$where['jam_pelajaran_urutan'] 	= $i;
														$where['semester_kode'] 							= $semester_kode;
														if ($this->Jam_pelajaran_model->count_all_jam_pelajaran($where) < 1){
															$jam_pelajaran_ke = "";
															$jam_pelajaran_mulai = "07:00:00";
															$jam_pelajaran_selesai = "07:00:00";
															$jam_pelajaran_info = "KBM";
		
															$disabled = "disabled";
															$checked = "";
														} else {
															$jam_pelajaran = $this->Jam_pelajaran_model->get_jam_pelajaran("", $where);
															$jam_pelajaran_ke = $jam_pelajaran->jam_pelajaran_ke;
															$jam_pelajaran_mulai = $jam_pelajaran->jam_pelajaran_mulai;
															$jam_pelajaran_selesai = $jam_pelajaran->jam_pelajaran_selesai;
															$jam_pelajaran_info = $jam_pelajaran->jam_pelajaran_info;
															$disabled = "";
															$checked = "checked";
														}
													}
													?>
													<tr>
														<td style="text-align:center;"><input type="checkbox" name="jam_pelajaran_urutan[<?php echo $i; ?>]" id="jam_pelajaran_urutan_<?php echo $i; ?>" <?php echo $checked; ?>></td>
														<td><input type="number" min="0" class="form-control" name="jam_pelajaran_ke[<?php echo $i; ?>]" id="jam_pelajaran_ke_<?php echo $i; ?>" <?php echo $disabled; ?> autocomplete="off" value="<?php echo $jam_pelajaran_ke; ?>" placeholder=""></td>
														<td><input type="text" class="form-control jam" name="jam_pelajaran_mulai[<?php echo $i; ?>]" id="jam_pelajaran_mulai_<?php echo $i; ?>" <?php echo $disabled; ?> autocomplete="off" value="<?php echo $jam_pelajaran_mulai; ?>" placeholder="" required></td>
														<td><input type="text" class="form-control jam" name="jam_pelajaran_selesai[<?php echo $i; ?>]" id="jam_pelajaran_selesai_<?php echo $i; ?>" <?php echo $disabled; ?> autocomplete="off" value="<?php echo $jam_pelajaran_selesai; ?>" placeholder="" required></td>
														<td><input type="text" class="form-control" name="jam_pelajaran_info[<?php echo $i; ?>]" id="jam_pelajaran_info_<?php echo $i; ?>" <?php echo $disabled; ?> autocomplete="off" value="<?php echo $jam_pelajaran_info; ?>" placeholder="" required></td>
													</tr>
													<?php
												}
												?>
											</tbody>
										</table>
									</div>
									<div class="box-footer">
										<button type="submit" class="btn btn-primary" name="save" value="save">Simpan</button>
										<button type="reset" onclick="location.href='<?php echo module_url($this->uri->segment(2).'/index/'.$semester_kode); ?>'" class="btn btn-default">Batalkan</button>
									</div><!-- /.box-footer -->
								</form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
			</div><!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pilih Referensi</h4>
      </div>
      <div class="modal-body">
			<table id="datagrid" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tahun</th>
              <th>Hari</th>
              <th style="width:50px;">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $grid_ref = $this->db->query("SELECT * FROM akd_jam_pelajaran jam_pelajaran LEFT JOIN akd_semester ON jam_pelajaran.semester_kode=akd_semester.semester_kode GROUP BY jam_pelajaran.semester_kode, jam_pelajaran.jam_pelajaran_hari")->result();
          if ($grid_ref){
            foreach ($grid_ref as $row_ref){
							$dayName = array('1'=>'Senin', '2'=>'Selasa', '3'=>'Rabu', '4'=>'Kamis', '5'=>'Jumat', '6'=>'Sabtu', '7'=>'Minggu');
            	?>
              <tr>
                <td><?php echo $row_ref->semester_nama; ?></td>
                <td><?php echo $dayName[$row_ref->jam_pelajaran_hari]; ?></td>
                <td><a href="<?php echo module_url($this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$semester_kode.'/'.$jam_pelajaran_hari)."?refDay=".$row_ref->jam_pelajaran_hari."&refYear=".$row_ref->semester_kode; ?>" class="btn btn-sm btn-success"><i class="fa fa-copy"></i></a></td>
              </tr>
            	<?php
            }
          }
          ?>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>