<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * 
 *
 * @package		
 * @author		
 * @copyright	
 * @license		
 * @link		
 * @since		
 * @filesource
 */

// ------------------------------------------------------------------------

if ( ! function_exists('combo_tanggal'))
{
	function combo_tanggal($name_tgl, $name_bln, $name_thn, $tanggal, $awal, $akhir){
		$get_tgl = substr($tanggal,8,2);
		echo "<select name='$name_tgl' id='$name_tgl' class='text_input' style='width:50px'>";
		echo "<option value=''></option>";
		for($tgl=1; $tgl < 32; $tgl++){
			if ($tgl < 10){
				$tgl = "0".$tgl;	
			} else {
				$tgl = $tgl;
			}

			if ($tgl == $get_tgl){
				echo "<option value='".$tgl."' selected>".$tgl."</option>";
			} else {
				echo "<option value='".$tgl."'>".$tgl."</option>";
			}
		}
		echo "</select>&nbsp;";
		
		$nama_bulan = array (1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktobet","November", "Desember");
		$get_bln = substr ($tanggal,5,2);
		echo "<select name='$name_bln' id='$name_bln' class='text_input' style='width:100px;'>";
		echo "<option value=''></option>";
		for($bln=1; $bln < 13; $bln++){
			if ($bln < 10){
				$xbln = "0".$bln;	
			} else {
				$xbln = $bln;
			}
			if ($xbln == $get_bln){
				echo "<option value='".$xbln."' selected>".$nama_bulan[$bln]."</option>";
			} else {
				echo "<option value='".$xbln."'>".$nama_bulan[$bln]."</option>";
			}
		}
		echo "</select>&nbsp;";
		
		$get_thn = substr($tanggal,0,4);
		$tahun = date("Y") - $akhir;
		echo "<select name='$name_thn' id='$name_thn' class='text_input'>";
		echo "<option value=''></option>";
		for($thn = $tahun; $thn > $awal; $thn--){
			if ($thn == $get_thn){
				echo "<option value='".$thn."' selected>".$thn."</option>";
			} else {
				echo "<option value='".$thn."'>".$thn."</option>";
			}
		}
		echo "</select>&nbsp;";
	}
	
}

if ( ! function_exists('combo_bulan'))
{
	function combo_bulan($name_bln, $tanggal){
		$nama_bulan = array (1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktobet","November", "Desember");
		$get_bln = $tanggal;
		echo "<select name='$name_bln' id='$name_bln' class='text_input'>";
		echo "<option value=''></option>";
		for($bln=1; $bln < 13; $bln++){
			if ($bln < 10){
				$xbln = "0".$bln;	
			} else {
				$xbln = $bln;
			}
			if ($xbln == $get_bln){
				echo "<option value='".$xbln."' selected>".$nama_bulan[$bln]."</option>";
			} else {
				echo "<option value='".$xbln."'>".$nama_bulan[$bln]."</option>";
			}
		}
		echo "</select>&nbsp;";
	}
	
}

if ( ! function_exists('combo_tahun'))
{
	function combo_tahun($name_thn, $tanggal, $awal, $akhir){
		$get_thn = $tanggal;
		$tahun = date("Y") - $akhir;
		echo "<select name='$name_thn' id='$name_thn' class='text_input'>";
		echo "<option value=''></option>";
		for($thn = $tahun; $thn > $awal; $thn--){
			if ($thn == $get_thn){
				echo "<option value='".$thn."' selected>".$thn."</option>";
			} else {
				echo "<option value='".$thn."'>".$thn."</option>";
			}
		}
		echo "</select>&nbsp;";
	}
}

if ( ! function_exists('combo_triwulan'))
{
	function combo_triwulan($name_triwulan, $triwulan){
		$data = array(1=>'TRIWULAN I', 2=>'TRIWULAN II', 3=>'TRIWULAN III', 4=>'TRIWULAN IV');
		echo "<select name='$name_triwulan' id='$name_triwulan' class='text_input'>";
		echo "<option value=''></option>";
		foreach ($data as $key => $value){
			if ($key == $triwulan){
				echo "<option value='".$key."' selected>".$value."</option>";
			} else {
				echo "<option value='".$key."'>".$value."</option>";
			}
		}
		echo "</select>&nbsp;";	
	}
}

if ( ! function_exists('jam_sekarang'))
{
	function jam_sekarang()
	{
		date_default_timezone_set('Asia/Jakarta');
		return date("H:i:s");
	}
}

if ( ! function_exists('hari_ini'))
{
	function hari_ini()
	{
		date_default_timezone_set('Asia/Jakarta');
		$seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
		$hari = date("w");
		$hari_ini = $seminggu[$hari];
		return $hari_ini;
	}
}

if ( ! function_exists('getDayShort'))
{
	function getDayShort($tanggal)
	{
		date_default_timezone_set('Asia/Jakarta');
		$seminggu = array("Min","Sen","Sel","Rab","Kam","Jum","Sab");
		$hari = date("w", strtotime($tanggal));
		$hari_ini = $seminggu[$hari];
		return $hari_ini;
	}
}

/**
 * dateIndo
 *
 * mengubah ke format 11 Juni 1993 11:06
 *
 */

// ------------------------------------------------------------------------
if ( ! function_exists('dateIndo'))
{
	function dateIndo($tgl='')
	{
		$waktu 	= substr ($tgl,11,5);
		$tanggal = substr ($tgl,8,2);
		$bulan = getBulan(substr($tgl,5,2));
		$tahun = substr ($tgl,0,4);
		return $tanggal . ' ' . $bulan . ' ' . $tahun . ' ' . $waktu;
	}
}

if ( ! function_exists('dateIndo_'))
{
	function dateIndo_($tgl='')
	{
		$waktu 	= substr ($tgl,11,5);
		$tanggal = substr ($tgl,8,2);
		$bulan = getBulan(substr($tgl,5,2));
		$tahun = substr ($tgl,0,4);
		return (int) $tanggal . ' ' . $bulan . ' ' . $tahun . ' ' . $waktu;
	}
}

/**
 * dateIndo2
 *
 * mengubah ke format 11/06/1993 11:06:00
 *
 */

// ------------------------------------------------------------------------
if ( ! function_exists('dateIndo2'))
{
	function dateIndo2($tgl='')
	{
		$waktu 	= substr ($tgl,11,7);
		$tanggal = substr ($tgl,8,2);
		$bulan = substr($tgl,5,2);
		$tahun = substr ($tgl,0,4);
		return $tanggal . '/' . $bulan . '/' . $tahun . ' ' . $waktu;
	}
}

/**
 * dateIndo3
 *
 * mengubah ke format 11-06-1993 11:06:00
 *
 */

// ------------------------------------------------------------------------
if ( ! function_exists('dateIndo3'))
{
	function dateIndo3($tgl='')
	{
		$waktu 	= substr ($tgl,11,7);
		$tanggal = substr($tgl,8,2);
		$bulan = substr($tgl,5,2);
		$tahun = substr ($tgl,0,4);
		return $tanggal . '-' . $bulan . '-' . $tahun . ' ' . $waktu;
	}
}

/**
 * dateIndo4
 *
 * mengubah ke format 1993-06-11 11:06:00
 *
 */

// ------------------------------------------------------------------------
if ( ! function_exists('dateIndo4'))
{
	function dateIndo4($tgl='')
	{
		$waktu 	= substr ($tgl,11,7);
		$tanggal = substr($tgl,0,2);
		$bulan = substr($tgl,3,2);
		$tahun = substr ($tgl,6,4);
		return $tahun . '-' . $bulan . '-' . $tanggal . ' ' . $waktu;
	}
}

if ( ! function_exists('dateIndo5'))
{
	function dateIndo5($tgl='')
	{
		$waktu 	= substr ($tgl,11,5);
		$tanggal = substr ($tgl,8,2);
		$bulan = getBulan1(substr($tgl,5,2));
		$tahun = substr ($tgl,0,4);
		return $tanggal . ' ' . $bulan . ' ' . $tahun . ' ' . $waktu;
	}
}

if ( ! function_exists('dateForDB'))
{
	function dateForDB($date='')
	{
		if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
			return $date;
		} else {
			$tanggal = substr($date,0,2);
			$bulan = substr($date,3,2);
			$tahun = substr ($date,6,4);
			return $tahun . '-' . $bulan . '-' . $tanggal;
		}
	}
}

if ( ! function_exists('dateShort'))
{
	function dateShort($date='')
	{
		$tanggal = substr ($date,8,2);
		$bulan = substr($date,5,2);
		$tahun = substr ($date,0,4);
		return $tanggal . '-' . $bulan;
	}
}

if ( ! function_exists('getBulan'))
{
	function getBulan($bulan='')
	{
		switch ($bulan){
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
}

if ( ! function_exists('getBulan1'))
{
	function getBulan1($bulan='')
	{
		switch ($bulan){
			case 1:
				return "Jan";
				break;
			case 2:
				return "Feb";
				break;
			case 3:
				return "Mar";
				break;
			case 4:
				return "Apr";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Jun";
				break;
			case 7:
				return "Jul";
				break;
			case 8:
				return "Agu";
				break;
			case 9:
				return "Sep";
				break;
			case 10:
				return "Okt";
				break;
			case 11:
				return "Nov";
				break;
			case 12:
				return "Des";
				break;
		}
	}
}

if ( ! function_exists('getBulanShortEn'))
{
	function getBulanShortEn($bulan='')
	{
		switch ($bulan){
			case 1:
				return "Jan";
				break;
			case 2:
				return "Feb";
				break;
			case 3:
				return "Mar";
				break;
			case 4:
				return "Apr";
				break;
			case 5:
				return "May";
				break;
			case 6:
				return "Jun";
				break;
			case 7:
				return "Jul";
				break;
			case 8:
				return "Aug";
				break;
			case 9:
				return "Sep";
				break;
			case 10:
				return "Oct";
				break;
			case 11:
				return "Nov";
				break;
			case 12:
				return "Dec";
				break;
		}
	}
}


if ( ! function_exists('inday'))
{
	function inday($tgl='')
	{
		$tanggal= substr ($tgl,8,2);
		$bulan 	= substr($tgl,5,2);
		$tahun 	= substr ($tgl,0,4);
		$day = date ("w", mktime(0,0,0,$bulan,$tanggal,$tahun));
		switch($day){
			case "0" : $day_value = "Minggu"; break;
			case "1" : $day_value = "Senin"; break;
			case "2" : $day_value = "Selasa"; break;
			case "3" : $day_value = "Rabu"; break;
			case "4" : $day_value = "Kamis"; break;
			case "5" : $day_value = "Jumat"; break;
			case "6" : $day_value = "Sabtu"; break;
		}
		return $day_value;
	}
}

if ( ! function_exists('inday_id'))
{
	function inday_id($tgl='')
	{
		$tanggal= substr ($tgl,8,2);
		$bulan 	= substr($tgl,5,2);
		$tahun 	= substr ($tgl,0,4);
		$day = date ("w", mktime(0,0,0,$bulan,$tanggal,$tahun));
		switch($day){
			case "0" : $day_value = "Minggu"; break;
			case "1" : $day_value = "Senin"; break;
			case "2" : $day_value = "Selasa"; break;
			case "3" : $day_value = "Rabu"; break;
			case "4" : $day_value = "Kamis"; break;
			case "5" : $day_value = "Jumat"; break;
			case "6" : $day_value = "Sabtu"; break;
		}
		return $day_value;
	}
}

if ( ! function_exists('timeToDate'))
{
	function timeToDate($seconds='')
	{
		$hours = floor($seconds / 3600);
		$mins = floor($seconds / 60 % 60);
		$secs = floor($seconds % 60);
		return str_pad($hours,2,"0",STR_PAD_LEFT) . ':' . str_pad($mins,2,"0",STR_PAD_LEFT) . ':' . str_pad($secs,2,"0",STR_PAD_LEFT);
	}
}


if ( ! function_exists('umur'))
{
	function umur($tanggal=''){
		if ($tanggal!='0000-00-00'){
			$thn = substr ("$tanggal",0,4);
			$bln = substr ("$tanggal",5,2);
			$tgl = substr ("$tanggal",8,2);
			$day = "";
			$birth = gregoriantojd($bln,$tgl,$thn);
			$cal = cal_from_jd(1721426+unixtojd()-$birth, CAL_GREGORIAN);
			
			$year = $cal['year']-1;
			$month = $cal['month']-1;
			
			if (($year == 0) AND ($month == 0)){
				$day = $cal['day'];
				$umur = array($day,'HR');
			} else if ($year == 0){
				$months= $cal['month']-1;
				$umur = array($months, 'BLN');
			} else {
				$years= $cal['year']-1;
				$umur = array($years, 'THN');	
			}
			
			return $umur;
		} else {
			$umur = array('','&nbsp;');
			return $umur;	
		}
	}
}

// Date Utilities
//----------------------------------------------------------------------------------------------


// Parses a string into a DateTime object, optionally forced into the given timezone.

if ( ! function_exists('parseDateTime'))
{
function parseDateTime($string, $timezone=null) {
	$date = new DateTime(
	  $string,
	  $timezone ? $timezone : new DateTimeZone('UTC')
		// Used only when the string is ambiguous.
		// Ignored if string has a timezone offset in it.
	);
	if ($timezone) {
	  // If our timezone was ignored above, force it.
	  $date->setTimezone($timezone);
	}
	return $date;
  }
}
  
// Takes the year/month/date values of the given DateTime and converts them to a new DateTime,
// but in UTC.
if ( ! function_exists('stripTime'))
{
	function stripTime($datetime) {
		return new DateTime($datetime->format('Y-m-d'));
	}
}

if ( ! function_exists('selisihDate'))
{
	function selisihDate($jam_masuk, $jam_keluar) 
	{ 
		// list($h,$m,$s) = explode(':',$jam_masuk); 
		// $dtAwal = mktime($h,$m,$s,'1','1','1'); 
		// list($h,$m,$s) = explode(':',$jam_keluar); 
		// $dtAkhir = mktime($h,$m,$s,'1','1','1');
		$dtSelisih = strtotime($jam_keluar) - strtotime($jam_masuk); 

		
		$jumlah_jam = floor($dtSelisih/3600);

		//Untuk menghitung jumlah dalam satuan menit:
		$sisa = $dtSelisih% 3600;
		$jumlah_menit = floor($sisa/60);

		//Untuk menghitung jumlah dalam satuan detik:
		$sisa = $sisa % 60;
		$jumlah_detik = floor($sisa/1);

		return $jumlah_jam.' jam '.$jumlah_menit.' menit '; 
	}
}

if ( ! function_exists('selisihDate2'))
{
	function selisihDate2($jam_masuk, $jam_keluar) 
	{ 
		// list($h,$m,$s) = explode(':',$jam_masuk); 
		// $dtAwal = mktime($h,$m,$s,'1','1','1'); 
		// list($h,$m,$s) = explode(':',$jam_keluar); 
		// $dtAkhir = mktime($h,$m,$s,'1','1','1');
		$dtSelisih = strtotime($jam_keluar) - strtotime($jam_masuk);
		return $dtSelisih; 
	}
}

if ( ! function_exists('stoh'))
{
	function stoh($second) 
	{ 
		// list($h,$m,$s) = explode(':',$jam_masuk); 
		// $dtAwal = mktime($h,$m,$s,'1','1','1'); 
		// list($h,$m,$s) = explode(':',$jam_keluar); 
		// $dtAkhir = mktime($h,$m,$s,'1','1','1');

		$jumlah_jam = floor($second/3600);

		//Untuk menghitung jumlah dalam satuan menit:
		$sisa = $second % 3600;
		$jumlah_menit = floor($sisa/60);

		//Untuk menghitung jumlah dalam satuan detik:
		$sisa = $sisa % 60;
		$jumlah_detik = floor($sisa/1);

		return $jumlah_jam.' jam '.$jumlah_menit.' menit '; 
	}
}
/* End of file MY_date_helper.php */
/* Location: ./application/helpers/MY_date_helper.php */