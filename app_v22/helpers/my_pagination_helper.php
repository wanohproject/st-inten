<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * 
 *
 * @package		
 * @author		
 * @copyright	
 * @license		
 * @link		
 * @since		
 * @filesource
 */

// ------------------------------------------------------------------------

if ( ! function_exists('admin_v2_pagination'))
{
	function admin_v2_pagination($halaman, $jmlhalaman, $url, $id=""){
		echo "<ul class=\"pagination pull-right\">";
		if ($halaman > 1){
			$previous = $halaman-1;
			echo "<li><a href=\"".site_url().$url."/1/".$id."\">&laquo;</a></li><li><a href=\"".site_url().$url."/".$previous."/".$id."\">&lsaquo;</a></li>";
		} else {
			echo "<li class=\"disabled\"><a href=\"#\">&laquo;</a></li><li class=\"disabled\"><a href=\"#\">&lsaquo;</a></li>";
		}
		if ($halaman < $jmlhalaman){
			$next = $halaman+1;
			echo "<li><a href=\"".site_url().$url."/".$next."/".$id."\">&rsaquo;</a></li><li><a href=\"".site_url().$url."/".$jmlhalaman."/".$id."\">&raquo;</a></li>";
		} else {
			echo "<li class=\"disabled\"><a href=\"#\">&rsaquo;</a></li><li class=\"disabled\"><a href=\"#\">&raquo;</a></li>";
		}
		echo "</ul>";
	}
}

if ( ! function_exists('admin_pagination'))
{
	function admin_pagination($jml_data, $batas, $page, $halaman, $jmlhalaman, $url, $id=""){
		if ($jml_data <= $batas) {
			echo "<div class=\"page-jml\">Menampilkan $jml_data data</div>";
		} elseif ($jml_data > $batas) {
			if ($halaman == $jmlhalaman){
				$last = $jml_data;
			} else {
				$last = $batas + $page;
			}
			echo "<div class=\"page-jml\">Menampilkan ". (1 + $page) ." sampai ". $last ." dari $jml_data data</div>";
			echo "<div class=\"paginate\">";
			if ($halaman > 1){
				$previous = $halaman-1;
				echo "<a href='".site_url().$url."/1/".$id."' class='paginate_enabled_previous3'></a><a href='".site_url().$url."/".$previous."/".$id."' class='paginate_enabled_previous2'></a>";
			} else {
				echo "<a class='paginate_disabled_previous3'></a><a class='paginate_disabled_previous2'></a>";
			}
			if ($halaman < $jmlhalaman){
				$next = $halaman+1;
				echo "<a href='".site_url().$url."/".$next."/".$id."' class='paginate_enabled_next2'></a><a href='".site_url().$url."/".$jmlhalaman."/".$id."' class='paginate_enabled_next3'></a>";
			} else {
				echo "<a class='paginate_disabled_next2'></a><a class='paginate_disabled_next3'></a>";
			}
			echo "</div>";
		} else {
			echo "&nbsp;";
		}
	}
}


if ( ! function_exists('pagination_1'))
{
	function pagination_1($halaman, $jmlhalaman, $url, $id=""){
		if ($jmlhalaman > 1){
			echo "<div class=\"main-pagination\">";
			if ($halaman > 1){
				$previous = $halaman-1;
				echo "<a class=\"prev page-numbers\" href=\"".site_url().$url."/$previous/$id\"><span class=\"visuallyhidden\">Prev</span><i class=\"fa fa-angle-left\"></i></a>";
			}
			for ($i=1; $i <= $jmlhalaman; $i++){
				if ($i == $halaman){
					echo "<span class=\"page-numbers current\">$i</span>";
				} else {
					echo "<a class=\"page-numbers\" href=\"".site_url().$url."/$i/$id\">$i</a>";
				}
			}
			if ($halaman < $jmlhalaman){
				$next = $halaman+1;
				echo "<a class=\"next page-numbers\" href=\"".site_url().$url."/$next/$id\"><span class=\"visuallyhidden\">Next</span><i class=\"fa fa-angle-right\"></i></a>";
			}
			echo "</div>";
		}
	}
}

if ( ! function_exists('pagination_2'))
{
	function pagination_2($halaman, $jmlhalaman, $url, $id=""){
		if ($jmlhalaman > 1){
			echo "<div class=\"main-pagination\">";
			if ($halaman > 1){
				$previous = $halaman-1;
				echo "<a class=\"prev page-numbers\" href=\"".site_url().$url."/1/$id\"><span class=\"visuallyhidden\">Prev</span><i class=\"fa fa-angle-left\"></i><i class=\"fa fa-angle-left\"></i></a>
					  <a class=\"prev page-numbers\" href=\"".site_url().$url."/$previous/$id\"><span class=\"visuallyhidden\">Prev</span><i class=\"fa fa-angle-left\"></i></a>";
			}
			if ($jmlhalaman > 6 && $halaman > 3){
				echo "<span class=\"page-numbers\">...</span>";
			}
			
			if ($jmlhalaman > 6 && $halaman > ($jmlhalaman-3)){
				for ($i=$jmlhalaman-4; $i <= $jmlhalaman; $i++){
					if ($i == $halaman){
						echo "<span class=\"page-numbers current\">$i</span>";
					} else {
						echo "<a class=\"page-numbers\" href=\"".site_url().$url."/$i/$id\">$i</a>";
					}
				}
			} else if ($jmlhalaman > 6 && $halaman > 3){
				for ($i=$halaman-2; $i <= $halaman+2; $i++){
					if ($i == $halaman){
						echo "<span class=\"page-numbers current\">$i</span>";
					} else {
						echo "<a class=\"page-numbers\" href=\"".site_url().$url."/$i/$id\">$i</a>";
					}
				}			
			} else if ($jmlhalaman > 6 && $halaman <= 3){
				for ($i=1; $i <= 5; $i++){
					if ($i == $halaman){
						echo "<span class=\"page-numbers current\">$i</span>";
					} else {
						echo "<a class=\"page-numbers\" href=\"".site_url().$url."/$i/$id\">$i</a>";
					}
				}			
			} else {
				for ($i=1; $i <= $jmlhalaman; $i++){
					if ($i == $halaman){
						echo "<span class=\"page-numbers current\">$i</span>";
					} else {
						echo "<a class=\"page-numbers\" href=\"".site_url().$url."/$i/$id\">$i</a>";
					}
				}
			}
			if ($jmlhalaman > 6 && $halaman <= ($jmlhalaman-3)){
				echo "<span class=\"page-numbers\">...</span>";
			}
			if ($halaman < $jmlhalaman){
				$next = $halaman+1;
				echo "<a class=\"prev page-numbers\" href=\"".site_url().$url."/$previous/$id\"><span class=\"visuallyhidden\">Prev</span><i class=\"fa fa-angle-left\"></i></a>";
				echo "<a class=\"next page-numbers\" href=\"".site_url().$url."/$next/$id\"><span class=\"visuallyhidden\">Next</span><i class=\"fa fa-angle-right\"></i></a>
					  <a class=\"next page-numbers\" href=\"".site_url().$url."/$jmlhalaman/$id\"><span class=\"visuallyhidden\">Next</span><i class=\"fa fa-angle-right\"></i><i class=\"fa fa-angle-right\"></i></a>";
			}
			echo "</div>";
		}
	}
}

if ( ! function_exists('pagination_3'))
{
	function pagination_3($halaman, $jmlhalaman, $url, $id=""){
		if ($jmlhalaman > 1){
			echo "<nav>
					<ul class=\"pagination\">";
			if ($halaman > 1){
				$previous = $halaman-1;
				echo "<li><a href=\"".site_url().$url."/1/$id\" aria-label=\"Previous\"><span aria-hidden=\"true\">&laquo;</span></a></li>";
			} else {
				echo "<li class=\"disabled\"><a href=\"#\" aria-label=\"Previous\"><span aria-hidden=\"true\">&laquo;</span></a></li>";
			}
			if ($jmlhalaman > 6 && $halaman > 3){
				echo "<li><a href=\"#\">...</a></li>";
			}
			
			if ($jmlhalaman > 6 && $halaman > ($jmlhalaman-3)){
				for ($i=$jmlhalaman-4; $i <= $jmlhalaman; $i++){
					if ($i == $halaman){
						echo "<li class=\"active\"><a href=\"#\">$i </a></li>";
					} else {
						echo "<li><a href=\"".site_url().$url."/$i/$id\">$i </a></li>";
					}
				}
			} else if ($jmlhalaman > 6 && $halaman > 3){
				for ($i=$halaman-2; $i <= $halaman+2; $i++){
					if ($i == $halaman){
						echo "<li class=\"active\"><a href=\"#\">$i </a></li>";
					} else {
						echo "<li><a href=\"".site_url().$url."/$i/$id\">$i </a></li>";
					}
				}			
			} else if ($jmlhalaman > 6 && $halaman <= 3){
				for ($i=1; $i <= 5; $i++){
					if ($i == $halaman){
						echo "<li class=\"active\"><a href=\"#\">$i </a></li>";
					} else {
						echo "<li><a href=\"".site_url().$url."/$i/$id\">$i </a></li>";
					}
				}			
			} else {
				for ($i=1; $i <= $jmlhalaman; $i++){
					if ($i == $halaman){
						echo "<li class=\"active\"><a href=\"#\">$i </a></li>";
					} else {
						echo "<li><a href=\"".site_url().$url."/$i/$id\">$i </a></li>";
					}
				}
			}
			if ($jmlhalaman > 6 && $halaman <= ($jmlhalaman-3)){
				echo "<li><span class=\"page-numbers\">...</span></li>";
			}
			if ($halaman < $jmlhalaman){
				$next = $halaman+1;
				echo "<li><a href=\"".site_url().$url."/$jmlhalaman/$id\" aria-label=\"Next\"><span aria-hidden=\"true\">&raquo;</span></a></li>";
			} else {
				echo "<li class=\"disabled\"><a href=\"#\" aria-label=\"Previous\"><span aria-hidden=\"true\">&raquo;</span></a></li>";
			}
			
			echo "</ul>
				</nav>";
		}
	}
}

/* End of file my_pagination_helper.php */
/* Location: ./application/helpers/my_pagination_helper.php */