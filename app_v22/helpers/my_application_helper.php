<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * 
 *
 * @package		
 * @author		
 * @copyright	
 * @license		
 * @link		
 * @since		
 * @filesource
 */

// ------------------------------------------------------------------------

if ( ! function_exists('combobox'))
{
	function combobox($jenis='', $query, $name, $value, $name_value, $pilihan, $js='', $label='', $script=''){
		echo "<select name='$name' id='".str_replace(array("[", "]"), "_", $name)."' onchange='$js' $script>";
		if ($label != "none"){
			echo "<option value=''>".$label."</option>";
		}
		
		if (empty($jenis) || $jenis == "db"){			
			foreach ($query as $row){
				if ($pilihan == $row->$value){
					echo "<option value='".$row->$value."' selected>".$row->$name_value."</option>";
				} else {
					echo "<option value='".$row->$value."'>".$row->$name_value."</option>";
				}
			}
		} elseif ($jenis == "1d"){
			foreach ($query as $row){
				if ($pilihan == $row){
					echo "<option value='".$row."' selected>".$row."</option>";
				} else {
					echo "<option value='".$row."'>".$row."</option>";
				}
			}
		} elseif ($jenis == "2d"){
			foreach ($query as $row_key => $row_value){
				if ($pilihan == $row_key){
					echo "<option value='".$row_key."' selected>".$row_value."</option>";
				} else {
					echo "<option value='".$row_key."'>".$row_value."</option>";
				}
			}
		}
		echo "</select>";
	}
}

if ( ! function_exists('radiobutton'))
{
	function radiobutton($jenis='', $query, $name, $value, $name_value, $pilihan, $js='', $label='', $script=''){
		echo "<div class=\"radio\">";
		if (empty($jenis) || $jenis == "db"){			
			foreach ($query as $row){
				if ($pilihan == $row->$value){
					echo "<label><input type=\"radio\" name=\"$name\" value=\"".$row->$value."\" checked> ".$row->$name_value."</label>";
					echo "&nbsp;&nbsp;&nbsp;";
				} else {
					echo "<label><input type=\"radio\" name=\"$name\" value=\"".$row->$value."\"> ".$row->$name_value."</label>";
					echo "&nbsp;&nbsp;&nbsp;";
				}
			}
		} elseif ($jenis == "1d"){
			foreach ($query as $row){
				if ($pilihan == $row){
					echo "<label><input type=\"radio\" name=\"$name\" value=\"$row\" checked> $row</label>";
					echo "&nbsp;&nbsp;&nbsp;";
				} else {
					echo "<label><input type=\"radio\" name=\"$name\" value=\"$row\"> $row</label>";
					echo "&nbsp;&nbsp;&nbsp;";
				}
			}
		} elseif ($jenis == "2d"){
			foreach ($query as $row_key => $row_value){
				if ($pilihan == $row){
					echo "<label><input type=\"radio\" name=\"$name\" value=\"$row_key\" checked> $row_value</label>";
					echo "&nbsp;&nbsp;&nbsp;";
				} else {
					echo "<label><input type=\"radio\" name=\"$name\" value=\"$row_key\"> $row_value</label>";
					echo "&nbsp;&nbsp;&nbsp;";
				}
			}
		}
		echo "</div>";
	}
}

if ( ! function_exists('seo'))
{
	function seo($s) {
		$c = array (' ');
		$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
	
		$s = str_replace($d, '', $s);
		
		$s = strtolower(str_replace($c, '-', $s));
		return $s;
	}
}

if ( ! function_exists('filename_seo'))
{
	function filename_seo($s) {
		$c = array (' ');
		$d = array ('/','\\',',','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','’','…');
	
		$s = preg_replace('/[^A-Za-z0-9\-\ \.]/u', ' ', $s);
		
		$s = str_replace($c, '-', $s);
		$s = preg_replace('/-+/', '-', $s);
		$s = strtolower($s);
		return $s;
	}
}

if ( ! function_exists('seo2'))
{
	function seo2($s) {
		$c = array (' ');
		$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
	
		$s = str_replace($d, ' ', $s);
		
		$s = str_replace($c, ' ', $s);
		return $s;
	}
}

if ( ! function_exists('seo3'))
{
	function seo3($s) {
		$c = array (' ');
		$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
	
		$s = str_replace($d, ' ', $s);
		
		$s = str_replace($c, '-', $s);
		return $s;
	}
}

if ( ! function_exists('url_encode'))
{
	function url_encode($data) {
		return urlencode($data);
	}
}

if ( ! function_exists('url_decode'))
{
	function url_decode($data) {
		return urldecode($data);
	}
}

if ( ! function_exists('validasi'))
{
	function validasi($str, $tipe){
		switch($tipe){
			default:
			case'sql':
				$str = stripcslashes($str);	
				$str = htmlspecialchars($str);				
				$str = preg_replace('/[^A-Za-z0-9]/','',$str);				
				return intval($str);
			break;
			case'xss':
				$str = stripcslashes($str);	
				$str = htmlspecialchars($str);
				$str = preg_replace('/[\W]/','', $str);
				return $str;
			break;
		}
	}
}

if ( ! function_exists('extension'))
{
	function extension($path) {
		$file = pathinfo($path);
		//if(file_exists($file['dirname'].'/'.$file['basename'])){
			return $file['extension'];
		//}		 
	}
}

if ( ! function_exists('validasi_sql'))
{
	function validasi_sql($data){
		$filter = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter;
	}
}

if ( ! function_exists('validasi_input'))
{
	function validasi_input($data){
		if ($data){
			$filter = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
			return $filter;
		}
		return null;
	}
}

if ( ! function_exists('re_html'))
{
	function re_html($data){
		$filter = htmlspecialchars(strip_tags($data));
		return $filter;
	}
}

if ( ! function_exists('html_encode'))
{
	function html_encode($data){
		$filter = htmlspecialchars($data);
		return $filter;
	}
}

if ( ! function_exists('html_decode'))
{
	function html_decode($data) {		
		return htmlspecialchars_decode($data, ENT_QUOTES);
	}
}

if ( ! function_exists('trim_text'))
{
	function trim_text($data, $length=""){		
		if ($length == "") {
			$length = 90;
		}
		
		if (strlen($data) > $length) {
			$hilang	= array('&nbsp;','&quot;');
			$isi_komentar = strip_tags($data);
			$isi = substr($isi_komentar,0, $length);
			$isi = substr($isi_komentar,0, strrpos($isi," "));
			return $isi.'...';
		} else {
			return strip_tags($data);
		}		
	}
}

if ( ! function_exists('capitalize'))
{
	function capitalize($data){		
		return ucwords(strtolower($data));
	}
}

function googl_shorturl($longUrl) {     
	$GoogleApiKey = 'AIzaSyAXr46hJ_6h6Aco_D9fBNahpRlc2cyIUOY';     
	$postData = array('longUrl' => $longUrl, 'key' => $GoogleApiKey);
    $jsonData = json_encode($postData);
    $curlObj = curl_init();
    curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
    curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
    //As the API is on https, set the value for CURLOPT_SSL_VERIFYPEER to false. This will stop cURL from verifying the SSL certificate.
    curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlObj, CURLOPT_HEADER, 0);
    curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
    curl_setopt($curlObj, CURLOPT_POST, 1);
    curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
    $response = curl_exec($curlObj);
    $json = json_decode($response);
    curl_close($curlObj);
    return $json->id;
}

function bit_ly_short_url($url, $format='txt') {
    $login = "your-bitly-login";
    $appkey = "your-bitly-application-key";
    $bitly_api = 'http://api.bit.ly/v3/shorten?login='.$login.'&apiKey='.$appkey.'&uri='.urlencode($url).'&format='.$format;
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$bitly_api);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

if ( ! function_exists('ping'))
{
	function ping($domain){
		$starttime = microtime(true);
		$file      = @fsockopen ($domain, 80, $errno, $errstr, 10);
		$stoptime  = microtime(true);
		$status    = 0;

		if (!$file) $status = -1;  // Site is down
		else {
			fclose($file);
			$status = ($stoptime - $starttime) * 1000;
			$status = floor($status);
		}
		
		
		if ($status != -1){
			return "OK";
		} else {
			return "TIDAK TERKONEKSI";
		}
	}
}

if ( ! function_exists('terbilang'))
{
	function terbilang($angka) {
		$angka = ceil($angka);
		$bilangan = array('','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan','sepuluh', 'sebelas');
		
		if ($angka < 12) {
			return $bilangan[$angka];
		} else if ($angka < 20) {
			return $bilangan[$angka - 10] . ' belas';
		} else if ($angka < 100) {
			$hasil_bagi = (int)($angka / 10);
			$hasil_mod = $angka % 10;
			return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
		} else if ($angka < 200) {
			return sprintf('seratus %s', terbilang($angka - 100));
		} else if ($angka < 1000) {
			$hasil_bagi = (int)($angka / 100);
			$hasil_mod = $angka % 100;
			return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
		} else if ($angka < 2000) {
			return trim(sprintf('seribu %s', terbilang($angka - 1000)));
		} else if ($angka < 1000000) {
			$hasil_bagi = (int)($angka / 1000);
			$hasil_mod = $angka % 1000;
			return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
		} else if ($angka < 1000000000) {
			$hasil_bagi = (int)($angka / 1000000);
			$hasil_mod = $angka % 1000000;
			return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else if ($angka < 1000000000000) {
			$hasil_bagi = (int)($angka / 1000000000);
			$hasil_mod = fmod($angka, 1000000000);
			return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else if ($angka < 1000000000000000) {
			$hasil_bagi = $angka / 1000000000000;
			$hasil_mod = fmod($angka, 1000000000000);
			return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
		} else {
			return '';
		}
	}
}

if ( ! function_exists('konversi_grade'))
{
	function konversi_grade($tahun_kode="", $nilai=""){
		$nilai = floor($nilai);
		$obj =& get_instance();
		$obj->load->model('Grade_model');
		$data = $obj->Grade_model->get_grade('grade_huruf', array('tahun_kode'=>$tahun_kode, 'grade_batas_bawah <='=>$nilai, 'grade_batas_atas >='=>$nilai));
		if ($data){
			return $data->grade_huruf;
		} else {
			return '';
		}
	}
}

if ( ! function_exists('trimString'))
{
	function trimString($data=""){
		$clear_char = array("\n", "\r", "\t", "\x0B", "&rsquo;", "&ldquo;", "&lsquo;", "`", "&#039;", "&#39;", "'");
		return strip_tags(preg_replace('/\s\s+/', ' ', str_replace($clear_char, '', str_replace("&nbsp;", ' ', $data))));
	}
	
}

if ( ! function_exists('konversi_romawi'))
{
	function konversi_romawi($angka)
	{
		$hsl = "";
		if ($angka < 1 || $angka > 5000) { 
			// Statement di atas buat nentuin angka ngga boleh dibawah 1 atau di atas 5000
			$hsl = "Batas Angka 1 s/d 5000";
		} else {
			while ($angka >= 1000) {
				// While itu termasuk kedalam statement perulangan
				// Jadi misal variable angka lebih dari sama dengan 1000
				// Kondisi ini akan di jalankan
				$hsl .= "M"; 
				// jadi pas di jalanin , kondisi ini akan menambahkan M ke dalam
				// Varible hsl
				$angka -= 1000;
				// Lalu setelah itu varible angka di kurangi 1000 ,
				// Kenapa di kurangi
				// Karena statment ini mengambil 1000 untuk di konversi menjadi M
			}
		}
		if ($angka >= 500) {
			// statement di atas akan bernilai true / benar
			// Jika var angka lebih dari sama dengan 500
			if ($angka > 500) {
				if ($angka >= 900) {
					$hsl .= "CM";
					$angka -= 900;
				} else {
					$hsl .= "D";
					$angka-=500;
				}
			}
		}
		while ($angka>=100) {
			if ($angka>=400) {
				$hsl .= "CD";
				$angka -= 400;
			} else {
				$angka -= 100;
			}
		}
		if ($angka>=50) {
			if ($angka>=90) {
				$hsl .= "XC";
				$angka -= 90;
			} else {
				$hsl .= "L";
				$angka-=50;
			}
		}
		while ($angka >= 10) {
			if ($angka >= 40) {
				$hsl .= "XL";
				$angka -= 40;
			} else {
				$hsl .= "X";
				$angka -= 10;
			}
		}
		if ($angka >= 5) {
			if ($angka == 9) {
				$hsl .= "IX";
				$angka-=9;
			} else {
				$hsl .= "V";
				$angka -= 5;
			}
		}
		while ($angka >= 1) {
			if ($angka == 4) {
				$hsl .= "IV"; 
				$angka -= 4;
			} else {
				$hsl .= "I";
				$angka -= 1;
			}
		}
		return ($hsl);
	}
}

if ( ! function_exists('rupiah'))
{
	function rupiah($nilai){
		$rupiah = number_format(ceil($nilai), 0, ',', '.');
		return "Rp".$rupiah;
	}	
}

if ( ! function_exists('rupiah2'))
{
	function rupiah2($nilai){
		$rupiah = number_format(ceil($nilai), 0, ',', '.');
		return $rupiah;
	}
}

if ( ! function_exists('onlyNumber'))
{
	function onlyNumber($number){
		if (strlen($number) == 6 && is_numeric($number)){
			return preg_replace("/[^0-9]/", "",$number);
		} else {
			return '';
		}
	}
}

if ( ! function_exists('hash_password'))
{
	function hash_password($password=""){
		$obj =& get_instance();
		$obj->load->library('bcrypt');
		if ($password){
			$hash = $obj->bcrypt->hash_password(md5($password));
			return $hash;
		} else {
			return "";
		}
	}
}



if ( ! function_exists('check_permission'))
{
	function check_permission($access=""){
		$obj =& get_instance();
		if ($access){
			$pengguna_level_id	= $obj->session->userdata('level');
			$check = $obj->db->query("SELECT pengguna_level_id FROM pengguna_level WHERE pengguna_level_id = '$pengguna_level_id' AND pengguna_level_akses = '$access'")->row();
			if ($check){
				return true;
			}
			return false;
			return false;
		} else {
			return true;
		}
		return false;
	}
}

/* End of file MY_application_helper.php */
/* Location: ./application/helpers/MY_application_helper.php */