<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * 
 *
 * @package		
 * @author		
 * @copyright	
 * @license		
 * @link		
 * @since		
 * @filesource
 */

// ------------------------------------------------------------------------

// Array Pilihan
// Mengubah array ke combobox key & value
if ( ! function_exists('ckeditor'))
{
	function ckeditor($option = '', $attribute, $height = 400, $width = "100%") {
		if ($option == 'mini'){
			return "
			<script type=\"text/javascript\" src=\"".base_url()."asset/editor/ckeditor.js\"></script>
			<script type=\"text/javascript\" src=\"".base_url()."asset/finder/ckfinder.js\"></script>
			<script type=\"text/javascript\">
				var editor = CKEDITOR.replace(\"".$attribute."\",{
					filebrowserBrowseUrl 	  : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php\",
					filebrowserImageBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Images\",
					filebrowserFlashBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Flash\",
					filebrowserUploadUrl 	  : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Files\",
					filebrowserImageUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Images\",
					filebrowserFlashUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Flash\",
					filebrowserWindowWidth    : 900,
					filebrowserWindowHeight   : 700,
					toolbarStartupExpanded 	  : false,
					resize_enabled 			  : false,
					height					  : ".$height.",
					width					  : \"".$width."\",
					toolbar	: [
						{ name: 'document', items: [ 'Source' ] },
						{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike' ] },
						{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
						{ name: 'links', items: [ 'Link', 'Unlink' ] },
						{ name: 'tools', items: [ 'Maximize' ] }
					]

				});
			</script>";
		} else if ($option == 'medium'){
			return "
			<script type=\"text/javascript\" src=\"".base_url()."asset/editor/ckeditor.js\"></script>
			<script type=\"text/javascript\" src=\"".base_url()."asset/finder/ckfinder.js\"></script>
			<script type=\"text/javascript\">
				var editor = CKEDITOR.replace(\"".$attribute."\",{
					filebrowserBrowseUrl 	  : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php\",
					filebrowserImageBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Images\",
					filebrowserFlashBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Flash\",
					filebrowserUploadUrl 	  : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Files\",
					filebrowserImageUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Images\",
					filebrowserFlashUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Flash\",
					filebrowserWindowWidth    : 900,
					filebrowserWindowHeight   : 700,
					toolbarStartupExpanded 	  : false,
					resize_enabled 			  : false,
					height					  : ".$height.",
					width					  : \"".$width."\",
					toolbar	: [
						{ name: 'document', items: [ 'Source' ] },
						{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike' ] },
						{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
						{ name: 'links', items: [ 'Link', 'Unlink' ] },
						{ name: 'insert', items: [ 'Image', 'Youtube' ] }, '/',
						{ name: 'styles', items: [ 'Format'] },
						{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
						{ name: 'clipboard', items: [ 'PasteText', 'PasteFromWord', '-', 'RemoveFormat', 'SpecialChar', '-', 'Outdent', 'Indent', '-', 'Undo', 'Redo', 'HorizontalRule' ] },
						{ name: 'tools', items: [ 'Maximize' ] }
					]
				});
			</script>";
		} else if ($option == 'notoolbar'){
			return "
			<script type=\"text/javascript\" src=\"".base_url()."asset/editor/ckeditor.js\"></script>
			<script type=\"text/javascript\" src=\"".base_url()."asset/finder/ckfinder.js\"></script>
			<script type=\"text/javascript\">
				var editor = CKEDITOR.replace(\"".$attribute."\",{
					filebrowserBrowseUrl 	  : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php\",
					filebrowserImageBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Images\",
					filebrowserFlashBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Flash\",
					filebrowserUploadUrl 	  : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Files\",
					filebrowserImageUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Images\",
					filebrowserFlashUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Flash\",
					filebrowserWindowWidth    : 900,
					filebrowserWindowHeight   : 700,
					toolbarStartupExpanded 	  : false,
					toolbarCanCollapse        : true,
					resize_enabled 			  : false,
					height					  : ".$height.",
					width					  : \"".$width."\",
					toolbar	: [
						{ name: 'document', items: [ 'Source', '-', 'Preview'] },
						{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
						{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
						{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
						{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
						{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe', 'Youtube' ] },
						{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
						{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
						{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }
					]

				});
			</script>";
		} else {
			return "
			<script type=\"text/javascript\" src=\"".base_url()."asset/editor/ckeditor.js\"></script>
			<script type=\"text/javascript\" src=\"".base_url()."asset/finder/ckfinder.js\"></script>
			<script type=\"text/javascript\">
				var editor = CKEDITOR.replace(\"".$attribute."\",{
					filebrowserBrowseUrl 	  : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php\",
					filebrowserImageBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Images\",
					filebrowserFlashBrowseUrl : \"".base_url()."asset/finder/eb3555b67d206fc7a9fe174c511e9c13.php?Type=Flash\",
					filebrowserUploadUrl 	  : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Files\",
					filebrowserImageUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Images\",
					filebrowserFlashUploadUrl : \"".base_url()."asset/finder/core/connector/php/connector.php?command=QuickUpload&type=Flash\",
					filebrowserWindowWidth    : 900,
					filebrowserWindowHeight   : 700,
					toolbarStartupExpanded 	  : false,
					resize_enabled 			  : false,
					height					  : ".$height.",
					width					  : \"".$width."\",
					toolbar	: [
						{ name: 'document', items: [ 'Source', '-', 'Preview'] },
						{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
						{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
						{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
						{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
						{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe', 'Youtube' ] },
						{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
						{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
						{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }
					]

				});
			</script>";
		}
	}
}

if ( ! function_exists('tinymce'))
{
	function tinymce($option = '', $attribute, $height = 500, $library=true) {
		$result = "";
		if ($library){
			$result .= "<script type=\"text/javascript\" src=\"".base_url()."asset/tinymce/js/tinymce/tinymce.min.js\"></script>";
		}
		if ($option == 'mini'){
			$result .= "
			<script>
				tinymce.init({
					selector: '{$attribute}',
					height: {$height},
					theme: 'modern',
					plugins: 'print preview powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help responsivefilemanager',
					toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
					relative_urls : false,
					remove_script_host : false,
					convert_urls : true,
					filemanager_title: \"My Filemanager\",
					filemanager_crossdomain: true,
					external_filemanager_path: \"".base_url()."asset/filemanager/\",
					external_plugins: {
						\"filemanager\": \"".base_url()."asset/filemanager/plugin.min.js\"
					},
					image_advtab: true,
					templates: [],
					content_css: [
						'".base_url()."asset/tinymce/css/google_1.css',
						'".base_url()."asset/tinymce/css/codepen.min.css'
					]
				});
			</script>";
		} else {
			$result .= "
			<script>
				tinymce.init({
					selector: '{$attribute}',
					height: {$height},
					theme: 'modern',
					plugins: 'print preview powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help responsivefilemanager',
					toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
					relative_urls : false,
					remove_script_host : false,
					convert_urls : true,
					filemanager_title: \"My Filemanager\",
					filemanager_crossdomain: true,
					external_filemanager_path: \"".base_url()."asset/filemanager/\",
					external_plugins: {
						\"filemanager\": \"".base_url()."asset/filemanager/plugin.min.js\"
					},
					image_advtab: true,
					templates: [],
					content_css: [
						'".base_url()."asset/tinymce/css/google_1.css',
						'".base_url()."asset/tinymce/css/codepen.min.css'
					]
				});
			</script>";
		}
		return $result;
	}
}

/* End of file my_editor_helper.php */
/* Location: ./application/helpers/my_editor_helper.php */