<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * 
 *
 * @package		
 * @author		
 * @copyright	
 * @license		
 * @link		
 * @since		
 * @filesource
 */

// ------------------------------------------------------------------------

if ( ! function_exists('theme_dir'))
{
	function theme_dir($data=""){
		$obj =& get_instance();
		return $obj->config->item('theme_directory') . $data;
	}
}

if ( ! function_exists('admin_dir'))
{
	function admin_dir($data=""){
		$obj =& get_instance();
		if ($data){
			return $obj->config->item('module_directory') . '/' . $data;
		} else {
			return $obj->config->item('module_directory');
		}
	}
}

if ( ! function_exists('admin_url'))
{
	function admin_url($data=""){
		$obj =& get_instance();
		if ($data){
			return site_url($obj->config->item('admin_url') . '/' . $data);
		} else {
			return site_url($obj->config->item('admin_url'));
		}
	}
}

if ( ! function_exists('static_page'))
{
	function static_page($param1="", $param2="deskripsi"){
		$obj =& get_instance();
		$obj->load->model('tulisan_model');
		if ($obj->tulisan_model->count_all_tulisan(array('tulisan_permalink'=>$param1)) > 0){
			$fetch = $obj->tulisan_model->get_tulisan("*", array('tulisan_permalink'=>$param1));
			$data['id']				= $fetch->tulisan_id;
			$data['caption']		= $fetch->tulisan_nama;
			$data['deskripsi']		= str_replace("http_base_url/", base_url(), html_decode($fetch->tulisan_deskripsi));
			$data['thumbnail']		= $fetch->tulisan_thumbnail;
			$data['author']			= $fetch->pengguna_nama_depan;
			$data['permalink']		= $fetch->tulisan_permalink;
			$data['tag']			= $fetch->tulisan_tag;
			$data['waktu']			= $fetch->tulisan_waktu;
			if (isset($data[$param2])){
				return $data[$param2];
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}

if ( ! function_exists('profile'))
{
	function profile($param1=""){
		$obj =& get_instance();
		$obj->load->model('Profil_model');
		if ($obj->Profil_model->get_profil()){
			$profile = $obj->Profil_model->get_profil();
			if (isset($profile->$param1)){
				return $profile->$param1;
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}

if ( ! function_exists('userdata'))
{
	function userdata($param1=""){
		$obj =& get_instance();
		$obj->load->model('Pengguna_model');
		if ($obj->Pengguna_model->get_pengguna()){
			$where['pengguna.pengguna_id']			= $obj->session->userdata('id');
			$where['pengguna.pengguna_level_id']	= $obj->session->userdata('level');
			$userdata 				= $obj->Pengguna_model->get_pengguna('*', $where);
			if (isset($userdata->$param1)){
				return $userdata->$param1;
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}

if ( ! function_exists('settings_get_value'))
{
	function settings_get_value($param1=""){
		$obj =& get_instance();
		$obj->load->model('Settings_model');
		if ($param1){
			$userdata = $obj->Settings_model->get_value($param1);
			if ($userdata){
				return $userdata->setting_value;
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}

if ( ! function_exists('settings_get_label'))
{
	function settings_get_label($param1=""){
		$obj =& get_instance();
		$obj->load->model('Settings_model');
		if ($param1){
			$userdata = $obj->Settings_model->get_value($param1);
			if ($userdata){
				return $userdata->setting_label;
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}

if ( ! function_exists('settings_get_key'))
{
	function settings_get_key($param1=""){
		$obj =& get_instance();
		$obj->load->model('Settings_model');
		if ($param1){
			$userdata = $obj->Settings_model->get_value($param1);
			if ($userdata){
				return $userdata->setting_key;
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
}


if ( ! function_exists('getHeader'))
{
	function getHeader(){
		$num = 1;
		$nama = 'SEKOLAH MENENGAH ATAS DARUL HIKAM';
		$akreditasi = 'TERAKREDITASI "A"';
		$alamat1 = 'JL. TUBAGUS ISMAIL DEPAN NO. 78 ';
		$te1p1 = '0222532571';
		$situs = 'www.smadarulhikam.sch.id';
		$email = 'Email: sma@darulhikam.com';
		$head =	"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
		$head .="	<tr>";
		$head .="		<td width=\"18%\" align=\"right\">";
		$head .="			<img height=\"75\" width=\"75\" src=\"".base_url('asset/profil/logodh.png')."\" />";
		$head .="		</td>";
		$head .="		<td valign=\"top\" align='center' class='style22'>";
							if ($num >  0) {	
		$head .="				<font size=\"5\">".$nama."</font><br />".
				"				";
		$head .="				<font size=\"4\">".$akreditasi."</font><br />".
				"				<strong class='style54'>". strtoupper($alamat1);
										
							if ($te1p1 != "") 
		$head .="				TELP/FAX. ". strtoupper($te1p1);	
							if ($situs != "" || $email != "")
		$head .="				<br>";
							if ($situs != "" ) 
		$head .="				website: ".$situs."&nbsp;&nbsp;";
							if ($email != "" ) 
		$head .="				Email: ".$email;
							
		$head .="			</strong>";
							}  
		$head .="			</td>";
		$head .="		</tr>";
		$head .="		<tr>";
		$head .="			<td colspan=\"2\"><hr width=\"100%\" /></td>";
		$head .="		</tr>";
		$head .="		</table>";
		// $head .="	<br />";

		echo $head;
	}
}

if ( ! function_exists('log_url'))
{
	function log_url(){
		$obj =& get_instance();
		$ip				= $_SERVER['REMOTE_ADDR'];		
		$tanggal		= date("Y-m-d H:i:s");
		$batas			= time();
		$obj->db->insert('log_url',array('log_ip'=>$ip,
										 'log_tanggal'=>$tanggal,
										 'log_kunjungan'=>$batas,
										 'log_url'=>current_url(),
										 'log_pengguna'=>$obj->session->userdata('id'),
										 'log_kelompok'=>$obj->session->userdata('level')));
	}
}

if ( ! function_exists('statistik'))
{
	function statistik(){
		$obj =& get_instance();
		$ip				= $_SERVER['REMOTE_ADDR'];		
		$tanggal		= date("Y-m-d");
		$batas			= time();
		$cek			= $obj->db->query("SELECT * FROM statistik WHERE statistik_ip='".$ip."' AND statistik_pengguna='".$obj->session->userdata('id')."' AND statistik_tanggal='$tanggal'");
		if($cek->num_rows()<1){
			$obj->db->insert('statistik',array('statistik_ip'=>$ip,'statistik_tanggal'=>$tanggal,'statistik_hits'=>1,'statistik_kunjungan'=>$batas,'statistik_pengguna'=>$obj->session->userdata('id'),'statistik_kelompok'=>$obj->session->userdata('level')));
		} else {
			$obj->db->query("UPDATE statistik SET statistik_tanggal='".$tanggal."', statistik_hits=statistik_hits + 1, statistik_kunjungan=".$batas." WHERE statistik_ip='".$ip."' AND statistik_pengguna='".$obj->session->userdata('id')."' AND statistik_tanggal='".$tanggal."'");
		}
	}
}

if ( ! function_exists('statistik_view'))
{
	function statistik_view(){
		$obj =& get_instance();
		$tanggal		= date("Y-m-d");
		$pengunjung		= $obj->db->where('statistik_tanggal', $tanggal)->group_by('statistik_pengguna')->get('statistik')->num_rows();
		$pengunjung_m	= $obj->db->query("SELECT * FROM statistik WHERE MONTH(statistik_tanggal) = '".date('m')."' GROUP BY statistik_pengguna")->num_rows();
		$pengunjung_y	= $obj->db->query("SELECT * FROM statistik WHERE YEAR(statistik_tanggal) = '".date('Y')."' GROUP BY statistik_pengguna")->num_rows();
		$t_pengunjung	= $obj->db->query("SELECT COUNT('statistik_hits') as jum FROM statistik");
		$rowt			= $t_pengunjung->row();
		
		//hits hari ini
		$hits			= $obj->db->query("SELECT SUM(statistik_hits) as hitstoday FROM statistik WHERE statistik_tanggal='$tanggal' GROUP BY statistik_tanggal");
		$hitsnya		= $hits->row();
		
		//total hits
		$t_hits			= $obj->db->select_sum('statistik_hits')->get('statistik');
		$rowthits		= $t_hits->row();
		
		//User Online
		$bataswaktu 	= time() - 600;
		$p_online 		= $obj->db->query("SELECT * FROM statistik WHERE statistik_kunjungan > '$bataswaktu'");
		$online 		= $p_online->num_rows();
		
		echo '
		<div class="row">
			<div class="col-xs-6">Sedang Online</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$online.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Kunjungan Hari Ini</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$pengunjung.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Kunjungan Bulan Ini</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$pengunjung_m.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Kunjungan Tahun Ini</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$pengunjung_y.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Total Kunjungan</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$rowt->jum.'</strong></div>
		</div>';
	}
}

if ( ! function_exists('statistik_view_1'))
{
	function statistik_view_1(){
		$obj =& get_instance();
		$tanggal		= date("Y-m-d");
		$pengunjung		= $obj->db->where('statistik_tanggal', $tanggal)->where('statistik_kelompok IN (10, 11, 15)')->group_by('statistik_pengguna')->get('statistik')->num_rows();
		$pengunjung_m	= $obj->db->query("SELECT * FROM statistik WHERE MONTH(statistik_tanggal) = '".date('m')."' AND statistik_kelompok IN (10, 11, 15) GROUP BY statistik_pengguna")->num_rows();
		$pengunjung_y	= $obj->db->query("SELECT * FROM statistik WHERE YEAR(statistik_tanggal) = '".date('Y')."' AND statistik_kelompok IN (10, 11, 15) GROUP BY statistik_pengguna")->num_rows();
		$t_pengunjung	= $obj->db->query("SELECT COUNT('statistik_hits') as jum FROM statistik WHERE statistik_kelompok IN (10, 11, 15)");
		$rowt			= $t_pengunjung->row();
		
		//hits hari ini
		$hits			= $obj->db->query("SELECT SUM(statistik_hits) as hitstoday FROM statistik WHERE statistik_tanggal='$tanggal' AND statistik_kelompok IN (10, 11, 15) GROUP BY statistik_tanggal");
		$hitsnya		= $hits->row();
		
		//total hits
		$t_hits			= $obj->db->select_sum('statistik_hits')->where('statistik_kelompok IN (10, 11, 15)')->get('statistik');
		$rowthits		= $t_hits->row();
		
		//User Online
		$bataswaktu 	= time() - 600;
		$p_online 		= $obj->db->query("SELECT * FROM statistik WHERE statistik_kunjungan > '$bataswaktu' AND statistik_kelompok IN (10, 11, 15)");
		$online 		= $p_online->num_rows();
		
		echo '
		<div class="row">
			<div class="col-xs-6">Sedang Online</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$online.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Kunjungan Hari Ini</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$pengunjung.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Kunjungan Bulan Ini</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$pengunjung_m.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Kunjungan Tahun Ini</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$pengunjung_y.'</strong></div>
		</div>';
		echo '
		<div class="row">
			<div class="col-xs-6">Total Kunjungan</div>
			<div class="col-xs-3">: &nbsp;&nbsp;<strong>'.$rowt->jum.'</strong></div>
		</div>';
	}
}
	

/* End of file MY_application_helper.php */
/* Location: ./application/helpers/MY_application_helper.php */