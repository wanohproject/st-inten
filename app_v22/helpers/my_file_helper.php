<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * 
 *
 * @package		
 * @author		
 * @copyright	
 * @license		
 * @link		
 * @since		
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Upload Image
 *
 * Opens the file specfied in the path and returns it as a string.
 *
 * @access	public
 * @param	string	path to file
 * @return	string
 */

if ( ! function_exists('upload_image'))
{
	function upload_image($file, $direktori, $filesize="", $nama_baru="", $saveori=true)
	{
		$lokasi_file    = $_FILES[$file]['tmp_name'];
		$tipe_file      = $_FILES[$file]['type'];
		$nama_file      = $_FILES[$file]['name'];
		$waktu			= time();

		$str 			= '0123456789';
		$shuffled 		= substr(str_shuffle($str), 0, 3);
		$waktu 			= $waktu . $shuffled;
		
		
		// $infopath	= pathinfo($nama_file);		
		// $ekstensi	= $infopath['extension'];

		if($tipe_file == "image/jpeg") {
			$ekstensi	= 'jpg';
		} elseif($tipe_file == "image/png") {
			$ekstensi	= 'png';
		} elseif($tipe_file == "image/gif") {
			$ekstensi	= 'gif';
		} else {
			$ekstensi	= 'jpg';
		}
		
		if ($nama_baru){
			$ori_src		= $direktori . $waktu .'-'. $nama_baru .'.'. $ekstensi;
		} else {
			$ori_src		= $direktori . $waktu .'-'. $nama_file;
		} 
		
		// Apabila ada gambar yang diupload
		if (!empty($lokasi_file)){
			
			//Simpan gambar asli
			if(move_uploaded_file($lokasi_file, $ori_src)){
				chmod("$ori_src", 0777);
			}
			
			if ($filesize != "") {
				//Nama baru untuk gambar kecil

				if ($saveori){
					//unlink($ori_src);
					if ($nama_baru) {
						$thumb_src = $direktori . "small-" . $waktu .'-'. $nama_baru .'.'. $ekstensi;
					} else {
						$thumb_src = $direktori . "small-" . $waktu .'-'. $nama_file;
					}
				} else {
					if ($nama_baru) {
						$thumb_src = $direktori . $waktu .'-'. $nama_baru .'.'. $ekstensi;
					} else {
						$thumb_src = $direktori . $waktu .'-'. $nama_file;
					}
				}
				
				// Membaca gambar
				if($tipe_file == "image/jpeg") {
					$im = imagecreatefromjpeg($ori_src);
				} elseif($tipe_file == "image/png") {
					$im = imagecreatefrompng($ori_src);
				} elseif($tipe_file == "image/gif") {
					$im = imagecreatefromgif($ori_src);
				} else {
					$im = false;
				}
				
				// Membaca ukuran gambar asli
				$width	= imageSX($im);
				$height	= imageSY($im);
				
				// Membaca ukuran maksimal gambar baru
				$size		= explode("x", $filesize);
				$max_width	= $size[0];
				$max_height	= $size[1];
				
				$ratio1	= $width/$max_width;
				$ratio2	= $height/$max_height;
				if($ratio1 > $ratio2) {
					$n_width	= $max_width;
					$n_height	= $height/$ratio1;
				} else {
					$n_height	= $max_height;
					$n_width	= $width/$ratio2;
				}
					
	
				if($im === false) {
					echo '<p>Gagal membuat thumnail</p>';
					exit;
				} elseif($tipe_file == "image/png"){
					$newimage = imagecreatetruecolor($n_width,$n_height);                 
					$bg = imagecolorallocate($newimage, 255, 255, 255);
					imagefill($newimage, 0, 0, $bg);
					
					imagecopyresampled($newimage, $im, 0, 0, 0, 0, $n_width, $n_height, $width, $height);										
					imagejpeg($newimage, $thumb_src);
					chmod("$thumb_src", 0777);
				} else {				
					$newimage = imagecreatetruecolor($n_width,$n_height);                 
					imagecopyresampled($newimage, $im, 0, 0, 0, 0, $n_width, $n_height, $width, $height);
					imagejpeg($newimage, $thumb_src);
					chmod("$thumb_src", 0777);
				}
			}
				
			if ($nama_baru){
				return $waktu .'-'. $nama_baru .'.'. $ekstensi;
			} else {
				return $waktu .'-'. $nama_file;
			}
		} else {			
			return '';
		}
	}
}

/**
 * Upload File
 *
 * Opens the file specfied in the path and returns it as a string.
 *
 * @access	public
 * @param	string	path to file
 * @return	string
 */
if ( ! function_exists('upload_file'))
{
	function upload_file($file, $direktori)
	{
		$lokasi_file    = $_FILES[$file]['tmp_name'];
		$tipe_file      = $_FILES[$file]['type'];
		$nama_file      = time() . '_' . filename_seo($_FILES[$file]['name']);
		
		// Apabila ada file yang diupload
		if (!empty($lokasi_file)){
			
			// Simpan gambar Asli
			move_uploaded_file($lokasi_file,$direktori . $nama_file);

			return $nama_file;
		} else {
			return "";
		}
	
	}
}

// --------------------------------------------------------------------

/**
 * For view size file
 */


if ( ! function_exists('fsize'))
{
	function fsize($file){
		$a = array("B", "KB", "MB", "GB", "TB", "PB");
		$pos = 0;
		$size = filesize($file);
		while ($size >= 1024){
			$size /= 1024;
			$pos++;
		}
		return round($size,2)." ".$a[$pos];
	}
}

/* End of file MY_file_helper.php */
/* Location: ./application/helpers/MY_file_helper.php */