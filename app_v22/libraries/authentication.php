<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class authentication {	
	
	private $menu;
	
	public function permission($callback="")
	{
		$CI =& get_instance();
		$CI->load->library('session');
		$CI->load->model('Menu_model');
		
		$this->validation();
		if ($CI->session->userdata('level') != 1 && $this->menu != 0){
			$module_name = $CI->router->fetch_module();
			$class_name = (!is_numeric($this->menu))?$this->menu:$CI->router->fetch_class();
			if ($CI->db->query("SELECT * FROM menu_pengguna 
											LEFT JOIN menu ON menu_pengguna.menu_id=menu.menu_id
											LEFT JOIN modul ON menu.modul_id=modul.modul_id
										WHERE menu_pengguna.pengguna_level_id = '".$CI->session->userdata('level')."'
											AND menu_class = '$class_name'
											AND modul_dir = '$module_name'")->num_rows() > 0){
			} else {
				if ($callback){
					redirect($callback);
				} else {
					redirect(site_url());
				}
			}
		}
	}
	
	public function validation($status=TRUE, $callback="")
	{
		$CI =& get_instance();
		$CI->load->library('session');

		$call = ($callback)?$callback:current_url();
		
		if ($CI->session->userdata('logged_in') === $status){
		} else {
			if ($callback){
				redirect($callback);
			} else {
				redirect(site_url('auth/logout')."?url=".urlencode($call));
			}
		}
	}
	
	public function set_menu($param1="")
	{
		$this->menu = $param1;
	}
}