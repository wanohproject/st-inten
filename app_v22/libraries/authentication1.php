<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class authentication {	
	
	private $menu;
	
	public function permission($callback="")
	{
		$CI =& get_instance();
		$CI->load->library('session');
		$CI->load->model('Menu_model');
		
		$this->validation();
		if ($CI->session->userdata('level') != 1 && $this->menu != 0){
			if ($CI->Menu_model->count_all_menu_pengguna(array('menu.menu_id'=>$this->menu, 'pengguna_level.pengguna_level_id'=>$CI->session->userdata('level'))) > 0){
			} else {
				if ($callback){
					redirect($callback);
				} else {
					redirect(site_url());
				}
			}
		}
	}
	
	public function validation($status=TRUE, $callback="")
	{
		$CI =& get_instance();
		$CI->load->library('session');
		
		if ($CI->session->userdata('logged_in') === $status){
		} else {
			if ($callback){
				redirect($callback);
			} else {
				redirect(site_url('auth/logout'));
			}
		}
	}
	
	public function set_menu($param1="")
	{
		$this->menu = $param1;
	}
}