24 NOV 2018 - 
DATABASE
1. ALTER TABLE `staf` ADD `staf_unit_awal` INT NULL AFTER `staf_pendidikan_terakhir`, ADD `staf_unit_sekarang` INT NULL AFTER `staf_unit_awal`, ADD `staf_mk_golongan` DOUBLE NULL AFTER `staf_unit_sekarang`, ADD `staf_mk_yayasan` DOUBLE NULL AFTER `staf_mk_golongan`, ADD `staf_cuti` SMALLINT NULL AFTER `staf_mk_yayasan`;

2. ALTER TABLE `siswa` ADD `siswa_user_orangtua` VARCHAR(50) NULL AFTER `siswa_user`;

3. ALTER TABLE `siswa` ADD `siswa_finger` VARCHAR(50) NULL AFTER `siswa_pin_orangtua`, ADD `siswa_card` VARCHAR(50) NULL AFTER `siswa_finger`;

4. ALTER TABLE `staf` ADD `staf_status_pegawai` VARCHAR(50) NULL AFTER `staf_cuti`;

5. ALTER TABLE `captcha` ADD `session_id` VARCHAR(50) NULL AFTER `word`;

6. CREATE TABLE `db_tarbak`.`settings` ( `setting_id` INT NOT NULL AUTO_INCREMENT , `setting_key` VARCHAR(50) NOT NULL , `setting_value` TEXT NOT NULL , `setting_label` VARCHAR(100) NOT NULL , PRIMARY KEY (`setting_id`)) ENGINE = MyISAM;

7. ALTER TABLE `settings` ADD UNIQUE(`setting_key`);



ALTER TABLE `akd_bobot_nilai` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_dosen` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_dosen_status` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_kurikulum` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_mahasiswa` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_mahasiswa_kuliah` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_mahasiswa_nilai` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_mahasiswa_skripsi` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_mahasiswa_status` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_matakuliah` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_perguruan_tinggi` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_program_studi` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_semester` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_tahun` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `akd_yayasan` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updated_at` `updated_at` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL;



